# pylint: disable=no-member,undefined-variable

# Imports
import h5py
import numpy as np

import heterocl as hcl
import hlib
import heterocl.tvm as tvm

from keras.datasets import mnist
from keras.utils import np_utils

from collections import OrderedDict

# Constants
DEBUG = False
IMG_H = 28
IMG_W = 28
WEIGHTS_FILE = 'mnist_weights_rescaled.h5'

# Let every operation be floating-point
hcl.init(hcl.Float())

def dbg_print(msg):
    if (DEBUG is True):
        print('[DEBUG] {}'.format(msg))

class DataFormatException(Exception):
    def __init__(self, data_format):
        super('Wrong data_format specified: {}'.format(data_format))

# TODO: Include in hlib
def global_max_pool(data, name='global_max_pool'):
    assert len(data.shape) == 4, 'only support 4-dim global pooling'
    batch, channels, height, width = data.shape

    kernel = (height,width)
    stride = (1,1)
    max_pool = hlib.nn.max_pool(data, kernel, stride)

    return hcl.compute(
        (batch, channels),
        lambda i,c: max_pool[i,c,0,0],
        dtype=data.dtype,
        name=name,
        attrs=OrderedDict([('app_name', tvm.make.StringImm('global_max_pool'))])
    )

def load_mnist_data(data_format='channels_first'):
    '''Load MNIST dataset.
    Parameters
    ----------
    data_format : str, optional
        The data format used (default is 'channels_first')
    '''
    (train_x, train_y), (test_x, test_y) = mnist.load_data()

    # Reshape data according to the specified data format
    if (data_format is 'channels_first'):
        train_x = train_x.reshape(train_x.shape[0], 1, IMG_H, IMG_W)
        test_x  = test_x.reshape(test_x.shape[0], 1, IMG_H, IMG_W)
    elif (data_format is 'channels_last'):
        train_x = train_x.reshape(train_x.shape[0], IMG_H, IMG_W, 1)
        test_x  = test_x.reshape(test_x.shape[0], IMG_H, IMG_W, 1)
    else:
        raise DataFormatException(data_format)

    # Convert to float32
    train_x = train_x.astype('float32')
    test_x  = test_x.astype('float32')

    dbg_print('train_x.shape: {}'.format(train_x.shape))
    dbg_print('train_y.shape: {}'.format(train_y.shape))
    dbg_print('test_x.shape : {}'.format(test_x.shape))
    dbg_print('test_y.shape : {}'.format(test_y.shape))

    # Convert class vectors to binary class matrices
    train_y = np_utils.to_categorical(train_y, 10)
    test_y  = np_utils.to_categorical(test_y, 10)

    # Invert images (from white on black to black on white)
    train_x = 255 - train_x
    test_x  = 255 - test_x

    return train_x, train_y, test_x, test_y

def get_weights() -> dict:
    # Initialize weights dict
    weights = dict()

    # Open weights file
    f = h5py.File(WEIGHTS_FILE, 'r')

    # Iterate over layers (reshape is needed because of format divergence between HeteroCL and Keras weights)
    weights['conv1']   = np.asarray(    f['conv1']['conv1']['kernel:0'], dtype='float32').transpose(3,2,0,1)#.reshape(  (4,1,3,3))
    weights['conv2']   = np.asarray(    f['conv2']['conv2']['kernel:0'], dtype='float32').transpose(3,2,0,1)#.reshape(  (4,4,3,3))
    weights['conv3']   = np.asarray(    f['conv3']['conv3']['kernel:0'], dtype='float32').transpose(3,2,0,1)#.reshape(  (8,4,3,3))
    weights['conv4']   = np.asarray(    f['conv4']['conv4']['kernel:0'], dtype='float32').transpose(3,2,0,1)#.reshape(  (8,8,3,3))
    weights['conv5']   = np.asarray(    f['conv5']['conv5']['kernel:0'], dtype='float32').transpose(3,2,0,1)#.reshape((16, 8,3,3))
    weights['conv6']   = np.asarray(    f['conv6']['conv6']['kernel:0'], dtype='float32').transpose(3,2,0,1)#.reshape((16,16,3,3))
    weights['dense_1'] = np.asarray(f['dense_1']['dense_1']['kernel:0'], dtype='float32')

    dbg_print('weights:')
    for key in weights.keys():
        dbg_print(' {}: {}'.format(key, weights[key].shape))
    
    return weights

def build_mnist(input_image, w_conv1, w_conv2, w_conv3, w_conv4, w_conv5, w_conv6, w_dense1, output):
    # First convolutional layer
    conv1 = hlib.nn.conv2d_nchw(input_image, w_conv1, padding='SAME', name='conv1')
    dbg_print(conv1)
    relu1 = hlib.nn.relu(conv1, name='relu1')
    dbg_print(relu1)

    # Second convolutional layer
    conv2 = hlib.nn.conv2d_nchw(relu1, w_conv2, padding='SAME', name='conv2')
    dbg_print(conv2)
    relu2 = hlib.nn.relu(conv2, name='relu2')
    dbg_print(relu2)

    # First max pooling
    pool1 = hlib.nn.max_pool(relu2, kernel=(2,2), stride=(2,2), name='pool1')
    dbg_print(pool1)

    # Third convolutional layer
    conv3 = hlib.nn.conv2d_nchw(pool1, w_conv3, padding='SAME', name='conv3')
    dbg_print(conv3)
    relu3 = hlib.nn.relu(conv3, name='relu3')
    dbg_print(relu3)

    # Fourth convolutional layer
    conv4 = hlib.nn.conv2d_nchw(relu3, w_conv4, padding='SAME', name='conv4')
    dbg_print(conv4)
    relu4 = hlib.nn.relu(conv4, name='relu4')
    dbg_print(relu4)

    # Second max pooling
    pool2 = hlib.nn.max_pool(relu4, kernel=(2,2), stride=(2,2), name='pool2')
    dbg_print(pool2)

    # Fifth convolutional layer
    conv5 = hlib.nn.conv2d_nchw(pool2, w_conv5, padding='SAME', name='conv5')
    dbg_print(conv5)
    relu5 = hlib.nn.relu(conv5, name='relu5')
    dbg_print(relu5)

    # Sixth convolutional layer
    conv6 = hlib.nn.conv2d_nchw(relu5, w_conv6, padding='SAME', name='conv6')
    dbg_print(conv6)
    relu6 = hlib.nn.relu(conv6, name='relu6')
    dbg_print(relu6)

    # Third max pooling
    pool3 = global_max_pool(relu6, name='pool3')
    dbg_print(pool3)

    # Output layer
    dense1 = hlib.nn.dense(pool3, w_dense1, name='dense1')
    dbg_print(dense1)

    return hlib.nn.softmax(output, dense1)

def build_mnist_inf(batch_size, weights, qtype1, qtype2, target=None):
    # Set placeholders
    input_image = hcl.placeholder((batch_size,1,IMG_H,IMG_W),    name='input')
    w_conv1     = hcl.placeholder(    weights['conv1'].shape,  name='w_conv1', dtype=qtype1)
    w_conv2     = hcl.placeholder(    weights['conv2'].shape,  name='w_conv2', dtype=qtype1)
    w_conv3     = hcl.placeholder(    weights['conv3'].shape,  name='w_conv3', dtype=qtype1)
    w_conv4     = hcl.placeholder(    weights['conv4'].shape,  name='w_conv4', dtype=qtype1)
    w_conv5     = hcl.placeholder(    weights['conv5'].shape,  name='w_conv5', dtype=qtype1)
    w_conv6     = hcl.placeholder(    weights['conv6'].shape,  name='w_conv6', dtype=qtype1)
    w_dense1    = hcl.placeholder(  weights['dense_1'].shape, name='w_dense1', dtype=qtype1)
    output      = hcl.placeholder((batch_size,10), name='output')

    # Create quantization scheme
    scheme = hcl.create_scheme(
        [input_image, w_conv1, w_conv2, w_conv3, w_conv4, w_conv5, w_conv6, w_dense1, output],
        build_mnist
    )

    # Quantize activation layers
    scheme.quantize(
        [build_mnist.relu1, build_mnist.relu2, build_mnist.relu3, build_mnist.relu4, build_mnist.relu5, build_mnist.relu6],
        qtype2
    )

    s = hcl.create_schedule_from_scheme(scheme)

    return hcl.build(s, target=target)


def export_weights(weights):
    import os
    import shutil
    import sys

    if (os.path.exists('weights')):
        shutil.rmtree('weights')
    os.mkdir('weights')

    for name in weights.keys():
        s = str()
        data = weights[name]
        s += 'const static float w_{}'.format(name)
        for dim in data.shape:
            s += '[{}]'.format(dim)
        s += ' = \n'
        s += '{}'.format(np.array2string(data, max_line_width=80, separator=',', threshold=sys.maxsize).replace('[', '{').replace(']', '}'))
        s += ';'
        
        file = open('weights/w_{}.h'.format(name), 'w')
        file.write(s)
        file.close()


def get_accuracy(weights, qtype1, qtype2):
    batch_size = 1
    accuracy = 0

    print('Using qtype1={}, qtype2={}'.format(qtype1, qtype2))

    f = build_mnist_inf(batch_size, weights, qtype1, qtype2)

    # Prepare numpy weights for testing
    w_conv1_hcl  = hcl.asarray(  weights['conv1'], dtype=qtype1)
    w_conv2_hcl  = hcl.asarray(  weights['conv2'], dtype=qtype1)
    w_conv3_hcl  = hcl.asarray(  weights['conv3'], dtype=qtype1)
    w_conv4_hcl  = hcl.asarray(  weights['conv4'], dtype=qtype1)
    w_conv5_hcl  = hcl.asarray(  weights['conv5'], dtype=qtype1)
    w_conv6_hcl  = hcl.asarray(  weights['conv6'], dtype=qtype1)
    w_dense1_hcl = hcl.asarray(weights['dense_1'], dtype=qtype1)

    _, _, test_x, test_y = load_mnist_data()

    for i in range(len(test_x) // batch_size):
        label = np.argmax(test_y[i*batch_size:(i+1)*batch_size], axis=1)
        input_image_np = test_x[i*batch_size:(i+1)*batch_size]
        input_image_hcl = hcl.asarray(input_image_np)
        output_hcl = hcl.asarray(np.zeros((batch_size,10)))

        f(input_image_hcl,
            w_conv1_hcl, w_conv2_hcl, w_conv3_hcl, w_conv4_hcl, w_conv5_hcl, w_conv6_hcl, w_dense1_hcl,
            output_hcl)

        prediction = np.argmax(output_hcl.asnumpy(), axis=1)
        accuracy += np.sum(np.equal(prediction, label))

    print("-> Testing accuracy: {}%".format(accuracy / len(test_x) * 100.0))

    # Get Vivado HLS code
    f = build_mnist_inf(batch_size, weights, qtype1, qtype2, target='vhls')

    file = open('hls_fp{}.cpp'.format(qtype2.bits), 'w')

    file.write('/**\n')
    file.write(' * Testing accuracy: {}\n'.format(float(accuracy / float(len(test_x)))))
    file.write(' */\n\n')
    file.write(f)

    file.close()

def main():
    weights = get_weights()

    if False:
        qtypes = [hcl.Fixed(32,30), hcl.Fixed(16,14), hcl.Fixed(8,6), hcl.Fixed(4,2)]

        for qtype in qtypes:
            get_accuracy(weights, hcl.Float(), qtype)
    
    else:
        export_weights(weights)

if __name__ == '__main__':
    main()