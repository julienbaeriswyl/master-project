# Imports
import os
import numpy as np

# Set backend to Tensorflow
os.environ['KERAS_BACKEND'] = 'tensorflow'
# Disable GPU and use CPU
os.environ['CUDA_VISIBLE_DEVICES'] = '0'
# Disable Tensorflow logging
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

from PIL import Image

import tensorflow as tf
from keras.datasets import mnist
from keras.layers import Activation, Input, Dense, GlobalAveragePooling2D, GlobalMaxPooling2D, Conv2D, MaxPooling2D
from keras.models import Model
from keras.optimizers import Adam
from keras.utils import np_utils

import matplotlib.pyplot as plt

# Constants
DEBUG = False
IMG_H = 28
IMG_W = 28
WEIGHTS_PATH = 'mnist_weights.h5'
RESCALED_WEIGHTS_PATH = 'mnist_weights_rescaled.h5'

class DataFormatException(Exception):
    def __init__(self, data_format):
        super('Wrong data_format specified: {}'.format(data_format))

def dbg_print(msg):
    if (DEBUG is True):
        print('[DEBUG] {}'.format(msg))

def load_mnist_data(data_format='channels_first'):
    '''
    Load MNIST dataset.

    Parameters
    ----------
    data_format : str, optional
        The data format used (default is 'channels_first')

    Return
    -------
    train_x, train_y, test_x, test_y : array, array, array, array
        Training images, training labels, testing images, testing labels.
    '''
    (train_x, train_y), (test_x, test_y) = mnist.load_data()

    # Reshape data according to the specified data format
    if (data_format is 'channels_first'):
        train_x = train_x.reshape(train_x.shape[0], 1, IMG_H, IMG_W)
        test_x  = test_x.reshape(test_x.shape[0], 1, IMG_H, IMG_W)
    elif (data_format is 'channels_last'):
        train_x = train_x.reshape(train_x.shape[0], IMG_H, IMG_W, 1)
        test_x  = test_x.reshape(test_x.shape[0], IMG_H, IMG_W, 1)
    else:
        raise DataFormatException(data_format)

    # Convert to float32
    train_x = train_x.astype('float32')
    test_x  = test_x.astype('float32')

    dbg_print('train_x.shape: {}'.format(train_x.shape))
    dbg_print('test_x.shape : {}'.format(test_x.shape))

    # Convert class vectors to binary class matrices
    train_y = np_utils.to_categorical(train_y, 10)
    test_y  = np_utils.to_categorical(test_y, 10)

    # Invert images (from white on black to black on white)
    train_x = 255 - train_x
    test_x  = 255 - test_x

    return train_x, train_y, test_x, test_y

def build_model(data_format='channels_first', use_bias=False):
    '''
    Define network architecture.

    Parameters
    ----------
    data_format : str, optional
        The data format used (default is 'channels_first').
    use_bias    : boolean, optional
        Whether to use bias or not (default is False).

    Return
    -------
    model       : keras.models.Model
        The Keras model of the network.
    '''
    # Input is 28x28 grayscale (1 component) pixels
    if (data_format is 'channels_first'):
        input = Input((1, IMG_H, IMG_W))
    elif (data_format is 'channels_last'):
        input = Input((IMG_H, IMG_W, 1))
    else:
        raise DataFormatException(data_format)

    conv1 = Conv2D(4, (3,3), activation='relu', padding='same', data_format=data_format, name='conv1', use_bias=use_bias)(input)
    conv2 = Conv2D(4, (3,3), activation='relu', padding='same', data_format=data_format, name='conv2', use_bias=use_bias)(conv1)
    pool1 = MaxPooling2D((2,2), strides=(2,2), data_format=data_format, name='pool1')(conv2)

    conv3 = Conv2D(8, (3,3), activation='relu', padding='same', data_format=data_format, name='conv3', use_bias=use_bias)(pool1)
    conv4 = Conv2D(8, (3,3), activation='relu', padding='same', data_format=data_format, name='conv4', use_bias=use_bias)(conv3)
    pool2 = MaxPooling2D((2,2), strides=(2,2), data_format=data_format, name='pool2')(conv4)

    conv5 = Conv2D(16, (3,3), activation='relu', padding='same', data_format=data_format, name='conv5', use_bias=use_bias)(pool2)
    conv6 = Conv2D(16, (3,3), activation='relu', padding='same', data_format=data_format, name='conv6', use_bias=use_bias)(conv5)
    pool3 = GlobalMaxPooling2D(data_format=data_format, name='pool3')(conv6)

    dense1 = Dense(10, activation=None, use_bias=use_bias)(pool3)
    output = Activation('softmax')(dense1)

    model = Model(inputs=input, outputs=output)
    model.summary(print_fn=dbg_print)

    return model

def train_model(predict_image=False, data_format='channels_first') -> Model:
    '''
    Build and train the neural network model, then save into the file defined by
    the `WEIGHTS_PATH` constant.
    If the file already exists, load it.

    Parameters
    ----------
    predict_image   : boolean, optional
        Whether to predict class from `test_image.png` or not (default is False).

    Return
    ------
    model   : keras.models.Model
        The trained model.
    '''
    # Build Keras model
    model = build_model(data_format=data_format)

    # Check if model has already been trained
    if (os.path.isfile(WEIGHTS_PATH) is False):
        print('Weights file ({}) could not be found. Initiating model training...'.format(WEIGHTS_PATH))
        # Load MNIST dataset
        train_x, train_y, test_x, test_y = load_mnist_data(data_format=data_format)

        # Training parameters
        learning_rate = 0.001
        optimizer = Adam(lr=learning_rate)
        loss='categorical_crossentropy'
        metrics=['accuracy']
        batch_size = 128
        epochs = 10

        model.compile(optimizer=optimizer, loss=loss, metrics=metrics)

        model.fit(train_x, train_y, batch_size=batch_size, epochs=epochs, verbose=1, validation_data=(test_x, test_y))
        score = model.evaluate(test_x, test_y, verbose=0)

        print('Model score   :', score[0])
        print('Model accuracy:', score[1])

        model.save(WEIGHTS_PATH)
        print('Model saved into {}'.format(WEIGHTS_PATH))
    else:
        print('Weights file exists. Loading model...')
        model.load_weights(WEIGHTS_PATH)

    if (predict_image is True):
        test_img = np.asarray(Image.open('test_image.png'), dtype='float32')
        test_img = np.reshape(test_img, (1,1,IMG_H,IMG_W))

        preds = model.predict(test_img, batch_size=1)
        print('predictions: {}'.format(preds))
        print('predicted  : {} (expected: 3)'.format(np.argmax(preds, axis=1)))

    return model

def rescale_weights(model: Model, data_format='channels_first') -> Model:
    # Coefficient to make safe gap for found range to prevent overflow. Lower = less safe. Higher = more rounding error.
    GAP_COEFF = 1.1

    # Check if weights have already been rescaled
    if (os.path.isfile(RESCALED_WEIGHTS_PATH) is False):
        print('Rescaled weights file ({}) could not be found. Initiating weights rescaling...'.format(RESCALED_WEIGHTS_PATH))
        # Get all images from MNIST dataset
        train_x, _, test_x, _ = load_mnist_data(data_format=data_format)
        x = np.concatenate((train_x, test_x))

        # Initialize reduction rate
        reduction_rate = 1.0

        # Iterate over layers
        for layer in model.layers:
            dbg_print('Getting min and max value for layer \'{}\''.format(layer.name))

            weights = layer.get_weights()
            # Check there are weights available
            if (len(weights) > 0):
                # Extract submodel from original model
                submodel = Model(inputs=model.inputs, outputs=layer.output)
                submodel.summary(print_fn=dbg_print)
                preds = submodel.predict(x)

                # We're looking for the most out and about the scales. Weights should not exceed 1.0 including.
                coeff = GAP_COEFF * max(abs(preds.min()), abs(preds.max()), abs(weights[0].min()), abs(weights[0].max()))

                dbg_print(' Submodel shape   : {}'.format(preds.shape))
                dbg_print(' Min preds value  : {}, max: {}'.format(preds.min(), preds.max()))
                dbg_print(' Min weights value: {}, max: {}'.format(weights[0].min(), weights[0].max()))
                dbg_print(' Reduction coeff  : {}'.format(coeff))

                # Rescale weights
                layer.set_weights(weights / coeff)
                reduction_rate = reduction_rate * coeff
            else:
                dbg_print('-> No weights available')
            # For better readability
            dbg_print('')

        model.save_weights(RESCALED_WEIGHTS_PATH)
        print('Rescaled weights saved into {}'.format(RESCALED_WEIGHTS_PATH))
        print('Reduction rate: {}'.format(reduction_rate))
    else:
        print('Rescaled weights file exists. Loading model...')
        model.load_weights(RESCALED_WEIGHTS_PATH)

    return model

def main():
    data_format = 'channels_last'

    print('=== MODEL TRAINING ===')
    model = train_model(predict_image=False, data_format=data_format)

    print('=== WEIGHTS RESCALING ===')
    model = rescale_weights(model, data_format=data_format)

    model.save('mnist_model_rescaled.h5')

    _, _, test_x, test_y = load_mnist_data(data_format=data_format)

    labels = np.argmax(test_y, axis=1)
    # predictions = np.argmax(model.predict(test_x), axis=1)
    # score = np.sum(np.equal(predictions, labels))
    predictions = np.zeros(labels.shape)
    times = np.zeros(labels.shape)

    import time
    for i in range(len(test_x)):
        image = test_x[i].reshape(1,28,28,1)

        time_start = time.perf_counter()
        preds = model.predict(image)
        time_end = time.perf_counter()

        predictions[i] = np.argmax(preds, axis=1)
        times[i] = time_end - time_start

    score = np.sum(np.equal(predictions, labels))
    print('\n-> Testing accuracy: {}%'.format((score / len(test_x) * 100.0)))
    print('-> Inference time: {} ms'.format(np.average(times) * 1000))

if __name__ == '__main__':
    main()