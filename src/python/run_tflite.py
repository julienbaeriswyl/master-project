# import tflite_runtime.interpreter as tflite
import tensorflow as tf
from keras.datasets import mnist
import numpy as np

(train_x, train_y), (test_x, test_y) = mnist.load_data()
train_x = train_x.reshape(train_x.shape[0], 28, 28, 1)
test_x = test_x.reshape(test_x.shape[0], 28, 28, 1)

train_x = train_x.astype('float32')
test_x = test_x.astype('float32')

train_x = 255 - train_x
test_x  = 255 - test_x

interpreter = tf.lite.Interpreter(model_path='mnist_rescaled.tflite')
interpreter.allocate_tensors()

input_details = interpreter.get_input_details()
output_details = interpreter.get_output_details()

input_data = test_x[0].reshape(1,28,28,1)

input_data = np.zeros((1,28,28,1), dtype='float32')

interpreter.set_tensor(input_details[0]['index'], input_data)
interpreter.invoke()

output_data = interpreter.get_tensor(output_details[0]['index'])
# result = np.argmax(np.squeeze(output_data), axis=0)
result = output_data
print('label: {}'.format(test_y[0]))
print('result: {}'.format(result))