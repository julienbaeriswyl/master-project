//Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
//--------------------------------------------------------------------------------
//Tool Version: Vivado v.2018.2 (lin64) Build 2258646 Thu Jun 14 20:02:38 MDT 2018
//Date        : Fri Jan 24 14:09:37 2020
//Host        : ubuntu-lucas running 64-bit Ubuntu 16.04.3 LTS
//Command     : generate_target design_1_wrapper.bd
//Design      : design_1_wrapper
//Purpose     : IP block netlist
//--------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module design_1_wrapper
   (RESULT_0,
    STOP_0,
    sys_clock);
  output [3:0]RESULT_0;
  output STOP_0;
  input sys_clock;

  wire [3:0]RESULT_0;
  wire STOP_0;
  wire sys_clock;

  design_1 design_1_i
       (.RESULT_0(RESULT_0),
        .STOP_0(STOP_0),
        .sys_clock(sys_clock));
endmodule
