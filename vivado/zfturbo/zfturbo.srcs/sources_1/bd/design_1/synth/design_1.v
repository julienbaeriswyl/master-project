//Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
//--------------------------------------------------------------------------------
//Tool Version: Vivado v.2018.2 (lin64) Build 2258646 Thu Jun 14 20:02:38 MDT 2018
//Date        : Fri Jan 24 14:09:37 2020
//Host        : ubuntu-lucas running 64-bit Ubuntu 16.04.3 LTS
//Command     : generate_target design_1.bd
//Design      : design_1
//Purpose     : IP block netlist
//--------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CORE_GENERATION_INFO = "design_1,IP_Integrator,{x_ipVendor=xilinx.com,x_ipLibrary=BlockDiagram,x_ipName=design_1,x_ipVersion=1.00.a,x_ipLanguage=VERILOG,numBlks=5,numReposBlks=5,numNonXlnxBlks=0,numHierBlks=0,maxHierDepth=0,numSysgenBlks=0,numHlsBlks=0,numHdlrefBlks=1,numPkgbdBlks=0,bdsource=USER,da_board_cnt=2,da_clkrst_cnt=3,synth_mode=OOC_per_IP}" *) (* HW_HANDOFF = "design_1.hwdef" *) 
module design_1
   (RESULT_0,
    STOP_0,
    sys_clock);
  output [3:0]RESULT_0;
  output STOP_0;
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 CLK.SYS_CLOCK CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME CLK.SYS_CLOCK, CLK_DOMAIN design_1_sys_clock, FREQ_HZ 125000000, PHASE 0.000" *) input sys_clock;

  wire [3:0]TOP_0_RESULT;
  wire TOP_0_STOP;
  wire clk_wiz_clk_out1;
  wire sys_clock_1;
  wire [0:0]xlconstant_0_dout;
  wire [3:0]xlconstant_1_dout;
  wire [12:0]xlconstant_2_dout;

  assign RESULT_0[3:0] = TOP_0_RESULT;
  assign STOP_0 = TOP_0_STOP;
  assign sys_clock_1 = sys_clock;
  design_1_TOP_0_1 TOP_0
       (.GO(xlconstant_0_dout),
        .RESULT(TOP_0_RESULT),
        .STOP(TOP_0_STOP),
        .address_p_database(xlconstant_2_dout),
        .clk(clk_wiz_clk_out1),
        .dp_database(xlconstant_1_dout),
        .we_database(xlconstant_0_dout));
  design_1_clk_wiz_0 clk_wiz
       (.clk_in1(sys_clock_1),
        .clk_out1(clk_wiz_clk_out1));
  design_1_xlconstant_0_0 xlconstant_0
       (.dout(xlconstant_0_dout));
  design_1_xlconstant_1_0 xlconstant_1
       (.dout(xlconstant_1_dout));
  design_1_xlconstant_2_0 xlconstant_2
       (.dout(xlconstant_2_dout));
endmodule
