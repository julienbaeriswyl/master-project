# Definitional proc to organize widgets for parameters.
proc init_gui { IPINST } {
  ipgui::add_param $IPINST -name "Component_Name"
  #Adding Page
  set Page_0 [ipgui::add_page $IPINST -name "Page 0"]
  ipgui::add_param $IPINST -name "SIZE_1" -parent ${Page_0}
  ipgui::add_param $IPINST -name "SIZE_2" -parent ${Page_0}
  ipgui::add_param $IPINST -name "SIZE_3" -parent ${Page_0}
  ipgui::add_param $IPINST -name "SIZE_4" -parent ${Page_0}
  ipgui::add_param $IPINST -name "SIZE_5" -parent ${Page_0}
  ipgui::add_param $IPINST -name "SIZE_6" -parent ${Page_0}
  ipgui::add_param $IPINST -name "SIZE_7" -parent ${Page_0}
  ipgui::add_param $IPINST -name "SIZE_8" -parent ${Page_0}
  ipgui::add_param $IPINST -name "SIZE_9" -parent ${Page_0}
  ipgui::add_param $IPINST -name "SIZE_address_pix" -parent ${Page_0}
  ipgui::add_param $IPINST -name "SIZE_address_pix_t" -parent ${Page_0}
  ipgui::add_param $IPINST -name "SIZE_address_wei" -parent ${Page_0}
  ipgui::add_param $IPINST -name "convolution_size" -parent ${Page_0}
  ipgui::add_param $IPINST -name "num_conv" -parent ${Page_0}
  ipgui::add_param $IPINST -name "picture_size" -parent ${Page_0}
  ipgui::add_param $IPINST -name "picture_storage_limit" -parent ${Page_0}
  ipgui::add_param $IPINST -name "picture_storage_limit_2" -parent ${Page_0}
  ipgui::add_param $IPINST -name "razmpar" -parent ${Page_0}
  ipgui::add_param $IPINST -name "razmpar2" -parent ${Page_0}


}

proc update_PARAM_VALUE.SIZE_1 { PARAM_VALUE.SIZE_1 } {
	# Procedure called to update SIZE_1 when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.SIZE_1 { PARAM_VALUE.SIZE_1 } {
	# Procedure called to validate SIZE_1
	return true
}

proc update_PARAM_VALUE.SIZE_2 { PARAM_VALUE.SIZE_2 } {
	# Procedure called to update SIZE_2 when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.SIZE_2 { PARAM_VALUE.SIZE_2 } {
	# Procedure called to validate SIZE_2
	return true
}

proc update_PARAM_VALUE.SIZE_3 { PARAM_VALUE.SIZE_3 } {
	# Procedure called to update SIZE_3 when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.SIZE_3 { PARAM_VALUE.SIZE_3 } {
	# Procedure called to validate SIZE_3
	return true
}

proc update_PARAM_VALUE.SIZE_4 { PARAM_VALUE.SIZE_4 } {
	# Procedure called to update SIZE_4 when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.SIZE_4 { PARAM_VALUE.SIZE_4 } {
	# Procedure called to validate SIZE_4
	return true
}

proc update_PARAM_VALUE.SIZE_5 { PARAM_VALUE.SIZE_5 } {
	# Procedure called to update SIZE_5 when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.SIZE_5 { PARAM_VALUE.SIZE_5 } {
	# Procedure called to validate SIZE_5
	return true
}

proc update_PARAM_VALUE.SIZE_6 { PARAM_VALUE.SIZE_6 } {
	# Procedure called to update SIZE_6 when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.SIZE_6 { PARAM_VALUE.SIZE_6 } {
	# Procedure called to validate SIZE_6
	return true
}

proc update_PARAM_VALUE.SIZE_7 { PARAM_VALUE.SIZE_7 } {
	# Procedure called to update SIZE_7 when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.SIZE_7 { PARAM_VALUE.SIZE_7 } {
	# Procedure called to validate SIZE_7
	return true
}

proc update_PARAM_VALUE.SIZE_8 { PARAM_VALUE.SIZE_8 } {
	# Procedure called to update SIZE_8 when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.SIZE_8 { PARAM_VALUE.SIZE_8 } {
	# Procedure called to validate SIZE_8
	return true
}

proc update_PARAM_VALUE.SIZE_9 { PARAM_VALUE.SIZE_9 } {
	# Procedure called to update SIZE_9 when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.SIZE_9 { PARAM_VALUE.SIZE_9 } {
	# Procedure called to validate SIZE_9
	return true
}

proc update_PARAM_VALUE.SIZE_address_pix { PARAM_VALUE.SIZE_address_pix } {
	# Procedure called to update SIZE_address_pix when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.SIZE_address_pix { PARAM_VALUE.SIZE_address_pix } {
	# Procedure called to validate SIZE_address_pix
	return true
}

proc update_PARAM_VALUE.SIZE_address_pix_t { PARAM_VALUE.SIZE_address_pix_t } {
	# Procedure called to update SIZE_address_pix_t when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.SIZE_address_pix_t { PARAM_VALUE.SIZE_address_pix_t } {
	# Procedure called to validate SIZE_address_pix_t
	return true
}

proc update_PARAM_VALUE.SIZE_address_wei { PARAM_VALUE.SIZE_address_wei } {
	# Procedure called to update SIZE_address_wei when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.SIZE_address_wei { PARAM_VALUE.SIZE_address_wei } {
	# Procedure called to validate SIZE_address_wei
	return true
}

proc update_PARAM_VALUE.convolution_size { PARAM_VALUE.convolution_size } {
	# Procedure called to update convolution_size when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.convolution_size { PARAM_VALUE.convolution_size } {
	# Procedure called to validate convolution_size
	return true
}

proc update_PARAM_VALUE.num_conv { PARAM_VALUE.num_conv } {
	# Procedure called to update num_conv when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.num_conv { PARAM_VALUE.num_conv } {
	# Procedure called to validate num_conv
	return true
}

proc update_PARAM_VALUE.picture_size { PARAM_VALUE.picture_size } {
	# Procedure called to update picture_size when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.picture_size { PARAM_VALUE.picture_size } {
	# Procedure called to validate picture_size
	return true
}

proc update_PARAM_VALUE.picture_storage_limit { PARAM_VALUE.picture_storage_limit } {
	# Procedure called to update picture_storage_limit when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.picture_storage_limit { PARAM_VALUE.picture_storage_limit } {
	# Procedure called to validate picture_storage_limit
	return true
}

proc update_PARAM_VALUE.picture_storage_limit_2 { PARAM_VALUE.picture_storage_limit_2 } {
	# Procedure called to update picture_storage_limit_2 when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.picture_storage_limit_2 { PARAM_VALUE.picture_storage_limit_2 } {
	# Procedure called to validate picture_storage_limit_2
	return true
}

proc update_PARAM_VALUE.razmpar { PARAM_VALUE.razmpar } {
	# Procedure called to update razmpar when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.razmpar { PARAM_VALUE.razmpar } {
	# Procedure called to validate razmpar
	return true
}

proc update_PARAM_VALUE.razmpar2 { PARAM_VALUE.razmpar2 } {
	# Procedure called to update razmpar2 when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.razmpar2 { PARAM_VALUE.razmpar2 } {
	# Procedure called to validate razmpar2
	return true
}


proc update_MODELPARAM_VALUE.num_conv { MODELPARAM_VALUE.num_conv PARAM_VALUE.num_conv } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.num_conv}] ${MODELPARAM_VALUE.num_conv}
}

proc update_MODELPARAM_VALUE.SIZE_1 { MODELPARAM_VALUE.SIZE_1 PARAM_VALUE.SIZE_1 } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.SIZE_1}] ${MODELPARAM_VALUE.SIZE_1}
}

proc update_MODELPARAM_VALUE.SIZE_2 { MODELPARAM_VALUE.SIZE_2 PARAM_VALUE.SIZE_2 } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.SIZE_2}] ${MODELPARAM_VALUE.SIZE_2}
}

proc update_MODELPARAM_VALUE.SIZE_3 { MODELPARAM_VALUE.SIZE_3 PARAM_VALUE.SIZE_3 } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.SIZE_3}] ${MODELPARAM_VALUE.SIZE_3}
}

proc update_MODELPARAM_VALUE.SIZE_4 { MODELPARAM_VALUE.SIZE_4 PARAM_VALUE.SIZE_4 } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.SIZE_4}] ${MODELPARAM_VALUE.SIZE_4}
}

proc update_MODELPARAM_VALUE.SIZE_5 { MODELPARAM_VALUE.SIZE_5 PARAM_VALUE.SIZE_5 } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.SIZE_5}] ${MODELPARAM_VALUE.SIZE_5}
}

proc update_MODELPARAM_VALUE.SIZE_6 { MODELPARAM_VALUE.SIZE_6 PARAM_VALUE.SIZE_6 } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.SIZE_6}] ${MODELPARAM_VALUE.SIZE_6}
}

proc update_MODELPARAM_VALUE.SIZE_7 { MODELPARAM_VALUE.SIZE_7 PARAM_VALUE.SIZE_7 } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.SIZE_7}] ${MODELPARAM_VALUE.SIZE_7}
}

proc update_MODELPARAM_VALUE.SIZE_8 { MODELPARAM_VALUE.SIZE_8 PARAM_VALUE.SIZE_8 } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.SIZE_8}] ${MODELPARAM_VALUE.SIZE_8}
}

proc update_MODELPARAM_VALUE.SIZE_9 { MODELPARAM_VALUE.SIZE_9 PARAM_VALUE.SIZE_9 } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.SIZE_9}] ${MODELPARAM_VALUE.SIZE_9}
}

proc update_MODELPARAM_VALUE.SIZE_address_pix { MODELPARAM_VALUE.SIZE_address_pix PARAM_VALUE.SIZE_address_pix } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.SIZE_address_pix}] ${MODELPARAM_VALUE.SIZE_address_pix}
}

proc update_MODELPARAM_VALUE.SIZE_address_pix_t { MODELPARAM_VALUE.SIZE_address_pix_t PARAM_VALUE.SIZE_address_pix_t } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.SIZE_address_pix_t}] ${MODELPARAM_VALUE.SIZE_address_pix_t}
}

proc update_MODELPARAM_VALUE.SIZE_address_wei { MODELPARAM_VALUE.SIZE_address_wei PARAM_VALUE.SIZE_address_wei } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.SIZE_address_wei}] ${MODELPARAM_VALUE.SIZE_address_wei}
}

proc update_MODELPARAM_VALUE.picture_size { MODELPARAM_VALUE.picture_size PARAM_VALUE.picture_size } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.picture_size}] ${MODELPARAM_VALUE.picture_size}
}

proc update_MODELPARAM_VALUE.picture_storage_limit { MODELPARAM_VALUE.picture_storage_limit PARAM_VALUE.picture_storage_limit } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.picture_storage_limit}] ${MODELPARAM_VALUE.picture_storage_limit}
}

proc update_MODELPARAM_VALUE.razmpar { MODELPARAM_VALUE.razmpar PARAM_VALUE.razmpar } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.razmpar}] ${MODELPARAM_VALUE.razmpar}
}

proc update_MODELPARAM_VALUE.razmpar2 { MODELPARAM_VALUE.razmpar2 PARAM_VALUE.razmpar2 } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.razmpar2}] ${MODELPARAM_VALUE.razmpar2}
}

proc update_MODELPARAM_VALUE.picture_storage_limit_2 { MODELPARAM_VALUE.picture_storage_limit_2 PARAM_VALUE.picture_storage_limit_2 } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.picture_storage_limit_2}] ${MODELPARAM_VALUE.picture_storage_limit_2}
}

proc update_MODELPARAM_VALUE.convolution_size { MODELPARAM_VALUE.convolution_size PARAM_VALUE.convolution_size } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.convolution_size}] ${MODELPARAM_VALUE.convolution_size}
}

