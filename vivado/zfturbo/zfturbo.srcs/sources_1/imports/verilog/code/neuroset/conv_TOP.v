module conv_TOP(clk,conv_en,STOP,memstartp,memstartw,memstartzap,read_addressp,write_addressp,read_addresstp,write_addresstp,read_addressw,we,re_wb,re,we_t,re_t,qp,qtp,qw,dp,dtp,prov,matrix,matrix2,i_2,lvl,slvl,Y1,Y2,Y3,Y4,w15,w14,w16,w13,w17,w12,w18,w11,w19,w25,w24,w26,w23,w27,w22,w28,w21,w29,w35,w34,w36,w33,w37,w32,w38,w31,w39,w45,w44,w46,w43,w47,w42,w48,w41,w49,p1,p2,p3,p8,p7,p4,p5,p9,p6,go,num,filt,bias,globmaxp_en);

parameter num_conv=4;
parameter SIZE_1=0;
parameter SIZE_2=0;
parameter SIZE_3=0;
parameter SIZE_4=0;
parameter SIZE_5=0;
parameter SIZE_6=0;
parameter SIZE_7=0;
parameter SIZE_8=0;
parameter SIZE_9=0;
parameter SIZE_address_pix=0;
parameter SIZE_address_pix_t=0;
parameter SIZE_address_wei=0;

input clk,conv_en,globmaxp_en;
input [1:0] prov;
input [4:0] matrix;
input [9:0] matrix2;
input [SIZE_address_pix-1:0] memstartp; 
input [SIZE_address_wei-1:0] memstartw;
input [SIZE_address_pix-1:0] memstartzap;                  																	
input [4:0] lvl;
input [1:0] slvl;
output reg [SIZE_address_pix-1:0] read_addressp;
output reg [SIZE_address_pix_t-1:0] read_addresstp;
output reg [SIZE_address_wei-1:0] read_addressw;
output reg [SIZE_address_pix-1:0] write_addressp;
output reg [SIZE_address_pix_t-1:0] write_addresstp;
output reg we,re,re_wb;
output reg we_t,re_t;
input signed [SIZE_4-1:0] qp;
input signed [SIZE_2*4-1:0] qtp;
input signed [SIZE_9-1:0] qw;
output signed [SIZE_4-1:0] dp;
output signed [SIZE_2*4-1:0] dtp;
output reg STOP;
output [9:0] i_2;
input signed [SIZE_1+SIZE_1-2:0] Y1,Y2,Y3,Y4;
output reg signed [SIZE_1-1:0] w15,w14,w16,w13,w17,w12,w18,w11,w19,w25,w24,w26,w23,w27,w22,w28,w21,w29,w35,w34,w36,w33,w37,w32,w38,w31,w39,w45,w44,w46,w43,w47,w42,w48,w41,w49;
output reg signed [SIZE_1-1:0]p1,p2,p3,p4,p5,p6,p7,p8,p9;
output reg go;
input [2:0] num;
input [4:0] filt;
input bias;

reg signed [SIZE_1-1:0] res_out_1,res_out_2,res_out_3,res_out_4;
reg signed [SIZE_1+SIZE_1-2+1:0] res1,res2,res3,res4;
reg signed [SIZE_1+SIZE_1-2+1:0] res_old_1,res_old_2,res_old_3,res_old_4;
reg signed [SIZE_1-1:0] globmaxp_perem_1,globmaxp_perem_2,globmaxp_perem_3,globmaxp_perem_4;

reg signed [SIZE_1-1:0] buff0 [2:0];
reg signed [SIZE_1-1:0] buff1 [2:0];
reg signed [SIZE_1-1:0] buff2 [2:0];

reg [3:0] marker;
reg zagryzka_weight;
reg [9:0] i;

reg signed [SIZE_1-1+1:0] res_bias_check_1,res_bias_check_2,res_bias_check_3,res_bias_check_4;

initial zagryzka_weight=0;
initial marker=0;

always @(posedge clk)
begin
if (conv_en==1)        //enable convolution
	begin
		if (zagryzka_weight==0)        
		begin
		   case (marker)
				0: begin re_wb=1; read_addressw=memstartw+(2'd0*(filt+1)); end
				1: begin read_addressw=memstartw+(2'd1*(filt+1)); end
				2: begin read_addressw=memstartw+(2'd2*(filt+1));							w11=qw[SIZE_1-1:0]; 
							w12=qw[SIZE_2-1:SIZE_1]; 
							w13=qw[SIZE_3-1:SIZE_2]; 
							w14=qw[SIZE_4-1:SIZE_3]; 
							w15=qw[SIZE_5-1:SIZE_4]; 
							w16=qw[SIZE_6-1:SIZE_5]; 
							w17=qw[SIZE_7-1:SIZE_6]; 
							w18=qw[SIZE_8-1:SIZE_7]; 
							w19=qw[SIZE_9-1:SIZE_8]; 
 end
				3: begin read_addressw=memstartw+(2'd3*(filt+1));							w21=qw[SIZE_1-1:0]; 
							w22=qw[SIZE_2-1:SIZE_1]; 
							w23=qw[SIZE_3-1:SIZE_2]; 
							w24=qw[SIZE_4-1:SIZE_3]; 
							w25=qw[SIZE_5-1:SIZE_4]; 
							w26=qw[SIZE_6-1:SIZE_5]; 
							w27=qw[SIZE_7-1:SIZE_6]; 
							w28=qw[SIZE_8-1:SIZE_7]; 
							w29=qw[SIZE_9-1:SIZE_8]; 
 end
				4: begin							w31=qw[SIZE_1-1:0]; 
							w32=qw[SIZE_2-1:SIZE_1]; 
							w33=qw[SIZE_3-1:SIZE_2]; 
							w34=qw[SIZE_4-1:SIZE_3]; 
							w35=qw[SIZE_5-1:SIZE_4]; 
							w36=qw[SIZE_6-1:SIZE_5]; 
							w37=qw[SIZE_7-1:SIZE_6]; 
							w38=qw[SIZE_8-1:SIZE_7]; 
							w39=qw[SIZE_9-1:SIZE_8]; 
 end
				5: begin							w41=qw[SIZE_1-1:0]; 
							w42=qw[SIZE_2-1:SIZE_1]; 
							w43=qw[SIZE_3-1:SIZE_2]; 
							w44=qw[SIZE_4-1:SIZE_3]; 
							w45=qw[SIZE_5-1:SIZE_4]; 
							w46=qw[SIZE_6-1:SIZE_5]; 
							w47=qw[SIZE_7-1:SIZE_6]; 
							w48=qw[SIZE_8-1:SIZE_7]; 
							w49=qw[SIZE_9-1:SIZE_8]; 
 end
				6: begin zagryzka_weight=1; re_wb=0; marker=-1; end
				default: $display("Check zagryzka_weight");
		endcase
		marker=marker+1;
		end
		else
		begin
			re=1;
			case (marker)
				0: begin		
								re_t=0;
								read_addressp=i+memstartp; 
								if ((i-1)<matrix2-matrix) 
								begin
								if ({lvl[1],lvl[0]}==2'd0) buff2[2]=qp[SIZE_4-1:SIZE_3];
								else if ({lvl[1],lvl[0]}==2'd1) buff2[2]=qp[SIZE_3-1:SIZE_2];
								else if ({lvl[1],lvl[0]}==2'd2) buff2[2]=qp[SIZE_2-1:SIZE_1];
								else if ({lvl[1],lvl[0]}==2'd3) buff2[2]=qp[SIZE_1-1:0];
								end
								else buff2[2]=0;
								
								if (i>=2) go=1;
								
								
								p1=buff1[1];  //center
								p2=buff1[2];  //right
								p3=buff1[0];  //left
								p8=buff2[0];  //downright
								p7=buff0[2];  //up
								p4=buff2[1];  //downleft 
								p5=buff0[1];  //upright
								p9=buff2[2];  //upleft
								p6=buff0[0];  //down 
								
								
					end
				1: begin		if (i>=matrix-1) read_addressp=i-matrix+memstartp;
								res_old_1=qtp[(SIZE_2)*4-1:(SIZE_2)*3];
								res_old_2=qtp[(SIZE_2)*3-1:(SIZE_2)*2];
								res_old_3=qtp[(SIZE_2)*2-1:(SIZE_2)*1];
								res_old_4=qtp[(SIZE_2)*1-1:0];
								
								go=0;
								
								buff2[0]=buff2[1];
								buff1[0]=buff1[1];
								buff0[0]=buff0[1];
								buff2[1]=buff2[2];
								buff1[1]=buff1[2];
								buff0[1]=buff0[2];
					end
				2: begin    if (i<matrix2-matrix) read_addressp=i+matrix+memstartp;
								if ({lvl[1],lvl[0]}==2'd0) buff1[2]=qp[SIZE_4-1:SIZE_3];
								else if ({lvl[1],lvl[0]}==2'd1) buff1[2]=qp[SIZE_3-1:SIZE_2];
								else if ({lvl[1],lvl[0]}==2'd2) buff1[2]=qp[SIZE_2-1:SIZE_1];
								else if ({lvl[1],lvl[0]}==2'd3) buff1[2]=qp[SIZE_1-1:0];
								
								if (i>=2) 
								begin
								we_t=1;
								write_addresstp=i-2+matrix2*num+(slvl*((filt+1)*matrix2)>>(num_conv>>1));
								if (globmaxp_en)  write_addressp=memstartzap;
								else	write_addressp=memstartzap+i-2;
								res1=Y1; if (lvl!=0) res1=res1+res_old_1; 
								res2=Y2; if (lvl!=0) res2=res2+res_old_2; 
								res3=Y3; if (lvl!=0) res3=res3+res_old_3; 
								res4=Y4; if (lvl!=0) res4=res4+res_old_4; 
								if (bias==1) 
									begin  
										res_bias_check_1=res1[SIZE_1+SIZE_1-2+1:SIZE_1-1];
										if (res_bias_check_1>(2**(SIZE_1-1))-1) 
											begin
												$display("OVERFLOW in conv!");
												res_out_1=(2**(SIZE_1-1))-1;
											end
										else res_out_1=res1[SIZE_1+SIZE_1-2:SIZE_1-1];
										if (res_out_1<0) res_out_1=0; 
										res_bias_check_2=res2[SIZE_1+SIZE_1-2+1:SIZE_1-1];
										if (res_bias_check_2>(2**(SIZE_1-1))-1) 
											begin
												$display("OVERFLOW in conv!");
												res_out_2=(2**(SIZE_1-1))-1;
											end
										else res_out_2=res2[SIZE_1+SIZE_1-2:SIZE_1-1];
										if (res_out_2<0) res_out_2=0; 
										res_bias_check_3=res3[SIZE_1+SIZE_1-2+1:SIZE_1-1];
										if (res_bias_check_3>(2**(SIZE_1-1))-1) 
											begin
												$display("OVERFLOW in conv!");
												res_out_3=(2**(SIZE_1-1))-1;
											end
										else res_out_3=res3[SIZE_1+SIZE_1-2:SIZE_1-1];
										if (res_out_3<0) res_out_3=0; 
										res_bias_check_4=res4[SIZE_1+SIZE_1-2+1:SIZE_1-1];
										if (res_bias_check_4>(2**(SIZE_1-1))-1) 
											begin
												$display("OVERFLOW in conv!");
												res_out_4=(2**(SIZE_1-1))-1;
											end
										else res_out_4=res4[SIZE_1+SIZE_1-2:SIZE_1-1];
										if (res_out_4<0) res_out_4=0; 
										
										if (globmaxp_en)
											begin
												if (res_out_1>globmaxp_perem_1) globmaxp_perem_1=res_out_1;
												if (res_out_2>globmaxp_perem_2) globmaxp_perem_2=res_out_2;
												if (res_out_3>globmaxp_perem_3) globmaxp_perem_3=res_out_3;
												if (res_out_4>globmaxp_perem_4) globmaxp_perem_4=res_out_4;
										   end
										we=1;
									end
								end
					end
				3: begin		
								re_t=1;
								read_addresstp=i-1+matrix2*num+slvl*(((filt+1)*matrix2>>(num_conv>>1)));
								if (i>=matrix-1)
								begin
								if ({lvl[1],lvl[0]}==2'd0) buff0[2]=qp[SIZE_4-1:SIZE_3];
								else if ({lvl[1],lvl[0]}==2'd1) buff0[2]=qp[SIZE_3-1:SIZE_2];
								else if ({lvl[1],lvl[0]}==2'd2) buff0[2]=qp[SIZE_2-1:SIZE_1];
								else if ({lvl[1],lvl[0]}==2'd3) buff0[2]=qp[SIZE_1-1:0];
								end
								else buff0[2]=0;
								
								we_t=0;
								we=0;
					end						
			default: $display("Check case conv_TOP");
			endcase
			
			if (marker!=3) marker=marker+1; 
			else begin 
					marker=0; 
					if (i<matrix2+1) i=i+1; 
					else STOP=1; 
				  end
		end
	end
else 
	begin
		i=0;
		zagryzka_weight=0;
		STOP=0;
		re=0;
		re_t=0;
		go=0;
		marker=0;
		globmaxp_perem_1=0;
		globmaxp_perem_2=0;
		globmaxp_perem_3=0;
		globmaxp_perem_4=0;
	end
end
assign i_2=i-2;
assign dp=(globmaxp_en)?{globmaxp_perem_1,globmaxp_perem_2,globmaxp_perem_3,globmaxp_perem_4}:{res_out_1,res_out_2,res_out_3,res_out_4};
assign dtp={res1,res2,res3,res4};
endmodule
