-- Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2018.2 (lin64) Build 2258646 Thu Jun 14 20:02:38 MDT 2018
-- Date        : Fri Jan 24 14:10:49 2020
-- Host        : ubuntu-lucas running 64-bit Ubuntu 16.04.3 LTS
-- Command     : write_vhdl -force -mode synth_stub -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_TOP_0_1_stub.vhdl
-- Design      : design_1_TOP_0_1
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7z020clg400-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  Port ( 
    clk : in STD_LOGIC;
    GO : in STD_LOGIC;
    RESULT : out STD_LOGIC_VECTOR ( 3 downto 0 );
    we_database : in STD_LOGIC;
    dp_database : in STD_LOGIC_VECTOR ( 3 downto 0 );
    address_p_database : in STD_LOGIC_VECTOR ( 12 downto 0 );
    STOP : out STD_LOGIC
  );

end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture stub of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "clk,GO,RESULT[3:0],we_database,dp_database[3:0],address_p_database[12:0],STOP";
attribute X_CORE_INFO : string;
attribute X_CORE_INFO of stub : architecture is "TOP,Vivado 2018.2";
begin
end;
