library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
    
entity latency_counter is
    port (
        -- Inputs
        clk_i       : in std_logic;
        start_i     : in std_logic;
        end_i       : in std_logic;
        
        -- Output
        result_o    : out std_logic_vector(31 downto 0)
    );
end entity latency_counter;

architecture arch of latency_counter is

    -- Signals
    signal result_s : std_logic_vector(31 downto 0) := (others => '0');
    signal stop_s   : std_logic                     := '0';
    
begin
    
    process (clk_i, start_i, end_i)
    begin
        -- Asynchronous reset
        if (start_i = '1') then
            result_s <= (others => '0');
            stop_s   <= '0';
        -- Increment on clock rising edge
        elsif (end_i = '1') then
            stop_s   <= '1';
        elsif (stop_s = '0' and rising_edge(clk_i)) then
            result_s <= std_logic_vector(unsigned(result_s) + 1);
        end if;
    end process;
    result_o <= result_s;

end architecture arch;