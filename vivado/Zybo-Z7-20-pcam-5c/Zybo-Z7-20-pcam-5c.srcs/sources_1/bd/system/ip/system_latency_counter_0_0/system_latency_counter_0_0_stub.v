// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2018.2 (lin64) Build 2258646 Thu Jun 14 20:02:38 MDT 2018
// Date        : Fri Jan  3 13:13:56 2020
// Host        : ubuntu-lucas running 64-bit Ubuntu 16.04.3 LTS
// Command     : write_verilog -force -mode synth_stub
//               /home/faku/master-project/vivado/Zybo-Z7-20-pcam-5c/Zybo-Z7-20-pcam-5c.srcs/sources_1/bd/system/ip/system_latency_counter_0_0/system_latency_counter_0_0_stub.v
// Design      : system_latency_counter_0_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7z020clg400-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* x_core_info = "latency_counter,Vivado 2018.2" *)
module system_latency_counter_0_0(clk_i, start_i, end_i, result_o)
/* synthesis syn_black_box black_box_pad_pin="clk_i,start_i,end_i,result_o[31:0]" */;
  input clk_i;
  input start_i;
  input end_i;
  output [31:0]result_o;
endmodule
