// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2018.2 (lin64) Build 2258646 Thu Jun 14 20:02:38 MDT 2018
// Date        : Fri Jan  3 14:24:07 2020
// Host        : ubuntu-lucas running 64-bit Ubuntu 16.04.3 LTS
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ system_latency_counter_0_0_sim_netlist.v
// Design      : system_latency_counter_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z020clg400-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_latency_counter
   (result_o,
    end_i,
    start_i,
    clk_i);
  output [31:0]result_o;
  input end_i;
  input start_i;
  input clk_i;

  wire clk_i;
  wire end_i;
  wire [31:0]result_o;
  wire result_s0;
  wire \result_s[3]_i_2_n_0 ;
  wire \result_s_reg[11]_i_1_n_0 ;
  wire \result_s_reg[11]_i_1_n_1 ;
  wire \result_s_reg[11]_i_1_n_2 ;
  wire \result_s_reg[11]_i_1_n_3 ;
  wire \result_s_reg[11]_i_1_n_4 ;
  wire \result_s_reg[11]_i_1_n_5 ;
  wire \result_s_reg[11]_i_1_n_6 ;
  wire \result_s_reg[11]_i_1_n_7 ;
  wire \result_s_reg[15]_i_1_n_0 ;
  wire \result_s_reg[15]_i_1_n_1 ;
  wire \result_s_reg[15]_i_1_n_2 ;
  wire \result_s_reg[15]_i_1_n_3 ;
  wire \result_s_reg[15]_i_1_n_4 ;
  wire \result_s_reg[15]_i_1_n_5 ;
  wire \result_s_reg[15]_i_1_n_6 ;
  wire \result_s_reg[15]_i_1_n_7 ;
  wire \result_s_reg[19]_i_1_n_0 ;
  wire \result_s_reg[19]_i_1_n_1 ;
  wire \result_s_reg[19]_i_1_n_2 ;
  wire \result_s_reg[19]_i_1_n_3 ;
  wire \result_s_reg[19]_i_1_n_4 ;
  wire \result_s_reg[19]_i_1_n_5 ;
  wire \result_s_reg[19]_i_1_n_6 ;
  wire \result_s_reg[19]_i_1_n_7 ;
  wire \result_s_reg[23]_i_1_n_0 ;
  wire \result_s_reg[23]_i_1_n_1 ;
  wire \result_s_reg[23]_i_1_n_2 ;
  wire \result_s_reg[23]_i_1_n_3 ;
  wire \result_s_reg[23]_i_1_n_4 ;
  wire \result_s_reg[23]_i_1_n_5 ;
  wire \result_s_reg[23]_i_1_n_6 ;
  wire \result_s_reg[23]_i_1_n_7 ;
  wire \result_s_reg[27]_i_1_n_0 ;
  wire \result_s_reg[27]_i_1_n_1 ;
  wire \result_s_reg[27]_i_1_n_2 ;
  wire \result_s_reg[27]_i_1_n_3 ;
  wire \result_s_reg[27]_i_1_n_4 ;
  wire \result_s_reg[27]_i_1_n_5 ;
  wire \result_s_reg[27]_i_1_n_6 ;
  wire \result_s_reg[27]_i_1_n_7 ;
  wire \result_s_reg[31]_i_2_n_1 ;
  wire \result_s_reg[31]_i_2_n_2 ;
  wire \result_s_reg[31]_i_2_n_3 ;
  wire \result_s_reg[31]_i_2_n_4 ;
  wire \result_s_reg[31]_i_2_n_5 ;
  wire \result_s_reg[31]_i_2_n_6 ;
  wire \result_s_reg[31]_i_2_n_7 ;
  wire \result_s_reg[3]_i_1_n_0 ;
  wire \result_s_reg[3]_i_1_n_1 ;
  wire \result_s_reg[3]_i_1_n_2 ;
  wire \result_s_reg[3]_i_1_n_3 ;
  wire \result_s_reg[3]_i_1_n_4 ;
  wire \result_s_reg[3]_i_1_n_5 ;
  wire \result_s_reg[3]_i_1_n_6 ;
  wire \result_s_reg[3]_i_1_n_7 ;
  wire \result_s_reg[7]_i_1_n_0 ;
  wire \result_s_reg[7]_i_1_n_1 ;
  wire \result_s_reg[7]_i_1_n_2 ;
  wire \result_s_reg[7]_i_1_n_3 ;
  wire \result_s_reg[7]_i_1_n_4 ;
  wire \result_s_reg[7]_i_1_n_5 ;
  wire \result_s_reg[7]_i_1_n_6 ;
  wire \result_s_reg[7]_i_1_n_7 ;
  wire start_i;
  wire stop_s;
  wire [3:3]\NLW_result_s_reg[31]_i_2_CO_UNCONNECTED ;

  LUT2 #(
    .INIT(4'h1)) 
    \result_s[31]_i_1 
       (.I0(end_i),
        .I1(stop_s),
        .O(result_s0));
  LUT1 #(
    .INIT(2'h1)) 
    \result_s[3]_i_2 
       (.I0(result_o[0]),
        .O(\result_s[3]_i_2_n_0 ));
  FDCE #(
    .INIT(1'b0)) 
    \result_s_reg[0] 
       (.C(clk_i),
        .CE(result_s0),
        .CLR(start_i),
        .D(\result_s_reg[3]_i_1_n_7 ),
        .Q(result_o[0]));
  FDCE #(
    .INIT(1'b0)) 
    \result_s_reg[10] 
       (.C(clk_i),
        .CE(result_s0),
        .CLR(start_i),
        .D(\result_s_reg[11]_i_1_n_5 ),
        .Q(result_o[10]));
  FDCE #(
    .INIT(1'b0)) 
    \result_s_reg[11] 
       (.C(clk_i),
        .CE(result_s0),
        .CLR(start_i),
        .D(\result_s_reg[11]_i_1_n_4 ),
        .Q(result_o[11]));
  CARRY4 \result_s_reg[11]_i_1 
       (.CI(\result_s_reg[7]_i_1_n_0 ),
        .CO({\result_s_reg[11]_i_1_n_0 ,\result_s_reg[11]_i_1_n_1 ,\result_s_reg[11]_i_1_n_2 ,\result_s_reg[11]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\result_s_reg[11]_i_1_n_4 ,\result_s_reg[11]_i_1_n_5 ,\result_s_reg[11]_i_1_n_6 ,\result_s_reg[11]_i_1_n_7 }),
        .S(result_o[11:8]));
  FDCE #(
    .INIT(1'b0)) 
    \result_s_reg[12] 
       (.C(clk_i),
        .CE(result_s0),
        .CLR(start_i),
        .D(\result_s_reg[15]_i_1_n_7 ),
        .Q(result_o[12]));
  FDCE #(
    .INIT(1'b0)) 
    \result_s_reg[13] 
       (.C(clk_i),
        .CE(result_s0),
        .CLR(start_i),
        .D(\result_s_reg[15]_i_1_n_6 ),
        .Q(result_o[13]));
  FDCE #(
    .INIT(1'b0)) 
    \result_s_reg[14] 
       (.C(clk_i),
        .CE(result_s0),
        .CLR(start_i),
        .D(\result_s_reg[15]_i_1_n_5 ),
        .Q(result_o[14]));
  FDCE #(
    .INIT(1'b0)) 
    \result_s_reg[15] 
       (.C(clk_i),
        .CE(result_s0),
        .CLR(start_i),
        .D(\result_s_reg[15]_i_1_n_4 ),
        .Q(result_o[15]));
  CARRY4 \result_s_reg[15]_i_1 
       (.CI(\result_s_reg[11]_i_1_n_0 ),
        .CO({\result_s_reg[15]_i_1_n_0 ,\result_s_reg[15]_i_1_n_1 ,\result_s_reg[15]_i_1_n_2 ,\result_s_reg[15]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\result_s_reg[15]_i_1_n_4 ,\result_s_reg[15]_i_1_n_5 ,\result_s_reg[15]_i_1_n_6 ,\result_s_reg[15]_i_1_n_7 }),
        .S(result_o[15:12]));
  FDCE #(
    .INIT(1'b0)) 
    \result_s_reg[16] 
       (.C(clk_i),
        .CE(result_s0),
        .CLR(start_i),
        .D(\result_s_reg[19]_i_1_n_7 ),
        .Q(result_o[16]));
  FDCE #(
    .INIT(1'b0)) 
    \result_s_reg[17] 
       (.C(clk_i),
        .CE(result_s0),
        .CLR(start_i),
        .D(\result_s_reg[19]_i_1_n_6 ),
        .Q(result_o[17]));
  FDCE #(
    .INIT(1'b0)) 
    \result_s_reg[18] 
       (.C(clk_i),
        .CE(result_s0),
        .CLR(start_i),
        .D(\result_s_reg[19]_i_1_n_5 ),
        .Q(result_o[18]));
  FDCE #(
    .INIT(1'b0)) 
    \result_s_reg[19] 
       (.C(clk_i),
        .CE(result_s0),
        .CLR(start_i),
        .D(\result_s_reg[19]_i_1_n_4 ),
        .Q(result_o[19]));
  CARRY4 \result_s_reg[19]_i_1 
       (.CI(\result_s_reg[15]_i_1_n_0 ),
        .CO({\result_s_reg[19]_i_1_n_0 ,\result_s_reg[19]_i_1_n_1 ,\result_s_reg[19]_i_1_n_2 ,\result_s_reg[19]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\result_s_reg[19]_i_1_n_4 ,\result_s_reg[19]_i_1_n_5 ,\result_s_reg[19]_i_1_n_6 ,\result_s_reg[19]_i_1_n_7 }),
        .S(result_o[19:16]));
  FDCE #(
    .INIT(1'b0)) 
    \result_s_reg[1] 
       (.C(clk_i),
        .CE(result_s0),
        .CLR(start_i),
        .D(\result_s_reg[3]_i_1_n_6 ),
        .Q(result_o[1]));
  FDCE #(
    .INIT(1'b0)) 
    \result_s_reg[20] 
       (.C(clk_i),
        .CE(result_s0),
        .CLR(start_i),
        .D(\result_s_reg[23]_i_1_n_7 ),
        .Q(result_o[20]));
  FDCE #(
    .INIT(1'b0)) 
    \result_s_reg[21] 
       (.C(clk_i),
        .CE(result_s0),
        .CLR(start_i),
        .D(\result_s_reg[23]_i_1_n_6 ),
        .Q(result_o[21]));
  FDCE #(
    .INIT(1'b0)) 
    \result_s_reg[22] 
       (.C(clk_i),
        .CE(result_s0),
        .CLR(start_i),
        .D(\result_s_reg[23]_i_1_n_5 ),
        .Q(result_o[22]));
  FDCE #(
    .INIT(1'b0)) 
    \result_s_reg[23] 
       (.C(clk_i),
        .CE(result_s0),
        .CLR(start_i),
        .D(\result_s_reg[23]_i_1_n_4 ),
        .Q(result_o[23]));
  CARRY4 \result_s_reg[23]_i_1 
       (.CI(\result_s_reg[19]_i_1_n_0 ),
        .CO({\result_s_reg[23]_i_1_n_0 ,\result_s_reg[23]_i_1_n_1 ,\result_s_reg[23]_i_1_n_2 ,\result_s_reg[23]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\result_s_reg[23]_i_1_n_4 ,\result_s_reg[23]_i_1_n_5 ,\result_s_reg[23]_i_1_n_6 ,\result_s_reg[23]_i_1_n_7 }),
        .S(result_o[23:20]));
  FDCE #(
    .INIT(1'b0)) 
    \result_s_reg[24] 
       (.C(clk_i),
        .CE(result_s0),
        .CLR(start_i),
        .D(\result_s_reg[27]_i_1_n_7 ),
        .Q(result_o[24]));
  FDCE #(
    .INIT(1'b0)) 
    \result_s_reg[25] 
       (.C(clk_i),
        .CE(result_s0),
        .CLR(start_i),
        .D(\result_s_reg[27]_i_1_n_6 ),
        .Q(result_o[25]));
  FDCE #(
    .INIT(1'b0)) 
    \result_s_reg[26] 
       (.C(clk_i),
        .CE(result_s0),
        .CLR(start_i),
        .D(\result_s_reg[27]_i_1_n_5 ),
        .Q(result_o[26]));
  FDCE #(
    .INIT(1'b0)) 
    \result_s_reg[27] 
       (.C(clk_i),
        .CE(result_s0),
        .CLR(start_i),
        .D(\result_s_reg[27]_i_1_n_4 ),
        .Q(result_o[27]));
  CARRY4 \result_s_reg[27]_i_1 
       (.CI(\result_s_reg[23]_i_1_n_0 ),
        .CO({\result_s_reg[27]_i_1_n_0 ,\result_s_reg[27]_i_1_n_1 ,\result_s_reg[27]_i_1_n_2 ,\result_s_reg[27]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\result_s_reg[27]_i_1_n_4 ,\result_s_reg[27]_i_1_n_5 ,\result_s_reg[27]_i_1_n_6 ,\result_s_reg[27]_i_1_n_7 }),
        .S(result_o[27:24]));
  FDCE #(
    .INIT(1'b0)) 
    \result_s_reg[28] 
       (.C(clk_i),
        .CE(result_s0),
        .CLR(start_i),
        .D(\result_s_reg[31]_i_2_n_7 ),
        .Q(result_o[28]));
  FDCE #(
    .INIT(1'b0)) 
    \result_s_reg[29] 
       (.C(clk_i),
        .CE(result_s0),
        .CLR(start_i),
        .D(\result_s_reg[31]_i_2_n_6 ),
        .Q(result_o[29]));
  FDCE #(
    .INIT(1'b0)) 
    \result_s_reg[2] 
       (.C(clk_i),
        .CE(result_s0),
        .CLR(start_i),
        .D(\result_s_reg[3]_i_1_n_5 ),
        .Q(result_o[2]));
  FDCE #(
    .INIT(1'b0)) 
    \result_s_reg[30] 
       (.C(clk_i),
        .CE(result_s0),
        .CLR(start_i),
        .D(\result_s_reg[31]_i_2_n_5 ),
        .Q(result_o[30]));
  FDCE #(
    .INIT(1'b0)) 
    \result_s_reg[31] 
       (.C(clk_i),
        .CE(result_s0),
        .CLR(start_i),
        .D(\result_s_reg[31]_i_2_n_4 ),
        .Q(result_o[31]));
  CARRY4 \result_s_reg[31]_i_2 
       (.CI(\result_s_reg[27]_i_1_n_0 ),
        .CO({\NLW_result_s_reg[31]_i_2_CO_UNCONNECTED [3],\result_s_reg[31]_i_2_n_1 ,\result_s_reg[31]_i_2_n_2 ,\result_s_reg[31]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\result_s_reg[31]_i_2_n_4 ,\result_s_reg[31]_i_2_n_5 ,\result_s_reg[31]_i_2_n_6 ,\result_s_reg[31]_i_2_n_7 }),
        .S(result_o[31:28]));
  FDCE #(
    .INIT(1'b0)) 
    \result_s_reg[3] 
       (.C(clk_i),
        .CE(result_s0),
        .CLR(start_i),
        .D(\result_s_reg[3]_i_1_n_4 ),
        .Q(result_o[3]));
  CARRY4 \result_s_reg[3]_i_1 
       (.CI(1'b0),
        .CO({\result_s_reg[3]_i_1_n_0 ,\result_s_reg[3]_i_1_n_1 ,\result_s_reg[3]_i_1_n_2 ,\result_s_reg[3]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\result_s_reg[3]_i_1_n_4 ,\result_s_reg[3]_i_1_n_5 ,\result_s_reg[3]_i_1_n_6 ,\result_s_reg[3]_i_1_n_7 }),
        .S({result_o[3:1],\result_s[3]_i_2_n_0 }));
  FDCE #(
    .INIT(1'b0)) 
    \result_s_reg[4] 
       (.C(clk_i),
        .CE(result_s0),
        .CLR(start_i),
        .D(\result_s_reg[7]_i_1_n_7 ),
        .Q(result_o[4]));
  FDCE #(
    .INIT(1'b0)) 
    \result_s_reg[5] 
       (.C(clk_i),
        .CE(result_s0),
        .CLR(start_i),
        .D(\result_s_reg[7]_i_1_n_6 ),
        .Q(result_o[5]));
  FDCE #(
    .INIT(1'b0)) 
    \result_s_reg[6] 
       (.C(clk_i),
        .CE(result_s0),
        .CLR(start_i),
        .D(\result_s_reg[7]_i_1_n_5 ),
        .Q(result_o[6]));
  FDCE #(
    .INIT(1'b0)) 
    \result_s_reg[7] 
       (.C(clk_i),
        .CE(result_s0),
        .CLR(start_i),
        .D(\result_s_reg[7]_i_1_n_4 ),
        .Q(result_o[7]));
  CARRY4 \result_s_reg[7]_i_1 
       (.CI(\result_s_reg[3]_i_1_n_0 ),
        .CO({\result_s_reg[7]_i_1_n_0 ,\result_s_reg[7]_i_1_n_1 ,\result_s_reg[7]_i_1_n_2 ,\result_s_reg[7]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\result_s_reg[7]_i_1_n_4 ,\result_s_reg[7]_i_1_n_5 ,\result_s_reg[7]_i_1_n_6 ,\result_s_reg[7]_i_1_n_7 }),
        .S(result_o[7:4]));
  FDCE #(
    .INIT(1'b0)) 
    \result_s_reg[8] 
       (.C(clk_i),
        .CE(result_s0),
        .CLR(start_i),
        .D(\result_s_reg[11]_i_1_n_7 ),
        .Q(result_o[8]));
  FDCE #(
    .INIT(1'b0)) 
    \result_s_reg[9] 
       (.C(clk_i),
        .CE(result_s0),
        .CLR(start_i),
        .D(\result_s_reg[11]_i_1_n_6 ),
        .Q(result_o[9]));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    stop_s_reg
       (.CLR(start_i),
        .D(end_i),
        .G(end_i),
        .GE(1'b1),
        .Q(stop_s));
endmodule

(* CHECK_LICENSE_TYPE = "system_latency_counter_0_0,latency_counter,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* ip_definition_source = "module_ref" *) 
(* x_core_info = "latency_counter,Vivado 2018.2" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (clk_i,
    start_i,
    end_i,
    result_o);
  input clk_i;
  input start_i;
  input end_i;
  output [31:0]result_o;

  wire clk_i;
  wire end_i;
  wire [31:0]result_o;
  wire start_i;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_latency_counter U0
       (.clk_i(clk_i),
        .end_i(end_i),
        .result_o(result_o),
        .start_i(start_i));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
