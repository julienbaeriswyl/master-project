#include <xgray_rescale.h>
#include <xparameters.h>
#include <xstatus.h>

#ifndef __IP_GRAY_RESCALE_H__
#define __IP_GRAY_RESCALE_H__

static XGray_rescale instance;

XStatus gray_rescale_init(void) {
    XStatus status;

    status = XGray_rescale_Initialize(&instance, XPAR_GRAY_RESCALE_0_DEVICE_ID);
    if (status != XST_SUCCESS) {
        xil_printf("[gray_rescale] Initialization failed\n");

        return status;
    }

    XGray_rescale_Set_threshold_V(&instance, 127);

    return status;
}

void gray_rescale_set_threshold(u32 threshold) {
    XGray_rescale_Set_threshold_V(&instance, threshold);
}

u32 gray_rescale_get_threshold(void) {
    return XGray_rescale_Get_threshold_V(&instance);
}

#endif
