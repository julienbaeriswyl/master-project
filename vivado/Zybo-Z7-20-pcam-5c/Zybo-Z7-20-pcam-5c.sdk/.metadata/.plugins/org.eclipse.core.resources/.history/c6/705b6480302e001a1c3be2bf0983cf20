#include "xparameters.h"

#include "platform/platform.h"
#include "ov5640/OV5640.h"
#include "ov5640/ScuGicInterruptController.h"
#include "ov5640/PS_GPIO.h"
#include "ov5640/AXI_VDMA.h"
#include "ov5640/PS_IIC.h"

#include "MIPI_D_PHY_RX.h"
#include "MIPI_CSI_2_RX.h"

#include "ips/gray_rescale.h"

#define IRPT_CTL_DEVID      XPAR_PS7_SCUGIC_0_DEVICE_ID
#define GPIO_DEVID          XPAR_PS7_GPIO_0_DEVICE_ID
#define GPIO_IRPT_ID        XPAR_PS7_GPIO_0_INTR
#define CAM_I2C_DEVID       XPAR_PS7_I2C_0_DEVICE_ID
#define CAM_I2C_IRPT_ID     XPAR_PS7_I2C_0_INTR
#define VDMA_DEVID          XPAR_AXIVDMA_0_DEVICE_ID
#define VDMA_MM2S_IRPT_ID   XPAR_FABRIC_AXI_VDMA_0_MM2S_INTROUT_INTR
#define VDMA_S2MM_IRPT_ID   XPAR_FABRIC_AXI_VDMA_0_S2MM_INTROUT_INTR
#define CAM_I2C_SCLK_RATE   100000

#define DDR_BASE_ADDR       XPAR_DDR_MEM_BASEADDR
#define MEM_BASE_ADDR       (DDR_BASE_ADDR + 0x0A000000)

#define GAMMA_BASE_ADDR     XPAR_AXI_GAMMACORRECTION_0_BASEADDR

using namespace digilent;

void pipeline_mode_change(AXI_VDMA<ScuGicInterruptController>& vdma_driver, OV5640& cam, VideoOutput& vid, Resolution res, OV5640_cfg::mode_t mode)
{
    //Bring up input pipeline back-to-front
    {
        vdma_driver.resetWrite();
        MIPI_CSI_2_RX_mWriteReg(XPAR_MIPI_CSI_2_RX_0_S_AXI_LITE_BASEADDR, CR_OFFSET, (CR_RESET_MASK & ~CR_ENABLE_MASK));
        MIPI_D_PHY_RX_mWriteReg(XPAR_MIPI_D_PHY_RX_0_S_AXI_LITE_BASEADDR, CR_OFFSET, (CR_RESET_MASK & ~CR_ENABLE_MASK));
        cam.reset();
    }

    {
        vdma_driver.configureWrite(timing[static_cast<int>(res)].h_active, timing[static_cast<int>(res)].v_active);
        Xil_Out32(GAMMA_BASE_ADDR, 3); // Set Gamma correction factor to 1/1.8
        //TODO CSI-2, D-PHY config here
        cam.init();
    }

    {
        vdma_driver.enableWrite();
        MIPI_CSI_2_RX_mWriteReg(XPAR_MIPI_CSI_2_RX_0_S_AXI_LITE_BASEADDR, CR_OFFSET, CR_ENABLE_MASK);
        MIPI_D_PHY_RX_mWriteReg(XPAR_MIPI_D_PHY_RX_0_S_AXI_LITE_BASEADDR, CR_OFFSET, CR_ENABLE_MASK);
        cam.set_mode(mode);
        cam.set_awb(OV5640_cfg::awb_t::AWB_ADVANCED);
    }

    //Bring up output pipeline back-to-front
    {
        vid.reset();
        vdma_driver.resetRead();
    }

    {
        vid.configure(res);
        vdma_driver.configureRead(timing[static_cast<int>(res)].h_active, timing[static_cast<int>(res)].v_active);
    }

    {
        vid.enable();
        vdma_driver.enableRead();
    }
}

int main()
{
    init_platform();

    ScuGicInterruptController irpt_ctl(IRPT_CTL_DEVID);
    PS_GPIO<ScuGicInterruptController> gpio_driver(GPIO_DEVID, irpt_ctl, GPIO_IRPT_ID);
    PS_IIC<ScuGicInterruptController> iic_driver(CAM_I2C_DEVID, irpt_ctl, CAM_I2C_IRPT_ID, 100000);

    OV5640 cam(iic_driver, gpio_driver);
    AXI_VDMA<ScuGicInterruptController> vdma_driver(VDMA_DEVID, MEM_BASE_ADDR, irpt_ctl,
            VDMA_MM2S_IRPT_ID,
            VDMA_S2MM_IRPT_ID);
    VideoOutput vid(XPAR_VTC_0_DEVICE_ID, XPAR_VIDEO_DYNCLK_DEVICE_ID);

    gray_rescale_init();

    pipeline_mode_change(vdma_driver, cam, vid, Resolution::R1920_1080_60_PP, OV5640_cfg::mode_t::MODE_1080P_1920_1080_30fps);


    xil_printf("Video init done.\r\n");

    u8  read_char0 = 0;
    u8  read_char1 = 0;
    u8  read_char2 = 0;
    u8  read_char3 = 0;
    u32 threshold  = 0;

    while (1) {
        xil_printf("\n\n");
        xil_printf("=== PCAM 5C MAIN OPTIONS === \n");
        xil_printf("Please press the key corresponding to the desired option:\n");
        xil_printf("    a. Change threshold\n");

        read_char0 = getchar();
        getchar();

        switch(read_char0) {

        case 'a':
            xil_printf("Current threshold value: %u\n", gray_rescale_get_threshold());
            xil_printf("Enter new threshold value [000-255]: ");

            read_char1 = getchar();
            read_char2 = getchar();
            read_char3 = getchar();
            getchar();

            threshold = ((read_char1 - 48) * 100) + ((read_char2 - 48) * 10) + (read_char3 - 48);
            gray_rescale_set_threshold(threshold);
            break;

        case 'b':
            xil_printf("Timer value: 0x%08X\n", Xil_In32(0x41200000));

        default:
            xil_printf("Unknown key: %c\n", read_char0);
            break;
        }

        read_char0 = 0;
        read_char1 = 0;
        read_char2 = 0;
        read_char3 = 0;
    }

    cleanup_platform();

    return 0;
}
