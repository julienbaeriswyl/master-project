connect -url tcp:127.0.0.1:3121
source /home/faku/master-project/vivado/Zybo-Z7-20-pcam-5c/Zybo-Z7-20-pcam-5c.sdk/system_wrapper_hw_platform_0/ps7_init.tcl
targets -set -nocase -filter {name =~"APU*" && jtag_cable_name =~ "Digilent Zybo Z7 210351A81FB6A"} -index 0
rst -system
after 3000
targets -set -filter {jtag_cable_name =~ "Digilent Zybo Z7 210351A81FB6A" && level==0} -index 1
fpga -file /home/faku/master-project/vivado/Zybo-Z7-20-pcam-5c/Zybo-Z7-20-pcam-5c.sdk/system_wrapper_hw_platform_0/system_wrapper.bit
targets -set -nocase -filter {name =~"APU*" && jtag_cable_name =~ "Digilent Zybo Z7 210351A81FB6A"} -index 0
loadhw -hw /home/faku/master-project/vivado/Zybo-Z7-20-pcam-5c/Zybo-Z7-20-pcam-5c.sdk/system_wrapper_hw_platform_0/system.hdf -mem-ranges [list {0x40000000 0xbfffffff}]
configparams force-mem-access 1
targets -set -nocase -filter {name =~"APU*" && jtag_cable_name =~ "Digilent Zybo Z7 210351A81FB6A"} -index 0
ps7_init
ps7_post_config
targets -set -nocase -filter {name =~ "ARM*#0" && jtag_cable_name =~ "Digilent Zybo Z7 210351A81FB6A"} -index 0
dow /home/faku/master-project/vivado/Zybo-Z7-20-pcam-5c/Zybo-Z7-20-pcam-5c.sdk/z7-20_pcam/Debug/z7-20_pcam.elf
configparams force-mem-access 0
targets -set -nocase -filter {name =~ "ARM*#0" && jtag_cable_name =~ "Digilent Zybo Z7 210351A81FB6A"} -index 0
con
