// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2018.2 (lin64) Build 2258646 Thu Jun 14 20:02:38 MDT 2018
// Date        : Tue Dec  3 15:16:44 2019
// Host        : A23B-PC10 running 64-bit Ubuntu 16.04.3 LTS
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ system_center_image_0_0_sim_netlist.v
// Design      : system_center_image_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z020clg400-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_AXIvideo2Mat
   (ap_rst,
    image_in_TREADY,
    start_once_reg,
    ap_ready,
    Q,
    \mOutPtr_reg[1] ,
    AXIvideo2Mat_U0_img_data_stream_2_V_write,
    internal_empty_n_reg,
    internal_empty_n_reg_0,
    internal_empty_n_reg_1,
    D,
    \SRL_SIG_reg[0][7] ,
    \SRL_SIG_reg[0][7]_0 ,
    ap_clk,
    ap_rst_n,
    start_for_Loop_loop_height_pro_U0_full_n,
    ap_start,
    input_data_data_stre_2_full_n,
    input_data_data_stre_1_full_n,
    input_data_data_stre_full_n,
    image_in_TVALID,
    image_in_TLAST,
    image_in_TUSER,
    image_in_TDATA);
  output ap_rst;
  output image_in_TREADY;
  output start_once_reg;
  output ap_ready;
  output [0:0]Q;
  output \mOutPtr_reg[1] ;
  output AXIvideo2Mat_U0_img_data_stream_2_V_write;
  output internal_empty_n_reg;
  output internal_empty_n_reg_0;
  output internal_empty_n_reg_1;
  output [7:0]D;
  output [7:0]\SRL_SIG_reg[0][7] ;
  output [7:0]\SRL_SIG_reg[0][7]_0 ;
  input ap_clk;
  input ap_rst_n;
  input start_for_Loop_loop_height_pro_U0_full_n;
  input ap_start;
  input input_data_data_stre_2_full_n;
  input input_data_data_stre_1_full_n;
  input input_data_data_stre_full_n;
  input image_in_TVALID;
  input [0:0]image_in_TLAST;
  input [0:0]image_in_TUSER;
  input [23:0]image_in_TDATA;

  wire AXI_video_strm_V_data_V_0_ack_in;
  wire AXI_video_strm_V_data_V_0_ack_out;
  wire [23:0]AXI_video_strm_V_data_V_0_data_out;
  wire AXI_video_strm_V_data_V_0_load_A;
  wire AXI_video_strm_V_data_V_0_load_B;
  wire [23:0]AXI_video_strm_V_data_V_0_payload_A;
  wire [23:0]AXI_video_strm_V_data_V_0_payload_B;
  wire AXI_video_strm_V_data_V_0_sel;
  wire AXI_video_strm_V_data_V_0_sel2;
  wire AXI_video_strm_V_data_V_0_sel3__0;
  wire AXI_video_strm_V_data_V_0_sel_rd_i_1_n_0;
  wire AXI_video_strm_V_data_V_0_sel_wr;
  wire AXI_video_strm_V_data_V_0_sel_wr_i_1_n_0;
  wire [1:1]AXI_video_strm_V_data_V_0_state;
  wire \AXI_video_strm_V_data_V_0_state[0]_i_1_n_0 ;
  wire \AXI_video_strm_V_data_V_0_state_reg_n_0_[0] ;
  wire [1:1]AXI_video_strm_V_dest_V_0_state;
  wire \AXI_video_strm_V_dest_V_0_state[0]_i_1_n_0 ;
  wire \AXI_video_strm_V_dest_V_0_state_reg_n_0_[0] ;
  wire AXI_video_strm_V_last_V_0_ack_in;
  wire AXI_video_strm_V_last_V_0_data_out;
  wire AXI_video_strm_V_last_V_0_payload_A;
  wire \AXI_video_strm_V_last_V_0_payload_A[0]_i_1_n_0 ;
  wire AXI_video_strm_V_last_V_0_payload_B;
  wire \AXI_video_strm_V_last_V_0_payload_B[0]_i_1_n_0 ;
  wire AXI_video_strm_V_last_V_0_sel;
  wire AXI_video_strm_V_last_V_0_sel_rd_i_1_n_0;
  wire AXI_video_strm_V_last_V_0_sel_wr;
  wire AXI_video_strm_V_last_V_0_sel_wr_i_1_n_0;
  wire [1:1]AXI_video_strm_V_last_V_0_state;
  wire \AXI_video_strm_V_last_V_0_state[0]_i_1_n_0 ;
  wire \AXI_video_strm_V_last_V_0_state_reg_n_0_[0] ;
  wire AXI_video_strm_V_user_V_0_ack_in;
  wire AXI_video_strm_V_user_V_0_payload_A;
  wire \AXI_video_strm_V_user_V_0_payload_A[0]_i_1_n_0 ;
  wire AXI_video_strm_V_user_V_0_payload_B;
  wire \AXI_video_strm_V_user_V_0_payload_B[0]_i_1_n_0 ;
  wire AXI_video_strm_V_user_V_0_sel;
  wire AXI_video_strm_V_user_V_0_sel_rd_i_1_n_0;
  wire AXI_video_strm_V_user_V_0_sel_wr;
  wire AXI_video_strm_V_user_V_0_sel_wr_i_1_n_0;
  wire [1:1]AXI_video_strm_V_user_V_0_state;
  wire \AXI_video_strm_V_user_V_0_state[0]_i_1_n_0 ;
  wire \AXI_video_strm_V_user_V_0_state_reg_n_0_[0] ;
  wire AXIvideo2Mat_U0_img_data_stream_2_V_write;
  wire [7:0]D;
  wire [0:0]Q;
  wire [7:0]\SRL_SIG_reg[0][7] ;
  wire [7:0]\SRL_SIG_reg[0][7]_0 ;
  wire \ap_CS_fsm[1]_i_2_n_0 ;
  wire ap_CS_fsm_pp1_stage0;
  wire ap_CS_fsm_pp2_stage0;
  wire ap_CS_fsm_state10;
  wire ap_CS_fsm_state2;
  wire ap_CS_fsm_state3;
  wire ap_CS_fsm_state4;
  wire ap_CS_fsm_state7;
  wire [7:0]ap_NS_fsm;
  wire ap_block_pp1_stage0_110011;
  wire ap_clk;
  wire ap_enable_reg_pp1_iter0;
  wire ap_enable_reg_pp1_iter0_i_1_n_0;
  wire ap_enable_reg_pp1_iter1_i_1_n_0;
  wire ap_enable_reg_pp1_iter1_reg_n_0;
  wire ap_enable_reg_pp2_iter0;
  wire ap_enable_reg_pp2_iter0_i_1_n_0;
  wire ap_enable_reg_pp2_iter0_i_2_n_0;
  wire ap_enable_reg_pp2_iter1_i_1_n_0;
  wire ap_enable_reg_pp2_iter1_reg_n_0;
  wire ap_phi_mux_axi_last_V_2_phi_fu_248_p4__0;
  wire ap_ready;
  wire ap_ready_INST_0_i_2_n_0;
  wire ap_ready_INST_0_i_3_n_0;
  wire ap_rst;
  wire ap_rst_n;
  wire ap_start;
  wire [23:0]axi_data_V1_reg_177;
  wire \axi_data_V1_reg_177[0]_i_1_n_0 ;
  wire \axi_data_V1_reg_177[10]_i_1_n_0 ;
  wire \axi_data_V1_reg_177[11]_i_1_n_0 ;
  wire \axi_data_V1_reg_177[12]_i_1_n_0 ;
  wire \axi_data_V1_reg_177[13]_i_1_n_0 ;
  wire \axi_data_V1_reg_177[14]_i_1_n_0 ;
  wire \axi_data_V1_reg_177[15]_i_1_n_0 ;
  wire \axi_data_V1_reg_177[16]_i_1_n_0 ;
  wire \axi_data_V1_reg_177[17]_i_1_n_0 ;
  wire \axi_data_V1_reg_177[18]_i_1_n_0 ;
  wire \axi_data_V1_reg_177[19]_i_1_n_0 ;
  wire \axi_data_V1_reg_177[1]_i_1_n_0 ;
  wire \axi_data_V1_reg_177[20]_i_1_n_0 ;
  wire \axi_data_V1_reg_177[21]_i_1_n_0 ;
  wire \axi_data_V1_reg_177[22]_i_1_n_0 ;
  wire \axi_data_V1_reg_177[23]_i_1_n_0 ;
  wire \axi_data_V1_reg_177[2]_i_1_n_0 ;
  wire \axi_data_V1_reg_177[3]_i_1_n_0 ;
  wire \axi_data_V1_reg_177[4]_i_1_n_0 ;
  wire \axi_data_V1_reg_177[5]_i_1_n_0 ;
  wire \axi_data_V1_reg_177[6]_i_1_n_0 ;
  wire \axi_data_V1_reg_177[7]_i_1_n_0 ;
  wire \axi_data_V1_reg_177[8]_i_1_n_0 ;
  wire \axi_data_V1_reg_177[9]_i_1_n_0 ;
  wire [23:0]axi_data_V_1_reg_232;
  wire \axi_data_V_1_reg_232[0]_i_1_n_0 ;
  wire \axi_data_V_1_reg_232[10]_i_1_n_0 ;
  wire \axi_data_V_1_reg_232[11]_i_1_n_0 ;
  wire \axi_data_V_1_reg_232[12]_i_1_n_0 ;
  wire \axi_data_V_1_reg_232[13]_i_1_n_0 ;
  wire \axi_data_V_1_reg_232[14]_i_1_n_0 ;
  wire \axi_data_V_1_reg_232[15]_i_1_n_0 ;
  wire \axi_data_V_1_reg_232[16]_i_1_n_0 ;
  wire \axi_data_V_1_reg_232[17]_i_1_n_0 ;
  wire \axi_data_V_1_reg_232[18]_i_1_n_0 ;
  wire \axi_data_V_1_reg_232[19]_i_1_n_0 ;
  wire \axi_data_V_1_reg_232[1]_i_1_n_0 ;
  wire \axi_data_V_1_reg_232[20]_i_1_n_0 ;
  wire \axi_data_V_1_reg_232[21]_i_1_n_0 ;
  wire \axi_data_V_1_reg_232[22]_i_1_n_0 ;
  wire \axi_data_V_1_reg_232[23]_i_1_n_0 ;
  wire \axi_data_V_1_reg_232[2]_i_1_n_0 ;
  wire \axi_data_V_1_reg_232[3]_i_1_n_0 ;
  wire \axi_data_V_1_reg_232[4]_i_1_n_0 ;
  wire \axi_data_V_1_reg_232[5]_i_1_n_0 ;
  wire \axi_data_V_1_reg_232[6]_i_1_n_0 ;
  wire \axi_data_V_1_reg_232[7]_i_1_n_0 ;
  wire \axi_data_V_1_reg_232[8]_i_1_n_0 ;
  wire \axi_data_V_1_reg_232[9]_i_1_n_0 ;
  wire [23:0]axi_data_V_3_reg_291;
  wire \axi_data_V_3_reg_291[0]_i_1_n_0 ;
  wire \axi_data_V_3_reg_291[10]_i_1_n_0 ;
  wire \axi_data_V_3_reg_291[11]_i_1_n_0 ;
  wire \axi_data_V_3_reg_291[12]_i_1_n_0 ;
  wire \axi_data_V_3_reg_291[13]_i_1_n_0 ;
  wire \axi_data_V_3_reg_291[14]_i_1_n_0 ;
  wire \axi_data_V_3_reg_291[15]_i_1_n_0 ;
  wire \axi_data_V_3_reg_291[16]_i_1_n_0 ;
  wire \axi_data_V_3_reg_291[17]_i_1_n_0 ;
  wire \axi_data_V_3_reg_291[18]_i_1_n_0 ;
  wire \axi_data_V_3_reg_291[19]_i_1_n_0 ;
  wire \axi_data_V_3_reg_291[1]_i_1_n_0 ;
  wire \axi_data_V_3_reg_291[20]_i_1_n_0 ;
  wire \axi_data_V_3_reg_291[21]_i_1_n_0 ;
  wire \axi_data_V_3_reg_291[22]_i_1_n_0 ;
  wire \axi_data_V_3_reg_291[23]_i_1_n_0 ;
  wire \axi_data_V_3_reg_291[2]_i_1_n_0 ;
  wire \axi_data_V_3_reg_291[3]_i_1_n_0 ;
  wire \axi_data_V_3_reg_291[4]_i_1_n_0 ;
  wire \axi_data_V_3_reg_291[5]_i_1_n_0 ;
  wire \axi_data_V_3_reg_291[6]_i_1_n_0 ;
  wire \axi_data_V_3_reg_291[7]_i_1_n_0 ;
  wire \axi_data_V_3_reg_291[8]_i_1_n_0 ;
  wire \axi_data_V_3_reg_291[9]_i_1_n_0 ;
  wire axi_last_V1_reg_167;
  wire \axi_last_V1_reg_167[0]_i_1_n_0 ;
  wire axi_last_V_3_reg_279;
  wire \axi_last_V_3_reg_279[0]_i_1_n_0 ;
  wire brmerge_fu_349_p2;
  wire brmerge_reg_425;
  wire \brmerge_reg_425[0]_i_1_n_0 ;
  wire eol_1_reg_221;
  wire \eol_1_reg_221[0]_i_2_n_0 ;
  wire \eol_2_reg_268[0]_i_1_n_0 ;
  wire \eol_2_reg_268[0]_i_2_n_0 ;
  wire \eol_2_reg_268_reg_n_0_[0] ;
  wire eol_reg_209;
  wire \eol_reg_209[0]_i_1_n_0 ;
  wire \eol_reg_209_reg_n_0_[0] ;
  wire exitcond3_fu_322_p2__14;
  wire exitcond_fu_334_p2;
  wire exitcond_reg_4160;
  wire \exitcond_reg_416[0]_i_1_n_0 ;
  wire \exitcond_reg_416_reg_n_0_[0] ;
  wire [10:0]i_V_fu_328_p2;
  wire [10:0]i_V_reg_411;
  wire \i_V_reg_411[10]_i_2_n_0 ;
  wire [23:0]image_in_TDATA;
  wire [0:0]image_in_TLAST;
  wire image_in_TREADY;
  wire [0:0]image_in_TUSER;
  wire image_in_TVALID;
  wire input_data_data_stre_1_full_n;
  wire input_data_data_stre_2_full_n;
  wire input_data_data_stre_full_n;
  wire internal_empty_n_reg;
  wire internal_empty_n_reg_0;
  wire internal_empty_n_reg_1;
  wire [10:0]j_V_fu_340_p2;
  wire \mOutPtr_reg[1] ;
  wire sof_1_fu_124;
  wire sof_1_fu_1240;
  wire \sof_1_fu_124[0]_i_1_n_0 ;
  wire start_for_Loop_loop_height_pro_U0_full_n;
  wire start_once_reg;
  wire start_once_reg_i_1__0_n_0;
  wire t_V_2_reg_198;
  wire \t_V_2_reg_198[10]_i_5_n_0 ;
  wire \t_V_2_reg_198[10]_i_6_n_0 ;
  wire \t_V_2_reg_198[10]_i_7_n_0 ;
  wire [10:0]t_V_2_reg_198_reg__0;
  wire [10:0]t_V_reg_187;
  wire [23:0]tmp_data_V_reg_387;
  wire tmp_last_V_reg_395;

  LUT3 #(
    .INIT(8'h0D)) 
    \AXI_video_strm_V_data_V_0_payload_A[23]_i_1 
       (.I0(\AXI_video_strm_V_data_V_0_state_reg_n_0_[0] ),
        .I1(AXI_video_strm_V_data_V_0_ack_in),
        .I2(AXI_video_strm_V_data_V_0_sel_wr),
        .O(AXI_video_strm_V_data_V_0_load_A));
  FDRE \AXI_video_strm_V_data_V_0_payload_A_reg[0] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_load_A),
        .D(image_in_TDATA[0]),
        .Q(AXI_video_strm_V_data_V_0_payload_A[0]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_0_payload_A_reg[10] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_load_A),
        .D(image_in_TDATA[10]),
        .Q(AXI_video_strm_V_data_V_0_payload_A[10]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_0_payload_A_reg[11] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_load_A),
        .D(image_in_TDATA[11]),
        .Q(AXI_video_strm_V_data_V_0_payload_A[11]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_0_payload_A_reg[12] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_load_A),
        .D(image_in_TDATA[12]),
        .Q(AXI_video_strm_V_data_V_0_payload_A[12]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_0_payload_A_reg[13] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_load_A),
        .D(image_in_TDATA[13]),
        .Q(AXI_video_strm_V_data_V_0_payload_A[13]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_0_payload_A_reg[14] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_load_A),
        .D(image_in_TDATA[14]),
        .Q(AXI_video_strm_V_data_V_0_payload_A[14]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_0_payload_A_reg[15] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_load_A),
        .D(image_in_TDATA[15]),
        .Q(AXI_video_strm_V_data_V_0_payload_A[15]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_0_payload_A_reg[16] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_load_A),
        .D(image_in_TDATA[16]),
        .Q(AXI_video_strm_V_data_V_0_payload_A[16]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_0_payload_A_reg[17] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_load_A),
        .D(image_in_TDATA[17]),
        .Q(AXI_video_strm_V_data_V_0_payload_A[17]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_0_payload_A_reg[18] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_load_A),
        .D(image_in_TDATA[18]),
        .Q(AXI_video_strm_V_data_V_0_payload_A[18]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_0_payload_A_reg[19] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_load_A),
        .D(image_in_TDATA[19]),
        .Q(AXI_video_strm_V_data_V_0_payload_A[19]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_0_payload_A_reg[1] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_load_A),
        .D(image_in_TDATA[1]),
        .Q(AXI_video_strm_V_data_V_0_payload_A[1]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_0_payload_A_reg[20] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_load_A),
        .D(image_in_TDATA[20]),
        .Q(AXI_video_strm_V_data_V_0_payload_A[20]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_0_payload_A_reg[21] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_load_A),
        .D(image_in_TDATA[21]),
        .Q(AXI_video_strm_V_data_V_0_payload_A[21]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_0_payload_A_reg[22] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_load_A),
        .D(image_in_TDATA[22]),
        .Q(AXI_video_strm_V_data_V_0_payload_A[22]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_0_payload_A_reg[23] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_load_A),
        .D(image_in_TDATA[23]),
        .Q(AXI_video_strm_V_data_V_0_payload_A[23]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_0_payload_A_reg[2] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_load_A),
        .D(image_in_TDATA[2]),
        .Q(AXI_video_strm_V_data_V_0_payload_A[2]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_0_payload_A_reg[3] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_load_A),
        .D(image_in_TDATA[3]),
        .Q(AXI_video_strm_V_data_V_0_payload_A[3]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_0_payload_A_reg[4] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_load_A),
        .D(image_in_TDATA[4]),
        .Q(AXI_video_strm_V_data_V_0_payload_A[4]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_0_payload_A_reg[5] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_load_A),
        .D(image_in_TDATA[5]),
        .Q(AXI_video_strm_V_data_V_0_payload_A[5]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_0_payload_A_reg[6] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_load_A),
        .D(image_in_TDATA[6]),
        .Q(AXI_video_strm_V_data_V_0_payload_A[6]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_0_payload_A_reg[7] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_load_A),
        .D(image_in_TDATA[7]),
        .Q(AXI_video_strm_V_data_V_0_payload_A[7]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_0_payload_A_reg[8] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_load_A),
        .D(image_in_TDATA[8]),
        .Q(AXI_video_strm_V_data_V_0_payload_A[8]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_0_payload_A_reg[9] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_load_A),
        .D(image_in_TDATA[9]),
        .Q(AXI_video_strm_V_data_V_0_payload_A[9]),
        .R(1'b0));
  LUT3 #(
    .INIT(8'hA2)) 
    \AXI_video_strm_V_data_V_0_payload_B[23]_i_1 
       (.I0(AXI_video_strm_V_data_V_0_sel_wr),
        .I1(\AXI_video_strm_V_data_V_0_state_reg_n_0_[0] ),
        .I2(AXI_video_strm_V_data_V_0_ack_in),
        .O(AXI_video_strm_V_data_V_0_load_B));
  FDRE \AXI_video_strm_V_data_V_0_payload_B_reg[0] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_load_B),
        .D(image_in_TDATA[0]),
        .Q(AXI_video_strm_V_data_V_0_payload_B[0]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_0_payload_B_reg[10] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_load_B),
        .D(image_in_TDATA[10]),
        .Q(AXI_video_strm_V_data_V_0_payload_B[10]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_0_payload_B_reg[11] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_load_B),
        .D(image_in_TDATA[11]),
        .Q(AXI_video_strm_V_data_V_0_payload_B[11]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_0_payload_B_reg[12] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_load_B),
        .D(image_in_TDATA[12]),
        .Q(AXI_video_strm_V_data_V_0_payload_B[12]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_0_payload_B_reg[13] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_load_B),
        .D(image_in_TDATA[13]),
        .Q(AXI_video_strm_V_data_V_0_payload_B[13]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_0_payload_B_reg[14] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_load_B),
        .D(image_in_TDATA[14]),
        .Q(AXI_video_strm_V_data_V_0_payload_B[14]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_0_payload_B_reg[15] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_load_B),
        .D(image_in_TDATA[15]),
        .Q(AXI_video_strm_V_data_V_0_payload_B[15]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_0_payload_B_reg[16] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_load_B),
        .D(image_in_TDATA[16]),
        .Q(AXI_video_strm_V_data_V_0_payload_B[16]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_0_payload_B_reg[17] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_load_B),
        .D(image_in_TDATA[17]),
        .Q(AXI_video_strm_V_data_V_0_payload_B[17]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_0_payload_B_reg[18] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_load_B),
        .D(image_in_TDATA[18]),
        .Q(AXI_video_strm_V_data_V_0_payload_B[18]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_0_payload_B_reg[19] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_load_B),
        .D(image_in_TDATA[19]),
        .Q(AXI_video_strm_V_data_V_0_payload_B[19]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_0_payload_B_reg[1] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_load_B),
        .D(image_in_TDATA[1]),
        .Q(AXI_video_strm_V_data_V_0_payload_B[1]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_0_payload_B_reg[20] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_load_B),
        .D(image_in_TDATA[20]),
        .Q(AXI_video_strm_V_data_V_0_payload_B[20]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_0_payload_B_reg[21] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_load_B),
        .D(image_in_TDATA[21]),
        .Q(AXI_video_strm_V_data_V_0_payload_B[21]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_0_payload_B_reg[22] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_load_B),
        .D(image_in_TDATA[22]),
        .Q(AXI_video_strm_V_data_V_0_payload_B[22]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_0_payload_B_reg[23] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_load_B),
        .D(image_in_TDATA[23]),
        .Q(AXI_video_strm_V_data_V_0_payload_B[23]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_0_payload_B_reg[2] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_load_B),
        .D(image_in_TDATA[2]),
        .Q(AXI_video_strm_V_data_V_0_payload_B[2]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_0_payload_B_reg[3] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_load_B),
        .D(image_in_TDATA[3]),
        .Q(AXI_video_strm_V_data_V_0_payload_B[3]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_0_payload_B_reg[4] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_load_B),
        .D(image_in_TDATA[4]),
        .Q(AXI_video_strm_V_data_V_0_payload_B[4]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_0_payload_B_reg[5] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_load_B),
        .D(image_in_TDATA[5]),
        .Q(AXI_video_strm_V_data_V_0_payload_B[5]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_0_payload_B_reg[6] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_load_B),
        .D(image_in_TDATA[6]),
        .Q(AXI_video_strm_V_data_V_0_payload_B[6]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_0_payload_B_reg[7] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_load_B),
        .D(image_in_TDATA[7]),
        .Q(AXI_video_strm_V_data_V_0_payload_B[7]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_0_payload_B_reg[8] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_load_B),
        .D(image_in_TDATA[8]),
        .Q(AXI_video_strm_V_data_V_0_payload_B[8]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_0_payload_B_reg[9] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_load_B),
        .D(image_in_TDATA[9]),
        .Q(AXI_video_strm_V_data_V_0_payload_B[9]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair34" *) 
  LUT3 #(
    .INIT(8'h78)) 
    AXI_video_strm_V_data_V_0_sel_rd_i_1
       (.I0(\AXI_video_strm_V_data_V_0_state_reg_n_0_[0] ),
        .I1(AXI_video_strm_V_data_V_0_ack_out),
        .I2(AXI_video_strm_V_data_V_0_sel),
        .O(AXI_video_strm_V_data_V_0_sel_rd_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    AXI_video_strm_V_data_V_0_sel_rd_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(AXI_video_strm_V_data_V_0_sel_rd_i_1_n_0),
        .Q(AXI_video_strm_V_data_V_0_sel),
        .R(ap_rst));
  (* SOFT_HLUTNM = "soft_lutpair52" *) 
  LUT3 #(
    .INIT(8'h78)) 
    AXI_video_strm_V_data_V_0_sel_wr_i_1
       (.I0(image_in_TVALID),
        .I1(AXI_video_strm_V_data_V_0_ack_in),
        .I2(AXI_video_strm_V_data_V_0_sel_wr),
        .O(AXI_video_strm_V_data_V_0_sel_wr_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    AXI_video_strm_V_data_V_0_sel_wr_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(AXI_video_strm_V_data_V_0_sel_wr_i_1_n_0),
        .Q(AXI_video_strm_V_data_V_0_sel_wr),
        .R(ap_rst));
  LUT5 #(
    .INIT(32'hA8A820A0)) 
    \AXI_video_strm_V_data_V_0_state[0]_i_1 
       (.I0(ap_rst_n),
        .I1(AXI_video_strm_V_data_V_0_ack_in),
        .I2(\AXI_video_strm_V_data_V_0_state_reg_n_0_[0] ),
        .I3(AXI_video_strm_V_data_V_0_ack_out),
        .I4(image_in_TVALID),
        .O(\AXI_video_strm_V_data_V_0_state[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair34" *) 
  LUT4 #(
    .INIT(16'hF3FB)) 
    \AXI_video_strm_V_data_V_0_state[1]_i_1 
       (.I0(AXI_video_strm_V_data_V_0_ack_in),
        .I1(\AXI_video_strm_V_data_V_0_state_reg_n_0_[0] ),
        .I2(AXI_video_strm_V_data_V_0_ack_out),
        .I3(image_in_TVALID),
        .O(AXI_video_strm_V_data_V_0_state));
  FDRE #(
    .INIT(1'b0)) 
    \AXI_video_strm_V_data_V_0_state_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\AXI_video_strm_V_data_V_0_state[0]_i_1_n_0 ),
        .Q(\AXI_video_strm_V_data_V_0_state_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \AXI_video_strm_V_data_V_0_state_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(AXI_video_strm_V_data_V_0_state),
        .Q(AXI_video_strm_V_data_V_0_ack_in),
        .R(ap_rst));
  LUT5 #(
    .INIT(32'hA2AAA000)) 
    \AXI_video_strm_V_dest_V_0_state[0]_i_1 
       (.I0(ap_rst_n),
        .I1(AXI_video_strm_V_data_V_0_ack_out),
        .I2(image_in_TVALID),
        .I3(image_in_TREADY),
        .I4(\AXI_video_strm_V_dest_V_0_state_reg_n_0_[0] ),
        .O(\AXI_video_strm_V_dest_V_0_state[0]_i_1_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \AXI_video_strm_V_dest_V_0_state[1]_i_1 
       (.I0(ap_rst_n),
        .O(ap_rst));
  LUT4 #(
    .INIT(16'hBAFF)) 
    \AXI_video_strm_V_dest_V_0_state[1]_i_2 
       (.I0(AXI_video_strm_V_data_V_0_ack_out),
        .I1(image_in_TVALID),
        .I2(image_in_TREADY),
        .I3(\AXI_video_strm_V_dest_V_0_state_reg_n_0_[0] ),
        .O(AXI_video_strm_V_dest_V_0_state));
  LUT6 #(
    .INIT(64'hEEEEEEEEEEFEEEEE)) 
    \AXI_video_strm_V_dest_V_0_state[1]_i_3 
       (.I0(AXI_video_strm_V_data_V_0_sel2),
        .I1(AXI_video_strm_V_data_V_0_sel3__0),
        .I2(exitcond_reg_4160),
        .I3(\exitcond_reg_416_reg_n_0_[0] ),
        .I4(ap_enable_reg_pp1_iter1_reg_n_0),
        .I5(brmerge_reg_425),
        .O(AXI_video_strm_V_data_V_0_ack_out));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT4 #(
    .INIT(16'h0080)) 
    \AXI_video_strm_V_dest_V_0_state[1]_i_4 
       (.I0(ap_CS_fsm_pp2_stage0),
        .I1(\AXI_video_strm_V_data_V_0_state_reg_n_0_[0] ),
        .I2(ap_enable_reg_pp2_iter1_reg_n_0),
        .I3(\eol_2_reg_268_reg_n_0_[0] ),
        .O(AXI_video_strm_V_data_V_0_sel3__0));
  FDRE #(
    .INIT(1'b0)) 
    \AXI_video_strm_V_dest_V_0_state_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\AXI_video_strm_V_dest_V_0_state[0]_i_1_n_0 ),
        .Q(\AXI_video_strm_V_dest_V_0_state_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \AXI_video_strm_V_dest_V_0_state_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(AXI_video_strm_V_dest_V_0_state),
        .Q(image_in_TREADY),
        .R(ap_rst));
  LUT5 #(
    .INIT(32'hFFAE00A2)) 
    \AXI_video_strm_V_last_V_0_payload_A[0]_i_1 
       (.I0(image_in_TLAST),
        .I1(\AXI_video_strm_V_last_V_0_state_reg_n_0_[0] ),
        .I2(AXI_video_strm_V_last_V_0_ack_in),
        .I3(AXI_video_strm_V_last_V_0_sel_wr),
        .I4(AXI_video_strm_V_last_V_0_payload_A),
        .O(\AXI_video_strm_V_last_V_0_payload_A[0]_i_1_n_0 ));
  FDRE \AXI_video_strm_V_last_V_0_payload_A_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\AXI_video_strm_V_last_V_0_payload_A[0]_i_1_n_0 ),
        .Q(AXI_video_strm_V_last_V_0_payload_A),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hBBFB8808)) 
    \AXI_video_strm_V_last_V_0_payload_B[0]_i_1 
       (.I0(image_in_TLAST),
        .I1(AXI_video_strm_V_last_V_0_sel_wr),
        .I2(\AXI_video_strm_V_last_V_0_state_reg_n_0_[0] ),
        .I3(AXI_video_strm_V_last_V_0_ack_in),
        .I4(AXI_video_strm_V_last_V_0_payload_B),
        .O(\AXI_video_strm_V_last_V_0_payload_B[0]_i_1_n_0 ));
  FDRE \AXI_video_strm_V_last_V_0_payload_B_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\AXI_video_strm_V_last_V_0_payload_B[0]_i_1_n_0 ),
        .Q(AXI_video_strm_V_last_V_0_payload_B),
        .R(1'b0));
  LUT3 #(
    .INIT(8'h78)) 
    AXI_video_strm_V_last_V_0_sel_rd_i_1
       (.I0(AXI_video_strm_V_data_V_0_ack_out),
        .I1(\AXI_video_strm_V_last_V_0_state_reg_n_0_[0] ),
        .I2(AXI_video_strm_V_last_V_0_sel),
        .O(AXI_video_strm_V_last_V_0_sel_rd_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    AXI_video_strm_V_last_V_0_sel_rd_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(AXI_video_strm_V_last_V_0_sel_rd_i_1_n_0),
        .Q(AXI_video_strm_V_last_V_0_sel),
        .R(ap_rst));
  (* SOFT_HLUTNM = "soft_lutpair36" *) 
  LUT3 #(
    .INIT(8'h78)) 
    AXI_video_strm_V_last_V_0_sel_wr_i_1
       (.I0(image_in_TVALID),
        .I1(AXI_video_strm_V_last_V_0_ack_in),
        .I2(AXI_video_strm_V_last_V_0_sel_wr),
        .O(AXI_video_strm_V_last_V_0_sel_wr_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    AXI_video_strm_V_last_V_0_sel_wr_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(AXI_video_strm_V_last_V_0_sel_wr_i_1_n_0),
        .Q(AXI_video_strm_V_last_V_0_sel_wr),
        .R(ap_rst));
  LUT5 #(
    .INIT(32'hA2AAA000)) 
    \AXI_video_strm_V_last_V_0_state[0]_i_1 
       (.I0(ap_rst_n),
        .I1(AXI_video_strm_V_data_V_0_ack_out),
        .I2(image_in_TVALID),
        .I3(AXI_video_strm_V_last_V_0_ack_in),
        .I4(\AXI_video_strm_V_last_V_0_state_reg_n_0_[0] ),
        .O(\AXI_video_strm_V_last_V_0_state[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair36" *) 
  LUT4 #(
    .INIT(16'hBAFF)) 
    \AXI_video_strm_V_last_V_0_state[1]_i_1 
       (.I0(AXI_video_strm_V_data_V_0_ack_out),
        .I1(image_in_TVALID),
        .I2(AXI_video_strm_V_last_V_0_ack_in),
        .I3(\AXI_video_strm_V_last_V_0_state_reg_n_0_[0] ),
        .O(AXI_video_strm_V_last_V_0_state));
  FDRE #(
    .INIT(1'b0)) 
    \AXI_video_strm_V_last_V_0_state_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\AXI_video_strm_V_last_V_0_state[0]_i_1_n_0 ),
        .Q(\AXI_video_strm_V_last_V_0_state_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \AXI_video_strm_V_last_V_0_state_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(AXI_video_strm_V_last_V_0_state),
        .Q(AXI_video_strm_V_last_V_0_ack_in),
        .R(ap_rst));
  LUT5 #(
    .INIT(32'hFFAE00A2)) 
    \AXI_video_strm_V_user_V_0_payload_A[0]_i_1 
       (.I0(image_in_TUSER),
        .I1(\AXI_video_strm_V_user_V_0_state_reg_n_0_[0] ),
        .I2(AXI_video_strm_V_user_V_0_ack_in),
        .I3(AXI_video_strm_V_user_V_0_sel_wr),
        .I4(AXI_video_strm_V_user_V_0_payload_A),
        .O(\AXI_video_strm_V_user_V_0_payload_A[0]_i_1_n_0 ));
  FDRE \AXI_video_strm_V_user_V_0_payload_A_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\AXI_video_strm_V_user_V_0_payload_A[0]_i_1_n_0 ),
        .Q(AXI_video_strm_V_user_V_0_payload_A),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hBBFB8808)) 
    \AXI_video_strm_V_user_V_0_payload_B[0]_i_1 
       (.I0(image_in_TUSER),
        .I1(AXI_video_strm_V_user_V_0_sel_wr),
        .I2(\AXI_video_strm_V_user_V_0_state_reg_n_0_[0] ),
        .I3(AXI_video_strm_V_user_V_0_ack_in),
        .I4(AXI_video_strm_V_user_V_0_payload_B),
        .O(\AXI_video_strm_V_user_V_0_payload_B[0]_i_1_n_0 ));
  FDRE \AXI_video_strm_V_user_V_0_payload_B_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\AXI_video_strm_V_user_V_0_payload_B[0]_i_1_n_0 ),
        .Q(AXI_video_strm_V_user_V_0_payload_B),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair37" *) 
  LUT3 #(
    .INIT(8'h78)) 
    AXI_video_strm_V_user_V_0_sel_rd_i_1
       (.I0(\AXI_video_strm_V_user_V_0_state_reg_n_0_[0] ),
        .I1(AXI_video_strm_V_data_V_0_ack_out),
        .I2(AXI_video_strm_V_user_V_0_sel),
        .O(AXI_video_strm_V_user_V_0_sel_rd_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    AXI_video_strm_V_user_V_0_sel_rd_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(AXI_video_strm_V_user_V_0_sel_rd_i_1_n_0),
        .Q(AXI_video_strm_V_user_V_0_sel),
        .R(ap_rst));
  (* SOFT_HLUTNM = "soft_lutpair52" *) 
  LUT3 #(
    .INIT(8'h78)) 
    AXI_video_strm_V_user_V_0_sel_wr_i_1
       (.I0(image_in_TVALID),
        .I1(AXI_video_strm_V_user_V_0_ack_in),
        .I2(AXI_video_strm_V_user_V_0_sel_wr),
        .O(AXI_video_strm_V_user_V_0_sel_wr_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    AXI_video_strm_V_user_V_0_sel_wr_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(AXI_video_strm_V_user_V_0_sel_wr_i_1_n_0),
        .Q(AXI_video_strm_V_user_V_0_sel_wr),
        .R(ap_rst));
  LUT5 #(
    .INIT(32'hA8A820A0)) 
    \AXI_video_strm_V_user_V_0_state[0]_i_1 
       (.I0(ap_rst_n),
        .I1(AXI_video_strm_V_user_V_0_ack_in),
        .I2(\AXI_video_strm_V_user_V_0_state_reg_n_0_[0] ),
        .I3(AXI_video_strm_V_data_V_0_ack_out),
        .I4(image_in_TVALID),
        .O(\AXI_video_strm_V_user_V_0_state[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair37" *) 
  LUT4 #(
    .INIT(16'hF3FB)) 
    \AXI_video_strm_V_user_V_0_state[1]_i_1 
       (.I0(AXI_video_strm_V_user_V_0_ack_in),
        .I1(\AXI_video_strm_V_user_V_0_state_reg_n_0_[0] ),
        .I2(AXI_video_strm_V_data_V_0_ack_out),
        .I3(image_in_TVALID),
        .O(AXI_video_strm_V_user_V_0_state));
  FDRE #(
    .INIT(1'b0)) 
    \AXI_video_strm_V_user_V_0_state_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\AXI_video_strm_V_user_V_0_state[0]_i_1_n_0 ),
        .Q(\AXI_video_strm_V_user_V_0_state_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \AXI_video_strm_V_user_V_0_state_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(AXI_video_strm_V_user_V_0_state),
        .Q(AXI_video_strm_V_user_V_0_ack_in),
        .R(ap_rst));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \SRL_SIG[0][0]_i_1 
       (.I0(axi_data_V_1_reg_232[16]),
        .I1(brmerge_reg_425),
        .I2(AXI_video_strm_V_data_V_0_payload_B[16]),
        .I3(AXI_video_strm_V_data_V_0_sel),
        .I4(AXI_video_strm_V_data_V_0_payload_A[16]),
        .O(D[0]));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \SRL_SIG[0][0]_i_1__0 
       (.I0(axi_data_V_1_reg_232[8]),
        .I1(brmerge_reg_425),
        .I2(AXI_video_strm_V_data_V_0_payload_B[8]),
        .I3(AXI_video_strm_V_data_V_0_sel),
        .I4(AXI_video_strm_V_data_V_0_payload_A[8]),
        .O(\SRL_SIG_reg[0][7] [0]));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \SRL_SIG[0][0]_i_1__1 
       (.I0(axi_data_V_1_reg_232[0]),
        .I1(brmerge_reg_425),
        .I2(AXI_video_strm_V_data_V_0_payload_B[0]),
        .I3(AXI_video_strm_V_data_V_0_sel),
        .I4(AXI_video_strm_V_data_V_0_payload_A[0]),
        .O(\SRL_SIG_reg[0][7]_0 [0]));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \SRL_SIG[0][1]_i_1 
       (.I0(axi_data_V_1_reg_232[17]),
        .I1(brmerge_reg_425),
        .I2(AXI_video_strm_V_data_V_0_payload_B[17]),
        .I3(AXI_video_strm_V_data_V_0_sel),
        .I4(AXI_video_strm_V_data_V_0_payload_A[17]),
        .O(D[1]));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \SRL_SIG[0][1]_i_1__0 
       (.I0(axi_data_V_1_reg_232[9]),
        .I1(brmerge_reg_425),
        .I2(AXI_video_strm_V_data_V_0_payload_B[9]),
        .I3(AXI_video_strm_V_data_V_0_sel),
        .I4(AXI_video_strm_V_data_V_0_payload_A[9]),
        .O(\SRL_SIG_reg[0][7] [1]));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \SRL_SIG[0][1]_i_1__1 
       (.I0(axi_data_V_1_reg_232[1]),
        .I1(brmerge_reg_425),
        .I2(AXI_video_strm_V_data_V_0_payload_B[1]),
        .I3(AXI_video_strm_V_data_V_0_sel),
        .I4(AXI_video_strm_V_data_V_0_payload_A[1]),
        .O(\SRL_SIG_reg[0][7]_0 [1]));
  (* SOFT_HLUTNM = "soft_lutpair29" *) 
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \SRL_SIG[0][2]_i_1 
       (.I0(axi_data_V_1_reg_232[18]),
        .I1(brmerge_reg_425),
        .I2(AXI_video_strm_V_data_V_0_payload_B[18]),
        .I3(AXI_video_strm_V_data_V_0_sel),
        .I4(AXI_video_strm_V_data_V_0_payload_A[18]),
        .O(D[2]));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \SRL_SIG[0][2]_i_1__0 
       (.I0(axi_data_V_1_reg_232[10]),
        .I1(brmerge_reg_425),
        .I2(AXI_video_strm_V_data_V_0_payload_B[10]),
        .I3(AXI_video_strm_V_data_V_0_sel),
        .I4(AXI_video_strm_V_data_V_0_payload_A[10]),
        .O(\SRL_SIG_reg[0][7] [2]));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \SRL_SIG[0][2]_i_1__1 
       (.I0(axi_data_V_1_reg_232[2]),
        .I1(brmerge_reg_425),
        .I2(AXI_video_strm_V_data_V_0_payload_B[2]),
        .I3(AXI_video_strm_V_data_V_0_sel),
        .I4(AXI_video_strm_V_data_V_0_payload_A[2]),
        .O(\SRL_SIG_reg[0][7]_0 [2]));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \SRL_SIG[0][3]_i_1 
       (.I0(axi_data_V_1_reg_232[19]),
        .I1(brmerge_reg_425),
        .I2(AXI_video_strm_V_data_V_0_payload_B[19]),
        .I3(AXI_video_strm_V_data_V_0_sel),
        .I4(AXI_video_strm_V_data_V_0_payload_A[19]),
        .O(D[3]));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \SRL_SIG[0][3]_i_1__0 
       (.I0(axi_data_V_1_reg_232[11]),
        .I1(brmerge_reg_425),
        .I2(AXI_video_strm_V_data_V_0_payload_B[11]),
        .I3(AXI_video_strm_V_data_V_0_sel),
        .I4(AXI_video_strm_V_data_V_0_payload_A[11]),
        .O(\SRL_SIG_reg[0][7] [3]));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \SRL_SIG[0][3]_i_1__1 
       (.I0(axi_data_V_1_reg_232[3]),
        .I1(brmerge_reg_425),
        .I2(AXI_video_strm_V_data_V_0_payload_B[3]),
        .I3(AXI_video_strm_V_data_V_0_sel),
        .I4(AXI_video_strm_V_data_V_0_payload_A[3]),
        .O(\SRL_SIG_reg[0][7]_0 [3]));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \SRL_SIG[0][4]_i_1 
       (.I0(axi_data_V_1_reg_232[20]),
        .I1(brmerge_reg_425),
        .I2(AXI_video_strm_V_data_V_0_payload_B[20]),
        .I3(AXI_video_strm_V_data_V_0_sel),
        .I4(AXI_video_strm_V_data_V_0_payload_A[20]),
        .O(D[4]));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \SRL_SIG[0][4]_i_1__0 
       (.I0(axi_data_V_1_reg_232[12]),
        .I1(brmerge_reg_425),
        .I2(AXI_video_strm_V_data_V_0_payload_B[12]),
        .I3(AXI_video_strm_V_data_V_0_sel),
        .I4(AXI_video_strm_V_data_V_0_payload_A[12]),
        .O(\SRL_SIG_reg[0][7] [4]));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \SRL_SIG[0][4]_i_1__1 
       (.I0(axi_data_V_1_reg_232[4]),
        .I1(brmerge_reg_425),
        .I2(AXI_video_strm_V_data_V_0_payload_B[4]),
        .I3(AXI_video_strm_V_data_V_0_sel),
        .I4(AXI_video_strm_V_data_V_0_payload_A[4]),
        .O(\SRL_SIG_reg[0][7]_0 [4]));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \SRL_SIG[0][5]_i_1 
       (.I0(axi_data_V_1_reg_232[21]),
        .I1(brmerge_reg_425),
        .I2(AXI_video_strm_V_data_V_0_payload_B[21]),
        .I3(AXI_video_strm_V_data_V_0_sel),
        .I4(AXI_video_strm_V_data_V_0_payload_A[21]),
        .O(D[5]));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \SRL_SIG[0][5]_i_1__0 
       (.I0(axi_data_V_1_reg_232[13]),
        .I1(brmerge_reg_425),
        .I2(AXI_video_strm_V_data_V_0_payload_B[13]),
        .I3(AXI_video_strm_V_data_V_0_sel),
        .I4(AXI_video_strm_V_data_V_0_payload_A[13]),
        .O(\SRL_SIG_reg[0][7] [5]));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \SRL_SIG[0][5]_i_1__1 
       (.I0(axi_data_V_1_reg_232[5]),
        .I1(brmerge_reg_425),
        .I2(AXI_video_strm_V_data_V_0_payload_B[5]),
        .I3(AXI_video_strm_V_data_V_0_sel),
        .I4(AXI_video_strm_V_data_V_0_payload_A[5]),
        .O(\SRL_SIG_reg[0][7]_0 [5]));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \SRL_SIG[0][6]_i_1 
       (.I0(axi_data_V_1_reg_232[22]),
        .I1(brmerge_reg_425),
        .I2(AXI_video_strm_V_data_V_0_payload_B[22]),
        .I3(AXI_video_strm_V_data_V_0_sel),
        .I4(AXI_video_strm_V_data_V_0_payload_A[22]),
        .O(D[6]));
  (* SOFT_HLUTNM = "soft_lutpair28" *) 
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \SRL_SIG[0][6]_i_1__0 
       (.I0(axi_data_V_1_reg_232[14]),
        .I1(brmerge_reg_425),
        .I2(AXI_video_strm_V_data_V_0_payload_B[14]),
        .I3(AXI_video_strm_V_data_V_0_sel),
        .I4(AXI_video_strm_V_data_V_0_payload_A[14]),
        .O(\SRL_SIG_reg[0][7] [6]));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \SRL_SIG[0][6]_i_1__1 
       (.I0(axi_data_V_1_reg_232[6]),
        .I1(brmerge_reg_425),
        .I2(AXI_video_strm_V_data_V_0_payload_B[6]),
        .I3(AXI_video_strm_V_data_V_0_sel),
        .I4(AXI_video_strm_V_data_V_0_payload_A[6]),
        .O(\SRL_SIG_reg[0][7]_0 [6]));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \SRL_SIG[0][7]_i_2__2 
       (.I0(axi_data_V_1_reg_232[23]),
        .I1(brmerge_reg_425),
        .I2(AXI_video_strm_V_data_V_0_payload_B[23]),
        .I3(AXI_video_strm_V_data_V_0_sel),
        .I4(AXI_video_strm_V_data_V_0_payload_A[23]),
        .O(D[7]));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \SRL_SIG[0][7]_i_2__3 
       (.I0(axi_data_V_1_reg_232[15]),
        .I1(brmerge_reg_425),
        .I2(AXI_video_strm_V_data_V_0_payload_B[15]),
        .I3(AXI_video_strm_V_data_V_0_sel),
        .I4(AXI_video_strm_V_data_V_0_payload_A[15]),
        .O(\SRL_SIG_reg[0][7] [7]));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \SRL_SIG[0][7]_i_2__4 
       (.I0(axi_data_V_1_reg_232[7]),
        .I1(brmerge_reg_425),
        .I2(AXI_video_strm_V_data_V_0_payload_B[7]),
        .I3(AXI_video_strm_V_data_V_0_sel),
        .I4(AXI_video_strm_V_data_V_0_payload_A[7]),
        .O(\SRL_SIG_reg[0][7]_0 [7]));
  LUT6 #(
    .INIT(64'h888FFFFF88888888)) 
    \ap_CS_fsm[0]_i_1__2 
       (.I0(ap_CS_fsm_state4),
        .I1(exitcond3_fu_322_p2__14),
        .I2(start_for_Loop_loop_height_pro_U0_full_n),
        .I3(start_once_reg),
        .I4(ap_start),
        .I5(Q),
        .O(ap_NS_fsm[0]));
  LUT6 #(
    .INIT(64'hFFF8888888888888)) 
    \ap_CS_fsm[1]_i_1 
       (.I0(ap_CS_fsm_state2),
        .I1(\ap_CS_fsm[1]_i_2_n_0 ),
        .I2(start_for_Loop_loop_height_pro_U0_full_n),
        .I3(start_once_reg),
        .I4(ap_start),
        .I5(Q),
        .O(ap_NS_fsm[1]));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT5 #(
    .INIT(32'h1DFFFFFF)) 
    \ap_CS_fsm[1]_i_2 
       (.I0(AXI_video_strm_V_user_V_0_payload_A),
        .I1(AXI_video_strm_V_user_V_0_sel),
        .I2(AXI_video_strm_V_user_V_0_payload_B),
        .I3(\AXI_video_strm_V_data_V_0_state_reg_n_0_[0] ),
        .I4(ap_CS_fsm_state2),
        .O(\ap_CS_fsm[1]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT5 #(
    .INIT(32'h80888000)) 
    \ap_CS_fsm[2]_i_1 
       (.I0(ap_CS_fsm_state2),
        .I1(\AXI_video_strm_V_data_V_0_state_reg_n_0_[0] ),
        .I2(AXI_video_strm_V_user_V_0_payload_B),
        .I3(AXI_video_strm_V_user_V_0_sel),
        .I4(AXI_video_strm_V_user_V_0_payload_A),
        .O(ap_NS_fsm[2]));
  LUT2 #(
    .INIT(4'hE)) 
    \ap_CS_fsm[3]_i_1__2 
       (.I0(ap_CS_fsm_state3),
        .I1(ap_CS_fsm_state10),
        .O(ap_NS_fsm[3]));
  LUT6 #(
    .INIT(64'hF2FFFFFF22222222)) 
    \ap_CS_fsm[4]_i_1__0 
       (.I0(ap_CS_fsm_state4),
        .I1(exitcond3_fu_322_p2__14),
        .I2(ap_enable_reg_pp1_iter0),
        .I3(ap_enable_reg_pp1_iter1_reg_n_0),
        .I4(exitcond_reg_4160),
        .I5(ap_CS_fsm_pp1_stage0),
        .O(ap_NS_fsm[4]));
  (* SOFT_HLUTNM = "soft_lutpair38" *) 
  LUT4 #(
    .INIT(16'h0080)) 
    \ap_CS_fsm[5]_i_1 
       (.I0(ap_CS_fsm_pp1_stage0),
        .I1(exitcond_reg_4160),
        .I2(ap_enable_reg_pp1_iter1_reg_n_0),
        .I3(ap_enable_reg_pp1_iter0),
        .O(ap_NS_fsm[5]));
  LUT3 #(
    .INIT(8'h2A)) 
    \ap_CS_fsm[5]_i_2 
       (.I0(ap_CS_fsm_pp1_stage0),
        .I1(ap_enable_reg_pp1_iter1_reg_n_0),
        .I2(ap_block_pp1_stage0_110011),
        .O(exitcond_reg_4160));
  LUT6 #(
    .INIT(64'hFFFFFFFFAFBF0000)) 
    \ap_CS_fsm[6]_i_1 
       (.I0(ap_enable_reg_pp2_iter0),
        .I1(\eol_2_reg_268_reg_n_0_[0] ),
        .I2(ap_enable_reg_pp2_iter1_reg_n_0),
        .I3(\AXI_video_strm_V_data_V_0_state_reg_n_0_[0] ),
        .I4(ap_CS_fsm_pp2_stage0),
        .I5(ap_CS_fsm_state7),
        .O(ap_NS_fsm[6]));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT5 #(
    .INIT(32'h0000A080)) 
    \ap_CS_fsm[7]_i_1 
       (.I0(ap_CS_fsm_pp2_stage0),
        .I1(\AXI_video_strm_V_data_V_0_state_reg_n_0_[0] ),
        .I2(ap_enable_reg_pp2_iter1_reg_n_0),
        .I3(\eol_2_reg_268_reg_n_0_[0] ),
        .I4(ap_enable_reg_pp2_iter0),
        .O(ap_NS_fsm[7]));
  (* FSM_ENCODING = "none" *) 
  FDSE #(
    .INIT(1'b1)) 
    \ap_CS_fsm_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[0]),
        .Q(Q),
        .S(ap_rst));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[1]),
        .Q(ap_CS_fsm_state2),
        .R(ap_rst));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[2] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[2]),
        .Q(ap_CS_fsm_state3),
        .R(ap_rst));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[3] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[3]),
        .Q(ap_CS_fsm_state4),
        .R(ap_rst));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[4] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[4]),
        .Q(ap_CS_fsm_pp1_stage0),
        .R(ap_rst));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[5] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[5]),
        .Q(ap_CS_fsm_state7),
        .R(ap_rst));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[6] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[6]),
        .Q(ap_CS_fsm_pp2_stage0),
        .R(ap_rst));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[7] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[7]),
        .Q(ap_CS_fsm_state10),
        .R(ap_rst));
  LUT6 #(
    .INIT(64'h0000F200F200F200)) 
    ap_enable_reg_pp1_iter0_i_1
       (.I0(ap_CS_fsm_state4),
        .I1(exitcond3_fu_322_p2__14),
        .I2(ap_enable_reg_pp1_iter0),
        .I3(ap_rst_n),
        .I4(exitcond_reg_4160),
        .I5(exitcond_fu_334_p2),
        .O(ap_enable_reg_pp1_iter0_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    ap_enable_reg_pp1_iter0_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_enable_reg_pp1_iter0_i_1_n_0),
        .Q(ap_enable_reg_pp1_iter0),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hDD00F000F000F000)) 
    ap_enable_reg_pp1_iter1_i_1
       (.I0(ap_CS_fsm_state4),
        .I1(exitcond3_fu_322_p2__14),
        .I2(ap_enable_reg_pp1_iter0),
        .I3(ap_rst_n),
        .I4(ap_enable_reg_pp1_iter1_reg_n_0),
        .I5(ap_block_pp1_stage0_110011),
        .O(ap_enable_reg_pp1_iter1_i_1_n_0));
  LUT6 #(
    .INIT(64'h000057FF0000FFFF)) 
    ap_enable_reg_pp1_iter1_i_2
       (.I0(input_data_data_stre_2_full_n),
        .I1(\AXI_video_strm_V_data_V_0_state_reg_n_0_[0] ),
        .I2(brmerge_reg_425),
        .I3(input_data_data_stre_full_n),
        .I4(\exitcond_reg_416_reg_n_0_[0] ),
        .I5(input_data_data_stre_1_full_n),
        .O(ap_block_pp1_stage0_110011));
  FDRE #(
    .INIT(1'b0)) 
    ap_enable_reg_pp1_iter1_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_enable_reg_pp1_iter1_i_1_n_0),
        .Q(ap_enable_reg_pp1_iter1_reg_n_0),
        .R(1'b0));
  LUT3 #(
    .INIT(8'h0E)) 
    ap_enable_reg_pp2_iter0_i_1
       (.I0(ap_enable_reg_pp2_iter0),
        .I1(ap_CS_fsm_state7),
        .I2(ap_enable_reg_pp2_iter0_i_2_n_0),
        .O(ap_enable_reg_pp2_iter0_i_1_n_0));
  LUT6 #(
    .INIT(64'hDDDDD555DDDD5555)) 
    ap_enable_reg_pp2_iter0_i_2
       (.I0(ap_rst_n),
        .I1(ap_CS_fsm_pp2_stage0),
        .I2(\AXI_video_strm_V_data_V_0_state_reg_n_0_[0] ),
        .I3(ap_enable_reg_pp2_iter1_reg_n_0),
        .I4(\eol_2_reg_268_reg_n_0_[0] ),
        .I5(AXI_video_strm_V_last_V_0_data_out),
        .O(ap_enable_reg_pp2_iter0_i_2_n_0));
  FDRE #(
    .INIT(1'b0)) 
    ap_enable_reg_pp2_iter0_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_enable_reg_pp2_iter0_i_1_n_0),
        .Q(ap_enable_reg_pp2_iter0),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h88888888880C8888)) 
    ap_enable_reg_pp2_iter1_i_1
       (.I0(ap_enable_reg_pp2_iter0),
        .I1(ap_rst_n),
        .I2(ap_CS_fsm_state7),
        .I3(\AXI_video_strm_V_data_V_0_state_reg_n_0_[0] ),
        .I4(ap_enable_reg_pp2_iter1_reg_n_0),
        .I5(\eol_2_reg_268_reg_n_0_[0] ),
        .O(ap_enable_reg_pp2_iter1_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    ap_enable_reg_pp2_iter1_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_enable_reg_pp2_iter1_i_1_n_0),
        .Q(ap_enable_reg_pp2_iter1_reg_n_0),
        .R(1'b0));
  LUT2 #(
    .INIT(4'h8)) 
    ap_ready_INST_0
       (.I0(ap_CS_fsm_state4),
        .I1(exitcond3_fu_322_p2__14),
        .O(ap_ready));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT5 #(
    .INIT(32'h00000008)) 
    ap_ready_INST_0_i_1
       (.I0(ap_ready_INST_0_i_2_n_0),
        .I1(ap_ready_INST_0_i_3_n_0),
        .I2(t_V_reg_187[0]),
        .I3(t_V_reg_187[1]),
        .I4(t_V_reg_187[2]),
        .O(exitcond3_fu_322_p2__14));
  (* SOFT_HLUTNM = "soft_lutpair39" *) 
  LUT4 #(
    .INIT(16'h4000)) 
    ap_ready_INST_0_i_2
       (.I0(t_V_reg_187[6]),
        .I1(t_V_reg_187[5]),
        .I2(t_V_reg_187[4]),
        .I3(t_V_reg_187[3]),
        .O(ap_ready_INST_0_i_2_n_0));
  LUT4 #(
    .INIT(16'h0004)) 
    ap_ready_INST_0_i_3
       (.I0(t_V_reg_187[9]),
        .I1(t_V_reg_187[10]),
        .I2(t_V_reg_187[8]),
        .I3(t_V_reg_187[7]),
        .O(ap_ready_INST_0_i_3_n_0));
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_data_V1_reg_177[0]_i_1 
       (.I0(tmp_data_V_reg_387[0]),
        .I1(ap_CS_fsm_state3),
        .I2(axi_data_V_3_reg_291[0]),
        .O(\axi_data_V1_reg_177[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair44" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_data_V1_reg_177[10]_i_1 
       (.I0(tmp_data_V_reg_387[10]),
        .I1(ap_CS_fsm_state3),
        .I2(axi_data_V_3_reg_291[10]),
        .O(\axi_data_V1_reg_177[10]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair49" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_data_V1_reg_177[11]_i_1 
       (.I0(tmp_data_V_reg_387[11]),
        .I1(ap_CS_fsm_state3),
        .I2(axi_data_V_3_reg_291[11]),
        .O(\axi_data_V1_reg_177[11]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair49" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_data_V1_reg_177[12]_i_1 
       (.I0(tmp_data_V_reg_387[12]),
        .I1(ap_CS_fsm_state3),
        .I2(axi_data_V_3_reg_291[12]),
        .O(\axi_data_V1_reg_177[12]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair48" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_data_V1_reg_177[13]_i_1 
       (.I0(tmp_data_V_reg_387[13]),
        .I1(ap_CS_fsm_state3),
        .I2(axi_data_V_3_reg_291[13]),
        .O(\axi_data_V1_reg_177[13]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair48" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_data_V1_reg_177[14]_i_1 
       (.I0(tmp_data_V_reg_387[14]),
        .I1(ap_CS_fsm_state3),
        .I2(axi_data_V_3_reg_291[14]),
        .O(\axi_data_V1_reg_177[14]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair47" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_data_V1_reg_177[15]_i_1 
       (.I0(tmp_data_V_reg_387[15]),
        .I1(ap_CS_fsm_state3),
        .I2(axi_data_V_3_reg_291[15]),
        .O(\axi_data_V1_reg_177[15]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair47" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_data_V1_reg_177[16]_i_1 
       (.I0(tmp_data_V_reg_387[16]),
        .I1(ap_CS_fsm_state3),
        .I2(axi_data_V_3_reg_291[16]),
        .O(\axi_data_V1_reg_177[16]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair46" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_data_V1_reg_177[17]_i_1 
       (.I0(tmp_data_V_reg_387[17]),
        .I1(ap_CS_fsm_state3),
        .I2(axi_data_V_3_reg_291[17]),
        .O(\axi_data_V1_reg_177[17]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair46" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_data_V1_reg_177[18]_i_1 
       (.I0(tmp_data_V_reg_387[18]),
        .I1(ap_CS_fsm_state3),
        .I2(axi_data_V_3_reg_291[18]),
        .O(\axi_data_V1_reg_177[18]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair45" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_data_V1_reg_177[19]_i_1 
       (.I0(tmp_data_V_reg_387[19]),
        .I1(ap_CS_fsm_state3),
        .I2(axi_data_V_3_reg_291[19]),
        .O(\axi_data_V1_reg_177[19]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair54" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_data_V1_reg_177[1]_i_1 
       (.I0(tmp_data_V_reg_387[1]),
        .I1(ap_CS_fsm_state3),
        .I2(axi_data_V_3_reg_291[1]),
        .O(\axi_data_V1_reg_177[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair45" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_data_V1_reg_177[20]_i_1 
       (.I0(tmp_data_V_reg_387[20]),
        .I1(ap_CS_fsm_state3),
        .I2(axi_data_V_3_reg_291[20]),
        .O(\axi_data_V1_reg_177[20]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair44" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_data_V1_reg_177[21]_i_1 
       (.I0(tmp_data_V_reg_387[21]),
        .I1(ap_CS_fsm_state3),
        .I2(axi_data_V_3_reg_291[21]),
        .O(\axi_data_V1_reg_177[21]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair43" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_data_V1_reg_177[22]_i_1 
       (.I0(tmp_data_V_reg_387[22]),
        .I1(ap_CS_fsm_state3),
        .I2(axi_data_V_3_reg_291[22]),
        .O(\axi_data_V1_reg_177[22]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair42" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_data_V1_reg_177[23]_i_1 
       (.I0(tmp_data_V_reg_387[23]),
        .I1(ap_CS_fsm_state3),
        .I2(axi_data_V_3_reg_291[23]),
        .O(\axi_data_V1_reg_177[23]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair51" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_data_V1_reg_177[2]_i_1 
       (.I0(tmp_data_V_reg_387[2]),
        .I1(ap_CS_fsm_state3),
        .I2(axi_data_V_3_reg_291[2]),
        .O(\axi_data_V1_reg_177[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair54" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_data_V1_reg_177[3]_i_1 
       (.I0(tmp_data_V_reg_387[3]),
        .I1(ap_CS_fsm_state3),
        .I2(axi_data_V_3_reg_291[3]),
        .O(\axi_data_V1_reg_177[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair53" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_data_V1_reg_177[4]_i_1 
       (.I0(tmp_data_V_reg_387[4]),
        .I1(ap_CS_fsm_state3),
        .I2(axi_data_V_3_reg_291[4]),
        .O(\axi_data_V1_reg_177[4]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair53" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_data_V1_reg_177[5]_i_1 
       (.I0(tmp_data_V_reg_387[5]),
        .I1(ap_CS_fsm_state3),
        .I2(axi_data_V_3_reg_291[5]),
        .O(\axi_data_V1_reg_177[5]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair51" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_data_V1_reg_177[6]_i_1 
       (.I0(tmp_data_V_reg_387[6]),
        .I1(ap_CS_fsm_state3),
        .I2(axi_data_V_3_reg_291[6]),
        .O(\axi_data_V1_reg_177[6]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair50" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_data_V1_reg_177[7]_i_1 
       (.I0(tmp_data_V_reg_387[7]),
        .I1(ap_CS_fsm_state3),
        .I2(axi_data_V_3_reg_291[7]),
        .O(\axi_data_V1_reg_177[7]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair43" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_data_V1_reg_177[8]_i_1 
       (.I0(tmp_data_V_reg_387[8]),
        .I1(ap_CS_fsm_state3),
        .I2(axi_data_V_3_reg_291[8]),
        .O(\axi_data_V1_reg_177[8]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair50" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_data_V1_reg_177[9]_i_1 
       (.I0(tmp_data_V_reg_387[9]),
        .I1(ap_CS_fsm_state3),
        .I2(axi_data_V_3_reg_291[9]),
        .O(\axi_data_V1_reg_177[9]_i_1_n_0 ));
  FDRE \axi_data_V1_reg_177_reg[0] 
       (.C(ap_clk),
        .CE(ap_NS_fsm[3]),
        .D(\axi_data_V1_reg_177[0]_i_1_n_0 ),
        .Q(axi_data_V1_reg_177[0]),
        .R(1'b0));
  FDRE \axi_data_V1_reg_177_reg[10] 
       (.C(ap_clk),
        .CE(ap_NS_fsm[3]),
        .D(\axi_data_V1_reg_177[10]_i_1_n_0 ),
        .Q(axi_data_V1_reg_177[10]),
        .R(1'b0));
  FDRE \axi_data_V1_reg_177_reg[11] 
       (.C(ap_clk),
        .CE(ap_NS_fsm[3]),
        .D(\axi_data_V1_reg_177[11]_i_1_n_0 ),
        .Q(axi_data_V1_reg_177[11]),
        .R(1'b0));
  FDRE \axi_data_V1_reg_177_reg[12] 
       (.C(ap_clk),
        .CE(ap_NS_fsm[3]),
        .D(\axi_data_V1_reg_177[12]_i_1_n_0 ),
        .Q(axi_data_V1_reg_177[12]),
        .R(1'b0));
  FDRE \axi_data_V1_reg_177_reg[13] 
       (.C(ap_clk),
        .CE(ap_NS_fsm[3]),
        .D(\axi_data_V1_reg_177[13]_i_1_n_0 ),
        .Q(axi_data_V1_reg_177[13]),
        .R(1'b0));
  FDRE \axi_data_V1_reg_177_reg[14] 
       (.C(ap_clk),
        .CE(ap_NS_fsm[3]),
        .D(\axi_data_V1_reg_177[14]_i_1_n_0 ),
        .Q(axi_data_V1_reg_177[14]),
        .R(1'b0));
  FDRE \axi_data_V1_reg_177_reg[15] 
       (.C(ap_clk),
        .CE(ap_NS_fsm[3]),
        .D(\axi_data_V1_reg_177[15]_i_1_n_0 ),
        .Q(axi_data_V1_reg_177[15]),
        .R(1'b0));
  FDRE \axi_data_V1_reg_177_reg[16] 
       (.C(ap_clk),
        .CE(ap_NS_fsm[3]),
        .D(\axi_data_V1_reg_177[16]_i_1_n_0 ),
        .Q(axi_data_V1_reg_177[16]),
        .R(1'b0));
  FDRE \axi_data_V1_reg_177_reg[17] 
       (.C(ap_clk),
        .CE(ap_NS_fsm[3]),
        .D(\axi_data_V1_reg_177[17]_i_1_n_0 ),
        .Q(axi_data_V1_reg_177[17]),
        .R(1'b0));
  FDRE \axi_data_V1_reg_177_reg[18] 
       (.C(ap_clk),
        .CE(ap_NS_fsm[3]),
        .D(\axi_data_V1_reg_177[18]_i_1_n_0 ),
        .Q(axi_data_V1_reg_177[18]),
        .R(1'b0));
  FDRE \axi_data_V1_reg_177_reg[19] 
       (.C(ap_clk),
        .CE(ap_NS_fsm[3]),
        .D(\axi_data_V1_reg_177[19]_i_1_n_0 ),
        .Q(axi_data_V1_reg_177[19]),
        .R(1'b0));
  FDRE \axi_data_V1_reg_177_reg[1] 
       (.C(ap_clk),
        .CE(ap_NS_fsm[3]),
        .D(\axi_data_V1_reg_177[1]_i_1_n_0 ),
        .Q(axi_data_V1_reg_177[1]),
        .R(1'b0));
  FDRE \axi_data_V1_reg_177_reg[20] 
       (.C(ap_clk),
        .CE(ap_NS_fsm[3]),
        .D(\axi_data_V1_reg_177[20]_i_1_n_0 ),
        .Q(axi_data_V1_reg_177[20]),
        .R(1'b0));
  FDRE \axi_data_V1_reg_177_reg[21] 
       (.C(ap_clk),
        .CE(ap_NS_fsm[3]),
        .D(\axi_data_V1_reg_177[21]_i_1_n_0 ),
        .Q(axi_data_V1_reg_177[21]),
        .R(1'b0));
  FDRE \axi_data_V1_reg_177_reg[22] 
       (.C(ap_clk),
        .CE(ap_NS_fsm[3]),
        .D(\axi_data_V1_reg_177[22]_i_1_n_0 ),
        .Q(axi_data_V1_reg_177[22]),
        .R(1'b0));
  FDRE \axi_data_V1_reg_177_reg[23] 
       (.C(ap_clk),
        .CE(ap_NS_fsm[3]),
        .D(\axi_data_V1_reg_177[23]_i_1_n_0 ),
        .Q(axi_data_V1_reg_177[23]),
        .R(1'b0));
  FDRE \axi_data_V1_reg_177_reg[2] 
       (.C(ap_clk),
        .CE(ap_NS_fsm[3]),
        .D(\axi_data_V1_reg_177[2]_i_1_n_0 ),
        .Q(axi_data_V1_reg_177[2]),
        .R(1'b0));
  FDRE \axi_data_V1_reg_177_reg[3] 
       (.C(ap_clk),
        .CE(ap_NS_fsm[3]),
        .D(\axi_data_V1_reg_177[3]_i_1_n_0 ),
        .Q(axi_data_V1_reg_177[3]),
        .R(1'b0));
  FDRE \axi_data_V1_reg_177_reg[4] 
       (.C(ap_clk),
        .CE(ap_NS_fsm[3]),
        .D(\axi_data_V1_reg_177[4]_i_1_n_0 ),
        .Q(axi_data_V1_reg_177[4]),
        .R(1'b0));
  FDRE \axi_data_V1_reg_177_reg[5] 
       (.C(ap_clk),
        .CE(ap_NS_fsm[3]),
        .D(\axi_data_V1_reg_177[5]_i_1_n_0 ),
        .Q(axi_data_V1_reg_177[5]),
        .R(1'b0));
  FDRE \axi_data_V1_reg_177_reg[6] 
       (.C(ap_clk),
        .CE(ap_NS_fsm[3]),
        .D(\axi_data_V1_reg_177[6]_i_1_n_0 ),
        .Q(axi_data_V1_reg_177[6]),
        .R(1'b0));
  FDRE \axi_data_V1_reg_177_reg[7] 
       (.C(ap_clk),
        .CE(ap_NS_fsm[3]),
        .D(\axi_data_V1_reg_177[7]_i_1_n_0 ),
        .Q(axi_data_V1_reg_177[7]),
        .R(1'b0));
  FDRE \axi_data_V1_reg_177_reg[8] 
       (.C(ap_clk),
        .CE(ap_NS_fsm[3]),
        .D(\axi_data_V1_reg_177[8]_i_1_n_0 ),
        .Q(axi_data_V1_reg_177[8]),
        .R(1'b0));
  FDRE \axi_data_V1_reg_177_reg[9] 
       (.C(ap_clk),
        .CE(ap_NS_fsm[3]),
        .D(\axi_data_V1_reg_177[9]_i_1_n_0 ),
        .Q(axi_data_V1_reg_177[9]),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \axi_data_V_1_reg_232[0]_i_1 
       (.I0(axi_data_V_1_reg_232[0]),
        .I1(brmerge_reg_425),
        .I2(AXI_video_strm_V_data_V_0_data_out[0]),
        .I3(AXIvideo2Mat_U0_img_data_stream_2_V_write),
        .I4(axi_data_V1_reg_177[0]),
        .O(\axi_data_V_1_reg_232[0]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \axi_data_V_1_reg_232[10]_i_1 
       (.I0(axi_data_V_1_reg_232[10]),
        .I1(brmerge_reg_425),
        .I2(AXI_video_strm_V_data_V_0_data_out[10]),
        .I3(AXIvideo2Mat_U0_img_data_stream_2_V_write),
        .I4(axi_data_V1_reg_177[10]),
        .O(\axi_data_V_1_reg_232[10]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \axi_data_V_1_reg_232[11]_i_1 
       (.I0(axi_data_V_1_reg_232[11]),
        .I1(brmerge_reg_425),
        .I2(AXI_video_strm_V_data_V_0_data_out[11]),
        .I3(AXIvideo2Mat_U0_img_data_stream_2_V_write),
        .I4(axi_data_V1_reg_177[11]),
        .O(\axi_data_V_1_reg_232[11]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \axi_data_V_1_reg_232[12]_i_1 
       (.I0(axi_data_V_1_reg_232[12]),
        .I1(brmerge_reg_425),
        .I2(AXI_video_strm_V_data_V_0_data_out[12]),
        .I3(AXIvideo2Mat_U0_img_data_stream_2_V_write),
        .I4(axi_data_V1_reg_177[12]),
        .O(\axi_data_V_1_reg_232[12]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \axi_data_V_1_reg_232[13]_i_1 
       (.I0(axi_data_V_1_reg_232[13]),
        .I1(brmerge_reg_425),
        .I2(AXI_video_strm_V_data_V_0_data_out[13]),
        .I3(AXIvideo2Mat_U0_img_data_stream_2_V_write),
        .I4(axi_data_V1_reg_177[13]),
        .O(\axi_data_V_1_reg_232[13]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \axi_data_V_1_reg_232[14]_i_1 
       (.I0(axi_data_V_1_reg_232[14]),
        .I1(brmerge_reg_425),
        .I2(AXI_video_strm_V_data_V_0_data_out[14]),
        .I3(AXIvideo2Mat_U0_img_data_stream_2_V_write),
        .I4(axi_data_V1_reg_177[14]),
        .O(\axi_data_V_1_reg_232[14]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \axi_data_V_1_reg_232[15]_i_1 
       (.I0(axi_data_V_1_reg_232[15]),
        .I1(brmerge_reg_425),
        .I2(AXI_video_strm_V_data_V_0_data_out[15]),
        .I3(AXIvideo2Mat_U0_img_data_stream_2_V_write),
        .I4(axi_data_V1_reg_177[15]),
        .O(\axi_data_V_1_reg_232[15]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \axi_data_V_1_reg_232[16]_i_1 
       (.I0(axi_data_V_1_reg_232[16]),
        .I1(brmerge_reg_425),
        .I2(AXI_video_strm_V_data_V_0_data_out[16]),
        .I3(AXIvideo2Mat_U0_img_data_stream_2_V_write),
        .I4(axi_data_V1_reg_177[16]),
        .O(\axi_data_V_1_reg_232[16]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \axi_data_V_1_reg_232[17]_i_1 
       (.I0(axi_data_V_1_reg_232[17]),
        .I1(brmerge_reg_425),
        .I2(AXI_video_strm_V_data_V_0_data_out[17]),
        .I3(AXIvideo2Mat_U0_img_data_stream_2_V_write),
        .I4(axi_data_V1_reg_177[17]),
        .O(\axi_data_V_1_reg_232[17]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \axi_data_V_1_reg_232[18]_i_1 
       (.I0(axi_data_V_1_reg_232[18]),
        .I1(brmerge_reg_425),
        .I2(AXI_video_strm_V_data_V_0_data_out[18]),
        .I3(AXIvideo2Mat_U0_img_data_stream_2_V_write),
        .I4(axi_data_V1_reg_177[18]),
        .O(\axi_data_V_1_reg_232[18]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \axi_data_V_1_reg_232[19]_i_1 
       (.I0(axi_data_V_1_reg_232[19]),
        .I1(brmerge_reg_425),
        .I2(AXI_video_strm_V_data_V_0_data_out[19]),
        .I3(AXIvideo2Mat_U0_img_data_stream_2_V_write),
        .I4(axi_data_V1_reg_177[19]),
        .O(\axi_data_V_1_reg_232[19]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \axi_data_V_1_reg_232[1]_i_1 
       (.I0(axi_data_V_1_reg_232[1]),
        .I1(brmerge_reg_425),
        .I2(AXI_video_strm_V_data_V_0_data_out[1]),
        .I3(AXIvideo2Mat_U0_img_data_stream_2_V_write),
        .I4(axi_data_V1_reg_177[1]),
        .O(\axi_data_V_1_reg_232[1]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \axi_data_V_1_reg_232[20]_i_1 
       (.I0(axi_data_V_1_reg_232[20]),
        .I1(brmerge_reg_425),
        .I2(AXI_video_strm_V_data_V_0_data_out[20]),
        .I3(AXIvideo2Mat_U0_img_data_stream_2_V_write),
        .I4(axi_data_V1_reg_177[20]),
        .O(\axi_data_V_1_reg_232[20]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \axi_data_V_1_reg_232[21]_i_1 
       (.I0(axi_data_V_1_reg_232[21]),
        .I1(brmerge_reg_425),
        .I2(AXI_video_strm_V_data_V_0_data_out[21]),
        .I3(AXIvideo2Mat_U0_img_data_stream_2_V_write),
        .I4(axi_data_V1_reg_177[21]),
        .O(\axi_data_V_1_reg_232[21]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \axi_data_V_1_reg_232[22]_i_1 
       (.I0(axi_data_V_1_reg_232[22]),
        .I1(brmerge_reg_425),
        .I2(AXI_video_strm_V_data_V_0_data_out[22]),
        .I3(AXIvideo2Mat_U0_img_data_stream_2_V_write),
        .I4(axi_data_V1_reg_177[22]),
        .O(\axi_data_V_1_reg_232[22]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \axi_data_V_1_reg_232[23]_i_1 
       (.I0(axi_data_V_1_reg_232[23]),
        .I1(brmerge_reg_425),
        .I2(AXI_video_strm_V_data_V_0_data_out[23]),
        .I3(AXIvideo2Mat_U0_img_data_stream_2_V_write),
        .I4(axi_data_V1_reg_177[23]),
        .O(\axi_data_V_1_reg_232[23]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \axi_data_V_1_reg_232[2]_i_1 
       (.I0(axi_data_V_1_reg_232[2]),
        .I1(brmerge_reg_425),
        .I2(AXI_video_strm_V_data_V_0_data_out[2]),
        .I3(AXIvideo2Mat_U0_img_data_stream_2_V_write),
        .I4(axi_data_V1_reg_177[2]),
        .O(\axi_data_V_1_reg_232[2]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \axi_data_V_1_reg_232[3]_i_1 
       (.I0(axi_data_V_1_reg_232[3]),
        .I1(brmerge_reg_425),
        .I2(AXI_video_strm_V_data_V_0_data_out[3]),
        .I3(AXIvideo2Mat_U0_img_data_stream_2_V_write),
        .I4(axi_data_V1_reg_177[3]),
        .O(\axi_data_V_1_reg_232[3]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \axi_data_V_1_reg_232[4]_i_1 
       (.I0(axi_data_V_1_reg_232[4]),
        .I1(brmerge_reg_425),
        .I2(AXI_video_strm_V_data_V_0_data_out[4]),
        .I3(AXIvideo2Mat_U0_img_data_stream_2_V_write),
        .I4(axi_data_V1_reg_177[4]),
        .O(\axi_data_V_1_reg_232[4]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \axi_data_V_1_reg_232[5]_i_1 
       (.I0(axi_data_V_1_reg_232[5]),
        .I1(brmerge_reg_425),
        .I2(AXI_video_strm_V_data_V_0_data_out[5]),
        .I3(AXIvideo2Mat_U0_img_data_stream_2_V_write),
        .I4(axi_data_V1_reg_177[5]),
        .O(\axi_data_V_1_reg_232[5]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \axi_data_V_1_reg_232[6]_i_1 
       (.I0(axi_data_V_1_reg_232[6]),
        .I1(brmerge_reg_425),
        .I2(AXI_video_strm_V_data_V_0_data_out[6]),
        .I3(AXIvideo2Mat_U0_img_data_stream_2_V_write),
        .I4(axi_data_V1_reg_177[6]),
        .O(\axi_data_V_1_reg_232[6]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \axi_data_V_1_reg_232[7]_i_1 
       (.I0(axi_data_V_1_reg_232[7]),
        .I1(brmerge_reg_425),
        .I2(AXI_video_strm_V_data_V_0_data_out[7]),
        .I3(AXIvideo2Mat_U0_img_data_stream_2_V_write),
        .I4(axi_data_V1_reg_177[7]),
        .O(\axi_data_V_1_reg_232[7]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \axi_data_V_1_reg_232[8]_i_1 
       (.I0(axi_data_V_1_reg_232[8]),
        .I1(brmerge_reg_425),
        .I2(AXI_video_strm_V_data_V_0_data_out[8]),
        .I3(AXIvideo2Mat_U0_img_data_stream_2_V_write),
        .I4(axi_data_V1_reg_177[8]),
        .O(\axi_data_V_1_reg_232[8]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \axi_data_V_1_reg_232[9]_i_1 
       (.I0(axi_data_V_1_reg_232[9]),
        .I1(brmerge_reg_425),
        .I2(AXI_video_strm_V_data_V_0_data_out[9]),
        .I3(AXIvideo2Mat_U0_img_data_stream_2_V_write),
        .I4(axi_data_V1_reg_177[9]),
        .O(\axi_data_V_1_reg_232[9]_i_1_n_0 ));
  FDRE \axi_data_V_1_reg_232_reg[0] 
       (.C(ap_clk),
        .CE(eol_reg_209),
        .D(\axi_data_V_1_reg_232[0]_i_1_n_0 ),
        .Q(axi_data_V_1_reg_232[0]),
        .R(1'b0));
  FDRE \axi_data_V_1_reg_232_reg[10] 
       (.C(ap_clk),
        .CE(eol_reg_209),
        .D(\axi_data_V_1_reg_232[10]_i_1_n_0 ),
        .Q(axi_data_V_1_reg_232[10]),
        .R(1'b0));
  FDRE \axi_data_V_1_reg_232_reg[11] 
       (.C(ap_clk),
        .CE(eol_reg_209),
        .D(\axi_data_V_1_reg_232[11]_i_1_n_0 ),
        .Q(axi_data_V_1_reg_232[11]),
        .R(1'b0));
  FDRE \axi_data_V_1_reg_232_reg[12] 
       (.C(ap_clk),
        .CE(eol_reg_209),
        .D(\axi_data_V_1_reg_232[12]_i_1_n_0 ),
        .Q(axi_data_V_1_reg_232[12]),
        .R(1'b0));
  FDRE \axi_data_V_1_reg_232_reg[13] 
       (.C(ap_clk),
        .CE(eol_reg_209),
        .D(\axi_data_V_1_reg_232[13]_i_1_n_0 ),
        .Q(axi_data_V_1_reg_232[13]),
        .R(1'b0));
  FDRE \axi_data_V_1_reg_232_reg[14] 
       (.C(ap_clk),
        .CE(eol_reg_209),
        .D(\axi_data_V_1_reg_232[14]_i_1_n_0 ),
        .Q(axi_data_V_1_reg_232[14]),
        .R(1'b0));
  FDRE \axi_data_V_1_reg_232_reg[15] 
       (.C(ap_clk),
        .CE(eol_reg_209),
        .D(\axi_data_V_1_reg_232[15]_i_1_n_0 ),
        .Q(axi_data_V_1_reg_232[15]),
        .R(1'b0));
  FDRE \axi_data_V_1_reg_232_reg[16] 
       (.C(ap_clk),
        .CE(eol_reg_209),
        .D(\axi_data_V_1_reg_232[16]_i_1_n_0 ),
        .Q(axi_data_V_1_reg_232[16]),
        .R(1'b0));
  FDRE \axi_data_V_1_reg_232_reg[17] 
       (.C(ap_clk),
        .CE(eol_reg_209),
        .D(\axi_data_V_1_reg_232[17]_i_1_n_0 ),
        .Q(axi_data_V_1_reg_232[17]),
        .R(1'b0));
  FDRE \axi_data_V_1_reg_232_reg[18] 
       (.C(ap_clk),
        .CE(eol_reg_209),
        .D(\axi_data_V_1_reg_232[18]_i_1_n_0 ),
        .Q(axi_data_V_1_reg_232[18]),
        .R(1'b0));
  FDRE \axi_data_V_1_reg_232_reg[19] 
       (.C(ap_clk),
        .CE(eol_reg_209),
        .D(\axi_data_V_1_reg_232[19]_i_1_n_0 ),
        .Q(axi_data_V_1_reg_232[19]),
        .R(1'b0));
  FDRE \axi_data_V_1_reg_232_reg[1] 
       (.C(ap_clk),
        .CE(eol_reg_209),
        .D(\axi_data_V_1_reg_232[1]_i_1_n_0 ),
        .Q(axi_data_V_1_reg_232[1]),
        .R(1'b0));
  FDRE \axi_data_V_1_reg_232_reg[20] 
       (.C(ap_clk),
        .CE(eol_reg_209),
        .D(\axi_data_V_1_reg_232[20]_i_1_n_0 ),
        .Q(axi_data_V_1_reg_232[20]),
        .R(1'b0));
  FDRE \axi_data_V_1_reg_232_reg[21] 
       (.C(ap_clk),
        .CE(eol_reg_209),
        .D(\axi_data_V_1_reg_232[21]_i_1_n_0 ),
        .Q(axi_data_V_1_reg_232[21]),
        .R(1'b0));
  FDRE \axi_data_V_1_reg_232_reg[22] 
       (.C(ap_clk),
        .CE(eol_reg_209),
        .D(\axi_data_V_1_reg_232[22]_i_1_n_0 ),
        .Q(axi_data_V_1_reg_232[22]),
        .R(1'b0));
  FDRE \axi_data_V_1_reg_232_reg[23] 
       (.C(ap_clk),
        .CE(eol_reg_209),
        .D(\axi_data_V_1_reg_232[23]_i_1_n_0 ),
        .Q(axi_data_V_1_reg_232[23]),
        .R(1'b0));
  FDRE \axi_data_V_1_reg_232_reg[2] 
       (.C(ap_clk),
        .CE(eol_reg_209),
        .D(\axi_data_V_1_reg_232[2]_i_1_n_0 ),
        .Q(axi_data_V_1_reg_232[2]),
        .R(1'b0));
  FDRE \axi_data_V_1_reg_232_reg[3] 
       (.C(ap_clk),
        .CE(eol_reg_209),
        .D(\axi_data_V_1_reg_232[3]_i_1_n_0 ),
        .Q(axi_data_V_1_reg_232[3]),
        .R(1'b0));
  FDRE \axi_data_V_1_reg_232_reg[4] 
       (.C(ap_clk),
        .CE(eol_reg_209),
        .D(\axi_data_V_1_reg_232[4]_i_1_n_0 ),
        .Q(axi_data_V_1_reg_232[4]),
        .R(1'b0));
  FDRE \axi_data_V_1_reg_232_reg[5] 
       (.C(ap_clk),
        .CE(eol_reg_209),
        .D(\axi_data_V_1_reg_232[5]_i_1_n_0 ),
        .Q(axi_data_V_1_reg_232[5]),
        .R(1'b0));
  FDRE \axi_data_V_1_reg_232_reg[6] 
       (.C(ap_clk),
        .CE(eol_reg_209),
        .D(\axi_data_V_1_reg_232[6]_i_1_n_0 ),
        .Q(axi_data_V_1_reg_232[6]),
        .R(1'b0));
  FDRE \axi_data_V_1_reg_232_reg[7] 
       (.C(ap_clk),
        .CE(eol_reg_209),
        .D(\axi_data_V_1_reg_232[7]_i_1_n_0 ),
        .Q(axi_data_V_1_reg_232[7]),
        .R(1'b0));
  FDRE \axi_data_V_1_reg_232_reg[8] 
       (.C(ap_clk),
        .CE(eol_reg_209),
        .D(\axi_data_V_1_reg_232[8]_i_1_n_0 ),
        .Q(axi_data_V_1_reg_232[8]),
        .R(1'b0));
  FDRE \axi_data_V_1_reg_232_reg[9] 
       (.C(ap_clk),
        .CE(eol_reg_209),
        .D(\axi_data_V_1_reg_232[9]_i_1_n_0 ),
        .Q(axi_data_V_1_reg_232[9]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair31" *) 
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_data_V_3_reg_291[0]_i_1 
       (.I0(axi_data_V_1_reg_232[0]),
        .I1(ap_CS_fsm_state7),
        .I2(AXI_video_strm_V_data_V_0_payload_B[0]),
        .I3(AXI_video_strm_V_data_V_0_sel),
        .I4(AXI_video_strm_V_data_V_0_payload_A[0]),
        .O(\axi_data_V_3_reg_291[0]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_data_V_3_reg_291[10]_i_1 
       (.I0(axi_data_V_1_reg_232[10]),
        .I1(ap_CS_fsm_state7),
        .I2(AXI_video_strm_V_data_V_0_payload_B[10]),
        .I3(AXI_video_strm_V_data_V_0_sel),
        .I4(AXI_video_strm_V_data_V_0_payload_A[10]),
        .O(\axi_data_V_3_reg_291[10]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_data_V_3_reg_291[11]_i_1 
       (.I0(axi_data_V_1_reg_232[11]),
        .I1(ap_CS_fsm_state7),
        .I2(AXI_video_strm_V_data_V_0_payload_B[11]),
        .I3(AXI_video_strm_V_data_V_0_sel),
        .I4(AXI_video_strm_V_data_V_0_payload_A[11]),
        .O(\axi_data_V_3_reg_291[11]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_data_V_3_reg_291[12]_i_1 
       (.I0(axi_data_V_1_reg_232[12]),
        .I1(ap_CS_fsm_state7),
        .I2(AXI_video_strm_V_data_V_0_payload_B[12]),
        .I3(AXI_video_strm_V_data_V_0_sel),
        .I4(AXI_video_strm_V_data_V_0_payload_A[12]),
        .O(\axi_data_V_3_reg_291[12]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_data_V_3_reg_291[13]_i_1 
       (.I0(axi_data_V_1_reg_232[13]),
        .I1(ap_CS_fsm_state7),
        .I2(AXI_video_strm_V_data_V_0_payload_B[13]),
        .I3(AXI_video_strm_V_data_V_0_sel),
        .I4(AXI_video_strm_V_data_V_0_payload_A[13]),
        .O(\axi_data_V_3_reg_291[13]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_data_V_3_reg_291[14]_i_1 
       (.I0(axi_data_V_1_reg_232[14]),
        .I1(ap_CS_fsm_state7),
        .I2(AXI_video_strm_V_data_V_0_payload_B[14]),
        .I3(AXI_video_strm_V_data_V_0_sel),
        .I4(AXI_video_strm_V_data_V_0_payload_A[14]),
        .O(\axi_data_V_3_reg_291[14]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_data_V_3_reg_291[15]_i_1 
       (.I0(axi_data_V_1_reg_232[15]),
        .I1(ap_CS_fsm_state7),
        .I2(AXI_video_strm_V_data_V_0_payload_B[15]),
        .I3(AXI_video_strm_V_data_V_0_sel),
        .I4(AXI_video_strm_V_data_V_0_payload_A[15]),
        .O(\axi_data_V_3_reg_291[15]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_data_V_3_reg_291[16]_i_1 
       (.I0(axi_data_V_1_reg_232[16]),
        .I1(ap_CS_fsm_state7),
        .I2(AXI_video_strm_V_data_V_0_payload_B[16]),
        .I3(AXI_video_strm_V_data_V_0_sel),
        .I4(AXI_video_strm_V_data_V_0_payload_A[16]),
        .O(\axi_data_V_3_reg_291[16]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_data_V_3_reg_291[17]_i_1 
       (.I0(axi_data_V_1_reg_232[17]),
        .I1(ap_CS_fsm_state7),
        .I2(AXI_video_strm_V_data_V_0_payload_B[17]),
        .I3(AXI_video_strm_V_data_V_0_sel),
        .I4(AXI_video_strm_V_data_V_0_payload_A[17]),
        .O(\axi_data_V_3_reg_291[17]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_data_V_3_reg_291[18]_i_1 
       (.I0(axi_data_V_1_reg_232[18]),
        .I1(ap_CS_fsm_state7),
        .I2(AXI_video_strm_V_data_V_0_payload_B[18]),
        .I3(AXI_video_strm_V_data_V_0_sel),
        .I4(AXI_video_strm_V_data_V_0_payload_A[18]),
        .O(\axi_data_V_3_reg_291[18]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair33" *) 
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_data_V_3_reg_291[19]_i_1 
       (.I0(axi_data_V_1_reg_232[19]),
        .I1(ap_CS_fsm_state7),
        .I2(AXI_video_strm_V_data_V_0_payload_B[19]),
        .I3(AXI_video_strm_V_data_V_0_sel),
        .I4(AXI_video_strm_V_data_V_0_payload_A[19]),
        .O(\axi_data_V_3_reg_291[19]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_data_V_3_reg_291[1]_i_1 
       (.I0(axi_data_V_1_reg_232[1]),
        .I1(ap_CS_fsm_state7),
        .I2(AXI_video_strm_V_data_V_0_payload_B[1]),
        .I3(AXI_video_strm_V_data_V_0_sel),
        .I4(AXI_video_strm_V_data_V_0_payload_A[1]),
        .O(\axi_data_V_3_reg_291[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_data_V_3_reg_291[20]_i_1 
       (.I0(axi_data_V_1_reg_232[20]),
        .I1(ap_CS_fsm_state7),
        .I2(AXI_video_strm_V_data_V_0_payload_B[20]),
        .I3(AXI_video_strm_V_data_V_0_sel),
        .I4(AXI_video_strm_V_data_V_0_payload_A[20]),
        .O(\axi_data_V_3_reg_291[20]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_data_V_3_reg_291[21]_i_1 
       (.I0(axi_data_V_1_reg_232[21]),
        .I1(ap_CS_fsm_state7),
        .I2(AXI_video_strm_V_data_V_0_payload_B[21]),
        .I3(AXI_video_strm_V_data_V_0_sel),
        .I4(AXI_video_strm_V_data_V_0_payload_A[21]),
        .O(\axi_data_V_3_reg_291[21]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_data_V_3_reg_291[22]_i_1 
       (.I0(axi_data_V_1_reg_232[22]),
        .I1(ap_CS_fsm_state7),
        .I2(AXI_video_strm_V_data_V_0_payload_B[22]),
        .I3(AXI_video_strm_V_data_V_0_sel),
        .I4(AXI_video_strm_V_data_V_0_payload_A[22]),
        .O(\axi_data_V_3_reg_291[22]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_data_V_3_reg_291[23]_i_1 
       (.I0(axi_data_V_1_reg_232[23]),
        .I1(ap_CS_fsm_state7),
        .I2(AXI_video_strm_V_data_V_0_payload_B[23]),
        .I3(AXI_video_strm_V_data_V_0_sel),
        .I4(AXI_video_strm_V_data_V_0_payload_A[23]),
        .O(\axi_data_V_3_reg_291[23]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_data_V_3_reg_291[2]_i_1 
       (.I0(axi_data_V_1_reg_232[2]),
        .I1(ap_CS_fsm_state7),
        .I2(AXI_video_strm_V_data_V_0_payload_B[2]),
        .I3(AXI_video_strm_V_data_V_0_sel),
        .I4(AXI_video_strm_V_data_V_0_payload_A[2]),
        .O(\axi_data_V_3_reg_291[2]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_data_V_3_reg_291[3]_i_1 
       (.I0(axi_data_V_1_reg_232[3]),
        .I1(ap_CS_fsm_state7),
        .I2(AXI_video_strm_V_data_V_0_payload_B[3]),
        .I3(AXI_video_strm_V_data_V_0_sel),
        .I4(AXI_video_strm_V_data_V_0_payload_A[3]),
        .O(\axi_data_V_3_reg_291[3]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_data_V_3_reg_291[4]_i_1 
       (.I0(axi_data_V_1_reg_232[4]),
        .I1(ap_CS_fsm_state7),
        .I2(AXI_video_strm_V_data_V_0_payload_B[4]),
        .I3(AXI_video_strm_V_data_V_0_sel),
        .I4(AXI_video_strm_V_data_V_0_payload_A[4]),
        .O(\axi_data_V_3_reg_291[4]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_data_V_3_reg_291[5]_i_1 
       (.I0(axi_data_V_1_reg_232[5]),
        .I1(ap_CS_fsm_state7),
        .I2(AXI_video_strm_V_data_V_0_payload_B[5]),
        .I3(AXI_video_strm_V_data_V_0_sel),
        .I4(AXI_video_strm_V_data_V_0_payload_A[5]),
        .O(\axi_data_V_3_reg_291[5]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_data_V_3_reg_291[6]_i_1 
       (.I0(axi_data_V_1_reg_232[6]),
        .I1(ap_CS_fsm_state7),
        .I2(AXI_video_strm_V_data_V_0_payload_B[6]),
        .I3(AXI_video_strm_V_data_V_0_sel),
        .I4(AXI_video_strm_V_data_V_0_payload_A[6]),
        .O(\axi_data_V_3_reg_291[6]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_data_V_3_reg_291[7]_i_1 
       (.I0(axi_data_V_1_reg_232[7]),
        .I1(ap_CS_fsm_state7),
        .I2(AXI_video_strm_V_data_V_0_payload_B[7]),
        .I3(AXI_video_strm_V_data_V_0_sel),
        .I4(AXI_video_strm_V_data_V_0_payload_A[7]),
        .O(\axi_data_V_3_reg_291[7]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_data_V_3_reg_291[8]_i_1 
       (.I0(axi_data_V_1_reg_232[8]),
        .I1(ap_CS_fsm_state7),
        .I2(AXI_video_strm_V_data_V_0_payload_B[8]),
        .I3(AXI_video_strm_V_data_V_0_sel),
        .I4(AXI_video_strm_V_data_V_0_payload_A[8]),
        .O(\axi_data_V_3_reg_291[8]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_data_V_3_reg_291[9]_i_1 
       (.I0(axi_data_V_1_reg_232[9]),
        .I1(ap_CS_fsm_state7),
        .I2(AXI_video_strm_V_data_V_0_payload_B[9]),
        .I3(AXI_video_strm_V_data_V_0_sel),
        .I4(AXI_video_strm_V_data_V_0_payload_A[9]),
        .O(\axi_data_V_3_reg_291[9]_i_1_n_0 ));
  FDRE \axi_data_V_3_reg_291_reg[0] 
       (.C(ap_clk),
        .CE(\eol_2_reg_268[0]_i_1_n_0 ),
        .D(\axi_data_V_3_reg_291[0]_i_1_n_0 ),
        .Q(axi_data_V_3_reg_291[0]),
        .R(1'b0));
  FDRE \axi_data_V_3_reg_291_reg[10] 
       (.C(ap_clk),
        .CE(\eol_2_reg_268[0]_i_1_n_0 ),
        .D(\axi_data_V_3_reg_291[10]_i_1_n_0 ),
        .Q(axi_data_V_3_reg_291[10]),
        .R(1'b0));
  FDRE \axi_data_V_3_reg_291_reg[11] 
       (.C(ap_clk),
        .CE(\eol_2_reg_268[0]_i_1_n_0 ),
        .D(\axi_data_V_3_reg_291[11]_i_1_n_0 ),
        .Q(axi_data_V_3_reg_291[11]),
        .R(1'b0));
  FDRE \axi_data_V_3_reg_291_reg[12] 
       (.C(ap_clk),
        .CE(\eol_2_reg_268[0]_i_1_n_0 ),
        .D(\axi_data_V_3_reg_291[12]_i_1_n_0 ),
        .Q(axi_data_V_3_reg_291[12]),
        .R(1'b0));
  FDRE \axi_data_V_3_reg_291_reg[13] 
       (.C(ap_clk),
        .CE(\eol_2_reg_268[0]_i_1_n_0 ),
        .D(\axi_data_V_3_reg_291[13]_i_1_n_0 ),
        .Q(axi_data_V_3_reg_291[13]),
        .R(1'b0));
  FDRE \axi_data_V_3_reg_291_reg[14] 
       (.C(ap_clk),
        .CE(\eol_2_reg_268[0]_i_1_n_0 ),
        .D(\axi_data_V_3_reg_291[14]_i_1_n_0 ),
        .Q(axi_data_V_3_reg_291[14]),
        .R(1'b0));
  FDRE \axi_data_V_3_reg_291_reg[15] 
       (.C(ap_clk),
        .CE(\eol_2_reg_268[0]_i_1_n_0 ),
        .D(\axi_data_V_3_reg_291[15]_i_1_n_0 ),
        .Q(axi_data_V_3_reg_291[15]),
        .R(1'b0));
  FDRE \axi_data_V_3_reg_291_reg[16] 
       (.C(ap_clk),
        .CE(\eol_2_reg_268[0]_i_1_n_0 ),
        .D(\axi_data_V_3_reg_291[16]_i_1_n_0 ),
        .Q(axi_data_V_3_reg_291[16]),
        .R(1'b0));
  FDRE \axi_data_V_3_reg_291_reg[17] 
       (.C(ap_clk),
        .CE(\eol_2_reg_268[0]_i_1_n_0 ),
        .D(\axi_data_V_3_reg_291[17]_i_1_n_0 ),
        .Q(axi_data_V_3_reg_291[17]),
        .R(1'b0));
  FDRE \axi_data_V_3_reg_291_reg[18] 
       (.C(ap_clk),
        .CE(\eol_2_reg_268[0]_i_1_n_0 ),
        .D(\axi_data_V_3_reg_291[18]_i_1_n_0 ),
        .Q(axi_data_V_3_reg_291[18]),
        .R(1'b0));
  FDRE \axi_data_V_3_reg_291_reg[19] 
       (.C(ap_clk),
        .CE(\eol_2_reg_268[0]_i_1_n_0 ),
        .D(\axi_data_V_3_reg_291[19]_i_1_n_0 ),
        .Q(axi_data_V_3_reg_291[19]),
        .R(1'b0));
  FDRE \axi_data_V_3_reg_291_reg[1] 
       (.C(ap_clk),
        .CE(\eol_2_reg_268[0]_i_1_n_0 ),
        .D(\axi_data_V_3_reg_291[1]_i_1_n_0 ),
        .Q(axi_data_V_3_reg_291[1]),
        .R(1'b0));
  FDRE \axi_data_V_3_reg_291_reg[20] 
       (.C(ap_clk),
        .CE(\eol_2_reg_268[0]_i_1_n_0 ),
        .D(\axi_data_V_3_reg_291[20]_i_1_n_0 ),
        .Q(axi_data_V_3_reg_291[20]),
        .R(1'b0));
  FDRE \axi_data_V_3_reg_291_reg[21] 
       (.C(ap_clk),
        .CE(\eol_2_reg_268[0]_i_1_n_0 ),
        .D(\axi_data_V_3_reg_291[21]_i_1_n_0 ),
        .Q(axi_data_V_3_reg_291[21]),
        .R(1'b0));
  FDRE \axi_data_V_3_reg_291_reg[22] 
       (.C(ap_clk),
        .CE(\eol_2_reg_268[0]_i_1_n_0 ),
        .D(\axi_data_V_3_reg_291[22]_i_1_n_0 ),
        .Q(axi_data_V_3_reg_291[22]),
        .R(1'b0));
  FDRE \axi_data_V_3_reg_291_reg[23] 
       (.C(ap_clk),
        .CE(\eol_2_reg_268[0]_i_1_n_0 ),
        .D(\axi_data_V_3_reg_291[23]_i_1_n_0 ),
        .Q(axi_data_V_3_reg_291[23]),
        .R(1'b0));
  FDRE \axi_data_V_3_reg_291_reg[2] 
       (.C(ap_clk),
        .CE(\eol_2_reg_268[0]_i_1_n_0 ),
        .D(\axi_data_V_3_reg_291[2]_i_1_n_0 ),
        .Q(axi_data_V_3_reg_291[2]),
        .R(1'b0));
  FDRE \axi_data_V_3_reg_291_reg[3] 
       (.C(ap_clk),
        .CE(\eol_2_reg_268[0]_i_1_n_0 ),
        .D(\axi_data_V_3_reg_291[3]_i_1_n_0 ),
        .Q(axi_data_V_3_reg_291[3]),
        .R(1'b0));
  FDRE \axi_data_V_3_reg_291_reg[4] 
       (.C(ap_clk),
        .CE(\eol_2_reg_268[0]_i_1_n_0 ),
        .D(\axi_data_V_3_reg_291[4]_i_1_n_0 ),
        .Q(axi_data_V_3_reg_291[4]),
        .R(1'b0));
  FDRE \axi_data_V_3_reg_291_reg[5] 
       (.C(ap_clk),
        .CE(\eol_2_reg_268[0]_i_1_n_0 ),
        .D(\axi_data_V_3_reg_291[5]_i_1_n_0 ),
        .Q(axi_data_V_3_reg_291[5]),
        .R(1'b0));
  FDRE \axi_data_V_3_reg_291_reg[6] 
       (.C(ap_clk),
        .CE(\eol_2_reg_268[0]_i_1_n_0 ),
        .D(\axi_data_V_3_reg_291[6]_i_1_n_0 ),
        .Q(axi_data_V_3_reg_291[6]),
        .R(1'b0));
  FDRE \axi_data_V_3_reg_291_reg[7] 
       (.C(ap_clk),
        .CE(\eol_2_reg_268[0]_i_1_n_0 ),
        .D(\axi_data_V_3_reg_291[7]_i_1_n_0 ),
        .Q(axi_data_V_3_reg_291[7]),
        .R(1'b0));
  FDRE \axi_data_V_3_reg_291_reg[8] 
       (.C(ap_clk),
        .CE(\eol_2_reg_268[0]_i_1_n_0 ),
        .D(\axi_data_V_3_reg_291[8]_i_1_n_0 ),
        .Q(axi_data_V_3_reg_291[8]),
        .R(1'b0));
  FDRE \axi_data_V_3_reg_291_reg[9] 
       (.C(ap_clk),
        .CE(\eol_2_reg_268[0]_i_1_n_0 ),
        .D(\axi_data_V_3_reg_291[9]_i_1_n_0 ),
        .Q(axi_data_V_3_reg_291[9]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair42" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_last_V1_reg_167[0]_i_1 
       (.I0(tmp_last_V_reg_395),
        .I1(ap_CS_fsm_state3),
        .I2(axi_last_V_3_reg_279),
        .O(\axi_last_V1_reg_167[0]_i_1_n_0 ));
  FDRE \axi_last_V1_reg_167_reg[0] 
       (.C(ap_clk),
        .CE(ap_NS_fsm[3]),
        .D(\axi_last_V1_reg_167[0]_i_1_n_0 ),
        .Q(axi_last_V1_reg_167),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_last_V_3_reg_279[0]_i_1 
       (.I0(eol_1_reg_221),
        .I1(ap_CS_fsm_state7),
        .I2(AXI_video_strm_V_last_V_0_payload_B),
        .I3(AXI_video_strm_V_last_V_0_sel),
        .I4(AXI_video_strm_V_last_V_0_payload_A),
        .O(\axi_last_V_3_reg_279[0]_i_1_n_0 ));
  FDRE \axi_last_V_3_reg_279_reg[0] 
       (.C(ap_clk),
        .CE(\eol_2_reg_268[0]_i_1_n_0 ),
        .D(\axi_last_V_3_reg_279[0]_i_1_n_0 ),
        .Q(axi_last_V_3_reg_279),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair35" *) 
  LUT4 #(
    .INIT(16'hFB08)) 
    \brmerge_reg_425[0]_i_1 
       (.I0(brmerge_fu_349_p2),
        .I1(exitcond_reg_4160),
        .I2(exitcond_fu_334_p2),
        .I3(brmerge_reg_425),
        .O(\brmerge_reg_425[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hEFEEEEEEEAEEEEEE)) 
    \brmerge_reg_425[0]_i_2 
       (.I0(sof_1_fu_124),
        .I1(\eol_reg_209_reg_n_0_[0] ),
        .I2(\exitcond_reg_416_reg_n_0_[0] ),
        .I3(ap_CS_fsm_pp1_stage0),
        .I4(ap_enable_reg_pp1_iter1_reg_n_0),
        .I5(ap_phi_mux_axi_last_V_2_phi_fu_248_p4__0),
        .O(brmerge_fu_349_p2));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \brmerge_reg_425[0]_i_3 
       (.I0(eol_1_reg_221),
        .I1(brmerge_reg_425),
        .I2(AXI_video_strm_V_last_V_0_payload_B),
        .I3(AXI_video_strm_V_last_V_0_sel),
        .I4(AXI_video_strm_V_last_V_0_payload_A),
        .O(ap_phi_mux_axi_last_V_2_phi_fu_248_p4__0));
  FDRE \brmerge_reg_425_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\brmerge_reg_425[0]_i_1_n_0 ),
        .Q(brmerge_reg_425),
        .R(1'b0));
  LUT3 #(
    .INIT(8'hF2)) 
    \eol_1_reg_221[0]_i_1 
       (.I0(ap_CS_fsm_state4),
        .I1(exitcond3_fu_322_p2__14),
        .I2(AXIvideo2Mat_U0_img_data_stream_2_V_write),
        .O(eol_reg_209));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \eol_1_reg_221[0]_i_2 
       (.I0(eol_1_reg_221),
        .I1(brmerge_reg_425),
        .I2(AXI_video_strm_V_last_V_0_data_out),
        .I3(AXIvideo2Mat_U0_img_data_stream_2_V_write),
        .I4(axi_last_V1_reg_167),
        .O(\eol_1_reg_221[0]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair38" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \eol_1_reg_221[0]_i_3 
       (.I0(\exitcond_reg_416_reg_n_0_[0] ),
        .I1(ap_enable_reg_pp1_iter1_reg_n_0),
        .I2(exitcond_reg_4160),
        .O(AXIvideo2Mat_U0_img_data_stream_2_V_write));
  FDRE \eol_1_reg_221_reg[0] 
       (.C(ap_clk),
        .CE(eol_reg_209),
        .D(\eol_1_reg_221[0]_i_2_n_0 ),
        .Q(eol_1_reg_221),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hBAAAAAAA)) 
    \eol_2_reg_268[0]_i_1 
       (.I0(ap_CS_fsm_state7),
        .I1(\eol_2_reg_268_reg_n_0_[0] ),
        .I2(ap_enable_reg_pp2_iter1_reg_n_0),
        .I3(\AXI_video_strm_V_data_V_0_state_reg_n_0_[0] ),
        .I4(ap_CS_fsm_pp2_stage0),
        .O(\eol_2_reg_268[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \eol_2_reg_268[0]_i_2 
       (.I0(\eol_reg_209_reg_n_0_[0] ),
        .I1(ap_CS_fsm_state7),
        .I2(AXI_video_strm_V_last_V_0_payload_B),
        .I3(AXI_video_strm_V_last_V_0_sel),
        .I4(AXI_video_strm_V_last_V_0_payload_A),
        .O(\eol_2_reg_268[0]_i_2_n_0 ));
  FDRE \eol_2_reg_268_reg[0] 
       (.C(ap_clk),
        .CE(\eol_2_reg_268[0]_i_1_n_0 ),
        .D(\eol_2_reg_268[0]_i_2_n_0 ),
        .Q(\eol_2_reg_268_reg_n_0_[0] ),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hFFE200E200000000)) 
    \eol_reg_209[0]_i_1 
       (.I0(AXI_video_strm_V_last_V_0_payload_A),
        .I1(AXI_video_strm_V_last_V_0_sel),
        .I2(AXI_video_strm_V_last_V_0_payload_B),
        .I3(brmerge_reg_425),
        .I4(eol_1_reg_221),
        .I5(AXIvideo2Mat_U0_img_data_stream_2_V_write),
        .O(\eol_reg_209[0]_i_1_n_0 ));
  FDRE \eol_reg_209_reg[0] 
       (.C(ap_clk),
        .CE(eol_reg_209),
        .D(\eol_reg_209[0]_i_1_n_0 ),
        .Q(\eol_reg_209_reg_n_0_[0] ),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair35" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \exitcond_reg_416[0]_i_1 
       (.I0(exitcond_fu_334_p2),
        .I1(exitcond_reg_4160),
        .I2(\exitcond_reg_416_reg_n_0_[0] ),
        .O(\exitcond_reg_416[0]_i_1_n_0 ));
  FDRE \exitcond_reg_416_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\exitcond_reg_416[0]_i_1_n_0 ),
        .Q(\exitcond_reg_416_reg_n_0_[0] ),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair56" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \i_V_reg_411[0]_i_1 
       (.I0(t_V_reg_187[0]),
        .O(i_V_fu_328_p2[0]));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \i_V_reg_411[10]_i_1 
       (.I0(t_V_reg_187[8]),
        .I1(t_V_reg_187[6]),
        .I2(\i_V_reg_411[10]_i_2_n_0 ),
        .I3(t_V_reg_187[7]),
        .I4(t_V_reg_187[9]),
        .I5(t_V_reg_187[10]),
        .O(i_V_fu_328_p2[10]));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \i_V_reg_411[10]_i_2 
       (.I0(t_V_reg_187[5]),
        .I1(t_V_reg_187[3]),
        .I2(t_V_reg_187[1]),
        .I3(t_V_reg_187[0]),
        .I4(t_V_reg_187[2]),
        .I5(t_V_reg_187[4]),
        .O(\i_V_reg_411[10]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair56" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \i_V_reg_411[1]_i_1 
       (.I0(t_V_reg_187[0]),
        .I1(t_V_reg_187[1]),
        .O(i_V_fu_328_p2[1]));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \i_V_reg_411[2]_i_1 
       (.I0(t_V_reg_187[0]),
        .I1(t_V_reg_187[1]),
        .I2(t_V_reg_187[2]),
        .O(i_V_fu_328_p2[2]));
  (* SOFT_HLUTNM = "soft_lutpair32" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \i_V_reg_411[3]_i_1 
       (.I0(t_V_reg_187[1]),
        .I1(t_V_reg_187[0]),
        .I2(t_V_reg_187[2]),
        .I3(t_V_reg_187[3]),
        .O(i_V_fu_328_p2[3]));
  (* SOFT_HLUTNM = "soft_lutpair32" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \i_V_reg_411[4]_i_1 
       (.I0(t_V_reg_187[2]),
        .I1(t_V_reg_187[0]),
        .I2(t_V_reg_187[1]),
        .I3(t_V_reg_187[3]),
        .I4(t_V_reg_187[4]),
        .O(i_V_fu_328_p2[4]));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \i_V_reg_411[5]_i_1 
       (.I0(t_V_reg_187[3]),
        .I1(t_V_reg_187[1]),
        .I2(t_V_reg_187[0]),
        .I3(t_V_reg_187[2]),
        .I4(t_V_reg_187[4]),
        .I5(t_V_reg_187[5]),
        .O(i_V_fu_328_p2[5]));
  (* SOFT_HLUTNM = "soft_lutpair39" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \i_V_reg_411[6]_i_1 
       (.I0(\i_V_reg_411[10]_i_2_n_0 ),
        .I1(t_V_reg_187[6]),
        .O(i_V_fu_328_p2[6]));
  LUT3 #(
    .INIT(8'h78)) 
    \i_V_reg_411[7]_i_1 
       (.I0(\i_V_reg_411[10]_i_2_n_0 ),
        .I1(t_V_reg_187[6]),
        .I2(t_V_reg_187[7]),
        .O(i_V_fu_328_p2[7]));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \i_V_reg_411[8]_i_1 
       (.I0(t_V_reg_187[6]),
        .I1(\i_V_reg_411[10]_i_2_n_0 ),
        .I2(t_V_reg_187[7]),
        .I3(t_V_reg_187[8]),
        .O(i_V_fu_328_p2[8]));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \i_V_reg_411[9]_i_1 
       (.I0(t_V_reg_187[7]),
        .I1(\i_V_reg_411[10]_i_2_n_0 ),
        .I2(t_V_reg_187[6]),
        .I3(t_V_reg_187[8]),
        .I4(t_V_reg_187[9]),
        .O(i_V_fu_328_p2[9]));
  FDRE \i_V_reg_411_reg[0] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(i_V_fu_328_p2[0]),
        .Q(i_V_reg_411[0]),
        .R(1'b0));
  FDRE \i_V_reg_411_reg[10] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(i_V_fu_328_p2[10]),
        .Q(i_V_reg_411[10]),
        .R(1'b0));
  FDRE \i_V_reg_411_reg[1] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(i_V_fu_328_p2[1]),
        .Q(i_V_reg_411[1]),
        .R(1'b0));
  FDRE \i_V_reg_411_reg[2] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(i_V_fu_328_p2[2]),
        .Q(i_V_reg_411[2]),
        .R(1'b0));
  FDRE \i_V_reg_411_reg[3] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(i_V_fu_328_p2[3]),
        .Q(i_V_reg_411[3]),
        .R(1'b0));
  FDRE \i_V_reg_411_reg[4] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(i_V_fu_328_p2[4]),
        .Q(i_V_reg_411[4]),
        .R(1'b0));
  FDRE \i_V_reg_411_reg[5] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(i_V_fu_328_p2[5]),
        .Q(i_V_reg_411[5]),
        .R(1'b0));
  FDRE \i_V_reg_411_reg[6] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(i_V_fu_328_p2[6]),
        .Q(i_V_reg_411[6]),
        .R(1'b0));
  FDRE \i_V_reg_411_reg[7] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(i_V_fu_328_p2[7]),
        .Q(i_V_reg_411[7]),
        .R(1'b0));
  FDRE \i_V_reg_411_reg[8] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(i_V_fu_328_p2[8]),
        .Q(i_V_reg_411[8]),
        .R(1'b0));
  FDRE \i_V_reg_411_reg[9] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(i_V_fu_328_p2[9]),
        .Q(i_V_reg_411[9]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair41" *) 
  LUT4 #(
    .INIT(16'hF7FF)) 
    internal_empty_n_i_2
       (.I0(exitcond_reg_4160),
        .I1(ap_enable_reg_pp1_iter1_reg_n_0),
        .I2(\exitcond_reg_416_reg_n_0_[0] ),
        .I3(input_data_data_stre_2_full_n),
        .O(internal_empty_n_reg));
  LUT4 #(
    .INIT(16'hF7FF)) 
    internal_empty_n_i_2__0
       (.I0(exitcond_reg_4160),
        .I1(ap_enable_reg_pp1_iter1_reg_n_0),
        .I2(\exitcond_reg_416_reg_n_0_[0] ),
        .I3(input_data_data_stre_1_full_n),
        .O(internal_empty_n_reg_0));
  (* SOFT_HLUTNM = "soft_lutpair41" *) 
  LUT4 #(
    .INIT(16'hF7FF)) 
    internal_empty_n_i_2__1
       (.I0(exitcond_reg_4160),
        .I1(ap_enable_reg_pp1_iter1_reg_n_0),
        .I2(\exitcond_reg_416_reg_n_0_[0] ),
        .I3(input_data_data_stre_full_n),
        .O(internal_empty_n_reg_1));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT3 #(
    .INIT(8'hDF)) 
    \mOutPtr[1]_i_2 
       (.I0(ap_start),
        .I1(start_once_reg),
        .I2(start_for_Loop_loop_height_pro_U0_full_n),
        .O(\mOutPtr_reg[1] ));
  LUT5 #(
    .INIT(32'hF7F7F700)) 
    \sof_1_fu_124[0]_i_1 
       (.I0(ap_enable_reg_pp1_iter0),
        .I1(exitcond_reg_4160),
        .I2(exitcond_fu_334_p2),
        .I3(sof_1_fu_124),
        .I4(ap_CS_fsm_state3),
        .O(\sof_1_fu_124[0]_i_1_n_0 ));
  FDRE \sof_1_fu_124_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\sof_1_fu_124[0]_i_1_n_0 ),
        .Q(sof_1_fu_124),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT5 #(
    .INIT(32'h77707700)) 
    start_once_reg_i_1__0
       (.I0(ap_CS_fsm_state4),
        .I1(exitcond3_fu_322_p2__14),
        .I2(ap_start),
        .I3(start_once_reg),
        .I4(start_for_Loop_loop_height_pro_U0_full_n),
        .O(start_once_reg_i_1__0_n_0));
  FDRE #(
    .INIT(1'b0)) 
    start_once_reg_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(start_once_reg_i_1__0_n_0),
        .Q(start_once_reg),
        .R(ap_rst));
  (* SOFT_HLUTNM = "soft_lutpair55" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \t_V_2_reg_198[0]_i_1 
       (.I0(t_V_2_reg_198_reg__0[0]),
        .O(j_V_fu_340_p2[0]));
  LUT5 #(
    .INIT(32'h0000F700)) 
    \t_V_2_reg_198[10]_i_1 
       (.I0(ap_enable_reg_pp1_iter0),
        .I1(exitcond_reg_4160),
        .I2(exitcond_fu_334_p2),
        .I3(ap_CS_fsm_state4),
        .I4(exitcond3_fu_322_p2__14),
        .O(t_V_2_reg_198));
  LUT3 #(
    .INIT(8'h08)) 
    \t_V_2_reg_198[10]_i_2 
       (.I0(ap_enable_reg_pp1_iter0),
        .I1(exitcond_reg_4160),
        .I2(exitcond_fu_334_p2),
        .O(sof_1_fu_1240));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \t_V_2_reg_198[10]_i_3 
       (.I0(t_V_2_reg_198_reg__0[8]),
        .I1(t_V_2_reg_198_reg__0[6]),
        .I2(\t_V_2_reg_198[10]_i_5_n_0 ),
        .I3(t_V_2_reg_198_reg__0[7]),
        .I4(t_V_2_reg_198_reg__0[9]),
        .I5(t_V_2_reg_198_reg__0[10]),
        .O(j_V_fu_340_p2[10]));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'h00000008)) 
    \t_V_2_reg_198[10]_i_4 
       (.I0(\t_V_2_reg_198[10]_i_6_n_0 ),
        .I1(\t_V_2_reg_198[10]_i_7_n_0 ),
        .I2(t_V_2_reg_198_reg__0[0]),
        .I3(t_V_2_reg_198_reg__0[1]),
        .I4(t_V_2_reg_198_reg__0[2]),
        .O(exitcond_fu_334_p2));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \t_V_2_reg_198[10]_i_5 
       (.I0(t_V_2_reg_198_reg__0[5]),
        .I1(t_V_2_reg_198_reg__0[3]),
        .I2(t_V_2_reg_198_reg__0[1]),
        .I3(t_V_2_reg_198_reg__0[0]),
        .I4(t_V_2_reg_198_reg__0[2]),
        .I5(t_V_2_reg_198_reg__0[4]),
        .O(\t_V_2_reg_198[10]_i_5_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair40" *) 
  LUT4 #(
    .INIT(16'h0001)) 
    \t_V_2_reg_198[10]_i_6 
       (.I0(t_V_2_reg_198_reg__0[6]),
        .I1(t_V_2_reg_198_reg__0[5]),
        .I2(t_V_2_reg_198_reg__0[4]),
        .I3(t_V_2_reg_198_reg__0[3]),
        .O(\t_V_2_reg_198[10]_i_6_n_0 ));
  LUT4 #(
    .INIT(16'h8000)) 
    \t_V_2_reg_198[10]_i_7 
       (.I0(t_V_2_reg_198_reg__0[10]),
        .I1(t_V_2_reg_198_reg__0[9]),
        .I2(t_V_2_reg_198_reg__0[8]),
        .I3(t_V_2_reg_198_reg__0[7]),
        .O(\t_V_2_reg_198[10]_i_7_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair55" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \t_V_2_reg_198[1]_i_1 
       (.I0(t_V_2_reg_198_reg__0[0]),
        .I1(t_V_2_reg_198_reg__0[1]),
        .O(j_V_fu_340_p2[1]));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \t_V_2_reg_198[2]_i_1 
       (.I0(t_V_2_reg_198_reg__0[0]),
        .I1(t_V_2_reg_198_reg__0[1]),
        .I2(t_V_2_reg_198_reg__0[2]),
        .O(j_V_fu_340_p2[2]));
  (* SOFT_HLUTNM = "soft_lutpair30" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \t_V_2_reg_198[3]_i_1 
       (.I0(t_V_2_reg_198_reg__0[1]),
        .I1(t_V_2_reg_198_reg__0[0]),
        .I2(t_V_2_reg_198_reg__0[2]),
        .I3(t_V_2_reg_198_reg__0[3]),
        .O(j_V_fu_340_p2[3]));
  (* SOFT_HLUTNM = "soft_lutpair30" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \t_V_2_reg_198[4]_i_1 
       (.I0(t_V_2_reg_198_reg__0[2]),
        .I1(t_V_2_reg_198_reg__0[0]),
        .I2(t_V_2_reg_198_reg__0[1]),
        .I3(t_V_2_reg_198_reg__0[3]),
        .I4(t_V_2_reg_198_reg__0[4]),
        .O(j_V_fu_340_p2[4]));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \t_V_2_reg_198[5]_i_1 
       (.I0(t_V_2_reg_198_reg__0[3]),
        .I1(t_V_2_reg_198_reg__0[1]),
        .I2(t_V_2_reg_198_reg__0[0]),
        .I3(t_V_2_reg_198_reg__0[2]),
        .I4(t_V_2_reg_198_reg__0[4]),
        .I5(t_V_2_reg_198_reg__0[5]),
        .O(j_V_fu_340_p2[5]));
  (* SOFT_HLUTNM = "soft_lutpair40" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \t_V_2_reg_198[6]_i_1 
       (.I0(\t_V_2_reg_198[10]_i_5_n_0 ),
        .I1(t_V_2_reg_198_reg__0[6]),
        .O(j_V_fu_340_p2[6]));
  LUT3 #(
    .INIT(8'h78)) 
    \t_V_2_reg_198[7]_i_1 
       (.I0(\t_V_2_reg_198[10]_i_5_n_0 ),
        .I1(t_V_2_reg_198_reg__0[6]),
        .I2(t_V_2_reg_198_reg__0[7]),
        .O(j_V_fu_340_p2[7]));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \t_V_2_reg_198[8]_i_1 
       (.I0(t_V_2_reg_198_reg__0[6]),
        .I1(\t_V_2_reg_198[10]_i_5_n_0 ),
        .I2(t_V_2_reg_198_reg__0[7]),
        .I3(t_V_2_reg_198_reg__0[8]),
        .O(j_V_fu_340_p2[8]));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \t_V_2_reg_198[9]_i_1 
       (.I0(t_V_2_reg_198_reg__0[7]),
        .I1(\t_V_2_reg_198[10]_i_5_n_0 ),
        .I2(t_V_2_reg_198_reg__0[6]),
        .I3(t_V_2_reg_198_reg__0[8]),
        .I4(t_V_2_reg_198_reg__0[9]),
        .O(j_V_fu_340_p2[9]));
  FDRE \t_V_2_reg_198_reg[0] 
       (.C(ap_clk),
        .CE(sof_1_fu_1240),
        .D(j_V_fu_340_p2[0]),
        .Q(t_V_2_reg_198_reg__0[0]),
        .R(t_V_2_reg_198));
  FDRE \t_V_2_reg_198_reg[10] 
       (.C(ap_clk),
        .CE(sof_1_fu_1240),
        .D(j_V_fu_340_p2[10]),
        .Q(t_V_2_reg_198_reg__0[10]),
        .R(t_V_2_reg_198));
  FDRE \t_V_2_reg_198_reg[1] 
       (.C(ap_clk),
        .CE(sof_1_fu_1240),
        .D(j_V_fu_340_p2[1]),
        .Q(t_V_2_reg_198_reg__0[1]),
        .R(t_V_2_reg_198));
  FDRE \t_V_2_reg_198_reg[2] 
       (.C(ap_clk),
        .CE(sof_1_fu_1240),
        .D(j_V_fu_340_p2[2]),
        .Q(t_V_2_reg_198_reg__0[2]),
        .R(t_V_2_reg_198));
  FDRE \t_V_2_reg_198_reg[3] 
       (.C(ap_clk),
        .CE(sof_1_fu_1240),
        .D(j_V_fu_340_p2[3]),
        .Q(t_V_2_reg_198_reg__0[3]),
        .R(t_V_2_reg_198));
  FDRE \t_V_2_reg_198_reg[4] 
       (.C(ap_clk),
        .CE(sof_1_fu_1240),
        .D(j_V_fu_340_p2[4]),
        .Q(t_V_2_reg_198_reg__0[4]),
        .R(t_V_2_reg_198));
  FDRE \t_V_2_reg_198_reg[5] 
       (.C(ap_clk),
        .CE(sof_1_fu_1240),
        .D(j_V_fu_340_p2[5]),
        .Q(t_V_2_reg_198_reg__0[5]),
        .R(t_V_2_reg_198));
  FDRE \t_V_2_reg_198_reg[6] 
       (.C(ap_clk),
        .CE(sof_1_fu_1240),
        .D(j_V_fu_340_p2[6]),
        .Q(t_V_2_reg_198_reg__0[6]),
        .R(t_V_2_reg_198));
  FDRE \t_V_2_reg_198_reg[7] 
       (.C(ap_clk),
        .CE(sof_1_fu_1240),
        .D(j_V_fu_340_p2[7]),
        .Q(t_V_2_reg_198_reg__0[7]),
        .R(t_V_2_reg_198));
  FDRE \t_V_2_reg_198_reg[8] 
       (.C(ap_clk),
        .CE(sof_1_fu_1240),
        .D(j_V_fu_340_p2[8]),
        .Q(t_V_2_reg_198_reg__0[8]),
        .R(t_V_2_reg_198));
  FDRE \t_V_2_reg_198_reg[9] 
       (.C(ap_clk),
        .CE(sof_1_fu_1240),
        .D(j_V_fu_340_p2[9]),
        .Q(t_V_2_reg_198_reg__0[9]),
        .R(t_V_2_reg_198));
  FDRE \t_V_reg_187_reg[0] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state10),
        .D(i_V_reg_411[0]),
        .Q(t_V_reg_187[0]),
        .R(ap_CS_fsm_state3));
  FDRE \t_V_reg_187_reg[10] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state10),
        .D(i_V_reg_411[10]),
        .Q(t_V_reg_187[10]),
        .R(ap_CS_fsm_state3));
  FDRE \t_V_reg_187_reg[1] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state10),
        .D(i_V_reg_411[1]),
        .Q(t_V_reg_187[1]),
        .R(ap_CS_fsm_state3));
  FDRE \t_V_reg_187_reg[2] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state10),
        .D(i_V_reg_411[2]),
        .Q(t_V_reg_187[2]),
        .R(ap_CS_fsm_state3));
  FDRE \t_V_reg_187_reg[3] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state10),
        .D(i_V_reg_411[3]),
        .Q(t_V_reg_187[3]),
        .R(ap_CS_fsm_state3));
  FDRE \t_V_reg_187_reg[4] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state10),
        .D(i_V_reg_411[4]),
        .Q(t_V_reg_187[4]),
        .R(ap_CS_fsm_state3));
  FDRE \t_V_reg_187_reg[5] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state10),
        .D(i_V_reg_411[5]),
        .Q(t_V_reg_187[5]),
        .R(ap_CS_fsm_state3));
  FDRE \t_V_reg_187_reg[6] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state10),
        .D(i_V_reg_411[6]),
        .Q(t_V_reg_187[6]),
        .R(ap_CS_fsm_state3));
  FDRE \t_V_reg_187_reg[7] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state10),
        .D(i_V_reg_411[7]),
        .Q(t_V_reg_187[7]),
        .R(ap_CS_fsm_state3));
  FDRE \t_V_reg_187_reg[8] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state10),
        .D(i_V_reg_411[8]),
        .Q(t_V_reg_187[8]),
        .R(ap_CS_fsm_state3));
  FDRE \t_V_reg_187_reg[9] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state10),
        .D(i_V_reg_411[9]),
        .Q(t_V_reg_187[9]),
        .R(ap_CS_fsm_state3));
  (* SOFT_HLUTNM = "soft_lutpair31" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \tmp_data_V_reg_387[0]_i_1 
       (.I0(AXI_video_strm_V_data_V_0_payload_B[0]),
        .I1(AXI_video_strm_V_data_V_0_sel),
        .I2(AXI_video_strm_V_data_V_0_payload_A[0]),
        .O(AXI_video_strm_V_data_V_0_data_out[0]));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \tmp_data_V_reg_387[10]_i_1 
       (.I0(AXI_video_strm_V_data_V_0_payload_B[10]),
        .I1(AXI_video_strm_V_data_V_0_sel),
        .I2(AXI_video_strm_V_data_V_0_payload_A[10]),
        .O(AXI_video_strm_V_data_V_0_data_out[10]));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \tmp_data_V_reg_387[11]_i_1 
       (.I0(AXI_video_strm_V_data_V_0_payload_B[11]),
        .I1(AXI_video_strm_V_data_V_0_sel),
        .I2(AXI_video_strm_V_data_V_0_payload_A[11]),
        .O(AXI_video_strm_V_data_V_0_data_out[11]));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \tmp_data_V_reg_387[12]_i_1 
       (.I0(AXI_video_strm_V_data_V_0_payload_B[12]),
        .I1(AXI_video_strm_V_data_V_0_sel),
        .I2(AXI_video_strm_V_data_V_0_payload_A[12]),
        .O(AXI_video_strm_V_data_V_0_data_out[12]));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \tmp_data_V_reg_387[13]_i_1 
       (.I0(AXI_video_strm_V_data_V_0_payload_B[13]),
        .I1(AXI_video_strm_V_data_V_0_sel),
        .I2(AXI_video_strm_V_data_V_0_payload_A[13]),
        .O(AXI_video_strm_V_data_V_0_data_out[13]));
  (* SOFT_HLUTNM = "soft_lutpair28" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \tmp_data_V_reg_387[14]_i_1 
       (.I0(AXI_video_strm_V_data_V_0_payload_B[14]),
        .I1(AXI_video_strm_V_data_V_0_sel),
        .I2(AXI_video_strm_V_data_V_0_payload_A[14]),
        .O(AXI_video_strm_V_data_V_0_data_out[14]));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \tmp_data_V_reg_387[15]_i_1 
       (.I0(AXI_video_strm_V_data_V_0_payload_B[15]),
        .I1(AXI_video_strm_V_data_V_0_sel),
        .I2(AXI_video_strm_V_data_V_0_payload_A[15]),
        .O(AXI_video_strm_V_data_V_0_data_out[15]));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \tmp_data_V_reg_387[16]_i_1 
       (.I0(AXI_video_strm_V_data_V_0_payload_B[16]),
        .I1(AXI_video_strm_V_data_V_0_sel),
        .I2(AXI_video_strm_V_data_V_0_payload_A[16]),
        .O(AXI_video_strm_V_data_V_0_data_out[16]));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \tmp_data_V_reg_387[17]_i_1 
       (.I0(AXI_video_strm_V_data_V_0_payload_B[17]),
        .I1(AXI_video_strm_V_data_V_0_sel),
        .I2(AXI_video_strm_V_data_V_0_payload_A[17]),
        .O(AXI_video_strm_V_data_V_0_data_out[17]));
  (* SOFT_HLUTNM = "soft_lutpair29" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \tmp_data_V_reg_387[18]_i_1 
       (.I0(AXI_video_strm_V_data_V_0_payload_B[18]),
        .I1(AXI_video_strm_V_data_V_0_sel),
        .I2(AXI_video_strm_V_data_V_0_payload_A[18]),
        .O(AXI_video_strm_V_data_V_0_data_out[18]));
  (* SOFT_HLUTNM = "soft_lutpair33" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \tmp_data_V_reg_387[19]_i_1 
       (.I0(AXI_video_strm_V_data_V_0_payload_B[19]),
        .I1(AXI_video_strm_V_data_V_0_sel),
        .I2(AXI_video_strm_V_data_V_0_payload_A[19]),
        .O(AXI_video_strm_V_data_V_0_data_out[19]));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \tmp_data_V_reg_387[1]_i_1 
       (.I0(AXI_video_strm_V_data_V_0_payload_B[1]),
        .I1(AXI_video_strm_V_data_V_0_sel),
        .I2(AXI_video_strm_V_data_V_0_payload_A[1]),
        .O(AXI_video_strm_V_data_V_0_data_out[1]));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \tmp_data_V_reg_387[20]_i_1 
       (.I0(AXI_video_strm_V_data_V_0_payload_B[20]),
        .I1(AXI_video_strm_V_data_V_0_sel),
        .I2(AXI_video_strm_V_data_V_0_payload_A[20]),
        .O(AXI_video_strm_V_data_V_0_data_out[20]));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \tmp_data_V_reg_387[21]_i_1 
       (.I0(AXI_video_strm_V_data_V_0_payload_B[21]),
        .I1(AXI_video_strm_V_data_V_0_sel),
        .I2(AXI_video_strm_V_data_V_0_payload_A[21]),
        .O(AXI_video_strm_V_data_V_0_data_out[21]));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \tmp_data_V_reg_387[22]_i_1 
       (.I0(AXI_video_strm_V_data_V_0_payload_B[22]),
        .I1(AXI_video_strm_V_data_V_0_sel),
        .I2(AXI_video_strm_V_data_V_0_payload_A[22]),
        .O(AXI_video_strm_V_data_V_0_data_out[22]));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \tmp_data_V_reg_387[23]_i_1 
       (.I0(AXI_video_strm_V_data_V_0_payload_B[23]),
        .I1(AXI_video_strm_V_data_V_0_sel),
        .I2(AXI_video_strm_V_data_V_0_payload_A[23]),
        .O(AXI_video_strm_V_data_V_0_data_out[23]));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \tmp_data_V_reg_387[2]_i_1 
       (.I0(AXI_video_strm_V_data_V_0_payload_B[2]),
        .I1(AXI_video_strm_V_data_V_0_sel),
        .I2(AXI_video_strm_V_data_V_0_payload_A[2]),
        .O(AXI_video_strm_V_data_V_0_data_out[2]));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \tmp_data_V_reg_387[3]_i_1 
       (.I0(AXI_video_strm_V_data_V_0_payload_B[3]),
        .I1(AXI_video_strm_V_data_V_0_sel),
        .I2(AXI_video_strm_V_data_V_0_payload_A[3]),
        .O(AXI_video_strm_V_data_V_0_data_out[3]));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \tmp_data_V_reg_387[4]_i_1 
       (.I0(AXI_video_strm_V_data_V_0_payload_B[4]),
        .I1(AXI_video_strm_V_data_V_0_sel),
        .I2(AXI_video_strm_V_data_V_0_payload_A[4]),
        .O(AXI_video_strm_V_data_V_0_data_out[4]));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \tmp_data_V_reg_387[5]_i_1 
       (.I0(AXI_video_strm_V_data_V_0_payload_B[5]),
        .I1(AXI_video_strm_V_data_V_0_sel),
        .I2(AXI_video_strm_V_data_V_0_payload_A[5]),
        .O(AXI_video_strm_V_data_V_0_data_out[5]));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \tmp_data_V_reg_387[6]_i_1 
       (.I0(AXI_video_strm_V_data_V_0_payload_B[6]),
        .I1(AXI_video_strm_V_data_V_0_sel),
        .I2(AXI_video_strm_V_data_V_0_payload_A[6]),
        .O(AXI_video_strm_V_data_V_0_data_out[6]));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \tmp_data_V_reg_387[7]_i_1 
       (.I0(AXI_video_strm_V_data_V_0_payload_B[7]),
        .I1(AXI_video_strm_V_data_V_0_sel),
        .I2(AXI_video_strm_V_data_V_0_payload_A[7]),
        .O(AXI_video_strm_V_data_V_0_data_out[7]));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \tmp_data_V_reg_387[8]_i_1 
       (.I0(AXI_video_strm_V_data_V_0_payload_B[8]),
        .I1(AXI_video_strm_V_data_V_0_sel),
        .I2(AXI_video_strm_V_data_V_0_payload_A[8]),
        .O(AXI_video_strm_V_data_V_0_data_out[8]));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \tmp_data_V_reg_387[9]_i_1 
       (.I0(AXI_video_strm_V_data_V_0_payload_B[9]),
        .I1(AXI_video_strm_V_data_V_0_sel),
        .I2(AXI_video_strm_V_data_V_0_payload_A[9]),
        .O(AXI_video_strm_V_data_V_0_data_out[9]));
  FDRE \tmp_data_V_reg_387_reg[0] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_sel2),
        .D(AXI_video_strm_V_data_V_0_data_out[0]),
        .Q(tmp_data_V_reg_387[0]),
        .R(1'b0));
  FDRE \tmp_data_V_reg_387_reg[10] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_sel2),
        .D(AXI_video_strm_V_data_V_0_data_out[10]),
        .Q(tmp_data_V_reg_387[10]),
        .R(1'b0));
  FDRE \tmp_data_V_reg_387_reg[11] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_sel2),
        .D(AXI_video_strm_V_data_V_0_data_out[11]),
        .Q(tmp_data_V_reg_387[11]),
        .R(1'b0));
  FDRE \tmp_data_V_reg_387_reg[12] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_sel2),
        .D(AXI_video_strm_V_data_V_0_data_out[12]),
        .Q(tmp_data_V_reg_387[12]),
        .R(1'b0));
  FDRE \tmp_data_V_reg_387_reg[13] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_sel2),
        .D(AXI_video_strm_V_data_V_0_data_out[13]),
        .Q(tmp_data_V_reg_387[13]),
        .R(1'b0));
  FDRE \tmp_data_V_reg_387_reg[14] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_sel2),
        .D(AXI_video_strm_V_data_V_0_data_out[14]),
        .Q(tmp_data_V_reg_387[14]),
        .R(1'b0));
  FDRE \tmp_data_V_reg_387_reg[15] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_sel2),
        .D(AXI_video_strm_V_data_V_0_data_out[15]),
        .Q(tmp_data_V_reg_387[15]),
        .R(1'b0));
  FDRE \tmp_data_V_reg_387_reg[16] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_sel2),
        .D(AXI_video_strm_V_data_V_0_data_out[16]),
        .Q(tmp_data_V_reg_387[16]),
        .R(1'b0));
  FDRE \tmp_data_V_reg_387_reg[17] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_sel2),
        .D(AXI_video_strm_V_data_V_0_data_out[17]),
        .Q(tmp_data_V_reg_387[17]),
        .R(1'b0));
  FDRE \tmp_data_V_reg_387_reg[18] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_sel2),
        .D(AXI_video_strm_V_data_V_0_data_out[18]),
        .Q(tmp_data_V_reg_387[18]),
        .R(1'b0));
  FDRE \tmp_data_V_reg_387_reg[19] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_sel2),
        .D(AXI_video_strm_V_data_V_0_data_out[19]),
        .Q(tmp_data_V_reg_387[19]),
        .R(1'b0));
  FDRE \tmp_data_V_reg_387_reg[1] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_sel2),
        .D(AXI_video_strm_V_data_V_0_data_out[1]),
        .Q(tmp_data_V_reg_387[1]),
        .R(1'b0));
  FDRE \tmp_data_V_reg_387_reg[20] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_sel2),
        .D(AXI_video_strm_V_data_V_0_data_out[20]),
        .Q(tmp_data_V_reg_387[20]),
        .R(1'b0));
  FDRE \tmp_data_V_reg_387_reg[21] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_sel2),
        .D(AXI_video_strm_V_data_V_0_data_out[21]),
        .Q(tmp_data_V_reg_387[21]),
        .R(1'b0));
  FDRE \tmp_data_V_reg_387_reg[22] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_sel2),
        .D(AXI_video_strm_V_data_V_0_data_out[22]),
        .Q(tmp_data_V_reg_387[22]),
        .R(1'b0));
  FDRE \tmp_data_V_reg_387_reg[23] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_sel2),
        .D(AXI_video_strm_V_data_V_0_data_out[23]),
        .Q(tmp_data_V_reg_387[23]),
        .R(1'b0));
  FDRE \tmp_data_V_reg_387_reg[2] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_sel2),
        .D(AXI_video_strm_V_data_V_0_data_out[2]),
        .Q(tmp_data_V_reg_387[2]),
        .R(1'b0));
  FDRE \tmp_data_V_reg_387_reg[3] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_sel2),
        .D(AXI_video_strm_V_data_V_0_data_out[3]),
        .Q(tmp_data_V_reg_387[3]),
        .R(1'b0));
  FDRE \tmp_data_V_reg_387_reg[4] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_sel2),
        .D(AXI_video_strm_V_data_V_0_data_out[4]),
        .Q(tmp_data_V_reg_387[4]),
        .R(1'b0));
  FDRE \tmp_data_V_reg_387_reg[5] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_sel2),
        .D(AXI_video_strm_V_data_V_0_data_out[5]),
        .Q(tmp_data_V_reg_387[5]),
        .R(1'b0));
  FDRE \tmp_data_V_reg_387_reg[6] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_sel2),
        .D(AXI_video_strm_V_data_V_0_data_out[6]),
        .Q(tmp_data_V_reg_387[6]),
        .R(1'b0));
  FDRE \tmp_data_V_reg_387_reg[7] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_sel2),
        .D(AXI_video_strm_V_data_V_0_data_out[7]),
        .Q(tmp_data_V_reg_387[7]),
        .R(1'b0));
  FDRE \tmp_data_V_reg_387_reg[8] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_sel2),
        .D(AXI_video_strm_V_data_V_0_data_out[8]),
        .Q(tmp_data_V_reg_387[8]),
        .R(1'b0));
  FDRE \tmp_data_V_reg_387_reg[9] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_sel2),
        .D(AXI_video_strm_V_data_V_0_data_out[9]),
        .Q(tmp_data_V_reg_387[9]),
        .R(1'b0));
  LUT2 #(
    .INIT(4'h8)) 
    \tmp_last_V_reg_395[0]_i_1 
       (.I0(ap_CS_fsm_state2),
        .I1(\AXI_video_strm_V_data_V_0_state_reg_n_0_[0] ),
        .O(AXI_video_strm_V_data_V_0_sel2));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \tmp_last_V_reg_395[0]_i_2 
       (.I0(AXI_video_strm_V_last_V_0_payload_B),
        .I1(AXI_video_strm_V_last_V_0_sel),
        .I2(AXI_video_strm_V_last_V_0_payload_A),
        .O(AXI_video_strm_V_last_V_0_data_out));
  FDRE \tmp_last_V_reg_395_reg[0] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_sel2),
        .D(AXI_video_strm_V_last_V_0_data_out),
        .Q(tmp_last_V_reg_395),
        .R(1'b0));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Loop_loop_height_pro
   (start_once_reg_reg_0,
    Loop_loop_height_pro_U0_ap_ready,
    Q,
    ce,
    ce_0,
    ce_1,
    Loop_loop_height_pro_U0_passthrough_data_stream_2_V_write,
    ce_2,
    ce_3,
    ce_4,
    \SRL_SIG_reg[0][7] ,
    \SRL_SIG_reg[0][7]_0 ,
    \SRL_SIG_reg[0][7]_1 ,
    ap_rst,
    ap_clk,
    internal_full_n_reg,
    start_for_Mat2AXIvideo_U0_full_n,
    start_for_Mat2AXIvideo_1_U0_full_n,
    start_for_Loop_loop_height_pro_U0_empty_n,
    passthrough_data_str_2_full_n,
    passthrough_data_str_1_full_n,
    passthrough_data_str_full_n,
    internal_full_n_reg_0,
    input_data_data_stre_empty_n,
    crop_data_data_strea_full_n,
    crop_data_data_strea_1_full_n,
    crop_data_data_strea_2_full_n,
    internal_full_n_reg_1,
    internal_full_n_reg_2);
  output start_once_reg_reg_0;
  output Loop_loop_height_pro_U0_ap_ready;
  output [1:0]Q;
  output ce;
  output ce_0;
  output ce_1;
  output Loop_loop_height_pro_U0_passthrough_data_stream_2_V_write;
  output ce_2;
  output ce_3;
  output ce_4;
  output \SRL_SIG_reg[0][7] ;
  output \SRL_SIG_reg[0][7]_0 ;
  output \SRL_SIG_reg[0][7]_1 ;
  input ap_rst;
  input ap_clk;
  input internal_full_n_reg;
  input start_for_Mat2AXIvideo_U0_full_n;
  input start_for_Mat2AXIvideo_1_U0_full_n;
  input start_for_Loop_loop_height_pro_U0_empty_n;
  input passthrough_data_str_2_full_n;
  input passthrough_data_str_1_full_n;
  input passthrough_data_str_full_n;
  input internal_full_n_reg_0;
  input input_data_data_stre_empty_n;
  input crop_data_data_strea_full_n;
  input crop_data_data_strea_1_full_n;
  input crop_data_data_strea_2_full_n;
  input internal_full_n_reg_1;
  input internal_full_n_reg_2;

  wire Loop_loop_height_pro_U0_ap_ready;
  wire Loop_loop_height_pro_U0_crop_data_data_stream_2_V_write;
  wire Loop_loop_height_pro_U0_passthrough_data_stream_2_V_write;
  wire [1:0]Q;
  wire \SRL_SIG_reg[0][7] ;
  wire \SRL_SIG_reg[0][7]_0 ;
  wire \SRL_SIG_reg[0][7]_1 ;
  wire ap_CS_fsm_state2;
  wire ap_CS_fsm_state3;
  wire ap_CS_fsm_state5;
  wire [4:0]ap_NS_fsm;
  wire ap_NS_fsm1;
  wire ap_NS_fsm14_out;
  wire ap_NS_fsm16_out;
  wire ap_clk;
  wire ap_rst;
  wire ce;
  wire ce_0;
  wire ce_1;
  wire ce_2;
  wire ce_3;
  wire ce_4;
  wire crop_data_data_strea_1_full_n;
  wire crop_data_data_strea_2_full_n;
  wire crop_data_data_strea_full_n;
  wire [10:0]i_V_fu_303_p2;
  wire [10:0]i_V_reg_348;
  wire \i_V_reg_348[10]_i_2_n_0 ;
  wire input_data_data_stre_empty_n;
  wire internal_full_n_reg;
  wire internal_full_n_reg_0;
  wire internal_full_n_reg_1;
  wire internal_full_n_reg_2;
  wire [8:0]j_V_1_fu_339_p2;
  wire [10:0]j_V_2_fu_327_p2;
  wire [8:0]j_V_fu_315_p2;
  wire \p_070_1_reg_275[10]_i_10_n_0 ;
  wire \p_070_1_reg_275[10]_i_4_n_0 ;
  wire \p_070_1_reg_275[10]_i_7_n_0 ;
  wire \p_070_1_reg_275[10]_i_8_n_0 ;
  wire \p_070_1_reg_275[10]_i_9_n_0 ;
  wire [10:0]p_070_1_reg_275_reg__0;
  wire \p_070_2_reg_286[8]_i_2_n_0 ;
  wire \p_070_2_reg_286[8]_i_5_n_0 ;
  wire \p_070_2_reg_286[8]_i_6_n_0 ;
  wire [8:0]p_070_2_reg_286_reg__0;
  wire p_1_reg_264;
  wire p_1_reg_2640;
  wire \p_1_reg_264[8]_i_5_n_0 ;
  wire \p_1_reg_264[8]_i_6_n_0 ;
  wire \p_1_reg_264[8]_i_7_n_0 ;
  wire \p_1_reg_264[8]_i_8_n_0 ;
  wire [8:0]p_1_reg_264_reg__0;
  wire [10:0]p_s_reg_253;
  wire \p_s_reg_253[10]_i_3_n_0 ;
  wire p_s_reg_253_0;
  wire passthrough_data_str_1_full_n;
  wire passthrough_data_str_2_full_n;
  wire passthrough_data_str_full_n;
  wire start_for_Loop_loop_height_pro_U0_empty_n;
  wire start_for_Mat2AXIvideo_1_U0_full_n;
  wire start_for_Mat2AXIvideo_U0_full_n;
  wire start_once_reg_i_1_n_0;
  wire start_once_reg_reg_0;
  wire tmp_fu_297_p2__12;

  (* SOFT_HLUTNM = "soft_lutpair74" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \SRL_SIG[0][7]_i_1__2 
       (.I0(Loop_loop_height_pro_U0_crop_data_data_stream_2_V_write),
        .I1(crop_data_data_strea_full_n),
        .O(ce_2));
  (* SOFT_HLUTNM = "soft_lutpair78" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \SRL_SIG[0][7]_i_1__3 
       (.I0(Loop_loop_height_pro_U0_crop_data_data_stream_2_V_write),
        .I1(crop_data_data_strea_1_full_n),
        .O(ce_3));
  (* SOFT_HLUTNM = "soft_lutpair76" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \SRL_SIG[0][7]_i_1__4 
       (.I0(Loop_loop_height_pro_U0_crop_data_data_stream_2_V_write),
        .I1(crop_data_data_strea_2_full_n),
        .O(ce_4));
  (* SOFT_HLUTNM = "soft_lutpair74" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \SRL_SIG[0][7]_i_1__5 
       (.I0(ce),
        .I1(Loop_loop_height_pro_U0_crop_data_data_stream_2_V_write),
        .O(\SRL_SIG_reg[0][7] ));
  (* SOFT_HLUTNM = "soft_lutpair76" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \SRL_SIG[0][7]_i_1__6 
       (.I0(ce_0),
        .I1(Loop_loop_height_pro_U0_crop_data_data_stream_2_V_write),
        .O(\SRL_SIG_reg[0][7]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair78" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \SRL_SIG[0][7]_i_1__7 
       (.I0(ce_1),
        .I1(Loop_loop_height_pro_U0_crop_data_data_stream_2_V_write),
        .O(\SRL_SIG_reg[0][7]_1 ));
  (* SOFT_HLUTNM = "soft_lutpair67" *) 
  LUT4 #(
    .INIT(16'hFE00)) 
    \SRL_SIG[0][7]_i_2 
       (.I0(\p_070_2_reg_286[8]_i_2_n_0 ),
        .I1(Loop_loop_height_pro_U0_crop_data_data_stream_2_V_write),
        .I2(p_1_reg_2640),
        .I3(passthrough_data_str_2_full_n),
        .O(ce));
  (* SOFT_HLUTNM = "soft_lutpair68" *) 
  LUT4 #(
    .INIT(16'hFE00)) 
    \SRL_SIG[0][7]_i_2__0 
       (.I0(\p_070_2_reg_286[8]_i_2_n_0 ),
        .I1(Loop_loop_height_pro_U0_crop_data_data_stream_2_V_write),
        .I2(p_1_reg_2640),
        .I3(passthrough_data_str_1_full_n),
        .O(ce_0));
  (* SOFT_HLUTNM = "soft_lutpair67" *) 
  LUT4 #(
    .INIT(16'hFE00)) 
    \SRL_SIG[0][7]_i_2__1 
       (.I0(\p_070_2_reg_286[8]_i_2_n_0 ),
        .I1(Loop_loop_height_pro_U0_crop_data_data_stream_2_V_write),
        .I2(p_1_reg_2640),
        .I3(passthrough_data_str_full_n),
        .O(ce_1));
  LUT4 #(
    .INIT(16'h88F8)) 
    \ap_CS_fsm[0]_i_1 
       (.I0(Loop_loop_height_pro_U0_ap_ready),
        .I1(ap_CS_fsm_state2),
        .I2(Q[0]),
        .I3(internal_full_n_reg),
        .O(ap_NS_fsm[0]));
  LUT4 #(
    .INIT(16'hF888)) 
    \ap_CS_fsm[1]_i_1__0 
       (.I0(ap_NS_fsm1),
        .I1(ap_CS_fsm_state5),
        .I2(Q[0]),
        .I3(internal_full_n_reg),
        .O(ap_NS_fsm[1]));
  (* SOFT_HLUTNM = "soft_lutpair65" *) 
  LUT4 #(
    .INIT(16'h4F44)) 
    \ap_CS_fsm[2]_i_1__0 
       (.I0(ap_NS_fsm16_out),
        .I1(ap_CS_fsm_state3),
        .I2(Loop_loop_height_pro_U0_ap_ready),
        .I3(ap_CS_fsm_state2),
        .O(ap_NS_fsm[2]));
  (* SOFT_HLUTNM = "soft_lutpair65" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \ap_CS_fsm[2]_i_2 
       (.I0(tmp_fu_297_p2__12),
        .I1(ap_CS_fsm_state2),
        .O(Loop_loop_height_pro_U0_ap_ready));
  LUT4 #(
    .INIT(16'hF444)) 
    \ap_CS_fsm[3]_i_1 
       (.I0(ap_NS_fsm14_out),
        .I1(Q[1]),
        .I2(ap_NS_fsm16_out),
        .I3(ap_CS_fsm_state3),
        .O(ap_NS_fsm[3]));
  LUT4 #(
    .INIT(16'hF444)) 
    \ap_CS_fsm[4]_i_1 
       (.I0(ap_NS_fsm1),
        .I1(ap_CS_fsm_state5),
        .I2(ap_NS_fsm14_out),
        .I3(Q[1]),
        .O(ap_NS_fsm[4]));
  (* FSM_ENCODING = "none" *) 
  FDSE #(
    .INIT(1'b1)) 
    \ap_CS_fsm_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[0]),
        .Q(Q[0]),
        .S(ap_rst));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[1]),
        .Q(ap_CS_fsm_state2),
        .R(ap_rst));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[2] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[2]),
        .Q(ap_CS_fsm_state3),
        .R(ap_rst));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[3] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[3]),
        .Q(Q[1]),
        .R(ap_rst));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[4] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[4]),
        .Q(ap_CS_fsm_state5),
        .R(ap_rst));
  (* SOFT_HLUTNM = "soft_lutpair75" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \i_V_reg_348[0]_i_1 
       (.I0(p_s_reg_253[0]),
        .O(i_V_fu_303_p2[0]));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \i_V_reg_348[10]_i_1 
       (.I0(p_s_reg_253[8]),
        .I1(p_s_reg_253[6]),
        .I2(\i_V_reg_348[10]_i_2_n_0 ),
        .I3(p_s_reg_253[7]),
        .I4(p_s_reg_253[9]),
        .I5(p_s_reg_253[10]),
        .O(i_V_fu_303_p2[10]));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \i_V_reg_348[10]_i_2 
       (.I0(p_s_reg_253[5]),
        .I1(p_s_reg_253[3]),
        .I2(p_s_reg_253[1]),
        .I3(p_s_reg_253[0]),
        .I4(p_s_reg_253[2]),
        .I5(p_s_reg_253[4]),
        .O(\i_V_reg_348[10]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair75" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \i_V_reg_348[1]_i_1 
       (.I0(p_s_reg_253[0]),
        .I1(p_s_reg_253[1]),
        .O(i_V_fu_303_p2[1]));
  (* SOFT_HLUTNM = "soft_lutpair62" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \i_V_reg_348[2]_i_1 
       (.I0(p_s_reg_253[0]),
        .I1(p_s_reg_253[1]),
        .I2(p_s_reg_253[2]),
        .O(i_V_fu_303_p2[2]));
  (* SOFT_HLUTNM = "soft_lutpair58" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \i_V_reg_348[3]_i_1 
       (.I0(p_s_reg_253[1]),
        .I1(p_s_reg_253[0]),
        .I2(p_s_reg_253[2]),
        .I3(p_s_reg_253[3]),
        .O(i_V_fu_303_p2[3]));
  (* SOFT_HLUTNM = "soft_lutpair58" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \i_V_reg_348[4]_i_1 
       (.I0(p_s_reg_253[2]),
        .I1(p_s_reg_253[0]),
        .I2(p_s_reg_253[1]),
        .I3(p_s_reg_253[3]),
        .I4(p_s_reg_253[4]),
        .O(i_V_fu_303_p2[4]));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \i_V_reg_348[5]_i_1 
       (.I0(p_s_reg_253[3]),
        .I1(p_s_reg_253[1]),
        .I2(p_s_reg_253[0]),
        .I3(p_s_reg_253[2]),
        .I4(p_s_reg_253[4]),
        .I5(p_s_reg_253[5]),
        .O(i_V_fu_303_p2[5]));
  (* SOFT_HLUTNM = "soft_lutpair64" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \i_V_reg_348[6]_i_1 
       (.I0(\i_V_reg_348[10]_i_2_n_0 ),
        .I1(p_s_reg_253[6]),
        .O(i_V_fu_303_p2[6]));
  LUT3 #(
    .INIT(8'h78)) 
    \i_V_reg_348[7]_i_1 
       (.I0(\i_V_reg_348[10]_i_2_n_0 ),
        .I1(p_s_reg_253[6]),
        .I2(p_s_reg_253[7]),
        .O(i_V_fu_303_p2[7]));
  (* SOFT_HLUTNM = "soft_lutpair63" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \i_V_reg_348[8]_i_1 
       (.I0(p_s_reg_253[6]),
        .I1(\i_V_reg_348[10]_i_2_n_0 ),
        .I2(p_s_reg_253[7]),
        .I3(p_s_reg_253[8]),
        .O(i_V_fu_303_p2[8]));
  (* SOFT_HLUTNM = "soft_lutpair63" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \i_V_reg_348[9]_i_1 
       (.I0(p_s_reg_253[7]),
        .I1(\i_V_reg_348[10]_i_2_n_0 ),
        .I2(p_s_reg_253[6]),
        .I3(p_s_reg_253[8]),
        .I4(p_s_reg_253[9]),
        .O(i_V_fu_303_p2[9]));
  FDRE \i_V_reg_348_reg[0] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state2),
        .D(i_V_fu_303_p2[0]),
        .Q(i_V_reg_348[0]),
        .R(1'b0));
  FDRE \i_V_reg_348_reg[10] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state2),
        .D(i_V_fu_303_p2[10]),
        .Q(i_V_reg_348[10]),
        .R(1'b0));
  FDRE \i_V_reg_348_reg[1] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state2),
        .D(i_V_fu_303_p2[1]),
        .Q(i_V_reg_348[1]),
        .R(1'b0));
  FDRE \i_V_reg_348_reg[2] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state2),
        .D(i_V_fu_303_p2[2]),
        .Q(i_V_reg_348[2]),
        .R(1'b0));
  FDRE \i_V_reg_348_reg[3] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state2),
        .D(i_V_fu_303_p2[3]),
        .Q(i_V_reg_348[3]),
        .R(1'b0));
  FDRE \i_V_reg_348_reg[4] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state2),
        .D(i_V_fu_303_p2[4]),
        .Q(i_V_reg_348[4]),
        .R(1'b0));
  FDRE \i_V_reg_348_reg[5] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state2),
        .D(i_V_fu_303_p2[5]),
        .Q(i_V_reg_348[5]),
        .R(1'b0));
  FDRE \i_V_reg_348_reg[6] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state2),
        .D(i_V_fu_303_p2[6]),
        .Q(i_V_reg_348[6]),
        .R(1'b0));
  FDRE \i_V_reg_348_reg[7] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state2),
        .D(i_V_fu_303_p2[7]),
        .Q(i_V_reg_348[7]),
        .R(1'b0));
  FDRE \i_V_reg_348_reg[8] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state2),
        .D(i_V_fu_303_p2[8]),
        .Q(i_V_reg_348[8]),
        .R(1'b0));
  FDRE \i_V_reg_348_reg[9] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state2),
        .D(i_V_fu_303_p2[9]),
        .Q(i_V_reg_348[9]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair68" *) 
  LUT3 #(
    .INIT(8'hFE)) 
    \mOutPtr[1]_i_2__1 
       (.I0(p_1_reg_2640),
        .I1(Loop_loop_height_pro_U0_crop_data_data_stream_2_V_write),
        .I2(\p_070_2_reg_286[8]_i_2_n_0 ),
        .O(Loop_loop_height_pro_U0_passthrough_data_stream_2_V_write));
  (* SOFT_HLUTNM = "soft_lutpair80" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \p_070_1_reg_275[0]_i_1 
       (.I0(p_070_1_reg_275_reg__0[0]),
        .O(j_V_2_fu_327_p2[0]));
  LUT5 #(
    .INIT(32'h00200000)) 
    \p_070_1_reg_275[10]_i_1 
       (.I0(\p_070_1_reg_275[10]_i_4_n_0 ),
        .I1(p_1_reg_264_reg__0[0]),
        .I2(p_1_reg_264_reg__0[2]),
        .I3(p_1_reg_264_reg__0[1]),
        .I4(ap_CS_fsm_state3),
        .O(ap_NS_fsm16_out));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \p_070_1_reg_275[10]_i_10 
       (.I0(p_070_1_reg_275_reg__0[5]),
        .I1(p_070_1_reg_275_reg__0[3]),
        .I2(p_070_1_reg_275_reg__0[1]),
        .I3(p_070_1_reg_275_reg__0[0]),
        .I4(p_070_1_reg_275_reg__0[2]),
        .I5(p_070_1_reg_275_reg__0[4]),
        .O(\p_070_1_reg_275[10]_i_10_n_0 ));
  LUT5 #(
    .INIT(32'h08888888)) 
    \p_070_1_reg_275[10]_i_2 
       (.I0(internal_full_n_reg_1),
        .I1(internal_full_n_reg_2),
        .I2(\p_070_1_reg_275[10]_i_7_n_0 ),
        .I3(\p_070_1_reg_275[10]_i_8_n_0 ),
        .I4(\p_070_1_reg_275[10]_i_9_n_0 ),
        .O(Loop_loop_height_pro_U0_crop_data_data_stream_2_V_write));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \p_070_1_reg_275[10]_i_3 
       (.I0(p_070_1_reg_275_reg__0[8]),
        .I1(p_070_1_reg_275_reg__0[6]),
        .I2(\p_070_1_reg_275[10]_i_10_n_0 ),
        .I3(p_070_1_reg_275_reg__0[7]),
        .I4(p_070_1_reg_275_reg__0[9]),
        .I5(p_070_1_reg_275_reg__0[10]),
        .O(j_V_2_fu_327_p2[10]));
  LUT6 #(
    .INIT(64'h0010000000000000)) 
    \p_070_1_reg_275[10]_i_4 
       (.I0(p_1_reg_264_reg__0[3]),
        .I1(p_1_reg_264_reg__0[4]),
        .I2(p_1_reg_264_reg__0[5]),
        .I3(p_1_reg_264_reg__0[6]),
        .I4(p_1_reg_264_reg__0[8]),
        .I5(p_1_reg_264_reg__0[7]),
        .O(\p_070_1_reg_275[10]_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair72" *) 
  LUT3 #(
    .INIT(8'h01)) 
    \p_070_1_reg_275[10]_i_7 
       (.I0(p_070_1_reg_275_reg__0[2]),
        .I1(p_070_1_reg_275_reg__0[1]),
        .I2(p_070_1_reg_275_reg__0[0]),
        .O(\p_070_1_reg_275[10]_i_7_n_0 ));
  LUT4 #(
    .INIT(16'h0004)) 
    \p_070_1_reg_275[10]_i_8 
       (.I0(p_070_1_reg_275_reg__0[9]),
        .I1(p_070_1_reg_275_reg__0[10]),
        .I2(p_070_1_reg_275_reg__0[8]),
        .I3(p_070_1_reg_275_reg__0[7]),
        .O(\p_070_1_reg_275[10]_i_8_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair66" *) 
  LUT4 #(
    .INIT(16'h4000)) 
    \p_070_1_reg_275[10]_i_9 
       (.I0(p_070_1_reg_275_reg__0[6]),
        .I1(p_070_1_reg_275_reg__0[5]),
        .I2(p_070_1_reg_275_reg__0[4]),
        .I3(p_070_1_reg_275_reg__0[3]),
        .O(\p_070_1_reg_275[10]_i_9_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair80" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \p_070_1_reg_275[1]_i_1 
       (.I0(p_070_1_reg_275_reg__0[0]),
        .I1(p_070_1_reg_275_reg__0[1]),
        .O(j_V_2_fu_327_p2[1]));
  (* SOFT_HLUTNM = "soft_lutpair72" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \p_070_1_reg_275[2]_i_1 
       (.I0(p_070_1_reg_275_reg__0[0]),
        .I1(p_070_1_reg_275_reg__0[1]),
        .I2(p_070_1_reg_275_reg__0[2]),
        .O(j_V_2_fu_327_p2[2]));
  (* SOFT_HLUTNM = "soft_lutpair59" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \p_070_1_reg_275[3]_i_1 
       (.I0(p_070_1_reg_275_reg__0[1]),
        .I1(p_070_1_reg_275_reg__0[0]),
        .I2(p_070_1_reg_275_reg__0[2]),
        .I3(p_070_1_reg_275_reg__0[3]),
        .O(j_V_2_fu_327_p2[3]));
  (* SOFT_HLUTNM = "soft_lutpair59" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \p_070_1_reg_275[4]_i_1 
       (.I0(p_070_1_reg_275_reg__0[2]),
        .I1(p_070_1_reg_275_reg__0[0]),
        .I2(p_070_1_reg_275_reg__0[1]),
        .I3(p_070_1_reg_275_reg__0[3]),
        .I4(p_070_1_reg_275_reg__0[4]),
        .O(j_V_2_fu_327_p2[4]));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \p_070_1_reg_275[5]_i_1 
       (.I0(p_070_1_reg_275_reg__0[3]),
        .I1(p_070_1_reg_275_reg__0[1]),
        .I2(p_070_1_reg_275_reg__0[0]),
        .I3(p_070_1_reg_275_reg__0[2]),
        .I4(p_070_1_reg_275_reg__0[4]),
        .I5(p_070_1_reg_275_reg__0[5]),
        .O(j_V_2_fu_327_p2[5]));
  (* SOFT_HLUTNM = "soft_lutpair66" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \p_070_1_reg_275[6]_i_1 
       (.I0(\p_070_1_reg_275[10]_i_10_n_0 ),
        .I1(p_070_1_reg_275_reg__0[6]),
        .O(j_V_2_fu_327_p2[6]));
  LUT3 #(
    .INIT(8'h78)) 
    \p_070_1_reg_275[7]_i_1 
       (.I0(\p_070_1_reg_275[10]_i_10_n_0 ),
        .I1(p_070_1_reg_275_reg__0[6]),
        .I2(p_070_1_reg_275_reg__0[7]),
        .O(j_V_2_fu_327_p2[7]));
  (* SOFT_HLUTNM = "soft_lutpair61" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \p_070_1_reg_275[8]_i_1 
       (.I0(p_070_1_reg_275_reg__0[6]),
        .I1(\p_070_1_reg_275[10]_i_10_n_0 ),
        .I2(p_070_1_reg_275_reg__0[7]),
        .I3(p_070_1_reg_275_reg__0[8]),
        .O(j_V_2_fu_327_p2[8]));
  (* SOFT_HLUTNM = "soft_lutpair61" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \p_070_1_reg_275[9]_i_1 
       (.I0(p_070_1_reg_275_reg__0[7]),
        .I1(\p_070_1_reg_275[10]_i_10_n_0 ),
        .I2(p_070_1_reg_275_reg__0[6]),
        .I3(p_070_1_reg_275_reg__0[8]),
        .I4(p_070_1_reg_275_reg__0[9]),
        .O(j_V_2_fu_327_p2[9]));
  FDRE \p_070_1_reg_275_reg[0] 
       (.C(ap_clk),
        .CE(Loop_loop_height_pro_U0_crop_data_data_stream_2_V_write),
        .D(j_V_2_fu_327_p2[0]),
        .Q(p_070_1_reg_275_reg__0[0]),
        .R(ap_NS_fsm16_out));
  FDRE \p_070_1_reg_275_reg[10] 
       (.C(ap_clk),
        .CE(Loop_loop_height_pro_U0_crop_data_data_stream_2_V_write),
        .D(j_V_2_fu_327_p2[10]),
        .Q(p_070_1_reg_275_reg__0[10]),
        .R(ap_NS_fsm16_out));
  FDRE \p_070_1_reg_275_reg[1] 
       (.C(ap_clk),
        .CE(Loop_loop_height_pro_U0_crop_data_data_stream_2_V_write),
        .D(j_V_2_fu_327_p2[1]),
        .Q(p_070_1_reg_275_reg__0[1]),
        .R(ap_NS_fsm16_out));
  FDRE \p_070_1_reg_275_reg[2] 
       (.C(ap_clk),
        .CE(Loop_loop_height_pro_U0_crop_data_data_stream_2_V_write),
        .D(j_V_2_fu_327_p2[2]),
        .Q(p_070_1_reg_275_reg__0[2]),
        .R(ap_NS_fsm16_out));
  FDRE \p_070_1_reg_275_reg[3] 
       (.C(ap_clk),
        .CE(Loop_loop_height_pro_U0_crop_data_data_stream_2_V_write),
        .D(j_V_2_fu_327_p2[3]),
        .Q(p_070_1_reg_275_reg__0[3]),
        .R(ap_NS_fsm16_out));
  FDRE \p_070_1_reg_275_reg[4] 
       (.C(ap_clk),
        .CE(Loop_loop_height_pro_U0_crop_data_data_stream_2_V_write),
        .D(j_V_2_fu_327_p2[4]),
        .Q(p_070_1_reg_275_reg__0[4]),
        .R(ap_NS_fsm16_out));
  FDRE \p_070_1_reg_275_reg[5] 
       (.C(ap_clk),
        .CE(Loop_loop_height_pro_U0_crop_data_data_stream_2_V_write),
        .D(j_V_2_fu_327_p2[5]),
        .Q(p_070_1_reg_275_reg__0[5]),
        .R(ap_NS_fsm16_out));
  FDRE \p_070_1_reg_275_reg[6] 
       (.C(ap_clk),
        .CE(Loop_loop_height_pro_U0_crop_data_data_stream_2_V_write),
        .D(j_V_2_fu_327_p2[6]),
        .Q(p_070_1_reg_275_reg__0[6]),
        .R(ap_NS_fsm16_out));
  FDRE \p_070_1_reg_275_reg[7] 
       (.C(ap_clk),
        .CE(Loop_loop_height_pro_U0_crop_data_data_stream_2_V_write),
        .D(j_V_2_fu_327_p2[7]),
        .Q(p_070_1_reg_275_reg__0[7]),
        .R(ap_NS_fsm16_out));
  FDRE \p_070_1_reg_275_reg[8] 
       (.C(ap_clk),
        .CE(Loop_loop_height_pro_U0_crop_data_data_stream_2_V_write),
        .D(j_V_2_fu_327_p2[8]),
        .Q(p_070_1_reg_275_reg__0[8]),
        .R(ap_NS_fsm16_out));
  FDRE \p_070_1_reg_275_reg[9] 
       (.C(ap_clk),
        .CE(Loop_loop_height_pro_U0_crop_data_data_stream_2_V_write),
        .D(j_V_2_fu_327_p2[9]),
        .Q(p_070_1_reg_275_reg__0[9]),
        .R(ap_NS_fsm16_out));
  (* SOFT_HLUTNM = "soft_lutpair77" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \p_070_2_reg_286[0]_i_1 
       (.I0(p_070_2_reg_286_reg__0[0]),
        .O(j_V_1_fu_339_p2[0]));
  (* SOFT_HLUTNM = "soft_lutpair77" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \p_070_2_reg_286[1]_i_1 
       (.I0(p_070_2_reg_286_reg__0[0]),
        .I1(p_070_2_reg_286_reg__0[1]),
        .O(j_V_1_fu_339_p2[1]));
  (* SOFT_HLUTNM = "soft_lutpair71" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \p_070_2_reg_286[2]_i_1 
       (.I0(p_070_2_reg_286_reg__0[0]),
        .I1(p_070_2_reg_286_reg__0[1]),
        .I2(p_070_2_reg_286_reg__0[2]),
        .O(j_V_1_fu_339_p2[2]));
  (* SOFT_HLUTNM = "soft_lutpair57" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \p_070_2_reg_286[3]_i_1 
       (.I0(p_070_2_reg_286_reg__0[1]),
        .I1(p_070_2_reg_286_reg__0[0]),
        .I2(p_070_2_reg_286_reg__0[2]),
        .I3(p_070_2_reg_286_reg__0[3]),
        .O(j_V_1_fu_339_p2[3]));
  (* SOFT_HLUTNM = "soft_lutpair57" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \p_070_2_reg_286[4]_i_1 
       (.I0(p_070_2_reg_286_reg__0[2]),
        .I1(p_070_2_reg_286_reg__0[0]),
        .I2(p_070_2_reg_286_reg__0[1]),
        .I3(p_070_2_reg_286_reg__0[3]),
        .I4(p_070_2_reg_286_reg__0[4]),
        .O(j_V_1_fu_339_p2[4]));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \p_070_2_reg_286[5]_i_1 
       (.I0(p_070_2_reg_286_reg__0[3]),
        .I1(p_070_2_reg_286_reg__0[1]),
        .I2(p_070_2_reg_286_reg__0[0]),
        .I3(p_070_2_reg_286_reg__0[2]),
        .I4(p_070_2_reg_286_reg__0[4]),
        .I5(p_070_2_reg_286_reg__0[5]),
        .O(j_V_1_fu_339_p2[5]));
  LUT2 #(
    .INIT(4'h6)) 
    \p_070_2_reg_286[6]_i_1 
       (.I0(\p_070_2_reg_286[8]_i_6_n_0 ),
        .I1(p_070_2_reg_286_reg__0[6]),
        .O(j_V_1_fu_339_p2[6]));
  (* SOFT_HLUTNM = "soft_lutpair69" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \p_070_2_reg_286[7]_i_1 
       (.I0(\p_070_2_reg_286[8]_i_6_n_0 ),
        .I1(p_070_2_reg_286_reg__0[6]),
        .I2(p_070_2_reg_286_reg__0[7]),
        .O(j_V_1_fu_339_p2[7]));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \p_070_2_reg_286[8]_i_1 
       (.I0(\p_070_1_reg_275[10]_i_9_n_0 ),
        .I1(\p_070_1_reg_275[10]_i_8_n_0 ),
        .I2(p_070_1_reg_275_reg__0[0]),
        .I3(p_070_1_reg_275_reg__0[1]),
        .I4(p_070_1_reg_275_reg__0[2]),
        .I5(Q[1]),
        .O(ap_NS_fsm14_out));
  LUT6 #(
    .INIT(64'h0000800080008000)) 
    \p_070_2_reg_286[8]_i_2 
       (.I0(ap_CS_fsm_state5),
        .I1(internal_full_n_reg_0),
        .I2(passthrough_data_str_2_full_n),
        .I3(input_data_data_stre_empty_n),
        .I4(\p_070_2_reg_286[8]_i_5_n_0 ),
        .I5(\p_s_reg_253[10]_i_3_n_0 ),
        .O(\p_070_2_reg_286[8]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair69" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \p_070_2_reg_286[8]_i_3 
       (.I0(p_070_2_reg_286_reg__0[6]),
        .I1(\p_070_2_reg_286[8]_i_6_n_0 ),
        .I2(p_070_2_reg_286_reg__0[7]),
        .I3(p_070_2_reg_286_reg__0[8]),
        .O(j_V_1_fu_339_p2[8]));
  (* SOFT_HLUTNM = "soft_lutpair71" *) 
  LUT3 #(
    .INIT(8'h04)) 
    \p_070_2_reg_286[8]_i_5 
       (.I0(p_070_2_reg_286_reg__0[1]),
        .I1(p_070_2_reg_286_reg__0[2]),
        .I2(p_070_2_reg_286_reg__0[0]),
        .O(\p_070_2_reg_286[8]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \p_070_2_reg_286[8]_i_6 
       (.I0(p_070_2_reg_286_reg__0[5]),
        .I1(p_070_2_reg_286_reg__0[3]),
        .I2(p_070_2_reg_286_reg__0[1]),
        .I3(p_070_2_reg_286_reg__0[0]),
        .I4(p_070_2_reg_286_reg__0[2]),
        .I5(p_070_2_reg_286_reg__0[4]),
        .O(\p_070_2_reg_286[8]_i_6_n_0 ));
  FDRE \p_070_2_reg_286_reg[0] 
       (.C(ap_clk),
        .CE(\p_070_2_reg_286[8]_i_2_n_0 ),
        .D(j_V_1_fu_339_p2[0]),
        .Q(p_070_2_reg_286_reg__0[0]),
        .R(ap_NS_fsm14_out));
  FDRE \p_070_2_reg_286_reg[1] 
       (.C(ap_clk),
        .CE(\p_070_2_reg_286[8]_i_2_n_0 ),
        .D(j_V_1_fu_339_p2[1]),
        .Q(p_070_2_reg_286_reg__0[1]),
        .R(ap_NS_fsm14_out));
  FDRE \p_070_2_reg_286_reg[2] 
       (.C(ap_clk),
        .CE(\p_070_2_reg_286[8]_i_2_n_0 ),
        .D(j_V_1_fu_339_p2[2]),
        .Q(p_070_2_reg_286_reg__0[2]),
        .R(ap_NS_fsm14_out));
  FDRE \p_070_2_reg_286_reg[3] 
       (.C(ap_clk),
        .CE(\p_070_2_reg_286[8]_i_2_n_0 ),
        .D(j_V_1_fu_339_p2[3]),
        .Q(p_070_2_reg_286_reg__0[3]),
        .R(ap_NS_fsm14_out));
  FDRE \p_070_2_reg_286_reg[4] 
       (.C(ap_clk),
        .CE(\p_070_2_reg_286[8]_i_2_n_0 ),
        .D(j_V_1_fu_339_p2[4]),
        .Q(p_070_2_reg_286_reg__0[4]),
        .R(ap_NS_fsm14_out));
  FDRE \p_070_2_reg_286_reg[5] 
       (.C(ap_clk),
        .CE(\p_070_2_reg_286[8]_i_2_n_0 ),
        .D(j_V_1_fu_339_p2[5]),
        .Q(p_070_2_reg_286_reg__0[5]),
        .R(ap_NS_fsm14_out));
  FDRE \p_070_2_reg_286_reg[6] 
       (.C(ap_clk),
        .CE(\p_070_2_reg_286[8]_i_2_n_0 ),
        .D(j_V_1_fu_339_p2[6]),
        .Q(p_070_2_reg_286_reg__0[6]),
        .R(ap_NS_fsm14_out));
  FDRE \p_070_2_reg_286_reg[7] 
       (.C(ap_clk),
        .CE(\p_070_2_reg_286[8]_i_2_n_0 ),
        .D(j_V_1_fu_339_p2[7]),
        .Q(p_070_2_reg_286_reg__0[7]),
        .R(ap_NS_fsm14_out));
  FDRE \p_070_2_reg_286_reg[8] 
       (.C(ap_clk),
        .CE(\p_070_2_reg_286[8]_i_2_n_0 ),
        .D(j_V_1_fu_339_p2[8]),
        .Q(p_070_2_reg_286_reg__0[8]),
        .R(ap_NS_fsm14_out));
  (* SOFT_HLUTNM = "soft_lutpair79" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \p_1_reg_264[0]_i_1 
       (.I0(p_1_reg_264_reg__0[0]),
        .O(j_V_fu_315_p2[0]));
  (* SOFT_HLUTNM = "soft_lutpair79" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \p_1_reg_264[1]_i_1 
       (.I0(p_1_reg_264_reg__0[0]),
        .I1(p_1_reg_264_reg__0[1]),
        .O(j_V_fu_315_p2[1]));
  (* SOFT_HLUTNM = "soft_lutpair73" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \p_1_reg_264[2]_i_1 
       (.I0(p_1_reg_264_reg__0[0]),
        .I1(p_1_reg_264_reg__0[1]),
        .I2(p_1_reg_264_reg__0[2]),
        .O(j_V_fu_315_p2[2]));
  (* SOFT_HLUTNM = "soft_lutpair60" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \p_1_reg_264[3]_i_1 
       (.I0(p_1_reg_264_reg__0[1]),
        .I1(p_1_reg_264_reg__0[0]),
        .I2(p_1_reg_264_reg__0[2]),
        .I3(p_1_reg_264_reg__0[3]),
        .O(j_V_fu_315_p2[3]));
  (* SOFT_HLUTNM = "soft_lutpair60" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \p_1_reg_264[4]_i_1 
       (.I0(p_1_reg_264_reg__0[2]),
        .I1(p_1_reg_264_reg__0[0]),
        .I2(p_1_reg_264_reg__0[1]),
        .I3(p_1_reg_264_reg__0[3]),
        .I4(p_1_reg_264_reg__0[4]),
        .O(j_V_fu_315_p2[4]));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \p_1_reg_264[5]_i_1 
       (.I0(p_1_reg_264_reg__0[3]),
        .I1(p_1_reg_264_reg__0[1]),
        .I2(p_1_reg_264_reg__0[0]),
        .I3(p_1_reg_264_reg__0[2]),
        .I4(p_1_reg_264_reg__0[4]),
        .I5(p_1_reg_264_reg__0[5]),
        .O(j_V_fu_315_p2[5]));
  LUT2 #(
    .INIT(4'h6)) 
    \p_1_reg_264[6]_i_1 
       (.I0(\p_1_reg_264[8]_i_6_n_0 ),
        .I1(p_1_reg_264_reg__0[6]),
        .O(j_V_fu_315_p2[6]));
  (* SOFT_HLUTNM = "soft_lutpair70" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \p_1_reg_264[7]_i_1 
       (.I0(\p_1_reg_264[8]_i_6_n_0 ),
        .I1(p_1_reg_264_reg__0[6]),
        .I2(p_1_reg_264_reg__0[7]),
        .O(j_V_fu_315_p2[7]));
  LUT3 #(
    .INIT(8'h04)) 
    \p_1_reg_264[8]_i_1 
       (.I0(p_1_reg_2640),
        .I1(ap_CS_fsm_state2),
        .I2(tmp_fu_297_p2__12),
        .O(p_1_reg_264));
  LUT6 #(
    .INIT(64'h0000800080008000)) 
    \p_1_reg_264[8]_i_2 
       (.I0(ap_CS_fsm_state3),
        .I1(internal_full_n_reg_0),
        .I2(passthrough_data_str_2_full_n),
        .I3(input_data_data_stre_empty_n),
        .I4(\p_1_reg_264[8]_i_5_n_0 ),
        .I5(\p_070_1_reg_275[10]_i_4_n_0 ),
        .O(p_1_reg_2640));
  (* SOFT_HLUTNM = "soft_lutpair70" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \p_1_reg_264[8]_i_3 
       (.I0(p_1_reg_264_reg__0[6]),
        .I1(\p_1_reg_264[8]_i_6_n_0 ),
        .I2(p_1_reg_264_reg__0[7]),
        .I3(p_1_reg_264_reg__0[8]),
        .O(j_V_fu_315_p2[8]));
  (* SOFT_HLUTNM = "soft_lutpair62" *) 
  LUT5 #(
    .INIT(32'h00000008)) 
    \p_1_reg_264[8]_i_4 
       (.I0(\p_1_reg_264[8]_i_7_n_0 ),
        .I1(\p_1_reg_264[8]_i_8_n_0 ),
        .I2(p_s_reg_253[0]),
        .I3(p_s_reg_253[1]),
        .I4(p_s_reg_253[2]),
        .O(tmp_fu_297_p2__12));
  (* SOFT_HLUTNM = "soft_lutpair73" *) 
  LUT3 #(
    .INIT(8'h04)) 
    \p_1_reg_264[8]_i_5 
       (.I0(p_1_reg_264_reg__0[1]),
        .I1(p_1_reg_264_reg__0[2]),
        .I2(p_1_reg_264_reg__0[0]),
        .O(\p_1_reg_264[8]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \p_1_reg_264[8]_i_6 
       (.I0(p_1_reg_264_reg__0[5]),
        .I1(p_1_reg_264_reg__0[3]),
        .I2(p_1_reg_264_reg__0[1]),
        .I3(p_1_reg_264_reg__0[0]),
        .I4(p_1_reg_264_reg__0[2]),
        .I5(p_1_reg_264_reg__0[4]),
        .O(\p_1_reg_264[8]_i_6_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair64" *) 
  LUT4 #(
    .INIT(16'h4000)) 
    \p_1_reg_264[8]_i_7 
       (.I0(p_s_reg_253[6]),
        .I1(p_s_reg_253[5]),
        .I2(p_s_reg_253[4]),
        .I3(p_s_reg_253[3]),
        .O(\p_1_reg_264[8]_i_7_n_0 ));
  LUT4 #(
    .INIT(16'h0004)) 
    \p_1_reg_264[8]_i_8 
       (.I0(p_s_reg_253[9]),
        .I1(p_s_reg_253[10]),
        .I2(p_s_reg_253[8]),
        .I3(p_s_reg_253[7]),
        .O(\p_1_reg_264[8]_i_8_n_0 ));
  FDRE \p_1_reg_264_reg[0] 
       (.C(ap_clk),
        .CE(p_1_reg_2640),
        .D(j_V_fu_315_p2[0]),
        .Q(p_1_reg_264_reg__0[0]),
        .R(p_1_reg_264));
  FDRE \p_1_reg_264_reg[1] 
       (.C(ap_clk),
        .CE(p_1_reg_2640),
        .D(j_V_fu_315_p2[1]),
        .Q(p_1_reg_264_reg__0[1]),
        .R(p_1_reg_264));
  FDRE \p_1_reg_264_reg[2] 
       (.C(ap_clk),
        .CE(p_1_reg_2640),
        .D(j_V_fu_315_p2[2]),
        .Q(p_1_reg_264_reg__0[2]),
        .R(p_1_reg_264));
  FDRE \p_1_reg_264_reg[3] 
       (.C(ap_clk),
        .CE(p_1_reg_2640),
        .D(j_V_fu_315_p2[3]),
        .Q(p_1_reg_264_reg__0[3]),
        .R(p_1_reg_264));
  FDRE \p_1_reg_264_reg[4] 
       (.C(ap_clk),
        .CE(p_1_reg_2640),
        .D(j_V_fu_315_p2[4]),
        .Q(p_1_reg_264_reg__0[4]),
        .R(p_1_reg_264));
  FDRE \p_1_reg_264_reg[5] 
       (.C(ap_clk),
        .CE(p_1_reg_2640),
        .D(j_V_fu_315_p2[5]),
        .Q(p_1_reg_264_reg__0[5]),
        .R(p_1_reg_264));
  FDRE \p_1_reg_264_reg[6] 
       (.C(ap_clk),
        .CE(p_1_reg_2640),
        .D(j_V_fu_315_p2[6]),
        .Q(p_1_reg_264_reg__0[6]),
        .R(p_1_reg_264));
  FDRE \p_1_reg_264_reg[7] 
       (.C(ap_clk),
        .CE(p_1_reg_2640),
        .D(j_V_fu_315_p2[7]),
        .Q(p_1_reg_264_reg__0[7]),
        .R(p_1_reg_264));
  FDRE \p_1_reg_264_reg[8] 
       (.C(ap_clk),
        .CE(p_1_reg_2640),
        .D(j_V_fu_315_p2[8]),
        .Q(p_1_reg_264_reg__0[8]),
        .R(p_1_reg_264));
  LUT6 #(
    .INIT(64'h00000000F8000000)) 
    \p_s_reg_253[10]_i_1 
       (.I0(start_for_Mat2AXIvideo_U0_full_n),
        .I1(start_for_Mat2AXIvideo_1_U0_full_n),
        .I2(start_once_reg_reg_0),
        .I3(start_for_Loop_loop_height_pro_U0_empty_n),
        .I4(Q[0]),
        .I5(ap_NS_fsm1),
        .O(p_s_reg_253_0));
  LUT5 #(
    .INIT(32'h00200000)) 
    \p_s_reg_253[10]_i_2 
       (.I0(\p_s_reg_253[10]_i_3_n_0 ),
        .I1(p_070_2_reg_286_reg__0[0]),
        .I2(p_070_2_reg_286_reg__0[2]),
        .I3(p_070_2_reg_286_reg__0[1]),
        .I4(ap_CS_fsm_state5),
        .O(ap_NS_fsm1));
  LUT6 #(
    .INIT(64'h0010000000000000)) 
    \p_s_reg_253[10]_i_3 
       (.I0(p_070_2_reg_286_reg__0[3]),
        .I1(p_070_2_reg_286_reg__0[4]),
        .I2(p_070_2_reg_286_reg__0[5]),
        .I3(p_070_2_reg_286_reg__0[6]),
        .I4(p_070_2_reg_286_reg__0[8]),
        .I5(p_070_2_reg_286_reg__0[7]),
        .O(\p_s_reg_253[10]_i_3_n_0 ));
  FDRE \p_s_reg_253_reg[0] 
       (.C(ap_clk),
        .CE(ap_NS_fsm1),
        .D(i_V_reg_348[0]),
        .Q(p_s_reg_253[0]),
        .R(p_s_reg_253_0));
  FDRE \p_s_reg_253_reg[10] 
       (.C(ap_clk),
        .CE(ap_NS_fsm1),
        .D(i_V_reg_348[10]),
        .Q(p_s_reg_253[10]),
        .R(p_s_reg_253_0));
  FDRE \p_s_reg_253_reg[1] 
       (.C(ap_clk),
        .CE(ap_NS_fsm1),
        .D(i_V_reg_348[1]),
        .Q(p_s_reg_253[1]),
        .R(p_s_reg_253_0));
  FDRE \p_s_reg_253_reg[2] 
       (.C(ap_clk),
        .CE(ap_NS_fsm1),
        .D(i_V_reg_348[2]),
        .Q(p_s_reg_253[2]),
        .R(p_s_reg_253_0));
  FDRE \p_s_reg_253_reg[3] 
       (.C(ap_clk),
        .CE(ap_NS_fsm1),
        .D(i_V_reg_348[3]),
        .Q(p_s_reg_253[3]),
        .R(p_s_reg_253_0));
  FDRE \p_s_reg_253_reg[4] 
       (.C(ap_clk),
        .CE(ap_NS_fsm1),
        .D(i_V_reg_348[4]),
        .Q(p_s_reg_253[4]),
        .R(p_s_reg_253_0));
  FDRE \p_s_reg_253_reg[5] 
       (.C(ap_clk),
        .CE(ap_NS_fsm1),
        .D(i_V_reg_348[5]),
        .Q(p_s_reg_253[5]),
        .R(p_s_reg_253_0));
  FDRE \p_s_reg_253_reg[6] 
       (.C(ap_clk),
        .CE(ap_NS_fsm1),
        .D(i_V_reg_348[6]),
        .Q(p_s_reg_253[6]),
        .R(p_s_reg_253_0));
  FDRE \p_s_reg_253_reg[7] 
       (.C(ap_clk),
        .CE(ap_NS_fsm1),
        .D(i_V_reg_348[7]),
        .Q(p_s_reg_253[7]),
        .R(p_s_reg_253_0));
  FDRE \p_s_reg_253_reg[8] 
       (.C(ap_clk),
        .CE(ap_NS_fsm1),
        .D(i_V_reg_348[8]),
        .Q(p_s_reg_253[8]),
        .R(p_s_reg_253_0));
  FDRE \p_s_reg_253_reg[9] 
       (.C(ap_clk),
        .CE(ap_NS_fsm1),
        .D(i_V_reg_348[9]),
        .Q(p_s_reg_253[9]),
        .R(p_s_reg_253_0));
  LUT5 #(
    .INIT(32'h0000ECCC)) 
    start_once_reg_i_1
       (.I0(start_for_Loop_loop_height_pro_U0_empty_n),
        .I1(start_once_reg_reg_0),
        .I2(start_for_Mat2AXIvideo_1_U0_full_n),
        .I3(start_for_Mat2AXIvideo_U0_full_n),
        .I4(Loop_loop_height_pro_U0_ap_ready),
        .O(start_once_reg_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    start_once_reg_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(start_once_reg_i_1_n_0),
        .Q(start_once_reg_reg_0),
        .R(ap_rst));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Mat2AXIvideo
   (image_out_TVALID,
    Mat2AXIvideo_U0_ap_ready,
    Mat2AXIvideo_U0_img_data_stream_2_V_read,
    ap_done_reg_reg_0,
    ap_done,
    ap_idle,
    image_out_TUSER,
    image_out_TLAST,
    image_out_TDATA,
    ap_rst,
    ap_clk,
    ap_rst_n,
    start_for_Mat2AXIvideo_U0_empty_n,
    image_out_TREADY,
    passthrough_data_str_empty_n,
    passthrough_data_str_2_empty_n,
    passthrough_data_str_1_empty_n,
    ap_done_reg,
    Mat2AXIvideo_1_U0_ap_ready,
    Q,
    start_for_Mat2AXIvideo_1_U0_empty_n,
    \ap_CS_fsm_reg[0]_0 ,
    \ap_CS_fsm_reg[0]_1 ,
    D);
  output image_out_TVALID;
  output Mat2AXIvideo_U0_ap_ready;
  output Mat2AXIvideo_U0_img_data_stream_2_V_read;
  output ap_done_reg_reg_0;
  output ap_done;
  output ap_idle;
  output [0:0]image_out_TUSER;
  output [0:0]image_out_TLAST;
  output [23:0]image_out_TDATA;
  input ap_rst;
  input ap_clk;
  input ap_rst_n;
  input start_for_Mat2AXIvideo_U0_empty_n;
  input image_out_TREADY;
  input passthrough_data_str_empty_n;
  input passthrough_data_str_2_empty_n;
  input passthrough_data_str_1_empty_n;
  input ap_done_reg;
  input Mat2AXIvideo_1_U0_ap_ready;
  input [0:0]Q;
  input start_for_Mat2AXIvideo_1_U0_empty_n;
  input [0:0]\ap_CS_fsm_reg[0]_0 ;
  input [0:0]\ap_CS_fsm_reg[0]_1 ;
  input [23:0]D;

  wire AXI_video_strm_V_data_V_1_ack_in;
  wire AXI_video_strm_V_data_V_1_load_A;
  wire AXI_video_strm_V_data_V_1_load_B;
  wire [23:0]AXI_video_strm_V_data_V_1_payload_A;
  wire [23:0]AXI_video_strm_V_data_V_1_payload_B;
  wire AXI_video_strm_V_data_V_1_sel;
  wire AXI_video_strm_V_data_V_1_sel_rd_i_1_n_0;
  wire AXI_video_strm_V_data_V_1_sel_wr;
  wire AXI_video_strm_V_data_V_1_sel_wr_i_1_n_0;
  wire [1:1]AXI_video_strm_V_data_V_1_state;
  wire \AXI_video_strm_V_data_V_1_state[0]_i_1_n_0 ;
  wire \AXI_video_strm_V_data_V_1_state_reg_n_0_[0] ;
  wire [1:1]AXI_video_strm_V_dest_V_1_state;
  wire \AXI_video_strm_V_dest_V_1_state[0]_i_1_n_0 ;
  wire \AXI_video_strm_V_dest_V_1_state_reg_n_0_[1] ;
  wire [1:1]AXI_video_strm_V_id_V_1_state;
  wire \AXI_video_strm_V_id_V_1_state[0]_i_1_n_0 ;
  wire \AXI_video_strm_V_id_V_1_state_reg_n_0_[0] ;
  wire \AXI_video_strm_V_id_V_1_state_reg_n_0_[1] ;
  wire [1:1]AXI_video_strm_V_keep_V_1_state;
  wire \AXI_video_strm_V_keep_V_1_state[0]_i_1_n_0 ;
  wire \AXI_video_strm_V_keep_V_1_state_reg_n_0_[0] ;
  wire \AXI_video_strm_V_keep_V_1_state_reg_n_0_[1] ;
  wire AXI_video_strm_V_last_V_1_ack_in;
  wire AXI_video_strm_V_last_V_1_payload_A;
  wire \AXI_video_strm_V_last_V_1_payload_A[0]_i_1_n_0 ;
  wire AXI_video_strm_V_last_V_1_payload_B;
  wire \AXI_video_strm_V_last_V_1_payload_B[0]_i_1_n_0 ;
  wire AXI_video_strm_V_last_V_1_sel;
  wire AXI_video_strm_V_last_V_1_sel_rd_i_1_n_0;
  wire AXI_video_strm_V_last_V_1_sel_wr;
  wire AXI_video_strm_V_last_V_1_sel_wr_i_1_n_0;
  wire [1:1]AXI_video_strm_V_last_V_1_state;
  wire \AXI_video_strm_V_last_V_1_state[0]_i_1_n_0 ;
  wire \AXI_video_strm_V_last_V_1_state_reg_n_0_[0] ;
  wire [1:1]AXI_video_strm_V_strb_V_1_state;
  wire \AXI_video_strm_V_strb_V_1_state[0]_i_1_n_0 ;
  wire \AXI_video_strm_V_strb_V_1_state_reg_n_0_[0] ;
  wire \AXI_video_strm_V_strb_V_1_state_reg_n_0_[1] ;
  wire AXI_video_strm_V_user_V_1_ack_in;
  wire AXI_video_strm_V_user_V_1_payload_A;
  wire \AXI_video_strm_V_user_V_1_payload_A[0]_i_1_n_0 ;
  wire AXI_video_strm_V_user_V_1_payload_B;
  wire \AXI_video_strm_V_user_V_1_payload_B[0]_i_1_n_0 ;
  wire AXI_video_strm_V_user_V_1_sel;
  wire AXI_video_strm_V_user_V_1_sel_rd_i_1_n_0;
  wire AXI_video_strm_V_user_V_1_sel_wr;
  wire AXI_video_strm_V_user_V_1_sel_wr_i_1_n_0;
  wire [1:1]AXI_video_strm_V_user_V_1_state;
  wire \AXI_video_strm_V_user_V_1_state[0]_i_1_n_0 ;
  wire \AXI_video_strm_V_user_V_1_state_reg_n_0_[0] ;
  wire [23:0]D;
  wire Mat2AXIvideo_1_U0_ap_ready;
  wire Mat2AXIvideo_U0_ap_ready;
  wire Mat2AXIvideo_U0_img_data_stream_2_V_read;
  wire [0:0]Q;
  wire \ap_CS_fsm[2]_i_2__1_n_0 ;
  wire \ap_CS_fsm[2]_i_3_n_0 ;
  wire \ap_CS_fsm[3]_i_4_n_0 ;
  wire ap_CS_fsm_pp0_stage0;
  wire [0:0]\ap_CS_fsm_reg[0]_0 ;
  wire [0:0]\ap_CS_fsm_reg[0]_1 ;
  wire \ap_CS_fsm_reg_n_0_[0] ;
  wire ap_CS_fsm_state2;
  wire ap_CS_fsm_state6;
  wire [3:0]ap_NS_fsm;
  wire ap_NS_fsm1;
  wire ap_NS_fsm112_out;
  wire ap_block_pp0_stage0_subdone;
  wire ap_clk;
  wire ap_done;
  wire ap_done_INST_0_i_3_n_0;
  wire ap_done_INST_0_i_7_n_0;
  wire ap_done_INST_0_i_8_n_0;
  wire ap_done_reg;
  wire ap_done_reg_0;
  wire ap_done_reg_i_1_n_0;
  wire ap_done_reg_reg_0;
  wire ap_enable_reg_pp0_iter0;
  wire ap_enable_reg_pp0_iter0_i_1_n_0;
  wire ap_enable_reg_pp0_iter1__0;
  wire ap_enable_reg_pp0_iter1_i_1_n_0;
  wire ap_enable_reg_pp0_iter1_reg_n_0;
  wire ap_enable_reg_pp0_iter2_i_1_n_0;
  wire ap_enable_reg_pp0_iter2_reg_n_0;
  wire ap_idle;
  wire ap_rst;
  wire ap_rst_n;
  wire axi_last_V_reg_2750;
  wire \axi_last_V_reg_275[0]_i_1_n_0 ;
  wire \axi_last_V_reg_275[0]_i_2_n_0 ;
  wire \axi_last_V_reg_275[0]_i_3_n_0 ;
  wire \axi_last_V_reg_275_reg_n_0_[0] ;
  wire exitcond1_fu_200_p2__13;
  wire exitcond_fu_212_p2;
  wire \exitcond_reg_266[0]_i_1_n_0 ;
  wire \exitcond_reg_266[0]_i_3_n_0 ;
  wire \exitcond_reg_266[0]_i_4_n_0 ;
  wire exitcond_reg_266_pp0_iter1_reg;
  wire \exitcond_reg_266_pp0_iter1_reg[0]_i_1_n_0 ;
  wire \exitcond_reg_266_reg_n_0_[0] ;
  wire [10:0]i_V_fu_206_p2;
  wire [10:0]i_V_reg_261;
  wire i_V_reg_2610;
  wire \i_V_reg_261[10]_i_3_n_0 ;
  wire [23:0]image_out_TDATA;
  wire [0:0]image_out_TLAST;
  wire image_out_TREADY;
  wire [0:0]image_out_TUSER;
  wire image_out_TVALID;
  wire [10:0]j_V_fu_218_p2;
  wire passthrough_data_str_1_empty_n;
  wire passthrough_data_str_2_empty_n;
  wire passthrough_data_str_empty_n;
  wire start_for_Mat2AXIvideo_1_U0_empty_n;
  wire start_for_Mat2AXIvideo_U0_empty_n;
  wire t_V_1_reg_184;
  wire t_V_1_reg_1840;
  wire \t_V_1_reg_184[10]_i_5_n_0 ;
  wire [10:0]t_V_1_reg_184_reg__0;
  wire [10:0]t_V_reg_173;
  wire tmp_user_V_fu_122;
  wire \tmp_user_V_fu_122[0]_i_1_n_0 ;

  LUT3 #(
    .INIT(8'h0D)) 
    \AXI_video_strm_V_data_V_1_payload_A[23]_i_1 
       (.I0(\AXI_video_strm_V_data_V_1_state_reg_n_0_[0] ),
        .I1(AXI_video_strm_V_data_V_1_ack_in),
        .I2(AXI_video_strm_V_data_V_1_sel_wr),
        .O(AXI_video_strm_V_data_V_1_load_A));
  FDRE \AXI_video_strm_V_data_V_1_payload_A_reg[0] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_A),
        .D(D[0]),
        .Q(AXI_video_strm_V_data_V_1_payload_A[0]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_A_reg[10] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_A),
        .D(D[10]),
        .Q(AXI_video_strm_V_data_V_1_payload_A[10]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_A_reg[11] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_A),
        .D(D[11]),
        .Q(AXI_video_strm_V_data_V_1_payload_A[11]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_A_reg[12] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_A),
        .D(D[12]),
        .Q(AXI_video_strm_V_data_V_1_payload_A[12]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_A_reg[13] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_A),
        .D(D[13]),
        .Q(AXI_video_strm_V_data_V_1_payload_A[13]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_A_reg[14] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_A),
        .D(D[14]),
        .Q(AXI_video_strm_V_data_V_1_payload_A[14]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_A_reg[15] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_A),
        .D(D[15]),
        .Q(AXI_video_strm_V_data_V_1_payload_A[15]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_A_reg[16] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_A),
        .D(D[16]),
        .Q(AXI_video_strm_V_data_V_1_payload_A[16]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_A_reg[17] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_A),
        .D(D[17]),
        .Q(AXI_video_strm_V_data_V_1_payload_A[17]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_A_reg[18] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_A),
        .D(D[18]),
        .Q(AXI_video_strm_V_data_V_1_payload_A[18]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_A_reg[19] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_A),
        .D(D[19]),
        .Q(AXI_video_strm_V_data_V_1_payload_A[19]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_A_reg[1] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_A),
        .D(D[1]),
        .Q(AXI_video_strm_V_data_V_1_payload_A[1]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_A_reg[20] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_A),
        .D(D[20]),
        .Q(AXI_video_strm_V_data_V_1_payload_A[20]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_A_reg[21] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_A),
        .D(D[21]),
        .Q(AXI_video_strm_V_data_V_1_payload_A[21]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_A_reg[22] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_A),
        .D(D[22]),
        .Q(AXI_video_strm_V_data_V_1_payload_A[22]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_A_reg[23] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_A),
        .D(D[23]),
        .Q(AXI_video_strm_V_data_V_1_payload_A[23]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_A_reg[2] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_A),
        .D(D[2]),
        .Q(AXI_video_strm_V_data_V_1_payload_A[2]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_A_reg[3] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_A),
        .D(D[3]),
        .Q(AXI_video_strm_V_data_V_1_payload_A[3]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_A_reg[4] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_A),
        .D(D[4]),
        .Q(AXI_video_strm_V_data_V_1_payload_A[4]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_A_reg[5] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_A),
        .D(D[5]),
        .Q(AXI_video_strm_V_data_V_1_payload_A[5]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_A_reg[6] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_A),
        .D(D[6]),
        .Q(AXI_video_strm_V_data_V_1_payload_A[6]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_A_reg[7] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_A),
        .D(D[7]),
        .Q(AXI_video_strm_V_data_V_1_payload_A[7]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_A_reg[8] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_A),
        .D(D[8]),
        .Q(AXI_video_strm_V_data_V_1_payload_A[8]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_A_reg[9] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_A),
        .D(D[9]),
        .Q(AXI_video_strm_V_data_V_1_payload_A[9]),
        .R(1'b0));
  LUT3 #(
    .INIT(8'hA2)) 
    \AXI_video_strm_V_data_V_1_payload_B[23]_i_1 
       (.I0(AXI_video_strm_V_data_V_1_sel_wr),
        .I1(\AXI_video_strm_V_data_V_1_state_reg_n_0_[0] ),
        .I2(AXI_video_strm_V_data_V_1_ack_in),
        .O(AXI_video_strm_V_data_V_1_load_B));
  FDRE \AXI_video_strm_V_data_V_1_payload_B_reg[0] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_B),
        .D(D[0]),
        .Q(AXI_video_strm_V_data_V_1_payload_B[0]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_B_reg[10] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_B),
        .D(D[10]),
        .Q(AXI_video_strm_V_data_V_1_payload_B[10]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_B_reg[11] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_B),
        .D(D[11]),
        .Q(AXI_video_strm_V_data_V_1_payload_B[11]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_B_reg[12] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_B),
        .D(D[12]),
        .Q(AXI_video_strm_V_data_V_1_payload_B[12]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_B_reg[13] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_B),
        .D(D[13]),
        .Q(AXI_video_strm_V_data_V_1_payload_B[13]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_B_reg[14] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_B),
        .D(D[14]),
        .Q(AXI_video_strm_V_data_V_1_payload_B[14]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_B_reg[15] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_B),
        .D(D[15]),
        .Q(AXI_video_strm_V_data_V_1_payload_B[15]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_B_reg[16] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_B),
        .D(D[16]),
        .Q(AXI_video_strm_V_data_V_1_payload_B[16]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_B_reg[17] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_B),
        .D(D[17]),
        .Q(AXI_video_strm_V_data_V_1_payload_B[17]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_B_reg[18] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_B),
        .D(D[18]),
        .Q(AXI_video_strm_V_data_V_1_payload_B[18]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_B_reg[19] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_B),
        .D(D[19]),
        .Q(AXI_video_strm_V_data_V_1_payload_B[19]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_B_reg[1] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_B),
        .D(D[1]),
        .Q(AXI_video_strm_V_data_V_1_payload_B[1]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_B_reg[20] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_B),
        .D(D[20]),
        .Q(AXI_video_strm_V_data_V_1_payload_B[20]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_B_reg[21] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_B),
        .D(D[21]),
        .Q(AXI_video_strm_V_data_V_1_payload_B[21]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_B_reg[22] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_B),
        .D(D[22]),
        .Q(AXI_video_strm_V_data_V_1_payload_B[22]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_B_reg[23] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_B),
        .D(D[23]),
        .Q(AXI_video_strm_V_data_V_1_payload_B[23]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_B_reg[2] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_B),
        .D(D[2]),
        .Q(AXI_video_strm_V_data_V_1_payload_B[2]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_B_reg[3] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_B),
        .D(D[3]),
        .Q(AXI_video_strm_V_data_V_1_payload_B[3]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_B_reg[4] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_B),
        .D(D[4]),
        .Q(AXI_video_strm_V_data_V_1_payload_B[4]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_B_reg[5] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_B),
        .D(D[5]),
        .Q(AXI_video_strm_V_data_V_1_payload_B[5]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_B_reg[6] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_B),
        .D(D[6]),
        .Q(AXI_video_strm_V_data_V_1_payload_B[6]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_B_reg[7] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_B),
        .D(D[7]),
        .Q(AXI_video_strm_V_data_V_1_payload_B[7]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_B_reg[8] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_B),
        .D(D[8]),
        .Q(AXI_video_strm_V_data_V_1_payload_B[8]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_B_reg[9] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_B),
        .D(D[9]),
        .Q(AXI_video_strm_V_data_V_1_payload_B[9]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair124" *) 
  LUT3 #(
    .INIT(8'h78)) 
    AXI_video_strm_V_data_V_1_sel_rd_i_1
       (.I0(image_out_TREADY),
        .I1(\AXI_video_strm_V_data_V_1_state_reg_n_0_[0] ),
        .I2(AXI_video_strm_V_data_V_1_sel),
        .O(AXI_video_strm_V_data_V_1_sel_rd_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    AXI_video_strm_V_data_V_1_sel_rd_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(AXI_video_strm_V_data_V_1_sel_rd_i_1_n_0),
        .Q(AXI_video_strm_V_data_V_1_sel),
        .R(ap_rst));
  LUT3 #(
    .INIT(8'h78)) 
    AXI_video_strm_V_data_V_1_sel_wr_i_1
       (.I0(AXI_video_strm_V_data_V_1_ack_in),
        .I1(Mat2AXIvideo_U0_img_data_stream_2_V_read),
        .I2(AXI_video_strm_V_data_V_1_sel_wr),
        .O(AXI_video_strm_V_data_V_1_sel_wr_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    AXI_video_strm_V_data_V_1_sel_wr_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(AXI_video_strm_V_data_V_1_sel_wr_i_1_n_0),
        .Q(AXI_video_strm_V_data_V_1_sel_wr),
        .R(ap_rst));
  LUT5 #(
    .INIT(32'hA8A80888)) 
    \AXI_video_strm_V_data_V_1_state[0]_i_1 
       (.I0(ap_rst_n),
        .I1(\AXI_video_strm_V_data_V_1_state_reg_n_0_[0] ),
        .I2(AXI_video_strm_V_data_V_1_ack_in),
        .I3(image_out_TREADY),
        .I4(Mat2AXIvideo_U0_img_data_stream_2_V_read),
        .O(\AXI_video_strm_V_data_V_1_state[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair124" *) 
  LUT4 #(
    .INIT(16'hF5FD)) 
    \AXI_video_strm_V_data_V_1_state[1]_i_1 
       (.I0(\AXI_video_strm_V_data_V_1_state_reg_n_0_[0] ),
        .I1(AXI_video_strm_V_data_V_1_ack_in),
        .I2(image_out_TREADY),
        .I3(Mat2AXIvideo_U0_img_data_stream_2_V_read),
        .O(AXI_video_strm_V_data_V_1_state));
  FDRE #(
    .INIT(1'b0)) 
    \AXI_video_strm_V_data_V_1_state_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\AXI_video_strm_V_data_V_1_state[0]_i_1_n_0 ),
        .Q(\AXI_video_strm_V_data_V_1_state_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \AXI_video_strm_V_data_V_1_state_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(AXI_video_strm_V_data_V_1_state),
        .Q(AXI_video_strm_V_data_V_1_ack_in),
        .R(ap_rst));
  LUT5 #(
    .INIT(32'hAAA02A00)) 
    \AXI_video_strm_V_dest_V_1_state[0]_i_1 
       (.I0(ap_rst_n),
        .I1(image_out_TREADY),
        .I2(\AXI_video_strm_V_dest_V_1_state_reg_n_0_[1] ),
        .I3(image_out_TVALID),
        .I4(Mat2AXIvideo_U0_img_data_stream_2_V_read),
        .O(\AXI_video_strm_V_dest_V_1_state[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair122" *) 
  LUT4 #(
    .INIT(16'h0008)) 
    \AXI_video_strm_V_dest_V_1_state[0]_i_2 
       (.I0(ap_enable_reg_pp0_iter1_reg_n_0),
        .I1(ap_CS_fsm_pp0_stage0),
        .I2(\exitcond_reg_266_reg_n_0_[0] ),
        .I3(ap_block_pp0_stage0_subdone),
        .O(Mat2AXIvideo_U0_img_data_stream_2_V_read));
  LUT4 #(
    .INIT(16'hAFEF)) 
    \AXI_video_strm_V_dest_V_1_state[1]_i_1 
       (.I0(image_out_TREADY),
        .I1(\AXI_video_strm_V_dest_V_1_state_reg_n_0_[1] ),
        .I2(image_out_TVALID),
        .I3(Mat2AXIvideo_U0_img_data_stream_2_V_read),
        .O(AXI_video_strm_V_dest_V_1_state));
  FDRE #(
    .INIT(1'b0)) 
    \AXI_video_strm_V_dest_V_1_state_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\AXI_video_strm_V_dest_V_1_state[0]_i_1_n_0 ),
        .Q(image_out_TVALID),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \AXI_video_strm_V_dest_V_1_state_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(AXI_video_strm_V_dest_V_1_state),
        .Q(\AXI_video_strm_V_dest_V_1_state_reg_n_0_[1] ),
        .R(ap_rst));
  LUT5 #(
    .INIT(32'hAAA02A00)) 
    \AXI_video_strm_V_id_V_1_state[0]_i_1 
       (.I0(ap_rst_n),
        .I1(image_out_TREADY),
        .I2(\AXI_video_strm_V_id_V_1_state_reg_n_0_[1] ),
        .I3(\AXI_video_strm_V_id_V_1_state_reg_n_0_[0] ),
        .I4(Mat2AXIvideo_U0_img_data_stream_2_V_read),
        .O(\AXI_video_strm_V_id_V_1_state[0]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hAFEF)) 
    \AXI_video_strm_V_id_V_1_state[1]_i_1 
       (.I0(image_out_TREADY),
        .I1(\AXI_video_strm_V_id_V_1_state_reg_n_0_[1] ),
        .I2(\AXI_video_strm_V_id_V_1_state_reg_n_0_[0] ),
        .I3(Mat2AXIvideo_U0_img_data_stream_2_V_read),
        .O(AXI_video_strm_V_id_V_1_state));
  FDRE #(
    .INIT(1'b0)) 
    \AXI_video_strm_V_id_V_1_state_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\AXI_video_strm_V_id_V_1_state[0]_i_1_n_0 ),
        .Q(\AXI_video_strm_V_id_V_1_state_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \AXI_video_strm_V_id_V_1_state_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(AXI_video_strm_V_id_V_1_state),
        .Q(\AXI_video_strm_V_id_V_1_state_reg_n_0_[1] ),
        .R(ap_rst));
  LUT5 #(
    .INIT(32'hAAA02A00)) 
    \AXI_video_strm_V_keep_V_1_state[0]_i_1 
       (.I0(ap_rst_n),
        .I1(image_out_TREADY),
        .I2(\AXI_video_strm_V_keep_V_1_state_reg_n_0_[1] ),
        .I3(\AXI_video_strm_V_keep_V_1_state_reg_n_0_[0] ),
        .I4(Mat2AXIvideo_U0_img_data_stream_2_V_read),
        .O(\AXI_video_strm_V_keep_V_1_state[0]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hAFEF)) 
    \AXI_video_strm_V_keep_V_1_state[1]_i_1 
       (.I0(image_out_TREADY),
        .I1(\AXI_video_strm_V_keep_V_1_state_reg_n_0_[1] ),
        .I2(\AXI_video_strm_V_keep_V_1_state_reg_n_0_[0] ),
        .I3(Mat2AXIvideo_U0_img_data_stream_2_V_read),
        .O(AXI_video_strm_V_keep_V_1_state));
  FDRE #(
    .INIT(1'b0)) 
    \AXI_video_strm_V_keep_V_1_state_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\AXI_video_strm_V_keep_V_1_state[0]_i_1_n_0 ),
        .Q(\AXI_video_strm_V_keep_V_1_state_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \AXI_video_strm_V_keep_V_1_state_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(AXI_video_strm_V_keep_V_1_state),
        .Q(\AXI_video_strm_V_keep_V_1_state_reg_n_0_[1] ),
        .R(ap_rst));
  LUT5 #(
    .INIT(32'hFFAE00A2)) 
    \AXI_video_strm_V_last_V_1_payload_A[0]_i_1 
       (.I0(\axi_last_V_reg_275_reg_n_0_[0] ),
        .I1(\AXI_video_strm_V_last_V_1_state_reg_n_0_[0] ),
        .I2(AXI_video_strm_V_last_V_1_ack_in),
        .I3(AXI_video_strm_V_last_V_1_sel_wr),
        .I4(AXI_video_strm_V_last_V_1_payload_A),
        .O(\AXI_video_strm_V_last_V_1_payload_A[0]_i_1_n_0 ));
  FDRE \AXI_video_strm_V_last_V_1_payload_A_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\AXI_video_strm_V_last_V_1_payload_A[0]_i_1_n_0 ),
        .Q(AXI_video_strm_V_last_V_1_payload_A),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hBBFB8808)) 
    \AXI_video_strm_V_last_V_1_payload_B[0]_i_1 
       (.I0(\axi_last_V_reg_275_reg_n_0_[0] ),
        .I1(AXI_video_strm_V_last_V_1_sel_wr),
        .I2(\AXI_video_strm_V_last_V_1_state_reg_n_0_[0] ),
        .I3(AXI_video_strm_V_last_V_1_ack_in),
        .I4(AXI_video_strm_V_last_V_1_payload_B),
        .O(\AXI_video_strm_V_last_V_1_payload_B[0]_i_1_n_0 ));
  FDRE \AXI_video_strm_V_last_V_1_payload_B_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\AXI_video_strm_V_last_V_1_payload_B[0]_i_1_n_0 ),
        .Q(AXI_video_strm_V_last_V_1_payload_B),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair126" *) 
  LUT3 #(
    .INIT(8'h78)) 
    AXI_video_strm_V_last_V_1_sel_rd_i_1
       (.I0(image_out_TREADY),
        .I1(\AXI_video_strm_V_last_V_1_state_reg_n_0_[0] ),
        .I2(AXI_video_strm_V_last_V_1_sel),
        .O(AXI_video_strm_V_last_V_1_sel_rd_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    AXI_video_strm_V_last_V_1_sel_rd_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(AXI_video_strm_V_last_V_1_sel_rd_i_1_n_0),
        .Q(AXI_video_strm_V_last_V_1_sel),
        .R(ap_rst));
  (* SOFT_HLUTNM = "soft_lutpair129" *) 
  LUT3 #(
    .INIT(8'h78)) 
    AXI_video_strm_V_last_V_1_sel_wr_i_1
       (.I0(AXI_video_strm_V_last_V_1_ack_in),
        .I1(Mat2AXIvideo_U0_img_data_stream_2_V_read),
        .I2(AXI_video_strm_V_last_V_1_sel_wr),
        .O(AXI_video_strm_V_last_V_1_sel_wr_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    AXI_video_strm_V_last_V_1_sel_wr_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(AXI_video_strm_V_last_V_1_sel_wr_i_1_n_0),
        .Q(AXI_video_strm_V_last_V_1_sel_wr),
        .R(ap_rst));
  LUT5 #(
    .INIT(32'hA8A80888)) 
    \AXI_video_strm_V_last_V_1_state[0]_i_1 
       (.I0(ap_rst_n),
        .I1(\AXI_video_strm_V_last_V_1_state_reg_n_0_[0] ),
        .I2(AXI_video_strm_V_last_V_1_ack_in),
        .I3(image_out_TREADY),
        .I4(Mat2AXIvideo_U0_img_data_stream_2_V_read),
        .O(\AXI_video_strm_V_last_V_1_state[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair126" *) 
  LUT4 #(
    .INIT(16'hF5FD)) 
    \AXI_video_strm_V_last_V_1_state[1]_i_1 
       (.I0(\AXI_video_strm_V_last_V_1_state_reg_n_0_[0] ),
        .I1(AXI_video_strm_V_last_V_1_ack_in),
        .I2(image_out_TREADY),
        .I3(Mat2AXIvideo_U0_img_data_stream_2_V_read),
        .O(AXI_video_strm_V_last_V_1_state));
  FDRE #(
    .INIT(1'b0)) 
    \AXI_video_strm_V_last_V_1_state_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\AXI_video_strm_V_last_V_1_state[0]_i_1_n_0 ),
        .Q(\AXI_video_strm_V_last_V_1_state_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \AXI_video_strm_V_last_V_1_state_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(AXI_video_strm_V_last_V_1_state),
        .Q(AXI_video_strm_V_last_V_1_ack_in),
        .R(ap_rst));
  LUT5 #(
    .INIT(32'hAAA02A00)) 
    \AXI_video_strm_V_strb_V_1_state[0]_i_1 
       (.I0(ap_rst_n),
        .I1(image_out_TREADY),
        .I2(\AXI_video_strm_V_strb_V_1_state_reg_n_0_[1] ),
        .I3(\AXI_video_strm_V_strb_V_1_state_reg_n_0_[0] ),
        .I4(Mat2AXIvideo_U0_img_data_stream_2_V_read),
        .O(\AXI_video_strm_V_strb_V_1_state[0]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hAFEF)) 
    \AXI_video_strm_V_strb_V_1_state[1]_i_1 
       (.I0(image_out_TREADY),
        .I1(\AXI_video_strm_V_strb_V_1_state_reg_n_0_[1] ),
        .I2(\AXI_video_strm_V_strb_V_1_state_reg_n_0_[0] ),
        .I3(Mat2AXIvideo_U0_img_data_stream_2_V_read),
        .O(AXI_video_strm_V_strb_V_1_state));
  FDRE #(
    .INIT(1'b0)) 
    \AXI_video_strm_V_strb_V_1_state_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\AXI_video_strm_V_strb_V_1_state[0]_i_1_n_0 ),
        .Q(\AXI_video_strm_V_strb_V_1_state_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \AXI_video_strm_V_strb_V_1_state_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(AXI_video_strm_V_strb_V_1_state),
        .Q(\AXI_video_strm_V_strb_V_1_state_reg_n_0_[1] ),
        .R(ap_rst));
  LUT5 #(
    .INIT(32'hFFAE00A2)) 
    \AXI_video_strm_V_user_V_1_payload_A[0]_i_1 
       (.I0(tmp_user_V_fu_122),
        .I1(\AXI_video_strm_V_user_V_1_state_reg_n_0_[0] ),
        .I2(AXI_video_strm_V_user_V_1_ack_in),
        .I3(AXI_video_strm_V_user_V_1_sel_wr),
        .I4(AXI_video_strm_V_user_V_1_payload_A),
        .O(\AXI_video_strm_V_user_V_1_payload_A[0]_i_1_n_0 ));
  FDRE \AXI_video_strm_V_user_V_1_payload_A_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\AXI_video_strm_V_user_V_1_payload_A[0]_i_1_n_0 ),
        .Q(AXI_video_strm_V_user_V_1_payload_A),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hBBFB8808)) 
    \AXI_video_strm_V_user_V_1_payload_B[0]_i_1 
       (.I0(tmp_user_V_fu_122),
        .I1(AXI_video_strm_V_user_V_1_sel_wr),
        .I2(\AXI_video_strm_V_user_V_1_state_reg_n_0_[0] ),
        .I3(AXI_video_strm_V_user_V_1_ack_in),
        .I4(AXI_video_strm_V_user_V_1_payload_B),
        .O(\AXI_video_strm_V_user_V_1_payload_B[0]_i_1_n_0 ));
  FDRE \AXI_video_strm_V_user_V_1_payload_B_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\AXI_video_strm_V_user_V_1_payload_B[0]_i_1_n_0 ),
        .Q(AXI_video_strm_V_user_V_1_payload_B),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair127" *) 
  LUT3 #(
    .INIT(8'h78)) 
    AXI_video_strm_V_user_V_1_sel_rd_i_1
       (.I0(image_out_TREADY),
        .I1(\AXI_video_strm_V_user_V_1_state_reg_n_0_[0] ),
        .I2(AXI_video_strm_V_user_V_1_sel),
        .O(AXI_video_strm_V_user_V_1_sel_rd_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    AXI_video_strm_V_user_V_1_sel_rd_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(AXI_video_strm_V_user_V_1_sel_rd_i_1_n_0),
        .Q(AXI_video_strm_V_user_V_1_sel),
        .R(ap_rst));
  (* SOFT_HLUTNM = "soft_lutpair129" *) 
  LUT3 #(
    .INIT(8'h78)) 
    AXI_video_strm_V_user_V_1_sel_wr_i_1
       (.I0(AXI_video_strm_V_user_V_1_ack_in),
        .I1(Mat2AXIvideo_U0_img_data_stream_2_V_read),
        .I2(AXI_video_strm_V_user_V_1_sel_wr),
        .O(AXI_video_strm_V_user_V_1_sel_wr_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    AXI_video_strm_V_user_V_1_sel_wr_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(AXI_video_strm_V_user_V_1_sel_wr_i_1_n_0),
        .Q(AXI_video_strm_V_user_V_1_sel_wr),
        .R(ap_rst));
  LUT5 #(
    .INIT(32'hA8A80888)) 
    \AXI_video_strm_V_user_V_1_state[0]_i_1 
       (.I0(ap_rst_n),
        .I1(\AXI_video_strm_V_user_V_1_state_reg_n_0_[0] ),
        .I2(AXI_video_strm_V_user_V_1_ack_in),
        .I3(image_out_TREADY),
        .I4(Mat2AXIvideo_U0_img_data_stream_2_V_read),
        .O(\AXI_video_strm_V_user_V_1_state[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair127" *) 
  LUT4 #(
    .INIT(16'hF5FD)) 
    \AXI_video_strm_V_user_V_1_state[1]_i_1 
       (.I0(\AXI_video_strm_V_user_V_1_state_reg_n_0_[0] ),
        .I1(AXI_video_strm_V_user_V_1_ack_in),
        .I2(image_out_TREADY),
        .I3(Mat2AXIvideo_U0_img_data_stream_2_V_read),
        .O(AXI_video_strm_V_user_V_1_state));
  FDRE #(
    .INIT(1'b0)) 
    \AXI_video_strm_V_user_V_1_state_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\AXI_video_strm_V_user_V_1_state[0]_i_1_n_0 ),
        .Q(\AXI_video_strm_V_user_V_1_state_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \AXI_video_strm_V_user_V_1_state_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(AXI_video_strm_V_user_V_1_state),
        .Q(AXI_video_strm_V_user_V_1_ack_in),
        .R(ap_rst));
  LUT5 #(
    .INIT(32'hFF888F88)) 
    \ap_CS_fsm[0]_i_1__0 
       (.I0(Mat2AXIvideo_U0_ap_ready),
        .I1(ap_CS_fsm_state2),
        .I2(start_for_Mat2AXIvideo_U0_empty_n),
        .I3(\ap_CS_fsm_reg_n_0_[0] ),
        .I4(ap_done_reg_0),
        .O(ap_NS_fsm[0]));
  LUT6 #(
    .INIT(64'hEAEAEAEAEAFFEAEA)) 
    \ap_CS_fsm[1]_i_1__1 
       (.I0(ap_CS_fsm_state6),
        .I1(\ap_CS_fsm_reg_n_0_[0] ),
        .I2(ap_NS_fsm112_out),
        .I3(Mat2AXIvideo_U0_ap_ready),
        .I4(ap_CS_fsm_state2),
        .I5(ap_NS_fsm1),
        .O(ap_NS_fsm[1]));
  LUT6 #(
    .INIT(64'h0000000080000000)) 
    \ap_CS_fsm[1]_i_2__0 
       (.I0(\AXI_video_strm_V_id_V_1_state_reg_n_0_[1] ),
        .I1(\AXI_video_strm_V_dest_V_1_state_reg_n_0_[1] ),
        .I2(ap_CS_fsm_state2),
        .I3(AXI_video_strm_V_data_V_1_ack_in),
        .I4(ap_done_INST_0_i_3_n_0),
        .I5(exitcond1_fu_200_p2__13),
        .O(ap_NS_fsm1));
  LUT6 #(
    .INIT(64'hBBBBABBBAAAAAAAA)) 
    \ap_CS_fsm[2]_i_1__1 
       (.I0(\ap_CS_fsm[2]_i_2__1_n_0 ),
        .I1(\ap_CS_fsm[2]_i_3_n_0 ),
        .I2(ap_enable_reg_pp0_iter1__0),
        .I3(ap_enable_reg_pp0_iter0),
        .I4(ap_enable_reg_pp0_iter1_reg_n_0),
        .I5(ap_CS_fsm_pp0_stage0),
        .O(ap_NS_fsm[2]));
  LUT6 #(
    .INIT(64'h0000000080000000)) 
    \ap_CS_fsm[2]_i_2__1 
       (.I0(\AXI_video_strm_V_id_V_1_state_reg_n_0_[1] ),
        .I1(\AXI_video_strm_V_dest_V_1_state_reg_n_0_[1] ),
        .I2(ap_CS_fsm_state2),
        .I3(AXI_video_strm_V_data_V_1_ack_in),
        .I4(ap_done_INST_0_i_3_n_0),
        .I5(exitcond1_fu_200_p2__13),
        .O(\ap_CS_fsm[2]_i_2__1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair117" *) 
  LUT3 #(
    .INIT(8'h04)) 
    \ap_CS_fsm[2]_i_3 
       (.I0(ap_enable_reg_pp0_iter1_reg_n_0),
        .I1(ap_enable_reg_pp0_iter2_reg_n_0),
        .I2(ap_block_pp0_stage0_subdone),
        .O(\ap_CS_fsm[2]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h2000200022222000)) 
    \ap_CS_fsm[3]_i_1__0 
       (.I0(ap_CS_fsm_pp0_stage0),
        .I1(ap_enable_reg_pp0_iter1_reg_n_0),
        .I2(ap_enable_reg_pp0_iter0),
        .I3(ap_enable_reg_pp0_iter1__0),
        .I4(ap_enable_reg_pp0_iter2_reg_n_0),
        .I5(ap_block_pp0_stage0_subdone),
        .O(ap_NS_fsm[3]));
  LUT2 #(
    .INIT(4'h2)) 
    \ap_CS_fsm[3]_i_2 
       (.I0(exitcond_fu_212_p2),
        .I1(ap_block_pp0_stage0_subdone),
        .O(ap_enable_reg_pp0_iter1__0));
  LUT4 #(
    .INIT(16'hFF04)) 
    \ap_CS_fsm[3]_i_3 
       (.I0(exitcond_reg_266_pp0_iter1_reg),
        .I1(ap_enable_reg_pp0_iter2_reg_n_0),
        .I2(AXI_video_strm_V_data_V_1_ack_in),
        .I3(\ap_CS_fsm[3]_i_4_n_0 ),
        .O(ap_block_pp0_stage0_subdone));
  LUT6 #(
    .INIT(64'h070F0F0F00000000)) 
    \ap_CS_fsm[3]_i_4 
       (.I0(passthrough_data_str_empty_n),
        .I1(AXI_video_strm_V_data_V_1_ack_in),
        .I2(\exitcond_reg_266_reg_n_0_[0] ),
        .I3(passthrough_data_str_2_empty_n),
        .I4(passthrough_data_str_1_empty_n),
        .I5(ap_enable_reg_pp0_iter1_reg_n_0),
        .O(\ap_CS_fsm[3]_i_4_n_0 ));
  (* FSM_ENCODING = "none" *) 
  FDSE #(
    .INIT(1'b1)) 
    \ap_CS_fsm_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[0]),
        .Q(\ap_CS_fsm_reg_n_0_[0] ),
        .S(ap_rst));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[1]),
        .Q(ap_CS_fsm_state2),
        .R(ap_rst));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[2] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[2]),
        .Q(ap_CS_fsm_pp0_stage0),
        .R(ap_rst));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[3] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[3]),
        .Q(ap_CS_fsm_state6),
        .R(ap_rst));
  LUT4 #(
    .INIT(16'hEEE0)) 
    ap_done_INST_0
       (.I0(ap_done_reg_0),
        .I1(Mat2AXIvideo_U0_ap_ready),
        .I2(ap_done_reg),
        .I3(Mat2AXIvideo_1_U0_ap_ready),
        .O(ap_done));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    ap_done_INST_0_i_1
       (.I0(\AXI_video_strm_V_id_V_1_state_reg_n_0_[1] ),
        .I1(\AXI_video_strm_V_dest_V_1_state_reg_n_0_[1] ),
        .I2(ap_CS_fsm_state2),
        .I3(AXI_video_strm_V_data_V_1_ack_in),
        .I4(ap_done_INST_0_i_3_n_0),
        .I5(exitcond1_fu_200_p2__13),
        .O(Mat2AXIvideo_U0_ap_ready));
  LUT4 #(
    .INIT(16'h8000)) 
    ap_done_INST_0_i_3
       (.I0(\AXI_video_strm_V_strb_V_1_state_reg_n_0_[1] ),
        .I1(\AXI_video_strm_V_keep_V_1_state_reg_n_0_[1] ),
        .I2(AXI_video_strm_V_last_V_1_ack_in),
        .I3(AXI_video_strm_V_user_V_1_ack_in),
        .O(ap_done_INST_0_i_3_n_0));
  (* SOFT_HLUTNM = "soft_lutpair113" *) 
  LUT5 #(
    .INIT(32'h00000008)) 
    ap_done_INST_0_i_4
       (.I0(ap_done_INST_0_i_7_n_0),
        .I1(ap_done_INST_0_i_8_n_0),
        .I2(t_V_reg_173[0]),
        .I3(t_V_reg_173[1]),
        .I4(t_V_reg_173[2]),
        .O(exitcond1_fu_200_p2__13));
  (* SOFT_HLUTNM = "soft_lutpair125" *) 
  LUT4 #(
    .INIT(16'h4000)) 
    ap_done_INST_0_i_7
       (.I0(t_V_reg_173[6]),
        .I1(t_V_reg_173[5]),
        .I2(t_V_reg_173[4]),
        .I3(t_V_reg_173[3]),
        .O(ap_done_INST_0_i_7_n_0));
  LUT4 #(
    .INIT(16'h0004)) 
    ap_done_INST_0_i_8
       (.I0(t_V_reg_173[9]),
        .I1(t_V_reg_173[10]),
        .I2(t_V_reg_173[8]),
        .I3(t_V_reg_173[7]),
        .O(ap_done_INST_0_i_8_n_0));
  (* SOFT_HLUTNM = "soft_lutpair114" *) 
  LUT5 #(
    .INIT(32'h000000A8)) 
    ap_done_reg_i_1
       (.I0(ap_rst_n),
        .I1(ap_done_reg_0),
        .I2(Mat2AXIvideo_U0_ap_ready),
        .I3(ap_done_reg),
        .I4(Mat2AXIvideo_1_U0_ap_ready),
        .O(ap_done_reg_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair114" *) 
  LUT5 #(
    .INIT(32'h02020200)) 
    ap_done_reg_i_1__0
       (.I0(ap_rst_n),
        .I1(ap_done_reg_0),
        .I2(Mat2AXIvideo_U0_ap_ready),
        .I3(ap_done_reg),
        .I4(Mat2AXIvideo_1_U0_ap_ready),
        .O(ap_done_reg_reg_0));
  FDRE #(
    .INIT(1'b0)) 
    ap_done_reg_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_done_reg_i_1_n_0),
        .Q(ap_done_reg_0),
        .R(1'b0));
  LUT5 #(
    .INIT(32'h00E0E0E0)) 
    ap_enable_reg_pp0_iter0_i_1
       (.I0(ap_enable_reg_pp0_iter0),
        .I1(ap_NS_fsm1),
        .I2(ap_rst_n),
        .I3(ap_enable_reg_pp0_iter1__0),
        .I4(ap_CS_fsm_pp0_stage0),
        .O(ap_enable_reg_pp0_iter0_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    ap_enable_reg_pp0_iter0_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_enable_reg_pp0_iter0_i_1_n_0),
        .Q(ap_enable_reg_pp0_iter0),
        .R(1'b0));
  LUT5 #(
    .INIT(32'h00008A80)) 
    ap_enable_reg_pp0_iter1_i_1
       (.I0(ap_rst_n),
        .I1(ap_enable_reg_pp0_iter1_reg_n_0),
        .I2(ap_block_pp0_stage0_subdone),
        .I3(ap_enable_reg_pp0_iter0),
        .I4(ap_enable_reg_pp0_iter1__0),
        .O(ap_enable_reg_pp0_iter1_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    ap_enable_reg_pp0_iter1_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_enable_reg_pp0_iter1_i_1_n_0),
        .Q(ap_enable_reg_pp0_iter1_reg_n_0),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair117" *) 
  LUT5 #(
    .INIT(32'h00C0A0A0)) 
    ap_enable_reg_pp0_iter2_i_1
       (.I0(ap_enable_reg_pp0_iter1_reg_n_0),
        .I1(ap_enable_reg_pp0_iter2_reg_n_0),
        .I2(ap_rst_n),
        .I3(ap_NS_fsm1),
        .I4(ap_block_pp0_stage0_subdone),
        .O(ap_enable_reg_pp0_iter2_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    ap_enable_reg_pp0_iter2_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_enable_reg_pp0_iter2_i_1_n_0),
        .Q(ap_enable_reg_pp0_iter2_reg_n_0),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h0020000000000000)) 
    ap_idle_INST_0_i_1
       (.I0(\ap_CS_fsm_reg_n_0_[0] ),
        .I1(start_for_Mat2AXIvideo_U0_empty_n),
        .I2(Q),
        .I3(start_for_Mat2AXIvideo_1_U0_empty_n),
        .I4(\ap_CS_fsm_reg[0]_0 ),
        .I5(\ap_CS_fsm_reg[0]_1 ),
        .O(ap_idle));
  LUT6 #(
    .INIT(64'h8000FFFF80000000)) 
    \axi_last_V_reg_275[0]_i_1 
       (.I0(\axi_last_V_reg_275[0]_i_2_n_0 ),
        .I1(t_V_1_reg_184_reg__0[0]),
        .I2(t_V_1_reg_184_reg__0[1]),
        .I3(t_V_1_reg_184_reg__0[2]),
        .I4(axi_last_V_reg_2750),
        .I5(\axi_last_V_reg_275_reg_n_0_[0] ),
        .O(\axi_last_V_reg_275[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair120" *) 
  LUT5 #(
    .INIT(32'h20000000)) 
    \axi_last_V_reg_275[0]_i_2 
       (.I0(t_V_1_reg_184_reg__0[8]),
        .I1(t_V_1_reg_184_reg__0[7]),
        .I2(t_V_1_reg_184_reg__0[9]),
        .I3(t_V_1_reg_184_reg__0[10]),
        .I4(\axi_last_V_reg_275[0]_i_3_n_0 ),
        .O(\axi_last_V_reg_275[0]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair123" *) 
  LUT4 #(
    .INIT(16'h8000)) 
    \axi_last_V_reg_275[0]_i_3 
       (.I0(t_V_1_reg_184_reg__0[6]),
        .I1(t_V_1_reg_184_reg__0[5]),
        .I2(t_V_1_reg_184_reg__0[4]),
        .I3(t_V_1_reg_184_reg__0[3]),
        .O(\axi_last_V_reg_275[0]_i_3_n_0 ));
  FDRE \axi_last_V_reg_275_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\axi_last_V_reg_275[0]_i_1_n_0 ),
        .Q(\axi_last_V_reg_275_reg_n_0_[0] ),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair121" *) 
  LUT4 #(
    .INIT(16'hFB08)) 
    \exitcond_reg_266[0]_i_1 
       (.I0(exitcond_fu_212_p2),
        .I1(ap_CS_fsm_pp0_stage0),
        .I2(ap_block_pp0_stage0_subdone),
        .I3(\exitcond_reg_266_reg_n_0_[0] ),
        .O(\exitcond_reg_266[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair112" *) 
  LUT5 #(
    .INIT(32'h00000008)) 
    \exitcond_reg_266[0]_i_2 
       (.I0(\exitcond_reg_266[0]_i_3_n_0 ),
        .I1(\exitcond_reg_266[0]_i_4_n_0 ),
        .I2(t_V_1_reg_184_reg__0[0]),
        .I3(t_V_1_reg_184_reg__0[1]),
        .I4(t_V_1_reg_184_reg__0[2]),
        .O(exitcond_fu_212_p2));
  (* SOFT_HLUTNM = "soft_lutpair123" *) 
  LUT4 #(
    .INIT(16'h0001)) 
    \exitcond_reg_266[0]_i_3 
       (.I0(t_V_1_reg_184_reg__0[6]),
        .I1(t_V_1_reg_184_reg__0[5]),
        .I2(t_V_1_reg_184_reg__0[4]),
        .I3(t_V_1_reg_184_reg__0[3]),
        .O(\exitcond_reg_266[0]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair120" *) 
  LUT4 #(
    .INIT(16'h8000)) 
    \exitcond_reg_266[0]_i_4 
       (.I0(t_V_1_reg_184_reg__0[10]),
        .I1(t_V_1_reg_184_reg__0[9]),
        .I2(t_V_1_reg_184_reg__0[8]),
        .I3(t_V_1_reg_184_reg__0[7]),
        .O(\exitcond_reg_266[0]_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair122" *) 
  LUT4 #(
    .INIT(16'hFB08)) 
    \exitcond_reg_266_pp0_iter1_reg[0]_i_1 
       (.I0(\exitcond_reg_266_reg_n_0_[0] ),
        .I1(ap_CS_fsm_pp0_stage0),
        .I2(ap_block_pp0_stage0_subdone),
        .I3(exitcond_reg_266_pp0_iter1_reg),
        .O(\exitcond_reg_266_pp0_iter1_reg[0]_i_1_n_0 ));
  FDRE \exitcond_reg_266_pp0_iter1_reg_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\exitcond_reg_266_pp0_iter1_reg[0]_i_1_n_0 ),
        .Q(exitcond_reg_266_pp0_iter1_reg),
        .R(1'b0));
  FDRE \exitcond_reg_266_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\exitcond_reg_266[0]_i_1_n_0 ),
        .Q(\exitcond_reg_266_reg_n_0_[0] ),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair143" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \i_V_reg_261[0]_i_1 
       (.I0(t_V_reg_173[0]),
        .O(i_V_fu_206_p2[0]));
  LUT5 #(
    .INIT(32'h80000000)) 
    \i_V_reg_261[10]_i_1 
       (.I0(\AXI_video_strm_V_id_V_1_state_reg_n_0_[1] ),
        .I1(\AXI_video_strm_V_dest_V_1_state_reg_n_0_[1] ),
        .I2(ap_CS_fsm_state2),
        .I3(AXI_video_strm_V_data_V_1_ack_in),
        .I4(ap_done_INST_0_i_3_n_0),
        .O(i_V_reg_2610));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \i_V_reg_261[10]_i_2 
       (.I0(t_V_reg_173[8]),
        .I1(t_V_reg_173[6]),
        .I2(\i_V_reg_261[10]_i_3_n_0 ),
        .I3(t_V_reg_173[7]),
        .I4(t_V_reg_173[9]),
        .I5(t_V_reg_173[10]),
        .O(i_V_fu_206_p2[10]));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \i_V_reg_261[10]_i_3 
       (.I0(t_V_reg_173[5]),
        .I1(t_V_reg_173[3]),
        .I2(t_V_reg_173[1]),
        .I3(t_V_reg_173[0]),
        .I4(t_V_reg_173[2]),
        .I5(t_V_reg_173[4]),
        .O(\i_V_reg_261[10]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair143" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \i_V_reg_261[1]_i_1 
       (.I0(t_V_reg_173[0]),
        .I1(t_V_reg_173[1]),
        .O(i_V_fu_206_p2[1]));
  (* SOFT_HLUTNM = "soft_lutpair113" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \i_V_reg_261[2]_i_1 
       (.I0(t_V_reg_173[0]),
        .I1(t_V_reg_173[1]),
        .I2(t_V_reg_173[2]),
        .O(i_V_fu_206_p2[2]));
  (* SOFT_HLUTNM = "soft_lutpair115" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \i_V_reg_261[3]_i_1 
       (.I0(t_V_reg_173[1]),
        .I1(t_V_reg_173[0]),
        .I2(t_V_reg_173[2]),
        .I3(t_V_reg_173[3]),
        .O(i_V_fu_206_p2[3]));
  (* SOFT_HLUTNM = "soft_lutpair115" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \i_V_reg_261[4]_i_1 
       (.I0(t_V_reg_173[2]),
        .I1(t_V_reg_173[0]),
        .I2(t_V_reg_173[1]),
        .I3(t_V_reg_173[3]),
        .I4(t_V_reg_173[4]),
        .O(i_V_fu_206_p2[4]));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \i_V_reg_261[5]_i_1 
       (.I0(t_V_reg_173[3]),
        .I1(t_V_reg_173[1]),
        .I2(t_V_reg_173[0]),
        .I3(t_V_reg_173[2]),
        .I4(t_V_reg_173[4]),
        .I5(t_V_reg_173[5]),
        .O(i_V_fu_206_p2[5]));
  (* SOFT_HLUTNM = "soft_lutpair125" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \i_V_reg_261[6]_i_1 
       (.I0(\i_V_reg_261[10]_i_3_n_0 ),
        .I1(t_V_reg_173[6]),
        .O(i_V_fu_206_p2[6]));
  LUT3 #(
    .INIT(8'h78)) 
    \i_V_reg_261[7]_i_1 
       (.I0(\i_V_reg_261[10]_i_3_n_0 ),
        .I1(t_V_reg_173[6]),
        .I2(t_V_reg_173[7]),
        .O(i_V_fu_206_p2[7]));
  (* SOFT_HLUTNM = "soft_lutpair119" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \i_V_reg_261[8]_i_1 
       (.I0(t_V_reg_173[6]),
        .I1(\i_V_reg_261[10]_i_3_n_0 ),
        .I2(t_V_reg_173[7]),
        .I3(t_V_reg_173[8]),
        .O(i_V_fu_206_p2[8]));
  (* SOFT_HLUTNM = "soft_lutpair119" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \i_V_reg_261[9]_i_1 
       (.I0(t_V_reg_173[7]),
        .I1(\i_V_reg_261[10]_i_3_n_0 ),
        .I2(t_V_reg_173[6]),
        .I3(t_V_reg_173[8]),
        .I4(t_V_reg_173[9]),
        .O(i_V_fu_206_p2[9]));
  FDRE \i_V_reg_261_reg[0] 
       (.C(ap_clk),
        .CE(i_V_reg_2610),
        .D(i_V_fu_206_p2[0]),
        .Q(i_V_reg_261[0]),
        .R(1'b0));
  FDRE \i_V_reg_261_reg[10] 
       (.C(ap_clk),
        .CE(i_V_reg_2610),
        .D(i_V_fu_206_p2[10]),
        .Q(i_V_reg_261[10]),
        .R(1'b0));
  FDRE \i_V_reg_261_reg[1] 
       (.C(ap_clk),
        .CE(i_V_reg_2610),
        .D(i_V_fu_206_p2[1]),
        .Q(i_V_reg_261[1]),
        .R(1'b0));
  FDRE \i_V_reg_261_reg[2] 
       (.C(ap_clk),
        .CE(i_V_reg_2610),
        .D(i_V_fu_206_p2[2]),
        .Q(i_V_reg_261[2]),
        .R(1'b0));
  FDRE \i_V_reg_261_reg[3] 
       (.C(ap_clk),
        .CE(i_V_reg_2610),
        .D(i_V_fu_206_p2[3]),
        .Q(i_V_reg_261[3]),
        .R(1'b0));
  FDRE \i_V_reg_261_reg[4] 
       (.C(ap_clk),
        .CE(i_V_reg_2610),
        .D(i_V_fu_206_p2[4]),
        .Q(i_V_reg_261[4]),
        .R(1'b0));
  FDRE \i_V_reg_261_reg[5] 
       (.C(ap_clk),
        .CE(i_V_reg_2610),
        .D(i_V_fu_206_p2[5]),
        .Q(i_V_reg_261[5]),
        .R(1'b0));
  FDRE \i_V_reg_261_reg[6] 
       (.C(ap_clk),
        .CE(i_V_reg_2610),
        .D(i_V_fu_206_p2[6]),
        .Q(i_V_reg_261[6]),
        .R(1'b0));
  FDRE \i_V_reg_261_reg[7] 
       (.C(ap_clk),
        .CE(i_V_reg_2610),
        .D(i_V_fu_206_p2[7]),
        .Q(i_V_reg_261[7]),
        .R(1'b0));
  FDRE \i_V_reg_261_reg[8] 
       (.C(ap_clk),
        .CE(i_V_reg_2610),
        .D(i_V_fu_206_p2[8]),
        .Q(i_V_reg_261[8]),
        .R(1'b0));
  FDRE \i_V_reg_261_reg[9] 
       (.C(ap_clk),
        .CE(i_V_reg_2610),
        .D(i_V_fu_206_p2[9]),
        .Q(i_V_reg_261[9]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair130" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \image_out_TDATA[0]_INST_0 
       (.I0(AXI_video_strm_V_data_V_1_payload_B[0]),
        .I1(AXI_video_strm_V_data_V_1_payload_A[0]),
        .I2(AXI_video_strm_V_data_V_1_sel),
        .O(image_out_TDATA[0]));
  (* SOFT_HLUTNM = "soft_lutpair136" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \image_out_TDATA[10]_INST_0 
       (.I0(AXI_video_strm_V_data_V_1_payload_B[10]),
        .I1(AXI_video_strm_V_data_V_1_payload_A[10]),
        .I2(AXI_video_strm_V_data_V_1_sel),
        .O(image_out_TDATA[10]));
  (* SOFT_HLUTNM = "soft_lutpair137" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \image_out_TDATA[11]_INST_0 
       (.I0(AXI_video_strm_V_data_V_1_payload_B[11]),
        .I1(AXI_video_strm_V_data_V_1_payload_A[11]),
        .I2(AXI_video_strm_V_data_V_1_sel),
        .O(image_out_TDATA[11]));
  (* SOFT_HLUTNM = "soft_lutpair138" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \image_out_TDATA[12]_INST_0 
       (.I0(AXI_video_strm_V_data_V_1_payload_B[12]),
        .I1(AXI_video_strm_V_data_V_1_payload_A[12]),
        .I2(AXI_video_strm_V_data_V_1_sel),
        .O(image_out_TDATA[12]));
  (* SOFT_HLUTNM = "soft_lutpair137" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \image_out_TDATA[13]_INST_0 
       (.I0(AXI_video_strm_V_data_V_1_payload_B[13]),
        .I1(AXI_video_strm_V_data_V_1_payload_A[13]),
        .I2(AXI_video_strm_V_data_V_1_sel),
        .O(image_out_TDATA[13]));
  (* SOFT_HLUTNM = "soft_lutpair138" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \image_out_TDATA[14]_INST_0 
       (.I0(AXI_video_strm_V_data_V_1_payload_B[14]),
        .I1(AXI_video_strm_V_data_V_1_payload_A[14]),
        .I2(AXI_video_strm_V_data_V_1_sel),
        .O(image_out_TDATA[14]));
  (* SOFT_HLUTNM = "soft_lutpair139" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \image_out_TDATA[15]_INST_0 
       (.I0(AXI_video_strm_V_data_V_1_payload_B[15]),
        .I1(AXI_video_strm_V_data_V_1_payload_A[15]),
        .I2(AXI_video_strm_V_data_V_1_sel),
        .O(image_out_TDATA[15]));
  (* SOFT_HLUTNM = "soft_lutpair139" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \image_out_TDATA[16]_INST_0 
       (.I0(AXI_video_strm_V_data_V_1_payload_B[16]),
        .I1(AXI_video_strm_V_data_V_1_payload_A[16]),
        .I2(AXI_video_strm_V_data_V_1_sel),
        .O(image_out_TDATA[16]));
  (* SOFT_HLUTNM = "soft_lutpair140" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \image_out_TDATA[17]_INST_0 
       (.I0(AXI_video_strm_V_data_V_1_payload_B[17]),
        .I1(AXI_video_strm_V_data_V_1_payload_A[17]),
        .I2(AXI_video_strm_V_data_V_1_sel),
        .O(image_out_TDATA[17]));
  (* SOFT_HLUTNM = "soft_lutpair141" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \image_out_TDATA[18]_INST_0 
       (.I0(AXI_video_strm_V_data_V_1_payload_B[18]),
        .I1(AXI_video_strm_V_data_V_1_payload_A[18]),
        .I2(AXI_video_strm_V_data_V_1_sel),
        .O(image_out_TDATA[18]));
  (* SOFT_HLUTNM = "soft_lutpair141" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \image_out_TDATA[19]_INST_0 
       (.I0(AXI_video_strm_V_data_V_1_payload_B[19]),
        .I1(AXI_video_strm_V_data_V_1_payload_A[19]),
        .I2(AXI_video_strm_V_data_V_1_sel),
        .O(image_out_TDATA[19]));
  (* SOFT_HLUTNM = "soft_lutpair131" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \image_out_TDATA[1]_INST_0 
       (.I0(AXI_video_strm_V_data_V_1_payload_B[1]),
        .I1(AXI_video_strm_V_data_V_1_payload_A[1]),
        .I2(AXI_video_strm_V_data_V_1_sel),
        .O(image_out_TDATA[1]));
  (* SOFT_HLUTNM = "soft_lutpair140" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \image_out_TDATA[20]_INST_0 
       (.I0(AXI_video_strm_V_data_V_1_payload_B[20]),
        .I1(AXI_video_strm_V_data_V_1_payload_A[20]),
        .I2(AXI_video_strm_V_data_V_1_sel),
        .O(image_out_TDATA[20]));
  (* SOFT_HLUTNM = "soft_lutpair134" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \image_out_TDATA[21]_INST_0 
       (.I0(AXI_video_strm_V_data_V_1_payload_B[21]),
        .I1(AXI_video_strm_V_data_V_1_payload_A[21]),
        .I2(AXI_video_strm_V_data_V_1_sel),
        .O(image_out_TDATA[21]));
  (* SOFT_HLUTNM = "soft_lutpair132" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \image_out_TDATA[22]_INST_0 
       (.I0(AXI_video_strm_V_data_V_1_payload_B[22]),
        .I1(AXI_video_strm_V_data_V_1_payload_A[22]),
        .I2(AXI_video_strm_V_data_V_1_sel),
        .O(image_out_TDATA[22]));
  (* SOFT_HLUTNM = "soft_lutpair131" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \image_out_TDATA[23]_INST_0 
       (.I0(AXI_video_strm_V_data_V_1_payload_B[23]),
        .I1(AXI_video_strm_V_data_V_1_payload_A[23]),
        .I2(AXI_video_strm_V_data_V_1_sel),
        .O(image_out_TDATA[23]));
  (* SOFT_HLUTNM = "soft_lutpair132" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \image_out_TDATA[2]_INST_0 
       (.I0(AXI_video_strm_V_data_V_1_payload_B[2]),
        .I1(AXI_video_strm_V_data_V_1_payload_A[2]),
        .I2(AXI_video_strm_V_data_V_1_sel),
        .O(image_out_TDATA[2]));
  (* SOFT_HLUTNM = "soft_lutpair133" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \image_out_TDATA[3]_INST_0 
       (.I0(AXI_video_strm_V_data_V_1_payload_B[3]),
        .I1(AXI_video_strm_V_data_V_1_payload_A[3]),
        .I2(AXI_video_strm_V_data_V_1_sel),
        .O(image_out_TDATA[3]));
  (* SOFT_HLUTNM = "soft_lutpair134" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \image_out_TDATA[4]_INST_0 
       (.I0(AXI_video_strm_V_data_V_1_payload_B[4]),
        .I1(AXI_video_strm_V_data_V_1_payload_A[4]),
        .I2(AXI_video_strm_V_data_V_1_sel),
        .O(image_out_TDATA[4]));
  (* SOFT_HLUTNM = "soft_lutpair133" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \image_out_TDATA[5]_INST_0 
       (.I0(AXI_video_strm_V_data_V_1_payload_B[5]),
        .I1(AXI_video_strm_V_data_V_1_payload_A[5]),
        .I2(AXI_video_strm_V_data_V_1_sel),
        .O(image_out_TDATA[5]));
  (* SOFT_HLUTNM = "soft_lutpair130" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \image_out_TDATA[6]_INST_0 
       (.I0(AXI_video_strm_V_data_V_1_payload_B[6]),
        .I1(AXI_video_strm_V_data_V_1_payload_A[6]),
        .I2(AXI_video_strm_V_data_V_1_sel),
        .O(image_out_TDATA[6]));
  (* SOFT_HLUTNM = "soft_lutpair135" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \image_out_TDATA[7]_INST_0 
       (.I0(AXI_video_strm_V_data_V_1_payload_B[7]),
        .I1(AXI_video_strm_V_data_V_1_payload_A[7]),
        .I2(AXI_video_strm_V_data_V_1_sel),
        .O(image_out_TDATA[7]));
  (* SOFT_HLUTNM = "soft_lutpair135" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \image_out_TDATA[8]_INST_0 
       (.I0(AXI_video_strm_V_data_V_1_payload_B[8]),
        .I1(AXI_video_strm_V_data_V_1_payload_A[8]),
        .I2(AXI_video_strm_V_data_V_1_sel),
        .O(image_out_TDATA[8]));
  (* SOFT_HLUTNM = "soft_lutpair136" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \image_out_TDATA[9]_INST_0 
       (.I0(AXI_video_strm_V_data_V_1_payload_B[9]),
        .I1(AXI_video_strm_V_data_V_1_payload_A[9]),
        .I2(AXI_video_strm_V_data_V_1_sel),
        .O(image_out_TDATA[9]));
  LUT3 #(
    .INIT(8'hB8)) 
    \image_out_TLAST[0]_INST_0 
       (.I0(AXI_video_strm_V_last_V_1_payload_B),
        .I1(AXI_video_strm_V_last_V_1_sel),
        .I2(AXI_video_strm_V_last_V_1_payload_A),
        .O(image_out_TLAST));
  LUT3 #(
    .INIT(8'hB8)) 
    \image_out_TUSER[0]_INST_0 
       (.I0(AXI_video_strm_V_user_V_1_payload_B),
        .I1(AXI_video_strm_V_user_V_1_sel),
        .I2(AXI_video_strm_V_user_V_1_payload_A),
        .O(image_out_TUSER));
  (* SOFT_HLUTNM = "soft_lutpair142" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \t_V_1_reg_184[0]_i_1 
       (.I0(t_V_1_reg_184_reg__0[0]),
        .O(j_V_fu_218_p2[0]));
  LUT3 #(
    .INIT(8'h2A)) 
    \t_V_1_reg_184[10]_i_1 
       (.I0(ap_NS_fsm1),
        .I1(axi_last_V_reg_2750),
        .I2(ap_enable_reg_pp0_iter0),
        .O(t_V_1_reg_184));
  LUT2 #(
    .INIT(4'h8)) 
    \t_V_1_reg_184[10]_i_2 
       (.I0(ap_enable_reg_pp0_iter0),
        .I1(axi_last_V_reg_2750),
        .O(t_V_1_reg_1840));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \t_V_1_reg_184[10]_i_3 
       (.I0(t_V_1_reg_184_reg__0[8]),
        .I1(t_V_1_reg_184_reg__0[6]),
        .I2(\t_V_1_reg_184[10]_i_5_n_0 ),
        .I3(t_V_1_reg_184_reg__0[7]),
        .I4(t_V_1_reg_184_reg__0[9]),
        .I5(t_V_1_reg_184_reg__0[10]),
        .O(j_V_fu_218_p2[10]));
  (* SOFT_HLUTNM = "soft_lutpair121" *) 
  LUT3 #(
    .INIT(8'h04)) 
    \t_V_1_reg_184[10]_i_4 
       (.I0(exitcond_fu_212_p2),
        .I1(ap_CS_fsm_pp0_stage0),
        .I2(ap_block_pp0_stage0_subdone),
        .O(axi_last_V_reg_2750));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \t_V_1_reg_184[10]_i_5 
       (.I0(t_V_1_reg_184_reg__0[5]),
        .I1(t_V_1_reg_184_reg__0[3]),
        .I2(t_V_1_reg_184_reg__0[1]),
        .I3(t_V_1_reg_184_reg__0[0]),
        .I4(t_V_1_reg_184_reg__0[2]),
        .I5(t_V_1_reg_184_reg__0[4]),
        .O(\t_V_1_reg_184[10]_i_5_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair142" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \t_V_1_reg_184[1]_i_1 
       (.I0(t_V_1_reg_184_reg__0[0]),
        .I1(t_V_1_reg_184_reg__0[1]),
        .O(j_V_fu_218_p2[1]));
  (* SOFT_HLUTNM = "soft_lutpair112" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \t_V_1_reg_184[2]_i_1 
       (.I0(t_V_1_reg_184_reg__0[0]),
        .I1(t_V_1_reg_184_reg__0[1]),
        .I2(t_V_1_reg_184_reg__0[2]),
        .O(j_V_fu_218_p2[2]));
  (* SOFT_HLUTNM = "soft_lutpair118" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \t_V_1_reg_184[3]_i_1 
       (.I0(t_V_1_reg_184_reg__0[1]),
        .I1(t_V_1_reg_184_reg__0[0]),
        .I2(t_V_1_reg_184_reg__0[2]),
        .I3(t_V_1_reg_184_reg__0[3]),
        .O(j_V_fu_218_p2[3]));
  (* SOFT_HLUTNM = "soft_lutpair118" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \t_V_1_reg_184[4]_i_1 
       (.I0(t_V_1_reg_184_reg__0[2]),
        .I1(t_V_1_reg_184_reg__0[0]),
        .I2(t_V_1_reg_184_reg__0[1]),
        .I3(t_V_1_reg_184_reg__0[3]),
        .I4(t_V_1_reg_184_reg__0[4]),
        .O(j_V_fu_218_p2[4]));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \t_V_1_reg_184[5]_i_1 
       (.I0(t_V_1_reg_184_reg__0[3]),
        .I1(t_V_1_reg_184_reg__0[1]),
        .I2(t_V_1_reg_184_reg__0[0]),
        .I3(t_V_1_reg_184_reg__0[2]),
        .I4(t_V_1_reg_184_reg__0[4]),
        .I5(t_V_1_reg_184_reg__0[5]),
        .O(j_V_fu_218_p2[5]));
  (* SOFT_HLUTNM = "soft_lutpair128" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \t_V_1_reg_184[6]_i_1 
       (.I0(\t_V_1_reg_184[10]_i_5_n_0 ),
        .I1(t_V_1_reg_184_reg__0[6]),
        .O(j_V_fu_218_p2[6]));
  (* SOFT_HLUTNM = "soft_lutpair128" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \t_V_1_reg_184[7]_i_1 
       (.I0(\t_V_1_reg_184[10]_i_5_n_0 ),
        .I1(t_V_1_reg_184_reg__0[6]),
        .I2(t_V_1_reg_184_reg__0[7]),
        .O(j_V_fu_218_p2[7]));
  (* SOFT_HLUTNM = "soft_lutpair116" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \t_V_1_reg_184[8]_i_1 
       (.I0(t_V_1_reg_184_reg__0[6]),
        .I1(\t_V_1_reg_184[10]_i_5_n_0 ),
        .I2(t_V_1_reg_184_reg__0[7]),
        .I3(t_V_1_reg_184_reg__0[8]),
        .O(j_V_fu_218_p2[8]));
  (* SOFT_HLUTNM = "soft_lutpair116" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \t_V_1_reg_184[9]_i_1 
       (.I0(t_V_1_reg_184_reg__0[7]),
        .I1(\t_V_1_reg_184[10]_i_5_n_0 ),
        .I2(t_V_1_reg_184_reg__0[6]),
        .I3(t_V_1_reg_184_reg__0[8]),
        .I4(t_V_1_reg_184_reg__0[9]),
        .O(j_V_fu_218_p2[9]));
  FDRE \t_V_1_reg_184_reg[0] 
       (.C(ap_clk),
        .CE(t_V_1_reg_1840),
        .D(j_V_fu_218_p2[0]),
        .Q(t_V_1_reg_184_reg__0[0]),
        .R(t_V_1_reg_184));
  FDRE \t_V_1_reg_184_reg[10] 
       (.C(ap_clk),
        .CE(t_V_1_reg_1840),
        .D(j_V_fu_218_p2[10]),
        .Q(t_V_1_reg_184_reg__0[10]),
        .R(t_V_1_reg_184));
  FDRE \t_V_1_reg_184_reg[1] 
       (.C(ap_clk),
        .CE(t_V_1_reg_1840),
        .D(j_V_fu_218_p2[1]),
        .Q(t_V_1_reg_184_reg__0[1]),
        .R(t_V_1_reg_184));
  FDRE \t_V_1_reg_184_reg[2] 
       (.C(ap_clk),
        .CE(t_V_1_reg_1840),
        .D(j_V_fu_218_p2[2]),
        .Q(t_V_1_reg_184_reg__0[2]),
        .R(t_V_1_reg_184));
  FDRE \t_V_1_reg_184_reg[3] 
       (.C(ap_clk),
        .CE(t_V_1_reg_1840),
        .D(j_V_fu_218_p2[3]),
        .Q(t_V_1_reg_184_reg__0[3]),
        .R(t_V_1_reg_184));
  FDRE \t_V_1_reg_184_reg[4] 
       (.C(ap_clk),
        .CE(t_V_1_reg_1840),
        .D(j_V_fu_218_p2[4]),
        .Q(t_V_1_reg_184_reg__0[4]),
        .R(t_V_1_reg_184));
  FDRE \t_V_1_reg_184_reg[5] 
       (.C(ap_clk),
        .CE(t_V_1_reg_1840),
        .D(j_V_fu_218_p2[5]),
        .Q(t_V_1_reg_184_reg__0[5]),
        .R(t_V_1_reg_184));
  FDRE \t_V_1_reg_184_reg[6] 
       (.C(ap_clk),
        .CE(t_V_1_reg_1840),
        .D(j_V_fu_218_p2[6]),
        .Q(t_V_1_reg_184_reg__0[6]),
        .R(t_V_1_reg_184));
  FDRE \t_V_1_reg_184_reg[7] 
       (.C(ap_clk),
        .CE(t_V_1_reg_1840),
        .D(j_V_fu_218_p2[7]),
        .Q(t_V_1_reg_184_reg__0[7]),
        .R(t_V_1_reg_184));
  FDRE \t_V_1_reg_184_reg[8] 
       (.C(ap_clk),
        .CE(t_V_1_reg_1840),
        .D(j_V_fu_218_p2[8]),
        .Q(t_V_1_reg_184_reg__0[8]),
        .R(t_V_1_reg_184));
  FDRE \t_V_1_reg_184_reg[9] 
       (.C(ap_clk),
        .CE(t_V_1_reg_1840),
        .D(j_V_fu_218_p2[9]),
        .Q(t_V_1_reg_184_reg__0[9]),
        .R(t_V_1_reg_184));
  LUT3 #(
    .INIT(8'h08)) 
    \t_V_reg_173[10]_i_1 
       (.I0(start_for_Mat2AXIvideo_U0_empty_n),
        .I1(\ap_CS_fsm_reg_n_0_[0] ),
        .I2(ap_done_reg_0),
        .O(ap_NS_fsm112_out));
  FDRE \t_V_reg_173_reg[0] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state6),
        .D(i_V_reg_261[0]),
        .Q(t_V_reg_173[0]),
        .R(ap_NS_fsm112_out));
  FDRE \t_V_reg_173_reg[10] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state6),
        .D(i_V_reg_261[10]),
        .Q(t_V_reg_173[10]),
        .R(ap_NS_fsm112_out));
  FDRE \t_V_reg_173_reg[1] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state6),
        .D(i_V_reg_261[1]),
        .Q(t_V_reg_173[1]),
        .R(ap_NS_fsm112_out));
  FDRE \t_V_reg_173_reg[2] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state6),
        .D(i_V_reg_261[2]),
        .Q(t_V_reg_173[2]),
        .R(ap_NS_fsm112_out));
  FDRE \t_V_reg_173_reg[3] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state6),
        .D(i_V_reg_261[3]),
        .Q(t_V_reg_173[3]),
        .R(ap_NS_fsm112_out));
  FDRE \t_V_reg_173_reg[4] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state6),
        .D(i_V_reg_261[4]),
        .Q(t_V_reg_173[4]),
        .R(ap_NS_fsm112_out));
  FDRE \t_V_reg_173_reg[5] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state6),
        .D(i_V_reg_261[5]),
        .Q(t_V_reg_173[5]),
        .R(ap_NS_fsm112_out));
  FDRE \t_V_reg_173_reg[6] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state6),
        .D(i_V_reg_261[6]),
        .Q(t_V_reg_173[6]),
        .R(ap_NS_fsm112_out));
  FDRE \t_V_reg_173_reg[7] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state6),
        .D(i_V_reg_261[7]),
        .Q(t_V_reg_173[7]),
        .R(ap_NS_fsm112_out));
  FDRE \t_V_reg_173_reg[8] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state6),
        .D(i_V_reg_261[8]),
        .Q(t_V_reg_173[8]),
        .R(ap_NS_fsm112_out));
  FDRE \t_V_reg_173_reg[9] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state6),
        .D(i_V_reg_261[9]),
        .Q(t_V_reg_173[9]),
        .R(ap_NS_fsm112_out));
  LUT5 #(
    .INIT(32'h0000AAEA)) 
    \tmp_user_V_fu_122[0]_i_1 
       (.I0(tmp_user_V_fu_122),
        .I1(start_for_Mat2AXIvideo_U0_empty_n),
        .I2(\ap_CS_fsm_reg_n_0_[0] ),
        .I3(ap_done_reg_0),
        .I4(Mat2AXIvideo_U0_img_data_stream_2_V_read),
        .O(\tmp_user_V_fu_122[0]_i_1_n_0 ));
  FDRE \tmp_user_V_fu_122_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\tmp_user_V_fu_122[0]_i_1_n_0 ),
        .Q(tmp_user_V_fu_122),
        .R(1'b0));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Mat2AXIvideo_1
   (ap_done_reg,
    crop_out_TVALID,
    Q,
    Mat2AXIvideo_1_U0_ap_ready,
    Mat2AXIvideo_1_U0_img_data_stream_2_V_read,
    crop_out_TUSER,
    crop_out_TLAST,
    crop_out_TDATA,
    ap_rst,
    ap_clk,
    ap_done_reg_reg_0,
    ap_rst_n,
    start_for_Mat2AXIvideo_1_U0_empty_n,
    crop_out_TREADY,
    crop_data_data_strea_empty_n,
    crop_data_data_strea_2_empty_n,
    crop_data_data_strea_1_empty_n,
    D);
  output ap_done_reg;
  output crop_out_TVALID;
  output [0:0]Q;
  output Mat2AXIvideo_1_U0_ap_ready;
  output Mat2AXIvideo_1_U0_img_data_stream_2_V_read;
  output [0:0]crop_out_TUSER;
  output [0:0]crop_out_TLAST;
  output [23:0]crop_out_TDATA;
  input ap_rst;
  input ap_clk;
  input ap_done_reg_reg_0;
  input ap_rst_n;
  input start_for_Mat2AXIvideo_1_U0_empty_n;
  input crop_out_TREADY;
  input crop_data_data_strea_empty_n;
  input crop_data_data_strea_2_empty_n;
  input crop_data_data_strea_1_empty_n;
  input [23:0]D;

  wire AXI_video_strm_V_data_V_1_ack_in;
  wire AXI_video_strm_V_data_V_1_load_A;
  wire AXI_video_strm_V_data_V_1_load_B;
  wire \AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[0] ;
  wire \AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[10] ;
  wire \AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[11] ;
  wire \AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[12] ;
  wire \AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[13] ;
  wire \AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[14] ;
  wire \AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[15] ;
  wire \AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[16] ;
  wire \AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[17] ;
  wire \AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[18] ;
  wire \AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[19] ;
  wire \AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[1] ;
  wire \AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[20] ;
  wire \AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[21] ;
  wire \AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[22] ;
  wire \AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[23] ;
  wire \AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[2] ;
  wire \AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[3] ;
  wire \AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[4] ;
  wire \AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[5] ;
  wire \AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[6] ;
  wire \AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[7] ;
  wire \AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[8] ;
  wire \AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[9] ;
  wire \AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[0] ;
  wire \AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[10] ;
  wire \AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[11] ;
  wire \AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[12] ;
  wire \AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[13] ;
  wire \AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[14] ;
  wire \AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[15] ;
  wire \AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[16] ;
  wire \AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[17] ;
  wire \AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[18] ;
  wire \AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[19] ;
  wire \AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[1] ;
  wire \AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[20] ;
  wire \AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[21] ;
  wire \AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[22] ;
  wire \AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[23] ;
  wire \AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[2] ;
  wire \AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[3] ;
  wire \AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[4] ;
  wire \AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[5] ;
  wire \AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[6] ;
  wire \AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[7] ;
  wire \AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[8] ;
  wire \AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[9] ;
  wire AXI_video_strm_V_data_V_1_sel_rd_i_1__0_n_0;
  wire AXI_video_strm_V_data_V_1_sel_rd_reg_n_0;
  wire AXI_video_strm_V_data_V_1_sel_wr;
  wire AXI_video_strm_V_data_V_1_sel_wr_i_1__0_n_0;
  wire [1:1]AXI_video_strm_V_data_V_1_state;
  wire \AXI_video_strm_V_data_V_1_state[0]_i_1__0_n_0 ;
  wire \AXI_video_strm_V_data_V_1_state_reg_n_0_[0] ;
  wire [1:1]AXI_video_strm_V_dest_V_1_state;
  wire \AXI_video_strm_V_dest_V_1_state[0]_i_1__0_n_0 ;
  wire \AXI_video_strm_V_dest_V_1_state_reg_n_0_[1] ;
  wire [1:1]AXI_video_strm_V_id_V_1_state;
  wire \AXI_video_strm_V_id_V_1_state[0]_i_1__0_n_0 ;
  wire \AXI_video_strm_V_id_V_1_state_reg_n_0_[0] ;
  wire \AXI_video_strm_V_id_V_1_state_reg_n_0_[1] ;
  wire [1:1]AXI_video_strm_V_keep_V_1_state;
  wire \AXI_video_strm_V_keep_V_1_state[0]_i_1__0_n_0 ;
  wire \AXI_video_strm_V_keep_V_1_state_reg_n_0_[0] ;
  wire \AXI_video_strm_V_keep_V_1_state_reg_n_0_[1] ;
  wire AXI_video_strm_V_last_V_1_ack_in;
  wire AXI_video_strm_V_last_V_1_payload_A;
  wire \AXI_video_strm_V_last_V_1_payload_A[0]_i_1__0_n_0 ;
  wire AXI_video_strm_V_last_V_1_payload_B;
  wire \AXI_video_strm_V_last_V_1_payload_B[0]_i_1__0_n_0 ;
  wire AXI_video_strm_V_last_V_1_sel;
  wire AXI_video_strm_V_last_V_1_sel_rd_i_1__0_n_0;
  wire AXI_video_strm_V_last_V_1_sel_wr;
  wire AXI_video_strm_V_last_V_1_sel_wr_i_1__0_n_0;
  wire [1:1]AXI_video_strm_V_last_V_1_state;
  wire \AXI_video_strm_V_last_V_1_state[0]_i_1__0_n_0 ;
  wire \AXI_video_strm_V_last_V_1_state_reg_n_0_[0] ;
  wire [1:1]AXI_video_strm_V_strb_V_1_state;
  wire \AXI_video_strm_V_strb_V_1_state[0]_i_1__0_n_0 ;
  wire \AXI_video_strm_V_strb_V_1_state_reg_n_0_[0] ;
  wire \AXI_video_strm_V_strb_V_1_state_reg_n_0_[1] ;
  wire AXI_video_strm_V_user_V_1_ack_in;
  wire AXI_video_strm_V_user_V_1_payload_A;
  wire \AXI_video_strm_V_user_V_1_payload_A[0]_i_1__0_n_0 ;
  wire AXI_video_strm_V_user_V_1_payload_B;
  wire \AXI_video_strm_V_user_V_1_payload_B[0]_i_1__0_n_0 ;
  wire AXI_video_strm_V_user_V_1_sel;
  wire AXI_video_strm_V_user_V_1_sel_rd_i_1__0_n_0;
  wire AXI_video_strm_V_user_V_1_sel_wr;
  wire AXI_video_strm_V_user_V_1_sel_wr_i_1__0_n_0;
  wire [1:1]AXI_video_strm_V_user_V_1_state;
  wire \AXI_video_strm_V_user_V_1_state[0]_i_1__0_n_0 ;
  wire \AXI_video_strm_V_user_V_1_state_reg_n_0_[0] ;
  wire [23:0]D;
  wire Mat2AXIvideo_1_U0_ap_ready;
  wire Mat2AXIvideo_1_U0_img_data_stream_2_V_read;
  wire [0:0]Q;
  wire \ap_CS_fsm[2]_i_3__0_n_0 ;
  wire \ap_CS_fsm[2]_i_5_n_0 ;
  wire \ap_CS_fsm[3]_i_4__0_n_0 ;
  wire ap_CS_fsm_pp0_stage0;
  wire \ap_CS_fsm_reg_n_0_[3] ;
  wire ap_CS_fsm_state2;
  wire [3:0]ap_NS_fsm;
  wire ap_NS_fsm1;
  wire ap_NS_fsm112_out;
  wire ap_NS_fsm39_out__1;
  wire ap_block_pp0_stage0_subdone;
  wire ap_clk;
  wire ap_done_INST_0_i_10_n_0;
  wire ap_done_INST_0_i_5_n_0;
  wire ap_done_INST_0_i_9_n_0;
  wire ap_done_reg;
  wire ap_done_reg_reg_0;
  wire ap_enable_reg_pp0_iter0;
  wire ap_enable_reg_pp0_iter0_i_1__0_n_0;
  wire ap_enable_reg_pp0_iter1_i_1__0_n_0;
  wire ap_enable_reg_pp0_iter1_reg_n_0;
  wire ap_enable_reg_pp0_iter2_i_1__0_n_0;
  wire ap_enable_reg_pp0_iter2_reg_n_0;
  wire ap_rst;
  wire ap_rst_n;
  wire axi_last_V_reg_2710;
  wire \axi_last_V_reg_271[0]_i_1_n_0 ;
  wire \axi_last_V_reg_271[0]_i_2_n_0 ;
  wire \axi_last_V_reg_271_reg_n_0_[0] ;
  wire crop_data_data_strea_1_empty_n;
  wire crop_data_data_strea_2_empty_n;
  wire crop_data_data_strea_empty_n;
  wire [23:0]crop_out_TDATA;
  wire [0:0]crop_out_TLAST;
  wire crop_out_TREADY;
  wire [0:0]crop_out_TUSER;
  wire crop_out_TVALID;
  wire exitcond2_fu_196_p2__13;
  wire exitcond_fu_208_p2;
  wire \exitcond_reg_262[0]_i_1_n_0 ;
  wire \exitcond_reg_262[0]_i_3_n_0 ;
  wire \exitcond_reg_262[0]_i_4_n_0 ;
  wire exitcond_reg_262_pp0_iter1_reg;
  wire \exitcond_reg_262_pp0_iter1_reg[0]_i_1_n_0 ;
  wire \exitcond_reg_262_reg_n_0_[0] ;
  wire [10:0]i_V_fu_202_p2;
  wire [10:0]i_V_reg_257;
  wire i_V_reg_2570;
  wire \i_V_reg_257[10]_i_3_n_0 ;
  wire [10:0]j_V_fu_214_p2;
  wire p_10_in;
  wire start_for_Mat2AXIvideo_1_U0_empty_n;
  wire t_V_1_reg_180;
  wire t_V_1_reg_1800;
  wire \t_V_1_reg_180[10]_i_5_n_0 ;
  wire [10:0]t_V_1_reg_180_reg__0;
  wire [10:0]t_V_reg_169;
  wire tmp_user_V_fu_118;
  wire \tmp_user_V_fu_118[0]_i_1_n_0 ;

  LUT3 #(
    .INIT(8'h0D)) 
    \AXI_video_strm_V_data_V_1_payload_A[23]_i_1__0 
       (.I0(\AXI_video_strm_V_data_V_1_state_reg_n_0_[0] ),
        .I1(AXI_video_strm_V_data_V_1_ack_in),
        .I2(AXI_video_strm_V_data_V_1_sel_wr),
        .O(AXI_video_strm_V_data_V_1_load_A));
  FDRE \AXI_video_strm_V_data_V_1_payload_A_reg[0] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_A),
        .D(D[0]),
        .Q(\AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[0] ),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_A_reg[10] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_A),
        .D(D[10]),
        .Q(\AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[10] ),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_A_reg[11] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_A),
        .D(D[11]),
        .Q(\AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[11] ),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_A_reg[12] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_A),
        .D(D[12]),
        .Q(\AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[12] ),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_A_reg[13] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_A),
        .D(D[13]),
        .Q(\AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[13] ),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_A_reg[14] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_A),
        .D(D[14]),
        .Q(\AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[14] ),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_A_reg[15] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_A),
        .D(D[15]),
        .Q(\AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[15] ),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_A_reg[16] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_A),
        .D(D[16]),
        .Q(\AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[16] ),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_A_reg[17] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_A),
        .D(D[17]),
        .Q(\AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[17] ),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_A_reg[18] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_A),
        .D(D[18]),
        .Q(\AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[18] ),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_A_reg[19] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_A),
        .D(D[19]),
        .Q(\AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[19] ),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_A_reg[1] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_A),
        .D(D[1]),
        .Q(\AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[1] ),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_A_reg[20] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_A),
        .D(D[20]),
        .Q(\AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[20] ),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_A_reg[21] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_A),
        .D(D[21]),
        .Q(\AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[21] ),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_A_reg[22] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_A),
        .D(D[22]),
        .Q(\AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[22] ),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_A_reg[23] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_A),
        .D(D[23]),
        .Q(\AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[23] ),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_A_reg[2] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_A),
        .D(D[2]),
        .Q(\AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[2] ),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_A_reg[3] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_A),
        .D(D[3]),
        .Q(\AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[3] ),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_A_reg[4] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_A),
        .D(D[4]),
        .Q(\AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[4] ),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_A_reg[5] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_A),
        .D(D[5]),
        .Q(\AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[5] ),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_A_reg[6] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_A),
        .D(D[6]),
        .Q(\AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[6] ),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_A_reg[7] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_A),
        .D(D[7]),
        .Q(\AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[7] ),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_A_reg[8] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_A),
        .D(D[8]),
        .Q(\AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[8] ),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_A_reg[9] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_A),
        .D(D[9]),
        .Q(\AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[9] ),
        .R(1'b0));
  LUT3 #(
    .INIT(8'hA2)) 
    \AXI_video_strm_V_data_V_1_payload_B[23]_i_1__0 
       (.I0(AXI_video_strm_V_data_V_1_sel_wr),
        .I1(\AXI_video_strm_V_data_V_1_state_reg_n_0_[0] ),
        .I2(AXI_video_strm_V_data_V_1_ack_in),
        .O(AXI_video_strm_V_data_V_1_load_B));
  FDRE \AXI_video_strm_V_data_V_1_payload_B_reg[0] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_B),
        .D(D[0]),
        .Q(\AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[0] ),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_B_reg[10] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_B),
        .D(D[10]),
        .Q(\AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[10] ),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_B_reg[11] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_B),
        .D(D[11]),
        .Q(\AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[11] ),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_B_reg[12] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_B),
        .D(D[12]),
        .Q(\AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[12] ),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_B_reg[13] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_B),
        .D(D[13]),
        .Q(\AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[13] ),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_B_reg[14] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_B),
        .D(D[14]),
        .Q(\AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[14] ),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_B_reg[15] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_B),
        .D(D[15]),
        .Q(\AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[15] ),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_B_reg[16] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_B),
        .D(D[16]),
        .Q(\AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[16] ),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_B_reg[17] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_B),
        .D(D[17]),
        .Q(\AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[17] ),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_B_reg[18] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_B),
        .D(D[18]),
        .Q(\AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[18] ),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_B_reg[19] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_B),
        .D(D[19]),
        .Q(\AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[19] ),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_B_reg[1] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_B),
        .D(D[1]),
        .Q(\AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[1] ),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_B_reg[20] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_B),
        .D(D[20]),
        .Q(\AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[20] ),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_B_reg[21] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_B),
        .D(D[21]),
        .Q(\AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[21] ),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_B_reg[22] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_B),
        .D(D[22]),
        .Q(\AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[22] ),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_B_reg[23] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_B),
        .D(D[23]),
        .Q(\AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[23] ),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_B_reg[2] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_B),
        .D(D[2]),
        .Q(\AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[2] ),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_B_reg[3] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_B),
        .D(D[3]),
        .Q(\AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[3] ),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_B_reg[4] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_B),
        .D(D[4]),
        .Q(\AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[4] ),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_B_reg[5] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_B),
        .D(D[5]),
        .Q(\AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[5] ),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_B_reg[6] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_B),
        .D(D[6]),
        .Q(\AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[6] ),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_B_reg[7] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_B),
        .D(D[7]),
        .Q(\AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[7] ),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_B_reg[8] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_B),
        .D(D[8]),
        .Q(\AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[8] ),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_B_reg[9] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_B),
        .D(D[9]),
        .Q(\AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[9] ),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair90" *) 
  LUT3 #(
    .INIT(8'h78)) 
    AXI_video_strm_V_data_V_1_sel_rd_i_1__0
       (.I0(crop_out_TREADY),
        .I1(\AXI_video_strm_V_data_V_1_state_reg_n_0_[0] ),
        .I2(AXI_video_strm_V_data_V_1_sel_rd_reg_n_0),
        .O(AXI_video_strm_V_data_V_1_sel_rd_i_1__0_n_0));
  FDRE #(
    .INIT(1'b0)) 
    AXI_video_strm_V_data_V_1_sel_rd_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(AXI_video_strm_V_data_V_1_sel_rd_i_1__0_n_0),
        .Q(AXI_video_strm_V_data_V_1_sel_rd_reg_n_0),
        .R(ap_rst));
  LUT3 #(
    .INIT(8'h78)) 
    AXI_video_strm_V_data_V_1_sel_wr_i_1__0
       (.I0(AXI_video_strm_V_data_V_1_ack_in),
        .I1(Mat2AXIvideo_1_U0_img_data_stream_2_V_read),
        .I2(AXI_video_strm_V_data_V_1_sel_wr),
        .O(AXI_video_strm_V_data_V_1_sel_wr_i_1__0_n_0));
  FDRE #(
    .INIT(1'b0)) 
    AXI_video_strm_V_data_V_1_sel_wr_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(AXI_video_strm_V_data_V_1_sel_wr_i_1__0_n_0),
        .Q(AXI_video_strm_V_data_V_1_sel_wr),
        .R(ap_rst));
  LUT5 #(
    .INIT(32'hA8A80888)) 
    \AXI_video_strm_V_data_V_1_state[0]_i_1__0 
       (.I0(ap_rst_n),
        .I1(\AXI_video_strm_V_data_V_1_state_reg_n_0_[0] ),
        .I2(AXI_video_strm_V_data_V_1_ack_in),
        .I3(crop_out_TREADY),
        .I4(Mat2AXIvideo_1_U0_img_data_stream_2_V_read),
        .O(\AXI_video_strm_V_data_V_1_state[0]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair90" *) 
  LUT4 #(
    .INIT(16'hF5FD)) 
    \AXI_video_strm_V_data_V_1_state[1]_i_1__0 
       (.I0(\AXI_video_strm_V_data_V_1_state_reg_n_0_[0] ),
        .I1(AXI_video_strm_V_data_V_1_ack_in),
        .I2(crop_out_TREADY),
        .I3(Mat2AXIvideo_1_U0_img_data_stream_2_V_read),
        .O(AXI_video_strm_V_data_V_1_state));
  FDRE #(
    .INIT(1'b0)) 
    \AXI_video_strm_V_data_V_1_state_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\AXI_video_strm_V_data_V_1_state[0]_i_1__0_n_0 ),
        .Q(\AXI_video_strm_V_data_V_1_state_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \AXI_video_strm_V_data_V_1_state_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(AXI_video_strm_V_data_V_1_state),
        .Q(AXI_video_strm_V_data_V_1_ack_in),
        .R(ap_rst));
  LUT5 #(
    .INIT(32'hAAA02A00)) 
    \AXI_video_strm_V_dest_V_1_state[0]_i_1__0 
       (.I0(ap_rst_n),
        .I1(crop_out_TREADY),
        .I2(\AXI_video_strm_V_dest_V_1_state_reg_n_0_[1] ),
        .I3(crop_out_TVALID),
        .I4(Mat2AXIvideo_1_U0_img_data_stream_2_V_read),
        .O(\AXI_video_strm_V_dest_V_1_state[0]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair91" *) 
  LUT4 #(
    .INIT(16'h0008)) 
    \AXI_video_strm_V_dest_V_1_state[0]_i_2__0 
       (.I0(ap_enable_reg_pp0_iter1_reg_n_0),
        .I1(ap_CS_fsm_pp0_stage0),
        .I2(\exitcond_reg_262_reg_n_0_[0] ),
        .I3(ap_block_pp0_stage0_subdone),
        .O(Mat2AXIvideo_1_U0_img_data_stream_2_V_read));
  LUT4 #(
    .INIT(16'hAFEF)) 
    \AXI_video_strm_V_dest_V_1_state[1]_i_1__0 
       (.I0(crop_out_TREADY),
        .I1(\AXI_video_strm_V_dest_V_1_state_reg_n_0_[1] ),
        .I2(crop_out_TVALID),
        .I3(Mat2AXIvideo_1_U0_img_data_stream_2_V_read),
        .O(AXI_video_strm_V_dest_V_1_state));
  FDRE #(
    .INIT(1'b0)) 
    \AXI_video_strm_V_dest_V_1_state_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\AXI_video_strm_V_dest_V_1_state[0]_i_1__0_n_0 ),
        .Q(crop_out_TVALID),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \AXI_video_strm_V_dest_V_1_state_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(AXI_video_strm_V_dest_V_1_state),
        .Q(\AXI_video_strm_V_dest_V_1_state_reg_n_0_[1] ),
        .R(ap_rst));
  LUT5 #(
    .INIT(32'hAAA02A00)) 
    \AXI_video_strm_V_id_V_1_state[0]_i_1__0 
       (.I0(ap_rst_n),
        .I1(crop_out_TREADY),
        .I2(\AXI_video_strm_V_id_V_1_state_reg_n_0_[1] ),
        .I3(\AXI_video_strm_V_id_V_1_state_reg_n_0_[0] ),
        .I4(Mat2AXIvideo_1_U0_img_data_stream_2_V_read),
        .O(\AXI_video_strm_V_id_V_1_state[0]_i_1__0_n_0 ));
  LUT4 #(
    .INIT(16'hAFEF)) 
    \AXI_video_strm_V_id_V_1_state[1]_i_1__0 
       (.I0(crop_out_TREADY),
        .I1(\AXI_video_strm_V_id_V_1_state_reg_n_0_[1] ),
        .I2(\AXI_video_strm_V_id_V_1_state_reg_n_0_[0] ),
        .I3(Mat2AXIvideo_1_U0_img_data_stream_2_V_read),
        .O(AXI_video_strm_V_id_V_1_state));
  FDRE #(
    .INIT(1'b0)) 
    \AXI_video_strm_V_id_V_1_state_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\AXI_video_strm_V_id_V_1_state[0]_i_1__0_n_0 ),
        .Q(\AXI_video_strm_V_id_V_1_state_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \AXI_video_strm_V_id_V_1_state_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(AXI_video_strm_V_id_V_1_state),
        .Q(\AXI_video_strm_V_id_V_1_state_reg_n_0_[1] ),
        .R(ap_rst));
  LUT5 #(
    .INIT(32'hAAA02A00)) 
    \AXI_video_strm_V_keep_V_1_state[0]_i_1__0 
       (.I0(ap_rst_n),
        .I1(crop_out_TREADY),
        .I2(\AXI_video_strm_V_keep_V_1_state_reg_n_0_[1] ),
        .I3(\AXI_video_strm_V_keep_V_1_state_reg_n_0_[0] ),
        .I4(Mat2AXIvideo_1_U0_img_data_stream_2_V_read),
        .O(\AXI_video_strm_V_keep_V_1_state[0]_i_1__0_n_0 ));
  LUT4 #(
    .INIT(16'hAFEF)) 
    \AXI_video_strm_V_keep_V_1_state[1]_i_1__0 
       (.I0(crop_out_TREADY),
        .I1(\AXI_video_strm_V_keep_V_1_state_reg_n_0_[1] ),
        .I2(\AXI_video_strm_V_keep_V_1_state_reg_n_0_[0] ),
        .I3(Mat2AXIvideo_1_U0_img_data_stream_2_V_read),
        .O(AXI_video_strm_V_keep_V_1_state));
  FDRE #(
    .INIT(1'b0)) 
    \AXI_video_strm_V_keep_V_1_state_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\AXI_video_strm_V_keep_V_1_state[0]_i_1__0_n_0 ),
        .Q(\AXI_video_strm_V_keep_V_1_state_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \AXI_video_strm_V_keep_V_1_state_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(AXI_video_strm_V_keep_V_1_state),
        .Q(\AXI_video_strm_V_keep_V_1_state_reg_n_0_[1] ),
        .R(ap_rst));
  LUT5 #(
    .INIT(32'hFFAE00A2)) 
    \AXI_video_strm_V_last_V_1_payload_A[0]_i_1__0 
       (.I0(\axi_last_V_reg_271_reg_n_0_[0] ),
        .I1(\AXI_video_strm_V_last_V_1_state_reg_n_0_[0] ),
        .I2(AXI_video_strm_V_last_V_1_ack_in),
        .I3(AXI_video_strm_V_last_V_1_sel_wr),
        .I4(AXI_video_strm_V_last_V_1_payload_A),
        .O(\AXI_video_strm_V_last_V_1_payload_A[0]_i_1__0_n_0 ));
  FDRE \AXI_video_strm_V_last_V_1_payload_A_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\AXI_video_strm_V_last_V_1_payload_A[0]_i_1__0_n_0 ),
        .Q(AXI_video_strm_V_last_V_1_payload_A),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hBBFB8808)) 
    \AXI_video_strm_V_last_V_1_payload_B[0]_i_1__0 
       (.I0(\axi_last_V_reg_271_reg_n_0_[0] ),
        .I1(AXI_video_strm_V_last_V_1_sel_wr),
        .I2(\AXI_video_strm_V_last_V_1_state_reg_n_0_[0] ),
        .I3(AXI_video_strm_V_last_V_1_ack_in),
        .I4(AXI_video_strm_V_last_V_1_payload_B),
        .O(\AXI_video_strm_V_last_V_1_payload_B[0]_i_1__0_n_0 ));
  FDRE \AXI_video_strm_V_last_V_1_payload_B_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\AXI_video_strm_V_last_V_1_payload_B[0]_i_1__0_n_0 ),
        .Q(AXI_video_strm_V_last_V_1_payload_B),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair94" *) 
  LUT3 #(
    .INIT(8'h78)) 
    AXI_video_strm_V_last_V_1_sel_rd_i_1__0
       (.I0(crop_out_TREADY),
        .I1(\AXI_video_strm_V_last_V_1_state_reg_n_0_[0] ),
        .I2(AXI_video_strm_V_last_V_1_sel),
        .O(AXI_video_strm_V_last_V_1_sel_rd_i_1__0_n_0));
  FDRE #(
    .INIT(1'b0)) 
    AXI_video_strm_V_last_V_1_sel_rd_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(AXI_video_strm_V_last_V_1_sel_rd_i_1__0_n_0),
        .Q(AXI_video_strm_V_last_V_1_sel),
        .R(ap_rst));
  (* SOFT_HLUTNM = "soft_lutpair96" *) 
  LUT3 #(
    .INIT(8'h78)) 
    AXI_video_strm_V_last_V_1_sel_wr_i_1__0
       (.I0(AXI_video_strm_V_last_V_1_ack_in),
        .I1(Mat2AXIvideo_1_U0_img_data_stream_2_V_read),
        .I2(AXI_video_strm_V_last_V_1_sel_wr),
        .O(AXI_video_strm_V_last_V_1_sel_wr_i_1__0_n_0));
  FDRE #(
    .INIT(1'b0)) 
    AXI_video_strm_V_last_V_1_sel_wr_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(AXI_video_strm_V_last_V_1_sel_wr_i_1__0_n_0),
        .Q(AXI_video_strm_V_last_V_1_sel_wr),
        .R(ap_rst));
  LUT5 #(
    .INIT(32'hA8A80888)) 
    \AXI_video_strm_V_last_V_1_state[0]_i_1__0 
       (.I0(ap_rst_n),
        .I1(\AXI_video_strm_V_last_V_1_state_reg_n_0_[0] ),
        .I2(AXI_video_strm_V_last_V_1_ack_in),
        .I3(crop_out_TREADY),
        .I4(Mat2AXIvideo_1_U0_img_data_stream_2_V_read),
        .O(\AXI_video_strm_V_last_V_1_state[0]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair94" *) 
  LUT4 #(
    .INIT(16'hF5FD)) 
    \AXI_video_strm_V_last_V_1_state[1]_i_1__0 
       (.I0(\AXI_video_strm_V_last_V_1_state_reg_n_0_[0] ),
        .I1(AXI_video_strm_V_last_V_1_ack_in),
        .I2(crop_out_TREADY),
        .I3(Mat2AXIvideo_1_U0_img_data_stream_2_V_read),
        .O(AXI_video_strm_V_last_V_1_state));
  FDRE #(
    .INIT(1'b0)) 
    \AXI_video_strm_V_last_V_1_state_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\AXI_video_strm_V_last_V_1_state[0]_i_1__0_n_0 ),
        .Q(\AXI_video_strm_V_last_V_1_state_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \AXI_video_strm_V_last_V_1_state_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(AXI_video_strm_V_last_V_1_state),
        .Q(AXI_video_strm_V_last_V_1_ack_in),
        .R(ap_rst));
  LUT5 #(
    .INIT(32'hAAA02A00)) 
    \AXI_video_strm_V_strb_V_1_state[0]_i_1__0 
       (.I0(ap_rst_n),
        .I1(crop_out_TREADY),
        .I2(\AXI_video_strm_V_strb_V_1_state_reg_n_0_[1] ),
        .I3(\AXI_video_strm_V_strb_V_1_state_reg_n_0_[0] ),
        .I4(Mat2AXIvideo_1_U0_img_data_stream_2_V_read),
        .O(\AXI_video_strm_V_strb_V_1_state[0]_i_1__0_n_0 ));
  LUT4 #(
    .INIT(16'hAFEF)) 
    \AXI_video_strm_V_strb_V_1_state[1]_i_1__0 
       (.I0(crop_out_TREADY),
        .I1(\AXI_video_strm_V_strb_V_1_state_reg_n_0_[1] ),
        .I2(\AXI_video_strm_V_strb_V_1_state_reg_n_0_[0] ),
        .I3(Mat2AXIvideo_1_U0_img_data_stream_2_V_read),
        .O(AXI_video_strm_V_strb_V_1_state));
  FDRE #(
    .INIT(1'b0)) 
    \AXI_video_strm_V_strb_V_1_state_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\AXI_video_strm_V_strb_V_1_state[0]_i_1__0_n_0 ),
        .Q(\AXI_video_strm_V_strb_V_1_state_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \AXI_video_strm_V_strb_V_1_state_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(AXI_video_strm_V_strb_V_1_state),
        .Q(\AXI_video_strm_V_strb_V_1_state_reg_n_0_[1] ),
        .R(ap_rst));
  LUT5 #(
    .INIT(32'hFFAE00A2)) 
    \AXI_video_strm_V_user_V_1_payload_A[0]_i_1__0 
       (.I0(tmp_user_V_fu_118),
        .I1(\AXI_video_strm_V_user_V_1_state_reg_n_0_[0] ),
        .I2(AXI_video_strm_V_user_V_1_ack_in),
        .I3(AXI_video_strm_V_user_V_1_sel_wr),
        .I4(AXI_video_strm_V_user_V_1_payload_A),
        .O(\AXI_video_strm_V_user_V_1_payload_A[0]_i_1__0_n_0 ));
  FDRE \AXI_video_strm_V_user_V_1_payload_A_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\AXI_video_strm_V_user_V_1_payload_A[0]_i_1__0_n_0 ),
        .Q(AXI_video_strm_V_user_V_1_payload_A),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hBBFB8808)) 
    \AXI_video_strm_V_user_V_1_payload_B[0]_i_1__0 
       (.I0(tmp_user_V_fu_118),
        .I1(AXI_video_strm_V_user_V_1_sel_wr),
        .I2(\AXI_video_strm_V_user_V_1_state_reg_n_0_[0] ),
        .I3(AXI_video_strm_V_user_V_1_ack_in),
        .I4(AXI_video_strm_V_user_V_1_payload_B),
        .O(\AXI_video_strm_V_user_V_1_payload_B[0]_i_1__0_n_0 ));
  FDRE \AXI_video_strm_V_user_V_1_payload_B_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\AXI_video_strm_V_user_V_1_payload_B[0]_i_1__0_n_0 ),
        .Q(AXI_video_strm_V_user_V_1_payload_B),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair95" *) 
  LUT3 #(
    .INIT(8'h78)) 
    AXI_video_strm_V_user_V_1_sel_rd_i_1__0
       (.I0(crop_out_TREADY),
        .I1(\AXI_video_strm_V_user_V_1_state_reg_n_0_[0] ),
        .I2(AXI_video_strm_V_user_V_1_sel),
        .O(AXI_video_strm_V_user_V_1_sel_rd_i_1__0_n_0));
  FDRE #(
    .INIT(1'b0)) 
    AXI_video_strm_V_user_V_1_sel_rd_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(AXI_video_strm_V_user_V_1_sel_rd_i_1__0_n_0),
        .Q(AXI_video_strm_V_user_V_1_sel),
        .R(ap_rst));
  (* SOFT_HLUTNM = "soft_lutpair96" *) 
  LUT3 #(
    .INIT(8'h78)) 
    AXI_video_strm_V_user_V_1_sel_wr_i_1__0
       (.I0(AXI_video_strm_V_user_V_1_ack_in),
        .I1(Mat2AXIvideo_1_U0_img_data_stream_2_V_read),
        .I2(AXI_video_strm_V_user_V_1_sel_wr),
        .O(AXI_video_strm_V_user_V_1_sel_wr_i_1__0_n_0));
  FDRE #(
    .INIT(1'b0)) 
    AXI_video_strm_V_user_V_1_sel_wr_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(AXI_video_strm_V_user_V_1_sel_wr_i_1__0_n_0),
        .Q(AXI_video_strm_V_user_V_1_sel_wr),
        .R(ap_rst));
  LUT5 #(
    .INIT(32'hA8A80888)) 
    \AXI_video_strm_V_user_V_1_state[0]_i_1__0 
       (.I0(ap_rst_n),
        .I1(\AXI_video_strm_V_user_V_1_state_reg_n_0_[0] ),
        .I2(AXI_video_strm_V_user_V_1_ack_in),
        .I3(crop_out_TREADY),
        .I4(Mat2AXIvideo_1_U0_img_data_stream_2_V_read),
        .O(\AXI_video_strm_V_user_V_1_state[0]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair95" *) 
  LUT4 #(
    .INIT(16'hF5FD)) 
    \AXI_video_strm_V_user_V_1_state[1]_i_1__0 
       (.I0(\AXI_video_strm_V_user_V_1_state_reg_n_0_[0] ),
        .I1(AXI_video_strm_V_user_V_1_ack_in),
        .I2(crop_out_TREADY),
        .I3(Mat2AXIvideo_1_U0_img_data_stream_2_V_read),
        .O(AXI_video_strm_V_user_V_1_state));
  FDRE #(
    .INIT(1'b0)) 
    \AXI_video_strm_V_user_V_1_state_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\AXI_video_strm_V_user_V_1_state[0]_i_1__0_n_0 ),
        .Q(\AXI_video_strm_V_user_V_1_state_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \AXI_video_strm_V_user_V_1_state_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(AXI_video_strm_V_user_V_1_state),
        .Q(AXI_video_strm_V_user_V_1_ack_in),
        .R(ap_rst));
  LUT5 #(
    .INIT(32'hFF888F88)) 
    \ap_CS_fsm[0]_i_1__1 
       (.I0(Mat2AXIvideo_1_U0_ap_ready),
        .I1(ap_CS_fsm_state2),
        .I2(start_for_Mat2AXIvideo_1_U0_empty_n),
        .I3(Q),
        .I4(ap_done_reg),
        .O(ap_NS_fsm[0]));
  LUT6 #(
    .INIT(64'hEAEAEAEAEAFFEAEA)) 
    \ap_CS_fsm[1]_i_1__2 
       (.I0(\ap_CS_fsm_reg_n_0_[3] ),
        .I1(Q),
        .I2(ap_NS_fsm112_out),
        .I3(ap_NS_fsm1),
        .I4(ap_CS_fsm_state2),
        .I5(Mat2AXIvideo_1_U0_ap_ready),
        .O(ap_NS_fsm[1]));
  LUT6 #(
    .INIT(64'hFFFF150015001500)) 
    \ap_CS_fsm[2]_i_1__2 
       (.I0(ap_NS_fsm39_out__1),
        .I1(p_10_in),
        .I2(\ap_CS_fsm[2]_i_3__0_n_0 ),
        .I3(ap_CS_fsm_pp0_stage0),
        .I4(ap_NS_fsm1),
        .I5(\ap_CS_fsm[2]_i_5_n_0 ),
        .O(ap_NS_fsm[2]));
  (* SOFT_HLUTNM = "soft_lutpair88" *) 
  LUT3 #(
    .INIT(8'h04)) 
    \ap_CS_fsm[2]_i_2__0 
       (.I0(ap_enable_reg_pp0_iter1_reg_n_0),
        .I1(ap_enable_reg_pp0_iter2_reg_n_0),
        .I2(ap_block_pp0_stage0_subdone),
        .O(ap_NS_fsm39_out__1));
  (* SOFT_HLUTNM = "soft_lutpair83" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \ap_CS_fsm[2]_i_3__0 
       (.I0(ap_enable_reg_pp0_iter0),
        .I1(ap_enable_reg_pp0_iter1_reg_n_0),
        .O(\ap_CS_fsm[2]_i_3__0_n_0 ));
  LUT6 #(
    .INIT(64'h0000000080000000)) 
    \ap_CS_fsm[2]_i_4 
       (.I0(\AXI_video_strm_V_id_V_1_state_reg_n_0_[1] ),
        .I1(\AXI_video_strm_V_dest_V_1_state_reg_n_0_[1] ),
        .I2(ap_CS_fsm_state2),
        .I3(AXI_video_strm_V_data_V_1_ack_in),
        .I4(ap_done_INST_0_i_5_n_0),
        .I5(exitcond2_fu_196_p2__13),
        .O(ap_NS_fsm1));
  LUT6 #(
    .INIT(64'h70F0F0F0F0F0F0F0)) 
    \ap_CS_fsm[2]_i_5 
       (.I0(\AXI_video_strm_V_id_V_1_state_reg_n_0_[1] ),
        .I1(\AXI_video_strm_V_dest_V_1_state_reg_n_0_[1] ),
        .I2(ap_CS_fsm_state2),
        .I3(AXI_video_strm_V_data_V_1_ack_in),
        .I4(ap_done_INST_0_i_5_n_0),
        .I5(exitcond2_fu_196_p2__13),
        .O(\ap_CS_fsm[2]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h4040554000000000)) 
    \ap_CS_fsm[3]_i_1__1 
       (.I0(ap_enable_reg_pp0_iter1_reg_n_0),
        .I1(ap_enable_reg_pp0_iter0),
        .I2(p_10_in),
        .I3(ap_enable_reg_pp0_iter2_reg_n_0),
        .I4(ap_block_pp0_stage0_subdone),
        .I5(ap_CS_fsm_pp0_stage0),
        .O(ap_NS_fsm[3]));
  LUT2 #(
    .INIT(4'h2)) 
    \ap_CS_fsm[3]_i_2__0 
       (.I0(exitcond_fu_208_p2),
        .I1(ap_block_pp0_stage0_subdone),
        .O(p_10_in));
  LUT4 #(
    .INIT(16'hFF04)) 
    \ap_CS_fsm[3]_i_3__0 
       (.I0(exitcond_reg_262_pp0_iter1_reg),
        .I1(ap_enable_reg_pp0_iter2_reg_n_0),
        .I2(AXI_video_strm_V_data_V_1_ack_in),
        .I3(\ap_CS_fsm[3]_i_4__0_n_0 ),
        .O(ap_block_pp0_stage0_subdone));
  LUT6 #(
    .INIT(64'h1333333300000000)) 
    \ap_CS_fsm[3]_i_4__0 
       (.I0(crop_data_data_strea_empty_n),
        .I1(\exitcond_reg_262_reg_n_0_[0] ),
        .I2(AXI_video_strm_V_data_V_1_ack_in),
        .I3(crop_data_data_strea_2_empty_n),
        .I4(crop_data_data_strea_1_empty_n),
        .I5(ap_enable_reg_pp0_iter1_reg_n_0),
        .O(\ap_CS_fsm[3]_i_4__0_n_0 ));
  (* FSM_ENCODING = "none" *) 
  FDSE #(
    .INIT(1'b1)) 
    \ap_CS_fsm_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[0]),
        .Q(Q),
        .S(ap_rst));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[1]),
        .Q(ap_CS_fsm_state2),
        .R(ap_rst));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[2] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[2]),
        .Q(ap_CS_fsm_pp0_stage0),
        .R(ap_rst));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[3] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[3]),
        .Q(\ap_CS_fsm_reg_n_0_[3] ),
        .R(ap_rst));
  LUT4 #(
    .INIT(16'h0004)) 
    ap_done_INST_0_i_10
       (.I0(t_V_reg_169[9]),
        .I1(t_V_reg_169[10]),
        .I2(t_V_reg_169[8]),
        .I3(t_V_reg_169[7]),
        .O(ap_done_INST_0_i_10_n_0));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    ap_done_INST_0_i_2
       (.I0(\AXI_video_strm_V_id_V_1_state_reg_n_0_[1] ),
        .I1(\AXI_video_strm_V_dest_V_1_state_reg_n_0_[1] ),
        .I2(ap_CS_fsm_state2),
        .I3(AXI_video_strm_V_data_V_1_ack_in),
        .I4(ap_done_INST_0_i_5_n_0),
        .I5(exitcond2_fu_196_p2__13),
        .O(Mat2AXIvideo_1_U0_ap_ready));
  LUT4 #(
    .INIT(16'h8000)) 
    ap_done_INST_0_i_5
       (.I0(\AXI_video_strm_V_strb_V_1_state_reg_n_0_[1] ),
        .I1(\AXI_video_strm_V_keep_V_1_state_reg_n_0_[1] ),
        .I2(AXI_video_strm_V_last_V_1_ack_in),
        .I3(AXI_video_strm_V_user_V_1_ack_in),
        .O(ap_done_INST_0_i_5_n_0));
  (* SOFT_HLUTNM = "soft_lutpair82" *) 
  LUT5 #(
    .INIT(32'h00000008)) 
    ap_done_INST_0_i_6
       (.I0(ap_done_INST_0_i_9_n_0),
        .I1(ap_done_INST_0_i_10_n_0),
        .I2(t_V_reg_169[0]),
        .I3(t_V_reg_169[1]),
        .I4(t_V_reg_169[2]),
        .O(exitcond2_fu_196_p2__13));
  (* SOFT_HLUTNM = "soft_lutpair93" *) 
  LUT4 #(
    .INIT(16'h4000)) 
    ap_done_INST_0_i_9
       (.I0(t_V_reg_169[6]),
        .I1(t_V_reg_169[5]),
        .I2(t_V_reg_169[4]),
        .I3(t_V_reg_169[3]),
        .O(ap_done_INST_0_i_9_n_0));
  FDRE #(
    .INIT(1'b0)) 
    ap_done_reg_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_done_reg_reg_0),
        .Q(ap_done_reg),
        .R(1'b0));
  LUT5 #(
    .INIT(32'h00E0E0E0)) 
    ap_enable_reg_pp0_iter0_i_1__0
       (.I0(ap_enable_reg_pp0_iter0),
        .I1(ap_NS_fsm1),
        .I2(ap_rst_n),
        .I3(p_10_in),
        .I4(ap_CS_fsm_pp0_stage0),
        .O(ap_enable_reg_pp0_iter0_i_1__0_n_0));
  FDRE #(
    .INIT(1'b0)) 
    ap_enable_reg_pp0_iter0_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_enable_reg_pp0_iter0_i_1__0_n_0),
        .Q(ap_enable_reg_pp0_iter0),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair83" *) 
  LUT5 #(
    .INIT(32'h00008A80)) 
    ap_enable_reg_pp0_iter1_i_1__0
       (.I0(ap_rst_n),
        .I1(ap_enable_reg_pp0_iter1_reg_n_0),
        .I2(ap_block_pp0_stage0_subdone),
        .I3(ap_enable_reg_pp0_iter0),
        .I4(p_10_in),
        .O(ap_enable_reg_pp0_iter1_i_1__0_n_0));
  FDRE #(
    .INIT(1'b0)) 
    ap_enable_reg_pp0_iter1_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_enable_reg_pp0_iter1_i_1__0_n_0),
        .Q(ap_enable_reg_pp0_iter1_reg_n_0),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair88" *) 
  LUT5 #(
    .INIT(32'h00C0A0A0)) 
    ap_enable_reg_pp0_iter2_i_1__0
       (.I0(ap_enable_reg_pp0_iter1_reg_n_0),
        .I1(ap_enable_reg_pp0_iter2_reg_n_0),
        .I2(ap_rst_n),
        .I3(ap_NS_fsm1),
        .I4(ap_block_pp0_stage0_subdone),
        .O(ap_enable_reg_pp0_iter2_i_1__0_n_0));
  FDRE #(
    .INIT(1'b0)) 
    ap_enable_reg_pp0_iter2_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_enable_reg_pp0_iter2_i_1__0_n_0),
        .Q(ap_enable_reg_pp0_iter2_reg_n_0),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h8000FFFF80000000)) 
    \axi_last_V_reg_271[0]_i_1 
       (.I0(\axi_last_V_reg_271[0]_i_2_n_0 ),
        .I1(t_V_1_reg_180_reg__0[0]),
        .I2(t_V_1_reg_180_reg__0[1]),
        .I3(t_V_1_reg_180_reg__0[2]),
        .I4(axi_last_V_reg_2710),
        .I5(\axi_last_V_reg_271_reg_n_0_[0] ),
        .O(\axi_last_V_reg_271[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair89" *) 
  LUT5 #(
    .INIT(32'h00000800)) 
    \axi_last_V_reg_271[0]_i_2 
       (.I0(\exitcond_reg_262[0]_i_4_n_0 ),
        .I1(t_V_1_reg_180_reg__0[4]),
        .I2(t_V_1_reg_180_reg__0[3]),
        .I3(t_V_1_reg_180_reg__0[5]),
        .I4(t_V_1_reg_180_reg__0[6]),
        .O(\axi_last_V_reg_271[0]_i_2_n_0 ));
  FDRE \axi_last_V_reg_271_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\axi_last_V_reg_271[0]_i_1_n_0 ),
        .Q(\axi_last_V_reg_271_reg_n_0_[0] ),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair98" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \crop_out_TDATA[0]_INST_0 
       (.I0(\AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[0] ),
        .I1(\AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[0] ),
        .I2(AXI_video_strm_V_data_V_1_sel_rd_reg_n_0),
        .O(crop_out_TDATA[0]));
  (* SOFT_HLUTNM = "soft_lutpair106" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \crop_out_TDATA[10]_INST_0 
       (.I0(\AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[10] ),
        .I1(\AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[10] ),
        .I2(AXI_video_strm_V_data_V_1_sel_rd_reg_n_0),
        .O(crop_out_TDATA[10]));
  (* SOFT_HLUTNM = "soft_lutpair107" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \crop_out_TDATA[11]_INST_0 
       (.I0(\AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[11] ),
        .I1(\AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[11] ),
        .I2(AXI_video_strm_V_data_V_1_sel_rd_reg_n_0),
        .O(crop_out_TDATA[11]));
  (* SOFT_HLUTNM = "soft_lutpair106" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \crop_out_TDATA[12]_INST_0 
       (.I0(\AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[12] ),
        .I1(\AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[12] ),
        .I2(AXI_video_strm_V_data_V_1_sel_rd_reg_n_0),
        .O(crop_out_TDATA[12]));
  (* SOFT_HLUTNM = "soft_lutpair108" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \crop_out_TDATA[13]_INST_0 
       (.I0(\AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[13] ),
        .I1(\AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[13] ),
        .I2(AXI_video_strm_V_data_V_1_sel_rd_reg_n_0),
        .O(crop_out_TDATA[13]));
  (* SOFT_HLUTNM = "soft_lutpair108" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \crop_out_TDATA[14]_INST_0 
       (.I0(\AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[14] ),
        .I1(\AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[14] ),
        .I2(AXI_video_strm_V_data_V_1_sel_rd_reg_n_0),
        .O(crop_out_TDATA[14]));
  (* SOFT_HLUTNM = "soft_lutpair109" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \crop_out_TDATA[15]_INST_0 
       (.I0(\AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[15] ),
        .I1(\AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[15] ),
        .I2(AXI_video_strm_V_data_V_1_sel_rd_reg_n_0),
        .O(crop_out_TDATA[15]));
  (* SOFT_HLUTNM = "soft_lutpair109" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \crop_out_TDATA[16]_INST_0 
       (.I0(\AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[16] ),
        .I1(\AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[16] ),
        .I2(AXI_video_strm_V_data_V_1_sel_rd_reg_n_0),
        .O(crop_out_TDATA[16]));
  (* SOFT_HLUTNM = "soft_lutpair107" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \crop_out_TDATA[17]_INST_0 
       (.I0(\AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[17] ),
        .I1(\AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[17] ),
        .I2(AXI_video_strm_V_data_V_1_sel_rd_reg_n_0),
        .O(crop_out_TDATA[17]));
  (* SOFT_HLUTNM = "soft_lutpair105" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \crop_out_TDATA[18]_INST_0 
       (.I0(\AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[18] ),
        .I1(\AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[18] ),
        .I2(AXI_video_strm_V_data_V_1_sel_rd_reg_n_0),
        .O(crop_out_TDATA[18]));
  (* SOFT_HLUTNM = "soft_lutpair104" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \crop_out_TDATA[19]_INST_0 
       (.I0(\AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[19] ),
        .I1(\AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[19] ),
        .I2(AXI_video_strm_V_data_V_1_sel_rd_reg_n_0),
        .O(crop_out_TDATA[19]));
  (* SOFT_HLUTNM = "soft_lutpair99" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \crop_out_TDATA[1]_INST_0 
       (.I0(\AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[1] ),
        .I1(\AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[1] ),
        .I2(AXI_video_strm_V_data_V_1_sel_rd_reg_n_0),
        .O(crop_out_TDATA[1]));
  (* SOFT_HLUTNM = "soft_lutpair103" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \crop_out_TDATA[20]_INST_0 
       (.I0(\AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[20] ),
        .I1(\AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[20] ),
        .I2(AXI_video_strm_V_data_V_1_sel_rd_reg_n_0),
        .O(crop_out_TDATA[20]));
  (* SOFT_HLUTNM = "soft_lutpair102" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \crop_out_TDATA[21]_INST_0 
       (.I0(\AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[21] ),
        .I1(\AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[21] ),
        .I2(AXI_video_strm_V_data_V_1_sel_rd_reg_n_0),
        .O(crop_out_TDATA[21]));
  (* SOFT_HLUTNM = "soft_lutpair100" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \crop_out_TDATA[22]_INST_0 
       (.I0(\AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[22] ),
        .I1(\AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[22] ),
        .I2(AXI_video_strm_V_data_V_1_sel_rd_reg_n_0),
        .O(crop_out_TDATA[22]));
  (* SOFT_HLUTNM = "soft_lutpair101" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \crop_out_TDATA[23]_INST_0 
       (.I0(\AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[23] ),
        .I1(\AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[23] ),
        .I2(AXI_video_strm_V_data_V_1_sel_rd_reg_n_0),
        .O(crop_out_TDATA[23]));
  (* SOFT_HLUTNM = "soft_lutpair100" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \crop_out_TDATA[2]_INST_0 
       (.I0(\AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[2] ),
        .I1(\AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[2] ),
        .I2(AXI_video_strm_V_data_V_1_sel_rd_reg_n_0),
        .O(crop_out_TDATA[2]));
  (* SOFT_HLUTNM = "soft_lutpair101" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \crop_out_TDATA[3]_INST_0 
       (.I0(\AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[3] ),
        .I1(\AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[3] ),
        .I2(AXI_video_strm_V_data_V_1_sel_rd_reg_n_0),
        .O(crop_out_TDATA[3]));
  (* SOFT_HLUTNM = "soft_lutpair102" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \crop_out_TDATA[4]_INST_0 
       (.I0(\AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[4] ),
        .I1(\AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[4] ),
        .I2(AXI_video_strm_V_data_V_1_sel_rd_reg_n_0),
        .O(crop_out_TDATA[4]));
  (* SOFT_HLUTNM = "soft_lutpair98" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \crop_out_TDATA[5]_INST_0 
       (.I0(\AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[5] ),
        .I1(\AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[5] ),
        .I2(AXI_video_strm_V_data_V_1_sel_rd_reg_n_0),
        .O(crop_out_TDATA[5]));
  (* SOFT_HLUTNM = "soft_lutpair99" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \crop_out_TDATA[6]_INST_0 
       (.I0(\AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[6] ),
        .I1(\AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[6] ),
        .I2(AXI_video_strm_V_data_V_1_sel_rd_reg_n_0),
        .O(crop_out_TDATA[6]));
  (* SOFT_HLUTNM = "soft_lutpair103" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \crop_out_TDATA[7]_INST_0 
       (.I0(\AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[7] ),
        .I1(\AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[7] ),
        .I2(AXI_video_strm_V_data_V_1_sel_rd_reg_n_0),
        .O(crop_out_TDATA[7]));
  (* SOFT_HLUTNM = "soft_lutpair104" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \crop_out_TDATA[8]_INST_0 
       (.I0(\AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[8] ),
        .I1(\AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[8] ),
        .I2(AXI_video_strm_V_data_V_1_sel_rd_reg_n_0),
        .O(crop_out_TDATA[8]));
  (* SOFT_HLUTNM = "soft_lutpair105" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \crop_out_TDATA[9]_INST_0 
       (.I0(\AXI_video_strm_V_data_V_1_payload_B_reg_n_0_[9] ),
        .I1(\AXI_video_strm_V_data_V_1_payload_A_reg_n_0_[9] ),
        .I2(AXI_video_strm_V_data_V_1_sel_rd_reg_n_0),
        .O(crop_out_TDATA[9]));
  LUT3 #(
    .INIT(8'hB8)) 
    \crop_out_TLAST[0]_INST_0 
       (.I0(AXI_video_strm_V_last_V_1_payload_B),
        .I1(AXI_video_strm_V_last_V_1_sel),
        .I2(AXI_video_strm_V_last_V_1_payload_A),
        .O(crop_out_TLAST));
  LUT3 #(
    .INIT(8'hB8)) 
    \crop_out_TUSER[0]_INST_0 
       (.I0(AXI_video_strm_V_user_V_1_payload_B),
        .I1(AXI_video_strm_V_user_V_1_sel),
        .I2(AXI_video_strm_V_user_V_1_payload_A),
        .O(crop_out_TUSER));
  (* SOFT_HLUTNM = "soft_lutpair91" *) 
  LUT4 #(
    .INIT(16'hFB08)) 
    \exitcond_reg_262[0]_i_1 
       (.I0(exitcond_fu_208_p2),
        .I1(ap_CS_fsm_pp0_stage0),
        .I2(ap_block_pp0_stage0_subdone),
        .I3(\exitcond_reg_262_reg_n_0_[0] ),
        .O(\exitcond_reg_262[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair81" *) 
  LUT5 #(
    .INIT(32'h00000008)) 
    \exitcond_reg_262[0]_i_2 
       (.I0(\exitcond_reg_262[0]_i_3_n_0 ),
        .I1(\exitcond_reg_262[0]_i_4_n_0 ),
        .I2(t_V_1_reg_180_reg__0[0]),
        .I3(t_V_1_reg_180_reg__0[1]),
        .I4(t_V_1_reg_180_reg__0[2]),
        .O(exitcond_fu_208_p2));
  (* SOFT_HLUTNM = "soft_lutpair89" *) 
  LUT4 #(
    .INIT(16'h4000)) 
    \exitcond_reg_262[0]_i_3 
       (.I0(t_V_1_reg_180_reg__0[6]),
        .I1(t_V_1_reg_180_reg__0[5]),
        .I2(t_V_1_reg_180_reg__0[4]),
        .I3(t_V_1_reg_180_reg__0[3]),
        .O(\exitcond_reg_262[0]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h0004)) 
    \exitcond_reg_262[0]_i_4 
       (.I0(t_V_1_reg_180_reg__0[9]),
        .I1(t_V_1_reg_180_reg__0[10]),
        .I2(t_V_1_reg_180_reg__0[8]),
        .I3(t_V_1_reg_180_reg__0[7]),
        .O(\exitcond_reg_262[0]_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair92" *) 
  LUT4 #(
    .INIT(16'hFB08)) 
    \exitcond_reg_262_pp0_iter1_reg[0]_i_1 
       (.I0(\exitcond_reg_262_reg_n_0_[0] ),
        .I1(ap_CS_fsm_pp0_stage0),
        .I2(ap_block_pp0_stage0_subdone),
        .I3(exitcond_reg_262_pp0_iter1_reg),
        .O(\exitcond_reg_262_pp0_iter1_reg[0]_i_1_n_0 ));
  FDRE \exitcond_reg_262_pp0_iter1_reg_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\exitcond_reg_262_pp0_iter1_reg[0]_i_1_n_0 ),
        .Q(exitcond_reg_262_pp0_iter1_reg),
        .R(1'b0));
  FDRE \exitcond_reg_262_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\exitcond_reg_262[0]_i_1_n_0 ),
        .Q(\exitcond_reg_262_reg_n_0_[0] ),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair111" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \i_V_reg_257[0]_i_1 
       (.I0(t_V_reg_169[0]),
        .O(i_V_fu_202_p2[0]));
  LUT5 #(
    .INIT(32'h80000000)) 
    \i_V_reg_257[10]_i_1 
       (.I0(\AXI_video_strm_V_id_V_1_state_reg_n_0_[1] ),
        .I1(\AXI_video_strm_V_dest_V_1_state_reg_n_0_[1] ),
        .I2(ap_CS_fsm_state2),
        .I3(AXI_video_strm_V_data_V_1_ack_in),
        .I4(ap_done_INST_0_i_5_n_0),
        .O(i_V_reg_2570));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \i_V_reg_257[10]_i_2 
       (.I0(t_V_reg_169[8]),
        .I1(t_V_reg_169[6]),
        .I2(\i_V_reg_257[10]_i_3_n_0 ),
        .I3(t_V_reg_169[7]),
        .I4(t_V_reg_169[9]),
        .I5(t_V_reg_169[10]),
        .O(i_V_fu_202_p2[10]));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \i_V_reg_257[10]_i_3 
       (.I0(t_V_reg_169[5]),
        .I1(t_V_reg_169[3]),
        .I2(t_V_reg_169[1]),
        .I3(t_V_reg_169[0]),
        .I4(t_V_reg_169[2]),
        .I5(t_V_reg_169[4]),
        .O(\i_V_reg_257[10]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair111" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \i_V_reg_257[1]_i_1 
       (.I0(t_V_reg_169[0]),
        .I1(t_V_reg_169[1]),
        .O(i_V_fu_202_p2[1]));
  (* SOFT_HLUTNM = "soft_lutpair82" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \i_V_reg_257[2]_i_1 
       (.I0(t_V_reg_169[0]),
        .I1(t_V_reg_169[1]),
        .I2(t_V_reg_169[2]),
        .O(i_V_fu_202_p2[2]));
  (* SOFT_HLUTNM = "soft_lutpair85" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \i_V_reg_257[3]_i_1 
       (.I0(t_V_reg_169[1]),
        .I1(t_V_reg_169[0]),
        .I2(t_V_reg_169[2]),
        .I3(t_V_reg_169[3]),
        .O(i_V_fu_202_p2[3]));
  (* SOFT_HLUTNM = "soft_lutpair85" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \i_V_reg_257[4]_i_1 
       (.I0(t_V_reg_169[2]),
        .I1(t_V_reg_169[0]),
        .I2(t_V_reg_169[1]),
        .I3(t_V_reg_169[3]),
        .I4(t_V_reg_169[4]),
        .O(i_V_fu_202_p2[4]));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \i_V_reg_257[5]_i_1 
       (.I0(t_V_reg_169[3]),
        .I1(t_V_reg_169[1]),
        .I2(t_V_reg_169[0]),
        .I3(t_V_reg_169[2]),
        .I4(t_V_reg_169[4]),
        .I5(t_V_reg_169[5]),
        .O(i_V_fu_202_p2[5]));
  (* SOFT_HLUTNM = "soft_lutpair93" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \i_V_reg_257[6]_i_1 
       (.I0(\i_V_reg_257[10]_i_3_n_0 ),
        .I1(t_V_reg_169[6]),
        .O(i_V_fu_202_p2[6]));
  LUT3 #(
    .INIT(8'h78)) 
    \i_V_reg_257[7]_i_1 
       (.I0(\i_V_reg_257[10]_i_3_n_0 ),
        .I1(t_V_reg_169[6]),
        .I2(t_V_reg_169[7]),
        .O(i_V_fu_202_p2[7]));
  (* SOFT_HLUTNM = "soft_lutpair87" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \i_V_reg_257[8]_i_1 
       (.I0(t_V_reg_169[6]),
        .I1(\i_V_reg_257[10]_i_3_n_0 ),
        .I2(t_V_reg_169[7]),
        .I3(t_V_reg_169[8]),
        .O(i_V_fu_202_p2[8]));
  (* SOFT_HLUTNM = "soft_lutpair87" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \i_V_reg_257[9]_i_1 
       (.I0(t_V_reg_169[7]),
        .I1(\i_V_reg_257[10]_i_3_n_0 ),
        .I2(t_V_reg_169[6]),
        .I3(t_V_reg_169[8]),
        .I4(t_V_reg_169[9]),
        .O(i_V_fu_202_p2[9]));
  FDRE \i_V_reg_257_reg[0] 
       (.C(ap_clk),
        .CE(i_V_reg_2570),
        .D(i_V_fu_202_p2[0]),
        .Q(i_V_reg_257[0]),
        .R(1'b0));
  FDRE \i_V_reg_257_reg[10] 
       (.C(ap_clk),
        .CE(i_V_reg_2570),
        .D(i_V_fu_202_p2[10]),
        .Q(i_V_reg_257[10]),
        .R(1'b0));
  FDRE \i_V_reg_257_reg[1] 
       (.C(ap_clk),
        .CE(i_V_reg_2570),
        .D(i_V_fu_202_p2[1]),
        .Q(i_V_reg_257[1]),
        .R(1'b0));
  FDRE \i_V_reg_257_reg[2] 
       (.C(ap_clk),
        .CE(i_V_reg_2570),
        .D(i_V_fu_202_p2[2]),
        .Q(i_V_reg_257[2]),
        .R(1'b0));
  FDRE \i_V_reg_257_reg[3] 
       (.C(ap_clk),
        .CE(i_V_reg_2570),
        .D(i_V_fu_202_p2[3]),
        .Q(i_V_reg_257[3]),
        .R(1'b0));
  FDRE \i_V_reg_257_reg[4] 
       (.C(ap_clk),
        .CE(i_V_reg_2570),
        .D(i_V_fu_202_p2[4]),
        .Q(i_V_reg_257[4]),
        .R(1'b0));
  FDRE \i_V_reg_257_reg[5] 
       (.C(ap_clk),
        .CE(i_V_reg_2570),
        .D(i_V_fu_202_p2[5]),
        .Q(i_V_reg_257[5]),
        .R(1'b0));
  FDRE \i_V_reg_257_reg[6] 
       (.C(ap_clk),
        .CE(i_V_reg_2570),
        .D(i_V_fu_202_p2[6]),
        .Q(i_V_reg_257[6]),
        .R(1'b0));
  FDRE \i_V_reg_257_reg[7] 
       (.C(ap_clk),
        .CE(i_V_reg_2570),
        .D(i_V_fu_202_p2[7]),
        .Q(i_V_reg_257[7]),
        .R(1'b0));
  FDRE \i_V_reg_257_reg[8] 
       (.C(ap_clk),
        .CE(i_V_reg_2570),
        .D(i_V_fu_202_p2[8]),
        .Q(i_V_reg_257[8]),
        .R(1'b0));
  FDRE \i_V_reg_257_reg[9] 
       (.C(ap_clk),
        .CE(i_V_reg_2570),
        .D(i_V_fu_202_p2[9]),
        .Q(i_V_reg_257[9]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair110" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \t_V_1_reg_180[0]_i_1 
       (.I0(t_V_1_reg_180_reg__0[0]),
        .O(j_V_fu_214_p2[0]));
  LUT3 #(
    .INIT(8'h2A)) 
    \t_V_1_reg_180[10]_i_1 
       (.I0(ap_NS_fsm1),
        .I1(axi_last_V_reg_2710),
        .I2(ap_enable_reg_pp0_iter0),
        .O(t_V_1_reg_180));
  LUT2 #(
    .INIT(4'h8)) 
    \t_V_1_reg_180[10]_i_2 
       (.I0(ap_enable_reg_pp0_iter0),
        .I1(axi_last_V_reg_2710),
        .O(t_V_1_reg_1800));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \t_V_1_reg_180[10]_i_3 
       (.I0(t_V_1_reg_180_reg__0[8]),
        .I1(t_V_1_reg_180_reg__0[6]),
        .I2(\t_V_1_reg_180[10]_i_5_n_0 ),
        .I3(t_V_1_reg_180_reg__0[7]),
        .I4(t_V_1_reg_180_reg__0[9]),
        .I5(t_V_1_reg_180_reg__0[10]),
        .O(j_V_fu_214_p2[10]));
  (* SOFT_HLUTNM = "soft_lutpair92" *) 
  LUT3 #(
    .INIT(8'h04)) 
    \t_V_1_reg_180[10]_i_4 
       (.I0(exitcond_fu_208_p2),
        .I1(ap_CS_fsm_pp0_stage0),
        .I2(ap_block_pp0_stage0_subdone),
        .O(axi_last_V_reg_2710));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \t_V_1_reg_180[10]_i_5 
       (.I0(t_V_1_reg_180_reg__0[5]),
        .I1(t_V_1_reg_180_reg__0[3]),
        .I2(t_V_1_reg_180_reg__0[1]),
        .I3(t_V_1_reg_180_reg__0[0]),
        .I4(t_V_1_reg_180_reg__0[2]),
        .I5(t_V_1_reg_180_reg__0[4]),
        .O(\t_V_1_reg_180[10]_i_5_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair110" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \t_V_1_reg_180[1]_i_1 
       (.I0(t_V_1_reg_180_reg__0[0]),
        .I1(t_V_1_reg_180_reg__0[1]),
        .O(j_V_fu_214_p2[1]));
  (* SOFT_HLUTNM = "soft_lutpair81" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \t_V_1_reg_180[2]_i_1 
       (.I0(t_V_1_reg_180_reg__0[0]),
        .I1(t_V_1_reg_180_reg__0[1]),
        .I2(t_V_1_reg_180_reg__0[2]),
        .O(j_V_fu_214_p2[2]));
  (* SOFT_HLUTNM = "soft_lutpair86" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \t_V_1_reg_180[3]_i_1 
       (.I0(t_V_1_reg_180_reg__0[1]),
        .I1(t_V_1_reg_180_reg__0[0]),
        .I2(t_V_1_reg_180_reg__0[2]),
        .I3(t_V_1_reg_180_reg__0[3]),
        .O(j_V_fu_214_p2[3]));
  (* SOFT_HLUTNM = "soft_lutpair86" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \t_V_1_reg_180[4]_i_1 
       (.I0(t_V_1_reg_180_reg__0[2]),
        .I1(t_V_1_reg_180_reg__0[0]),
        .I2(t_V_1_reg_180_reg__0[1]),
        .I3(t_V_1_reg_180_reg__0[3]),
        .I4(t_V_1_reg_180_reg__0[4]),
        .O(j_V_fu_214_p2[4]));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \t_V_1_reg_180[5]_i_1 
       (.I0(t_V_1_reg_180_reg__0[3]),
        .I1(t_V_1_reg_180_reg__0[1]),
        .I2(t_V_1_reg_180_reg__0[0]),
        .I3(t_V_1_reg_180_reg__0[2]),
        .I4(t_V_1_reg_180_reg__0[4]),
        .I5(t_V_1_reg_180_reg__0[5]),
        .O(j_V_fu_214_p2[5]));
  (* SOFT_HLUTNM = "soft_lutpair97" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \t_V_1_reg_180[6]_i_1 
       (.I0(\t_V_1_reg_180[10]_i_5_n_0 ),
        .I1(t_V_1_reg_180_reg__0[6]),
        .O(j_V_fu_214_p2[6]));
  (* SOFT_HLUTNM = "soft_lutpair97" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \t_V_1_reg_180[7]_i_1 
       (.I0(\t_V_1_reg_180[10]_i_5_n_0 ),
        .I1(t_V_1_reg_180_reg__0[6]),
        .I2(t_V_1_reg_180_reg__0[7]),
        .O(j_V_fu_214_p2[7]));
  (* SOFT_HLUTNM = "soft_lutpair84" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \t_V_1_reg_180[8]_i_1 
       (.I0(t_V_1_reg_180_reg__0[6]),
        .I1(\t_V_1_reg_180[10]_i_5_n_0 ),
        .I2(t_V_1_reg_180_reg__0[7]),
        .I3(t_V_1_reg_180_reg__0[8]),
        .O(j_V_fu_214_p2[8]));
  (* SOFT_HLUTNM = "soft_lutpair84" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \t_V_1_reg_180[9]_i_1 
       (.I0(t_V_1_reg_180_reg__0[7]),
        .I1(\t_V_1_reg_180[10]_i_5_n_0 ),
        .I2(t_V_1_reg_180_reg__0[6]),
        .I3(t_V_1_reg_180_reg__0[8]),
        .I4(t_V_1_reg_180_reg__0[9]),
        .O(j_V_fu_214_p2[9]));
  FDRE \t_V_1_reg_180_reg[0] 
       (.C(ap_clk),
        .CE(t_V_1_reg_1800),
        .D(j_V_fu_214_p2[0]),
        .Q(t_V_1_reg_180_reg__0[0]),
        .R(t_V_1_reg_180));
  FDRE \t_V_1_reg_180_reg[10] 
       (.C(ap_clk),
        .CE(t_V_1_reg_1800),
        .D(j_V_fu_214_p2[10]),
        .Q(t_V_1_reg_180_reg__0[10]),
        .R(t_V_1_reg_180));
  FDRE \t_V_1_reg_180_reg[1] 
       (.C(ap_clk),
        .CE(t_V_1_reg_1800),
        .D(j_V_fu_214_p2[1]),
        .Q(t_V_1_reg_180_reg__0[1]),
        .R(t_V_1_reg_180));
  FDRE \t_V_1_reg_180_reg[2] 
       (.C(ap_clk),
        .CE(t_V_1_reg_1800),
        .D(j_V_fu_214_p2[2]),
        .Q(t_V_1_reg_180_reg__0[2]),
        .R(t_V_1_reg_180));
  FDRE \t_V_1_reg_180_reg[3] 
       (.C(ap_clk),
        .CE(t_V_1_reg_1800),
        .D(j_V_fu_214_p2[3]),
        .Q(t_V_1_reg_180_reg__0[3]),
        .R(t_V_1_reg_180));
  FDRE \t_V_1_reg_180_reg[4] 
       (.C(ap_clk),
        .CE(t_V_1_reg_1800),
        .D(j_V_fu_214_p2[4]),
        .Q(t_V_1_reg_180_reg__0[4]),
        .R(t_V_1_reg_180));
  FDRE \t_V_1_reg_180_reg[5] 
       (.C(ap_clk),
        .CE(t_V_1_reg_1800),
        .D(j_V_fu_214_p2[5]),
        .Q(t_V_1_reg_180_reg__0[5]),
        .R(t_V_1_reg_180));
  FDRE \t_V_1_reg_180_reg[6] 
       (.C(ap_clk),
        .CE(t_V_1_reg_1800),
        .D(j_V_fu_214_p2[6]),
        .Q(t_V_1_reg_180_reg__0[6]),
        .R(t_V_1_reg_180));
  FDRE \t_V_1_reg_180_reg[7] 
       (.C(ap_clk),
        .CE(t_V_1_reg_1800),
        .D(j_V_fu_214_p2[7]),
        .Q(t_V_1_reg_180_reg__0[7]),
        .R(t_V_1_reg_180));
  FDRE \t_V_1_reg_180_reg[8] 
       (.C(ap_clk),
        .CE(t_V_1_reg_1800),
        .D(j_V_fu_214_p2[8]),
        .Q(t_V_1_reg_180_reg__0[8]),
        .R(t_V_1_reg_180));
  FDRE \t_V_1_reg_180_reg[9] 
       (.C(ap_clk),
        .CE(t_V_1_reg_1800),
        .D(j_V_fu_214_p2[9]),
        .Q(t_V_1_reg_180_reg__0[9]),
        .R(t_V_1_reg_180));
  LUT3 #(
    .INIT(8'h08)) 
    \t_V_reg_169[10]_i_1 
       (.I0(start_for_Mat2AXIvideo_1_U0_empty_n),
        .I1(Q),
        .I2(ap_done_reg),
        .O(ap_NS_fsm112_out));
  FDRE \t_V_reg_169_reg[0] 
       (.C(ap_clk),
        .CE(\ap_CS_fsm_reg_n_0_[3] ),
        .D(i_V_reg_257[0]),
        .Q(t_V_reg_169[0]),
        .R(ap_NS_fsm112_out));
  FDRE \t_V_reg_169_reg[10] 
       (.C(ap_clk),
        .CE(\ap_CS_fsm_reg_n_0_[3] ),
        .D(i_V_reg_257[10]),
        .Q(t_V_reg_169[10]),
        .R(ap_NS_fsm112_out));
  FDRE \t_V_reg_169_reg[1] 
       (.C(ap_clk),
        .CE(\ap_CS_fsm_reg_n_0_[3] ),
        .D(i_V_reg_257[1]),
        .Q(t_V_reg_169[1]),
        .R(ap_NS_fsm112_out));
  FDRE \t_V_reg_169_reg[2] 
       (.C(ap_clk),
        .CE(\ap_CS_fsm_reg_n_0_[3] ),
        .D(i_V_reg_257[2]),
        .Q(t_V_reg_169[2]),
        .R(ap_NS_fsm112_out));
  FDRE \t_V_reg_169_reg[3] 
       (.C(ap_clk),
        .CE(\ap_CS_fsm_reg_n_0_[3] ),
        .D(i_V_reg_257[3]),
        .Q(t_V_reg_169[3]),
        .R(ap_NS_fsm112_out));
  FDRE \t_V_reg_169_reg[4] 
       (.C(ap_clk),
        .CE(\ap_CS_fsm_reg_n_0_[3] ),
        .D(i_V_reg_257[4]),
        .Q(t_V_reg_169[4]),
        .R(ap_NS_fsm112_out));
  FDRE \t_V_reg_169_reg[5] 
       (.C(ap_clk),
        .CE(\ap_CS_fsm_reg_n_0_[3] ),
        .D(i_V_reg_257[5]),
        .Q(t_V_reg_169[5]),
        .R(ap_NS_fsm112_out));
  FDRE \t_V_reg_169_reg[6] 
       (.C(ap_clk),
        .CE(\ap_CS_fsm_reg_n_0_[3] ),
        .D(i_V_reg_257[6]),
        .Q(t_V_reg_169[6]),
        .R(ap_NS_fsm112_out));
  FDRE \t_V_reg_169_reg[7] 
       (.C(ap_clk),
        .CE(\ap_CS_fsm_reg_n_0_[3] ),
        .D(i_V_reg_257[7]),
        .Q(t_V_reg_169[7]),
        .R(ap_NS_fsm112_out));
  FDRE \t_V_reg_169_reg[8] 
       (.C(ap_clk),
        .CE(\ap_CS_fsm_reg_n_0_[3] ),
        .D(i_V_reg_257[8]),
        .Q(t_V_reg_169[8]),
        .R(ap_NS_fsm112_out));
  FDRE \t_V_reg_169_reg[9] 
       (.C(ap_clk),
        .CE(\ap_CS_fsm_reg_n_0_[3] ),
        .D(i_V_reg_257[9]),
        .Q(t_V_reg_169[9]),
        .R(ap_NS_fsm112_out));
  LUT5 #(
    .INIT(32'h0000AAEA)) 
    \tmp_user_V_fu_118[0]_i_1 
       (.I0(tmp_user_V_fu_118),
        .I1(start_for_Mat2AXIvideo_1_U0_empty_n),
        .I2(Q),
        .I3(ap_done_reg),
        .I4(Mat2AXIvideo_1_U0_img_data_stream_2_V_read),
        .O(\tmp_user_V_fu_118[0]_i_1_n_0 ));
  FDRE \tmp_user_V_fu_118_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\tmp_user_V_fu_118[0]_i_1_n_0 ),
        .Q(tmp_user_V_fu_118),
        .R(1'b0));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_center_image
   (image_in_TDATA,
    image_in_TKEEP,
    image_in_TSTRB,
    image_in_TUSER,
    image_in_TLAST,
    image_in_TID,
    image_in_TDEST,
    image_out_TDATA,
    image_out_TKEEP,
    image_out_TSTRB,
    image_out_TUSER,
    image_out_TLAST,
    image_out_TID,
    image_out_TDEST,
    crop_out_TDATA,
    crop_out_TKEEP,
    crop_out_TSTRB,
    crop_out_TUSER,
    crop_out_TLAST,
    crop_out_TID,
    crop_out_TDEST,
    ap_clk,
    ap_rst_n,
    image_in_TVALID,
    image_in_TREADY,
    image_out_TVALID,
    image_out_TREADY,
    ap_done,
    crop_out_TVALID,
    crop_out_TREADY,
    ap_start,
    ap_ready,
    ap_idle);
  input [23:0]image_in_TDATA;
  input [2:0]image_in_TKEEP;
  input [2:0]image_in_TSTRB;
  input [0:0]image_in_TUSER;
  input [0:0]image_in_TLAST;
  input [0:0]image_in_TID;
  input [0:0]image_in_TDEST;
  output [23:0]image_out_TDATA;
  output [2:0]image_out_TKEEP;
  output [2:0]image_out_TSTRB;
  output [0:0]image_out_TUSER;
  output [0:0]image_out_TLAST;
  output [0:0]image_out_TID;
  output [0:0]image_out_TDEST;
  output [23:0]crop_out_TDATA;
  output [2:0]crop_out_TKEEP;
  output [2:0]crop_out_TSTRB;
  output [0:0]crop_out_TUSER;
  output [0:0]crop_out_TLAST;
  output [0:0]crop_out_TID;
  output [0:0]crop_out_TDEST;
  input ap_clk;
  input ap_rst_n;
  input image_in_TVALID;
  output image_in_TREADY;
  output image_out_TVALID;
  input image_out_TREADY;
  output ap_done;
  output crop_out_TVALID;
  input crop_out_TREADY;
  input ap_start;
  output ap_ready;
  output ap_idle;

  wire \<const0> ;
  wire \<const1> ;
  wire AXIvideo2Mat_U0_img_data_stream_2_V_write;
  wire AXIvideo2Mat_U0_n_10;
  wire AXIvideo2Mat_U0_n_11;
  wire AXIvideo2Mat_U0_n_12;
  wire AXIvideo2Mat_U0_n_13;
  wire AXIvideo2Mat_U0_n_14;
  wire AXIvideo2Mat_U0_n_15;
  wire AXIvideo2Mat_U0_n_16;
  wire AXIvideo2Mat_U0_n_17;
  wire AXIvideo2Mat_U0_n_26;
  wire AXIvideo2Mat_U0_n_27;
  wire AXIvideo2Mat_U0_n_28;
  wire AXIvideo2Mat_U0_n_29;
  wire AXIvideo2Mat_U0_n_30;
  wire AXIvideo2Mat_U0_n_31;
  wire AXIvideo2Mat_U0_n_32;
  wire AXIvideo2Mat_U0_n_33;
  wire AXIvideo2Mat_U0_n_4;
  wire AXIvideo2Mat_U0_n_5;
  wire AXIvideo2Mat_U0_n_7;
  wire AXIvideo2Mat_U0_n_8;
  wire AXIvideo2Mat_U0_n_9;
  wire Loop_loop_height_pro_U0_ap_ready;
  wire Loop_loop_height_pro_U0_n_0;
  wire Loop_loop_height_pro_U0_n_11;
  wire Loop_loop_height_pro_U0_n_12;
  wire Loop_loop_height_pro_U0_n_13;
  wire Loop_loop_height_pro_U0_n_3;
  wire Loop_loop_height_pro_U0_passthrough_data_stream_2_V_write;
  wire Mat2AXIvideo_1_U0_ap_ready;
  wire Mat2AXIvideo_1_U0_img_data_stream_2_V_read;
  wire Mat2AXIvideo_1_U0_n_2;
  wire Mat2AXIvideo_U0_ap_ready;
  wire Mat2AXIvideo_U0_img_data_stream_2_V_read;
  wire Mat2AXIvideo_U0_n_3;
  wire Mat2AXIvideo_U0_n_5;
  wire ap_CS_fsm_state4;
  wire ap_clk;
  wire ap_done;
  wire ap_done_reg;
  wire ap_idle;
  wire ap_ready;
  wire ap_rst;
  wire ap_rst_n;
  wire ap_start;
  wire ce;
  wire ce_0;
  wire ce_1;
  wire ce_2;
  wire ce_3;
  wire ce_4;
  wire crop_data_data_strea_1_empty_n;
  wire crop_data_data_strea_1_full_n;
  wire crop_data_data_strea_2_U_n_2;
  wire crop_data_data_strea_2_empty_n;
  wire crop_data_data_strea_2_full_n;
  wire crop_data_data_strea_empty_n;
  wire crop_data_data_strea_full_n;
  wire [23:0]crop_out_TDATA;
  wire [0:0]crop_out_TLAST;
  wire crop_out_TREADY;
  wire [0:0]crop_out_TUSER;
  wire crop_out_TVALID;
  wire [7:0]data;
  wire [23:0]image_in_TDATA;
  wire [0:0]image_in_TLAST;
  wire image_in_TREADY;
  wire [0:0]image_in_TUSER;
  wire image_in_TVALID;
  wire [23:0]image_out_TDATA;
  wire [0:0]image_out_TLAST;
  wire image_out_TREADY;
  wire [0:0]image_out_TUSER;
  wire image_out_TVALID;
  wire input_data_data_stre_1_U_n_2;
  wire input_data_data_stre_1_U_n_3;
  wire input_data_data_stre_1_U_n_4;
  wire input_data_data_stre_1_U_n_5;
  wire input_data_data_stre_1_U_n_6;
  wire input_data_data_stre_1_U_n_7;
  wire input_data_data_stre_1_U_n_8;
  wire input_data_data_stre_1_U_n_9;
  wire input_data_data_stre_1_empty_n;
  wire input_data_data_stre_1_full_n;
  wire input_data_data_stre_2_U_n_2;
  wire input_data_data_stre_2_U_n_3;
  wire input_data_data_stre_2_U_n_4;
  wire input_data_data_stre_2_U_n_5;
  wire input_data_data_stre_2_U_n_6;
  wire input_data_data_stre_2_U_n_7;
  wire input_data_data_stre_2_U_n_8;
  wire input_data_data_stre_2_U_n_9;
  wire input_data_data_stre_2_empty_n;
  wire input_data_data_stre_2_full_n;
  wire input_data_data_stre_U_n_2;
  wire input_data_data_stre_U_n_3;
  wire input_data_data_stre_U_n_4;
  wire input_data_data_stre_U_n_5;
  wire input_data_data_stre_U_n_6;
  wire input_data_data_stre_U_n_7;
  wire input_data_data_stre_U_n_8;
  wire input_data_data_stre_U_n_9;
  wire input_data_data_stre_empty_n;
  wire input_data_data_stre_full_n;
  wire passthrough_data_str_1_U_n_2;
  wire passthrough_data_str_1_empty_n;
  wire passthrough_data_str_1_full_n;
  wire passthrough_data_str_2_U_n_2;
  wire passthrough_data_str_2_empty_n;
  wire passthrough_data_str_2_full_n;
  wire passthrough_data_str_empty_n;
  wire passthrough_data_str_full_n;
  wire start_for_Loop_lobkb_U_n_3;
  wire start_for_Loop_loop_height_pro_U0_empty_n;
  wire start_for_Loop_loop_height_pro_U0_full_n;
  wire start_for_Mat2AXIcud_U_n_2;
  wire start_for_Mat2AXIvideo_1_U0_empty_n;
  wire start_for_Mat2AXIvideo_1_U0_full_n;
  wire start_for_Mat2AXIvideo_U0_empty_n;
  wire start_for_Mat2AXIvideo_U0_full_n;
  wire start_once_reg;
  wire [23:0]tmp_data_V_fu_230_p4;
  wire [23:0]tmp_data_V_fu_234_p4;

  assign crop_out_TDEST[0] = \<const0> ;
  assign crop_out_TID[0] = \<const0> ;
  assign crop_out_TKEEP[2] = \<const1> ;
  assign crop_out_TKEEP[1] = \<const1> ;
  assign crop_out_TKEEP[0] = \<const1> ;
  assign crop_out_TSTRB[2] = \<const0> ;
  assign crop_out_TSTRB[1] = \<const0> ;
  assign crop_out_TSTRB[0] = \<const0> ;
  assign image_out_TDEST[0] = \<const0> ;
  assign image_out_TID[0] = \<const0> ;
  assign image_out_TKEEP[2] = \<const1> ;
  assign image_out_TKEEP[1] = \<const1> ;
  assign image_out_TKEEP[0] = \<const1> ;
  assign image_out_TSTRB[2] = \<const0> ;
  assign image_out_TSTRB[1] = \<const0> ;
  assign image_out_TSTRB[0] = \<const0> ;
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_AXIvideo2Mat AXIvideo2Mat_U0
       (.AXIvideo2Mat_U0_img_data_stream_2_V_write(AXIvideo2Mat_U0_img_data_stream_2_V_write),
        .D({AXIvideo2Mat_U0_n_10,AXIvideo2Mat_U0_n_11,AXIvideo2Mat_U0_n_12,AXIvideo2Mat_U0_n_13,AXIvideo2Mat_U0_n_14,AXIvideo2Mat_U0_n_15,AXIvideo2Mat_U0_n_16,AXIvideo2Mat_U0_n_17}),
        .Q(AXIvideo2Mat_U0_n_4),
        .\SRL_SIG_reg[0][7] (data),
        .\SRL_SIG_reg[0][7]_0 ({AXIvideo2Mat_U0_n_26,AXIvideo2Mat_U0_n_27,AXIvideo2Mat_U0_n_28,AXIvideo2Mat_U0_n_29,AXIvideo2Mat_U0_n_30,AXIvideo2Mat_U0_n_31,AXIvideo2Mat_U0_n_32,AXIvideo2Mat_U0_n_33}),
        .ap_clk(ap_clk),
        .ap_ready(ap_ready),
        .ap_rst(ap_rst),
        .ap_rst_n(ap_rst_n),
        .ap_start(ap_start),
        .image_in_TDATA(image_in_TDATA),
        .image_in_TLAST(image_in_TLAST),
        .image_in_TREADY(image_in_TREADY),
        .image_in_TUSER(image_in_TUSER),
        .image_in_TVALID(image_in_TVALID),
        .input_data_data_stre_1_full_n(input_data_data_stre_1_full_n),
        .input_data_data_stre_2_full_n(input_data_data_stre_2_full_n),
        .input_data_data_stre_full_n(input_data_data_stre_full_n),
        .internal_empty_n_reg(AXIvideo2Mat_U0_n_7),
        .internal_empty_n_reg_0(AXIvideo2Mat_U0_n_8),
        .internal_empty_n_reg_1(AXIvideo2Mat_U0_n_9),
        .\mOutPtr_reg[1] (AXIvideo2Mat_U0_n_5),
        .start_for_Loop_loop_height_pro_U0_full_n(start_for_Loop_loop_height_pro_U0_full_n),
        .start_once_reg(start_once_reg));
  GND GND
       (.G(\<const0> ));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Loop_loop_height_pro Loop_loop_height_pro_U0
       (.Loop_loop_height_pro_U0_ap_ready(Loop_loop_height_pro_U0_ap_ready),
        .Loop_loop_height_pro_U0_passthrough_data_stream_2_V_write(Loop_loop_height_pro_U0_passthrough_data_stream_2_V_write),
        .Q({ap_CS_fsm_state4,Loop_loop_height_pro_U0_n_3}),
        .\SRL_SIG_reg[0][7] (Loop_loop_height_pro_U0_n_11),
        .\SRL_SIG_reg[0][7]_0 (Loop_loop_height_pro_U0_n_12),
        .\SRL_SIG_reg[0][7]_1 (Loop_loop_height_pro_U0_n_13),
        .ap_clk(ap_clk),
        .ap_rst(ap_rst),
        .ce(ce_4),
        .ce_0(ce_3),
        .ce_1(ce_2),
        .ce_2(ce_1),
        .ce_3(ce_0),
        .ce_4(ce),
        .crop_data_data_strea_1_full_n(crop_data_data_strea_1_full_n),
        .crop_data_data_strea_2_full_n(crop_data_data_strea_2_full_n),
        .crop_data_data_strea_full_n(crop_data_data_strea_full_n),
        .input_data_data_stre_empty_n(input_data_data_stre_empty_n),
        .internal_full_n_reg(start_for_Mat2AXIcud_U_n_2),
        .internal_full_n_reg_0(passthrough_data_str_1_U_n_2),
        .internal_full_n_reg_1(passthrough_data_str_2_U_n_2),
        .internal_full_n_reg_2(crop_data_data_strea_2_U_n_2),
        .passthrough_data_str_1_full_n(passthrough_data_str_1_full_n),
        .passthrough_data_str_2_full_n(passthrough_data_str_2_full_n),
        .passthrough_data_str_full_n(passthrough_data_str_full_n),
        .start_for_Loop_loop_height_pro_U0_empty_n(start_for_Loop_loop_height_pro_U0_empty_n),
        .start_for_Mat2AXIvideo_1_U0_full_n(start_for_Mat2AXIvideo_1_U0_full_n),
        .start_for_Mat2AXIvideo_U0_full_n(start_for_Mat2AXIvideo_U0_full_n),
        .start_once_reg_reg_0(Loop_loop_height_pro_U0_n_0));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Mat2AXIvideo_1 Mat2AXIvideo_1_U0
       (.D(tmp_data_V_fu_230_p4),
        .Mat2AXIvideo_1_U0_ap_ready(Mat2AXIvideo_1_U0_ap_ready),
        .Mat2AXIvideo_1_U0_img_data_stream_2_V_read(Mat2AXIvideo_1_U0_img_data_stream_2_V_read),
        .Q(Mat2AXIvideo_1_U0_n_2),
        .ap_clk(ap_clk),
        .ap_done_reg(ap_done_reg),
        .ap_done_reg_reg_0(Mat2AXIvideo_U0_n_3),
        .ap_rst(ap_rst),
        .ap_rst_n(ap_rst_n),
        .crop_data_data_strea_1_empty_n(crop_data_data_strea_1_empty_n),
        .crop_data_data_strea_2_empty_n(crop_data_data_strea_2_empty_n),
        .crop_data_data_strea_empty_n(crop_data_data_strea_empty_n),
        .crop_out_TDATA(crop_out_TDATA),
        .crop_out_TLAST(crop_out_TLAST),
        .crop_out_TREADY(crop_out_TREADY),
        .crop_out_TUSER(crop_out_TUSER),
        .crop_out_TVALID(crop_out_TVALID),
        .start_for_Mat2AXIvideo_1_U0_empty_n(start_for_Mat2AXIvideo_1_U0_empty_n));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Mat2AXIvideo Mat2AXIvideo_U0
       (.D(tmp_data_V_fu_234_p4),
        .Mat2AXIvideo_1_U0_ap_ready(Mat2AXIvideo_1_U0_ap_ready),
        .Mat2AXIvideo_U0_ap_ready(Mat2AXIvideo_U0_ap_ready),
        .Mat2AXIvideo_U0_img_data_stream_2_V_read(Mat2AXIvideo_U0_img_data_stream_2_V_read),
        .Q(Mat2AXIvideo_1_U0_n_2),
        .\ap_CS_fsm_reg[0]_0 (AXIvideo2Mat_U0_n_4),
        .\ap_CS_fsm_reg[0]_1 (Loop_loop_height_pro_U0_n_3),
        .ap_clk(ap_clk),
        .ap_done(ap_done),
        .ap_done_reg(ap_done_reg),
        .ap_done_reg_reg_0(Mat2AXIvideo_U0_n_3),
        .ap_idle(Mat2AXIvideo_U0_n_5),
        .ap_rst(ap_rst),
        .ap_rst_n(ap_rst_n),
        .image_out_TDATA(image_out_TDATA),
        .image_out_TLAST(image_out_TLAST),
        .image_out_TREADY(image_out_TREADY),
        .image_out_TUSER(image_out_TUSER),
        .image_out_TVALID(image_out_TVALID),
        .passthrough_data_str_1_empty_n(passthrough_data_str_1_empty_n),
        .passthrough_data_str_2_empty_n(passthrough_data_str_2_empty_n),
        .passthrough_data_str_empty_n(passthrough_data_str_empty_n),
        .start_for_Mat2AXIvideo_1_U0_empty_n(start_for_Mat2AXIvideo_1_U0_empty_n),
        .start_for_Mat2AXIvideo_U0_empty_n(start_for_Mat2AXIvideo_U0_empty_n));
  VCC VCC
       (.P(\<const1> ));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A crop_data_data_strea_1_U
       (.D(tmp_data_V_fu_230_p4[15:8]),
        .Mat2AXIvideo_1_U0_img_data_stream_2_V_read(Mat2AXIvideo_1_U0_img_data_stream_2_V_read),
        .\SRL_SIG_reg[1][7] ({input_data_data_stre_1_U_n_2,input_data_data_stre_1_U_n_3,input_data_data_stre_1_U_n_4,input_data_data_stre_1_U_n_5,input_data_data_stre_1_U_n_6,input_data_data_stre_1_U_n_7,input_data_data_stre_1_U_n_8,input_data_data_stre_1_U_n_9}),
        .ap_clk(ap_clk),
        .ap_rst(ap_rst),
        .ap_rst_n(ap_rst_n),
        .ce(ce_0),
        .crop_data_data_strea_1_empty_n(crop_data_data_strea_1_empty_n),
        .crop_data_data_strea_1_full_n(crop_data_data_strea_1_full_n));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_0 crop_data_data_strea_2_U
       (.D(tmp_data_V_fu_230_p4[23:16]),
        .Mat2AXIvideo_1_U0_img_data_stream_2_V_read(Mat2AXIvideo_1_U0_img_data_stream_2_V_read),
        .Q(ap_CS_fsm_state4),
        .\SRL_SIG_reg[1][7] ({input_data_data_stre_2_U_n_2,input_data_data_stre_2_U_n_3,input_data_data_stre_2_U_n_4,input_data_data_stre_2_U_n_5,input_data_data_stre_2_U_n_6,input_data_data_stre_2_U_n_7,input_data_data_stre_2_U_n_8,input_data_data_stre_2_U_n_9}),
        .ap_clk(ap_clk),
        .ap_rst(ap_rst),
        .ap_rst_n(ap_rst_n),
        .ce(ce),
        .crop_data_data_strea_1_full_n(crop_data_data_strea_1_full_n),
        .crop_data_data_strea_2_empty_n(crop_data_data_strea_2_empty_n),
        .crop_data_data_strea_2_full_n(crop_data_data_strea_2_full_n),
        .crop_data_data_strea_full_n(crop_data_data_strea_full_n),
        .\p_070_1_reg_275_reg[0] (crop_data_data_strea_2_U_n_2));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_1 crop_data_data_strea_U
       (.D(tmp_data_V_fu_230_p4[7:0]),
        .Mat2AXIvideo_1_U0_img_data_stream_2_V_read(Mat2AXIvideo_1_U0_img_data_stream_2_V_read),
        .\SRL_SIG_reg[1][7] ({input_data_data_stre_U_n_2,input_data_data_stre_U_n_3,input_data_data_stre_U_n_4,input_data_data_stre_U_n_5,input_data_data_stre_U_n_6,input_data_data_stre_U_n_7,input_data_data_stre_U_n_8,input_data_data_stre_U_n_9}),
        .ap_clk(ap_clk),
        .ap_rst(ap_rst),
        .ap_rst_n(ap_rst_n),
        .ce(ce_1),
        .crop_data_data_strea_empty_n(crop_data_data_strea_empty_n),
        .crop_data_data_strea_full_n(crop_data_data_strea_full_n));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_2 input_data_data_stre_1_U
       (.AXIvideo2Mat_U0_img_data_stream_2_V_write(AXIvideo2Mat_U0_img_data_stream_2_V_write),
        .D(data),
        .Loop_loop_height_pro_U0_passthrough_data_stream_2_V_write(Loop_loop_height_pro_U0_passthrough_data_stream_2_V_write),
        .\SRL_SIG_reg[0][7] ({input_data_data_stre_1_U_n_2,input_data_data_stre_1_U_n_3,input_data_data_stre_1_U_n_4,input_data_data_stre_1_U_n_5,input_data_data_stre_1_U_n_6,input_data_data_stre_1_U_n_7,input_data_data_stre_1_U_n_8,input_data_data_stre_1_U_n_9}),
        .ap_clk(ap_clk),
        .ap_enable_reg_pp1_iter1_reg(AXIvideo2Mat_U0_n_8),
        .ap_rst(ap_rst),
        .ap_rst_n(ap_rst_n),
        .input_data_data_stre_1_empty_n(input_data_data_stre_1_empty_n),
        .input_data_data_stre_1_full_n(input_data_data_stre_1_full_n));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_3 input_data_data_stre_2_U
       (.AXIvideo2Mat_U0_img_data_stream_2_V_write(AXIvideo2Mat_U0_img_data_stream_2_V_write),
        .D({AXIvideo2Mat_U0_n_10,AXIvideo2Mat_U0_n_11,AXIvideo2Mat_U0_n_12,AXIvideo2Mat_U0_n_13,AXIvideo2Mat_U0_n_14,AXIvideo2Mat_U0_n_15,AXIvideo2Mat_U0_n_16,AXIvideo2Mat_U0_n_17}),
        .Loop_loop_height_pro_U0_passthrough_data_stream_2_V_write(Loop_loop_height_pro_U0_passthrough_data_stream_2_V_write),
        .\SRL_SIG_reg[0][7] ({input_data_data_stre_2_U_n_2,input_data_data_stre_2_U_n_3,input_data_data_stre_2_U_n_4,input_data_data_stre_2_U_n_5,input_data_data_stre_2_U_n_6,input_data_data_stre_2_U_n_7,input_data_data_stre_2_U_n_8,input_data_data_stre_2_U_n_9}),
        .ap_clk(ap_clk),
        .ap_enable_reg_pp1_iter1_reg(AXIvideo2Mat_U0_n_7),
        .ap_rst(ap_rst),
        .ap_rst_n(ap_rst_n),
        .input_data_data_stre_2_empty_n(input_data_data_stre_2_empty_n),
        .input_data_data_stre_2_full_n(input_data_data_stre_2_full_n));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_4 input_data_data_stre_U
       (.AXIvideo2Mat_U0_img_data_stream_2_V_write(AXIvideo2Mat_U0_img_data_stream_2_V_write),
        .D({AXIvideo2Mat_U0_n_26,AXIvideo2Mat_U0_n_27,AXIvideo2Mat_U0_n_28,AXIvideo2Mat_U0_n_29,AXIvideo2Mat_U0_n_30,AXIvideo2Mat_U0_n_31,AXIvideo2Mat_U0_n_32,AXIvideo2Mat_U0_n_33}),
        .Loop_loop_height_pro_U0_passthrough_data_stream_2_V_write(Loop_loop_height_pro_U0_passthrough_data_stream_2_V_write),
        .\SRL_SIG_reg[0][7] ({input_data_data_stre_U_n_2,input_data_data_stre_U_n_3,input_data_data_stre_U_n_4,input_data_data_stre_U_n_5,input_data_data_stre_U_n_6,input_data_data_stre_U_n_7,input_data_data_stre_U_n_8,input_data_data_stre_U_n_9}),
        .ap_clk(ap_clk),
        .ap_enable_reg_pp1_iter1_reg(AXIvideo2Mat_U0_n_9),
        .ap_rst(ap_rst),
        .ap_rst_n(ap_rst_n),
        .input_data_data_stre_empty_n(input_data_data_stre_empty_n),
        .input_data_data_stre_full_n(input_data_data_stre_full_n));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_5 passthrough_data_str_1_U
       (.D(tmp_data_V_fu_234_p4[15:8]),
        .Mat2AXIvideo_U0_img_data_stream_2_V_read(Mat2AXIvideo_U0_img_data_stream_2_V_read),
        .\SRL_SIG_reg[1][7] ({input_data_data_stre_1_U_n_2,input_data_data_stre_1_U_n_3,input_data_data_stre_1_U_n_4,input_data_data_stre_1_U_n_5,input_data_data_stre_1_U_n_6,input_data_data_stre_1_U_n_7,input_data_data_stre_1_U_n_8,input_data_data_stre_1_U_n_9}),
        .ap_clk(ap_clk),
        .ap_rst(ap_rst),
        .ap_rst_n(ap_rst_n),
        .ce(ce_3),
        .input_data_data_stre_1_empty_n(input_data_data_stre_1_empty_n),
        .input_data_data_stre_2_empty_n(input_data_data_stre_2_empty_n),
        .internal_full_n_reg_0(Loop_loop_height_pro_U0_n_12),
        .\p_1_reg_264_reg[0] (passthrough_data_str_1_U_n_2),
        .passthrough_data_str_1_empty_n(passthrough_data_str_1_empty_n),
        .passthrough_data_str_1_full_n(passthrough_data_str_1_full_n),
        .passthrough_data_str_full_n(passthrough_data_str_full_n));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_6 passthrough_data_str_2_U
       (.D(tmp_data_V_fu_234_p4[23:16]),
        .Mat2AXIvideo_U0_img_data_stream_2_V_read(Mat2AXIvideo_U0_img_data_stream_2_V_read),
        .\SRL_SIG_reg[1][7] ({input_data_data_stre_2_U_n_2,input_data_data_stre_2_U_n_3,input_data_data_stre_2_U_n_4,input_data_data_stre_2_U_n_5,input_data_data_stre_2_U_n_6,input_data_data_stre_2_U_n_7,input_data_data_stre_2_U_n_8,input_data_data_stre_2_U_n_9}),
        .ap_clk(ap_clk),
        .ap_rst(ap_rst),
        .ap_rst_n(ap_rst_n),
        .ce(ce_4),
        .input_data_data_stre_1_empty_n(input_data_data_stre_1_empty_n),
        .input_data_data_stre_2_empty_n(input_data_data_stre_2_empty_n),
        .input_data_data_stre_empty_n(input_data_data_stre_empty_n),
        .internal_full_n_reg_0(Loop_loop_height_pro_U0_n_11),
        .\p_070_1_reg_275_reg[0] (passthrough_data_str_2_U_n_2),
        .passthrough_data_str_1_full_n(passthrough_data_str_1_full_n),
        .passthrough_data_str_2_empty_n(passthrough_data_str_2_empty_n),
        .passthrough_data_str_2_full_n(passthrough_data_str_2_full_n),
        .passthrough_data_str_full_n(passthrough_data_str_full_n));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_7 passthrough_data_str_U
       (.D(tmp_data_V_fu_234_p4[7:0]),
        .Mat2AXIvideo_U0_img_data_stream_2_V_read(Mat2AXIvideo_U0_img_data_stream_2_V_read),
        .\SRL_SIG_reg[1][7] ({input_data_data_stre_U_n_2,input_data_data_stre_U_n_3,input_data_data_stre_U_n_4,input_data_data_stre_U_n_5,input_data_data_stre_U_n_6,input_data_data_stre_U_n_7,input_data_data_stre_U_n_8,input_data_data_stre_U_n_9}),
        .ap_clk(ap_clk),
        .ap_rst(ap_rst),
        .ap_rst_n(ap_rst_n),
        .ce(ce_2),
        .internal_full_n_reg_0(Loop_loop_height_pro_U0_n_13),
        .passthrough_data_str_empty_n(passthrough_data_str_empty_n),
        .passthrough_data_str_full_n(passthrough_data_str_full_n));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_start_for_Loop_lobkb start_for_Loop_lobkb_U
       (.Loop_loop_height_pro_U0_ap_ready(Loop_loop_height_pro_U0_ap_ready),
        .\ap_CS_fsm_reg[0] (Mat2AXIvideo_U0_n_5),
        .ap_clk(ap_clk),
        .ap_idle(ap_idle),
        .ap_rst(ap_rst),
        .ap_rst_n(ap_rst_n),
        .ap_start(ap_start),
        .internal_full_n_reg_0(start_for_Loop_lobkb_U_n_3),
        .internal_full_n_reg_1(start_for_Mat2AXIcud_U_n_2),
        .start_for_Loop_loop_height_pro_U0_empty_n(start_for_Loop_loop_height_pro_U0_empty_n),
        .start_for_Loop_loop_height_pro_U0_full_n(start_for_Loop_loop_height_pro_U0_full_n),
        .start_for_Mat2AXIvideo_1_U0_full_n(start_for_Mat2AXIvideo_1_U0_full_n),
        .start_for_Mat2AXIvideo_U0_full_n(start_for_Mat2AXIvideo_U0_full_n),
        .start_once_reg(start_once_reg),
        .start_once_reg_reg(AXIvideo2Mat_U0_n_5),
        .start_once_reg_reg_0(Loop_loop_height_pro_U0_n_0));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_start_for_Mat2AXIcud start_for_Mat2AXIcud_U
       (.Mat2AXIvideo_U0_ap_ready(Mat2AXIvideo_U0_ap_ready),
        .ap_clk(ap_clk),
        .ap_rst(ap_rst),
        .ap_rst_n(ap_rst_n),
        .internal_empty_n_reg_0(start_for_Loop_lobkb_U_n_3),
        .\mOutPtr_reg[0]_0 (start_for_Mat2AXIcud_U_n_2),
        .start_for_Loop_loop_height_pro_U0_empty_n(start_for_Loop_loop_height_pro_U0_empty_n),
        .start_for_Mat2AXIvideo_1_U0_full_n(start_for_Mat2AXIvideo_1_U0_full_n),
        .start_for_Mat2AXIvideo_U0_empty_n(start_for_Mat2AXIvideo_U0_empty_n),
        .start_for_Mat2AXIvideo_U0_full_n(start_for_Mat2AXIvideo_U0_full_n),
        .start_once_reg_reg(Loop_loop_height_pro_U0_n_0));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_start_for_Mat2AXIdEe start_for_Mat2AXIdEe_U
       (.Mat2AXIvideo_1_U0_ap_ready(Mat2AXIvideo_1_U0_ap_ready),
        .ap_clk(ap_clk),
        .ap_rst(ap_rst),
        .ap_rst_n(ap_rst_n),
        .internal_empty_n_reg_0(start_for_Loop_lobkb_U_n_3),
        .internal_full_n_reg_0(start_for_Mat2AXIcud_U_n_2),
        .start_for_Mat2AXIvideo_1_U0_empty_n(start_for_Mat2AXIvideo_1_U0_empty_n),
        .start_for_Mat2AXIvideo_1_U0_full_n(start_for_Mat2AXIvideo_1_U0_full_n),
        .start_once_reg_reg(Loop_loop_height_pro_U0_n_0));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A
   (crop_data_data_strea_1_full_n,
    crop_data_data_strea_1_empty_n,
    D,
    ap_clk,
    ap_rst_n,
    ce,
    Mat2AXIvideo_1_U0_img_data_stream_2_V_read,
    ap_rst,
    \SRL_SIG_reg[1][7] );
  output crop_data_data_strea_1_full_n;
  output crop_data_data_strea_1_empty_n;
  output [7:0]D;
  input ap_clk;
  input ap_rst_n;
  input ce;
  input Mat2AXIvideo_1_U0_img_data_stream_2_V_read;
  input ap_rst;
  input [7:0]\SRL_SIG_reg[1][7] ;

  wire [7:0]D;
  wire Mat2AXIvideo_1_U0_img_data_stream_2_V_read;
  wire [7:0]\SRL_SIG_reg[1][7] ;
  wire ap_clk;
  wire ap_rst;
  wire ap_rst_n;
  wire ce;
  wire crop_data_data_strea_1_empty_n;
  wire crop_data_data_strea_1_full_n;
  wire internal_empty_n_i_1__9_n_0;
  wire internal_full_n_i_1__9_n_0;
  wire internal_full_n_i_2__8_n_0;
  wire \mOutPtr[0]_i_1_n_0 ;
  wire \mOutPtr[1]_i_1_n_0 ;
  wire \mOutPtr_reg_n_0_[0] ;
  wire \mOutPtr_reg_n_0_[1] ;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_shiftReg_15 U_fifo_w8_d2_A_shiftReg
       (.D(D),
        .\SRL_SIG_reg[1][7]_0 (\SRL_SIG_reg[1][7] ),
        .ap_clk(ap_clk),
        .ce(ce),
        .\mOutPtr_reg[0] (\mOutPtr_reg_n_0_[0] ),
        .\mOutPtr_reg[1] (\mOutPtr_reg_n_0_[1] ));
  LUT6 #(
    .INIT(64'hAAA8AAAAAA00AA00)) 
    internal_empty_n_i_1__9
       (.I0(ap_rst_n),
        .I1(\mOutPtr_reg_n_0_[1] ),
        .I2(\mOutPtr_reg_n_0_[0] ),
        .I3(ce),
        .I4(Mat2AXIvideo_1_U0_img_data_stream_2_V_read),
        .I5(crop_data_data_strea_1_empty_n),
        .O(internal_empty_n_i_1__9_n_0));
  FDRE #(
    .INIT(1'b0)) 
    internal_empty_n_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(internal_empty_n_i_1__9_n_0),
        .Q(crop_data_data_strea_1_empty_n),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hDDFF5DDD5DDD5DDD)) 
    internal_full_n_i_1__9
       (.I0(ap_rst_n),
        .I1(crop_data_data_strea_1_full_n),
        .I2(internal_full_n_i_2__8_n_0),
        .I3(ce),
        .I4(Mat2AXIvideo_1_U0_img_data_stream_2_V_read),
        .I5(crop_data_data_strea_1_empty_n),
        .O(internal_full_n_i_1__9_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    internal_full_n_i_2__8
       (.I0(\mOutPtr_reg_n_0_[0] ),
        .I1(\mOutPtr_reg_n_0_[1] ),
        .O(internal_full_n_i_2__8_n_0));
  FDRE #(
    .INIT(1'b1)) 
    internal_full_n_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(internal_full_n_i_1__9_n_0),
        .Q(crop_data_data_strea_1_full_n),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair144" *) 
  LUT4 #(
    .INIT(16'h8778)) 
    \mOutPtr[0]_i_1 
       (.I0(crop_data_data_strea_1_empty_n),
        .I1(Mat2AXIvideo_1_U0_img_data_stream_2_V_read),
        .I2(ce),
        .I3(\mOutPtr_reg_n_0_[0] ),
        .O(\mOutPtr[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair144" *) 
  LUT5 #(
    .INIT(32'hE7771888)) 
    \mOutPtr[1]_i_1 
       (.I0(\mOutPtr_reg_n_0_[0] ),
        .I1(ce),
        .I2(Mat2AXIvideo_1_U0_img_data_stream_2_V_read),
        .I3(crop_data_data_strea_1_empty_n),
        .I4(\mOutPtr_reg_n_0_[1] ),
        .O(\mOutPtr[1]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \mOutPtr_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\mOutPtr[0]_i_1_n_0 ),
        .Q(\mOutPtr_reg_n_0_[0] ),
        .S(ap_rst));
  FDSE #(
    .INIT(1'b1)) 
    \mOutPtr_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\mOutPtr[1]_i_1_n_0 ),
        .Q(\mOutPtr_reg_n_0_[1] ),
        .S(ap_rst));
endmodule

(* ORIG_REF_NAME = "fifo_w8_d2_A" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_0
   (crop_data_data_strea_2_full_n,
    crop_data_data_strea_2_empty_n,
    \p_070_1_reg_275_reg[0] ,
    D,
    ap_clk,
    ap_rst_n,
    ce,
    Mat2AXIvideo_1_U0_img_data_stream_2_V_read,
    Q,
    crop_data_data_strea_full_n,
    crop_data_data_strea_1_full_n,
    ap_rst,
    \SRL_SIG_reg[1][7] );
  output crop_data_data_strea_2_full_n;
  output crop_data_data_strea_2_empty_n;
  output \p_070_1_reg_275_reg[0] ;
  output [7:0]D;
  input ap_clk;
  input ap_rst_n;
  input ce;
  input Mat2AXIvideo_1_U0_img_data_stream_2_V_read;
  input [0:0]Q;
  input crop_data_data_strea_full_n;
  input crop_data_data_strea_1_full_n;
  input ap_rst;
  input [7:0]\SRL_SIG_reg[1][7] ;

  wire [7:0]D;
  wire Mat2AXIvideo_1_U0_img_data_stream_2_V_read;
  wire [0:0]Q;
  wire [7:0]\SRL_SIG_reg[1][7] ;
  wire ap_clk;
  wire ap_rst;
  wire ap_rst_n;
  wire ce;
  wire crop_data_data_strea_1_full_n;
  wire crop_data_data_strea_2_empty_n;
  wire crop_data_data_strea_2_full_n;
  wire crop_data_data_strea_full_n;
  wire internal_empty_n_i_1__10_n_0;
  wire internal_full_n_i_1__10_n_0;
  wire internal_full_n_i_2__9_n_0;
  wire \mOutPtr[0]_i_1_n_0 ;
  wire \mOutPtr[1]_i_1_n_0 ;
  wire \mOutPtr_reg_n_0_[0] ;
  wire \mOutPtr_reg_n_0_[1] ;
  wire \p_070_1_reg_275_reg[0] ;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_shiftReg_14 U_fifo_w8_d2_A_shiftReg
       (.D(D),
        .\SRL_SIG_reg[1][7]_0 (\SRL_SIG_reg[1][7] ),
        .ap_clk(ap_clk),
        .ce(ce),
        .\mOutPtr_reg[0] (\mOutPtr_reg_n_0_[0] ),
        .\mOutPtr_reg[1] (\mOutPtr_reg_n_0_[1] ));
  LUT6 #(
    .INIT(64'hAAA8AAAAAA00AA00)) 
    internal_empty_n_i_1__10
       (.I0(ap_rst_n),
        .I1(\mOutPtr_reg_n_0_[1] ),
        .I2(\mOutPtr_reg_n_0_[0] ),
        .I3(ce),
        .I4(Mat2AXIvideo_1_U0_img_data_stream_2_V_read),
        .I5(crop_data_data_strea_2_empty_n),
        .O(internal_empty_n_i_1__10_n_0));
  FDRE #(
    .INIT(1'b0)) 
    internal_empty_n_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(internal_empty_n_i_1__10_n_0),
        .Q(crop_data_data_strea_2_empty_n),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hDDFF5DDD5DDD5DDD)) 
    internal_full_n_i_1__10
       (.I0(ap_rst_n),
        .I1(crop_data_data_strea_2_full_n),
        .I2(internal_full_n_i_2__9_n_0),
        .I3(ce),
        .I4(Mat2AXIvideo_1_U0_img_data_stream_2_V_read),
        .I5(crop_data_data_strea_2_empty_n),
        .O(internal_full_n_i_1__10_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    internal_full_n_i_2__9
       (.I0(\mOutPtr_reg_n_0_[0] ),
        .I1(\mOutPtr_reg_n_0_[1] ),
        .O(internal_full_n_i_2__9_n_0));
  FDRE #(
    .INIT(1'b1)) 
    internal_full_n_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(internal_full_n_i_1__10_n_0),
        .Q(crop_data_data_strea_2_full_n),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair145" *) 
  LUT4 #(
    .INIT(16'h8778)) 
    \mOutPtr[0]_i_1 
       (.I0(crop_data_data_strea_2_empty_n),
        .I1(Mat2AXIvideo_1_U0_img_data_stream_2_V_read),
        .I2(ce),
        .I3(\mOutPtr_reg_n_0_[0] ),
        .O(\mOutPtr[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair145" *) 
  LUT5 #(
    .INIT(32'hE7771888)) 
    \mOutPtr[1]_i_1 
       (.I0(\mOutPtr_reg_n_0_[0] ),
        .I1(ce),
        .I2(Mat2AXIvideo_1_U0_img_data_stream_2_V_read),
        .I3(crop_data_data_strea_2_empty_n),
        .I4(\mOutPtr_reg_n_0_[1] ),
        .O(\mOutPtr[1]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \mOutPtr_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\mOutPtr[0]_i_1_n_0 ),
        .Q(\mOutPtr_reg_n_0_[0] ),
        .S(ap_rst));
  FDSE #(
    .INIT(1'b1)) 
    \mOutPtr_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\mOutPtr[1]_i_1_n_0 ),
        .Q(\mOutPtr_reg_n_0_[1] ),
        .S(ap_rst));
  LUT4 #(
    .INIT(16'h8000)) 
    \p_070_1_reg_275[10]_i_6 
       (.I0(crop_data_data_strea_2_full_n),
        .I1(Q),
        .I2(crop_data_data_strea_full_n),
        .I3(crop_data_data_strea_1_full_n),
        .O(\p_070_1_reg_275_reg[0] ));
endmodule

(* ORIG_REF_NAME = "fifo_w8_d2_A" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_1
   (crop_data_data_strea_full_n,
    crop_data_data_strea_empty_n,
    D,
    ap_clk,
    ap_rst_n,
    ce,
    Mat2AXIvideo_1_U0_img_data_stream_2_V_read,
    ap_rst,
    \SRL_SIG_reg[1][7] );
  output crop_data_data_strea_full_n;
  output crop_data_data_strea_empty_n;
  output [7:0]D;
  input ap_clk;
  input ap_rst_n;
  input ce;
  input Mat2AXIvideo_1_U0_img_data_stream_2_V_read;
  input ap_rst;
  input [7:0]\SRL_SIG_reg[1][7] ;

  wire [7:0]D;
  wire Mat2AXIvideo_1_U0_img_data_stream_2_V_read;
  wire [7:0]\SRL_SIG_reg[1][7] ;
  wire ap_clk;
  wire ap_rst;
  wire ap_rst_n;
  wire ce;
  wire crop_data_data_strea_empty_n;
  wire crop_data_data_strea_full_n;
  wire internal_empty_n_i_1__8_n_0;
  wire internal_full_n_i_1__8_n_0;
  wire internal_full_n_i_2__7_n_0;
  wire \mOutPtr[0]_i_1_n_0 ;
  wire \mOutPtr[1]_i_1_n_0 ;
  wire \mOutPtr_reg_n_0_[0] ;
  wire \mOutPtr_reg_n_0_[1] ;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_shiftReg_13 U_fifo_w8_d2_A_shiftReg
       (.D(D),
        .\SRL_SIG_reg[1][7]_0 (\SRL_SIG_reg[1][7] ),
        .ap_clk(ap_clk),
        .ce(ce),
        .\mOutPtr_reg[0] (\mOutPtr_reg_n_0_[0] ),
        .\mOutPtr_reg[1] (\mOutPtr_reg_n_0_[1] ));
  LUT6 #(
    .INIT(64'hAAA8AAAAAA00AA00)) 
    internal_empty_n_i_1__8
       (.I0(ap_rst_n),
        .I1(\mOutPtr_reg_n_0_[1] ),
        .I2(\mOutPtr_reg_n_0_[0] ),
        .I3(ce),
        .I4(Mat2AXIvideo_1_U0_img_data_stream_2_V_read),
        .I5(crop_data_data_strea_empty_n),
        .O(internal_empty_n_i_1__8_n_0));
  FDRE #(
    .INIT(1'b0)) 
    internal_empty_n_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(internal_empty_n_i_1__8_n_0),
        .Q(crop_data_data_strea_empty_n),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hDDFF5DDD5DDD5DDD)) 
    internal_full_n_i_1__8
       (.I0(ap_rst_n),
        .I1(crop_data_data_strea_full_n),
        .I2(internal_full_n_i_2__7_n_0),
        .I3(ce),
        .I4(Mat2AXIvideo_1_U0_img_data_stream_2_V_read),
        .I5(crop_data_data_strea_empty_n),
        .O(internal_full_n_i_1__8_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    internal_full_n_i_2__7
       (.I0(\mOutPtr_reg_n_0_[0] ),
        .I1(\mOutPtr_reg_n_0_[1] ),
        .O(internal_full_n_i_2__7_n_0));
  FDRE #(
    .INIT(1'b1)) 
    internal_full_n_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(internal_full_n_i_1__8_n_0),
        .Q(crop_data_data_strea_full_n),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair146" *) 
  LUT4 #(
    .INIT(16'h8778)) 
    \mOutPtr[0]_i_1 
       (.I0(crop_data_data_strea_empty_n),
        .I1(Mat2AXIvideo_1_U0_img_data_stream_2_V_read),
        .I2(ce),
        .I3(\mOutPtr_reg_n_0_[0] ),
        .O(\mOutPtr[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair146" *) 
  LUT5 #(
    .INIT(32'hE7771888)) 
    \mOutPtr[1]_i_1 
       (.I0(\mOutPtr_reg_n_0_[0] ),
        .I1(ce),
        .I2(Mat2AXIvideo_1_U0_img_data_stream_2_V_read),
        .I3(crop_data_data_strea_empty_n),
        .I4(\mOutPtr_reg_n_0_[1] ),
        .O(\mOutPtr[1]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \mOutPtr_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\mOutPtr[0]_i_1_n_0 ),
        .Q(\mOutPtr_reg_n_0_[0] ),
        .S(ap_rst));
  FDSE #(
    .INIT(1'b1)) 
    \mOutPtr_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\mOutPtr[1]_i_1_n_0 ),
        .Q(\mOutPtr_reg_n_0_[1] ),
        .S(ap_rst));
endmodule

(* ORIG_REF_NAME = "fifo_w8_d2_A" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_2
   (input_data_data_stre_1_full_n,
    input_data_data_stre_1_empty_n,
    \SRL_SIG_reg[0][7] ,
    ap_clk,
    AXIvideo2Mat_U0_img_data_stream_2_V_write,
    ap_rst_n,
    Loop_loop_height_pro_U0_passthrough_data_stream_2_V_write,
    ap_enable_reg_pp1_iter1_reg,
    ap_rst,
    D);
  output input_data_data_stre_1_full_n;
  output input_data_data_stre_1_empty_n;
  output [7:0]\SRL_SIG_reg[0][7] ;
  input ap_clk;
  input AXIvideo2Mat_U0_img_data_stream_2_V_write;
  input ap_rst_n;
  input Loop_loop_height_pro_U0_passthrough_data_stream_2_V_write;
  input ap_enable_reg_pp1_iter1_reg;
  input ap_rst;
  input [7:0]D;

  wire AXIvideo2Mat_U0_img_data_stream_2_V_write;
  wire [7:0]D;
  wire Loop_loop_height_pro_U0_passthrough_data_stream_2_V_write;
  wire [7:0]\SRL_SIG_reg[0][7] ;
  wire ap_clk;
  wire ap_enable_reg_pp1_iter1_reg;
  wire ap_rst;
  wire ap_rst_n;
  wire input_data_data_stre_1_empty_n;
  wire input_data_data_stre_1_full_n;
  wire internal_empty_n_i_1__1_n_0;
  wire internal_full_n_i_1__1_n_0;
  wire internal_full_n_i_2__0_n_0;
  wire \mOutPtr[0]_i_1_n_0 ;
  wire \mOutPtr[1]_i_1_n_0 ;
  wire \mOutPtr_reg_n_0_[0] ;
  wire \mOutPtr_reg_n_0_[1] ;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_shiftReg_12 U_fifo_w8_d2_A_shiftReg
       (.AXIvideo2Mat_U0_img_data_stream_2_V_write(AXIvideo2Mat_U0_img_data_stream_2_V_write),
        .D(D),
        .\SRL_SIG_reg[0][7]_0 (\SRL_SIG_reg[0][7] ),
        .ap_clk(ap_clk),
        .internal_full_n_reg(input_data_data_stre_1_full_n),
        .\mOutPtr_reg[0] (\mOutPtr_reg_n_0_[0] ),
        .\mOutPtr_reg[1] (\mOutPtr_reg_n_0_[1] ));
  LUT6 #(
    .INIT(64'hA8AA0000AAAAAAAA)) 
    internal_empty_n_i_1__1
       (.I0(ap_rst_n),
        .I1(\mOutPtr_reg_n_0_[1] ),
        .I2(\mOutPtr_reg_n_0_[0] ),
        .I3(Loop_loop_height_pro_U0_passthrough_data_stream_2_V_write),
        .I4(input_data_data_stre_1_empty_n),
        .I5(ap_enable_reg_pp1_iter1_reg),
        .O(internal_empty_n_i_1__1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    internal_empty_n_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(internal_empty_n_i_1__1_n_0),
        .Q(input_data_data_stre_1_empty_n),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hF777FFFFF555F555)) 
    internal_full_n_i_1__1
       (.I0(ap_rst_n),
        .I1(internal_full_n_i_2__0_n_0),
        .I2(Loop_loop_height_pro_U0_passthrough_data_stream_2_V_write),
        .I3(input_data_data_stre_1_empty_n),
        .I4(AXIvideo2Mat_U0_img_data_stream_2_V_write),
        .I5(input_data_data_stre_1_full_n),
        .O(internal_full_n_i_1__1_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    internal_full_n_i_2__0
       (.I0(\mOutPtr_reg_n_0_[0] ),
        .I1(\mOutPtr_reg_n_0_[1] ),
        .O(internal_full_n_i_2__0_n_0));
  FDRE #(
    .INIT(1'b1)) 
    internal_full_n_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(internal_full_n_i_1__1_n_0),
        .Q(input_data_data_stre_1_full_n),
        .R(1'b0));
  LUT5 #(
    .INIT(32'h87777888)) 
    \mOutPtr[0]_i_1 
       (.I0(input_data_data_stre_1_empty_n),
        .I1(Loop_loop_height_pro_U0_passthrough_data_stream_2_V_write),
        .I2(input_data_data_stre_1_full_n),
        .I3(AXIvideo2Mat_U0_img_data_stream_2_V_write),
        .I4(\mOutPtr_reg_n_0_[0] ),
        .O(\mOutPtr[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hEA7F7F7F15808080)) 
    \mOutPtr[1]_i_1 
       (.I0(\mOutPtr_reg_n_0_[0] ),
        .I1(AXIvideo2Mat_U0_img_data_stream_2_V_write),
        .I2(input_data_data_stre_1_full_n),
        .I3(Loop_loop_height_pro_U0_passthrough_data_stream_2_V_write),
        .I4(input_data_data_stre_1_empty_n),
        .I5(\mOutPtr_reg_n_0_[1] ),
        .O(\mOutPtr[1]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \mOutPtr_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\mOutPtr[0]_i_1_n_0 ),
        .Q(\mOutPtr_reg_n_0_[0] ),
        .S(ap_rst));
  FDSE #(
    .INIT(1'b1)) 
    \mOutPtr_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\mOutPtr[1]_i_1_n_0 ),
        .Q(\mOutPtr_reg_n_0_[1] ),
        .S(ap_rst));
endmodule

(* ORIG_REF_NAME = "fifo_w8_d2_A" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_3
   (input_data_data_stre_2_full_n,
    input_data_data_stre_2_empty_n,
    \SRL_SIG_reg[0][7] ,
    ap_clk,
    AXIvideo2Mat_U0_img_data_stream_2_V_write,
    ap_rst_n,
    Loop_loop_height_pro_U0_passthrough_data_stream_2_V_write,
    ap_enable_reg_pp1_iter1_reg,
    ap_rst,
    D);
  output input_data_data_stre_2_full_n;
  output input_data_data_stre_2_empty_n;
  output [7:0]\SRL_SIG_reg[0][7] ;
  input ap_clk;
  input AXIvideo2Mat_U0_img_data_stream_2_V_write;
  input ap_rst_n;
  input Loop_loop_height_pro_U0_passthrough_data_stream_2_V_write;
  input ap_enable_reg_pp1_iter1_reg;
  input ap_rst;
  input [7:0]D;

  wire AXIvideo2Mat_U0_img_data_stream_2_V_write;
  wire [7:0]D;
  wire Loop_loop_height_pro_U0_passthrough_data_stream_2_V_write;
  wire [7:0]\SRL_SIG_reg[0][7] ;
  wire ap_clk;
  wire ap_enable_reg_pp1_iter1_reg;
  wire ap_rst;
  wire ap_rst_n;
  wire input_data_data_stre_2_empty_n;
  wire input_data_data_stre_2_full_n;
  wire internal_empty_n_i_1__0_n_0;
  wire internal_full_n_i_1__0_n_0;
  wire internal_full_n_i_2__1_n_0;
  wire \mOutPtr[0]_i_1_n_0 ;
  wire \mOutPtr[1]_i_1_n_0 ;
  wire \mOutPtr_reg_n_0_[0] ;
  wire \mOutPtr_reg_n_0_[1] ;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_shiftReg_11 U_fifo_w8_d2_A_shiftReg
       (.AXIvideo2Mat_U0_img_data_stream_2_V_write(AXIvideo2Mat_U0_img_data_stream_2_V_write),
        .D(D),
        .\SRL_SIG_reg[0][7]_0 (\SRL_SIG_reg[0][7] ),
        .ap_clk(ap_clk),
        .internal_full_n_reg(input_data_data_stre_2_full_n),
        .\mOutPtr_reg[0] (\mOutPtr_reg_n_0_[0] ),
        .\mOutPtr_reg[1] (\mOutPtr_reg_n_0_[1] ));
  LUT6 #(
    .INIT(64'hA8AA0000AAAAAAAA)) 
    internal_empty_n_i_1__0
       (.I0(ap_rst_n),
        .I1(\mOutPtr_reg_n_0_[1] ),
        .I2(\mOutPtr_reg_n_0_[0] ),
        .I3(Loop_loop_height_pro_U0_passthrough_data_stream_2_V_write),
        .I4(input_data_data_stre_2_empty_n),
        .I5(ap_enable_reg_pp1_iter1_reg),
        .O(internal_empty_n_i_1__0_n_0));
  FDRE #(
    .INIT(1'b0)) 
    internal_empty_n_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(internal_empty_n_i_1__0_n_0),
        .Q(input_data_data_stre_2_empty_n),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hF777FFFFF555F555)) 
    internal_full_n_i_1__0
       (.I0(ap_rst_n),
        .I1(internal_full_n_i_2__1_n_0),
        .I2(Loop_loop_height_pro_U0_passthrough_data_stream_2_V_write),
        .I3(input_data_data_stre_2_empty_n),
        .I4(AXIvideo2Mat_U0_img_data_stream_2_V_write),
        .I5(input_data_data_stre_2_full_n),
        .O(internal_full_n_i_1__0_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    internal_full_n_i_2__1
       (.I0(\mOutPtr_reg_n_0_[0] ),
        .I1(\mOutPtr_reg_n_0_[1] ),
        .O(internal_full_n_i_2__1_n_0));
  FDRE #(
    .INIT(1'b1)) 
    internal_full_n_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(internal_full_n_i_1__0_n_0),
        .Q(input_data_data_stre_2_full_n),
        .R(1'b0));
  LUT5 #(
    .INIT(32'h87777888)) 
    \mOutPtr[0]_i_1 
       (.I0(input_data_data_stre_2_empty_n),
        .I1(Loop_loop_height_pro_U0_passthrough_data_stream_2_V_write),
        .I2(input_data_data_stre_2_full_n),
        .I3(AXIvideo2Mat_U0_img_data_stream_2_V_write),
        .I4(\mOutPtr_reg_n_0_[0] ),
        .O(\mOutPtr[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hEA7F7F7F15808080)) 
    \mOutPtr[1]_i_1 
       (.I0(\mOutPtr_reg_n_0_[0] ),
        .I1(AXIvideo2Mat_U0_img_data_stream_2_V_write),
        .I2(input_data_data_stre_2_full_n),
        .I3(Loop_loop_height_pro_U0_passthrough_data_stream_2_V_write),
        .I4(input_data_data_stre_2_empty_n),
        .I5(\mOutPtr_reg_n_0_[1] ),
        .O(\mOutPtr[1]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \mOutPtr_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\mOutPtr[0]_i_1_n_0 ),
        .Q(\mOutPtr_reg_n_0_[0] ),
        .S(ap_rst));
  FDSE #(
    .INIT(1'b1)) 
    \mOutPtr_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\mOutPtr[1]_i_1_n_0 ),
        .Q(\mOutPtr_reg_n_0_[1] ),
        .S(ap_rst));
endmodule

(* ORIG_REF_NAME = "fifo_w8_d2_A" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_4
   (input_data_data_stre_full_n,
    input_data_data_stre_empty_n,
    \SRL_SIG_reg[0][7] ,
    ap_clk,
    AXIvideo2Mat_U0_img_data_stream_2_V_write,
    ap_rst_n,
    Loop_loop_height_pro_U0_passthrough_data_stream_2_V_write,
    ap_enable_reg_pp1_iter1_reg,
    ap_rst,
    D);
  output input_data_data_stre_full_n;
  output input_data_data_stre_empty_n;
  output [7:0]\SRL_SIG_reg[0][7] ;
  input ap_clk;
  input AXIvideo2Mat_U0_img_data_stream_2_V_write;
  input ap_rst_n;
  input Loop_loop_height_pro_U0_passthrough_data_stream_2_V_write;
  input ap_enable_reg_pp1_iter1_reg;
  input ap_rst;
  input [7:0]D;

  wire AXIvideo2Mat_U0_img_data_stream_2_V_write;
  wire [7:0]D;
  wire Loop_loop_height_pro_U0_passthrough_data_stream_2_V_write;
  wire [7:0]\SRL_SIG_reg[0][7] ;
  wire ap_clk;
  wire ap_enable_reg_pp1_iter1_reg;
  wire ap_rst;
  wire ap_rst_n;
  wire input_data_data_stre_empty_n;
  wire input_data_data_stre_full_n;
  wire internal_empty_n_i_1__2_n_0;
  wire internal_full_n_i_1__2_n_0;
  wire internal_full_n_i_2_n_0;
  wire [1:0]mOutPtr;
  wire \mOutPtr[0]_i_1_n_0 ;
  wire \mOutPtr[1]_i_1_n_0 ;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_shiftReg_10 U_fifo_w8_d2_A_shiftReg
       (.AXIvideo2Mat_U0_img_data_stream_2_V_write(AXIvideo2Mat_U0_img_data_stream_2_V_write),
        .D(D),
        .\SRL_SIG_reg[0][7]_0 (\SRL_SIG_reg[0][7] ),
        .ap_clk(ap_clk),
        .internal_full_n_reg(input_data_data_stre_full_n),
        .mOutPtr(mOutPtr));
  LUT6 #(
    .INIT(64'hA8AA0000AAAAAAAA)) 
    internal_empty_n_i_1__2
       (.I0(ap_rst_n),
        .I1(mOutPtr[1]),
        .I2(mOutPtr[0]),
        .I3(Loop_loop_height_pro_U0_passthrough_data_stream_2_V_write),
        .I4(input_data_data_stre_empty_n),
        .I5(ap_enable_reg_pp1_iter1_reg),
        .O(internal_empty_n_i_1__2_n_0));
  FDRE #(
    .INIT(1'b0)) 
    internal_empty_n_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(internal_empty_n_i_1__2_n_0),
        .Q(input_data_data_stre_empty_n),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hF777FFFFF555F555)) 
    internal_full_n_i_1__2
       (.I0(ap_rst_n),
        .I1(internal_full_n_i_2_n_0),
        .I2(Loop_loop_height_pro_U0_passthrough_data_stream_2_V_write),
        .I3(input_data_data_stre_empty_n),
        .I4(AXIvideo2Mat_U0_img_data_stream_2_V_write),
        .I5(input_data_data_stre_full_n),
        .O(internal_full_n_i_1__2_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    internal_full_n_i_2
       (.I0(mOutPtr[0]),
        .I1(mOutPtr[1]),
        .O(internal_full_n_i_2_n_0));
  FDRE #(
    .INIT(1'b1)) 
    internal_full_n_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(internal_full_n_i_1__2_n_0),
        .Q(input_data_data_stre_full_n),
        .R(1'b0));
  LUT5 #(
    .INIT(32'h87777888)) 
    \mOutPtr[0]_i_1 
       (.I0(input_data_data_stre_empty_n),
        .I1(Loop_loop_height_pro_U0_passthrough_data_stream_2_V_write),
        .I2(input_data_data_stre_full_n),
        .I3(AXIvideo2Mat_U0_img_data_stream_2_V_write),
        .I4(mOutPtr[0]),
        .O(\mOutPtr[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hEA7F7F7F15808080)) 
    \mOutPtr[1]_i_1 
       (.I0(mOutPtr[0]),
        .I1(AXIvideo2Mat_U0_img_data_stream_2_V_write),
        .I2(input_data_data_stre_full_n),
        .I3(Loop_loop_height_pro_U0_passthrough_data_stream_2_V_write),
        .I4(input_data_data_stre_empty_n),
        .I5(mOutPtr[1]),
        .O(\mOutPtr[1]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \mOutPtr_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\mOutPtr[0]_i_1_n_0 ),
        .Q(mOutPtr[0]),
        .S(ap_rst));
  FDSE #(
    .INIT(1'b1)) 
    \mOutPtr_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\mOutPtr[1]_i_1_n_0 ),
        .Q(mOutPtr[1]),
        .S(ap_rst));
endmodule

(* ORIG_REF_NAME = "fifo_w8_d2_A" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_5
   (passthrough_data_str_1_full_n,
    passthrough_data_str_1_empty_n,
    \p_1_reg_264_reg[0] ,
    D,
    ap_clk,
    ap_rst_n,
    ce,
    Mat2AXIvideo_U0_img_data_stream_2_V_read,
    passthrough_data_str_full_n,
    input_data_data_stre_2_empty_n,
    input_data_data_stre_1_empty_n,
    ap_rst,
    internal_full_n_reg_0,
    \SRL_SIG_reg[1][7] );
  output passthrough_data_str_1_full_n;
  output passthrough_data_str_1_empty_n;
  output \p_1_reg_264_reg[0] ;
  output [7:0]D;
  input ap_clk;
  input ap_rst_n;
  input ce;
  input Mat2AXIvideo_U0_img_data_stream_2_V_read;
  input passthrough_data_str_full_n;
  input input_data_data_stre_2_empty_n;
  input input_data_data_stre_1_empty_n;
  input ap_rst;
  input internal_full_n_reg_0;
  input [7:0]\SRL_SIG_reg[1][7] ;

  wire [7:0]D;
  wire Mat2AXIvideo_U0_img_data_stream_2_V_read;
  wire [7:0]\SRL_SIG_reg[1][7] ;
  wire ap_clk;
  wire ap_rst;
  wire ap_rst_n;
  wire ce;
  wire input_data_data_stre_1_empty_n;
  wire input_data_data_stre_2_empty_n;
  wire internal_empty_n_i_1__6_n_0;
  wire internal_full_n_i_1__6_n_0;
  wire internal_full_n_i_2__5_n_0;
  wire internal_full_n_reg_0;
  wire \mOutPtr[0]_i_1_n_0 ;
  wire \mOutPtr[1]_i_1_n_0 ;
  wire \mOutPtr_reg_n_0_[0] ;
  wire \mOutPtr_reg_n_0_[1] ;
  wire \p_1_reg_264_reg[0] ;
  wire passthrough_data_str_1_empty_n;
  wire passthrough_data_str_1_full_n;
  wire passthrough_data_str_full_n;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_shiftReg_9 U_fifo_w8_d2_A_shiftReg
       (.D(D),
        .\SRL_SIG_reg[1][7]_0 (\SRL_SIG_reg[1][7] ),
        .ap_clk(ap_clk),
        .ce(ce),
        .internal_full_n_reg(internal_full_n_reg_0),
        .\mOutPtr_reg[0] (\mOutPtr_reg_n_0_[0] ),
        .\mOutPtr_reg[1] (\mOutPtr_reg_n_0_[1] ));
  LUT6 #(
    .INIT(64'hAAA8AAAAAA00AA00)) 
    internal_empty_n_i_1__6
       (.I0(ap_rst_n),
        .I1(\mOutPtr_reg_n_0_[1] ),
        .I2(\mOutPtr_reg_n_0_[0] ),
        .I3(ce),
        .I4(Mat2AXIvideo_U0_img_data_stream_2_V_read),
        .I5(passthrough_data_str_1_empty_n),
        .O(internal_empty_n_i_1__6_n_0));
  FDRE #(
    .INIT(1'b0)) 
    internal_empty_n_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(internal_empty_n_i_1__6_n_0),
        .Q(passthrough_data_str_1_empty_n),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hDDFF5DDD5DDD5DDD)) 
    internal_full_n_i_1__6
       (.I0(ap_rst_n),
        .I1(passthrough_data_str_1_full_n),
        .I2(internal_full_n_i_2__5_n_0),
        .I3(ce),
        .I4(Mat2AXIvideo_U0_img_data_stream_2_V_read),
        .I5(passthrough_data_str_1_empty_n),
        .O(internal_full_n_i_1__6_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    internal_full_n_i_2__5
       (.I0(\mOutPtr_reg_n_0_[0] ),
        .I1(\mOutPtr_reg_n_0_[1] ),
        .O(internal_full_n_i_2__5_n_0));
  FDRE #(
    .INIT(1'b1)) 
    internal_full_n_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(internal_full_n_i_1__6_n_0),
        .Q(passthrough_data_str_1_full_n),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair147" *) 
  LUT4 #(
    .INIT(16'h8778)) 
    \mOutPtr[0]_i_1 
       (.I0(passthrough_data_str_1_empty_n),
        .I1(Mat2AXIvideo_U0_img_data_stream_2_V_read),
        .I2(ce),
        .I3(\mOutPtr_reg_n_0_[0] ),
        .O(\mOutPtr[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair147" *) 
  LUT5 #(
    .INIT(32'hE7771888)) 
    \mOutPtr[1]_i_1 
       (.I0(\mOutPtr_reg_n_0_[0] ),
        .I1(ce),
        .I2(Mat2AXIvideo_U0_img_data_stream_2_V_read),
        .I3(passthrough_data_str_1_empty_n),
        .I4(\mOutPtr_reg_n_0_[1] ),
        .O(\mOutPtr[1]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \mOutPtr_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\mOutPtr[0]_i_1_n_0 ),
        .Q(\mOutPtr_reg_n_0_[0] ),
        .S(ap_rst));
  FDSE #(
    .INIT(1'b1)) 
    \mOutPtr_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\mOutPtr[1]_i_1_n_0 ),
        .Q(\mOutPtr_reg_n_0_[1] ),
        .S(ap_rst));
  LUT4 #(
    .INIT(16'h8000)) 
    \p_070_2_reg_286[8]_i_4 
       (.I0(passthrough_data_str_1_full_n),
        .I1(passthrough_data_str_full_n),
        .I2(input_data_data_stre_2_empty_n),
        .I3(input_data_data_stre_1_empty_n),
        .O(\p_1_reg_264_reg[0] ));
endmodule

(* ORIG_REF_NAME = "fifo_w8_d2_A" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_6
   (passthrough_data_str_2_full_n,
    passthrough_data_str_2_empty_n,
    \p_070_1_reg_275_reg[0] ,
    D,
    ap_clk,
    ap_rst_n,
    ce,
    Mat2AXIvideo_U0_img_data_stream_2_V_read,
    input_data_data_stre_empty_n,
    input_data_data_stre_1_empty_n,
    input_data_data_stre_2_empty_n,
    passthrough_data_str_1_full_n,
    passthrough_data_str_full_n,
    ap_rst,
    internal_full_n_reg_0,
    \SRL_SIG_reg[1][7] );
  output passthrough_data_str_2_full_n;
  output passthrough_data_str_2_empty_n;
  output \p_070_1_reg_275_reg[0] ;
  output [7:0]D;
  input ap_clk;
  input ap_rst_n;
  input ce;
  input Mat2AXIvideo_U0_img_data_stream_2_V_read;
  input input_data_data_stre_empty_n;
  input input_data_data_stre_1_empty_n;
  input input_data_data_stre_2_empty_n;
  input passthrough_data_str_1_full_n;
  input passthrough_data_str_full_n;
  input ap_rst;
  input internal_full_n_reg_0;
  input [7:0]\SRL_SIG_reg[1][7] ;

  wire [7:0]D;
  wire Mat2AXIvideo_U0_img_data_stream_2_V_read;
  wire [7:0]\SRL_SIG_reg[1][7] ;
  wire ap_clk;
  wire ap_rst;
  wire ap_rst_n;
  wire ce;
  wire input_data_data_stre_1_empty_n;
  wire input_data_data_stre_2_empty_n;
  wire input_data_data_stre_empty_n;
  wire internal_empty_n_i_1__5_n_0;
  wire internal_full_n_i_1__5_n_0;
  wire internal_full_n_i_2__6_n_0;
  wire internal_full_n_reg_0;
  wire \mOutPtr[0]_i_1_n_0 ;
  wire \mOutPtr[1]_i_1_n_0 ;
  wire \mOutPtr_reg_n_0_[0] ;
  wire \mOutPtr_reg_n_0_[1] ;
  wire \p_070_1_reg_275_reg[0] ;
  wire passthrough_data_str_1_full_n;
  wire passthrough_data_str_2_empty_n;
  wire passthrough_data_str_2_full_n;
  wire passthrough_data_str_full_n;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_shiftReg_8 U_fifo_w8_d2_A_shiftReg
       (.D(D),
        .\SRL_SIG_reg[1][7]_0 (\SRL_SIG_reg[1][7] ),
        .ap_clk(ap_clk),
        .ce(ce),
        .internal_full_n_reg(internal_full_n_reg_0),
        .\mOutPtr_reg[0] (\mOutPtr_reg_n_0_[0] ),
        .\mOutPtr_reg[1] (\mOutPtr_reg_n_0_[1] ));
  LUT6 #(
    .INIT(64'hAAA8AAAAAA00AA00)) 
    internal_empty_n_i_1__5
       (.I0(ap_rst_n),
        .I1(\mOutPtr_reg_n_0_[1] ),
        .I2(\mOutPtr_reg_n_0_[0] ),
        .I3(ce),
        .I4(Mat2AXIvideo_U0_img_data_stream_2_V_read),
        .I5(passthrough_data_str_2_empty_n),
        .O(internal_empty_n_i_1__5_n_0));
  FDRE #(
    .INIT(1'b0)) 
    internal_empty_n_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(internal_empty_n_i_1__5_n_0),
        .Q(passthrough_data_str_2_empty_n),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hDDFF5DDD5DDD5DDD)) 
    internal_full_n_i_1__5
       (.I0(ap_rst_n),
        .I1(passthrough_data_str_2_full_n),
        .I2(internal_full_n_i_2__6_n_0),
        .I3(ce),
        .I4(Mat2AXIvideo_U0_img_data_stream_2_V_read),
        .I5(passthrough_data_str_2_empty_n),
        .O(internal_full_n_i_1__5_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    internal_full_n_i_2__6
       (.I0(\mOutPtr_reg_n_0_[0] ),
        .I1(\mOutPtr_reg_n_0_[1] ),
        .O(internal_full_n_i_2__6_n_0));
  FDRE #(
    .INIT(1'b1)) 
    internal_full_n_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(internal_full_n_i_1__5_n_0),
        .Q(passthrough_data_str_2_full_n),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair148" *) 
  LUT4 #(
    .INIT(16'h8778)) 
    \mOutPtr[0]_i_1 
       (.I0(passthrough_data_str_2_empty_n),
        .I1(Mat2AXIvideo_U0_img_data_stream_2_V_read),
        .I2(ce),
        .I3(\mOutPtr_reg_n_0_[0] ),
        .O(\mOutPtr[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair148" *) 
  LUT5 #(
    .INIT(32'hE7771888)) 
    \mOutPtr[1]_i_1 
       (.I0(\mOutPtr_reg_n_0_[0] ),
        .I1(ce),
        .I2(Mat2AXIvideo_U0_img_data_stream_2_V_read),
        .I3(passthrough_data_str_2_empty_n),
        .I4(\mOutPtr_reg_n_0_[1] ),
        .O(\mOutPtr[1]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \mOutPtr_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\mOutPtr[0]_i_1_n_0 ),
        .Q(\mOutPtr_reg_n_0_[0] ),
        .S(ap_rst));
  FDSE #(
    .INIT(1'b1)) 
    \mOutPtr_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\mOutPtr[1]_i_1_n_0 ),
        .Q(\mOutPtr_reg_n_0_[1] ),
        .S(ap_rst));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \p_070_1_reg_275[10]_i_5 
       (.I0(passthrough_data_str_2_full_n),
        .I1(input_data_data_stre_empty_n),
        .I2(input_data_data_stre_1_empty_n),
        .I3(input_data_data_stre_2_empty_n),
        .I4(passthrough_data_str_1_full_n),
        .I5(passthrough_data_str_full_n),
        .O(\p_070_1_reg_275_reg[0] ));
endmodule

(* ORIG_REF_NAME = "fifo_w8_d2_A" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_7
   (passthrough_data_str_full_n,
    passthrough_data_str_empty_n,
    D,
    ap_clk,
    ap_rst_n,
    ce,
    Mat2AXIvideo_U0_img_data_stream_2_V_read,
    ap_rst,
    internal_full_n_reg_0,
    \SRL_SIG_reg[1][7] );
  output passthrough_data_str_full_n;
  output passthrough_data_str_empty_n;
  output [7:0]D;
  input ap_clk;
  input ap_rst_n;
  input ce;
  input Mat2AXIvideo_U0_img_data_stream_2_V_read;
  input ap_rst;
  input internal_full_n_reg_0;
  input [7:0]\SRL_SIG_reg[1][7] ;

  wire [7:0]D;
  wire Mat2AXIvideo_U0_img_data_stream_2_V_read;
  wire [7:0]\SRL_SIG_reg[1][7] ;
  wire ap_clk;
  wire ap_rst;
  wire ap_rst_n;
  wire ce;
  wire internal_empty_n_i_1__7_n_0;
  wire internal_full_n_i_1__7_n_0;
  wire internal_full_n_i_2__4_n_0;
  wire internal_full_n_reg_0;
  wire \mOutPtr[0]_i_1_n_0 ;
  wire \mOutPtr[1]_i_1_n_0 ;
  wire \mOutPtr_reg_n_0_[0] ;
  wire \mOutPtr_reg_n_0_[1] ;
  wire passthrough_data_str_empty_n;
  wire passthrough_data_str_full_n;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_shiftReg U_fifo_w8_d2_A_shiftReg
       (.D(D),
        .\SRL_SIG_reg[1][7]_0 (\SRL_SIG_reg[1][7] ),
        .ap_clk(ap_clk),
        .ce(ce),
        .internal_full_n_reg(internal_full_n_reg_0),
        .\mOutPtr_reg[0] (\mOutPtr_reg_n_0_[0] ),
        .\mOutPtr_reg[1] (\mOutPtr_reg_n_0_[1] ));
  LUT6 #(
    .INIT(64'hAAA8AAAAAA00AA00)) 
    internal_empty_n_i_1__7
       (.I0(ap_rst_n),
        .I1(\mOutPtr_reg_n_0_[1] ),
        .I2(\mOutPtr_reg_n_0_[0] ),
        .I3(ce),
        .I4(Mat2AXIvideo_U0_img_data_stream_2_V_read),
        .I5(passthrough_data_str_empty_n),
        .O(internal_empty_n_i_1__7_n_0));
  FDRE #(
    .INIT(1'b0)) 
    internal_empty_n_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(internal_empty_n_i_1__7_n_0),
        .Q(passthrough_data_str_empty_n),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hDDFF5DDD5DDD5DDD)) 
    internal_full_n_i_1__7
       (.I0(ap_rst_n),
        .I1(passthrough_data_str_full_n),
        .I2(internal_full_n_i_2__4_n_0),
        .I3(ce),
        .I4(Mat2AXIvideo_U0_img_data_stream_2_V_read),
        .I5(passthrough_data_str_empty_n),
        .O(internal_full_n_i_1__7_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    internal_full_n_i_2__4
       (.I0(\mOutPtr_reg_n_0_[0] ),
        .I1(\mOutPtr_reg_n_0_[1] ),
        .O(internal_full_n_i_2__4_n_0));
  FDRE #(
    .INIT(1'b1)) 
    internal_full_n_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(internal_full_n_i_1__7_n_0),
        .Q(passthrough_data_str_full_n),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair149" *) 
  LUT4 #(
    .INIT(16'h8778)) 
    \mOutPtr[0]_i_1 
       (.I0(passthrough_data_str_empty_n),
        .I1(Mat2AXIvideo_U0_img_data_stream_2_V_read),
        .I2(ce),
        .I3(\mOutPtr_reg_n_0_[0] ),
        .O(\mOutPtr[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair149" *) 
  LUT5 #(
    .INIT(32'hE7771888)) 
    \mOutPtr[1]_i_1 
       (.I0(\mOutPtr_reg_n_0_[0] ),
        .I1(ce),
        .I2(Mat2AXIvideo_U0_img_data_stream_2_V_read),
        .I3(passthrough_data_str_empty_n),
        .I4(\mOutPtr_reg_n_0_[1] ),
        .O(\mOutPtr[1]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \mOutPtr_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\mOutPtr[0]_i_1_n_0 ),
        .Q(\mOutPtr_reg_n_0_[0] ),
        .S(ap_rst));
  FDSE #(
    .INIT(1'b1)) 
    \mOutPtr_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\mOutPtr[1]_i_1_n_0 ),
        .Q(\mOutPtr_reg_n_0_[1] ),
        .S(ap_rst));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_shiftReg
   (D,
    \mOutPtr_reg[0] ,
    \mOutPtr_reg[1] ,
    internal_full_n_reg,
    ce,
    \SRL_SIG_reg[1][7]_0 ,
    ap_clk);
  output [7:0]D;
  input \mOutPtr_reg[0] ;
  input \mOutPtr_reg[1] ;
  input internal_full_n_reg;
  input ce;
  input [7:0]\SRL_SIG_reg[1][7]_0 ;
  input ap_clk;

  wire [7:0]D;
  wire [7:0]\SRL_SIG_reg[1][7]_0 ;
  wire \SRL_SIG_reg_n_0_[0][0] ;
  wire \SRL_SIG_reg_n_0_[0][1] ;
  wire \SRL_SIG_reg_n_0_[0][2] ;
  wire \SRL_SIG_reg_n_0_[0][3] ;
  wire \SRL_SIG_reg_n_0_[0][4] ;
  wire \SRL_SIG_reg_n_0_[0][5] ;
  wire \SRL_SIG_reg_n_0_[0][6] ;
  wire \SRL_SIG_reg_n_0_[0][7] ;
  wire \SRL_SIG_reg_n_0_[1][0] ;
  wire \SRL_SIG_reg_n_0_[1][1] ;
  wire \SRL_SIG_reg_n_0_[1][2] ;
  wire \SRL_SIG_reg_n_0_[1][3] ;
  wire \SRL_SIG_reg_n_0_[1][4] ;
  wire \SRL_SIG_reg_n_0_[1][5] ;
  wire \SRL_SIG_reg_n_0_[1][6] ;
  wire \SRL_SIG_reg_n_0_[1][7] ;
  wire ap_clk;
  wire ce;
  wire internal_full_n_reg;
  wire \mOutPtr_reg[0] ;
  wire \mOutPtr_reg[1] ;

  LUT4 #(
    .INIT(16'hFB08)) 
    \AXI_video_strm_V_data_V_1_payload_A[0]_i_1 
       (.I0(\SRL_SIG_reg_n_0_[1][0] ),
        .I1(\mOutPtr_reg[0] ),
        .I2(\mOutPtr_reg[1] ),
        .I3(\SRL_SIG_reg_n_0_[0][0] ),
        .O(D[0]));
  LUT4 #(
    .INIT(16'hFB08)) 
    \AXI_video_strm_V_data_V_1_payload_A[1]_i_1 
       (.I0(\SRL_SIG_reg_n_0_[1][1] ),
        .I1(\mOutPtr_reg[0] ),
        .I2(\mOutPtr_reg[1] ),
        .I3(\SRL_SIG_reg_n_0_[0][1] ),
        .O(D[1]));
  LUT4 #(
    .INIT(16'hFB08)) 
    \AXI_video_strm_V_data_V_1_payload_A[2]_i_1 
       (.I0(\SRL_SIG_reg_n_0_[1][2] ),
        .I1(\mOutPtr_reg[0] ),
        .I2(\mOutPtr_reg[1] ),
        .I3(\SRL_SIG_reg_n_0_[0][2] ),
        .O(D[2]));
  LUT4 #(
    .INIT(16'hFB08)) 
    \AXI_video_strm_V_data_V_1_payload_A[3]_i_1 
       (.I0(\SRL_SIG_reg_n_0_[1][3] ),
        .I1(\mOutPtr_reg[0] ),
        .I2(\mOutPtr_reg[1] ),
        .I3(\SRL_SIG_reg_n_0_[0][3] ),
        .O(D[3]));
  LUT4 #(
    .INIT(16'hFB08)) 
    \AXI_video_strm_V_data_V_1_payload_A[4]_i_1 
       (.I0(\SRL_SIG_reg_n_0_[1][4] ),
        .I1(\mOutPtr_reg[0] ),
        .I2(\mOutPtr_reg[1] ),
        .I3(\SRL_SIG_reg_n_0_[0][4] ),
        .O(D[4]));
  LUT4 #(
    .INIT(16'hFB08)) 
    \AXI_video_strm_V_data_V_1_payload_A[5]_i_1 
       (.I0(\SRL_SIG_reg_n_0_[1][5] ),
        .I1(\mOutPtr_reg[0] ),
        .I2(\mOutPtr_reg[1] ),
        .I3(\SRL_SIG_reg_n_0_[0][5] ),
        .O(D[5]));
  LUT4 #(
    .INIT(16'hFB08)) 
    \AXI_video_strm_V_data_V_1_payload_A[6]_i_1 
       (.I0(\SRL_SIG_reg_n_0_[1][6] ),
        .I1(\mOutPtr_reg[0] ),
        .I2(\mOutPtr_reg[1] ),
        .I3(\SRL_SIG_reg_n_0_[0][6] ),
        .O(D[6]));
  LUT4 #(
    .INIT(16'hFB08)) 
    \AXI_video_strm_V_data_V_1_payload_A[7]_i_1 
       (.I0(\SRL_SIG_reg_n_0_[1][7] ),
        .I1(\mOutPtr_reg[0] ),
        .I2(\mOutPtr_reg[1] ),
        .I3(\SRL_SIG_reg_n_0_[0][7] ),
        .O(D[7]));
  FDRE \SRL_SIG_reg[0][0] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg[1][7]_0 [0]),
        .Q(\SRL_SIG_reg_n_0_[0][0] ),
        .R(internal_full_n_reg));
  FDRE \SRL_SIG_reg[0][1] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg[1][7]_0 [1]),
        .Q(\SRL_SIG_reg_n_0_[0][1] ),
        .R(internal_full_n_reg));
  FDRE \SRL_SIG_reg[0][2] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg[1][7]_0 [2]),
        .Q(\SRL_SIG_reg_n_0_[0][2] ),
        .R(internal_full_n_reg));
  FDRE \SRL_SIG_reg[0][3] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg[1][7]_0 [3]),
        .Q(\SRL_SIG_reg_n_0_[0][3] ),
        .R(internal_full_n_reg));
  FDRE \SRL_SIG_reg[0][4] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg[1][7]_0 [4]),
        .Q(\SRL_SIG_reg_n_0_[0][4] ),
        .R(internal_full_n_reg));
  FDRE \SRL_SIG_reg[0][5] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg[1][7]_0 [5]),
        .Q(\SRL_SIG_reg_n_0_[0][5] ),
        .R(internal_full_n_reg));
  FDRE \SRL_SIG_reg[0][6] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg[1][7]_0 [6]),
        .Q(\SRL_SIG_reg_n_0_[0][6] ),
        .R(internal_full_n_reg));
  FDRE \SRL_SIG_reg[0][7] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg[1][7]_0 [7]),
        .Q(\SRL_SIG_reg_n_0_[0][7] ),
        .R(internal_full_n_reg));
  FDRE \SRL_SIG_reg[1][0] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg_n_0_[0][0] ),
        .Q(\SRL_SIG_reg_n_0_[1][0] ),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][1] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg_n_0_[0][1] ),
        .Q(\SRL_SIG_reg_n_0_[1][1] ),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][2] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg_n_0_[0][2] ),
        .Q(\SRL_SIG_reg_n_0_[1][2] ),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][3] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg_n_0_[0][3] ),
        .Q(\SRL_SIG_reg_n_0_[1][3] ),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][4] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg_n_0_[0][4] ),
        .Q(\SRL_SIG_reg_n_0_[1][4] ),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][5] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg_n_0_[0][5] ),
        .Q(\SRL_SIG_reg_n_0_[1][5] ),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][6] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg_n_0_[0][6] ),
        .Q(\SRL_SIG_reg_n_0_[1][6] ),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][7] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg_n_0_[0][7] ),
        .Q(\SRL_SIG_reg_n_0_[1][7] ),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "fifo_w8_d2_A_shiftReg" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_shiftReg_10
   (\SRL_SIG_reg[0][7]_0 ,
    internal_full_n_reg,
    AXIvideo2Mat_U0_img_data_stream_2_V_write,
    mOutPtr,
    D,
    ap_clk);
  output [7:0]\SRL_SIG_reg[0][7]_0 ;
  input internal_full_n_reg;
  input AXIvideo2Mat_U0_img_data_stream_2_V_write;
  input [1:0]mOutPtr;
  input [7:0]D;
  input ap_clk;

  wire AXIvideo2Mat_U0_img_data_stream_2_V_write;
  wire [7:0]D;
  wire [7:0]\SRL_SIG_reg[0] ;
  wire [7:0]\SRL_SIG_reg[0][7]_0 ;
  wire [7:0]\SRL_SIG_reg[1] ;
  wire ap_clk;
  wire ce;
  wire internal_full_n_reg;
  wire [1:0]mOutPtr;

  LUT4 #(
    .INIT(16'hFB08)) 
    \SRL_SIG[0][0]_i_1__2 
       (.I0(\SRL_SIG_reg[1] [0]),
        .I1(mOutPtr[0]),
        .I2(mOutPtr[1]),
        .I3(\SRL_SIG_reg[0] [0]),
        .O(\SRL_SIG_reg[0][7]_0 [0]));
  LUT4 #(
    .INIT(16'hFB08)) 
    \SRL_SIG[0][1]_i_1__2 
       (.I0(\SRL_SIG_reg[1] [1]),
        .I1(mOutPtr[0]),
        .I2(mOutPtr[1]),
        .I3(\SRL_SIG_reg[0] [1]),
        .O(\SRL_SIG_reg[0][7]_0 [1]));
  LUT4 #(
    .INIT(16'hFB08)) 
    \SRL_SIG[0][2]_i_1__2 
       (.I0(\SRL_SIG_reg[1] [2]),
        .I1(mOutPtr[0]),
        .I2(mOutPtr[1]),
        .I3(\SRL_SIG_reg[0] [2]),
        .O(\SRL_SIG_reg[0][7]_0 [2]));
  LUT4 #(
    .INIT(16'hFB08)) 
    \SRL_SIG[0][3]_i_1__2 
       (.I0(\SRL_SIG_reg[1] [3]),
        .I1(mOutPtr[0]),
        .I2(mOutPtr[1]),
        .I3(\SRL_SIG_reg[0] [3]),
        .O(\SRL_SIG_reg[0][7]_0 [3]));
  LUT4 #(
    .INIT(16'hFB08)) 
    \SRL_SIG[0][4]_i_1__2 
       (.I0(\SRL_SIG_reg[1] [4]),
        .I1(mOutPtr[0]),
        .I2(mOutPtr[1]),
        .I3(\SRL_SIG_reg[0] [4]),
        .O(\SRL_SIG_reg[0][7]_0 [4]));
  LUT4 #(
    .INIT(16'hFB08)) 
    \SRL_SIG[0][5]_i_1__2 
       (.I0(\SRL_SIG_reg[1] [5]),
        .I1(mOutPtr[0]),
        .I2(mOutPtr[1]),
        .I3(\SRL_SIG_reg[0] [5]),
        .O(\SRL_SIG_reg[0][7]_0 [5]));
  LUT4 #(
    .INIT(16'hFB08)) 
    \SRL_SIG[0][6]_i_1__2 
       (.I0(\SRL_SIG_reg[1] [6]),
        .I1(mOutPtr[0]),
        .I2(mOutPtr[1]),
        .I3(\SRL_SIG_reg[0] [6]),
        .O(\SRL_SIG_reg[0][7]_0 [6]));
  LUT2 #(
    .INIT(4'h8)) 
    \SRL_SIG[0][7]_i_1__1 
       (.I0(internal_full_n_reg),
        .I1(AXIvideo2Mat_U0_img_data_stream_2_V_write),
        .O(ce));
  LUT4 #(
    .INIT(16'hFB08)) 
    \SRL_SIG[0][7]_i_3 
       (.I0(\SRL_SIG_reg[1] [7]),
        .I1(mOutPtr[0]),
        .I2(mOutPtr[1]),
        .I3(\SRL_SIG_reg[0] [7]),
        .O(\SRL_SIG_reg[0][7]_0 [7]));
  FDRE \SRL_SIG_reg[0][0] 
       (.C(ap_clk),
        .CE(ce),
        .D(D[0]),
        .Q(\SRL_SIG_reg[0] [0]),
        .R(1'b0));
  FDRE \SRL_SIG_reg[0][1] 
       (.C(ap_clk),
        .CE(ce),
        .D(D[1]),
        .Q(\SRL_SIG_reg[0] [1]),
        .R(1'b0));
  FDRE \SRL_SIG_reg[0][2] 
       (.C(ap_clk),
        .CE(ce),
        .D(D[2]),
        .Q(\SRL_SIG_reg[0] [2]),
        .R(1'b0));
  FDRE \SRL_SIG_reg[0][3] 
       (.C(ap_clk),
        .CE(ce),
        .D(D[3]),
        .Q(\SRL_SIG_reg[0] [3]),
        .R(1'b0));
  FDRE \SRL_SIG_reg[0][4] 
       (.C(ap_clk),
        .CE(ce),
        .D(D[4]),
        .Q(\SRL_SIG_reg[0] [4]),
        .R(1'b0));
  FDRE \SRL_SIG_reg[0][5] 
       (.C(ap_clk),
        .CE(ce),
        .D(D[5]),
        .Q(\SRL_SIG_reg[0] [5]),
        .R(1'b0));
  FDRE \SRL_SIG_reg[0][6] 
       (.C(ap_clk),
        .CE(ce),
        .D(D[6]),
        .Q(\SRL_SIG_reg[0] [6]),
        .R(1'b0));
  FDRE \SRL_SIG_reg[0][7] 
       (.C(ap_clk),
        .CE(ce),
        .D(D[7]),
        .Q(\SRL_SIG_reg[0] [7]),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][0] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg[0] [0]),
        .Q(\SRL_SIG_reg[1] [0]),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][1] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg[0] [1]),
        .Q(\SRL_SIG_reg[1] [1]),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][2] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg[0] [2]),
        .Q(\SRL_SIG_reg[1] [2]),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][3] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg[0] [3]),
        .Q(\SRL_SIG_reg[1] [3]),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][4] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg[0] [4]),
        .Q(\SRL_SIG_reg[1] [4]),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][5] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg[0] [5]),
        .Q(\SRL_SIG_reg[1] [5]),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][6] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg[0] [6]),
        .Q(\SRL_SIG_reg[1] [6]),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][7] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg[0] [7]),
        .Q(\SRL_SIG_reg[1] [7]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "fifo_w8_d2_A_shiftReg" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_shiftReg_11
   (\SRL_SIG_reg[0][7]_0 ,
    internal_full_n_reg,
    AXIvideo2Mat_U0_img_data_stream_2_V_write,
    \mOutPtr_reg[0] ,
    \mOutPtr_reg[1] ,
    D,
    ap_clk);
  output [7:0]\SRL_SIG_reg[0][7]_0 ;
  input internal_full_n_reg;
  input AXIvideo2Mat_U0_img_data_stream_2_V_write;
  input \mOutPtr_reg[0] ;
  input \mOutPtr_reg[1] ;
  input [7:0]D;
  input ap_clk;

  wire AXIvideo2Mat_U0_img_data_stream_2_V_write;
  wire [7:0]D;
  wire [7:0]\SRL_SIG_reg[0][7]_0 ;
  wire \SRL_SIG_reg_n_0_[0][0] ;
  wire \SRL_SIG_reg_n_0_[0][1] ;
  wire \SRL_SIG_reg_n_0_[0][2] ;
  wire \SRL_SIG_reg_n_0_[0][3] ;
  wire \SRL_SIG_reg_n_0_[0][4] ;
  wire \SRL_SIG_reg_n_0_[0][5] ;
  wire \SRL_SIG_reg_n_0_[0][6] ;
  wire \SRL_SIG_reg_n_0_[0][7] ;
  wire \SRL_SIG_reg_n_0_[1][0] ;
  wire \SRL_SIG_reg_n_0_[1][1] ;
  wire \SRL_SIG_reg_n_0_[1][2] ;
  wire \SRL_SIG_reg_n_0_[1][3] ;
  wire \SRL_SIG_reg_n_0_[1][4] ;
  wire \SRL_SIG_reg_n_0_[1][5] ;
  wire \SRL_SIG_reg_n_0_[1][6] ;
  wire \SRL_SIG_reg_n_0_[1][7] ;
  wire ap_clk;
  wire ce;
  wire internal_full_n_reg;
  wire \mOutPtr_reg[0] ;
  wire \mOutPtr_reg[1] ;

  LUT4 #(
    .INIT(16'hFB08)) 
    \SRL_SIG[0][0]_i_1__4 
       (.I0(\SRL_SIG_reg_n_0_[1][0] ),
        .I1(\mOutPtr_reg[0] ),
        .I2(\mOutPtr_reg[1] ),
        .I3(\SRL_SIG_reg_n_0_[0][0] ),
        .O(\SRL_SIG_reg[0][7]_0 [0]));
  LUT4 #(
    .INIT(16'hFB08)) 
    \SRL_SIG[0][1]_i_1__4 
       (.I0(\SRL_SIG_reg_n_0_[1][1] ),
        .I1(\mOutPtr_reg[0] ),
        .I2(\mOutPtr_reg[1] ),
        .I3(\SRL_SIG_reg_n_0_[0][1] ),
        .O(\SRL_SIG_reg[0][7]_0 [1]));
  LUT4 #(
    .INIT(16'hFB08)) 
    \SRL_SIG[0][2]_i_1__4 
       (.I0(\SRL_SIG_reg_n_0_[1][2] ),
        .I1(\mOutPtr_reg[0] ),
        .I2(\mOutPtr_reg[1] ),
        .I3(\SRL_SIG_reg_n_0_[0][2] ),
        .O(\SRL_SIG_reg[0][7]_0 [2]));
  LUT4 #(
    .INIT(16'hFB08)) 
    \SRL_SIG[0][3]_i_1__4 
       (.I0(\SRL_SIG_reg_n_0_[1][3] ),
        .I1(\mOutPtr_reg[0] ),
        .I2(\mOutPtr_reg[1] ),
        .I3(\SRL_SIG_reg_n_0_[0][3] ),
        .O(\SRL_SIG_reg[0][7]_0 [3]));
  LUT4 #(
    .INIT(16'hFB08)) 
    \SRL_SIG[0][4]_i_1__4 
       (.I0(\SRL_SIG_reg_n_0_[1][4] ),
        .I1(\mOutPtr_reg[0] ),
        .I2(\mOutPtr_reg[1] ),
        .I3(\SRL_SIG_reg_n_0_[0][4] ),
        .O(\SRL_SIG_reg[0][7]_0 [4]));
  LUT4 #(
    .INIT(16'hFB08)) 
    \SRL_SIG[0][5]_i_1__4 
       (.I0(\SRL_SIG_reg_n_0_[1][5] ),
        .I1(\mOutPtr_reg[0] ),
        .I2(\mOutPtr_reg[1] ),
        .I3(\SRL_SIG_reg_n_0_[0][5] ),
        .O(\SRL_SIG_reg[0][7]_0 [5]));
  LUT4 #(
    .INIT(16'hFB08)) 
    \SRL_SIG[0][6]_i_1__4 
       (.I0(\SRL_SIG_reg_n_0_[1][6] ),
        .I1(\mOutPtr_reg[0] ),
        .I2(\mOutPtr_reg[1] ),
        .I3(\SRL_SIG_reg_n_0_[0][6] ),
        .O(\SRL_SIG_reg[0][7]_0 [6]));
  LUT2 #(
    .INIT(4'h8)) 
    \SRL_SIG[0][7]_i_1 
       (.I0(internal_full_n_reg),
        .I1(AXIvideo2Mat_U0_img_data_stream_2_V_write),
        .O(ce));
  LUT4 #(
    .INIT(16'hFB08)) 
    \SRL_SIG[0][7]_i_3__1 
       (.I0(\SRL_SIG_reg_n_0_[1][7] ),
        .I1(\mOutPtr_reg[0] ),
        .I2(\mOutPtr_reg[1] ),
        .I3(\SRL_SIG_reg_n_0_[0][7] ),
        .O(\SRL_SIG_reg[0][7]_0 [7]));
  FDRE \SRL_SIG_reg[0][0] 
       (.C(ap_clk),
        .CE(ce),
        .D(D[0]),
        .Q(\SRL_SIG_reg_n_0_[0][0] ),
        .R(1'b0));
  FDRE \SRL_SIG_reg[0][1] 
       (.C(ap_clk),
        .CE(ce),
        .D(D[1]),
        .Q(\SRL_SIG_reg_n_0_[0][1] ),
        .R(1'b0));
  FDRE \SRL_SIG_reg[0][2] 
       (.C(ap_clk),
        .CE(ce),
        .D(D[2]),
        .Q(\SRL_SIG_reg_n_0_[0][2] ),
        .R(1'b0));
  FDRE \SRL_SIG_reg[0][3] 
       (.C(ap_clk),
        .CE(ce),
        .D(D[3]),
        .Q(\SRL_SIG_reg_n_0_[0][3] ),
        .R(1'b0));
  FDRE \SRL_SIG_reg[0][4] 
       (.C(ap_clk),
        .CE(ce),
        .D(D[4]),
        .Q(\SRL_SIG_reg_n_0_[0][4] ),
        .R(1'b0));
  FDRE \SRL_SIG_reg[0][5] 
       (.C(ap_clk),
        .CE(ce),
        .D(D[5]),
        .Q(\SRL_SIG_reg_n_0_[0][5] ),
        .R(1'b0));
  FDRE \SRL_SIG_reg[0][6] 
       (.C(ap_clk),
        .CE(ce),
        .D(D[6]),
        .Q(\SRL_SIG_reg_n_0_[0][6] ),
        .R(1'b0));
  FDRE \SRL_SIG_reg[0][7] 
       (.C(ap_clk),
        .CE(ce),
        .D(D[7]),
        .Q(\SRL_SIG_reg_n_0_[0][7] ),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][0] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg_n_0_[0][0] ),
        .Q(\SRL_SIG_reg_n_0_[1][0] ),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][1] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg_n_0_[0][1] ),
        .Q(\SRL_SIG_reg_n_0_[1][1] ),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][2] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg_n_0_[0][2] ),
        .Q(\SRL_SIG_reg_n_0_[1][2] ),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][3] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg_n_0_[0][3] ),
        .Q(\SRL_SIG_reg_n_0_[1][3] ),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][4] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg_n_0_[0][4] ),
        .Q(\SRL_SIG_reg_n_0_[1][4] ),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][5] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg_n_0_[0][5] ),
        .Q(\SRL_SIG_reg_n_0_[1][5] ),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][6] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg_n_0_[0][6] ),
        .Q(\SRL_SIG_reg_n_0_[1][6] ),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][7] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg_n_0_[0][7] ),
        .Q(\SRL_SIG_reg_n_0_[1][7] ),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "fifo_w8_d2_A_shiftReg" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_shiftReg_12
   (\SRL_SIG_reg[0][7]_0 ,
    internal_full_n_reg,
    AXIvideo2Mat_U0_img_data_stream_2_V_write,
    \mOutPtr_reg[0] ,
    \mOutPtr_reg[1] ,
    D,
    ap_clk);
  output [7:0]\SRL_SIG_reg[0][7]_0 ;
  input internal_full_n_reg;
  input AXIvideo2Mat_U0_img_data_stream_2_V_write;
  input \mOutPtr_reg[0] ;
  input \mOutPtr_reg[1] ;
  input [7:0]D;
  input ap_clk;

  wire AXIvideo2Mat_U0_img_data_stream_2_V_write;
  wire [7:0]D;
  wire [7:0]\SRL_SIG_reg[0][7]_0 ;
  wire \SRL_SIG_reg_n_0_[0][0] ;
  wire \SRL_SIG_reg_n_0_[0][1] ;
  wire \SRL_SIG_reg_n_0_[0][2] ;
  wire \SRL_SIG_reg_n_0_[0][3] ;
  wire \SRL_SIG_reg_n_0_[0][4] ;
  wire \SRL_SIG_reg_n_0_[0][5] ;
  wire \SRL_SIG_reg_n_0_[0][6] ;
  wire \SRL_SIG_reg_n_0_[0][7] ;
  wire \SRL_SIG_reg_n_0_[1][0] ;
  wire \SRL_SIG_reg_n_0_[1][1] ;
  wire \SRL_SIG_reg_n_0_[1][2] ;
  wire \SRL_SIG_reg_n_0_[1][3] ;
  wire \SRL_SIG_reg_n_0_[1][4] ;
  wire \SRL_SIG_reg_n_0_[1][5] ;
  wire \SRL_SIG_reg_n_0_[1][6] ;
  wire \SRL_SIG_reg_n_0_[1][7] ;
  wire ap_clk;
  wire ce;
  wire internal_full_n_reg;
  wire \mOutPtr_reg[0] ;
  wire \mOutPtr_reg[1] ;

  LUT4 #(
    .INIT(16'hFB08)) 
    \SRL_SIG[0][0]_i_1__3 
       (.I0(\SRL_SIG_reg_n_0_[1][0] ),
        .I1(\mOutPtr_reg[0] ),
        .I2(\mOutPtr_reg[1] ),
        .I3(\SRL_SIG_reg_n_0_[0][0] ),
        .O(\SRL_SIG_reg[0][7]_0 [0]));
  LUT4 #(
    .INIT(16'hFB08)) 
    \SRL_SIG[0][1]_i_1__3 
       (.I0(\SRL_SIG_reg_n_0_[1][1] ),
        .I1(\mOutPtr_reg[0] ),
        .I2(\mOutPtr_reg[1] ),
        .I3(\SRL_SIG_reg_n_0_[0][1] ),
        .O(\SRL_SIG_reg[0][7]_0 [1]));
  LUT4 #(
    .INIT(16'hFB08)) 
    \SRL_SIG[0][2]_i_1__3 
       (.I0(\SRL_SIG_reg_n_0_[1][2] ),
        .I1(\mOutPtr_reg[0] ),
        .I2(\mOutPtr_reg[1] ),
        .I3(\SRL_SIG_reg_n_0_[0][2] ),
        .O(\SRL_SIG_reg[0][7]_0 [2]));
  LUT4 #(
    .INIT(16'hFB08)) 
    \SRL_SIG[0][3]_i_1__3 
       (.I0(\SRL_SIG_reg_n_0_[1][3] ),
        .I1(\mOutPtr_reg[0] ),
        .I2(\mOutPtr_reg[1] ),
        .I3(\SRL_SIG_reg_n_0_[0][3] ),
        .O(\SRL_SIG_reg[0][7]_0 [3]));
  LUT4 #(
    .INIT(16'hFB08)) 
    \SRL_SIG[0][4]_i_1__3 
       (.I0(\SRL_SIG_reg_n_0_[1][4] ),
        .I1(\mOutPtr_reg[0] ),
        .I2(\mOutPtr_reg[1] ),
        .I3(\SRL_SIG_reg_n_0_[0][4] ),
        .O(\SRL_SIG_reg[0][7]_0 [4]));
  LUT4 #(
    .INIT(16'hFB08)) 
    \SRL_SIG[0][5]_i_1__3 
       (.I0(\SRL_SIG_reg_n_0_[1][5] ),
        .I1(\mOutPtr_reg[0] ),
        .I2(\mOutPtr_reg[1] ),
        .I3(\SRL_SIG_reg_n_0_[0][5] ),
        .O(\SRL_SIG_reg[0][7]_0 [5]));
  LUT4 #(
    .INIT(16'hFB08)) 
    \SRL_SIG[0][6]_i_1__3 
       (.I0(\SRL_SIG_reg_n_0_[1][6] ),
        .I1(\mOutPtr_reg[0] ),
        .I2(\mOutPtr_reg[1] ),
        .I3(\SRL_SIG_reg_n_0_[0][6] ),
        .O(\SRL_SIG_reg[0][7]_0 [6]));
  LUT2 #(
    .INIT(4'h8)) 
    \SRL_SIG[0][7]_i_1__0 
       (.I0(internal_full_n_reg),
        .I1(AXIvideo2Mat_U0_img_data_stream_2_V_write),
        .O(ce));
  LUT4 #(
    .INIT(16'hFB08)) 
    \SRL_SIG[0][7]_i_3__0 
       (.I0(\SRL_SIG_reg_n_0_[1][7] ),
        .I1(\mOutPtr_reg[0] ),
        .I2(\mOutPtr_reg[1] ),
        .I3(\SRL_SIG_reg_n_0_[0][7] ),
        .O(\SRL_SIG_reg[0][7]_0 [7]));
  FDRE \SRL_SIG_reg[0][0] 
       (.C(ap_clk),
        .CE(ce),
        .D(D[0]),
        .Q(\SRL_SIG_reg_n_0_[0][0] ),
        .R(1'b0));
  FDRE \SRL_SIG_reg[0][1] 
       (.C(ap_clk),
        .CE(ce),
        .D(D[1]),
        .Q(\SRL_SIG_reg_n_0_[0][1] ),
        .R(1'b0));
  FDRE \SRL_SIG_reg[0][2] 
       (.C(ap_clk),
        .CE(ce),
        .D(D[2]),
        .Q(\SRL_SIG_reg_n_0_[0][2] ),
        .R(1'b0));
  FDRE \SRL_SIG_reg[0][3] 
       (.C(ap_clk),
        .CE(ce),
        .D(D[3]),
        .Q(\SRL_SIG_reg_n_0_[0][3] ),
        .R(1'b0));
  FDRE \SRL_SIG_reg[0][4] 
       (.C(ap_clk),
        .CE(ce),
        .D(D[4]),
        .Q(\SRL_SIG_reg_n_0_[0][4] ),
        .R(1'b0));
  FDRE \SRL_SIG_reg[0][5] 
       (.C(ap_clk),
        .CE(ce),
        .D(D[5]),
        .Q(\SRL_SIG_reg_n_0_[0][5] ),
        .R(1'b0));
  FDRE \SRL_SIG_reg[0][6] 
       (.C(ap_clk),
        .CE(ce),
        .D(D[6]),
        .Q(\SRL_SIG_reg_n_0_[0][6] ),
        .R(1'b0));
  FDRE \SRL_SIG_reg[0][7] 
       (.C(ap_clk),
        .CE(ce),
        .D(D[7]),
        .Q(\SRL_SIG_reg_n_0_[0][7] ),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][0] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg_n_0_[0][0] ),
        .Q(\SRL_SIG_reg_n_0_[1][0] ),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][1] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg_n_0_[0][1] ),
        .Q(\SRL_SIG_reg_n_0_[1][1] ),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][2] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg_n_0_[0][2] ),
        .Q(\SRL_SIG_reg_n_0_[1][2] ),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][3] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg_n_0_[0][3] ),
        .Q(\SRL_SIG_reg_n_0_[1][3] ),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][4] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg_n_0_[0][4] ),
        .Q(\SRL_SIG_reg_n_0_[1][4] ),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][5] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg_n_0_[0][5] ),
        .Q(\SRL_SIG_reg_n_0_[1][5] ),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][6] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg_n_0_[0][6] ),
        .Q(\SRL_SIG_reg_n_0_[1][6] ),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][7] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg_n_0_[0][7] ),
        .Q(\SRL_SIG_reg_n_0_[1][7] ),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "fifo_w8_d2_A_shiftReg" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_shiftReg_13
   (D,
    \mOutPtr_reg[0] ,
    \mOutPtr_reg[1] ,
    ce,
    \SRL_SIG_reg[1][7]_0 ,
    ap_clk);
  output [7:0]D;
  input \mOutPtr_reg[0] ;
  input \mOutPtr_reg[1] ;
  input ce;
  input [7:0]\SRL_SIG_reg[1][7]_0 ;
  input ap_clk;

  wire [7:0]D;
  wire [7:0]\SRL_SIG_reg[1][7]_0 ;
  wire \SRL_SIG_reg_n_0_[0][0] ;
  wire \SRL_SIG_reg_n_0_[0][1] ;
  wire \SRL_SIG_reg_n_0_[0][2] ;
  wire \SRL_SIG_reg_n_0_[0][3] ;
  wire \SRL_SIG_reg_n_0_[0][4] ;
  wire \SRL_SIG_reg_n_0_[0][5] ;
  wire \SRL_SIG_reg_n_0_[0][6] ;
  wire \SRL_SIG_reg_n_0_[0][7] ;
  wire \SRL_SIG_reg_n_0_[1][0] ;
  wire \SRL_SIG_reg_n_0_[1][1] ;
  wire \SRL_SIG_reg_n_0_[1][2] ;
  wire \SRL_SIG_reg_n_0_[1][3] ;
  wire \SRL_SIG_reg_n_0_[1][4] ;
  wire \SRL_SIG_reg_n_0_[1][5] ;
  wire \SRL_SIG_reg_n_0_[1][6] ;
  wire \SRL_SIG_reg_n_0_[1][7] ;
  wire ap_clk;
  wire ce;
  wire \mOutPtr_reg[0] ;
  wire \mOutPtr_reg[1] ;

  LUT4 #(
    .INIT(16'hFB08)) 
    \AXI_video_strm_V_data_V_1_payload_A[0]_i_1__0 
       (.I0(\SRL_SIG_reg_n_0_[1][0] ),
        .I1(\mOutPtr_reg[0] ),
        .I2(\mOutPtr_reg[1] ),
        .I3(\SRL_SIG_reg_n_0_[0][0] ),
        .O(D[0]));
  LUT4 #(
    .INIT(16'hFB08)) 
    \AXI_video_strm_V_data_V_1_payload_A[1]_i_1__0 
       (.I0(\SRL_SIG_reg_n_0_[1][1] ),
        .I1(\mOutPtr_reg[0] ),
        .I2(\mOutPtr_reg[1] ),
        .I3(\SRL_SIG_reg_n_0_[0][1] ),
        .O(D[1]));
  LUT4 #(
    .INIT(16'hFB08)) 
    \AXI_video_strm_V_data_V_1_payload_A[2]_i_1__0 
       (.I0(\SRL_SIG_reg_n_0_[1][2] ),
        .I1(\mOutPtr_reg[0] ),
        .I2(\mOutPtr_reg[1] ),
        .I3(\SRL_SIG_reg_n_0_[0][2] ),
        .O(D[2]));
  LUT4 #(
    .INIT(16'hFB08)) 
    \AXI_video_strm_V_data_V_1_payload_A[3]_i_1__0 
       (.I0(\SRL_SIG_reg_n_0_[1][3] ),
        .I1(\mOutPtr_reg[0] ),
        .I2(\mOutPtr_reg[1] ),
        .I3(\SRL_SIG_reg_n_0_[0][3] ),
        .O(D[3]));
  LUT4 #(
    .INIT(16'hFB08)) 
    \AXI_video_strm_V_data_V_1_payload_A[4]_i_1__0 
       (.I0(\SRL_SIG_reg_n_0_[1][4] ),
        .I1(\mOutPtr_reg[0] ),
        .I2(\mOutPtr_reg[1] ),
        .I3(\SRL_SIG_reg_n_0_[0][4] ),
        .O(D[4]));
  LUT4 #(
    .INIT(16'hFB08)) 
    \AXI_video_strm_V_data_V_1_payload_A[5]_i_1__0 
       (.I0(\SRL_SIG_reg_n_0_[1][5] ),
        .I1(\mOutPtr_reg[0] ),
        .I2(\mOutPtr_reg[1] ),
        .I3(\SRL_SIG_reg_n_0_[0][5] ),
        .O(D[5]));
  LUT4 #(
    .INIT(16'hFB08)) 
    \AXI_video_strm_V_data_V_1_payload_A[6]_i_1__0 
       (.I0(\SRL_SIG_reg_n_0_[1][6] ),
        .I1(\mOutPtr_reg[0] ),
        .I2(\mOutPtr_reg[1] ),
        .I3(\SRL_SIG_reg_n_0_[0][6] ),
        .O(D[6]));
  LUT4 #(
    .INIT(16'hFB08)) 
    \AXI_video_strm_V_data_V_1_payload_A[7]_i_1__0 
       (.I0(\SRL_SIG_reg_n_0_[1][7] ),
        .I1(\mOutPtr_reg[0] ),
        .I2(\mOutPtr_reg[1] ),
        .I3(\SRL_SIG_reg_n_0_[0][7] ),
        .O(D[7]));
  FDRE \SRL_SIG_reg[0][0] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg[1][7]_0 [0]),
        .Q(\SRL_SIG_reg_n_0_[0][0] ),
        .R(1'b0));
  FDRE \SRL_SIG_reg[0][1] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg[1][7]_0 [1]),
        .Q(\SRL_SIG_reg_n_0_[0][1] ),
        .R(1'b0));
  FDRE \SRL_SIG_reg[0][2] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg[1][7]_0 [2]),
        .Q(\SRL_SIG_reg_n_0_[0][2] ),
        .R(1'b0));
  FDRE \SRL_SIG_reg[0][3] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg[1][7]_0 [3]),
        .Q(\SRL_SIG_reg_n_0_[0][3] ),
        .R(1'b0));
  FDRE \SRL_SIG_reg[0][4] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg[1][7]_0 [4]),
        .Q(\SRL_SIG_reg_n_0_[0][4] ),
        .R(1'b0));
  FDRE \SRL_SIG_reg[0][5] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg[1][7]_0 [5]),
        .Q(\SRL_SIG_reg_n_0_[0][5] ),
        .R(1'b0));
  FDRE \SRL_SIG_reg[0][6] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg[1][7]_0 [6]),
        .Q(\SRL_SIG_reg_n_0_[0][6] ),
        .R(1'b0));
  FDRE \SRL_SIG_reg[0][7] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg[1][7]_0 [7]),
        .Q(\SRL_SIG_reg_n_0_[0][7] ),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][0] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg_n_0_[0][0] ),
        .Q(\SRL_SIG_reg_n_0_[1][0] ),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][1] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg_n_0_[0][1] ),
        .Q(\SRL_SIG_reg_n_0_[1][1] ),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][2] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg_n_0_[0][2] ),
        .Q(\SRL_SIG_reg_n_0_[1][2] ),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][3] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg_n_0_[0][3] ),
        .Q(\SRL_SIG_reg_n_0_[1][3] ),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][4] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg_n_0_[0][4] ),
        .Q(\SRL_SIG_reg_n_0_[1][4] ),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][5] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg_n_0_[0][5] ),
        .Q(\SRL_SIG_reg_n_0_[1][5] ),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][6] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg_n_0_[0][6] ),
        .Q(\SRL_SIG_reg_n_0_[1][6] ),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][7] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg_n_0_[0][7] ),
        .Q(\SRL_SIG_reg_n_0_[1][7] ),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "fifo_w8_d2_A_shiftReg" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_shiftReg_14
   (D,
    \mOutPtr_reg[0] ,
    \mOutPtr_reg[1] ,
    ce,
    \SRL_SIG_reg[1][7]_0 ,
    ap_clk);
  output [7:0]D;
  input \mOutPtr_reg[0] ;
  input \mOutPtr_reg[1] ;
  input ce;
  input [7:0]\SRL_SIG_reg[1][7]_0 ;
  input ap_clk;

  wire [7:0]D;
  wire [7:0]\SRL_SIG_reg[1][7]_0 ;
  wire \SRL_SIG_reg_n_0_[0][0] ;
  wire \SRL_SIG_reg_n_0_[0][1] ;
  wire \SRL_SIG_reg_n_0_[0][2] ;
  wire \SRL_SIG_reg_n_0_[0][3] ;
  wire \SRL_SIG_reg_n_0_[0][4] ;
  wire \SRL_SIG_reg_n_0_[0][5] ;
  wire \SRL_SIG_reg_n_0_[0][6] ;
  wire \SRL_SIG_reg_n_0_[0][7] ;
  wire \SRL_SIG_reg_n_0_[1][0] ;
  wire \SRL_SIG_reg_n_0_[1][1] ;
  wire \SRL_SIG_reg_n_0_[1][2] ;
  wire \SRL_SIG_reg_n_0_[1][3] ;
  wire \SRL_SIG_reg_n_0_[1][4] ;
  wire \SRL_SIG_reg_n_0_[1][5] ;
  wire \SRL_SIG_reg_n_0_[1][6] ;
  wire \SRL_SIG_reg_n_0_[1][7] ;
  wire ap_clk;
  wire ce;
  wire \mOutPtr_reg[0] ;
  wire \mOutPtr_reg[1] ;

  LUT4 #(
    .INIT(16'hFB08)) 
    \AXI_video_strm_V_data_V_1_payload_A[16]_i_1__0 
       (.I0(\SRL_SIG_reg_n_0_[1][0] ),
        .I1(\mOutPtr_reg[0] ),
        .I2(\mOutPtr_reg[1] ),
        .I3(\SRL_SIG_reg_n_0_[0][0] ),
        .O(D[0]));
  LUT4 #(
    .INIT(16'hFB08)) 
    \AXI_video_strm_V_data_V_1_payload_A[17]_i_1__0 
       (.I0(\SRL_SIG_reg_n_0_[1][1] ),
        .I1(\mOutPtr_reg[0] ),
        .I2(\mOutPtr_reg[1] ),
        .I3(\SRL_SIG_reg_n_0_[0][1] ),
        .O(D[1]));
  LUT4 #(
    .INIT(16'hFB08)) 
    \AXI_video_strm_V_data_V_1_payload_A[18]_i_1__0 
       (.I0(\SRL_SIG_reg_n_0_[1][2] ),
        .I1(\mOutPtr_reg[0] ),
        .I2(\mOutPtr_reg[1] ),
        .I3(\SRL_SIG_reg_n_0_[0][2] ),
        .O(D[2]));
  LUT4 #(
    .INIT(16'hFB08)) 
    \AXI_video_strm_V_data_V_1_payload_A[19]_i_1__0 
       (.I0(\SRL_SIG_reg_n_0_[1][3] ),
        .I1(\mOutPtr_reg[0] ),
        .I2(\mOutPtr_reg[1] ),
        .I3(\SRL_SIG_reg_n_0_[0][3] ),
        .O(D[3]));
  LUT4 #(
    .INIT(16'hFB08)) 
    \AXI_video_strm_V_data_V_1_payload_A[20]_i_1__0 
       (.I0(\SRL_SIG_reg_n_0_[1][4] ),
        .I1(\mOutPtr_reg[0] ),
        .I2(\mOutPtr_reg[1] ),
        .I3(\SRL_SIG_reg_n_0_[0][4] ),
        .O(D[4]));
  LUT4 #(
    .INIT(16'hFB08)) 
    \AXI_video_strm_V_data_V_1_payload_A[21]_i_1__0 
       (.I0(\SRL_SIG_reg_n_0_[1][5] ),
        .I1(\mOutPtr_reg[0] ),
        .I2(\mOutPtr_reg[1] ),
        .I3(\SRL_SIG_reg_n_0_[0][5] ),
        .O(D[5]));
  LUT4 #(
    .INIT(16'hFB08)) 
    \AXI_video_strm_V_data_V_1_payload_A[22]_i_1__0 
       (.I0(\SRL_SIG_reg_n_0_[1][6] ),
        .I1(\mOutPtr_reg[0] ),
        .I2(\mOutPtr_reg[1] ),
        .I3(\SRL_SIG_reg_n_0_[0][6] ),
        .O(D[6]));
  LUT4 #(
    .INIT(16'hFB08)) 
    \AXI_video_strm_V_data_V_1_payload_A[23]_i_2__0 
       (.I0(\SRL_SIG_reg_n_0_[1][7] ),
        .I1(\mOutPtr_reg[0] ),
        .I2(\mOutPtr_reg[1] ),
        .I3(\SRL_SIG_reg_n_0_[0][7] ),
        .O(D[7]));
  FDRE \SRL_SIG_reg[0][0] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg[1][7]_0 [0]),
        .Q(\SRL_SIG_reg_n_0_[0][0] ),
        .R(1'b0));
  FDRE \SRL_SIG_reg[0][1] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg[1][7]_0 [1]),
        .Q(\SRL_SIG_reg_n_0_[0][1] ),
        .R(1'b0));
  FDRE \SRL_SIG_reg[0][2] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg[1][7]_0 [2]),
        .Q(\SRL_SIG_reg_n_0_[0][2] ),
        .R(1'b0));
  FDRE \SRL_SIG_reg[0][3] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg[1][7]_0 [3]),
        .Q(\SRL_SIG_reg_n_0_[0][3] ),
        .R(1'b0));
  FDRE \SRL_SIG_reg[0][4] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg[1][7]_0 [4]),
        .Q(\SRL_SIG_reg_n_0_[0][4] ),
        .R(1'b0));
  FDRE \SRL_SIG_reg[0][5] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg[1][7]_0 [5]),
        .Q(\SRL_SIG_reg_n_0_[0][5] ),
        .R(1'b0));
  FDRE \SRL_SIG_reg[0][6] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg[1][7]_0 [6]),
        .Q(\SRL_SIG_reg_n_0_[0][6] ),
        .R(1'b0));
  FDRE \SRL_SIG_reg[0][7] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg[1][7]_0 [7]),
        .Q(\SRL_SIG_reg_n_0_[0][7] ),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][0] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg_n_0_[0][0] ),
        .Q(\SRL_SIG_reg_n_0_[1][0] ),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][1] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg_n_0_[0][1] ),
        .Q(\SRL_SIG_reg_n_0_[1][1] ),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][2] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg_n_0_[0][2] ),
        .Q(\SRL_SIG_reg_n_0_[1][2] ),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][3] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg_n_0_[0][3] ),
        .Q(\SRL_SIG_reg_n_0_[1][3] ),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][4] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg_n_0_[0][4] ),
        .Q(\SRL_SIG_reg_n_0_[1][4] ),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][5] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg_n_0_[0][5] ),
        .Q(\SRL_SIG_reg_n_0_[1][5] ),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][6] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg_n_0_[0][6] ),
        .Q(\SRL_SIG_reg_n_0_[1][6] ),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][7] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg_n_0_[0][7] ),
        .Q(\SRL_SIG_reg_n_0_[1][7] ),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "fifo_w8_d2_A_shiftReg" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_shiftReg_15
   (D,
    \mOutPtr_reg[0] ,
    \mOutPtr_reg[1] ,
    ce,
    \SRL_SIG_reg[1][7]_0 ,
    ap_clk);
  output [7:0]D;
  input \mOutPtr_reg[0] ;
  input \mOutPtr_reg[1] ;
  input ce;
  input [7:0]\SRL_SIG_reg[1][7]_0 ;
  input ap_clk;

  wire [7:0]D;
  wire [7:0]\SRL_SIG_reg[1][7]_0 ;
  wire \SRL_SIG_reg_n_0_[0][0] ;
  wire \SRL_SIG_reg_n_0_[0][1] ;
  wire \SRL_SIG_reg_n_0_[0][2] ;
  wire \SRL_SIG_reg_n_0_[0][3] ;
  wire \SRL_SIG_reg_n_0_[0][4] ;
  wire \SRL_SIG_reg_n_0_[0][5] ;
  wire \SRL_SIG_reg_n_0_[0][6] ;
  wire \SRL_SIG_reg_n_0_[0][7] ;
  wire \SRL_SIG_reg_n_0_[1][0] ;
  wire \SRL_SIG_reg_n_0_[1][1] ;
  wire \SRL_SIG_reg_n_0_[1][2] ;
  wire \SRL_SIG_reg_n_0_[1][3] ;
  wire \SRL_SIG_reg_n_0_[1][4] ;
  wire \SRL_SIG_reg_n_0_[1][5] ;
  wire \SRL_SIG_reg_n_0_[1][6] ;
  wire \SRL_SIG_reg_n_0_[1][7] ;
  wire ap_clk;
  wire ce;
  wire \mOutPtr_reg[0] ;
  wire \mOutPtr_reg[1] ;

  LUT4 #(
    .INIT(16'hFB08)) 
    \AXI_video_strm_V_data_V_1_payload_A[10]_i_1__0 
       (.I0(\SRL_SIG_reg_n_0_[1][2] ),
        .I1(\mOutPtr_reg[0] ),
        .I2(\mOutPtr_reg[1] ),
        .I3(\SRL_SIG_reg_n_0_[0][2] ),
        .O(D[2]));
  LUT4 #(
    .INIT(16'hFB08)) 
    \AXI_video_strm_V_data_V_1_payload_A[11]_i_1__0 
       (.I0(\SRL_SIG_reg_n_0_[1][3] ),
        .I1(\mOutPtr_reg[0] ),
        .I2(\mOutPtr_reg[1] ),
        .I3(\SRL_SIG_reg_n_0_[0][3] ),
        .O(D[3]));
  LUT4 #(
    .INIT(16'hFB08)) 
    \AXI_video_strm_V_data_V_1_payload_A[12]_i_1__0 
       (.I0(\SRL_SIG_reg_n_0_[1][4] ),
        .I1(\mOutPtr_reg[0] ),
        .I2(\mOutPtr_reg[1] ),
        .I3(\SRL_SIG_reg_n_0_[0][4] ),
        .O(D[4]));
  LUT4 #(
    .INIT(16'hFB08)) 
    \AXI_video_strm_V_data_V_1_payload_A[13]_i_1__0 
       (.I0(\SRL_SIG_reg_n_0_[1][5] ),
        .I1(\mOutPtr_reg[0] ),
        .I2(\mOutPtr_reg[1] ),
        .I3(\SRL_SIG_reg_n_0_[0][5] ),
        .O(D[5]));
  LUT4 #(
    .INIT(16'hFB08)) 
    \AXI_video_strm_V_data_V_1_payload_A[14]_i_1__0 
       (.I0(\SRL_SIG_reg_n_0_[1][6] ),
        .I1(\mOutPtr_reg[0] ),
        .I2(\mOutPtr_reg[1] ),
        .I3(\SRL_SIG_reg_n_0_[0][6] ),
        .O(D[6]));
  LUT4 #(
    .INIT(16'hFB08)) 
    \AXI_video_strm_V_data_V_1_payload_A[15]_i_1__0 
       (.I0(\SRL_SIG_reg_n_0_[1][7] ),
        .I1(\mOutPtr_reg[0] ),
        .I2(\mOutPtr_reg[1] ),
        .I3(\SRL_SIG_reg_n_0_[0][7] ),
        .O(D[7]));
  LUT4 #(
    .INIT(16'hFB08)) 
    \AXI_video_strm_V_data_V_1_payload_A[8]_i_1__0 
       (.I0(\SRL_SIG_reg_n_0_[1][0] ),
        .I1(\mOutPtr_reg[0] ),
        .I2(\mOutPtr_reg[1] ),
        .I3(\SRL_SIG_reg_n_0_[0][0] ),
        .O(D[0]));
  LUT4 #(
    .INIT(16'hFB08)) 
    \AXI_video_strm_V_data_V_1_payload_A[9]_i_1__0 
       (.I0(\SRL_SIG_reg_n_0_[1][1] ),
        .I1(\mOutPtr_reg[0] ),
        .I2(\mOutPtr_reg[1] ),
        .I3(\SRL_SIG_reg_n_0_[0][1] ),
        .O(D[1]));
  FDRE \SRL_SIG_reg[0][0] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg[1][7]_0 [0]),
        .Q(\SRL_SIG_reg_n_0_[0][0] ),
        .R(1'b0));
  FDRE \SRL_SIG_reg[0][1] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg[1][7]_0 [1]),
        .Q(\SRL_SIG_reg_n_0_[0][1] ),
        .R(1'b0));
  FDRE \SRL_SIG_reg[0][2] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg[1][7]_0 [2]),
        .Q(\SRL_SIG_reg_n_0_[0][2] ),
        .R(1'b0));
  FDRE \SRL_SIG_reg[0][3] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg[1][7]_0 [3]),
        .Q(\SRL_SIG_reg_n_0_[0][3] ),
        .R(1'b0));
  FDRE \SRL_SIG_reg[0][4] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg[1][7]_0 [4]),
        .Q(\SRL_SIG_reg_n_0_[0][4] ),
        .R(1'b0));
  FDRE \SRL_SIG_reg[0][5] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg[1][7]_0 [5]),
        .Q(\SRL_SIG_reg_n_0_[0][5] ),
        .R(1'b0));
  FDRE \SRL_SIG_reg[0][6] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg[1][7]_0 [6]),
        .Q(\SRL_SIG_reg_n_0_[0][6] ),
        .R(1'b0));
  FDRE \SRL_SIG_reg[0][7] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg[1][7]_0 [7]),
        .Q(\SRL_SIG_reg_n_0_[0][7] ),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][0] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg_n_0_[0][0] ),
        .Q(\SRL_SIG_reg_n_0_[1][0] ),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][1] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg_n_0_[0][1] ),
        .Q(\SRL_SIG_reg_n_0_[1][1] ),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][2] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg_n_0_[0][2] ),
        .Q(\SRL_SIG_reg_n_0_[1][2] ),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][3] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg_n_0_[0][3] ),
        .Q(\SRL_SIG_reg_n_0_[1][3] ),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][4] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg_n_0_[0][4] ),
        .Q(\SRL_SIG_reg_n_0_[1][4] ),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][5] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg_n_0_[0][5] ),
        .Q(\SRL_SIG_reg_n_0_[1][5] ),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][6] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg_n_0_[0][6] ),
        .Q(\SRL_SIG_reg_n_0_[1][6] ),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][7] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg_n_0_[0][7] ),
        .Q(\SRL_SIG_reg_n_0_[1][7] ),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "fifo_w8_d2_A_shiftReg" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_shiftReg_8
   (D,
    \mOutPtr_reg[0] ,
    \mOutPtr_reg[1] ,
    internal_full_n_reg,
    ce,
    \SRL_SIG_reg[1][7]_0 ,
    ap_clk);
  output [7:0]D;
  input \mOutPtr_reg[0] ;
  input \mOutPtr_reg[1] ;
  input internal_full_n_reg;
  input ce;
  input [7:0]\SRL_SIG_reg[1][7]_0 ;
  input ap_clk;

  wire [7:0]D;
  wire [7:0]\SRL_SIG_reg[1][7]_0 ;
  wire \SRL_SIG_reg_n_0_[0][0] ;
  wire \SRL_SIG_reg_n_0_[0][1] ;
  wire \SRL_SIG_reg_n_0_[0][2] ;
  wire \SRL_SIG_reg_n_0_[0][3] ;
  wire \SRL_SIG_reg_n_0_[0][4] ;
  wire \SRL_SIG_reg_n_0_[0][5] ;
  wire \SRL_SIG_reg_n_0_[0][6] ;
  wire \SRL_SIG_reg_n_0_[0][7] ;
  wire \SRL_SIG_reg_n_0_[1][0] ;
  wire \SRL_SIG_reg_n_0_[1][1] ;
  wire \SRL_SIG_reg_n_0_[1][2] ;
  wire \SRL_SIG_reg_n_0_[1][3] ;
  wire \SRL_SIG_reg_n_0_[1][4] ;
  wire \SRL_SIG_reg_n_0_[1][5] ;
  wire \SRL_SIG_reg_n_0_[1][6] ;
  wire \SRL_SIG_reg_n_0_[1][7] ;
  wire ap_clk;
  wire ce;
  wire internal_full_n_reg;
  wire \mOutPtr_reg[0] ;
  wire \mOutPtr_reg[1] ;

  LUT4 #(
    .INIT(16'hFB08)) 
    \AXI_video_strm_V_data_V_1_payload_A[16]_i_1 
       (.I0(\SRL_SIG_reg_n_0_[1][0] ),
        .I1(\mOutPtr_reg[0] ),
        .I2(\mOutPtr_reg[1] ),
        .I3(\SRL_SIG_reg_n_0_[0][0] ),
        .O(D[0]));
  LUT4 #(
    .INIT(16'hFB08)) 
    \AXI_video_strm_V_data_V_1_payload_A[17]_i_1 
       (.I0(\SRL_SIG_reg_n_0_[1][1] ),
        .I1(\mOutPtr_reg[0] ),
        .I2(\mOutPtr_reg[1] ),
        .I3(\SRL_SIG_reg_n_0_[0][1] ),
        .O(D[1]));
  LUT4 #(
    .INIT(16'hFB08)) 
    \AXI_video_strm_V_data_V_1_payload_A[18]_i_1 
       (.I0(\SRL_SIG_reg_n_0_[1][2] ),
        .I1(\mOutPtr_reg[0] ),
        .I2(\mOutPtr_reg[1] ),
        .I3(\SRL_SIG_reg_n_0_[0][2] ),
        .O(D[2]));
  LUT4 #(
    .INIT(16'hFB08)) 
    \AXI_video_strm_V_data_V_1_payload_A[19]_i_1 
       (.I0(\SRL_SIG_reg_n_0_[1][3] ),
        .I1(\mOutPtr_reg[0] ),
        .I2(\mOutPtr_reg[1] ),
        .I3(\SRL_SIG_reg_n_0_[0][3] ),
        .O(D[3]));
  LUT4 #(
    .INIT(16'hFB08)) 
    \AXI_video_strm_V_data_V_1_payload_A[20]_i_1 
       (.I0(\SRL_SIG_reg_n_0_[1][4] ),
        .I1(\mOutPtr_reg[0] ),
        .I2(\mOutPtr_reg[1] ),
        .I3(\SRL_SIG_reg_n_0_[0][4] ),
        .O(D[4]));
  LUT4 #(
    .INIT(16'hFB08)) 
    \AXI_video_strm_V_data_V_1_payload_A[21]_i_1 
       (.I0(\SRL_SIG_reg_n_0_[1][5] ),
        .I1(\mOutPtr_reg[0] ),
        .I2(\mOutPtr_reg[1] ),
        .I3(\SRL_SIG_reg_n_0_[0][5] ),
        .O(D[5]));
  LUT4 #(
    .INIT(16'hFB08)) 
    \AXI_video_strm_V_data_V_1_payload_A[22]_i_1 
       (.I0(\SRL_SIG_reg_n_0_[1][6] ),
        .I1(\mOutPtr_reg[0] ),
        .I2(\mOutPtr_reg[1] ),
        .I3(\SRL_SIG_reg_n_0_[0][6] ),
        .O(D[6]));
  LUT4 #(
    .INIT(16'hFB08)) 
    \AXI_video_strm_V_data_V_1_payload_A[23]_i_2 
       (.I0(\SRL_SIG_reg_n_0_[1][7] ),
        .I1(\mOutPtr_reg[0] ),
        .I2(\mOutPtr_reg[1] ),
        .I3(\SRL_SIG_reg_n_0_[0][7] ),
        .O(D[7]));
  FDRE \SRL_SIG_reg[0][0] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg[1][7]_0 [0]),
        .Q(\SRL_SIG_reg_n_0_[0][0] ),
        .R(internal_full_n_reg));
  FDRE \SRL_SIG_reg[0][1] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg[1][7]_0 [1]),
        .Q(\SRL_SIG_reg_n_0_[0][1] ),
        .R(internal_full_n_reg));
  FDRE \SRL_SIG_reg[0][2] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg[1][7]_0 [2]),
        .Q(\SRL_SIG_reg_n_0_[0][2] ),
        .R(internal_full_n_reg));
  FDRE \SRL_SIG_reg[0][3] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg[1][7]_0 [3]),
        .Q(\SRL_SIG_reg_n_0_[0][3] ),
        .R(internal_full_n_reg));
  FDRE \SRL_SIG_reg[0][4] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg[1][7]_0 [4]),
        .Q(\SRL_SIG_reg_n_0_[0][4] ),
        .R(internal_full_n_reg));
  FDRE \SRL_SIG_reg[0][5] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg[1][7]_0 [5]),
        .Q(\SRL_SIG_reg_n_0_[0][5] ),
        .R(internal_full_n_reg));
  FDRE \SRL_SIG_reg[0][6] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg[1][7]_0 [6]),
        .Q(\SRL_SIG_reg_n_0_[0][6] ),
        .R(internal_full_n_reg));
  FDRE \SRL_SIG_reg[0][7] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg[1][7]_0 [7]),
        .Q(\SRL_SIG_reg_n_0_[0][7] ),
        .R(internal_full_n_reg));
  FDRE \SRL_SIG_reg[1][0] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg_n_0_[0][0] ),
        .Q(\SRL_SIG_reg_n_0_[1][0] ),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][1] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg_n_0_[0][1] ),
        .Q(\SRL_SIG_reg_n_0_[1][1] ),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][2] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg_n_0_[0][2] ),
        .Q(\SRL_SIG_reg_n_0_[1][2] ),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][3] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg_n_0_[0][3] ),
        .Q(\SRL_SIG_reg_n_0_[1][3] ),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][4] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg_n_0_[0][4] ),
        .Q(\SRL_SIG_reg_n_0_[1][4] ),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][5] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg_n_0_[0][5] ),
        .Q(\SRL_SIG_reg_n_0_[1][5] ),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][6] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg_n_0_[0][6] ),
        .Q(\SRL_SIG_reg_n_0_[1][6] ),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][7] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg_n_0_[0][7] ),
        .Q(\SRL_SIG_reg_n_0_[1][7] ),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "fifo_w8_d2_A_shiftReg" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d2_A_shiftReg_9
   (D,
    \mOutPtr_reg[0] ,
    \mOutPtr_reg[1] ,
    internal_full_n_reg,
    ce,
    \SRL_SIG_reg[1][7]_0 ,
    ap_clk);
  output [7:0]D;
  input \mOutPtr_reg[0] ;
  input \mOutPtr_reg[1] ;
  input internal_full_n_reg;
  input ce;
  input [7:0]\SRL_SIG_reg[1][7]_0 ;
  input ap_clk;

  wire [7:0]D;
  wire [7:0]\SRL_SIG_reg[1][7]_0 ;
  wire \SRL_SIG_reg_n_0_[0][0] ;
  wire \SRL_SIG_reg_n_0_[0][1] ;
  wire \SRL_SIG_reg_n_0_[0][2] ;
  wire \SRL_SIG_reg_n_0_[0][3] ;
  wire \SRL_SIG_reg_n_0_[0][4] ;
  wire \SRL_SIG_reg_n_0_[0][5] ;
  wire \SRL_SIG_reg_n_0_[0][6] ;
  wire \SRL_SIG_reg_n_0_[0][7] ;
  wire \SRL_SIG_reg_n_0_[1][0] ;
  wire \SRL_SIG_reg_n_0_[1][1] ;
  wire \SRL_SIG_reg_n_0_[1][2] ;
  wire \SRL_SIG_reg_n_0_[1][3] ;
  wire \SRL_SIG_reg_n_0_[1][4] ;
  wire \SRL_SIG_reg_n_0_[1][5] ;
  wire \SRL_SIG_reg_n_0_[1][6] ;
  wire \SRL_SIG_reg_n_0_[1][7] ;
  wire ap_clk;
  wire ce;
  wire internal_full_n_reg;
  wire \mOutPtr_reg[0] ;
  wire \mOutPtr_reg[1] ;

  LUT4 #(
    .INIT(16'hFB08)) 
    \AXI_video_strm_V_data_V_1_payload_A[10]_i_1 
       (.I0(\SRL_SIG_reg_n_0_[1][2] ),
        .I1(\mOutPtr_reg[0] ),
        .I2(\mOutPtr_reg[1] ),
        .I3(\SRL_SIG_reg_n_0_[0][2] ),
        .O(D[2]));
  LUT4 #(
    .INIT(16'hFB08)) 
    \AXI_video_strm_V_data_V_1_payload_A[11]_i_1 
       (.I0(\SRL_SIG_reg_n_0_[1][3] ),
        .I1(\mOutPtr_reg[0] ),
        .I2(\mOutPtr_reg[1] ),
        .I3(\SRL_SIG_reg_n_0_[0][3] ),
        .O(D[3]));
  LUT4 #(
    .INIT(16'hFB08)) 
    \AXI_video_strm_V_data_V_1_payload_A[12]_i_1 
       (.I0(\SRL_SIG_reg_n_0_[1][4] ),
        .I1(\mOutPtr_reg[0] ),
        .I2(\mOutPtr_reg[1] ),
        .I3(\SRL_SIG_reg_n_0_[0][4] ),
        .O(D[4]));
  LUT4 #(
    .INIT(16'hFB08)) 
    \AXI_video_strm_V_data_V_1_payload_A[13]_i_1 
       (.I0(\SRL_SIG_reg_n_0_[1][5] ),
        .I1(\mOutPtr_reg[0] ),
        .I2(\mOutPtr_reg[1] ),
        .I3(\SRL_SIG_reg_n_0_[0][5] ),
        .O(D[5]));
  LUT4 #(
    .INIT(16'hFB08)) 
    \AXI_video_strm_V_data_V_1_payload_A[14]_i_1 
       (.I0(\SRL_SIG_reg_n_0_[1][6] ),
        .I1(\mOutPtr_reg[0] ),
        .I2(\mOutPtr_reg[1] ),
        .I3(\SRL_SIG_reg_n_0_[0][6] ),
        .O(D[6]));
  LUT4 #(
    .INIT(16'hFB08)) 
    \AXI_video_strm_V_data_V_1_payload_A[15]_i_1 
       (.I0(\SRL_SIG_reg_n_0_[1][7] ),
        .I1(\mOutPtr_reg[0] ),
        .I2(\mOutPtr_reg[1] ),
        .I3(\SRL_SIG_reg_n_0_[0][7] ),
        .O(D[7]));
  LUT4 #(
    .INIT(16'hFB08)) 
    \AXI_video_strm_V_data_V_1_payload_A[8]_i_1 
       (.I0(\SRL_SIG_reg_n_0_[1][0] ),
        .I1(\mOutPtr_reg[0] ),
        .I2(\mOutPtr_reg[1] ),
        .I3(\SRL_SIG_reg_n_0_[0][0] ),
        .O(D[0]));
  LUT4 #(
    .INIT(16'hFB08)) 
    \AXI_video_strm_V_data_V_1_payload_A[9]_i_1 
       (.I0(\SRL_SIG_reg_n_0_[1][1] ),
        .I1(\mOutPtr_reg[0] ),
        .I2(\mOutPtr_reg[1] ),
        .I3(\SRL_SIG_reg_n_0_[0][1] ),
        .O(D[1]));
  FDRE \SRL_SIG_reg[0][0] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg[1][7]_0 [0]),
        .Q(\SRL_SIG_reg_n_0_[0][0] ),
        .R(internal_full_n_reg));
  FDRE \SRL_SIG_reg[0][1] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg[1][7]_0 [1]),
        .Q(\SRL_SIG_reg_n_0_[0][1] ),
        .R(internal_full_n_reg));
  FDRE \SRL_SIG_reg[0][2] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg[1][7]_0 [2]),
        .Q(\SRL_SIG_reg_n_0_[0][2] ),
        .R(internal_full_n_reg));
  FDRE \SRL_SIG_reg[0][3] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg[1][7]_0 [3]),
        .Q(\SRL_SIG_reg_n_0_[0][3] ),
        .R(internal_full_n_reg));
  FDRE \SRL_SIG_reg[0][4] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg[1][7]_0 [4]),
        .Q(\SRL_SIG_reg_n_0_[0][4] ),
        .R(internal_full_n_reg));
  FDRE \SRL_SIG_reg[0][5] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg[1][7]_0 [5]),
        .Q(\SRL_SIG_reg_n_0_[0][5] ),
        .R(internal_full_n_reg));
  FDRE \SRL_SIG_reg[0][6] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg[1][7]_0 [6]),
        .Q(\SRL_SIG_reg_n_0_[0][6] ),
        .R(internal_full_n_reg));
  FDRE \SRL_SIG_reg[0][7] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg[1][7]_0 [7]),
        .Q(\SRL_SIG_reg_n_0_[0][7] ),
        .R(internal_full_n_reg));
  FDRE \SRL_SIG_reg[1][0] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg_n_0_[0][0] ),
        .Q(\SRL_SIG_reg_n_0_[1][0] ),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][1] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg_n_0_[0][1] ),
        .Q(\SRL_SIG_reg_n_0_[1][1] ),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][2] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg_n_0_[0][2] ),
        .Q(\SRL_SIG_reg_n_0_[1][2] ),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][3] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg_n_0_[0][3] ),
        .Q(\SRL_SIG_reg_n_0_[1][3] ),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][4] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg_n_0_[0][4] ),
        .Q(\SRL_SIG_reg_n_0_[1][4] ),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][5] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg_n_0_[0][5] ),
        .Q(\SRL_SIG_reg_n_0_[1][5] ),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][6] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg_n_0_[0][6] ),
        .Q(\SRL_SIG_reg_n_0_[1][6] ),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][7] 
       (.C(ap_clk),
        .CE(ce),
        .D(\SRL_SIG_reg_n_0_[0][7] ),
        .Q(\SRL_SIG_reg_n_0_[1][7] ),
        .R(1'b0));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_start_for_Loop_lobkb
   (start_for_Loop_loop_height_pro_U0_full_n,
    start_for_Loop_loop_height_pro_U0_empty_n,
    ap_idle,
    internal_full_n_reg_0,
    ap_clk,
    start_once_reg,
    ap_start,
    \ap_CS_fsm_reg[0] ,
    internal_full_n_reg_1,
    ap_rst_n,
    Loop_loop_height_pro_U0_ap_ready,
    start_once_reg_reg,
    start_once_reg_reg_0,
    start_for_Mat2AXIvideo_1_U0_full_n,
    start_for_Mat2AXIvideo_U0_full_n,
    ap_rst);
  output start_for_Loop_loop_height_pro_U0_full_n;
  output start_for_Loop_loop_height_pro_U0_empty_n;
  output ap_idle;
  output internal_full_n_reg_0;
  input ap_clk;
  input start_once_reg;
  input ap_start;
  input \ap_CS_fsm_reg[0] ;
  input internal_full_n_reg_1;
  input ap_rst_n;
  input Loop_loop_height_pro_U0_ap_ready;
  input start_once_reg_reg;
  input start_once_reg_reg_0;
  input start_for_Mat2AXIvideo_1_U0_full_n;
  input start_for_Mat2AXIvideo_U0_full_n;
  input ap_rst;

  wire Loop_loop_height_pro_U0_ap_ready;
  wire \ap_CS_fsm_reg[0] ;
  wire ap_clk;
  wire ap_idle;
  wire ap_rst;
  wire ap_rst_n;
  wire ap_start;
  wire internal_empty_n_i_1_n_0;
  wire internal_full_n_i_1_n_0;
  wire internal_full_n_i_2__10_n_0;
  wire internal_full_n_reg_0;
  wire internal_full_n_reg_1;
  wire \mOutPtr[0]_i_1_n_0 ;
  wire \mOutPtr[1]_i_1_n_0 ;
  wire \mOutPtr_reg_n_0_[0] ;
  wire \mOutPtr_reg_n_0_[1] ;
  wire start_for_Loop_loop_height_pro_U0_empty_n;
  wire start_for_Loop_loop_height_pro_U0_full_n;
  wire start_for_Mat2AXIvideo_1_U0_full_n;
  wire start_for_Mat2AXIvideo_U0_full_n;
  wire start_once_reg;
  wire start_once_reg_reg;
  wire start_once_reg_reg_0;

  LUT5 #(
    .INIT(32'h00001F00)) 
    ap_idle_INST_0
       (.I0(start_for_Loop_loop_height_pro_U0_full_n),
        .I1(start_once_reg),
        .I2(ap_start),
        .I3(\ap_CS_fsm_reg[0] ),
        .I4(internal_full_n_reg_1),
        .O(ap_idle));
  LUT6 #(
    .INIT(64'hA8AA0000AAAAAAAA)) 
    internal_empty_n_i_1
       (.I0(ap_rst_n),
        .I1(\mOutPtr_reg_n_0_[1] ),
        .I2(\mOutPtr_reg_n_0_[0] ),
        .I3(Loop_loop_height_pro_U0_ap_ready),
        .I4(start_for_Loop_loop_height_pro_U0_empty_n),
        .I5(start_once_reg_reg),
        .O(internal_empty_n_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    internal_empty_n_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(internal_empty_n_i_1_n_0),
        .Q(start_for_Loop_loop_height_pro_U0_empty_n),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hFFDDDDDDDD5D5D5D)) 
    internal_full_n_i_1
       (.I0(ap_rst_n),
        .I1(start_for_Loop_loop_height_pro_U0_full_n),
        .I2(internal_full_n_i_2__10_n_0),
        .I3(Loop_loop_height_pro_U0_ap_ready),
        .I4(start_for_Loop_loop_height_pro_U0_empty_n),
        .I5(start_once_reg_reg),
        .O(internal_full_n_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair150" *) 
  LUT2 #(
    .INIT(4'h1)) 
    internal_full_n_i_2__10
       (.I0(\mOutPtr_reg_n_0_[0] ),
        .I1(\mOutPtr_reg_n_0_[1] ),
        .O(internal_full_n_i_2__10_n_0));
  FDRE #(
    .INIT(1'b1)) 
    internal_full_n_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(internal_full_n_i_1_n_0),
        .Q(start_for_Loop_loop_height_pro_U0_full_n),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h7787777788788888)) 
    \mOutPtr[0]_i_1 
       (.I0(start_for_Loop_loop_height_pro_U0_empty_n),
        .I1(Loop_loop_height_pro_U0_ap_ready),
        .I2(start_for_Loop_loop_height_pro_U0_full_n),
        .I3(start_once_reg),
        .I4(ap_start),
        .I5(\mOutPtr_reg_n_0_[0] ),
        .O(\mOutPtr[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair150" *) 
  LUT5 #(
    .INIT(32'hBDDD4222)) 
    \mOutPtr[1]_i_1 
       (.I0(\mOutPtr_reg_n_0_[0] ),
        .I1(start_once_reg_reg),
        .I2(Loop_loop_height_pro_U0_ap_ready),
        .I3(start_for_Loop_loop_height_pro_U0_empty_n),
        .I4(\mOutPtr_reg_n_0_[1] ),
        .O(\mOutPtr[1]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hDFFF)) 
    \mOutPtr[1]_i_2__0 
       (.I0(start_for_Loop_loop_height_pro_U0_empty_n),
        .I1(start_once_reg_reg_0),
        .I2(start_for_Mat2AXIvideo_1_U0_full_n),
        .I3(start_for_Mat2AXIvideo_U0_full_n),
        .O(internal_full_n_reg_0));
  FDSE #(
    .INIT(1'b1)) 
    \mOutPtr_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\mOutPtr[0]_i_1_n_0 ),
        .Q(\mOutPtr_reg_n_0_[0] ),
        .S(ap_rst));
  FDSE #(
    .INIT(1'b1)) 
    \mOutPtr_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\mOutPtr[1]_i_1_n_0 ),
        .Q(\mOutPtr_reg_n_0_[1] ),
        .S(ap_rst));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_start_for_Mat2AXIcud
   (start_for_Mat2AXIvideo_U0_full_n,
    start_for_Mat2AXIvideo_U0_empty_n,
    \mOutPtr_reg[0]_0 ,
    ap_clk,
    ap_rst_n,
    Mat2AXIvideo_U0_ap_ready,
    internal_empty_n_reg_0,
    start_for_Mat2AXIvideo_1_U0_full_n,
    start_once_reg_reg,
    start_for_Loop_loop_height_pro_U0_empty_n,
    ap_rst);
  output start_for_Mat2AXIvideo_U0_full_n;
  output start_for_Mat2AXIvideo_U0_empty_n;
  output \mOutPtr_reg[0]_0 ;
  input ap_clk;
  input ap_rst_n;
  input Mat2AXIvideo_U0_ap_ready;
  input internal_empty_n_reg_0;
  input start_for_Mat2AXIvideo_1_U0_full_n;
  input start_once_reg_reg;
  input start_for_Loop_loop_height_pro_U0_empty_n;
  input ap_rst;

  wire [1:0]A;
  wire Mat2AXIvideo_U0_ap_ready;
  wire ap_clk;
  wire ap_rst;
  wire ap_rst_n;
  wire internal_empty_n_i_1__4_n_0;
  wire internal_empty_n_reg_0;
  wire internal_full_n_i_1__4_n_0;
  wire internal_full_n_i_2__2_n_0;
  wire \mOutPtr[0]_i_1_n_0 ;
  wire \mOutPtr[1]_i_1_n_0 ;
  wire \mOutPtr_reg[0]_0 ;
  wire start_for_Loop_loop_height_pro_U0_empty_n;
  wire start_for_Mat2AXIvideo_1_U0_full_n;
  wire start_for_Mat2AXIvideo_U0_empty_n;
  wire start_for_Mat2AXIvideo_U0_full_n;
  wire start_once_reg_reg;

  LUT4 #(
    .INIT(16'hF800)) 
    ap_idle_INST_0_i_2
       (.I0(start_for_Mat2AXIvideo_U0_full_n),
        .I1(start_for_Mat2AXIvideo_1_U0_full_n),
        .I2(start_once_reg_reg),
        .I3(start_for_Loop_loop_height_pro_U0_empty_n),
        .O(\mOutPtr_reg[0]_0 ));
  LUT6 #(
    .INIT(64'hA8AA0000AAAAAAAA)) 
    internal_empty_n_i_1__4
       (.I0(ap_rst_n),
        .I1(A[1]),
        .I2(A[0]),
        .I3(Mat2AXIvideo_U0_ap_ready),
        .I4(start_for_Mat2AXIvideo_U0_empty_n),
        .I5(internal_empty_n_reg_0),
        .O(internal_empty_n_i_1__4_n_0));
  FDRE #(
    .INIT(1'b0)) 
    internal_empty_n_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(internal_empty_n_i_1__4_n_0),
        .Q(start_for_Mat2AXIvideo_U0_empty_n),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hFFDDDDDDDD5D5D5D)) 
    internal_full_n_i_1__4
       (.I0(ap_rst_n),
        .I1(start_for_Mat2AXIvideo_U0_full_n),
        .I2(internal_full_n_i_2__2_n_0),
        .I3(Mat2AXIvideo_U0_ap_ready),
        .I4(start_for_Mat2AXIvideo_U0_empty_n),
        .I5(internal_empty_n_reg_0),
        .O(internal_full_n_i_1__4_n_0));
  (* SOFT_HLUTNM = "soft_lutpair151" *) 
  LUT2 #(
    .INIT(4'h1)) 
    internal_full_n_i_2__2
       (.I0(A[0]),
        .I1(A[1]),
        .O(internal_full_n_i_2__2_n_0));
  FDRE #(
    .INIT(1'b1)) 
    internal_full_n_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(internal_full_n_i_1__4_n_0),
        .Q(start_for_Mat2AXIvideo_U0_full_n),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h7787777788788888)) 
    \mOutPtr[0]_i_1 
       (.I0(start_for_Mat2AXIvideo_U0_empty_n),
        .I1(Mat2AXIvideo_U0_ap_ready),
        .I2(\mOutPtr_reg[0]_0 ),
        .I3(start_once_reg_reg),
        .I4(start_for_Mat2AXIvideo_U0_full_n),
        .I5(A[0]),
        .O(\mOutPtr[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair151" *) 
  LUT5 #(
    .INIT(32'hBDDD4222)) 
    \mOutPtr[1]_i_1 
       (.I0(A[0]),
        .I1(internal_empty_n_reg_0),
        .I2(Mat2AXIvideo_U0_ap_ready),
        .I3(start_for_Mat2AXIvideo_U0_empty_n),
        .I4(A[1]),
        .O(\mOutPtr[1]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \mOutPtr_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\mOutPtr[0]_i_1_n_0 ),
        .Q(A[0]),
        .S(ap_rst));
  FDSE #(
    .INIT(1'b1)) 
    \mOutPtr_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\mOutPtr[1]_i_1_n_0 ),
        .Q(A[1]),
        .S(ap_rst));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_start_for_Mat2AXIdEe
   (start_for_Mat2AXIvideo_1_U0_full_n,
    start_for_Mat2AXIvideo_1_U0_empty_n,
    ap_clk,
    ap_rst_n,
    Mat2AXIvideo_1_U0_ap_ready,
    internal_empty_n_reg_0,
    internal_full_n_reg_0,
    start_once_reg_reg,
    ap_rst);
  output start_for_Mat2AXIvideo_1_U0_full_n;
  output start_for_Mat2AXIvideo_1_U0_empty_n;
  input ap_clk;
  input ap_rst_n;
  input Mat2AXIvideo_1_U0_ap_ready;
  input internal_empty_n_reg_0;
  input internal_full_n_reg_0;
  input start_once_reg_reg;
  input ap_rst;

  wire Mat2AXIvideo_1_U0_ap_ready;
  wire ap_clk;
  wire ap_rst;
  wire ap_rst_n;
  wire internal_empty_n_i_1__3_n_0;
  wire internal_empty_n_reg_0;
  wire internal_full_n_i_1__3_n_0;
  wire internal_full_n_i_2__3_n_0;
  wire internal_full_n_reg_0;
  wire \mOutPtr[0]_i_1_n_0 ;
  wire \mOutPtr[1]_i_1_n_0 ;
  wire \mOutPtr_reg_n_0_[0] ;
  wire \mOutPtr_reg_n_0_[1] ;
  wire start_for_Mat2AXIvideo_1_U0_empty_n;
  wire start_for_Mat2AXIvideo_1_U0_full_n;
  wire start_once_reg_reg;

  LUT6 #(
    .INIT(64'hA800AA00AAAAAAAA)) 
    internal_empty_n_i_1__3
       (.I0(ap_rst_n),
        .I1(\mOutPtr_reg_n_0_[1] ),
        .I2(\mOutPtr_reg_n_0_[0] ),
        .I3(start_for_Mat2AXIvideo_1_U0_empty_n),
        .I4(Mat2AXIvideo_1_U0_ap_ready),
        .I5(internal_empty_n_reg_0),
        .O(internal_empty_n_i_1__3_n_0));
  FDRE #(
    .INIT(1'b0)) 
    internal_empty_n_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(internal_empty_n_i_1__3_n_0),
        .Q(start_for_Mat2AXIvideo_1_U0_empty_n),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hFFDDDDDDDD5D5D5D)) 
    internal_full_n_i_1__3
       (.I0(ap_rst_n),
        .I1(start_for_Mat2AXIvideo_1_U0_full_n),
        .I2(internal_full_n_i_2__3_n_0),
        .I3(start_for_Mat2AXIvideo_1_U0_empty_n),
        .I4(Mat2AXIvideo_1_U0_ap_ready),
        .I5(internal_empty_n_reg_0),
        .O(internal_full_n_i_1__3_n_0));
  (* SOFT_HLUTNM = "soft_lutpair152" *) 
  LUT2 #(
    .INIT(4'h1)) 
    internal_full_n_i_2__3
       (.I0(\mOutPtr_reg_n_0_[0] ),
        .I1(\mOutPtr_reg_n_0_[1] ),
        .O(internal_full_n_i_2__3_n_0));
  FDRE #(
    .INIT(1'b1)) 
    internal_full_n_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(internal_full_n_i_1__3_n_0),
        .Q(start_for_Mat2AXIvideo_1_U0_full_n),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h7787777788788888)) 
    \mOutPtr[0]_i_1 
       (.I0(Mat2AXIvideo_1_U0_ap_ready),
        .I1(start_for_Mat2AXIvideo_1_U0_empty_n),
        .I2(internal_full_n_reg_0),
        .I3(start_once_reg_reg),
        .I4(start_for_Mat2AXIvideo_1_U0_full_n),
        .I5(\mOutPtr_reg_n_0_[0] ),
        .O(\mOutPtr[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair152" *) 
  LUT5 #(
    .INIT(32'hBDDD4222)) 
    \mOutPtr[1]_i_1 
       (.I0(\mOutPtr_reg_n_0_[0] ),
        .I1(internal_empty_n_reg_0),
        .I2(start_for_Mat2AXIvideo_1_U0_empty_n),
        .I3(Mat2AXIvideo_1_U0_ap_ready),
        .I4(\mOutPtr_reg_n_0_[1] ),
        .O(\mOutPtr[1]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \mOutPtr_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\mOutPtr[0]_i_1_n_0 ),
        .Q(\mOutPtr_reg_n_0_[0] ),
        .S(ap_rst));
  FDSE #(
    .INIT(1'b1)) 
    \mOutPtr_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\mOutPtr[1]_i_1_n_0 ),
        .Q(\mOutPtr_reg_n_0_[1] ),
        .S(ap_rst));
endmodule

(* CHECK_LICENSE_TYPE = "system_center_image_0_0,center_image,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* ip_definition_source = "HLS" *) 
(* x_core_info = "center_image,Vivado 2018.2" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (image_in_TVALID,
    image_in_TREADY,
    image_in_TDATA,
    image_in_TKEEP,
    image_in_TSTRB,
    image_in_TUSER,
    image_in_TLAST,
    image_in_TID,
    image_in_TDEST,
    image_out_TVALID,
    image_out_TREADY,
    image_out_TDATA,
    image_out_TKEEP,
    image_out_TSTRB,
    image_out_TUSER,
    image_out_TLAST,
    image_out_TID,
    image_out_TDEST,
    crop_out_TVALID,
    crop_out_TREADY,
    crop_out_TDATA,
    crop_out_TKEEP,
    crop_out_TSTRB,
    crop_out_TUSER,
    crop_out_TLAST,
    crop_out_TID,
    crop_out_TDEST,
    ap_clk,
    ap_rst_n,
    ap_done,
    ap_start,
    ap_ready,
    ap_idle);
  (* x_interface_info = "xilinx.com:interface:axis:1.0 image_in TVALID" *) (* x_interface_parameter = "XIL_INTERFACENAME image_in, TDATA_NUM_BYTES 3, TDEST_WIDTH 1, TID_WIDTH 1, TUSER_WIDTH 1, LAYERED_METADATA undef, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 1, HAS_TLAST 1, FREQ_HZ 150000000, PHASE 0.0, CLK_DOMAIN system_clk_wiz_0_0_clk_out1" *) input image_in_TVALID;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 image_in TREADY" *) output image_in_TREADY;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 image_in TDATA" *) input [23:0]image_in_TDATA;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 image_in TKEEP" *) input [2:0]image_in_TKEEP;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 image_in TSTRB" *) input [2:0]image_in_TSTRB;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 image_in TUSER" *) input [0:0]image_in_TUSER;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 image_in TLAST" *) input [0:0]image_in_TLAST;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 image_in TID" *) input [0:0]image_in_TID;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 image_in TDEST" *) input [0:0]image_in_TDEST;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 image_out TVALID" *) (* x_interface_parameter = "XIL_INTERFACENAME image_out, TDATA_NUM_BYTES 3, TDEST_WIDTH 1, TID_WIDTH 1, TUSER_WIDTH 1, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {CLK {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} TDATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 24} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} integer {signed {attribs {resolve_type immediate dependency {} format bool minimum {} maximum {}} value false}}}} TDATA_WIDTH 24 TUSER {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} integer {signed {attribs {resolve_type immediate dependency {} format bool minimum {} maximum {}} value false}}}} TUSER_WIDTH 1}, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 1, HAS_TLAST 1, FREQ_HZ 150000000, PHASE 0.0, CLK_DOMAIN system_clk_wiz_0_0_clk_out1" *) output image_out_TVALID;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 image_out TREADY" *) input image_out_TREADY;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 image_out TDATA" *) output [23:0]image_out_TDATA;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 image_out TKEEP" *) output [2:0]image_out_TKEEP;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 image_out TSTRB" *) output [2:0]image_out_TSTRB;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 image_out TUSER" *) output [0:0]image_out_TUSER;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 image_out TLAST" *) output [0:0]image_out_TLAST;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 image_out TID" *) output [0:0]image_out_TID;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 image_out TDEST" *) output [0:0]image_out_TDEST;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 crop_out TVALID" *) (* x_interface_parameter = "XIL_INTERFACENAME crop_out, TDATA_NUM_BYTES 3, TDEST_WIDTH 1, TID_WIDTH 1, TUSER_WIDTH 1, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {CLK {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} TDATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 24} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} integer {signed {attribs {resolve_type immediate dependency {} format bool minimum {} maximum {}} value false}}}} TDATA_WIDTH 24 TUSER {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} integer {signed {attribs {resolve_type immediate dependency {} format bool minimum {} maximum {}} value false}}}} TUSER_WIDTH 1}, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 1, HAS_TLAST 1, FREQ_HZ 150000000, PHASE 0.0, CLK_DOMAIN system_clk_wiz_0_0_clk_out1" *) output crop_out_TVALID;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 crop_out TREADY" *) input crop_out_TREADY;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 crop_out TDATA" *) output [23:0]crop_out_TDATA;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 crop_out TKEEP" *) output [2:0]crop_out_TKEEP;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 crop_out TSTRB" *) output [2:0]crop_out_TSTRB;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 crop_out TUSER" *) output [0:0]crop_out_TUSER;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 crop_out TLAST" *) output [0:0]crop_out_TLAST;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 crop_out TID" *) output [0:0]crop_out_TID;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 crop_out TDEST" *) output [0:0]crop_out_TDEST;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 ap_clk CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME ap_clk, ASSOCIATED_BUSIF image_in:image_out:crop_out, ASSOCIATED_RESET ap_rst_n, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {CLK {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}}}, FREQ_HZ 150000000, PHASE 0.0, CLK_DOMAIN system_clk_wiz_0_0_clk_out1" *) input ap_clk;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 ap_rst_n RST" *) (* x_interface_parameter = "XIL_INTERFACENAME ap_rst_n, POLARITY ACTIVE_LOW, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {RST {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}}}" *) input ap_rst_n;
  (* x_interface_info = "xilinx.com:interface:acc_handshake:1.0 ap_ctrl done" *) (* x_interface_parameter = "XIL_INTERFACENAME ap_ctrl, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {done {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} start {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} ready {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} idle {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}}}" *) output ap_done;
  (* x_interface_info = "xilinx.com:interface:acc_handshake:1.0 ap_ctrl start" *) input ap_start;
  (* x_interface_info = "xilinx.com:interface:acc_handshake:1.0 ap_ctrl ready" *) output ap_ready;
  (* x_interface_info = "xilinx.com:interface:acc_handshake:1.0 ap_ctrl idle" *) output ap_idle;

  wire ap_clk;
  wire ap_done;
  wire ap_idle;
  wire ap_ready;
  wire ap_rst_n;
  wire ap_start;
  wire [23:0]crop_out_TDATA;
  wire [0:0]crop_out_TDEST;
  wire [0:0]crop_out_TID;
  wire [2:0]crop_out_TKEEP;
  wire [0:0]crop_out_TLAST;
  wire crop_out_TREADY;
  wire [2:0]crop_out_TSTRB;
  wire [0:0]crop_out_TUSER;
  wire crop_out_TVALID;
  wire [23:0]image_in_TDATA;
  wire [0:0]image_in_TDEST;
  wire [0:0]image_in_TID;
  wire [2:0]image_in_TKEEP;
  wire [0:0]image_in_TLAST;
  wire image_in_TREADY;
  wire [2:0]image_in_TSTRB;
  wire [0:0]image_in_TUSER;
  wire image_in_TVALID;
  wire [23:0]image_out_TDATA;
  wire [0:0]image_out_TDEST;
  wire [0:0]image_out_TID;
  wire [2:0]image_out_TKEEP;
  wire [0:0]image_out_TLAST;
  wire image_out_TREADY;
  wire [2:0]image_out_TSTRB;
  wire [0:0]image_out_TUSER;
  wire image_out_TVALID;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_center_image U0
       (.ap_clk(ap_clk),
        .ap_done(ap_done),
        .ap_idle(ap_idle),
        .ap_ready(ap_ready),
        .ap_rst_n(ap_rst_n),
        .ap_start(ap_start),
        .crop_out_TDATA(crop_out_TDATA),
        .crop_out_TDEST(crop_out_TDEST),
        .crop_out_TID(crop_out_TID),
        .crop_out_TKEEP(crop_out_TKEEP),
        .crop_out_TLAST(crop_out_TLAST),
        .crop_out_TREADY(crop_out_TREADY),
        .crop_out_TSTRB(crop_out_TSTRB),
        .crop_out_TUSER(crop_out_TUSER),
        .crop_out_TVALID(crop_out_TVALID),
        .image_in_TDATA(image_in_TDATA),
        .image_in_TDEST(image_in_TDEST),
        .image_in_TID(image_in_TID),
        .image_in_TKEEP(image_in_TKEEP),
        .image_in_TLAST(image_in_TLAST),
        .image_in_TREADY(image_in_TREADY),
        .image_in_TSTRB(image_in_TSTRB),
        .image_in_TUSER(image_in_TUSER),
        .image_in_TVALID(image_in_TVALID),
        .image_out_TDATA(image_out_TDATA),
        .image_out_TDEST(image_out_TDEST),
        .image_out_TID(image_out_TID),
        .image_out_TKEEP(image_out_TKEEP),
        .image_out_TLAST(image_out_TLAST),
        .image_out_TREADY(image_out_TREADY),
        .image_out_TSTRB(image_out_TSTRB),
        .image_out_TUSER(image_out_TUSER),
        .image_out_TVALID(image_out_TVALID));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
