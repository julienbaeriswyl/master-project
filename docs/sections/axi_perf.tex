\section{Performance measurements}

This section covers the measurements done on the performance of both IP and interfaces produced by Vivado HLS.

We need to measure interface throughput and IP latency beforehand so that we don't choose hardware that might be too
efficient (thus too expensive) for an embedded system.

\subsection{IP latency}

Latency is a time interval between the stimulation and the response, \textit{i.e.} the time it takes to produce a result
from a given input. Since IPs are driven by an input clock, the latency is expressed in clock cycles.

After synthesizing an IP with Vivado HLS, the program outputs an estimated latency. For our measurements, we decided to
test two IPs.

\begin{minipage}{0.49\linewidth}
    \centering
    \begin{tabular}{|r|r|}
        \hline \textbf{min} & \textbf{max} \\
        \hline 0 & 1 \\
        \hline
    \end{tabular}
    \captionof{table}{Estimated latencies for IP A.}
\end{minipage}
\hfill
\begin{minipage}{0.49\linewidth}
    \centering
    \begin{tabular}{|c|c|}
        \hline \textbf{min} & \textbf{max} \\
        \hline 9'205'913 & 9'278'233 \\
        \hline
    \end{tabular}
    \captionof{table}{Estimated latencies for IP B.}
\end{minipage}

IP A is the simplest one: it takes an integer as input, increments it by 1 and stores the result in the output. \\
IP B is a bit more complicated: it runs inference for an hand-written digits classification \acrfull{nn}. \\
Code for both IPs is available in \autoref{appx:ip-a-code} and \autoref{appx:ip-b-code}.

Since the estimated latency is given as clock cycles, we have to convert it to time unit. \\
The design used for these measures operates at 100 MHz. Thus, we will base our calculations on this frequency. A
frequency of 100 MHz means that there are 100 million clock cycles per second. So, we have

\begin{equation}
    \label{eq:freq-to-sec}
    1 \text{ clock cycle} \equiv \frac{1}{10^8} \text{ seconds} \equiv 10 \text{ nanoseconds}
\end{equation}

According to equation \ref{eq:freq-to-sec}, we know that a clock cycle is 10 nanoseconds. We can now estimate the
latency as a time unit of both IPs by taking the mean of minimum and maximum estimated latency as clock cycles and
multiply it by 10 nanoseconds.

\begin{equation}
    \label{eq:latency-a}
    \frac{(0 + 1)}{2} \cdot \frac{1}{10^8} \equiv 0.5 \cdot 10^{-8} \equiv 5 \text{ nanoseconds}
\end{equation}

\begin{equation}
    \label{eq:latency-b}
    \frac{(9'205'913 + 9'278'233)}{2} \cdot \frac{1}{10^8} \equiv 9'242'073 \cdot 10^{-8} \equiv 92.4 \text{ milliseconds}
\end{equation}

Equation \ref{eq:latency-a} shows the estimated time latency for IP A and equation \ref{eq:latency-b} is for IP B. Now
that we have a theoretical time latency, we must see how the IPs behave in real conditions. \\
To do so, we created a simple design where the IP is driven by a Zynq pre-processing system and a bare-metal program
that feeds the IP with input data, starts a timer and an IP at the same time and stops the timer when the IP is done
computing. The timer then outputs the time interval.

\begin{minipage}{0.49\linewidth}
    \centering
    \begin{tabular}{|r|r|}
        \hline \textbf{Measured latency} & \textbf{\% of theoretical} \\
        \hline 790 ns & 15'800 \\
        \hline
    \end{tabular}
    \captionof{table}{Measured latency for IP A.}
    \label{table:latency-soft-a}
\end{minipage}
\begin{minipage}{0.49\linewidth}
    \centering
    \begin{tabular}{|r|r|}
        \hline \textbf{Measured latency} & \textbf{\% of theoretical} \\
        \hline 93 ms & 100.5 \\
        \hline
    \end{tabular}
    \captionof{table}{Measured latency for IP B.}
    \label{table:latency-soft-b}
\end{minipage}

The results presented in tables \ref{table:latency-soft-a} and \ref{table:latency-soft-b} show different results. The
measured latency for IP B is the same as the theoretical. However, the measured latency for IP B is 15'800\% of the
theoretical one. This is due to the fact that the timer is software-sided and its activation/deactivation take several
clock cycles to be made.

The conclusion of the IP latency measurements is that the estimated latency issued by Vivado HLS is good above a certain
threshold but might be too small to measure below.

\subsection{AXI interfaces throughput}

The system can also be limited through interfaces throughput and the protocols they use.
The IPs we develop are driven by AXI protocols. These protocols can either be AXI4 or AXI4-Lite.

To measure the AXI interfaces throughput, we used the AXI DMA 7.1 IP \cite{pg021}. According to this documentation,
the AXI interfaces theoretical throughputs are given by the following equation:
$\text{frequency} \cdot \text{data width}$. As said earlier, our system is clocked at 100 MHz.

Primary high-speed DMA data movement between system memory and stream target is through the AXI Read Master to AXI
memory-mapped to stream (MM2S) Master, and AXI stream to memory-mapped (S2MM) Slave to AXI Write Master. Thus, it is
important to measure both channels.

The MM2S throughput is measured between the first \texttt{arvalid} on Memory Map side to the \texttt{tlast} on streaming
side. The S2MM throughput is measured between the first \texttt{tvalid} on streaming side to last \texttt{wlast} on the
memory map side. Both throughputs have been measures by transferring 10'000 bytes.

\begin{center}
\begin{tabular}{|l|r|r|}
    \hline
    \textbf{Interface}  & \textbf{MM2S Throughput (\% of Theoretical)} & \textbf{S2MM Throughput (\% of Theoretical)} \\
    \hline
    \textbf{AXI4}       & 382.6 MB/s (95.65\%)  & 352.1 MB/s (88.03\%) \\
    \hline
    \textbf{AXI4-Lite}  &                       & \\
    \hline
\end{tabular}
\captionof{table}{AXI4 \& AXI4-Lite interfaces throughput.}
\label{table:axi-throughput}
\end{center}

\todo{Add AXI4-Lite values}

We see that there is a small difference with the theoretical throughputs presented by Xilinx.