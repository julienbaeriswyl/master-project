\cleardoublepage
\section{Performance results}
\label{sec:results}

This section presents and compares the results obtained from several sources, as mentioned previously. As a reminder,
here is the list of the different sources:
\begin{itemize}
    \item   A solution running on a \acrshort{cpu}
    \item   A solution developed by a -- human -- programmer, with a \acrshort{hdl}
    \item   A solution generated with the framework
    \item   The same solution as above, but optimized with code transformations manually applied after generation
\end{itemize}

In the following section, the term "hardware latency" refers to the latency in clock cycles while the term "time
latency" refers to the minimum latency in seconds, which is calculated by multiplying the hardware latency by the
maximum frequency.


\subsection{Inference running on a CPU}

The first source of results was obtained by simply running the model inference on a \acrshort{cpu}. The code was run on
the Zybo Z7-20 processor, which is a dual-core ARM Cortex-A9 processor operating at 667 MHz \cite{zybo-rm},
\cite{arm-cortex-a9}.

To perform the inference, we used the TensorFlow Lite framework \cite{tflite}. It is designed to run TensorFlow models
on mobile, embedded and \acrfull{iot} devices. It allows on-device \acrlong{ml} inference with low latency and a small
binary size. Among the proposed APIs by TensorFlow Lite, we used the C++ API \cite{tflite-cpp-api} to run the
inference. \\
The corresponding code is available in \autoref{appx:tflite}.


\subsubsection{Accuracy}

The accuracy measured for the Keras model implemented with Python (and detailed in \autoref{sec:workflow-training}) is
\textbf{97.56\%}.


\subsubsection{Resources utilization}

The only resource we could measure for the \acrshort{cpu}-running solution was the memory utilization during inference
execution. The memory profiling was performed with the Valgrind Massif tool \cite{massif}. \\
The \autoref{fig:results-massif} shows the memory usage over the execution of the inference. During execution, memory
usage peaks at \textbf{313.3 KiB}.

\begin{minipage}{\linewidth}
    \centering
    \includegraphics[width=0.8\linewidth]{figures/results-massif.png}
    \captionof{figure}{Inference memory usage over time.}
    \label{fig:results-massif}
\end{minipage}


\subsubsection{Latency}

To measure the inference execution time, we performed 10 inference runs and we calculated the average of these runs.
The calculated average inference execution time was \textbf{7.386 milliseconds}.

The memory usage is pretty low, as well as the execution time for a system like the Zybo Z7-20. These performances
allow to perform handwritten digits recognition up to 135 frames per second -- which is more than enough for a real-life
application -- while consuming low memory.


\subsection{Inference developed by a human developer}

The second set of results, the inference developed by a human developer, was taken from the paper written by
\citeauthor{solovyev-2019} \cite{solovyev-2019}.


\subsubsection{Accuracy}

The paper doesn't mention anything about the accuracy performed by the model. However, since we based our model and its
training on the one presented in the paper, we can safely suppose that it offers about the same accuracy as the Python
model we propose (97.56\%) in \autoref{sec:workflow-training}.


\subsubsection{Resources utilization}

About resources utilization, the \citetitle{solovyev-2019} paper doesn't talk extensively about it and, more
importantly, it uses another tool for compiling the inference SystemVerilog module. To be sure to have the same basis of
comparison, we recompiled it with Vivado, the tool used in this work. The target board, defining the available
resources, is the Zybo Z7-20. Its characteristics are detailed in \autoref{appx:zybo-z7}. \\

To obtain comparable results, we compiled the SystemVerilog module in four different ways, depending on the
number of bits used for weight's fixed-point data representation. \\
After compiling the module, we obtain the following resources utilization:

\begin{center}
\begin{tabular}{|l|r|r|r|r|r|}
\hline
    \multirow{2}{*}{\textbf{Resource}}   &   \multicolumn{4}{c|}{\textbf{Utilization}}  &   \multirow{2}{*}{\textbf{Available}}  \\
\cline{2-5}
    &   \multicolumn{1}{c|}{\textbf{32-bit}}    &   \multicolumn{1}{c|}{\textbf{16-bit}}    &   \multicolumn{1}{c|}{\textbf{8-bit}} &   \multicolumn{1}{c|}{\textbf{4-bit}} &   \\
\hline
    \textbf{LUT}    &   4'005 (7.53\%)  &   2'491 (4.68\%)  &   2'554 (4.80\%)  &   1'917 (3.60\%)  &   53'200  \\
    \textbf{FF}     &   2'799 (2.63\%)  &   1'562 (1.47\%)  &   1'010 (0.95\%)  &   706 (0.66\%)    &   106'400 \\
    \textbf{BRAM}   &   55 (19.64\%)    &   28 (10.00\%)    &   14 (5.00\%)     &   7 (2.50\%)      &   280     \\
    \textbf{DSP}    &   40 (18.18\%)    &   17 (7.73\%)     &   4 (1.82\%)      &   4 (1.82\%)      &   220     \\
\hline
\end{tabular}
    \captionof{table}{Resources utilization of the inference developed by a human developer.}
    \label{tab:solovyev-resources-conv1}
\end{center}

This resources utilization summary is valid for a sequential implementation of the model. However, human implementation
offers an interesting feature over automatically generated methods: parallelization. The \citetitle{solovyev-2019} paper
also presents two other implementations: one with 2 convolutional blocks parallelized and the other with 4 parallelized
convolutional blocks.

\begin{center}
\begin{tabular}{|l|r|r|r|r|r|}
\hline
    \multirow{2}{*}{\textbf{Resource}}   &   \multicolumn{4}{c|}{\textbf{Utilization}}  &   \multirow{2}{*}{\textbf{Available}}  \\
\cline{2-5}
    &   \multicolumn{1}{c|}{\textbf{32-bit}}    &   \multicolumn{1}{c|}{\textbf{16-bit}}    &   \multicolumn{1}{c|}{\textbf{8-bit}} &   \multicolumn{1}{c|}{\textbf{4-bit}} &   \\
\hline
    \textbf{LUT}        &   5'979 (11.24\%) &   3'286 (6.18\%)  &   3'565 (6.70\%)  &   2'383 (4.48\%)  &   53'200  \\
    \textbf{FF}         &   3'948 (3.71\%)  &   2'096 (1.97\%)  &   1'271 (1.19\%)  &   823 (0.77\%)    &   106'400 \\
    \textbf{BRAM}       &   68 (24.29\%)    &   35 (12.50\%)    &   18 (6.43\%)     &   9 (3.21\%)      &   280     \\
    \textbf{DSP}        &   74 (33.64\%)    &   28 (12.73\%)    &   2 (0.91\%)      &   2 (0.91\%)      &   220     \\
\hline
\end{tabular}
    \captionof{table}{Resources utilization of the inference developed by a human developer with 2 convolutional
    blocks.}
    \label{tab:solovyev-resources-conv2}
\end{center}

\begin{center}
\begin{tabular}{|l|r|r|r|r|r|}
\hline
    \multirow{2}{*}{\textbf{Resource}}   &   \multicolumn{4}{c|}{\textbf{Utilization}}  &   \multirow{2}{*}{\textbf{Available}}  \\
\cline{2-5}
    &   \multicolumn{1}{c|}{\textbf{32-bit}}    &   \multicolumn{1}{c|}{\textbf{16-bit}}    &   \multicolumn{1}{c|}{\textbf{8-bit}} &   \multicolumn{1}{c|}{\textbf{4-bit}} &   \\
\hline
    \textbf{LUT}        &   9'486 (17.83\%) &   4'811 (9.04\%)  &   5'627 (10.58\%) &   3'163 (5.95\%)  &   53'200  \\
    \textbf{FF}         &   6'267 (5.89\%)  &   3'198 (3.01\%)  &   1'852 (1.74\%)  &   1'112 (1.05\%)  &   106'400 \\
    \textbf{BRAM}       &   110 (39.29\%)   &   56 (20.00\%)    &   29 (10.36\%)    &   15 (5.36\%)     &   280     \\
    \textbf{DSP}        &   146 (66.36\%)   &   54 (24.55\%)    &   2 (0.91\%)      &   2 (0.91\%)      &   220     \\
\hline
\end{tabular}
    \captionof{table}{Resources utilization of the inference developed by a human developer with 4 convolutional
    blocks.}
    \label{tab:solovyev-resources-conv4}
\end{center}


\subsubsection{Timing}

Upon synthesis, the Vivado Design Suite outputs the timing estimates of the SystemVerilog module. We compiled every
fixed-point data representation for 1, 2 and 4 convolutional blocks. The number of parallelized blocks didn't change the
timing, thus \autoref{tab:solovyev-timings} only shows the timings for the different data representation.

\begin{center}
\begin{tabular}{|l|r|r|}
\hline
                    &   \textbf{Timing [ns]}    &   \textbf{Max freq. [MHz]}    \\
\hline
    \textbf{32-bit} &   16.293  &   61.376  \\
    \textbf{16-bit} &   15.303  &   65.347  \\
    \textbf{8-bit}  &   14.874  &   67.231  \\
    \textbf{4-bit}  &   14.896  &   67.132  \\
\hline
\end{tabular}
    \captionof{table}{Timings for inference developed by a human developer.}
    \label{tab:solovyev-timings}
\end{center}

We notice that the max frequency decreases with the number of bits used for data representation.


\subsubsection{Latency}

According to this same paper, the number of clock cycles required to predict a digit from an image
is \textbf{236'746}. \autoref{tab:solovyev-cycles} details the clock cycles needed for each stage of the inference.

\begin{center}
\begin{tabular}{|l|r|}
\hline
    \textbf{Stage}  &   \textbf{Number of clock cycles} \\
\hline
    Loading input image                                 &   1'570   \\
    1\textsuperscript{st} convolution: loading weights  &   76      \\
    1\textsuperscript{st} convolution: processing       &   12'605  \\
    2\textsuperscript{nd} convolution: loading weights  &   291     \\
    2\textsuperscript{nd} convolution: processing       &   50'416  \\
    1\textsuperscript{st} max pooling: processing       &   3'164   \\
    3\textsuperscript{rd} convolution: loading weights  &   580     \\
    3\textsuperscript{rd} convolution: processing       &   25'569  \\
    4\textsuperscript{th} convolution: loading weights  &   1'155   \\
    4\textsuperscript{th} convolution: processing       &   51'136  \\
    2\textsuperscript{nd} max pooling: processing       &   1'623   \\
    5\textsuperscript{th} convolution: loading weights  &   2'309   \\
    5\textsuperscript{th} convolution: processing       &   27'009  \\
    6\textsuperscript{th} convolution: loading weights  &   4'611   \\
    6\textsuperscript{th} convolution: processing \& global max pooling &   54'016  \\
    Dense layer: loading weights                        &   356     \\
    Dense layer: processing                             &   244     \\
    Saving result                                       &   16      \\
\hline
    \textbf{Total}                                      &   \textbf{236'746}    \\
\hline
\end{tabular}
    \captionof{table}{Clock cycles at each stage of FPGA-based image processing \cite{solovyev-2019}.}
    \label{tab:solovyev-cycles}
\end{center}

The hardware latencies detailed in \autoref{tab:solovyev-cycles} are valid only for the sequential implementation. \\
\autoref{tab:solovyev-cycles-par} shows the clock cycles that are needed when convolutional blocks are parallelized. To
obtain the time latency in milliseconds, we multiply the number of clock cycles (hardware latency) by the maximum
frequency presented in \autoref{tab:solovyev-timings}.

\begin{center}
    \begin{tabular}{|r|r|r|r|}
        \hline
            \textbf{Convolutional blocks}   &   \textbf{Clock cycles}   &   \textbf{Data type}  &   \textbf{Timing [ms]}    \\
        \hline
            \multirow{4}{*}{1}  &   \multirow{4}{*}{236'746}    &   32-bit  &   3.857   \\
                &   &   16-bit  &   3.623   \\
                &   &   8-bit   &   3.521   \\
                &   &   4-bit   &   3.527   \\
        \hline
            \multirow{4}{*}{2}  &   \multirow{4}{*}{125'320}    &   32-bit  &   2.042   \\
                &   &   16-bit  &   1.918   \\
                &   &   8-bit   &   1.864   \\
                &   &   4-bit   &   1.867   \\
        \hline
            \multirow{4}{*}{4}  &   \multirow{4}{*}{67'861}     &   32-bit  &   1.106   \\
                &   &   16-bit  &   1.038   \\
                &   &   8-bit   &   1.009   \\
                &   &   4-bit   &   1.011   \\
        \hline
    \end{tabular}
    \captionof{table}{Clock cycles with parallelized convolutional blocks \cite{solovyev-2019}.}
    \label{tab:solovyev-cycles-par}
\end{center}

\autoref{tab:solovyev-cycles-par} shows that adding parallelized convolutional blocks reduces drastically the inference
time.


\subsection{Inference generated with our workflow}

The third source comes from the Vivado \acrshort{hls} code that has been generated following the workflow described in
this paper. Upon compilation, the Vivado \acrshort{hls} tool outputs several estimates: resources, timing and latency.
In this subsection, we will present the values estimated by the tool for each IPs previously generated.


\subsubsection{Accuracy}

The \autoref{tab:results-accuracy} shows the accuracy each previously generated IP performs for handwritten digits
recognition.

\begin{center}
    \begin{tabular}{|l|r|}
        \hline
            \textbf{Generated IP}   &   \textbf{Accuracy}   \\
        \hline
            \textbf{FP32}           &   97.26\%             \\
            \textbf{FP16}           &   97.28\%             \\
            \textbf{FP8}            &   9.81\%              \\
            \textbf{FP4}            &   9.80\%              \\
        \hline
    \end{tabular}
    \captionof{table}{Accuracy for each generated IP.}
    \label{tab:results-accuracy}
\end{center}

It can be noticed that allocating less than 16 bits for fixed-point data representation decreases drastically the
model accuracy. \\
Interestingly, we notice that decreasing the bits used from 32 to 16 increases slightly the model accuracy. It shows
that handwritten digits recognition doesn't require high precision for its weights.


\subsubsection{Resources utilization}

The manual modifications we made to the generated Vivado \acrshort{hls} code implies that weights are included into
the IP as memory. Thus, a lot of memory resources might be used for the IP implementation when the SystemVerilog module
written by \citeauthor{solovyev-2019} fetches weights from another module than the one doing the inference. Moreover, we
also introduced the AXI-Stream protocol, which might change resources utilization and latency. For the sake of the good
comparison, in this subsection, we will present four versions for each of the generated Vivado \acrshort{hls} IPs.

\newpage
\autoref{tab:results-resources} presents the resources available on the Zybo Z7-20 development board. The values are
interesting for comparing with the following graphs.

\begin{center}
    \begin{tabular}{|l|r|}
        \hline
            \textbf{Resource}   &   \textbf{Available}  \\
        \hline
            \textbf{LUT}        &   53'200              \\
            \textbf{FF}         &   106'400             \\
            \textbf{BRAM}       &   280                 \\
            \textbf{DSP}        &   220                 \\
        \hline
    \end{tabular}
    \captionof{table}{Amount of resources available on the Zybo Z7-20.}
    \label{tab:results-resources}
\end{center}

\begin{enumerate}
    \item   No AXI-Stream \& no weights included. \\
            \begin{minipage}{\linewidth}
                \centering
                \includegraphics[width=\linewidth]{figures/results-ns_nw.png}
                \captionof{figure}{Resources utilization without AXI-Stream and weights not included into the IP.}
                \label{fig:results-ns-nw}
            \end{minipage}

            \autoref{fig:results-ns-nw} shows that the BRAM utilization increases proportionately with the number of
            bits used for fixed-point data representation. \\
            About the other resources, we notice that 32- and 16-bit use approximatively the same amount and the same
            goes for 8- and 4-bit fixed-point data representation.

    \item   No AXI-Stream \& weights included. \\
            \begin{minipage}{\linewidth}
                \centering
                \includegraphics[width=\linewidth]{figures/results-ns_w.png}
                \captionof{figure}{Resources utilization without AXI-Stream and weights included into the IP.}
            \end{minipage}

            From the chart above, we observe that including the model's weights into the IP only increase the amount
            of BRAM used. The other resources are left untouched.

    \newpage
    \item   AXI-Stream \& no weights included. \\
            \begin{minipage}{\linewidth}
                \centering
                \includegraphics[width=\linewidth]{figures/results-s_nw.png}
                \captionof{figure}{Resources utilization with AXI-Stream and weights not included into the IP.}
            \end{minipage}

            Adding the AXI-Stream protocol for fetching the input data increases the utilization of BRAM, FF and LUT
            resources. The DSP48E resources utilization is the same with or without the AXI-Stream protocol.

    \item   AXI-Stream \& weights included. \\
            \begin{minipage}{\linewidth}
                \centering
                \includegraphics[width=\linewidth]{figures/results-s_w.png}
                \captionof{figure}{Resources utilization with AXI-Stream and weights included into the IP.}
            \end{minipage}

            As noticed in the previous graphs, all resources utilization increase except for the \texttt{DSP48E}, which
            doesn't change either the AXI-Stream protocol or the weights are included in the IP.
\end{enumerate}

From the tables above, it can be seen that the more bits are used for data formats, the more resources are used. When
the weights are included into the IP, the BRAM resources utilization increases. \\
When AXI-Stream is used as the protocol for fetching input data, BRAM, Flip-Flops and LUT resources increase a bit.


\subsubsection{Timing}

Upon generation, the Vivado \acrshort{hls} tool also outputs the timing estimates. These timings change with the data
format but do not depend on the AXI-Stream protocol or where the weights are stored. \autoref{tab:results-timings} shows
the estimated timings for each of the generated IPs.

\begin{center}
\begin{tabular}{|l|r|r|}
\hline
                    &   \textbf{Timing [ns]}    &   \textbf{Max freq. [MHz]}    \\
\hline
    \textbf{FP32}   &   8.621   &   115.996 \\
    \textbf{FP16}   &   8.671   &   115.327 \\
    \textbf{FP8}    &   8.465   &   118.133 \\
    \textbf{FP4}    &   8.709   &   114.824 \\
\hline
\end{tabular}
    \captionof{table}{Estimated timings for generated IPs.}
    \label{tab:results-timings}
\end{center}

We notice that changing the fixed-point data representation doesn't significantly change the maximum frequency (1\% of
variation at most).


\subsubsection{Latency}

The latency changes if the AXI-Stream protocol is used for the input data or not. Latency estimates will be presented
for each previously generated IP. \\
For calculating the estimated timing in milliseconds, we take the average latency and multiply it by the maximum
frequency presented in \autoref{tab:results-timings}.

\begin{enumerate}[label=\textbf{\arabic*.}]
    \item   \textbf{FP32}   \\
            \begin{minipage}{\linewidth}
                \centering
                \begin{tabular}{|l|r|r|r|r|}
                \hline
                                        &   \textbf{Minimum}    &   \textbf{Maximum}    &   \multicolumn{1}{c|}{\textbf{Average}}   &   \textbf{Timing [ms]}    \\
                \hline
                    \textbf{No Stream}  &   4'570'271           &   4'810'211           &   4'690'241                               &   40.435                  \\
                    \textbf{Stream}     &   4'571'254           &   4'811'194           &   4'691'224                               &   40.443                  \\
                \hline
                \end{tabular}
                \captionof{table}{Latency estimates for 32-bit fixed-point IP.}
            \end{minipage}

    \item   \textbf{FP16}   \\
            \begin{minipage}{\linewidth}
                \centering
                \begin{tabular}{|l|r|r|r|r|}
                \hline
                                        &   \textbf{Minimum}    &   \textbf{Maximum}    &   \multicolumn{1}{c|}{\textbf{Average}}   &   \textbf{Timing [ms]}    \\
                \hline
                    \textbf{No Stream}  &   4'242'559           &   4'489'443           &   4'366'001                               &   37.858                  \\
                    \textbf{Stream}     &   4'243'542           &   4'490'426           &   4'366'984                               &   37.866                  \\
                \hline
                \end{tabular}
                \captionof{table}{Latency estimates for 16-bit fixed-point IP.}
            \end{minipage}

    \item   \textbf{FP8}    \\
            \begin{minipage}{\linewidth}
                \centering
                \begin{tabular}{|l|r|r|r|r|}
                \hline
                                        &   \textbf{Minimum}    &   \textbf{Maximum}    &   \multicolumn{1}{c|}{\textbf{Average}}   &   \textbf{Timing [ms]}    \\
                \hline
                    \textbf{No Stream}  &   3'022'067           &   3'215'371           &   3'118'719                               &   26.400                  \\
                    \textbf{Stream}     &   3'023'050           &   3'216'354           &   3'119'702                               &   26.408                  \\
                \hline
                \end{tabular}
                \captionof{table}{Latency estimates for 8-bit fixed-point IP.}
            \end{minipage}

    \item   \textbf{FP4}    \\
            \begin{minipage}{\linewidth}
                \centering
                \begin{tabular}{|l|r|r|r|r|}
                \hline
                                        &   \textbf{Minimum}    &   \textbf{Maximum}    &   \multicolumn{1}{c|}{\textbf{Average}}   &   \textbf{Timing [ms]}    \\
                \hline
                    \textbf{No Stream}  &   3'017'363           &   3'210'667           &   3'114'015                               &   27.120                  \\
                    \textbf{Stream}     &   3'018'346           &   3'211'650           &   3'114'998                               &   27.129                  \\
                \hline
                \end{tabular}
                \captionof{table}{Latency estimates for 4-bit fixed-point IP.}
            \end{minipage}
\end{enumerate}

The tables above show that adding the AXI-Stream protocol for fetching input data increases the hardware latency by 983
clock cycles, independently of the fixed-point data representation. \\
The latency decreases greatly when the number of bits used for data representation is also decreased. The difference is
less noticeable when passing from 8-bit to 4-bit data representation. However, it is interesting to note that even if
the hardware latency decreases a bit when using 4-bit instead of 8-bit data representation, the time latency (in
milliseconds) increases. This is due to the maximum frequency being lower for 4-bit data representation.

From all the results presented in this subsection, we decided to pick the IP which uses the 16-bit fixed-point data
representation because it offers the best accuracy for resources utilization. It will be used for the comparison with
the other solutions.


\subsection{Inference generated and optimized}

The final source is the result of the \autoref{sec:framework-transformations}. This actual subsection presents the
performances offered by the IP with transformations applied by hand to the code generated by HeteroCL. These
transformations have been only applied to the IP using 16-bit for fixed-point data representation, because it was the
best candidate of the unoptimized generated solutions.


\subsubsection{Accuracy}

The accuracy of this solution is the same as the 16-bit unoptimized IP detailed in the previous subsection. As a
reminder, it was \textbf{97.28\%}.


\subsubsection{Resources utilization}

Adding transformations to the code such as pipelining functions increases the resources utilization.
\autoref{tab:results-opt-resources} presents the resources utilization of the optimized generated solution.

\begin{center}
    \begin{tabular}{|l|r|r|}
    \hline
        \textbf{Resource}   &   \multicolumn{1}{c|}{\textbf{Utilization}}    &   \textbf{Available} \\
    \hline
        \textbf{LUT}        &   51'545 (96.89\%)    &   53'200  \\
        \textbf{FF}         &   39'750 (37.36\%)    &   106'400 \\
        \textbf{BRAM}       &   88 (31.43\%)        &   280     \\
        \textbf{DSP}        &   80 (36.36\%)        &   220     \\
    \hline
    \end{tabular}
    \captionof{table}{Resources utilization for optimized generated IP.}
    \label{tab:results-opt-resources}
\end{center}

We notice that the \acrshort{lut} utilization is almost overflowing. The DSP utilization is close to the double of the
previous solution.


\subsubsection{Timing}

Upon synthesis, the Vivado Design tool outputs \textbf{8.742 nanoseconds} as the critical path delay. This means that
the maximum frequency at which the IP can operate is \textbf{114.390 MHz}.


\subsubsection{Latency}

The main goal of applying transformations to the code was to reduce the latency. At the end, when all transformations
presented in \autoref{tab:framework-transformations} have been applied, the hardware latency of the IP is
\textbf{1'242'094} clock cycles, which is about a fourth of the solution presented previously. \\
If we multiply this latency by the estimated timing, we obtain a time latency of \textbf{10.858 milliseconds}.


\subsection{Comparison}

This subsection summarizes and compares the three solutions on their accuracy, their resources utilization, and their
timings.

From the HeteroCL-generated solutions, we picked the ones using the 16-bit fixed-point data representation, as they
presented the best performances over the other data formats. In order to have a consistent comparison, we will therefore
use the human-developed module with 16-bit fixed-point.

In the following results, the solution named "CPU" refers to the first set of results, where the inference was run on a
\acrshort{cpu}.
The solution referred by "Human-\#" is the one described by \citeauthor{solovyev-2019} in the
\citetitle{solovyev-2019} paper \cite{solovyev-2019}. The "\#" represents the number of convolutional blocks that have
been parallelized. An absence of "\#" means that the value is the same regardless of whether several convolutional
blocks have been parallelized or not. \\
"FP16" refers to the IP previously generated by HeteroCL and which uses the 16-bit fixed-point data
representation. \\
Finally, "FP16-opt" refers to the last solution, which is FP16 but optimized with code transformations.


\subsubsection{Accuracy}

\begin{center}
    \begin{tabular}{|l|r|}
        \hline
            \multicolumn{1}{|c|}{\textbf{Solution}}  &   \textbf{Accuracy}   \\
        \hline
            \textbf{CPU}        &   97.56\%             \\
            \textbf{Human}      &   97.56\%             \\
            \textbf{FP16}       &   97.28\%             \\
            \textbf{FP16-opt}   &   97.28\%             \\
        \hline
    \end{tabular}
    \captionof{table}{Accuracy of the different solutions.}
    \label{tab:results-comp-accuracy}
\end{center}

From \autoref{tab:results-comp-accuracy}, we notice that the accuracy doesn't change much among the proposed solutions.
This first comparison shows that HeteroCL performs well at keeping quite the same accuracy even with weights fixed-point
data original representation being truncated.


\subsubsection{Resources utilization}

Comparing the software solution and the hardware solutions is complex for resources utilization, since they are not very
related. Here, we compare the memory utilization for each solution and then we compare the Zybo Z7-20 development board
resources utilization for each hardware solution.


\begin{center}
    \begin{tabular}{|l|r|}
        \hline
            \multicolumn{1}{|c|}{\textbf{Solution}}  &   \textbf{Memory [KiB]}   \\
        \hline
            \textbf{CPU}        &   313.3   \\
            \textbf{Human-1}    &   63.0    \\
            \textbf{Human-2}    &   78.8    \\
            \textbf{Human-4}    &   126.0   \\
            \textbf{FP16}       &   195.8   \\
            \textbf{FP16-opt}   &   198.0   \\
        \hline
    \end{tabular}
    \captionof{table}{Memory utilization of the different solutions.}
    \label{tab:results-comp-memory}
\end{center}

\autoref{tab:results-comp-memory} shows that hardware solutions require less memory than the software one. The
human-developed inference solution requires up to 5 times less than the \acrshort{cpu} one (for the sequential version).
The optimized version of FP16 doesn't require much more memory than the unoptimized version (1\% more).

The next table details the complete resources utilization for all hardware solutions. As a reminder, the Zybo Z7-20
\acrshort{fpga} development board offers 53'200 \acrshortpl{lut}, 106'400 \acrshortpl{ff}, 280 \acrfull{bram} and 220
DSP48E1 units.

\begin{center}
    \begin{tabular}{|l|r|r|r|r|r|}
        \hline
            \textbf{Resource}   &   \multicolumn{1}{c|}{\textbf{Human-1}}   &   \multicolumn{1}{c|}{\textbf{Human-2}}   &   \multicolumn{1}{c|}{\textbf{Human-4}}   &   \multicolumn{1}{c|}{\textbf{FP16}}  &   \multicolumn{1}{c|}{\textbf{FP16-opt}}  \\
        \hline
            \textbf{LUT}    &   2'491 (4.68\%)  &   3'286 (6.18\%)  &   4'811 (9.04\%)  &   34'458 (64.77\%)    &   51'545 (96.89\%)    \\
            \textbf{FF}     &   1'562 (1.47\%)  &   2'096 (1.97\%)  &   3'198 (3.01\%)  &   17'079 (16.05\%)    &   39'750 (37.36\%)    \\
            \textbf{BRAM}   &   28 (10.00\%)    &   35 (12.50\%)    &   56 (20.00\%)    &   87 (31.07\%)        &   88 (31.43\%)        \\
            \textbf{DSP}    &   17 (7.73\%)     &   28 (12.73\%)    &   54 (24.55\%)    &   48 (21.82\%)        &   80 (36.36\%)        \\
        \hline
    \end{tabular}
    \captionof{table}{Resources utilization of the hardware solutions.}
\end{center}

We notice that resources utilization, especially for \acrshortpl{lut} and \acrshortpl{ff}, is way more higher for the IP
generated with HeteroCL than for the IPs developed by a human developer. Most notably, the FP16 solution only has one
convolutional block when the solution developed by \citeauthor{solovyev-2019} offers up to 4 convolutional blocks in
parallel. \\
The optimized FP16-opt solution uses almost all of the available \acrshortpl{lut} and consumes about the double of the
\acrshortpl{ff} and DSP units than the unoptimized solution. It only requires one more \acrshort{bram}.


\subsubsection{Timing}

Timing cannot be discussed for the software solution since it doesn't have critical path delay nor a maximum frequency
at which it can operate. Thus, only the hardware solutions will be discussed in the table below.

\begin{center}
\begin{tabular}{|l|r|r|}
\hline
    \textbf{Solution}   &   \textbf{Timing [ns]}    &   \textbf{Max freq. [MHz]}    \\
\hline
    \textbf{Human}      &   15.303  &   65.347  \\
    \textbf{FP16}       &   8.671   &   115.327 \\
    \textbf{FP16-opt}   &   8.742   &   114.390 \\
\hline
\end{tabular}
    \captionof{table}{Timing estimates for hardware solutions.}
    \label{tab:results-comp-timing}
\end{center}

\autoref{tab:results-comp-timing} shows that the IP generated by HeteroCL can operate at a frequency almost twice as big
as the SystemVerilog module presented in the \citetitle{solovyev-2019} paper \cite{solovyev-2019}. We notice that both
the FP16 and FP16-opt solutions have about the same maximum frequency. \\
This measure might be interesting, depending on the latency of both IPs, since the generated one can operate faster.
This same latency is presented next.


\subsubsection{Latency}

\begin{center}
\begin{tabular}{|l|r|r|}
\hline
    \textbf{Solution}   &   \textbf{Clock cycles}   &   \textbf{Latency [ms]}   \\
\hline
    \textbf{CPU}        &   -           &   7.386   \\
    \textbf{Human-1}    &   236'746     &   3.623   \\
    \textbf{Human-2}    &   125'320     &   1.918   \\
    \textbf{Human-4}    &   67'861      &   1.038   \\
    \textbf{FP16}       &   4'366'984   &   37.866  \\
    \textbf{FP16-opt}   &   1'242'094   &   10.858  \\
\hline
\end{tabular}
    \captionof{table}{Latency of the different solutions.}
\end{center}

In terms of both hardware and time latencies, the table above shows that the hardware solution written by a human
developer outperforms software and the other hardware solutions. We also notice the performances increase when
parallelizing multiple convolutional blocks. \\
We observe that adding simple code transformations to the generated Vivado \acrshort{hls} code reduces the hardware
latency by a factor of almost 4. In terms of time latency, the FP16-opt solution almost reaches the software solution.


\subsubsection{Summary}

From \autoref{tab:results-comp-accuracy}, we noticed that truncating the weights width to 16-bit from 32-bit doesn't
change the accuracy much. This observation is valid for both hardware solutions.

In terms of memory usage, hardware solutions beat the software solution by a factor of two thirds. We saw that
adding convolutional blocks to the human-developed hardware solution increases the memory usage but stays way under the
software solution usage. However, the FP16 solution requires more memory than the human-developed, even if up
to 4 convolutional blocks are incorporated in the SystemVerilog module. We also saw that adding code transformations for
pipelining loops doesn't increase memory usage by a significant amount. \\
When comparing the resources usage of hardware solutions, the human-developed solution clearly outperforms the
solution generated by HeteroCL. For some resources (\ie \acrshort{lut}), there is a difference of more than 60\%. We
also saw that the optimized FP16-opt solution uses almost all of the available \acrshortpl{lut} and consumes about the
double of \acrshortpl{ff} and DSP48E1 units than the FP16 hardware solution.

When it comes to timing, HeteroCL-generated IPs outperform the human-developed solution. In fact, we saw
that both unoptimized and optimized solutions can operate at a maximum frequency of almost the double of the
human-developed's maximum frequency.

However, concerning time latency, the generated IP is far behind the other two solutions. The latter performs the
inference in more than 37 milliseconds, when the software solution do it in about 7 milliseconds. Finally, the best of
the three solutions appears to be the hardware solution developed by a human developer. It performs inference in 3.623
milliseconds with only one convolutional block and it goes down to 1 millisecond with 4 convolutional blocks. We noticed
that once code transformations have been applied to the HeteroCL solution, time latency has been reduced by almost a
factor 4 for the same maximum frequency, thus decreasing to 10.858 milliseconds.

Ultimately, this comparison showed that the hardware solution written by \citeauthor{solovyev-2019} performs better for
almost all points, except for the maximum frequency. The software solution defends defends itself rather well on all
points of comparison. Moreover, the hardware solutions \textit{automatically} generated with the HeteroCL framework keep
up in term of memory usage but not in terms of overall resources usage. They offer the greatest maximum frequency but
fails to keep pace in terms of latency, thus cancelling the interest for its maximum frequency. It is interesting to
note that the optimized version of the HeteroCL-generated hardware solution doesn't require much knowledge to apply
code transformations while offering a latency 4 times lower than the unoptimized version. However, optimizing the
IP by pipelining its loops requires a lot of resources, and we noticed that the optimized version almost overflew in
terms of \acrshortpl{lut}.