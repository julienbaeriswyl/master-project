\cleardoublepage
\section{Using the framework}

This section presents how to generate \acrshort{hls} code using HeteroCL, the framework chosen to be critically
analyzed in this project. \\
As a first step, we explain how to install HeteroCL. Then, we show how we used the framework to generate \acrshort{hls}
code and present some custom modifications we chose to apply to the generated \acrshort{hls} code. Finally, a discussion
and a critical review about the use of this framework is presented.

The explanations in this section can be very technical. It is possible to go directly to
\autoref{sec:framework-analysis} for a critical analysis of the framework or to \autoref{sec:results} for the results
analysis.


\subsection{Installation}

The HeteroCL framework is available on their Github repository \cite{github-heterocl}. However, we decided to not use
the official version but chose instead a version of the framework that we modified especially for this project. \\
Indeed, some operations were not available in the official source code, so we added them in our personal fork.

Before installing the framework, one must install LLVM compiler. The tool requires version 4.0, 5.0, 6.0 or 7.0.

To install HeteroCL, simply clone our modified version and install it:
\begin{minted}{bash}
git clone --recursive https://github.com/faku99/heterocl.git
cd heterocl/
make
\end{minted}


\subsection{Generating High-Level Synthesis code}
\label{sec:framework-generating-hls}

This step consists of specifying to the framework the \acrshort{ml} model we are using. Unfortunately, HeteroCL doesn't
support loading a model from a file yet, thus we had to define the model architecture once again, but this time using
HeteroCL API instead of Keras'. Then, the weights of the trained model are loaded one by one and given to HeteroCL
for inference.

\inputminted[firstline=110,lastline=163,label=src/python/generate\_hls.py]{python}{../src/python/generate_hls.py}

This code does essentially the same thing as the first snippet of code in \autoref{sec:workflow-training}. The arguments
of the \mintinline{python}{build_mnist()} function are the weights we obtained previously, during the training. They are
needed by HeteroCL for generating the hardware operations, which depend on the data length and type.

\inputminted[firstline=165,lastline=191,label=src/python/generate\_hls.py]{python}{../src/python/generate_hls.py}

The \mintinline{python}{build_mnist_inf} quantizes the network layers weights. It is in this particular function that we
can inform HeteroCL the fixed-point format we want to use for the data. \\
The \mintinline{python}{qtype1} argument is the quantization type for the weights, while \mintinline{python}{qtype2} is
for the activation layers. \\
A quantization type is defined as follows:
\begin{minted}{python}
import heterocl as hcl

qtype = hcl.Fixed(32,30)
\end{minted}
Here, 32 corresponds to the total number of bits and 30 the number of bits used for the mantissa. Thus, 1 bit is used
for the sign and 1 bit is used for the integer part (0 or 1).

For this project, we measured the performance of the four following quantization types:

\begin{multicols}{2}
\begin{enumerate}
\item   \mintinline{python}{hcl.Fixed(32,30)}
\item   \mintinline{python}{hcl.Fixed(16,14)}
\item   \mintinline{python}{hcl.Fixed(8,6)}
\item   \mintinline{python}{hcl.Fixed(4,2)}
\end{enumerate}
\end{multicols}

HeteroCL offers the possibility of running an hardware simulation of the model on the \acrshort{cpu}.
\autoref{tab:workflow-types_acc} shows the accuracy for each of the quantization types:

\begin{center}
    \begin{tabular}{|l|r|}
        \hline
            \textbf{Quantization type}  &   \textbf{Accuracy}   \\
        \hline
            \texttt{hcl.Fixed(32,30)}   &   97.26\%             \\
            \texttt{hcl.Fixed(16,14)}   &   97.28\%             \\
            \texttt{hcl.Fixed(8,6)}     &   9.81\%              \\
            \texttt{hcl.Fixed(4,2)}     &   9.80\%              \\
        \hline
    \end{tabular}
    \captionof{table}{Accuracy for each quantization types.}
    \label{tab:workflow-types_acc}
\end{center}

We observe a small loss when the model passes from using 32-bit fixed-point representation to 16-bit
fixed-point representation. However, the loss is more than acceptable (0.02\%). On the contrary, the loss of switching
from the 32- or 16-bit representation to the 8-bit fixed-point representation is way more important and is not suitable
for a real-life application. We also notice that there is no loss starting from the 4-bit fixed-point representation. \\
The impact on resources utilization was measured and will be discussed later.

The entirety of the source code for this subsection is available in \autoref{appx:generate-hls}.

An example of generated Vivado \acrshort{hls} code is available in \autoref{appx:hls_fp32}.


\subsection{Manual modifications}
\label{sec:framework-modifications}

Depending on how data is passed to the IP, we may want to make some modifications to the previously generated Vivado
\acrshort{hls} code. This step is optional.

In this subsection, we will show how to include the \acrshort{ml} model's weights into the IP and how to accept the
input data from the AXI-Stream protocol.

The first step is to export the weights of the trained model to C/C++ arrays so that we can \textit{delete} the
arguments of the Vivado \acrshort{hls} function. The following \mintinline{python}{exports_weights} function allows to
export the weights in C/C++ arrays style.

\inputminted[firstline=194,lastline=215,label=src/python/generate\_hls.py]{python}{../src/python/generate_hls.py}

This function generates a file for each entry present in the \mintinline{python}{weight} dictionary passed as the
function parameter. The resulting C/C++ array is named \mintinline{text}{w_<name>}, where \mintinline{text}{<name>}
is the dictionary key corresponding to the weights array. The file has the same name as what will be placed into a
folder named \mintinline{text}{weights}.

After having exported of the trained weights as C/C++ arrays, we include each header file into the Vivado
\acrshort{hls} code and modify the function signature.

\begin{minted}[label=mnist\_fp32.cpp]{diff}
+   #include "w_conv1.h"
+   #include "w_conv2.h"
+   #include "w_conv3.h"
+   #include "w_conv4.h"
+   #include "w_conv5.h"
+   #include "w_conv6.h"
+   #include "w_dense_1.h"

-   void default_function(float input[1][1][28][28], float w_conv1[4][1][3][3], float w_conv2[4][4][3][3], float w_conv3[8][4][3][3], float w_conv4[8][8][3][3], float w_conv5[16][8][3][3], float w_conv6[16][16][3][3], float w_dense1[16][10], float output[1][10]) {
+   void mnist_fp32(float input[1][1][28][28], output[1][10]) {
\end{minted}

Noticeably, we also changed the function name. The function we discuss in this subsection is the one using 32-bit
fixed-point data. All the modifications made to this function can be applied to the others data formats generated
previously.

The next modification to make concerns the input protocol. At this point, the generated IP expects the address of
an array containing the input image 28x28 data. However, we wanted the IP to gather the data from a \acrfull{vdma}
offering AXI-Stream protocol for the reading part.

The first thing to do was to include the header files offered by Vivado for using AXI-Streams.

\begin{minted}[label=mnist\_fp32.cpp]{diff}
+   #include <hls_video.h>
\end{minted}

Some definitions are then needed, as for the pixel's format and the type of the elements the AXI-Stream is composed of.
Moreover, the function arguments needed to be modified.

\begin{minted}[label=mnist\_fp32.cpp]{diff}
+   typedef ap_axiu<32,1,1,1> pixel_t;
+   typedef hls::stream<pixel_t> stream_t;

-   void mnist_fp32(float input[1][1][28][28], float output[1][10]) {
+   void mnist_fp32(stream_t& stream_in, output[1][10]) {
\end{minted}

The following snippet of code is used to synchronize the AXI-Stream. It is a bit tricky, but it basically waits for the
start signal, indicating the beginning of the input image. It also waits for the end signal, which, in turn, indicates
the end of a row of the input image, thus every 28 pixels. \\
It also puts the data received from the AXI-Stream into a \mintinline{text}{float} array.

\begin{minted}[label=mnist\_fp32.cpp]{diff}
+       float image[1][1][28][28];
+       pixel_t pixel;
+       union_t uni;
+       HLS_SIZE_T i, j;
+
+       bool sof = 0;
+       loop_wait_sof: while (sof == 0) {
+   #pragma HLS LOOP_TRIPCOUNT avg=0 max=0
+   #pragma HLS PIPELINE II=1
+           stream_in >> pixel;
+           sof = pixel.user.to_int();
+       }
+
+       loop_height: for (i = 0; i < 28; ++i) {
+           bool eol = 0;
+
+           loop_width: for (j = 0; j < 28; ++j) {
+   #pragma HLS LOOP_FLATTEN off
+   #pragma HLS PIPELINE II=1
+               if (sof || eol) {
+                   sof = 0;
+                   eol = pixel.last.to_int();
+               }
+               else {
+                   stream_in >> pixel;
+                   eol = pixel.last.to_int();
+               }
+
+               uni.u = pixel.data.to_uint();
+               image[0][0][i][j] = uni.f; // pixel.data.to_double();
+           }
+
+           loop_wait_eol: while (eol == 0) {
+   #pragma HLS PIPELINE II=1
+   #pragma HLS LOOP_TRIPCOUNT avg=0 max=0
+               stream_in >> pixel;
+               eol = pixel.last.to_int();
+           }
+       }
\end{minted}

The last modification we made was to tell the compiler that our input uses the AXI-Stream protocol. This can be
made by adding a pragma to the code.

\begin{minted}[label=mnist\_fp32.cpp]{diff}
+   #pragma HLS INTERFACE axis port=stream_in
\end{minted}

After all those custom modifications were made, we compiled the Vivado \acrshort{hls} code and moved on to critically
discuss the use of the HeteroCL framework.


\subsection{Code analysis}

This subsection presents a critic analysis about the quality of the \acrshort{hls} code generated by the HeteroCL
framework.

The first observation we can make is about the code readability. At several points in the code, we notice things like
the following:
\begin{minted}{c++}
float reducer84;
reducer84 = 0.000000e+00f;
\end{minted}
Even if it doesn't change anything for the compiler's understanding, a better way for human comprehension would be
something like \mintinline{c++}{float reducer84 = 0.0f}. However, we consider it a normal behavior since it might be
difficult to add a \textit{human-readability} dimension during \acrshort{hls} code generation.

The second observation is about array indexes. Often, the first dimension of the arrays generated is 1.
\begin{minted}{c++}
float conv1[1][4][28][28];
\end{minted}
This causes the generated code to have a useless \textit{outside} loop that lowers the readability. Again, this doesn't
affect the compilation, since the compiler optimizes it by erasing the \textit{outside} loop.

The third observation is about optimizations that are made automatically by the Vivado \acrshort{hls} compiler. During
compilation, some functions (\eg \mintinline{c++}{std::max()}) are inlined, small loops are automatically unrolled and
array are partitioned. We consider these features being helpful and time-saving.

Finally, the most important observation, and most critical one. In \autoref{sec:framework-generating-hls}, we specified
a type to use for the quantization and data layers. This is respected for the \acrshort{relu} layers, as shown below.
\begin{minted}[linenos=true,firstnumber=117,label=mnist\_fp16.cpp]{c++}
ap_fixed<16, 2> relu1[1][4][28][28];
for (ap_int<32> args = 0; args < 1; ++args) {
    for (ap_int<32> args0 = 0; args0 < 4; ++args0) {
        for (ap_int<32> args1 = 0; args1 < 28; ++args1) {
            for (ap_int<32> args2 = 0; args2 < 28; ++args2) {
                relu1[args][args0][args1][args2] = ((ap_fixed<16, 2>)((conv1[args][args0][args1][args2] < 0.000000e+00f) ? 0.000000e+00f : conv1[args][args0][args1][args2]));
            }
        }
    }
}
\end{minted}

On line 122, we notice that \mintinline{c++}{float} values are casted to \mintinline{c++}{ap_fixed<16, 2>}, which is the
data representation we specified previously.

For the data layers (\eg convolutional layers), calculations and storage are done in floating-point data representation.

\begin{minted}[linenos=true,firstnumber=102,label=mnist\_fp16.cpp]{c++}
float conv1[1][4][28][28];
for (ap_int<32> ff = 0; ff < 4; ++ff) {
    for (ap_int<32> yy = 0; yy < 28; ++yy) {
        for (ap_int<32> xx = 0; xx < 28; ++xx) {
            float reducer84;
            reducer84 = 0.000000e+00f;
            for (ap_int<32> ry = 0; ry < 3; ++ry) {
                for (ap_int<32> rx = 0; rx < 3; ++rx) {
                    reducer84 = ((pad_temp[0][0][(yy + ry)][(xx + rx)] * w_conv1[ff][0][ry][rx]) + reducer84);
                }
            }
            conv1[0][ff][yy][xx] = reducer84;
        }
    }
}
\end{minted}

Line 102 shows that calculations are stored using \mintinline{c++}{float} format, even if we specified to use 16-bit
fixed-point data representation. Moreover, we notice that calculations are also done with floating-point representation.
This limitation might make the IP more demanding in terms of resources utilization.

We tried to understand why HeteroCL does not use the \mintinline{c++}{ap_fixed<16, 2>} format for data layers. In
activation layers, the fixed-point format is used but we look closely, no operations are made, only a comparison. It
seems that when an operation is needed, HeteroCL prefers to use the floating-point format. Surprisingly, Vivado
\acrshort{hls} offers an implementation for such operations. So, we assumed that the HeteroCL team did not integrate
this feature in the framework yet, or have a good reason to not do so. In the case, we did not find any documentation
talking about this choice.

In conclusion, the code generated by HeteroCL is understandable by a human, even if some enhancements can still be made.
However, we noticed that the frameworks doesn't always respect the data format constraints it has been given during the
development phase.


\subsection{Use analysis}
\label{sec:framework-analysis}

This subsection presents our critic analysis about the use of HeteroCL for generating \acrshort{hls} code from Python.

Looking at the HeteroCL website, we can read that "\textit{HeteroCL provides a fully automated compilation flow from a
HeteroCL program to heterogeneous compute platforms integrating CPUs and FPGAs}".

As it was previously shown, this statement is not entirely valid. Indeed, some widely used operators, such as the
\acrshort{relu} activation function, or also widely used parameters of the 2D-Convolutional operator, like the dilation,
had to be implemented by us. Thus, the "\textit{fully automated compilation flow}" statement is yet to be fully
accurate.

Moreover, the IP generated from the code produced by HeteroCL can't be easily integrated into a system design by people
with little to no experience in hardware designs. The manual modifications we presented in the subsection above are
required if one wants to change the protocol used to fetch the input data. The default protocol, fetching data from
an address in memory, is fine but might not be relevant in some cases, as our system design, which carries the image
data captured from the camera sensor to the \acrshort{hdmi} output port by using the AXI-Stream protocol.

In conclusion, HeteroCL still has a long way to go to propose a fully automated compilation flow for programming
\acrshortpl{fpga} that can be used by any developer with little hardware design knowledge.


\subsection{Optimizations}
\label{sec:framework-transformations}

This subsection presents the transformations manually applied to the \acrshort{hls} code to optimize it. Every step will
be presented and all resources variations will be discussed.

At this point, apart from the customizations applied previously (\ie adding the AXI-Stream protocol and including the
weights into the IP), the \acrshort{hls} code hasn't been touched. No optimizations have been applied to the code. The
Vivado \acrshort{hls} C++ language allows the developer to specify code optimizations by simply adding pragmas (as in
the customizations made to the code in \autoref{sec:framework-modifications}). \\
The \citetitle{hls-transformations} paper, written by \citeauthor{hls-transformations}, presents the possible
transformations and optimizations that can be applied to \acrshort{hls} code \cite{hls-transformations}. In this work,
we only applied pipeline transformations to the code.

A transformation optimizes the code and more extensively, the compiled IP. It is optimized in terms of latency (\ie
clock cycles). However, adding transformations like pipelining requires more resources. In the following paragraphs, we
will present step by step the transformations made and the differences in both terms of clock cycles and resources
utilization. \\
The reference IP for the transformations is the one using 16-bit fixed-point data representation. This
version will now be called \textit{FP16-opt}. This is due to the fact that the work presented in this subsection has
been done after results were analyzed and discussed for the unoptimized versions generated by HeteroCL. Thus, we chose
the best among these and used it as the reference IP.

The first transformation we applied to the code is the following:

\inputminted[firstline=78,lastline=85,linenos]{c++}{../vivado_hls/mnist_fp16-opt/src/mnist_fp16.cpp}

The nested loop shown in the snippet of code above is the first padding operation of the model (just before the first
convolutional network). Here, we apply a pipelining transformation on both loops. This transformation increases the
latency by 5966 clock cycles, the number of DSP by 13, the number of Flip-Flops used by 8'269. It also increases the
number of \acrlongpl{lut} by 7'503. The BRAM usage has not been changed by this transformation. It seems that pipelining
these loops only increases the latency, thus not optimizing the IP. So it has been discarded.

The second transformation happens in the first convolutional block, performed by 5 nested loops.

\inputminted[firstline=86,lastline=101,linenos]{c++}{../vivado_hls/mnist_fp16-opt/src/mnist_fp16.cpp}

The transformation shown in the snippet of code above pipelines the most nested loop. This transformation reduces the
latency by 113'600 clock cycles while increasing the Flip-Flops usage by 244 and \acrshortpl{lut} by 283. The BRAM and
DSP resources utilization have stayed untouched. This particular code transformation is interesting since it greatly
reduces the latency while moderately increasing the resources utilization. \\
Then, we tried to pipeline the loop \texttt{conv1\_kh} at line 92 but it changed the timing estimates from 8.671 to
15.210, thus decreasing the maximum frequency to 65.746 MHz. In terms of latency, the transformation decreased it
by 59'575 clock cycles. Since the tradeoff between latency and maximum frequency is not worth it, we decided to revert
pipelining the loop.

The third transformation consisted of pipelining the \acrshort{relu} activation of the first convolutional layer. As a
test, we tried to pipeline all the loops.

\inputminted[firstline=102,lastline=114,linenos]{c++}{../vivado_hls/mnist_fp16-opt/src/mnist_fp16.cpp}

These transformations reduce the latency by 15'906 clock cycles while the Flip-Flops increase by 185 and the
\acrlongpl{lut} by 148. The BRAM and DSP utilizations did not change. This transformation optimizes well the IP.

Since the transformations are quite the same, we will not detail them anymore. Every "\textit{case}" has already been
treated (\ie padding operation, convolution, and \acrshort{relu} activation). Instead,
\autoref{tab:framework-transformations} lists all the transformations that have been applied to the generated Vivado
\acrshort{hls} code. It also details the timings and resources difference it implies. Values that are in red mean that
they are not satisfying enough (\ie resources overflow, overlong timing) and that the code transformation was therefore
not included. \\
The code including all the transformations presented in \autoref{tab:framework-transformations} is available in
\autoref{appx:fp16-opt}.

As the result of all these transformations added in the Vivado \acrshort{hls} code generated by the HeteroCL framework,
we obtained the performances presented in \autoref{tab:fp16-opt-perfs}. These performances will be discussed in the next
section.

\begin{center}
    \begin{tabular}{|l|r|}
    \hline
        \textbf{Latency}    &   1'242'094 clock cycles  \\
        \textbf{Timing}     &   8.742 ns                \\
        \textbf{Max freq.}  &   114.390 MHz             \\
        \textbf{LUT}        &   51'545 (96.89\%)        \\
        \textbf{FF}         &   39'750 (37.36\%)        \\
        \textbf{BRAM}       &   88 (31.43\%)            \\
        \textbf{DSP}        &   80 (36.36\%)            \\
    \hline
    \end{tabular}
    \captionof{table}{Latency, timing and resources optimizations after applying transformations by hand.}
    \label{tab:fp16-opt-perfs}
\end{center}

We generated Vivado \acrshort{hls} code from a Python code describing a \acrlong{nn} model. Then, we modified the code
to adapt it to our needs (\ie AXI-Stream and weights included in the IP). Moreover, we made critical reviews of both the
code generated by HeteroCL and its use. Finally, we applied code transformations by hand to optimize the resulting
IP. \\
We will now discuss the performances of each of the sources mentioned previously in \autoref{sec:aim-objectives}.

\begin{center}
    \begin{tabular}{|l|r|r|r|r|r|r|}
    \hline
        \textbf{Name}   &   \multicolumn{1}{c|}{\textbf{Cycles}}    &   \multicolumn{1}{c|}{\textbf{Timing}}    &   \textbf{BRAM}   &   \textbf{DSP}    &   \multicolumn{1}{c|}{\textbf{FF}}    &   \multicolumn{1}{c|}{\textbf{LUT}}   \\
    \hline
        pad1        &   \colorbox{red2}{+5966}   &   8.671   &   0   &   +13 &   +8269   &   +7503   \\
        conv1\_kw   &   -113600 &   8.671   &   0   &   0   &   +244    &   +283    \\
        conv1\_kh   &   -59575  &   \colorbox{red2}{15.210} &   -1  &   0   &   +361    &   +207    \\
        relu1       &   -15906  &   8.671   &   0   &   0   &   +546    &   +355    \\
        pad2        &   -59644  &   8.717   &   0   &   -2  &   \colorbox{red2}{+137505} &   \colorbox{red2}{+363813} \\
        conv2\_kw   &   -583537 &   8.671   &   0   &   0   &   -327    &   -156    \\
        pool1       &   -27560  &   8.671   &   0   &   0   &   +255    &   +195    \\
        conv3\_kw   &   -378128 &   8.704   &   0   &   0   &   -17     &   +80     \\
        pad3        &   -10357  &   8.704   &   0   &   0   &   +1129   &   +534    \\
        relu3       &   -8074   &   8.704   &   0   &   0   &   +181    &   +154    \\
        pad4        &   -30963  &   8.704   &   0   &   0   &   +1688   &   +641    \\
        conv4\_kw   &   -655664 &   8.704   &   0   &   0   &   +26     &   +58     \\
        relu4       &   -8074   &   8.704   &   0   &   0   &   +180    &   +154    \\
        relu2       &   -15906  &   8.704   &   0   &   0   &   +186    &   +148    \\
        pool2       &   -13848  &   8.731   &   0   &   0   &   +247    &   +196    \\
        pad5        &   -6298   &   8.742   &   0   &   0   &   +995    &   +429    \\
        pad2-OK     &   -57817  &   8.742   &   0   &   0   &   +1891   &   +761    \\
        conv5\_kw   &   -382848 &   8.742   &   0   &   0   &   -26     &   +60     \\
        relu5       &   -4170   &   8.742   &   0   &   0   &   +177    &   +149    \\
        pad6        &   -19084  &   8.742   &   0   &   0   &   +1595   &   +544    \\
        conv6\_kw   &   -659600 &   8.742   &   0   &   0   &   -17     &   -50     \\
        relu6       &   -4170   &   8.742   &   0   &   0   &   +177    &   +149    \\
        maxpool     &   -8656   &   8.742   &   0   &   0   &   +230    &   +130    \\
        maxpool1    &   -45     &   8.742   &   0   &   0   &   +126    &   +78     \\
        dense       &   -2404   &   \colorbox{red2}{15.210}  &   0   &   0   &   +354    &   +308    \\
        relu2\_h    &   \colorbox{red2}{0}       &   8.742   &   0   &   0   &   +1      &   0       \\
        relu3\_h    &   \colorbox{red2}{0}       &   8.742   &   0   &   0   &   0       &   0       \\
        pad3\_h     &   -511    &   8.742   &   0   &   +13 &   +3204   &   +3185   \\
        pad2\_h     &   -1199   &   8.742   &   0   &   +45 &   +21728  &   \colorbox{red2}{+26863}  \\
        pad4\_h     &   -1023   &   8.742   &   0   &   +13 &   +5946   &   \colorbox{red2}{+9127}   \\
        relu4\_h    &   \colorbox{red2}{0}       &   8.742   &   0   &   0   &   0       &   0       \\
        pad5\_h     &   -288    &   8.742   &   +1  &   +6  &   +1453   &   +1471   \\
        relu5\_h    &   \colorbox{red2}{0}       &   8.742   &   0   &   0   &   +4      &   0       \\
        pad6\_h     &   -576    &   8.742   &   0   &   +6  &   +2606   &   \colorbox{red2}{+4357}   \\
        relu6\_h    &   \colorbox{red2}{0}       &   8.742   &   0   &   0   &   -4      &   0       \\
        maxpool\_h  &   \colorbox{red2}{0}       &   8.742   &   0   &   0   &   -5      &   0       \\
        softmax     &   -18     &   8.742   &   0   &   0   &   +4      &   +18     \\
        softmax1    &   -253    &   \colorbox{red2}{28.512}  &   0   &   0   &   +15     &   -36     \\
        softmax2    &   -522    &   8.742   &   0   &   0   &   +128    &   -78     \\
        results     &   -27     &   8.742   &   0   &   0   &   +131    &   +96     \\
    \hline
    \end{tabular}
    \captionof{table}{List of transformations applied by hand to the Vivado HLS code.}
    \label{tab:framework-transformations}
\end{center}