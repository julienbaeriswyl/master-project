\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{bachelor}%[v1.0 Lucas Elisei LaTeX class]

% Load 'report' class.
\LoadClass[10pt,a4paper]{article}

% Required packages.
\RequirePackage{geometry}
\RequirePackage{lmodern}
\RequirePackage{tikz}

% Layout.
\geometry{left=1.0in,right=1.0in,top=1.0in,bottom=1.0in}

% Default font.
\renewcommand{\familydefault}{\sfdefault}

\newcommand*{\subtitle}[1]{\gdef\@subtitle{#1}}

\newcommand*{\supervisor}[1]{\gdef\@supervisor{#1}}

% Redefine \maketitle
\renewcommand{\maketitle}{
    \newpage
    \null

    \begin{tikzpicture}[remember picture,overlay]
        \node[anchor=north east,inner sep=1cm] at (current page.north east)
        {\includegraphics[height=3cm]{figures/hesso_logo.png}};
    \end{tikzpicture}

    \begin{tikzpicture}[remember picture,overlay]
        \node[anchor=north west,inner sep=1cm] at (current page.north west)
        {\includegraphics[width=8cm]{figures/mse_logo.jpg}};
    \end{tikzpicture}

    \vskip 20pt

    Master of Science HES-SO in Engineering \\
    Avenue de Provence 6 \\
    CH-1007 Lausanne

    \vfill

    \begin{flushright}
        {\fontsize{25}{25}\selectfont Master of Science HES-SO in Engineering}
        \vskip 20pt
        {\fontsize{15}{15}\selectfont Orientation: Technologies de l'Information et la Communication (TIC)}
        \vskip 60pt
        {\fontsize{30}{30}\selectfont AUTOMATIC GENERATION OF}
        \vskip 6pt
        {\fontsize{30}{30}\selectfont FPGA-BASED ACCELERATORS}
        \vskip 40pt
        {\fontsize{15}{15}\selectfont Fait par}
        \vskip 6pt
        {\fontsize{30}{30}\selectfont Lucas ELISEI}
        \vskip 40pt
        {\fontsize{15}{15}\selectfont Sous la direction de}
        \vskip 3pt
        {\fontsize{15}{15}\selectfont Prof. Alberto Dassatti}
        \vskip 3pt
        {\fontsize{15}{15}\selectfont Dans l'institut REDS à l'HEIG-VD}
        \vfill
        {\fontsize{15}{15}\selectfont Expert externe: Andrea Guerrieri, EPFL}
        \vfill
        {\fontsize{15}{15}\selectfont Lausanne, HES-SO//Master, Février 2020}
    \end{flushright}
}

% Disable paragraph indentation.
\setlength\parindent{0pt}

% Create i.e. and e.g. macros
\newcommand{\ie}{\textit{i.e.} }
\newcommand{\eg}{\textit{e.g.} }

% Add Appendix to autoref
\newcommand*{\Appendixautorefname}{Appendix}