#include <stdio.h>
#include <time.h>

#include "tensorflow/lite/interpreter.h"
#include "tensorflow/lite/kernels/register.h"
#include "tensorflow/lite/model.h"
#include "tensorflow/lite/optional_debug_tools.h"

using namespace tflite;

#define TFLITE_MINIMAL_CHECK(x)                                  \
    if (!(x)) {                                                  \
        fprintf(stderr, "Error at %s:%d\n", __FILE__, __LINE__); \
        exit(1);                                                 \
    }

timespec diff(timespec start, timespec end) {
    timespec temp;
    if ((end.tv_nsec - start.tv_nsec) < 0) {
        temp.tv_sec = end.tv_sec - start.tv_sec - 1;
        temp.tv_nsec = 1000000000 + end.tv_nsec - start.tv_nsec;
    } else {
        temp.tv_sec = end.tv_sec - start.tv_sec;
        temp.tv_nsec = end.tv_nsec - start.tv_nsec;
    }

    return temp;
}

int main(int argc, char* argv[]) {
    timespec time1, time2;

    if (argc != 2) {
        fprintf(stderr, "minimal <tflite model>\n");
        return 1;
    }
    const char* filename = argv[1];

    // Load model
    std::unique_ptr<tflite::FlatBufferModel> model =
        tflite::FlatBufferModel::BuildFromFile(filename);
    TFLITE_MINIMAL_CHECK(model != nullptr);

    // Build the interpreter
    tflite::ops::builtin::BuiltinOpResolver resolver;
    InterpreterBuilder builder(*model, resolver);
    std::unique_ptr<Interpreter> interpreter;
    builder(&interpreter);
    TFLITE_MINIMAL_CHECK(interpreter != nullptr);

    // Allocate tensor buffers.
    TFLITE_MINIMAL_CHECK(interpreter->AllocateTensors() == kTfLiteOk);

    // Input tensor is 21
    // Output tensor is 0

    // Fill input buffers with black pixels
    float* input = interpreter->typed_input_tensor<float>(21);
    for (int i = 0; i < 784; ++i) {
        input[i] = 0.0f;
    }

    // Run inference and measure time
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &time1);
    TFLITE_MINIMAL_CHECK(interpreter->Invoke() == kTfLiteOk);
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &time2);

    // Read output buffers
    // Expected output is 0.1 for each output neuron
    float *output = interpreter->typed_output_tensor<float>(0);
    printf("Output: [");
    for (int i = 0; i < 10; ++i) {
        printf(" %.5f", output[i]);
    }
    printf("]\n");
    printf("Inference time: %ld s %ld ns\n", diff(time1, time2).tv_sec, diff(time1, time2).tv_nsec);

    return 0;
}
