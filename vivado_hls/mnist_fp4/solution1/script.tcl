############################################################
## This file is generated automatically by Vivado HLS.
## Please DO NOT edit it.
## Copyright (C) 1986-2018 Xilinx, Inc. All Rights Reserved.
############################################################
open_project mnist_fp4
set_top mnist_fp4
add_files mnist_fp4/src/mnist_fp4.cpp
add_files mnist_fp4/src/w_conv1.h
add_files mnist_fp4/src/w_conv2.h
add_files mnist_fp4/src/w_conv3.h
add_files mnist_fp4/src/w_conv4.h
add_files mnist_fp4/src/w_conv5.h
add_files mnist_fp4/src/w_conv6.h
add_files mnist_fp4/src/w_dense_1.h
open_solution "solution1"
set_part {xc7z020clg400-1} -tool vivado
create_clock -period 10 -name default
#source "./mnist_fp4/solution1/directives.tcl"
#csim_design
csynth_design
#cosim_design
export_design -format ip_catalog
