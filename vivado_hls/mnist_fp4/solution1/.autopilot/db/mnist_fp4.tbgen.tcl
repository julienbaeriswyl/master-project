set C_TypeInfoList {{ 
"mnist_fp4" : [[], { "return": [[], "void"]} , [{"ExternC" : 0}], [ {"stream_in": [[], {"reference": "0"}] }, {"w_conv1": [[], {"array": [ {"array": [ {"scalar": "float"}, [1,3,3]]}, [4]]}] }, {"w_conv2": [[], {"array": [ {"array": [ {"scalar": "float"}, [4,3,3]]}, [4]]}] }, {"w_conv3": [[], {"array": [ {"array": [ {"scalar": "float"}, [4,3,3]]}, [8]]}] }, {"w_conv4": [[], {"array": [ {"array": [ {"scalar": "float"}, [8,3,3]]}, [8]]}] }, {"w_conv5": [[], {"array": [ {"array": [ {"scalar": "float"}, [8,3,3]]}, [16]]}] }, {"w_conv6": [[], {"array": [ {"array": [ {"scalar": "float"}, [16,3,3]]}, [16]]}] }, {"w_dense_1": [[], {"array": [ {"array": [ {"scalar": "float"}, [10]]}, [16]]}] }, {"result": [[], {"reference": "1"}] }],[],""], 
"1": [ "ap_uint<4>", {"hls_type": {"ap_uint": [[[[], {"scalar": { "int": 4}}]],""]}}], 
"0": [ "stream_t", {"typedef": [[[],"2"],""]}], 
"2": [ "stream<ap_axiu<32, 1, 1, 1> >", {"hls_type": {"stream": [[[[],"3"]],"4"]}}], 
"3": [ "ap_axiu<32, 1, 1, 1>", {"struct": [[],[{"D":[[], {"scalar": { "int": 32}}]},{"U":[[], {"scalar": { "int": 1}}]},{"TI":[[], {"scalar": { "int": 1}}]},{"TD":[[], {"scalar": { "int": 1}}]}],[{ "data": [[], "5"]},{ "keep": [[], "1"]},{ "strb": [[], "1"]},{ "user": [[], "6"]},{ "last": [[], "6"]},{ "id": [[], "6"]},{ "dest": [[], "6"]}],""]}], 
"6": [ "ap_uint<1>", {"hls_type": {"ap_uint": [[[[], {"scalar": { "int": 1}}]],""]}}], 
"5": [ "ap_uint<32>", {"hls_type": {"ap_uint": [[[[], {"scalar": { "int": 32}}]],""]}}],
"4": ["hls", ""]
}}
set moduleName mnist_fp4
set isTaskLevelControl 1
set isCombinational 0
set isDatapathOnly 0
set isPipelined 0
set pipeline_type none
set FunctionProtocol ap_ctrl_hs
set isOneStateSeq 0
set ProfileFlag 0
set StallSigGenFlag 0
set isEnableWaveformDebug 1
set C_modelName {mnist_fp4}
set C_modelType { void 0 }
set C_modelArgList {
	{ stream_in_V_data_V int 32 regular {axi_s 0 volatile  { stream_in Data } }  }
	{ stream_in_V_keep_V int 4 regular {axi_s 0 volatile  { stream_in Keep } }  }
	{ stream_in_V_strb_V int 4 regular {axi_s 0 volatile  { stream_in Strb } }  }
	{ stream_in_V_user_V int 1 regular {axi_s 0 volatile  { stream_in User } }  }
	{ stream_in_V_last_V int 1 regular {axi_s 0 volatile  { stream_in Last } }  }
	{ stream_in_V_id_V int 1 regular {axi_s 0 volatile  { stream_in ID } }  }
	{ stream_in_V_dest_V int 1 regular {axi_s 0 volatile  { stream_in Dest } }  }
	{ w_conv1 float 32 regular {array 36 { 1 3 } 1 1 }  }
	{ w_conv2 float 32 regular {array 144 { 1 3 } 1 1 }  }
	{ w_conv3 float 32 regular {array 288 { 1 3 } 1 1 }  }
	{ w_conv4 float 32 regular {array 576 { 1 3 } 1 1 }  }
	{ w_conv5 float 32 unused {array 1152 { } 0 1 }  }
	{ w_conv6 float 32 unused {array 2304 { } 0 1 }  }
	{ w_dense_1 float 32 unused {array 160 { } 0 1 }  }
	{ result_V int 4 unused {pointer 0}  }
}
set C_modelArgMapList {[ 
	{ "Name" : "stream_in_V_data_V", "interface" : "axis", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "stream_in.V.data.V","cData": "uint32","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "stream_in_V_keep_V", "interface" : "axis", "bitwidth" : 4, "direction" : "READONLY", "bitSlice":[{"low":0,"up":3,"cElement": [{"cName": "stream_in.V.keep.V","cData": "uint4","bit_use": { "low": 0,"up": 3},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "stream_in_V_strb_V", "interface" : "axis", "bitwidth" : 4, "direction" : "READONLY", "bitSlice":[{"low":0,"up":3,"cElement": [{"cName": "stream_in.V.strb.V","cData": "uint4","bit_use": { "low": 0,"up": 3},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "stream_in_V_user_V", "interface" : "axis", "bitwidth" : 1, "direction" : "READONLY", "bitSlice":[{"low":0,"up":0,"cElement": [{"cName": "stream_in.V.user.V","cData": "uint1","bit_use": { "low": 0,"up": 0},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "stream_in_V_last_V", "interface" : "axis", "bitwidth" : 1, "direction" : "READONLY", "bitSlice":[{"low":0,"up":0,"cElement": [{"cName": "stream_in.V.last.V","cData": "uint1","bit_use": { "low": 0,"up": 0},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "stream_in_V_id_V", "interface" : "axis", "bitwidth" : 1, "direction" : "READONLY", "bitSlice":[{"low":0,"up":0,"cElement": [{"cName": "stream_in.V.id.V","cData": "uint1","bit_use": { "low": 0,"up": 0},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "stream_in_V_dest_V", "interface" : "axis", "bitwidth" : 1, "direction" : "READONLY", "bitSlice":[{"low":0,"up":0,"cElement": [{"cName": "stream_in.V.dest.V","cData": "uint1","bit_use": { "low": 0,"up": 0},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "w_conv1", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "w_conv1","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 3,"step" : 1},{"low" : 0,"up" : 0,"step" : 1},{"low" : 0,"up" : 2,"step" : 1},{"low" : 0,"up" : 2,"step" : 1}]}]}]} , 
 	{ "Name" : "w_conv2", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "w_conv2","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 3,"step" : 1},{"low" : 0,"up" : 3,"step" : 1},{"low" : 0,"up" : 2,"step" : 1},{"low" : 0,"up" : 2,"step" : 1}]}]}]} , 
 	{ "Name" : "w_conv3", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "w_conv3","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 7,"step" : 1},{"low" : 0,"up" : 3,"step" : 1},{"low" : 0,"up" : 2,"step" : 1},{"low" : 0,"up" : 2,"step" : 1}]}]}]} , 
 	{ "Name" : "w_conv4", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "w_conv4","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 7,"step" : 1},{"low" : 0,"up" : 7,"step" : 1},{"low" : 0,"up" : 2,"step" : 1},{"low" : 0,"up" : 2,"step" : 1}]}]}]} , 
 	{ "Name" : "w_conv5", "interface" : "memory", "bitwidth" : 32, "direction" : "NONE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "w_conv5","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 15,"step" : 1},{"low" : 0,"up" : 7,"step" : 1},{"low" : 0,"up" : 2,"step" : 1},{"low" : 0,"up" : 2,"step" : 1}]}]}]} , 
 	{ "Name" : "w_conv6", "interface" : "memory", "bitwidth" : 32, "direction" : "NONE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "w_conv6","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 15,"step" : 1},{"low" : 0,"up" : 15,"step" : 1},{"low" : 0,"up" : 2,"step" : 1},{"low" : 0,"up" : 2,"step" : 1}]}]}]} , 
 	{ "Name" : "w_dense_1", "interface" : "memory", "bitwidth" : 32, "direction" : "NONE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "w_dense_1","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 15,"step" : 1},{"low" : 0,"up" : 9,"step" : 1}]}]}]} , 
 	{ "Name" : "result_V", "interface" : "wire", "bitwidth" : 4, "direction" : "READONLY", "bitSlice":[{"low":0,"up":3,"cElement": [{"cName": "result.V","cData": "uint4","bit_use": { "low": 0,"up": 3},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} ]}
# RTL Port declarations: 
set portNum 59
set portList { 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst_n sc_in sc_logic 1 reset -1 active_low_sync } 
	{ ap_start sc_in sc_logic 1 start -1 } 
	{ ap_done sc_out sc_logic 1 predone -1 } 
	{ ap_idle sc_out sc_logic 1 done -1 } 
	{ ap_ready sc_out sc_logic 1 ready -1 } 
	{ stream_in_TDATA sc_in sc_lv 32 signal 0 } 
	{ stream_in_TVALID sc_in sc_logic 1 invld 6 } 
	{ stream_in_TREADY sc_out sc_logic 1 inacc 6 } 
	{ stream_in_TKEEP sc_in sc_lv 4 signal 1 } 
	{ stream_in_TSTRB sc_in sc_lv 4 signal 2 } 
	{ stream_in_TUSER sc_in sc_lv 1 signal 3 } 
	{ stream_in_TLAST sc_in sc_lv 1 signal 4 } 
	{ stream_in_TID sc_in sc_lv 1 signal 5 } 
	{ stream_in_TDEST sc_in sc_lv 1 signal 6 } 
	{ w_conv1_address0 sc_out sc_lv 6 signal 7 } 
	{ w_conv1_ce0 sc_out sc_logic 1 signal 7 } 
	{ w_conv1_q0 sc_in sc_lv 32 signal 7 } 
	{ w_conv2_address0 sc_out sc_lv 8 signal 8 } 
	{ w_conv2_ce0 sc_out sc_logic 1 signal 8 } 
	{ w_conv2_q0 sc_in sc_lv 32 signal 8 } 
	{ w_conv3_address0 sc_out sc_lv 9 signal 9 } 
	{ w_conv3_ce0 sc_out sc_logic 1 signal 9 } 
	{ w_conv3_q0 sc_in sc_lv 32 signal 9 } 
	{ w_conv4_address0 sc_out sc_lv 10 signal 10 } 
	{ w_conv4_ce0 sc_out sc_logic 1 signal 10 } 
	{ w_conv4_q0 sc_in sc_lv 32 signal 10 } 
	{ w_conv5_address0 sc_out sc_lv 11 signal 11 } 
	{ w_conv5_ce0 sc_out sc_logic 1 signal 11 } 
	{ w_conv5_we0 sc_out sc_logic 1 signal 11 } 
	{ w_conv5_d0 sc_out sc_lv 32 signal 11 } 
	{ w_conv5_q0 sc_in sc_lv 32 signal 11 } 
	{ w_conv5_address1 sc_out sc_lv 11 signal 11 } 
	{ w_conv5_ce1 sc_out sc_logic 1 signal 11 } 
	{ w_conv5_we1 sc_out sc_logic 1 signal 11 } 
	{ w_conv5_d1 sc_out sc_lv 32 signal 11 } 
	{ w_conv5_q1 sc_in sc_lv 32 signal 11 } 
	{ w_conv6_address0 sc_out sc_lv 12 signal 12 } 
	{ w_conv6_ce0 sc_out sc_logic 1 signal 12 } 
	{ w_conv6_we0 sc_out sc_logic 1 signal 12 } 
	{ w_conv6_d0 sc_out sc_lv 32 signal 12 } 
	{ w_conv6_q0 sc_in sc_lv 32 signal 12 } 
	{ w_conv6_address1 sc_out sc_lv 12 signal 12 } 
	{ w_conv6_ce1 sc_out sc_logic 1 signal 12 } 
	{ w_conv6_we1 sc_out sc_logic 1 signal 12 } 
	{ w_conv6_d1 sc_out sc_lv 32 signal 12 } 
	{ w_conv6_q1 sc_in sc_lv 32 signal 12 } 
	{ w_dense_1_address0 sc_out sc_lv 8 signal 13 } 
	{ w_dense_1_ce0 sc_out sc_logic 1 signal 13 } 
	{ w_dense_1_we0 sc_out sc_logic 1 signal 13 } 
	{ w_dense_1_d0 sc_out sc_lv 32 signal 13 } 
	{ w_dense_1_q0 sc_in sc_lv 32 signal 13 } 
	{ w_dense_1_address1 sc_out sc_lv 8 signal 13 } 
	{ w_dense_1_ce1 sc_out sc_logic 1 signal 13 } 
	{ w_dense_1_we1 sc_out sc_logic 1 signal 13 } 
	{ w_dense_1_d1 sc_out sc_lv 32 signal 13 } 
	{ w_dense_1_q1 sc_in sc_lv 32 signal 13 } 
	{ result_V sc_in sc_lv 4 signal 14 } 
	{ result_V_ap_vld sc_in sc_logic 1 invld 14 } 
}
set NewPortList {[ 
	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst_n", "role": "default" }} , 
 	{ "name": "ap_start", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "start", "bundle":{"name": "ap_start", "role": "default" }} , 
 	{ "name": "ap_done", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "predone", "bundle":{"name": "ap_done", "role": "default" }} , 
 	{ "name": "ap_idle", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "done", "bundle":{"name": "ap_idle", "role": "default" }} , 
 	{ "name": "ap_ready", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "ready", "bundle":{"name": "ap_ready", "role": "default" }} , 
 	{ "name": "stream_in_TDATA", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "stream_in_V_data_V", "role": "default" }} , 
 	{ "name": "stream_in_TVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "invld", "bundle":{"name": "stream_in_V_dest_V", "role": "default" }} , 
 	{ "name": "stream_in_TREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "inacc", "bundle":{"name": "stream_in_V_dest_V", "role": "default" }} , 
 	{ "name": "stream_in_TKEEP", "direction": "in", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "stream_in_V_keep_V", "role": "default" }} , 
 	{ "name": "stream_in_TSTRB", "direction": "in", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "stream_in_V_strb_V", "role": "default" }} , 
 	{ "name": "stream_in_TUSER", "direction": "in", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "stream_in_V_user_V", "role": "default" }} , 
 	{ "name": "stream_in_TLAST", "direction": "in", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "stream_in_V_last_V", "role": "default" }} , 
 	{ "name": "stream_in_TID", "direction": "in", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "stream_in_V_id_V", "role": "default" }} , 
 	{ "name": "stream_in_TDEST", "direction": "in", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "stream_in_V_dest_V", "role": "default" }} , 
 	{ "name": "w_conv1_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "w_conv1", "role": "address0" }} , 
 	{ "name": "w_conv1_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "w_conv1", "role": "ce0" }} , 
 	{ "name": "w_conv1_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "w_conv1", "role": "q0" }} , 
 	{ "name": "w_conv2_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "w_conv2", "role": "address0" }} , 
 	{ "name": "w_conv2_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "w_conv2", "role": "ce0" }} , 
 	{ "name": "w_conv2_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "w_conv2", "role": "q0" }} , 
 	{ "name": "w_conv3_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":9, "type": "signal", "bundle":{"name": "w_conv3", "role": "address0" }} , 
 	{ "name": "w_conv3_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "w_conv3", "role": "ce0" }} , 
 	{ "name": "w_conv3_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "w_conv3", "role": "q0" }} , 
 	{ "name": "w_conv4_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":10, "type": "signal", "bundle":{"name": "w_conv4", "role": "address0" }} , 
 	{ "name": "w_conv4_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "w_conv4", "role": "ce0" }} , 
 	{ "name": "w_conv4_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "w_conv4", "role": "q0" }} , 
 	{ "name": "w_conv5_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":11, "type": "signal", "bundle":{"name": "w_conv5", "role": "address0" }} , 
 	{ "name": "w_conv5_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "w_conv5", "role": "ce0" }} , 
 	{ "name": "w_conv5_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "w_conv5", "role": "we0" }} , 
 	{ "name": "w_conv5_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "w_conv5", "role": "d0" }} , 
 	{ "name": "w_conv5_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "w_conv5", "role": "q0" }} , 
 	{ "name": "w_conv5_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":11, "type": "signal", "bundle":{"name": "w_conv5", "role": "address1" }} , 
 	{ "name": "w_conv5_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "w_conv5", "role": "ce1" }} , 
 	{ "name": "w_conv5_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "w_conv5", "role": "we1" }} , 
 	{ "name": "w_conv5_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "w_conv5", "role": "d1" }} , 
 	{ "name": "w_conv5_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "w_conv5", "role": "q1" }} , 
 	{ "name": "w_conv6_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":12, "type": "signal", "bundle":{"name": "w_conv6", "role": "address0" }} , 
 	{ "name": "w_conv6_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "w_conv6", "role": "ce0" }} , 
 	{ "name": "w_conv6_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "w_conv6", "role": "we0" }} , 
 	{ "name": "w_conv6_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "w_conv6", "role": "d0" }} , 
 	{ "name": "w_conv6_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "w_conv6", "role": "q0" }} , 
 	{ "name": "w_conv6_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":12, "type": "signal", "bundle":{"name": "w_conv6", "role": "address1" }} , 
 	{ "name": "w_conv6_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "w_conv6", "role": "ce1" }} , 
 	{ "name": "w_conv6_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "w_conv6", "role": "we1" }} , 
 	{ "name": "w_conv6_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "w_conv6", "role": "d1" }} , 
 	{ "name": "w_conv6_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "w_conv6", "role": "q1" }} , 
 	{ "name": "w_dense_1_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "w_dense_1", "role": "address0" }} , 
 	{ "name": "w_dense_1_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "w_dense_1", "role": "ce0" }} , 
 	{ "name": "w_dense_1_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "w_dense_1", "role": "we0" }} , 
 	{ "name": "w_dense_1_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "w_dense_1", "role": "d0" }} , 
 	{ "name": "w_dense_1_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "w_dense_1", "role": "q0" }} , 
 	{ "name": "w_dense_1_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "w_dense_1", "role": "address1" }} , 
 	{ "name": "w_dense_1_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "w_dense_1", "role": "ce1" }} , 
 	{ "name": "w_dense_1_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "w_dense_1", "role": "we1" }} , 
 	{ "name": "w_dense_1_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "w_dense_1", "role": "d1" }} , 
 	{ "name": "w_dense_1_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "w_dense_1", "role": "q1" }} , 
 	{ "name": "result_V", "direction": "in", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "result_V", "role": "default" }} , 
 	{ "name": "result_V_ap_vld", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "invld", "bundle":{"name": "result_V", "role": "ap_vld" }}  ]}

set RtlHierarchyInfo {[
	{"ID" : "0", "Level" : "0", "Path" : "`AUTOTB_DUT_INST", "Parent" : "", "Child" : ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33"],
		"CDFG" : "mnist_fp4",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "0", "ap_idle" : "1",
		"Pipeline" : "None", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "0",
		"VariableLatency" : "1", "ExactLatency" : "-1", "EstimateLatencyMin" : "3018346", "EstimateLatencyMax" : "3211650",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "0",
		"HasNonBlockingOperation" : "0",
		"Port" : [
			{"Name" : "stream_in_V_data_V", "Type" : "Axis", "Direction" : "I",
				"BlockSignal" : [
					{"Name" : "stream_in_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "stream_in_V_keep_V", "Type" : "Axis", "Direction" : "I"},
			{"Name" : "stream_in_V_strb_V", "Type" : "Axis", "Direction" : "I"},
			{"Name" : "stream_in_V_user_V", "Type" : "Axis", "Direction" : "I"},
			{"Name" : "stream_in_V_last_V", "Type" : "Axis", "Direction" : "I"},
			{"Name" : "stream_in_V_id_V", "Type" : "Axis", "Direction" : "I"},
			{"Name" : "stream_in_V_dest_V", "Type" : "Axis", "Direction" : "I"},
			{"Name" : "w_conv1", "Type" : "Memory", "Direction" : "I"},
			{"Name" : "w_conv2", "Type" : "Memory", "Direction" : "I"},
			{"Name" : "w_conv3", "Type" : "Memory", "Direction" : "I"},
			{"Name" : "w_conv4", "Type" : "Memory", "Direction" : "I"},
			{"Name" : "w_conv5", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "w_conv6", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "w_dense_1", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "result_V", "Type" : "Vld", "Direction" : "I"}]},
	{"ID" : "1", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.image_0_0_U", "Parent" : "0"},
	{"ID" : "2", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.pad_temp_0_0_U", "Parent" : "0"},
	{"ID" : "3", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.conv1_0_U", "Parent" : "0"},
	{"ID" : "4", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.relu1_0_V_U", "Parent" : "0"},
	{"ID" : "5", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.pad_temp1_0_U", "Parent" : "0"},
	{"ID" : "6", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.conv2_0_U", "Parent" : "0"},
	{"ID" : "7", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.relu2_0_V_U", "Parent" : "0"},
	{"ID" : "8", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.pool1_0_U", "Parent" : "0"},
	{"ID" : "9", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.pad_temp2_0_U", "Parent" : "0"},
	{"ID" : "10", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.conv3_0_U", "Parent" : "0"},
	{"ID" : "11", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.relu3_0_V_U", "Parent" : "0"},
	{"ID" : "12", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.pad_temp3_0_U", "Parent" : "0"},
	{"ID" : "13", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.conv4_0_U", "Parent" : "0"},
	{"ID" : "14", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.relu4_0_V_U", "Parent" : "0"},
	{"ID" : "15", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.mnist_fp4_fadd_32fYi_U1", "Parent" : "0"},
	{"ID" : "16", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.mnist_fp4_fmul_32g8j_U2", "Parent" : "0"},
	{"ID" : "17", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.mnist_fp4_uitofp_hbi_U3", "Parent" : "0"},
	{"ID" : "18", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.mnist_fp4_fpext_3ibs_U4", "Parent" : "0"},
	{"ID" : "19", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.mnist_fp4_fpext_3ibs_U5", "Parent" : "0"},
	{"ID" : "20", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.mnist_fp4_fcmp_32jbC_U6", "Parent" : "0"},
	{"ID" : "21", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.mnist_fp4_urem_12kbM_U7", "Parent" : "0"},
	{"ID" : "22", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.mnist_fp4_urem_14lbW_U8", "Parent" : "0"},
	{"ID" : "23", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.mnist_fp4_urem_11mb6_U9", "Parent" : "0"},
	{"ID" : "24", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.mnist_fp4_urem_12ncg_U10", "Parent" : "0"},
	{"ID" : "25", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.mnist_fp4_mul_mulocq_U11", "Parent" : "0"},
	{"ID" : "26", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.mnist_fp4_mul_mulpcA_U12", "Parent" : "0"},
	{"ID" : "27", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.mnist_fp4_mul_mulpcA_U13", "Parent" : "0"},
	{"ID" : "28", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.mnist_fp4_mac_mulqcK_U14", "Parent" : "0"},
	{"ID" : "29", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.mnist_fp4_mul_mulocq_U15", "Parent" : "0"},
	{"ID" : "30", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.mnist_fp4_mul_mulocq_U16", "Parent" : "0"},
	{"ID" : "31", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.mnist_fp4_mul_mulrcU_U17", "Parent" : "0"},
	{"ID" : "32", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.mnist_fp4_mul_mulrcU_U18", "Parent" : "0"},
	{"ID" : "33", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.mnist_fp4_mac_mulqcK_U19", "Parent" : "0"}]}


set ArgLastReadFirstWriteLatency {
	mnist_fp4 {
		stream_in_V_data_V {Type I LastRead 7 FirstWrite -1}
		stream_in_V_keep_V {Type I LastRead 7 FirstWrite -1}
		stream_in_V_strb_V {Type I LastRead 7 FirstWrite -1}
		stream_in_V_user_V {Type I LastRead 7 FirstWrite -1}
		stream_in_V_last_V {Type I LastRead 7 FirstWrite -1}
		stream_in_V_id_V {Type I LastRead 7 FirstWrite -1}
		stream_in_V_dest_V {Type I LastRead 7 FirstWrite -1}
		w_conv1 {Type I LastRead 9 FirstWrite -1}
		w_conv2 {Type I LastRead 13 FirstWrite -1}
		w_conv3 {Type I LastRead 17 FirstWrite -1}
		w_conv4 {Type I LastRead 20 FirstWrite -1}
		w_conv5 {Type X LastRead -1 FirstWrite -1}
		w_conv6 {Type X LastRead -1 FirstWrite -1}
		w_dense_1 {Type X LastRead -1 FirstWrite -1}
		result_V {Type I LastRead -1 FirstWrite -1}}}

set hasDtUnsupportedChannel 0

set PerformanceInfo {[
	{"Name" : "Latency", "Min" : "3018346", "Max" : "3211650"}
	, {"Name" : "Interval", "Min" : "3018347", "Max" : "3211651"}
]}

set PipelineEnableSignalInfo {[
	{"Pipeline" : "1", "EnableSignal" : "ap_enable_pp1"}
	{"Pipeline" : "2", "EnableSignal" : "ap_enable_pp2"}
]}

set Spec2ImplPortList { 
	stream_in_V_data_V { axis {  { stream_in_TDATA in_data 0 32 } } }
	stream_in_V_keep_V { axis {  { stream_in_TKEEP in_data 0 4 } } }
	stream_in_V_strb_V { axis {  { stream_in_TSTRB in_data 0 4 } } }
	stream_in_V_user_V { axis {  { stream_in_TUSER in_data 0 1 } } }
	stream_in_V_last_V { axis {  { stream_in_TLAST in_data 0 1 } } }
	stream_in_V_id_V { axis {  { stream_in_TID in_data 0 1 } } }
	stream_in_V_dest_V { axis {  { stream_in_TVALID in_vld 0 1 }  { stream_in_TREADY in_acc 1 1 }  { stream_in_TDEST in_data 0 1 } } }
	w_conv1 { ap_memory {  { w_conv1_address0 mem_address 1 6 }  { w_conv1_ce0 mem_ce 1 1 }  { w_conv1_q0 mem_dout 0 32 } } }
	w_conv2 { ap_memory {  { w_conv2_address0 mem_address 1 8 }  { w_conv2_ce0 mem_ce 1 1 }  { w_conv2_q0 mem_dout 0 32 } } }
	w_conv3 { ap_memory {  { w_conv3_address0 mem_address 1 9 }  { w_conv3_ce0 mem_ce 1 1 }  { w_conv3_q0 mem_dout 0 32 } } }
	w_conv4 { ap_memory {  { w_conv4_address0 mem_address 1 10 }  { w_conv4_ce0 mem_ce 1 1 }  { w_conv4_q0 mem_dout 0 32 } } }
	w_conv5 { ap_memory {  { w_conv5_address0 mem_address 1 11 }  { w_conv5_ce0 mem_ce 1 1 }  { w_conv5_we0 mem_we 1 1 }  { w_conv5_d0 mem_din 1 32 }  { w_conv5_q0 mem_dout 0 32 }  { w_conv5_address1 mem_address 1 11 }  { w_conv5_ce1 mem_ce 1 1 }  { w_conv5_we1 mem_we 1 1 }  { w_conv5_d1 mem_din 1 32 }  { w_conv5_q1 mem_dout 0 32 } } }
	w_conv6 { ap_memory {  { w_conv6_address0 mem_address 1 12 }  { w_conv6_ce0 mem_ce 1 1 }  { w_conv6_we0 mem_we 1 1 }  { w_conv6_d0 mem_din 1 32 }  { w_conv6_q0 mem_dout 0 32 }  { w_conv6_address1 mem_address 1 12 }  { w_conv6_ce1 mem_ce 1 1 }  { w_conv6_we1 mem_we 1 1 }  { w_conv6_d1 mem_din 1 32 }  { w_conv6_q1 mem_dout 0 32 } } }
	w_dense_1 { ap_memory {  { w_dense_1_address0 mem_address 1 8 }  { w_dense_1_ce0 mem_ce 1 1 }  { w_dense_1_we0 mem_we 1 1 }  { w_dense_1_d0 mem_din 1 32 }  { w_dense_1_q0 mem_dout 0 32 }  { w_dense_1_address1 mem_address 1 8 }  { w_dense_1_ce1 mem_ce 1 1 }  { w_dense_1_we1 mem_we 1 1 }  { w_dense_1_d1 mem_din 1 32 }  { w_dense_1_q1 mem_dout 0 32 } } }
	result_V { ap_vld {  { result_V in_data 0 4 }  { result_V_ap_vld in_vld 0 1 } } }
}

set busDeadlockParameterList { 
}

# RTL port scheduling information:
set fifoSchedulingInfoList { 
}

# RTL bus port read request latency information:
set busReadReqLatencyList { 
}

# RTL bus port write response latency information:
set busWriteResLatencyList { 
}

# RTL array port load latency information:
set memoryLoadLatencyList { 
}
