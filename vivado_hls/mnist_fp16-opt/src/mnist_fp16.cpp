/**
 * Testing accuracy: 0.9728
 */

#include <ap_int.h>
#include <ap_fixed.h>
#include <float.h>
#include <math.h>

#include <ap_axi_sdata.h>
#include <hls_video.h>

#include "w_conv1.h"
#include "w_conv2.h"
#include "w_conv3.h"
#include "w_conv4.h"
#include "w_conv5.h"
#include "w_conv6.h"
#include "w_dense_1.h"


typedef ap_axiu<32,1,1,1> pixel_t;
typedef hls::stream<pixel_t> stream_t;

typedef union {
    unsigned int    u;
    float           f;
} union_t;


void mnist_fp16_opt(stream_t& stream_in, ap_uint<4>& result) {
#pragma HLS INTERFACE ap_ctrl_hs port=return
#pragma HLS INTERFACE axis port=stream_in
#pragma HLS INTERFACE ap_vld port=result

    float preds[1][10];
    float image[1][1][28][28];
    pixel_t pixel;
    union_t uni;
    HLS_SIZE_T i, j;

    bool sof = 0;
    loop_wait_sof: while (sof == 0) {
#pragma HLS LOOP_TRIPCOUNT avg=0 max=0
#pragma HLS PIPELINE II=1
        stream_in >> pixel;
        sof = pixel.user.to_int();
    }

    loop_height: for (i = 0; i < 28; ++i) {
        bool eol = 0;

        loop_width: for (j = 0; j < 28; ++j) {
#pragma HLS LOOP_FLATTEN off
#pragma HLS PIPELINE II=1
            if (sof || eol) {
                sof = 0;
                eol = pixel.last.to_int();
            }
            else {
                stream_in >> pixel;
                eol = pixel.last.to_int();
            }

            uni.u = pixel.data.to_uint();
            image[0][0][i][j] = uni.f;
        }

        loop_wait_eol: while (eol == 0) {
#pragma HLS PIPELINE II=1
#pragma HLS LOOP_TRIPCOUNT avg=0 max=0
            stream_in >> pixel;
            eol = pixel.last.to_int();
        }
    }


    float pad_temp[1][1][30][30];
    pad1_h: for (ap_int<32> index_tuple = 0; index_tuple < 30; ++index_tuple) {
#pragma HLS PIPELINE
        pad1_w: for (ap_int<32> i = 0; i < 30; ++i) {
#pragma HLS PIPELINE
            pad_temp[0][0][index_tuple][i] = (((((1 <= index_tuple) && (index_tuple < 29)) && (1 <= i)) && (i < 29)) ? image[((((i - ((i + -1) % 28)) + (index_tuple * 28)) + -29) / 784)][0][(((((i - ((i + -1) % 28)) + (index_tuple * 28)) + -29) / 28) % 28)][((i + -1) % 28)] : 0.000000e+00f);
        }
    }
    float conv1[1][4][28][28];
    conv1_c: for (ap_int<32> ff = 0; ff < 4; ++ff) {
        conv1_h: for (ap_int<32> yy = 0; yy < 28; ++yy) {
            conv1_w: for (ap_int<32> xx = 0; xx < 28; ++xx) {
                float reducer84;
                reducer84 = 0.000000e+00f;
                conv1_kh: for (ap_int<32> ry = 0; ry < 3; ++ry) {
                    conv1_kw: for (ap_int<32> rx = 0; rx < 3; ++rx) {
#pragma HLS PIPELINE
                        reducer84 = ((pad_temp[0][0][(yy + ry)][(xx + rx)] * w_conv1[ff][0][ry][rx]) + reducer84);
                    }
                }
                conv1[0][ff][yy][xx] = reducer84;
            }
        }
    }
    ap_fixed<16, 2> relu1[1][4][28][28];
    relu1_n: for (ap_int<32> args = 0; args < 1; ++args) {
        relu1_c: for (ap_int<32> args0 = 0; args0 < 4; ++args0) {
#pragma HLS PIPELINE
            relu1_h: for (ap_int<32> args1 = 0; args1 < 28; ++args1) {
#pragma HLS PIPELINE
                relu1_w: for (ap_int<32> args2 = 0; args2 < 28; ++args2) {
#pragma HLS PIPELINE
                    relu1[args][args0][args1][args2] = ((ap_fixed<16, 2>)((conv1[args][args0][args1][args2] < 0.000000e+00f) ? 0.000000e+00f : conv1[args][args0][args1][args2]));
                }
            }
        }
    }


    float pad_temp1[1][4][30][30];
    pad2_c: for (ap_int<32> not_zero = 0; not_zero < 4; ++not_zero) {
// #pragma HLS PIPELINE
        for (ap_int<32> index_tuple1 = 0; index_tuple1 < 30; ++index_tuple1) {
            for (ap_int<32> i1 = 0; i1 < 30; ++i1) {
#pragma HLS PIPELINE
                pad_temp1[0][not_zero][index_tuple1][i1] = (((((1 <= index_tuple1) && (index_tuple1 < 29)) && (1 <= i1)) && (i1 < 29)) ? ((float)relu1[(((((i1 - ((i1 + -1) % 28)) + (index_tuple1 * 28)) + (not_zero * 784)) + -29) / 3136)][((((((i1 - ((i1 + -1) % 28)) + (index_tuple1 * 28)) + (not_zero * 784)) + -29) / 784) % 4)][((((((i1 - ((i1 + -1) % 28)) + (index_tuple1 * 28)) + (not_zero * 784)) + -29) / 28) % 28)][((i1 + -1) % 28)]) : 0.000000e+00f);
            }
        }
    }
    float conv2[1][4][28][28];
    conv2_c: for (ap_int<32> ff1 = 0; ff1 < 4; ++ff1) {
        for (ap_int<32> yy1 = 0; yy1 < 28; ++yy1) {
            for (ap_int<32> xx1 = 0; xx1 < 28; ++xx1) {
                ap_fixed<16, 2> reducer85;
                reducer85 = ((ap_fixed<16, 2>)0);
                for (ap_int<32> rc = 0; rc < 4; ++rc) {
                    for (ap_int<32> ry1 = 0; ry1 < 3; ++ry1) {
                        for (ap_int<32> rx1 = 0; rx1 < 3; ++rx1) {
#pragma HLS PIPELINE
                            reducer85 = ((ap_fixed<16, 2>)(((ap_fixed<33, 5>)(((ap_fixed<32, 18>)((ap_fixed<16, 2>)pad_temp1[0][rc][(yy1 + ry1)][(xx1 + rx1)])) * ((ap_fixed<32, 18>)((ap_fixed<16, 2>)w_conv2[ff1][rc][ry1][rx1])))) + ((ap_fixed<33, 5>)reducer85)));
                        }
                    }
                }
                conv2[0][ff1][yy1][xx1] = ((float)reducer85);
            }
        }
    }
    ap_fixed<16, 2> relu2[1][4][28][28];
    relu2_n: for (ap_int<32> args3 = 0; args3 < 1; ++args3) {
        relu2_c: for (ap_int<32> args01 = 0; args01 < 4; ++args01) {
            for (ap_int<32> args11 = 0; args11 < 28; ++args11) {
#pragma HLS PIPELINE
                for (ap_int<32> args21 = 0; args21 < 28; ++args21) {
#pragma HLS PIPELINE
                    relu2[args3][args01][args11][args21] = ((ap_fixed<16, 2>)((conv2[args3][args01][args11][args21] < 0.000000e+00f) ? 0.000000e+00f : conv2[args3][args01][args11][args21]));
                }
            }
        }
    }
    float pool1[1][4][14][14];
    pool1_n: for (ap_int<32> i2 = 0; i2 < 1; ++i2) {
        pool1_c: for (ap_int<32> c = 0; c < 4; ++c) {
            for (ap_int<32> h = 0; h < 14; ++h) {
                for (ap_int<32> w = 0; w < 14; ++w) {
                    float reducer86;
                    reducer86 = -1.000000e+00f;
                    for (ap_int<32> ra63 = 0; ra63 < 2; ++ra63) {
#pragma HLS PIPELINE
                        for (ap_int<32> ra64 = 0; ra64 < 2; ++ra64) {
#pragma HLS PIPELINE
                            reducer86 = std::max(((float)relu2[i2][c][((h * 2) + ra63)][((w * 2) + ra64)]), reducer86);
                        }
                    }
                    pool1[i2][c][h][w] = reducer86;
                }
            }
        }
    }


    float pad_temp2[1][4][16][16];
    pad3_c: for (ap_int<32> not_zero1 = 0; not_zero1 < 4; ++not_zero1) {
        for (ap_int<32> index_tuple2 = 0; index_tuple2 < 16; ++index_tuple2) {
#pragma HLS PIPELINE
            for (ap_int<32> i3 = 0; i3 < 16; ++i3) {
#pragma HLS PIPELINE
                pad_temp2[0][not_zero1][index_tuple2][i3] = (((((1 <= index_tuple2) && (index_tuple2 < 15)) && (1 <= i3)) && (i3 < 15)) ? pool1[(((((i3 - ((i3 + -1) % 14)) + (index_tuple2 * 14)) + (not_zero1 * 196)) + -15) / 784)][((((((i3 - ((i3 + -1) % 14)) + (index_tuple2 * 14)) + (not_zero1 * 196)) + -15) / 196) % 4)][((((((i3 - ((i3 + -1) % 14)) + (index_tuple2 * 14)) + (not_zero1 * 196)) + -15) / 14) % 14)][((i3 + -1) % 14)] : 0.000000e+00f);
            }
        }
    }
    float conv3[1][8][14][14];
    conv3_c: for (ap_int<32> ff2 = 0; ff2 < 8; ++ff2) {
        for (ap_int<32> yy2 = 0; yy2 < 14; ++yy2) {
            for (ap_int<32> xx2 = 0; xx2 < 14; ++xx2) {
                float reducer87;
                reducer87 = 0.000000e+00f;
                for (ap_int<32> rc1 = 0; rc1 < 4; ++rc1) {
                    for (ap_int<32> ry2 = 0; ry2 < 3; ++ry2) {
                        for (ap_int<32> rx2 = 0; rx2 < 3; ++rx2) {
#pragma HLS PIPELINE
                            reducer87 = ((pad_temp2[0][rc1][(yy2 + ry2)][(xx2 + rx2)] * w_conv3[ff2][rc1][ry2][rx2]) + reducer87);
                        }
                    }
                }
                conv3[0][ff2][yy2][xx2] = reducer87;
            }
        }
    }
    ap_fixed<16, 2> relu3[1][8][14][14];
    relu3_n: for (ap_int<32> args4 = 0; args4 < 1; ++args4) {
        relu3_c: for (ap_int<32> args02 = 0; args02 < 8; ++args02) {
            for (ap_int<32> args12 = 0; args12 < 14; ++args12) {
#pragma HLS PIPELINE
                for (ap_int<32> args22 = 0; args22 < 14; ++args22) {
#pragma HLS PIPELINE
                    relu3[args4][args02][args12][args22] = ((ap_fixed<16, 2>)((conv3[args4][args02][args12][args22] < 0.000000e+00f) ? 0.000000e+00f : conv3[args4][args02][args12][args22]));
                }
            }
        }
    }


    float pad_temp3[1][8][16][16];
    pad4_c: for (ap_int<32> not_zero2 = 0; not_zero2 < 8; ++not_zero2) {
        for (ap_int<32> index_tuple3 = 0; index_tuple3 < 16; ++index_tuple3) {
            for (ap_int<32> i4 = 0; i4 < 16; ++i4) {
#pragma HLS PIPELINE
                pad_temp3[0][not_zero2][index_tuple3][i4] = (((((1 <= index_tuple3) && (index_tuple3 < 15)) && (1 <= i4)) && (i4 < 15)) ? ((float)relu3[(((((i4 - ((i4 + -1) % 14)) + (index_tuple3 * 14)) + (not_zero2 * 196)) + -15) / 1568)][((((((i4 - ((i4 + -1) % 14)) + (index_tuple3 * 14)) + (not_zero2 * 196)) + -15) / 196) % 8)][((((((i4 - ((i4 + -1) % 14)) + (index_tuple3 * 14)) + (not_zero2 * 196)) + -15) / 14) % 14)][((i4 + -1) % 14)]) : 0.000000e+00f);
            }
        }
    }
    float conv4[1][8][14][14];
    conv4_c: for (ap_int<32> ff3 = 0; ff3 < 8; ++ff3) {
        for (ap_int<32> yy3 = 0; yy3 < 14; ++yy3) {
            for (ap_int<32> xx3 = 0; xx3 < 14; ++xx3) {
                ap_fixed<16, 2> reducer88;
                reducer88 = ((ap_fixed<16, 2>)0);
                for (ap_int<32> rc2 = 0; rc2 < 8; ++rc2) {
                    for (ap_int<32> ry3 = 0; ry3 < 3; ++ry3) {
                        for (ap_int<32> rx3 = 0; rx3 < 3; ++rx3) {
#pragma HLS PIPELINE
                            reducer88 = ((ap_fixed<16, 2>)(((ap_fixed<33, 5>)(((ap_fixed<32, 18>)((ap_fixed<16, 2>)pad_temp3[0][rc2][(yy3 + ry3)][(xx3 + rx3)])) * ((ap_fixed<32, 18>)((ap_fixed<16, 2>)w_conv4[ff3][rc2][ry3][rx3])))) + ((ap_fixed<33, 5>)reducer88)));
                        }
                    }
                }
                conv4[0][ff3][yy3][xx3] = ((float)reducer88);
            }
        }
    }
    ap_fixed<16, 2> relu4[1][8][14][14];
    relu4_n: for (ap_int<32> args5 = 0; args5 < 1; ++args5) {
        relu4_c: for (ap_int<32> args03 = 0; args03 < 8; ++args03) {
            for (ap_int<32> args13 = 0; args13 < 14; ++args13) {
#pragma HLS PIPELINE
                for (ap_int<32> args23 = 0; args23 < 14; ++args23) {
#pragma HLS PIPELINE
                    relu4[args5][args03][args13][args23] = ((ap_fixed<16, 2>)((conv4[args5][args03][args13][args23] < 0.000000e+00f) ? 0.000000e+00f : conv4[args5][args03][args13][args23]));
                }
            }
        }
    }
    float pool2[1][8][7][7];
    pool2_n: for (ap_int<32> i5 = 0; i5 < 1; ++i5) {
        pool2_c: for (ap_int<32> c1 = 0; c1 < 8; ++c1) {
            for (ap_int<32> h1 = 0; h1 < 7; ++h1) {
                for (ap_int<32> w1 = 0; w1 < 7; ++w1) {
                    float reducer89;
                    reducer89 = -1.000000e+00f;
                    for (ap_int<32> ra65 = 0; ra65 < 2; ++ra65) {
                        for (ap_int<32> ra66 = 0; ra66 < 2; ++ra66) {
#pragma HLS PIPELINE
                            reducer89 = std::max(((float)relu4[i5][c1][((h1 * 2) + ra65)][((w1 * 2) + ra66)]), reducer89);
                        }
                    }
                    pool2[i5][c1][h1][w1] = reducer89;
                }
            }
        }
    }


    float pad_temp4[1][8][9][9];
    pad5_c: for (ap_int<32> not_zero3 = 0; not_zero3 < 8; ++not_zero3) {
        for (ap_int<32> index_tuple4 = 0; index_tuple4 < 9; ++index_tuple4) {
#pragma HLS PIPELINE
            for (ap_int<32> i6 = 0; i6 < 9; ++i6) {
#pragma HLS PIPELINE
                pad_temp4[0][not_zero3][index_tuple4][i6] = (((((1 <= index_tuple4) && (index_tuple4 < 8)) && (1 <= i6)) && (i6 < 8)) ? pool2[(((((i6 - ((i6 + -1) % 7)) + (index_tuple4 * 7)) + (not_zero3 * 49)) + -8) / 392)][((((((i6 - ((i6 + -1) % 7)) + (index_tuple4 * 7)) + (not_zero3 * 49)) + -8) / 49) % 8)][((((((i6 - ((i6 + -1) % 7)) + (index_tuple4 * 7)) + (not_zero3 * 49)) + -8) / 7) % 7)][((i6 + -1) % 7)] : 0.000000e+00f);
            }
        }
    }
    float conv5[1][16][7][7];
    conv5_c: for (ap_int<32> ff4 = 0; ff4 < 16; ++ff4) {
        for (ap_int<32> yy4 = 0; yy4 < 7; ++yy4) {
            for (ap_int<32> xx4 = 0; xx4 < 7; ++xx4) {
                float reducer90;
                reducer90 = 0.000000e+00f;
                for (ap_int<32> rc3 = 0; rc3 < 8; ++rc3) {
                    for (ap_int<32> ry4 = 0; ry4 < 3; ++ry4) {
                        for (ap_int<32> rx4 = 0; rx4 < 3; ++rx4) {
#pragma HLS PIPELINE
                            reducer90 = ((pad_temp4[0][rc3][(yy4 + ry4)][(xx4 + rx4)] * w_conv5[ff4][rc3][ry4][rx4]) + reducer90);
                        }
                    }
                }
                conv5[0][ff4][yy4][xx4] = reducer90;
            }
        }
    }
    ap_fixed<16, 2> relu5[1][16][7][7];
    relu5_n: for (ap_int<32> args6 = 0; args6 < 1; ++args6) {
        relu5_c: for (ap_int<32> args04 = 0; args04 < 16; ++args04) {
            for (ap_int<32> args14 = 0; args14 < 7; ++args14) {
#pragma HLS PIPELINE
                for (ap_int<32> args24 = 0; args24 < 7; ++args24) {
#pragma HLS PIPELINE
                    relu5[args6][args04][args14][args24] = ((ap_fixed<16, 2>)((conv5[args6][args04][args14][args24] < 0.000000e+00f) ? 0.000000e+00f : conv5[args6][args04][args14][args24]));
                }
            }
        }
    }


    float pad_temp5[1][16][9][9];
    pad6_c: for (ap_int<32> not_zero4 = 0; not_zero4 < 16; ++not_zero4) {
        for (ap_int<32> index_tuple5 = 0; index_tuple5 < 9; ++index_tuple5) {
            for (ap_int<32> i7 = 0; i7 < 9; ++i7) {
#pragma HLS PIPELINE
                pad_temp5[0][not_zero4][index_tuple5][i7] = (((((1 <= index_tuple5) && (index_tuple5 < 8)) && (1 <= i7)) && (i7 < 8)) ? ((float)relu5[(((((i7 - ((i7 + -1) % 7)) + (index_tuple5 * 7)) + (not_zero4 * 49)) + -8) / 784)][((((((i7 - ((i7 + -1) % 7)) + (index_tuple5 * 7)) + (not_zero4 * 49)) + -8) / 49) % 16)][((((((i7 - ((i7 + -1) % 7)) + (index_tuple5 * 7)) + (not_zero4 * 49)) + -8) / 7) % 7)][((i7 + -1) % 7)]) : 0.000000e+00f);
            }
        }
    }
    float conv6[1][16][7][7];
    conv6_c: for (ap_int<32> ff5 = 0; ff5 < 16; ++ff5) {
        for (ap_int<32> yy5 = 0; yy5 < 7; ++yy5) {
            for (ap_int<32> xx5 = 0; xx5 < 7; ++xx5) {
                ap_fixed<16, 2> reducer91;
                reducer91 = ((ap_fixed<16, 2>)0);
                for (ap_int<32> rc4 = 0; rc4 < 16; ++rc4) {
                    for (ap_int<32> ry5 = 0; ry5 < 3; ++ry5) {
                        for (ap_int<32> rx5 = 0; rx5 < 3; ++rx5) {
#pragma HLS PIPELINE
                            reducer91 = ((ap_fixed<16, 2>)(((ap_fixed<33, 5>)(((ap_fixed<32, 18>)((ap_fixed<16, 2>)pad_temp5[0][rc4][(yy5 + ry5)][(xx5 + rx5)])) * ((ap_fixed<32, 18>)((ap_fixed<16, 2>)w_conv6[ff5][rc4][ry5][rx5])))) + ((ap_fixed<33, 5>)reducer91)));
                        }
                    }
                }
                conv6[0][ff5][yy5][xx5] = ((float)reducer91);
            }
        }
    }
    ap_fixed<16, 2> relu6[1][16][7][7];
    relu6_n: for (ap_int<32> args7 = 0; args7 < 1; ++args7) {
        relu6_c: for (ap_int<32> args05 = 0; args05 < 16; ++args05) {
            for (ap_int<32> args15 = 0; args15 < 7; ++args15) {
#pragma HLS PIPELINE
                for (ap_int<32> args25 = 0; args25 < 7; ++args25) {
#pragma HLS PIPELINE
                    relu6[args7][args05][args15][args25] = ((ap_fixed<16, 2>)((conv6[args7][args05][args15][args25] < 0.000000e+00f) ? 0.000000e+00f : conv6[args7][args05][args15][args25]));
                }
            }
        }
    }
    float max_pool[1][16][1][1];
    max_pool_n: for (ap_int<32> i8 = 0; i8 < 1; ++i8) {
        max_pool_c: for (ap_int<32> c2 = 0; c2 < 16; ++c2) {
            float reducer92;
            reducer92 = -1.000000e+00f;
            for (ap_int<32> ra67 = 0; ra67 < 7; ++ra67) {
#pragma HLS PIPELINE
                for (ap_int<32> ra68 = 0; ra68 < 7; ++ra68) {
#pragma HLS PIPELINE
                    reducer92 = std::max(((float)relu6[i8][c2][ra67][ra68]), reducer92);
                }
            }
            max_pool[i8][c2][0][0] = reducer92;
        }
    }
    ap_fixed<16, 2> pool3[1][16];
    max_pool_convert0: for (ap_int<32> i9 = 0; i9 < 1; ++i9) {
        max_pool_convert1: for (ap_int<32> c3 = 0; c3 < 16; ++c3) {
#pragma HLS PIPELINE
            pool3[i9][c3] = ((ap_fixed<16, 2>)max_pool[i9][c3][0][0]);
        }
    }


    float dense1[1][10];
    dense_n: for (ap_int<32> i10 = 0; i10 < 1; ++i10) {
        dense_c: for (ap_int<32> j = 0; j < 10; ++j) {
            float reducer93;
            reducer93 = 0.000000e+00f;
            for (ap_int<32> ra69 = 0; ra69 < 16; ++ra69) {
                reducer93 = ((((float)pool3[i10][ra69]) * w_dense_1[ra69][j]) + reducer93);
            }
            dense1[i10][j] = reducer93;
        }
    }

    float compute14;
    float reducer94;
    reducer94 = -1.000000e+00f;
    softmax_0: for (ap_int<32> ra70 = 0; ra70 < 10; ++ra70) {
#pragma HLS PIPELINE
        reducer94 = std::max(dense1[0][ra70], reducer94);
    }
    compute14 = reducer94;
    float compute15;
    float reducer95;
    reducer95 = 0.000000e+00f;
    softmax_1: for (ap_int<32> ra71 = 0; ra71 < 10; ++ra71) {
        reducer95 = ((float)(exp(((double)(dense1[0][ra71] - compute14))) + ((double)reducer95)));
    }
    compute15 = reducer95;
    float update7;
    softmax_2: for (ap_int<32> j1 = 0; j1 < 10; ++j1) {
#pragma HLS PIPELINE
        preds[0][j1] = ((float)(exp(((double)(dense1[0][j1] - compute14))) / ((double)compute15)));
    }

    ap_uint<4> index = 0;
    float pred = FLT_MIN;
    results: for (ap_uint<4> i = 0; i < 10; ++i) {
#pragma HLS PIPELINE
        if (preds[0][i] > pred) {
            index = i;
            pred = preds[0][i];
        }
    }

    result = index;
}

