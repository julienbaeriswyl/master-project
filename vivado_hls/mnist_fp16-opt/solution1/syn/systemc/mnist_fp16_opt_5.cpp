#include "mnist_fp16_opt.h"
#include "AESL_pkg.h"

using namespace std;

namespace ap_rtl {

void mnist_fp16_opt::thread_conv6_0_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state365.read()) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp22_stage0.read()) && 
          esl_seteq<1,1,1>(ap_block_pp22_stage0_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp22_iter1.read())))) {
        conv6_0_ce0 = ap_const_logic_1;
    } else {
        conv6_0_ce0 = ap_const_logic_0;
    }
}

void mnist_fp16_opt::thread_conv6_0_d0() {
    conv6_0_d0 = (!tmp_419_reg_27554.read()[0].is_01())? sc_lv<32>(): ((tmp_419_reg_27554.read()[0].to_bool())? ap_const_lv32_0: f_12_fu_20944_p1.read());
}

void mnist_fp16_opt::thread_conv6_0_load_to_int_fu_21136_p1() {
    conv6_0_load_to_int_fu_21136_p1 = conv6_0_load_reg_27628.read();
}

void mnist_fp16_opt::thread_conv6_0_we0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state365.read())) {
        conv6_0_we0 = ap_const_logic_1;
    } else {
        conv6_0_we0 = ap_const_logic_0;
    }
}

void mnist_fp16_opt::thread_dense1_0_address0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp26_stage0.read()) && 
         esl_seteq<1,1,1>(ap_enable_reg_pp26_iter0.read(), ap_const_logic_1) && 
         esl_seteq<1,1,1>(ap_block_pp26_stage0.read(), ap_const_boolean_0))) {
        dense1_0_address0 =  (sc_lv<4>) (tmp_523_fu_22589_p1.read());
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state418.read())) {
        dense1_0_address0 =  (sc_lv<4>) (tmp_496_fu_22572_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp25_stage0.read()) && 
                esl_seteq<1,1,1>(ap_enable_reg_pp25_iter0.read(), ap_const_logic_1) && 
                esl_seteq<1,1,1>(ap_block_pp25_stage0.read(), ap_const_boolean_0))) {
        dense1_0_address0 =  (sc_lv<4>) (tmp_462_fu_22463_p1.read());
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state395.read())) {
        dense1_0_address0 =  (sc_lv<4>) (tmp_464_reg_27912.read());
    } else {
        dense1_0_address0 =  (sc_lv<4>) ("XXXX");
    }
}

void mnist_fp16_opt::thread_dense1_0_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state418.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state395.read()) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp25_stage0.read()) && 
          esl_seteq<1,1,1>(ap_block_pp25_stage0_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_enable_reg_pp25_iter0.read(), ap_const_logic_1)) || 
         (esl_seteq<1,1,1>(ap_block_pp26_stage0_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp26_stage0.read()) && 
          esl_seteq<1,1,1>(ap_enable_reg_pp26_iter0.read(), ap_const_logic_1)))) {
        dense1_0_ce0 = ap_const_logic_1;
    } else {
        dense1_0_ce0 = ap_const_logic_0;
    }
}

void mnist_fp16_opt::thread_dense1_0_we0() {
    if ((esl_seteq<1,1,1>(exitcond35_fu_22189_p2.read(), ap_const_lv1_1) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state395.read()))) {
        dense1_0_we0 = ap_const_logic_1;
    } else {
        dense1_0_we0 = ap_const_logic_0;
    }
}

void mnist_fp16_opt::thread_exitcond10_fu_10407_p2() {
    exitcond10_fu_10407_p2 = (!p_33_reg_2921.read().is_01() || !ap_const_lv4_E.is_01())? sc_lv<1>(): sc_lv<1>(p_33_reg_2921.read() == ap_const_lv4_E);
}

void mnist_fp16_opt::thread_exitcond11_fu_11052_p2() {
    exitcond11_fu_11052_p2 = (!ap_phi_mux_p_20_phi_fu_3005_p4.read().is_01() || !ap_const_lv5_10.is_01())? sc_lv<1>(): sc_lv<1>(ap_phi_mux_p_20_phi_fu_3005_p4.read() == ap_const_lv5_10);
}

void mnist_fp16_opt::thread_exitcond12_fu_10624_p2() {
    exitcond12_fu_10624_p2 = (!p_43_reg_2968.read().is_01() || !ap_const_lv2_2.is_01())? sc_lv<1>(): sc_lv<1>(p_43_reg_2968.read() == ap_const_lv2_2);
}

void mnist_fp16_opt::thread_exitcond13_fu_12950_p2() {
    exitcond13_fu_12950_p2 = (!p_29_reg_3145.read().is_01() || !ap_const_lv4_E.is_01())? sc_lv<1>(): sc_lv<1>(p_29_reg_3145.read() == ap_const_lv4_E);
}

void mnist_fp16_opt::thread_exitcond13_mid_fu_7832_p2() {
    exitcond13_mid_fu_7832_p2 = (exitcond6_fu_7826_p2.read() & not_exitcond_flatten_1_fu_7820_p2.read());
}

void mnist_fp16_opt::thread_exitcond14_fu_13375_p2() {
    exitcond14_fu_13375_p2 = (!p_47_reg_3268.read().is_01() || !ap_const_lv4_E.is_01())? sc_lv<1>(): sc_lv<1>(p_47_reg_3268.read() == ap_const_lv4_E);
}

void mnist_fp16_opt::thread_exitcond15_fu_13123_p2() {
    exitcond15_fu_13123_p2 = (!ap_phi_mux_p_40_phi_fu_3204_p4.read().is_01() || !ap_const_lv2_3.is_01())? sc_lv<1>(): sc_lv<1>(ap_phi_mux_p_40_phi_fu_3204_p4.read() == ap_const_lv2_3);
}

void mnist_fp16_opt::thread_exitcond15_mid_fu_8623_p2() {
    exitcond15_mid_fu_8623_p2 = (exitcond7_fu_8617_p2.read() & not_exitcond_flatten_2_fu_8611_p2.read());
}

void mnist_fp16_opt::thread_exitcond16_fu_13944_p2() {
    exitcond16_fu_13944_p2 = (!p_35_reg_3323.read().is_01() || !ap_const_lv5_10.is_01())? sc_lv<1>(): sc_lv<1>(p_35_reg_3323.read() == ap_const_lv5_10);
}

void mnist_fp16_opt::thread_exitcond17_fu_14711_p2() {
    exitcond17_fu_14711_p2 = (!p_39_reg_3394.read().is_01() || !ap_const_lv4_E.is_01())? sc_lv<1>(): sc_lv<1>(p_39_reg_3394.read() == ap_const_lv4_E);
}

void mnist_fp16_opt::thread_exitcond17_mid_fu_7307_p2() {
    exitcond17_mid_fu_7307_p2 = (exitcond4_fu_7301_p2.read() & not_exitcond_flatten_fu_7295_p2.read());
}

void mnist_fp16_opt::thread_exitcond18_fu_15908_p2() {
    exitcond18_fu_15908_p2 = (!p_60_reg_3516.read().is_01() || !ap_const_lv4_E.is_01())? sc_lv<1>(): sc_lv<1>(p_60_reg_3516.read() == ap_const_lv4_E);
}

void mnist_fp16_opt::thread_exitcond19_fu_14884_p2() {
    exitcond19_fu_14884_p2 = (!p_51_reg_3461.read().is_01() || !ap_const_lv2_3.is_01())? sc_lv<1>(): sc_lv<1>(p_51_reg_3461.read() == ap_const_lv2_3);
}

void mnist_fp16_opt::thread_exitcond1_fu_4549_p2() {
    exitcond1_fu_4549_p2 = (!p_0_reg_2156.read().is_01() || !ap_const_lv5_1C.is_01())? sc_lv<1>(): sc_lv<1>(p_0_reg_2156.read() == ap_const_lv5_1C);
}

void mnist_fp16_opt::thread_exitcond20_fu_16453_p2() {
    exitcond20_fu_16453_p2 = (!p_65_reg_3573.read().is_01() || !ap_const_lv3_7.is_01())? sc_lv<1>(): sc_lv<1>(p_65_reg_3573.read() == ap_const_lv3_7);
}

void mnist_fp16_opt::thread_exitcond21_fu_17007_p2() {
    exitcond21_fu_17007_p2 = (!ap_phi_mux_p_46_phi_fu_3657_p4.read().is_01() || !ap_const_lv4_9.is_01())? sc_lv<1>(): sc_lv<1>(ap_phi_mux_p_46_phi_fu_3657_p4.read() == ap_const_lv4_9);
}

void mnist_fp16_opt::thread_exitcond22_fu_16579_p2() {
    exitcond22_fu_16579_p2 = (!p_75_reg_3620.read().is_01() || !ap_const_lv2_2.is_01())? sc_lv<1>(): sc_lv<1>(p_75_reg_3620.read() == ap_const_lv2_2);
}

void mnist_fp16_opt::thread_exitcond23_fu_18067_p2() {
    exitcond23_fu_18067_p2 = (!p_53_reg_3760.read().is_01() || !ap_const_lv3_7.is_01())? sc_lv<1>(): sc_lv<1>(p_53_reg_3760.read() == ap_const_lv3_7);
}

void mnist_fp16_opt::thread_exitcond24_fu_18529_p2() {
    exitcond24_fu_18529_p2 = (!p_78_reg_3883.read().is_01() || !ap_const_lv3_7.is_01())? sc_lv<1>(): sc_lv<1>(p_78_reg_3883.read() == ap_const_lv3_7);
}

void mnist_fp16_opt::thread_exitcond25_fu_18211_p2() {
    exitcond25_fu_18211_p2 = (!ap_phi_mux_p_66_phi_fu_3819_p4.read().is_01() || !ap_const_lv2_3.is_01())? sc_lv<1>(): sc_lv<1>(ap_phi_mux_p_66_phi_fu_3819_p4.read() == ap_const_lv2_3);
}

void mnist_fp16_opt::thread_exitcond26_fu_19076_p2() {
    exitcond26_fu_19076_p2 = (!p_62_reg_3938.read().is_01() || !ap_const_lv4_9.is_01())? sc_lv<1>(): sc_lv<1>(p_62_reg_3938.read() == ap_const_lv4_9);
}

void mnist_fp16_opt::thread_exitcond26_mid_fu_8873_p2() {
    exitcond26_mid_fu_8873_p2 = (exitcond9_fu_8867_p2.read() & not_exitcond_flatten_4_fu_8861_p2.read());
}

void mnist_fp16_opt::thread_exitcond27_fu_19798_p2() {
    exitcond27_fu_19798_p2 = (!p_64_reg_4009.read().is_01() || !ap_const_lv3_7.is_01())? sc_lv<1>(): sc_lv<1>(p_64_reg_4009.read() == ap_const_lv3_7);
}

void mnist_fp16_opt::thread_exitcond28_fu_21032_p2() {
    exitcond28_fu_21032_p2 = (!p_84_reg_4131.read().is_01() || !ap_const_lv3_7.is_01())? sc_lv<1>(): sc_lv<1>(p_84_reg_4131.read() == ap_const_lv3_7);
}

void mnist_fp16_opt::thread_exitcond29_fu_19942_p2() {
    exitcond29_fu_19942_p2 = (!p_72_reg_4076.read().is_01() || !ap_const_lv2_3.is_01())? sc_lv<1>(): sc_lv<1>(p_72_reg_4076.read() == ap_const_lv2_3);
}

void mnist_fp16_opt::thread_exitcond29_mid_fu_9944_p2() {
    exitcond29_mid_fu_9944_p2 = (exitcond8_fu_9938_p2.read() & not_exitcond_flatten_3_fu_9932_p2.read());
}

void mnist_fp16_opt::thread_exitcond30_fu_21453_p2() {
    exitcond30_fu_21453_p2 = (!p_80_reg_4142.read().is_01() || !ap_const_lv5_10.is_01())? sc_lv<1>(): sc_lv<1>(p_80_reg_4142.read() == ap_const_lv5_10);
}

void mnist_fp16_opt::thread_exitcond30_mid_fu_12956_p2() {
    exitcond30_mid_fu_12956_p2 = (exitcond13_fu_12950_p2.read() & not_exitcond_flatten_6_fu_12944_p2.read());
}

void mnist_fp16_opt::thread_exitcond31_fu_21881_p2() {
    exitcond31_fu_21881_p2 = (!p_82_reg_4199.read().is_01() || !ap_const_lv5_10.is_01())? sc_lv<1>(): sc_lv<1>(p_82_reg_4199.read() == ap_const_lv5_10);
}

void mnist_fp16_opt::thread_exitcond32_fu_21513_p2() {
    exitcond32_fu_21513_p2 = (!p_86_reg_4188.read().is_01() || !ap_const_lv3_7.is_01())? sc_lv<1>(): sc_lv<1>(p_86_reg_4188.read() == ap_const_lv3_7);
}

void mnist_fp16_opt::thread_exitcond33_fu_22169_p2() {
    exitcond33_fu_22169_p2 = (!p_85_reg_4210.read().is_01() || !ap_const_lv4_A.is_01())? sc_lv<1>(): sc_lv<1>(p_85_reg_4210.read() == ap_const_lv4_A);
}

void mnist_fp16_opt::thread_exitcond34_fu_22451_p2() {
    exitcond34_fu_22451_p2 = (!p_71_reg_4257.read().is_01() || !ap_const_lv4_A.is_01())? sc_lv<1>(): sc_lv<1>(p_71_reg_4257.read() == ap_const_lv4_A);
}

void mnist_fp16_opt::thread_exitcond34_mid_fu_10497_p2() {
    exitcond34_mid_fu_10497_p2 = (exitcond10_reg_24743.read() & not_exitcond_flatten_5_fu_10492_p2.read());
}

void mnist_fp16_opt::thread_exitcond35_fu_22189_p2() {
    exitcond35_fu_22189_p2 = (!p_87_reg_4221.read().is_01() || !ap_const_lv5_10.is_01())? sc_lv<1>(): sc_lv<1>(p_87_reg_4221.read() == ap_const_lv5_10);
}

void mnist_fp16_opt::thread_exitcond36_fu_22560_p2() {
    exitcond36_fu_22560_p2 = (!p_74_reg_4268.read().is_01() || !ap_const_lv4_A.is_01())? sc_lv<1>(): sc_lv<1>(p_74_reg_4268.read() == ap_const_lv4_A);
}

void mnist_fp16_opt::thread_exitcond36_mid_fu_13950_p2() {
    exitcond36_mid_fu_13950_p2 = (exitcond16_fu_13944_p2.read() & not_exitcond_flatten_9_fu_13932_p2.read());
}

void mnist_fp16_opt::thread_exitcond3_fu_6907_p2() {
    exitcond3_fu_6907_p2 = (!p_8_reg_2511.read().is_01() || !ap_const_lv5_1C.is_01())? sc_lv<1>(): sc_lv<1>(p_8_reg_2511.read() == ap_const_lv5_1C);
}

void mnist_fp16_opt::thread_exitcond40_mid_fu_14717_p2() {
    exitcond40_mid_fu_14717_p2 = (exitcond17_fu_14711_p2.read() & not_exitcond_flatten_10_fu_14705_p2.read());
}

void mnist_fp16_opt::thread_exitcond41_mid_fu_13129_p2() {
    exitcond41_mid_fu_13129_p2 = (exitcond15_fu_13123_p2.read() & not_exitcond_flatten_8_fu_13117_p2.read());
}

void mnist_fp16_opt::thread_exitcond48_mid_fu_13381_p2() {
    exitcond48_mid_fu_13381_p2 = (exitcond14_fu_13375_p2.read() & not_exitcond_flatten_7_fu_13369_p2.read());
}

void mnist_fp16_opt::thread_exitcond4_fu_7301_p2() {
    exitcond4_fu_7301_p2 = (!p_16_reg_2612.read().is_01() || !ap_const_lv5_1C.is_01())? sc_lv<1>(): sc_lv<1>(p_16_reg_2612.read() == ap_const_lv5_1C);
}

void mnist_fp16_opt::thread_exitcond52_mid_fu_14890_p2() {
    exitcond52_mid_fu_14890_p2 = (exitcond19_fu_14884_p2.read() & not_exitcond_flatten_12_fu_14878_p2.read());
}

void mnist_fp16_opt::thread_exitcond54_mid_fu_18073_p2() {
    exitcond54_mid_fu_18073_p2 = (exitcond23_fu_18067_p2.read() & not_exitcond_flatten_14_fu_18061_p2.read());
}

void mnist_fp16_opt::thread_exitcond5_fu_7069_p2() {
    exitcond5_fu_7069_p2 = (!ap_phi_mux_p_13_phi_fu_2548_p4.read().is_01() || !ap_const_lv2_3.is_01())? sc_lv<1>(): sc_lv<1>(ap_phi_mux_p_13_phi_fu_2548_p4.read() == ap_const_lv2_3);
}

void mnist_fp16_opt::thread_exitcond61_mid_fu_15914_p2() {
    exitcond61_mid_fu_15914_p2 = (exitcond18_fu_15908_p2.read() & not_exitcond_flatten_11_fu_15902_p2.read());
}

void mnist_fp16_opt::thread_exitcond63_mid_fu_19082_p2() {
    exitcond63_mid_fu_19082_p2 = (exitcond26_fu_19076_p2.read() & not_exitcond_flatten_17_fu_19056_p2.read());
}

void mnist_fp16_opt::thread_exitcond65_mid_fu_19804_p2() {
    exitcond65_mid_fu_19804_p2 = (exitcond27_fu_19798_p2.read() & not_exitcond_flatten_18_fu_19792_p2.read());
}

void mnist_fp16_opt::thread_exitcond66_mid_fu_16459_p2() {
    exitcond66_mid_fu_16459_p2 = (exitcond20_fu_16453_p2.read() & not_exitcond_flatten_13_fu_16448_p2.read());
}

void mnist_fp16_opt::thread_exitcond67_mid_fu_18217_p2() {
    exitcond67_mid_fu_18217_p2 = (exitcond25_fu_18211_p2.read() & not_exitcond_flatten_16_fu_18205_p2.read());
}

void mnist_fp16_opt::thread_exitcond6_fu_7826_p2() {
    exitcond6_fu_7826_p2 = (!p_12_reg_2667.read().is_01() || !ap_const_lv5_1E.is_01())? sc_lv<1>(): sc_lv<1>(p_12_reg_2667.read() == ap_const_lv5_1E);
}

void mnist_fp16_opt::thread_exitcond73_mid_fu_19948_p2() {
    exitcond73_mid_fu_19948_p2 = (exitcond29_fu_19942_p2.read() & not_exitcond_flatten_20_fu_19936_p2.read());
}

void mnist_fp16_opt::thread_exitcond77_mid_fu_18535_p2() {
    exitcond77_mid_fu_18535_p2 = (exitcond24_fu_18529_p2.read() & not_exitcond_flatten_15_fu_18523_p2.read());
}

void mnist_fp16_opt::thread_exitcond7_fu_8617_p2() {
    exitcond7_fu_8617_p2 = (!p_14_reg_2742.read().is_01() || !ap_const_lv5_1C.is_01())? sc_lv<1>(): sc_lv<1>(p_14_reg_2742.read() == ap_const_lv5_1C);
}

void mnist_fp16_opt::thread_exitcond83_mid_fu_21038_p2() {
    exitcond83_mid_fu_21038_p2 = (exitcond28_fu_21032_p2.read() & not_exitcond_flatten_19_fu_21026_p2.read());
}

void mnist_fp16_opt::thread_exitcond8_fu_9938_p2() {
    exitcond8_fu_9938_p2 = (!p_28_reg_2864.read().is_01() || !ap_const_lv5_1C.is_01())? sc_lv<1>(): sc_lv<1>(p_28_reg_2864.read() == ap_const_lv5_1C);
}

void mnist_fp16_opt::thread_exitcond9_fu_8867_p2() {
    exitcond9_fu_8867_p2 = (!p_25_reg_2809.read().is_01() || !ap_const_lv2_3.is_01())? sc_lv<1>(): sc_lv<1>(p_25_reg_2809.read() == ap_const_lv2_3);
}

void mnist_fp16_opt::thread_exitcond9_mid_fu_6913_p2() {
    exitcond9_mid_fu_6913_p2 = (exitcond3_fu_6907_p2.read() & not_exitcond_flatten_21_fu_6901_p2.read());
}

void mnist_fp16_opt::thread_exitcond_flatten10_fu_8756_p2() {
    exitcond_flatten10_fu_8756_p2 = (!indvar_flatten11_reg_2753.read().is_01() || !ap_const_lv6_24.is_01())? sc_lv<1>(): sc_lv<1>(indvar_flatten11_reg_2753.read() == ap_const_lv6_24);
}

void mnist_fp16_opt::thread_exitcond_flatten11_fu_9876_p2() {
    exitcond_flatten11_fu_9876_p2 = (!indvar_flatten10_reg_2842.read().is_01() || !ap_const_lv10_310.is_01())? sc_lv<1>(): sc_lv<1>(indvar_flatten10_reg_2842.read() == ap_const_lv10_310);
}

void mnist_fp16_opt::thread_exitcond_flatten12_fu_8774_p2() {
    exitcond_flatten12_fu_8774_p2 = (!indvar_flatten12_reg_2775.read().is_01() || !ap_const_lv4_9.is_01())? sc_lv<1>(): sc_lv<1>(indvar_flatten12_reg_2775.read() == ap_const_lv4_9);
}

void mnist_fp16_opt::thread_exitcond_flatten13_fu_10367_p2() {
    exitcond_flatten13_fu_10367_p2 = (!indvar_flatten13_reg_2875.read().is_01() || !ap_const_lv10_310.is_01())? sc_lv<1>(): sc_lv<1>(indvar_flatten13_reg_2875.read() == ap_const_lv10_310);
}

void mnist_fp16_opt::thread_exitcond_flatten14_fu_10385_p2() {
    exitcond_flatten14_fu_10385_p2 = (!indvar_flatten14_reg_2897.read().is_01() || !ap_const_lv8_C4.is_01())? sc_lv<1>(): sc_lv<1>(indvar_flatten14_reg_2897.read() == ap_const_lv8_C4);
}

void mnist_fp16_opt::thread_exitcond_flatten15_fu_11034_p2() {
    exitcond_flatten15_fu_11034_p2 = (!ap_phi_mux_indvar_flatten15_phi_fu_2983_p4.read().is_01() || !ap_const_lv7_40.is_01())? sc_lv<1>(): sc_lv<1>(ap_phi_mux_indvar_flatten15_phi_fu_2983_p4.read() == ap_const_lv7_40);
}

void mnist_fp16_opt::thread_exitcond_flatten16_fu_10606_p2() {
    exitcond_flatten16_fu_10606_p2 = (!indvar_flatten16_reg_2933.read().is_01() || !ap_const_lv3_4.is_01())? sc_lv<1>(): sc_lv<1>(indvar_flatten16_reg_2933.read() == ap_const_lv3_4);
}

void mnist_fp16_opt::thread_exitcond_flatten17_fu_12858_p2() {
    exitcond_flatten17_fu_12858_p2 = (!indvar_flatten17_reg_3100.read().is_01() || !ap_const_lv11_620.is_01())? sc_lv<1>(): sc_lv<1>(indvar_flatten17_reg_3100.read() == ap_const_lv11_620);
}

void mnist_fp16_opt::thread_exitcond_flatten18_fu_12876_p2() {
    exitcond_flatten18_fu_12876_p2 = (!indvar_flatten18_reg_3122.read().is_01() || !ap_const_lv8_C4.is_01())? sc_lv<1>(): sc_lv<1>(indvar_flatten18_reg_3122.read() == ap_const_lv8_C4);
}

void mnist_fp16_opt::thread_exitcond_flatten19_fu_13295_p2() {
    exitcond_flatten19_fu_13295_p2 = (!indvar_flatten19_reg_3224.read().is_01() || !ap_const_lv11_620.is_01())? sc_lv<1>(): sc_lv<1>(indvar_flatten19_reg_3224.read() == ap_const_lv11_620);
}

void mnist_fp16_opt::thread_exitcond_flatten1_fu_6875_p2() {
    exitcond_flatten1_fu_6875_p2 = (!indvar_flatten1_reg_2465.read().is_01() || !ap_const_lv12_C40.is_01())? sc_lv<1>(): sc_lv<1>(indvar_flatten1_reg_2465.read() == ap_const_lv12_C40);
}

void mnist_fp16_opt::thread_exitcond_flatten20_fu_13039_p2() {
    exitcond_flatten20_fu_13039_p2 = (!ap_phi_mux_indvar_flatten21_phi_fu_3160_p4.read().is_01() || !ap_const_lv6_24.is_01())? sc_lv<1>(): sc_lv<1>(ap_phi_mux_indvar_flatten21_phi_fu_3160_p4.read() == ap_const_lv6_24);
}

void mnist_fp16_opt::thread_exitcond_flatten21_fu_13313_p2() {
    exitcond_flatten21_fu_13313_p2 = (!indvar_flatten20_reg_3246.read().is_01() || !ap_const_lv8_C4.is_01())? sc_lv<1>(): sc_lv<1>(indvar_flatten20_reg_3246.read() == ap_const_lv8_C4);
}

void mnist_fp16_opt::thread_exitcond_flatten22_fu_13057_p2() {
    exitcond_flatten22_fu_13057_p2 = (!ap_phi_mux_indvar_flatten22_phi_fu_3182_p4.read().is_01() || !ap_const_lv4_9.is_01())? sc_lv<1>(): sc_lv<1>(ap_phi_mux_indvar_flatten22_phi_fu_3182_p4.read() == ap_const_lv4_9);
}

void mnist_fp16_opt::thread_exitcond_flatten23_fu_13874_p2() {
    exitcond_flatten23_fu_13874_p2 = (!indvar_flatten23_reg_3279.read().is_01() || !ap_const_lv12_800.is_01())? sc_lv<1>(): sc_lv<1>(indvar_flatten23_reg_3279.read() == ap_const_lv12_800);
}

void mnist_fp16_opt::thread_exitcond_flatten24_fu_13892_p2() {
    exitcond_flatten24_fu_13892_p2 = (!indvar_flatten24_reg_3301.read().is_01() || !ap_const_lv10_100.is_01())? sc_lv<1>(): sc_lv<1>(indvar_flatten24_reg_3301.read() == ap_const_lv10_100);
}

void mnist_fp16_opt::thread_exitcond_flatten25_fu_14619_p2() {
    exitcond_flatten25_fu_14619_p2 = (!indvar_flatten25_reg_3349.read().is_01() || !ap_const_lv11_620.is_01())? sc_lv<1>(): sc_lv<1>(indvar_flatten25_reg_3349.read() == ap_const_lv11_620);
}

void mnist_fp16_opt::thread_exitcond_flatten26_fu_14637_p2() {
    exitcond_flatten26_fu_14637_p2 = (!indvar_flatten26_reg_3371.read().is_01() || !ap_const_lv8_C4.is_01())? sc_lv<1>(): sc_lv<1>(indvar_flatten26_reg_3371.read() == ap_const_lv8_C4);
}

void mnist_fp16_opt::thread_exitcond_flatten27_fu_15828_p2() {
    exitcond_flatten27_fu_15828_p2 = (!indvar_flatten27_reg_3472.read().is_01() || !ap_const_lv11_620.is_01())? sc_lv<1>(): sc_lv<1>(indvar_flatten27_reg_3472.read() == ap_const_lv11_620);
}

void mnist_fp16_opt::thread_exitcond_flatten28_fu_14800_p2() {
    exitcond_flatten28_fu_14800_p2 = (!indvar_flatten29_reg_3405.read().is_01() || !ap_const_lv7_48.is_01())? sc_lv<1>(): sc_lv<1>(indvar_flatten29_reg_3405.read() == ap_const_lv7_48);
}

void mnist_fp16_opt::thread_exitcond_flatten29_fu_15846_p2() {
    exitcond_flatten29_fu_15846_p2 = (!indvar_flatten28_reg_3494.read().is_01() || !ap_const_lv8_C4.is_01())? sc_lv<1>(): sc_lv<1>(indvar_flatten28_reg_3494.read() == ap_const_lv8_C4);
}

void mnist_fp16_opt::thread_exitcond_flatten2_fu_6887_p2() {
    exitcond_flatten2_fu_6887_p2 = (!indvar_flatten7_reg_2488.read().is_01() || !ap_const_lv10_310.is_01())? sc_lv<1>(): sc_lv<1>(indvar_flatten7_reg_2488.read() == ap_const_lv10_310);
}

void mnist_fp16_opt::thread_exitcond_flatten30_fu_14818_p2() {
    exitcond_flatten30_fu_14818_p2 = (!indvar_flatten30_reg_3427.read().is_01() || !ap_const_lv4_9.is_01())? sc_lv<1>(): sc_lv<1>(indvar_flatten30_reg_3427.read() == ap_const_lv4_9);
}

void mnist_fp16_opt::thread_exitcond_flatten31_fu_16337_p2() {
    exitcond_flatten31_fu_16337_p2 = (!indvar_flatten31_reg_3527.read().is_01() || !ap_const_lv9_188.is_01())? sc_lv<1>(): sc_lv<1>(indvar_flatten31_reg_3527.read() == ap_const_lv9_188);
}

void mnist_fp16_opt::thread_exitcond_flatten32_fu_16355_p2() {
    exitcond_flatten32_fu_16355_p2 = (!indvar_flatten32_reg_3549.read().is_01() || !ap_const_lv6_31.is_01())? sc_lv<1>(): sc_lv<1>(indvar_flatten32_reg_3549.read() == ap_const_lv6_31);
}

void mnist_fp16_opt::thread_exitcond_flatten33_fu_16989_p2() {
    exitcond_flatten33_fu_16989_p2 = (!ap_phi_mux_indvar_flatten33_phi_fu_3635_p4.read().is_01() || !ap_const_lv7_48.is_01())? sc_lv<1>(): sc_lv<1>(ap_phi_mux_indvar_flatten33_phi_fu_3635_p4.read() == ap_const_lv7_48);
}

void mnist_fp16_opt::thread_exitcond_flatten34_fu_16561_p2() {
    exitcond_flatten34_fu_16561_p2 = (!indvar_flatten34_reg_3585.read().is_01() || !ap_const_lv3_4.is_01())? sc_lv<1>(): sc_lv<1>(indvar_flatten34_reg_3585.read() == ap_const_lv3_4);
}

void mnist_fp16_opt::thread_exitcond_flatten35_fu_17995_p2() {
    exitcond_flatten35_fu_17995_p2 = (!indvar_flatten35_reg_3715.read().is_01() || !ap_const_lv10_310.is_01())? sc_lv<1>(): sc_lv<1>(indvar_flatten35_reg_3715.read() == ap_const_lv10_310);
}

void mnist_fp16_opt::thread_exitcond_flatten36_fu_18013_p2() {
    exitcond_flatten36_fu_18013_p2 = (!indvar_flatten36_reg_3737.read().is_01() || !ap_const_lv6_31.is_01())? sc_lv<1>(): sc_lv<1>(indvar_flatten36_reg_3737.read() == ap_const_lv6_31);
}

void mnist_fp16_opt::thread_exitcond_flatten37_fu_18457_p2() {
    exitcond_flatten37_fu_18457_p2 = (!indvar_flatten37_reg_3839.read().is_01() || !ap_const_lv10_310.is_01())? sc_lv<1>(): sc_lv<1>(indvar_flatten37_reg_3839.read() == ap_const_lv10_310);
}

void mnist_fp16_opt::thread_exitcond_flatten38_fu_18156_p2() {
    exitcond_flatten38_fu_18156_p2 = (!ap_phi_mux_indvar_flatten39_phi_fu_3775_p4.read().is_01() || !ap_const_lv7_48.is_01())? sc_lv<1>(): sc_lv<1>(ap_phi_mux_indvar_flatten39_phi_fu_3775_p4.read() == ap_const_lv7_48);
}

void mnist_fp16_opt::thread_exitcond_flatten39_fu_18475_p2() {
    exitcond_flatten39_fu_18475_p2 = (!indvar_flatten38_reg_3861.read().is_01() || !ap_const_lv6_31.is_01())? sc_lv<1>(): sc_lv<1>(indvar_flatten38_reg_3861.read() == ap_const_lv6_31);
}

void mnist_fp16_opt::thread_exitcond_flatten3_fu_7221_p2() {
    exitcond_flatten3_fu_7221_p2 = (!indvar_flatten2_reg_2568.read().is_01() || !ap_const_lv12_C40.is_01())? sc_lv<1>(): sc_lv<1>(indvar_flatten2_reg_2568.read() == ap_const_lv12_C40);
}

void mnist_fp16_opt::thread_exitcond_flatten40_fu_18174_p2() {
    exitcond_flatten40_fu_18174_p2 = (!ap_phi_mux_indvar_flatten40_phi_fu_3797_p4.read().is_01() || !ap_const_lv4_9.is_01())? sc_lv<1>(): sc_lv<1>(ap_phi_mux_indvar_flatten40_phi_fu_3797_p4.read() == ap_const_lv4_9);
}

void mnist_fp16_opt::thread_exitcond_flatten41_fu_18998_p2() {
    exitcond_flatten41_fu_18998_p2 = (!indvar_flatten41_reg_3894.read().is_01() || !ap_const_lv11_510.is_01())? sc_lv<1>(): sc_lv<1>(indvar_flatten41_reg_3894.read() == ap_const_lv11_510);
}

void mnist_fp16_opt::thread_exitcond_flatten42_fu_19016_p2() {
    exitcond_flatten42_fu_19016_p2 = (!indvar_flatten42_reg_3916.read().is_01() || !ap_const_lv8_51.is_01())? sc_lv<1>(): sc_lv<1>(indvar_flatten42_reg_3916.read() == ap_const_lv8_51);
}

void mnist_fp16_opt::thread_exitcond_flatten43_fu_19714_p2() {
    exitcond_flatten43_fu_19714_p2 = (!indvar_flatten43_reg_3964.read().is_01() || !ap_const_lv10_310.is_01())? sc_lv<1>(): sc_lv<1>(indvar_flatten43_reg_3964.read() == ap_const_lv10_310);
}

void mnist_fp16_opt::thread_exitcond_flatten44_fu_19732_p2() {
    exitcond_flatten44_fu_19732_p2 = (!indvar_flatten44_reg_3986.read().is_01() || !ap_const_lv6_31.is_01())? sc_lv<1>(): sc_lv<1>(indvar_flatten44_reg_3986.read() == ap_const_lv6_31);
}

void mnist_fp16_opt::thread_exitcond_flatten45_fu_20960_p2() {
    exitcond_flatten45_fu_20960_p2 = (!indvar_flatten45_reg_4087.read().is_01() || !ap_const_lv10_310.is_01())? sc_lv<1>(): sc_lv<1>(indvar_flatten45_reg_4087.read() == ap_const_lv10_310);
}

void mnist_fp16_opt::thread_exitcond_flatten46_fu_19887_p2() {
    exitcond_flatten46_fu_19887_p2 = (!indvar_flatten47_reg_4020.read().is_01() || !ap_const_lv8_90.is_01())? sc_lv<1>(): sc_lv<1>(indvar_flatten47_reg_4020.read() == ap_const_lv8_90);
}

void mnist_fp16_opt::thread_exitcond_flatten47_fu_20978_p2() {
    exitcond_flatten47_fu_20978_p2 = (!indvar_flatten46_reg_4109.read().is_01() || !ap_const_lv6_31.is_01())? sc_lv<1>(): sc_lv<1>(indvar_flatten46_reg_4109.read() == ap_const_lv6_31);
}

void mnist_fp16_opt::thread_exitcond_flatten48_fu_19905_p2() {
    exitcond_flatten48_fu_19905_p2 = (!indvar_flatten48_reg_4042.read().is_01() || !ap_const_lv4_9.is_01())? sc_lv<1>(): sc_lv<1>(indvar_flatten48_reg_4042.read() == ap_const_lv4_9);
}

void mnist_fp16_opt::thread_exitcond_flatten49_fu_21495_p2() {
    exitcond_flatten49_fu_21495_p2 = (!indvar_flatten49_reg_4153.read().is_01() || !ap_const_lv6_31.is_01())? sc_lv<1>(): sc_lv<1>(indvar_flatten49_reg_4153.read() == ap_const_lv6_31);
}

void mnist_fp16_opt::thread_exitcond_flatten4_fu_7239_p2() {
    exitcond_flatten4_fu_7239_p2 = (!indvar_flatten3_reg_2590.read().is_01() || !ap_const_lv10_310.is_01())? sc_lv<1>(): sc_lv<1>(indvar_flatten3_reg_2590.read() == ap_const_lv10_310);
}

void mnist_fp16_opt::thread_exitcond_flatten5_fu_7780_p2() {
    exitcond_flatten5_fu_7780_p2 = (!indvar_flatten4_reg_2623.read().is_01() || !ap_const_lv12_E10.is_01())? sc_lv<1>(): sc_lv<1>(indvar_flatten4_reg_2623.read() == ap_const_lv12_E10);
}

void mnist_fp16_opt::thread_exitcond_flatten6_fu_7798_p2() {
    exitcond_flatten6_fu_7798_p2 = (!indvar_flatten5_reg_2645.read().is_01() || !ap_const_lv10_384.is_01())? sc_lv<1>(): sc_lv<1>(indvar_flatten5_reg_2645.read() == ap_const_lv10_384);
}

void mnist_fp16_opt::thread_exitcond_flatten7_fu_8585_p2() {
    exitcond_flatten7_fu_8585_p2 = (!indvar_flatten6_reg_2696.read().is_01() || !ap_const_lv12_C40.is_01())? sc_lv<1>(): sc_lv<1>(indvar_flatten6_reg_2696.read() == ap_const_lv12_C40);
}

void mnist_fp16_opt::thread_exitcond_flatten8_fu_8597_p2() {
    exitcond_flatten8_fu_8597_p2 = (!indvar_flatten8_reg_2719.read().is_01() || !ap_const_lv10_310.is_01())? sc_lv<1>(): sc_lv<1>(indvar_flatten8_reg_2719.read() == ap_const_lv10_310);
}

void mnist_fp16_opt::thread_exitcond_flatten9_fu_9858_p2() {
    exitcond_flatten9_fu_9858_p2 = (!indvar_flatten9_reg_2820.read().is_01() || !ap_const_lv12_C40.is_01())? sc_lv<1>(): sc_lv<1>(indvar_flatten9_reg_2820.read() == ap_const_lv12_C40);
}

void mnist_fp16_opt::thread_exitcond_flatten_fu_7051_p2() {
    exitcond_flatten_fu_7051_p2 = (!ap_phi_mux_indvar_flatten_phi_fu_2526_p4.read().is_01() || !ap_const_lv4_9.is_01())? sc_lv<1>(): sc_lv<1>(ap_phi_mux_indvar_flatten_phi_fu_2526_p4.read() == ap_const_lv4_9);
}

void mnist_fp16_opt::thread_exitcond_fu_22577_p2() {
    exitcond_fu_22577_p2 = (!p_76_reg_4291.read().is_01() || !ap_const_lv4_A.is_01())? sc_lv<1>(): sc_lv<1>(p_76_reg_4291.read() == ap_const_lv4_A);
}

void mnist_fp16_opt::thread_f_10_fu_19705_p1() {
    f_10_fu_19705_p1 = p_Result_81_fu_19694_p5.read();
}

void mnist_fp16_opt::thread_f_12_fu_20944_p1() {
    f_12_fu_20944_p1 = p_Result_86_fu_20933_p5.read();
}

void mnist_fp16_opt::thread_f_14_fu_21780_p1() {
    f_14_fu_21780_p1 = p_Result_58_fu_21769_p5.read();
}

void mnist_fp16_opt::thread_f_16_fu_22439_p1() {
    f_16_fu_22439_p1 = p_Result_62_fu_22428_p5.read();
}

void mnist_fp16_opt::thread_f_2_fu_9842_p1() {
    f_2_fu_9842_p1 = p_Result_70_fu_9831_p5.read();
}

void mnist_fp16_opt::thread_f_4_fu_10915_p1() {
    f_4_fu_10915_p1 = p_Result_17_fu_10904_p5.read();
}

void mnist_fp16_opt::thread_f_5_fu_14610_p1() {
    f_5_fu_14610_p1 = p_Result_73_fu_14599_p5.read();
}

void mnist_fp16_opt::thread_f_6_fu_15812_p1() {
    f_6_fu_15812_p1 = p_Result_78_fu_15801_p5.read();
}

void mnist_fp16_opt::thread_f_8_fu_16870_p1() {
    f_8_fu_16870_p1 = p_Result_36_fu_16859_p5.read();
}

void mnist_fp16_opt::thread_f_fu_8576_p1() {
    f_fu_8576_p1 = p_Result_65_fu_8565_p5.read();
}

void mnist_fp16_opt::thread_ff1_V_fu_8657_p2() {
    ff1_V_fu_8657_p2 = (!ap_const_lv3_1.is_01() || !p_9_reg_2707.read().is_01())? sc_lv<3>(): (sc_biguint<3>(ap_const_lv3_1) + sc_biguint<3>(p_9_reg_2707.read()));
}

void mnist_fp16_opt::thread_ff2_V_fu_12870_p2() {
    ff2_V_fu_12870_p2 = (!ap_const_lv4_1.is_01() || !p_18_reg_3111.read().is_01())? sc_lv<4>(): (sc_biguint<4>(ap_const_lv4_1) + sc_biguint<4>(p_18_reg_3111.read()));
}

void mnist_fp16_opt::thread_ff3_V_fu_14631_p2() {
    ff3_V_fu_14631_p2 = (!ap_const_lv4_1.is_01() || !p_30_reg_3360.read().is_01())? sc_lv<4>(): (sc_biguint<4>(ap_const_lv4_1) + sc_biguint<4>(p_30_reg_3360.read()));
}

void mnist_fp16_opt::thread_ff4_V_fu_18007_p2() {
    ff4_V_fu_18007_p2 = (!ap_const_lv5_1.is_01() || !p_45_reg_3726.read().is_01())? sc_lv<5>(): (sc_biguint<5>(ap_const_lv5_1) + sc_biguint<5>(p_45_reg_3726.read()));
}

void mnist_fp16_opt::thread_ff5_V_fu_19726_p2() {
    ff5_V_fu_19726_p2 = (!ap_const_lv5_1.is_01() || !p_56_reg_3975.read().is_01())? sc_lv<5>(): (sc_biguint<5>(ap_const_lv5_1) + sc_biguint<5>(p_56_reg_3975.read()));
}

void mnist_fp16_opt::thread_ff_V_fu_6947_p2() {
    ff_V_fu_6947_p2 = (!ap_const_lv3_1.is_01() || !p_2_reg_2476.read().is_01())? sc_lv<3>(): (sc_biguint<3>(ap_const_lv3_1) + sc_biguint<3>(p_2_reg_2476.read()));
}

void mnist_fp16_opt::thread_grp_fu_11261_p0() {
    grp_fu_11261_p0 = (!tmp_784_reg_24975.read()[0].is_01())? sc_lv<12>(): ((tmp_784_reg_24975.read()[0].to_bool())? neg_ti9_fu_11248_p2.read(): tmp_788_fu_11238_p1.read());
}

void mnist_fp16_opt::thread_grp_fu_11261_p1() {
    grp_fu_11261_p1 =  (sc_lv<5>) (ap_const_lv12_E);
}

void mnist_fp16_opt::thread_grp_fu_11341_p1() {
    grp_fu_11341_p1 =  (sc_lv<5>) (ap_const_lv12_E);
}

void mnist_fp16_opt::thread_grp_fu_11474_p1() {
    grp_fu_11474_p1 =  (sc_lv<5>) (ap_const_lv12_E);
}

void mnist_fp16_opt::thread_grp_fu_11578_p1() {
    grp_fu_11578_p1 =  (sc_lv<5>) (ap_const_lv12_E);
}

void mnist_fp16_opt::thread_grp_fu_11625_p1() {
    grp_fu_11625_p1 =  (sc_lv<5>) (ap_const_lv12_E);
}

void mnist_fp16_opt::thread_grp_fu_11635_p1() {
    grp_fu_11635_p1 =  (sc_lv<5>) (ap_const_lv12_E);
}

void mnist_fp16_opt::thread_grp_fu_11640_p1() {
    grp_fu_11640_p1 =  (sc_lv<5>) (ap_const_lv12_E);
}

void mnist_fp16_opt::thread_grp_fu_14275_p0() {
    grp_fu_14275_p0 = (!tmp_1023_reg_25766.read()[0].is_01())? sc_lv<12>(): ((tmp_1023_reg_25766.read()[0].to_bool())? neg_ti18_fu_14262_p2.read(): tmp_929_fu_14252_p1.read());
}

void mnist_fp16_opt::thread_grp_fu_14275_p1() {
    grp_fu_14275_p1 =  (sc_lv<5>) (ap_const_lv12_E);
}

void mnist_fp16_opt::thread_grp_fu_17205_p0() {
    grp_fu_17205_p0 = (!tmp_1365_reg_26552.read()[0].is_01())? sc_lv<10>(): ((tmp_1365_reg_26552.read()[0].to_bool())? neg_ti32_fu_17192_p2.read(): tmp_1109_fu_17182_p1.read());
}

void mnist_fp16_opt::thread_grp_fu_17205_p1() {
    grp_fu_17205_p1 =  (sc_lv<4>) (ap_const_lv10_7);
}

void mnist_fp16_opt::thread_grp_fu_17285_p1() {
    grp_fu_17285_p1 =  (sc_lv<4>) (ap_const_lv10_7);
}

void mnist_fp16_opt::thread_grp_fu_17379_p1() {
    grp_fu_17379_p1 =  (sc_lv<4>) (ap_const_lv10_7);
}

void mnist_fp16_opt::thread_grp_fu_17384_p1() {
    grp_fu_17384_p1 =  (sc_lv<4>) (ap_const_lv10_7);
}

void mnist_fp16_opt::thread_grp_fu_19386_p0() {
    grp_fu_19386_p0 = (!tmp_1414_reg_27134_pp20_iter2_reg.read()[0].is_01())? sc_lv<11>(): ((tmp_1414_reg_27134_pp20_iter2_reg.read()[0].to_bool())? neg_ti38_fu_19373_p2.read(): tmp_1191_fu_19363_p1.read());
}

void mnist_fp16_opt::thread_grp_fu_19386_p1() {
    grp_fu_19386_p1 =  (sc_lv<4>) (ap_const_lv11_7);
}

void mnist_fp16_opt::thread_grp_fu_22836_p2() {
    grp_fu_22836_p2 = esl_concat<16,14>(p_Val2_18_reg_2797.read(), ap_const_lv14_0);
}

void mnist_fp16_opt::thread_grp_fu_22845_p0() {
    grp_fu_22845_p0 =  (sc_lv<9>) (ap_const_lv11_C4);
}

void mnist_fp16_opt::thread_grp_fu_22845_p1() {
    grp_fu_22845_p1 =  (sc_lv<3>) (grp_fu_22845_p10.read());
}

void mnist_fp16_opt::thread_grp_fu_22845_p10() {
    grp_fu_22845_p10 = esl_zext<11,3>(lhs_V_5_mid2_v_reg_24913.read());
}

void mnist_fp16_opt::thread_grp_fu_22845_p2() {
    grp_fu_22845_p2 =  (sc_lv<5>) (ap_const_lv11_7F1);
}

void mnist_fp16_opt::thread_grp_fu_22974_p2() {
    grp_fu_22974_p2 = esl_concat<16,14>(p_Val2_43_reg_3449.read(), ap_const_lv14_0);
}

void mnist_fp16_opt::thread_grp_fu_23059_p2() {
    grp_fu_23059_p2 = esl_concat<16,14>(p_Val2_54_reg_4064.read(), ap_const_lv14_0);
}

void mnist_fp16_opt::thread_grp_fu_4339_opcode() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state420.read()) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp26_iter2.read()) && 
          esl_seteq<1,1,1>(ap_block_pp26_stage0_00001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_reg_28051_pp26_iter1_reg.read())))) {
        grp_fu_4339_opcode = ap_const_lv2_1;
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state409.read()) || 
                (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp4_iter1.read()) && 
                 esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_flatten_reg_23844_pp4_iter1_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp4_stage2.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp4_stage2_00001.read(), ap_const_boolean_0)) || 
                (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp11_iter1.read()) && 
                 esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_flatten20_reg_25453_pp11_iter1_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp11_stage2.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp11_stage2_00001.read(), ap_const_boolean_0)) || 
                (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp18_iter1.read()) && 
                 esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_flatten38_reg_26853_pp18_iter1_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp18_stage3.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp18_stage3_00001.read(), ap_const_boolean_0)))) {
        grp_fu_4339_opcode = ap_const_lv2_0;
    } else {
        grp_fu_4339_opcode =  (sc_lv<2>) ("XX");
    }
}

void mnist_fp16_opt::thread_grp_fu_4339_p0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state420.read()) || 
         (esl_seteq<1,1,1>(ap_block_pp26_stage0.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp26_iter2.read())))) {
        grp_fu_4339_p0 = reg_4515.read();
    } else if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp4_iter1.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp4_stage2.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp4_stage2.read(), ap_const_boolean_0)) || 
                (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp11_iter1.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp11_stage2.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp11_stage2.read(), ap_const_boolean_0)) || 
                (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp18_iter1.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp18_stage3.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp18_stage3.read(), ap_const_boolean_0)) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state409.read()))) {
        grp_fu_4339_p0 = reg_4486.read();
    } else {
        grp_fu_4339_p0 =  (sc_lv<32>) ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
    }
}

void mnist_fp16_opt::thread_grp_fu_4339_p1() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state420.read()) || 
         (esl_seteq<1,1,1>(ap_block_pp26_stage0.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp26_iter2.read())))) {
        grp_fu_4339_p1 = compute14_reg_4245.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state409.read())) {
        grp_fu_4339_p1 = reducer6_reg_4232.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp18_iter1.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp18_stage3.read()) && 
                esl_seteq<1,1,1>(ap_block_pp18_stage3.read(), ap_const_boolean_0))) {
        grp_fu_4339_p1 = reducer90_2_reg_3826.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp11_iter1.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp11_stage2.read()) && 
                esl_seteq<1,1,1>(ap_block_pp11_stage2.read(), ap_const_boolean_0))) {
        grp_fu_4339_p1 = reducer87_2_reg_3211.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp4_iter1.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp4_stage2.read()) && 
                esl_seteq<1,1,1>(ap_block_pp4_stage2.read(), ap_const_boolean_0))) {
        grp_fu_4339_p1 = reducer84_1_reg_2555.read();
    } else {
        grp_fu_4339_p1 =  (sc_lv<32>) ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
    }
}

void mnist_fp16_opt::thread_grp_fu_4348_p0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state405.read())) {
        grp_fu_4348_p0 = p_03_i6_fu_22443_p3.read();
    } else if ((esl_seteq<1,1,1>(ap_enable_reg_pp18_iter0.read(), ap_const_logic_1) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp18_stage4.read()) && 
                esl_seteq<1,1,1>(ap_block_pp18_stage4.read(), ap_const_boolean_0))) {
        grp_fu_4348_p0 = pad_temp4_0_load_reg_26919.read();
    } else if ((esl_seteq<1,1,1>(ap_enable_reg_pp11_iter0.read(), ap_const_logic_1) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp11_stage3.read()) && 
                esl_seteq<1,1,1>(ap_block_pp11_stage3.read(), ap_const_boolean_0))) {
        grp_fu_4348_p0 = pad_temp2_0_load_reg_25510.read();
    } else if ((esl_seteq<1,1,1>(ap_enable_reg_pp4_iter0.read(), ap_const_logic_1) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp4_stage3.read()) && 
                esl_seteq<1,1,1>(ap_block_pp4_stage3.read(), ap_const_boolean_0))) {
        grp_fu_4348_p0 = pad_temp_0_0_load_reg_23891.read();
    } else {
        grp_fu_4348_p0 =  (sc_lv<32>) ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
    }
}

void mnist_fp16_opt::thread_grp_fu_4348_p1() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state405.read())) {
        grp_fu_4348_p1 = w_dense_1_load_reg_28003.read();
    } else if ((esl_seteq<1,1,1>(ap_enable_reg_pp18_iter0.read(), ap_const_logic_1) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp18_stage4.read()) && 
                esl_seteq<1,1,1>(ap_block_pp18_stage4.read(), ap_const_boolean_0))) {
        grp_fu_4348_p1 = w_conv5_load_reg_26924.read();
    } else if ((esl_seteq<1,1,1>(ap_enable_reg_pp11_iter0.read(), ap_const_logic_1) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp11_stage3.read()) && 
                esl_seteq<1,1,1>(ap_block_pp11_stage3.read(), ap_const_boolean_0))) {
        grp_fu_4348_p1 = w_conv3_load_reg_25515.read();
    } else if ((esl_seteq<1,1,1>(ap_enable_reg_pp4_iter0.read(), ap_const_logic_1) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp4_stage3.read()) && 
                esl_seteq<1,1,1>(ap_block_pp4_stage3.read(), ap_const_boolean_0))) {
        grp_fu_4348_p1 = w_conv1_0_load_reg_23896.read();
    } else {
        grp_fu_4348_p1 =  (sc_lv<32>) ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
    }
}

void mnist_fp16_opt::thread_grp_fu_4352_p0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state399.read())) {
        grp_fu_4352_p0 = p_012_0_i9_reg_27983.read();
    } else if ((esl_seteq<1,1,1>(ap_block_pp23_stage0.read(), ap_const_boolean_0) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp23_iter5.read()))) {
        grp_fu_4352_p0 = p_012_0_i8_reg_27797.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state359.read())) {
        grp_fu_4352_p0 = tmp32_V_47_reg_27559.read();
    } else if ((esl_seteq<1,1,1>(ap_block_pp20_stage0.read(), ap_const_boolean_0) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp20_iter22.read()))) {
        grp_fu_4352_p0 = tmp32_V_45_reg_27234.read();
    } else if ((esl_seteq<1,1,1>(ap_block_pp16_stage0.read(), ap_const_boolean_0) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp16_iter5.read()))) {
        grp_fu_4352_p0 = p_012_0_i5_reg_26455.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state239.read())) {
        grp_fu_4352_p0 = tmp32_V_42_reg_26164.read();
    } else if ((esl_seteq<1,1,1>(ap_block_pp13_stage0.read(), ap_const_boolean_0) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp13_iter23.read()))) {
        grp_fu_4352_p0 = tmp32_V_38_reg_25856.read();
    } else if ((esl_seteq<1,1,1>(ap_block_pp9_stage0.read(), ap_const_boolean_0) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp9_iter5.read()))) {
        grp_fu_4352_p0 = p_012_0_i2_reg_24860.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state115.read())) {
        grp_fu_4352_p0 = tmp32_V_28_reg_24564.read();
    } else if ((esl_seteq<1,1,1>(ap_block_pp6_stage0.read(), ap_const_boolean_0) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp6_iter25.read()))) {
        grp_fu_4352_p0 = tmp32_V_12_reg_24246.read();
    } else {
        grp_fu_4352_p0 =  (sc_lv<32>) ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
    }
}

void mnist_fp16_opt::thread_grp_fu_4355_p0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp26_iter57.read()) && 
         esl_seteq<1,1,1>(ap_block_pp26_stage0.read(), ap_const_boolean_0))) {
        grp_fu_4355_p0 = tmp_527_reg_28075.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state449.read())) {
        grp_fu_4355_p0 = tmp_501_reg_28046.read();
    } else {
        grp_fu_4355_p0 =  (sc_lv<64>) ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
    }
}

void mnist_fp16_opt::thread_grp_fu_4358_p0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state443.read())) {
        grp_fu_4358_p0 = compute15_reg_4279.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state425.read()) || 
                (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp26_iter7.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp26_stage0.read(), ap_const_boolean_0)))) {
        grp_fu_4358_p0 = reg_4491.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state418.read())) {
        grp_fu_4358_p0 = ap_phi_mux_compute15_phi_fu_4283_p4.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp24_stage0.read()) && 
                esl_seteq<1,1,1>(ap_block_pp24_stage0.read(), ap_const_boolean_0) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp24_iter1.read()))) {
        grp_fu_4358_p0 = max_pool_0_0_0_q0.read();
    } else if ((esl_seteq<1,1,1>(ap_block_pp22_stage0.read(), ap_const_boolean_0) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp22_iter4.read()))) {
        grp_fu_4358_p0 = conv6_0_load_reg_27628_pp22_iter3_reg.read();
    } else if ((esl_seteq<1,1,1>(ap_block_pp21_stage0.read(), ap_const_boolean_0) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp21_iter4.read()))) {
        grp_fu_4358_p0 = pad_temp5_0_load_reg_27371.read();
    } else if ((esl_seteq<1,1,1>(ap_block_pp19_stage0.read(), ap_const_boolean_0) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp19_iter4.read()))) {
        grp_fu_4358_p0 = conv5_0_load_reg_26998_pp19_iter3_reg.read();
    } else if ((esl_seteq<1,1,1>(ap_block_pp15_stage0.read(), ap_const_boolean_0) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp15_iter4.read()))) {
        grp_fu_4358_p0 = conv4_0_load_reg_26233_pp15_iter3_reg.read();
    } else if ((esl_seteq<1,1,1>(ap_block_pp14_stage0.read(), ap_const_boolean_0) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp14_iter3.read()))) {
        grp_fu_4358_p0 = pad_temp3_0_load_reg_25976.read();
    } else if ((esl_seteq<1,1,1>(ap_block_pp12_stage0.read(), ap_const_boolean_0) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp12_iter4.read()))) {
        grp_fu_4358_p0 = conv3_0_load_reg_25589_pp12_iter3_reg.read();
    } else if ((esl_seteq<1,1,1>(ap_block_pp8_stage0.read(), ap_const_boolean_0) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp8_iter4.read()))) {
        grp_fu_4358_p0 = conv2_0_load_reg_24633_pp8_iter3_reg.read();
    } else if ((esl_seteq<1,1,1>(ap_block_pp7_stage0.read(), ap_const_boolean_0) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp7_iter3.read()))) {
        grp_fu_4358_p0 = pad_temp1_0_load_reg_24376.read();
    } else if ((esl_seteq<1,1,1>(ap_block_pp5_stage0.read(), ap_const_boolean_0) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp5_iter4.read()))) {
        grp_fu_4358_p0 = conv1_0_load_reg_23965_pp5_iter3_reg.read();
    } else {
        grp_fu_4358_p0 =  (sc_lv<32>) ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
    }
}

void mnist_fp16_opt::thread_grp_fu_4361_p0() {
    if ((esl_seteq<1,1,1>(ap_block_pp21_stage0.read(), ap_const_boolean_0) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp21_iter4.read()))) {
        grp_fu_4361_p0 = w_conv6_load_reg_27376.read();
    } else if ((esl_seteq<1,1,1>(ap_block_pp14_stage0.read(), ap_const_boolean_0) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp14_iter3.read()))) {
        grp_fu_4361_p0 = w_conv4_load_reg_25981.read();
    } else if ((esl_seteq<1,1,1>(ap_block_pp7_stage0.read(), ap_const_boolean_0) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp7_iter3.read()))) {
        grp_fu_4361_p0 = w_conv2_load_reg_24381.read();
    } else {
        grp_fu_4361_p0 =  (sc_lv<32>) ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
    }
}

void mnist_fp16_opt::thread_grp_fu_4367_opcode() {
    if ((esl_seteq<1,1,1>(ap_const_lv1_0, tmp_546_reg_28080_pp27_iter1_reg.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp27_iter2.read()) && 
         esl_seteq<1,1,1>(ap_block_pp27_stage0_00001.read(), ap_const_boolean_0))) {
        grp_fu_4367_opcode = ap_const_lv5_2;
    } else if (((esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_flatten3_reg_23911_pp5_iter2_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp5_iter3.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp5_stage0_00001.read(), ap_const_boolean_0)) || 
                (esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_flatten9_reg_24579_pp8_iter2_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp8_iter3.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp8_stage0_00001.read(), ap_const_boolean_0)) || 
                (esl_seteq<1,1,1>(ap_enable_reg_pp9_iter12.read(), ap_const_logic_1) && 
                 esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_flatten16_reg_24778_pp9_iter11_reg.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp9_stage0_00001.read(), ap_const_boolean_0)) || 
                (esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_flatten19_reg_25535_pp12_iter2_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp12_iter3.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp12_stage0_00001.read(), ap_const_boolean_0)) || 
                (esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_flatten27_reg_26179_pp15_iter2_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp15_iter3.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp15_stage0_00001.read(), ap_const_boolean_0)) || 
                (esl_seteq<1,1,1>(ap_enable_reg_pp16_iter12.read(), ap_const_logic_1) && 
                 esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_flatten34_reg_26373_pp16_iter11_reg.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp16_stage0_00001.read(), ap_const_boolean_0)) || 
                (esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_flatten37_reg_26944_pp19_iter2_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp19_iter3.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp19_stage0_00001.read(), ap_const_boolean_0)) || 
                (esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_flatten45_reg_27574_pp22_iter2_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp22_iter3.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp22_stage0_00001.read(), ap_const_boolean_0)) || 
                (esl_seteq<1,1,1>(ap_enable_reg_pp23_iter12.read(), ap_const_logic_1) && 
                 esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_flatten49_reg_27725_pp23_iter11_reg.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp23_stage0_00001.read(), ap_const_boolean_0)) || 
                (esl_seteq<1,1,1>(ap_enable_reg_pp25_iter2.read(), ap_const_logic_1) && 
                 esl_seteq<1,1,1>(ap_const_lv1_0, exitcond34_reg_28013_pp25_iter1_reg.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp25_stage0_00001.read(), ap_const_boolean_0)))) {
        grp_fu_4367_opcode = ap_const_lv5_4;
    } else {
        grp_fu_4367_opcode =  (sc_lv<5>) ("XXXXX");
    }
}

void mnist_fp16_opt::thread_grp_fu_4367_p0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp27_iter2.read()) && 
         esl_seteq<1,1,1>(ap_block_pp27_stage0.read(), ap_const_boolean_0))) {
        grp_fu_4367_p0 = pred_2_reg_28094.read();
    } else if ((esl_seteq<1,1,1>(ap_enable_reg_pp25_iter2.read(), ap_const_logic_1) && 
                esl_seteq<1,1,1>(ap_block_pp25_stage0.read(), ap_const_boolean_0))) {
        grp_fu_4367_p0 = reg_4515.read();
    } else if ((esl_seteq<1,1,1>(ap_enable_reg_pp23_iter12.read(), ap_const_logic_1) && 
                esl_seteq<1,1,1>(ap_block_pp23_stage0.read(), ap_const_boolean_0))) {
        grp_fu_4367_p0 = p_03_i5_reg_27812.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp22_iter3.read()) && 
                esl_seteq<1,1,1>(ap_block_pp22_stage0.read(), ap_const_boolean_0))) {
        grp_fu_4367_p0 = conv6_0_load_reg_27628.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp19_iter3.read()) && 
                esl_seteq<1,1,1>(ap_block_pp19_stage0.read(), ap_const_boolean_0))) {
        grp_fu_4367_p0 = conv5_0_load_reg_26998.read();
    } else if ((esl_seteq<1,1,1>(ap_enable_reg_pp16_iter12.read(), ap_const_logic_1) && 
                esl_seteq<1,1,1>(ap_block_pp16_stage0.read(), ap_const_boolean_0))) {
        grp_fu_4367_p0 = p_03_i3_reg_26470.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp15_iter3.read()) && 
                esl_seteq<1,1,1>(ap_block_pp15_stage0.read(), ap_const_boolean_0))) {
        grp_fu_4367_p0 = conv4_0_load_reg_26233.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp12_iter3.read()) && 
                esl_seteq<1,1,1>(ap_block_pp12_stage0.read(), ap_const_boolean_0))) {
        grp_fu_4367_p0 = conv3_0_load_reg_25589.read();
    } else if ((esl_seteq<1,1,1>(ap_enable_reg_pp9_iter12.read(), ap_const_logic_1) && 
                esl_seteq<1,1,1>(ap_block_pp9_stage0.read(), ap_const_boolean_0))) {
        grp_fu_4367_p0 = p_03_i1_reg_24875.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp8_iter3.read()) && 
                esl_seteq<1,1,1>(ap_block_pp8_stage0.read(), ap_const_boolean_0))) {
        grp_fu_4367_p0 = conv2_0_load_reg_24633.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp5_iter3.read()) && 
                esl_seteq<1,1,1>(ap_block_pp5_stage0.read(), ap_const_boolean_0))) {
        grp_fu_4367_p0 = conv1_0_load_reg_23965.read();
    } else {
        grp_fu_4367_p0 =  (sc_lv<32>) ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
    }
}

void mnist_fp16_opt::thread_grp_fu_4367_p1() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp27_iter2.read()) && 
         esl_seteq<1,1,1>(ap_block_pp27_stage0.read(), ap_const_boolean_0))) {
        grp_fu_4367_p1 = ap_phi_mux_pred_phi_fu_4331_p4.read();
    } else if ((esl_seteq<1,1,1>(ap_enable_reg_pp25_iter2.read(), ap_const_logic_1) && 
                esl_seteq<1,1,1>(ap_block_pp25_stage0.read(), ap_const_boolean_0))) {
        grp_fu_4367_p1 = compute14_reg_4245.read();
    } else if ((esl_seteq<1,1,1>(ap_enable_reg_pp23_iter12.read(), ap_const_logic_1) && 
                esl_seteq<1,1,1>(ap_block_pp23_stage0.read(), ap_const_boolean_0))) {
        grp_fu_4367_p1 = tmp_457_reg_4175.read();
    } else if ((esl_seteq<1,1,1>(ap_enable_reg_pp16_iter12.read(), ap_const_logic_1) && 
                esl_seteq<1,1,1>(ap_block_pp16_stage0.read(), ap_const_boolean_0))) {
        grp_fu_4367_p1 = tmp_588_reg_3607.read();
    } else if ((esl_seteq<1,1,1>(ap_enable_reg_pp9_iter12.read(), ap_const_logic_1) && 
                esl_seteq<1,1,1>(ap_block_pp9_stage0.read(), ap_const_boolean_0))) {
        grp_fu_4367_p1 = tmp_313_reg_2955.read();
    } else if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp5_iter3.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp5_stage0.read(), ap_const_boolean_0)) || 
                (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp8_iter3.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp8_stage0.read(), ap_const_boolean_0)) || 
                (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp12_iter3.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp12_stage0.read(), ap_const_boolean_0)) || 
                (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp15_iter3.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp15_stage0.read(), ap_const_boolean_0)) || 
                (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp19_iter3.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp19_stage0.read(), ap_const_boolean_0)) || 
                (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp22_iter3.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp22_stage0.read(), ap_const_boolean_0)))) {
        grp_fu_4367_p1 = ap_const_lv32_0;
    } else {
        grp_fu_4367_p1 =  (sc_lv<32>) ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
    }
}

void mnist_fp16_opt::thread_grp_fu_4385_p1() {
    if ((esl_seteq<1,1,1>(ap_block_pp26_stage0.read(), ap_const_boolean_0) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp26_iter8.read()))) {
        grp_fu_4385_p1 = tmp_525_reg_28070.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state426.read())) {
        grp_fu_4385_p1 = reg_4521.read();
    } else {
        grp_fu_4385_p1 =  (sc_lv<64>) ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
    }
}

void mnist_fp16_opt::thread_grp_fu_4400_p0() {
    if (((esl_seteq<1,1,1>(ap_enable_reg_pp3_iter0.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage1.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage1.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_enable_reg_pp3_iter0.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage2.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage2.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage3.read()) && 
          esl_seteq<1,1,1>(ap_enable_reg_pp3_iter0.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage3.read(), ap_const_boolean_0)))) {
        grp_fu_4400_p0 = p_s_reg_2273.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage0.read()) && 
                esl_seteq<1,1,1>(ap_enable_reg_pp3_iter0.read(), ap_const_logic_1) && 
                esl_seteq<1,1,1>(ap_block_pp3_stage0.read(), ap_const_boolean_0))) {
        grp_fu_4400_p0 = ap_phi_mux_p_s_phi_fu_2277_p4.read();
    } else {
        grp_fu_4400_p0 =  (sc_lv<5>) ("XXXXX");
    }
}

void mnist_fp16_opt::thread_grp_fu_4400_p2() {
    grp_fu_4400_p2 = (!grp_fu_4400_p0.read().is_01() || !ap_const_lv5_1E.is_01())? sc_lv<1>(): sc_lv<1>(grp_fu_4400_p0.read() == ap_const_lv5_1E);
}

void mnist_fp16_opt::thread_grp_fu_4407_p1() {
    if (((esl_seteq<1,1,1>(ap_enable_reg_pp3_iter0.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage2.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage2.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage3.read()) && 
          esl_seteq<1,1,1>(ap_enable_reg_pp3_iter0.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage3.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_enable_reg_pp3_iter0.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage4.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage4.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_enable_reg_pp3_iter0.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage5.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage5.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_enable_reg_pp3_iter0.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage6.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage6.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_enable_reg_pp3_iter0.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage7.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage7.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_enable_reg_pp3_iter0.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage8.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage8.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_enable_reg_pp3_iter0.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage9.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage9.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_enable_reg_pp3_iter0.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage10.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage10.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_enable_reg_pp3_iter0.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage11.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage11.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_enable_reg_pp3_iter0.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage12.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage12.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_enable_reg_pp3_iter0.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage13.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage13.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_enable_reg_pp3_iter0.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage14.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage14.read(), ap_const_boolean_0)))) {
        grp_fu_4407_p1 = tmp_3_reg_23227.read();
    } else if ((esl_seteq<1,1,1>(ap_enable_reg_pp3_iter0.read(), ap_const_logic_1) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage1.read()) && 
                esl_seteq<1,1,1>(ap_block_pp3_stage1.read(), ap_const_boolean_0))) {
        grp_fu_4407_p1 = tmp_3_fu_4739_p2.read();
    } else {
        grp_fu_4407_p1 = "XXXXXXXXXXXX";
    }
}

void mnist_fp16_opt::thread_grp_fu_4407_p2() {
    grp_fu_4407_p2 = (!ap_const_lv12_FE3.is_01() || !grp_fu_4407_p1.read().is_01())? sc_lv<12>(): (sc_bigint<12>(ap_const_lv12_FE3) + sc_biguint<12>(grp_fu_4407_p1.read()));
}

void mnist_fp16_opt::thread_grp_fu_4420_p0() {
    if (((esl_seteq<1,1,1>(ap_enable_reg_pp10_iter0.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage3.read()) && 
          esl_seteq<1,1,1>(ap_block_pp10_stage3.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_enable_reg_pp10_iter0.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage4.read()) && 
          esl_seteq<1,1,1>(ap_block_pp10_stage4.read(), ap_const_boolean_0)))) {
        grp_fu_4420_p0 = tmp350_cast_mid2_reg_24947.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage2.read()) && 
                esl_seteq<1,1,1>(ap_enable_reg_pp10_iter0.read(), ap_const_logic_1) && 
                esl_seteq<1,1,1>(ap_block_pp10_stage2.read(), ap_const_boolean_0))) {
        grp_fu_4420_p0 = tmp350_cast_mid2_fu_11155_p1.read();
    } else {
        grp_fu_4420_p0 = "XXXXXXXXXXXX";
    }
}

void mnist_fp16_opt::thread_grp_fu_4420_p1() {
    if (((esl_seteq<1,1,1>(ap_enable_reg_pp10_iter0.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage3.read()) && 
          esl_seteq<1,1,1>(ap_block_pp10_stage3.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_enable_reg_pp10_iter0.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage4.read()) && 
          esl_seteq<1,1,1>(ap_block_pp10_stage4.read(), ap_const_boolean_0)))) {
        grp_fu_4420_p1 = tmp_307_reg_24958.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage2.read()) && 
                esl_seteq<1,1,1>(ap_enable_reg_pp10_iter0.read(), ap_const_logic_1) && 
                esl_seteq<1,1,1>(ap_block_pp10_stage2.read(), ap_const_boolean_0))) {
        grp_fu_4420_p1 = tmp_307_fu_11179_p2.read();
    } else {
        grp_fu_4420_p1 = "XXXXXXXXXXXX";
    }
}

void mnist_fp16_opt::thread_grp_fu_4420_p2() {
    grp_fu_4420_p2 = (!grp_fu_4420_p0.read().is_01() || !grp_fu_4420_p1.read().is_01())? sc_lv<12>(): (sc_bigint<12>(grp_fu_4420_p0.read()) + sc_biguint<12>(grp_fu_4420_p1.read()));
}

void mnist_fp16_opt::thread_grp_fu_4432_p0() {
    if (((esl_seteq<1,1,1>(ap_enable_reg_pp10_iter0.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage3.read()) && 
          esl_seteq<1,1,1>(ap_block_pp10_stage3.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_enable_reg_pp10_iter0.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage4.read()) && 
          esl_seteq<1,1,1>(ap_block_pp10_stage4.read(), ap_const_boolean_0)))) {
        grp_fu_4432_p0 = tmp350_cast_mid2_reg_24947.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage2.read()) && 
                esl_seteq<1,1,1>(ap_enable_reg_pp10_iter0.read(), ap_const_logic_1) && 
                esl_seteq<1,1,1>(ap_block_pp10_stage2.read(), ap_const_boolean_0))) {
        grp_fu_4432_p0 = tmp350_cast_mid2_fu_11155_p1.read();
    } else {
        grp_fu_4432_p0 = "XXXXXXXXXXXX";
    }
}

void mnist_fp16_opt::thread_grp_fu_4432_p1() {
    if (((esl_seteq<1,1,1>(ap_enable_reg_pp10_iter0.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage3.read()) && 
          esl_seteq<1,1,1>(ap_block_pp10_stage3.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_enable_reg_pp10_iter0.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage4.read()) && 
          esl_seteq<1,1,1>(ap_block_pp10_stage4.read(), ap_const_boolean_0)))) {
        grp_fu_4432_p1 = tmp_307_reg_24958.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage2.read()) && 
                esl_seteq<1,1,1>(ap_enable_reg_pp10_iter0.read(), ap_const_logic_1) && 
                esl_seteq<1,1,1>(ap_block_pp10_stage2.read(), ap_const_boolean_0))) {
        grp_fu_4432_p1 = tmp_307_fu_11179_p2.read();
    } else {
        grp_fu_4432_p1 = "XXXXXXXXXXXX";
    }
}

void mnist_fp16_opt::thread_grp_fu_4432_p2() {
    grp_fu_4432_p2 = (!grp_fu_4432_p0.read().is_01() || !grp_fu_4432_p1.read().is_01())? sc_lv<12>(): (sc_bigint<12>(grp_fu_4432_p0.read()) + sc_biguint<12>(grp_fu_4432_p1.read()));
}

void mnist_fp16_opt::thread_grp_fu_4444_p0() {
    if (esl_seteq<1,1,1>(ap_enable_reg_pp17_iter0.read(), ap_const_logic_1)) {
        if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp17_stage2.read()) && 
             esl_seteq<1,1,1>(ap_block_pp17_stage2.read(), ap_const_boolean_0))) {
            grp_fu_4444_p0 = rhs_V_13_cast_reg_26542.read();
        } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp17_stage1.read()) && 
                    esl_seteq<1,1,1>(ap_block_pp17_stage1.read(), ap_const_boolean_0))) {
            grp_fu_4444_p0 = rhs_V_13_cast_fu_17120_p1.read();
        } else {
            grp_fu_4444_p0 =  (sc_lv<10>) ("XXXXXXXXXX");
        }
    } else {
        grp_fu_4444_p0 =  (sc_lv<10>) ("XXXXXXXXXX");
    }
}

void mnist_fp16_opt::thread_grp_fu_4444_p1() {
    if (esl_seteq<1,1,1>(ap_enable_reg_pp17_iter0.read(), ap_const_logic_1)) {
        if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp17_stage2.read()) && 
             esl_seteq<1,1,1>(ap_block_pp17_stage2.read(), ap_const_boolean_0))) {
            grp_fu_4444_p1 = rhs_V_2_mid2_reg_26525.read();
        } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp17_stage1.read()) && 
                    esl_seteq<1,1,1>(ap_block_pp17_stage1.read(), ap_const_boolean_0))) {
            grp_fu_4444_p1 = rhs_V_2_mid2_fu_17064_p2.read();
        } else {
            grp_fu_4444_p1 =  (sc_lv<10>) ("XXXXXXXXXX");
        }
    } else {
        grp_fu_4444_p1 =  (sc_lv<10>) ("XXXXXXXXXX");
    }
}

void mnist_fp16_opt::thread_grp_fu_4444_p2() {
    grp_fu_4444_p2 = (!grp_fu_4444_p0.read().is_01() || !grp_fu_4444_p1.read().is_01())? sc_lv<10>(): (sc_bigint<10>(grp_fu_4444_p0.read()) + sc_biguint<10>(grp_fu_4444_p1.read()));
}

void mnist_fp16_opt::thread_grp_fu_4448_p2() {
    grp_fu_4448_p2 = (!ap_const_lv10_3F9.is_01() || !grp_fu_4444_p2.read().is_01())? sc_lv<10>(): (sc_bigint<10>(ap_const_lv10_3F9) + sc_biguint<10>(grp_fu_4444_p2.read()));
}

void mnist_fp16_opt::thread_grp_fu_4462_p0() {
    if (esl_seteq<1,1,1>(ap_enable_reg_pp17_iter0.read(), ap_const_logic_1)) {
        if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp17_stage2.read()) && 
             esl_seteq<1,1,1>(ap_block_pp17_stage2.read(), ap_const_boolean_0))) {
            grp_fu_4462_p0 = rhs_V_13_cast_reg_26542.read();
        } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp17_stage1.read()) && 
                    esl_seteq<1,1,1>(ap_block_pp17_stage1.read(), ap_const_boolean_0))) {
            grp_fu_4462_p0 = rhs_V_13_cast_fu_17120_p1.read();
        } else {
            grp_fu_4462_p0 =  (sc_lv<10>) ("XXXXXXXXXX");
        }
    } else {
        grp_fu_4462_p0 =  (sc_lv<10>) ("XXXXXXXXXX");
    }
}

void mnist_fp16_opt::thread_grp_fu_4462_p1() {
    if (esl_seteq<1,1,1>(ap_enable_reg_pp17_iter0.read(), ap_const_logic_1)) {
        if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp17_stage2.read()) && 
             esl_seteq<1,1,1>(ap_block_pp17_stage2.read(), ap_const_boolean_0))) {
            grp_fu_4462_p1 = rhs_V_2_mid2_reg_26525.read();
        } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp17_stage1.read()) && 
                    esl_seteq<1,1,1>(ap_block_pp17_stage1.read(), ap_const_boolean_0))) {
            grp_fu_4462_p1 = rhs_V_2_mid2_fu_17064_p2.read();
        } else {
            grp_fu_4462_p1 =  (sc_lv<10>) ("XXXXXXXXXX");
        }
    } else {
        grp_fu_4462_p1 =  (sc_lv<10>) ("XXXXXXXXXX");
    }
}

void mnist_fp16_opt::thread_grp_fu_4462_p2() {
    grp_fu_4462_p2 = (!grp_fu_4462_p0.read().is_01() || !grp_fu_4462_p1.read().is_01())? sc_lv<10>(): (sc_bigint<10>(grp_fu_4462_p0.read()) + sc_biguint<10>(grp_fu_4462_p1.read()));
}

void mnist_fp16_opt::thread_grp_fu_4466_p2() {
    grp_fu_4466_p2 = (!ap_const_lv10_3F9.is_01() || !grp_fu_4462_p2.read().is_01())? sc_lv<10>(): (sc_bigint<10>(ap_const_lv10_3F9) + sc_biguint<10>(grp_fu_4462_p2.read()));
}

void mnist_fp16_opt::thread_grp_fu_4819_p0() {
    grp_fu_4819_p0 =  (sc_lv<13>) (grp_fu_4819_p00.read());
}

void mnist_fp16_opt::thread_grp_fu_4819_p00() {
    grp_fu_4819_p00 = (!tmp_252_reg_23241.read()[0].is_01())? sc_lv<12>(): ((tmp_252_reg_23241.read()[0].to_bool())? neg_ti2_fu_4806_p2.read(): tmp_265_fu_4796_p1.read());
}

void mnist_fp16_opt::thread_grp_fu_4819_p1() {
    grp_fu_4819_p1 =  (sc_lv<6>) (ap_const_lv12_1C);
}

void mnist_fp16_opt::thread_grp_fu_4913_p0() {
    grp_fu_4913_p0 =  (sc_lv<13>) (grp_fu_4913_p00.read());
}

void mnist_fp16_opt::thread_grp_fu_4913_p00() {
    grp_fu_4913_p00 = (!tmp_364_reg_23266.read()[0].is_01())? sc_lv<12>(): ((tmp_364_reg_23266.read()[0].to_bool())? neg_ti3_fu_4900_p2.read(): tmp_372_fu_4890_p1.read());
}

void mnist_fp16_opt::thread_grp_fu_4913_p1() {
    grp_fu_4913_p1 =  (sc_lv<6>) (ap_const_lv12_1C);
}

void mnist_fp16_opt::thread_grp_fu_5187_p0() {
    grp_fu_5187_p0 =  (sc_lv<13>) (grp_fu_5187_p00.read());
}

void mnist_fp16_opt::thread_grp_fu_5187_p00() {
    grp_fu_5187_p00 = (!tmp_537_reg_23295.read()[0].is_01())? sc_lv<12>(): ((tmp_537_reg_23295.read()[0].to_bool())? neg_ti6_fu_5174_p2.read(): tmp_519_fu_5164_p1.read());
}

void mnist_fp16_opt::thread_grp_fu_5187_p1() {
    grp_fu_5187_p1 =  (sc_lv<6>) (ap_const_lv12_1C);
}

void mnist_fp16_opt::thread_grp_fu_5251_p0() {
    grp_fu_5251_p0 =  (sc_lv<13>) (grp_fu_5251_p00.read());
}

void mnist_fp16_opt::thread_grp_fu_5251_p00() {
    grp_fu_5251_p00 = (!tmp_703_reg_23352.read()[0].is_01())? sc_lv<12>(): ((tmp_703_reg_23352.read()[0].to_bool())? neg_ti7_fu_5238_p2.read(): tmp_707_fu_5228_p1.read());
}

void mnist_fp16_opt::thread_grp_fu_5251_p1() {
    grp_fu_5251_p1 =  (sc_lv<6>) (ap_const_lv12_1C);
}

void mnist_fp16_opt::thread_grp_fu_5315_p0() {
    grp_fu_5315_p0 =  (sc_lv<13>) (grp_fu_5315_p00.read());
}

void mnist_fp16_opt::thread_grp_fu_5315_p00() {
    grp_fu_5315_p00 = (!tmp_731_reg_23373.read()[0].is_01())? sc_lv<12>(): ((tmp_731_reg_23373.read()[0].to_bool())? neg_ti8_fu_5302_p2.read(): tmp_735_fu_5292_p1.read());
}

void mnist_fp16_opt::thread_grp_fu_5315_p1() {
    grp_fu_5315_p1 =  (sc_lv<6>) (ap_const_lv12_1C);
}

void mnist_fp16_opt::thread_grp_fu_5379_p0() {
    grp_fu_5379_p0 =  (sc_lv<13>) (grp_fu_5379_p00.read());
}

void mnist_fp16_opt::thread_grp_fu_5379_p00() {
    grp_fu_5379_p00 = (!tmp_808_reg_23394.read()[0].is_01())? sc_lv<12>(): ((tmp_808_reg_23394.read()[0].to_bool())? neg_ti11_fu_5366_p2.read(): tmp_812_fu_5356_p1.read());
}

void mnist_fp16_opt::thread_grp_fu_5379_p1() {
    grp_fu_5379_p1 =  (sc_lv<6>) (ap_const_lv12_1C);
}

void mnist_fp16_opt::thread_grp_fu_5443_p0() {
    grp_fu_5443_p0 =  (sc_lv<13>) (grp_fu_5443_p00.read());
}

void mnist_fp16_opt::thread_grp_fu_5443_p00() {
    grp_fu_5443_p00 = (!tmp_858_reg_23415.read()[0].is_01())? sc_lv<12>(): ((tmp_858_reg_23415.read()[0].to_bool())? neg_ti14_fu_5430_p2.read(): tmp_862_fu_5420_p1.read());
}

void mnist_fp16_opt::thread_grp_fu_5443_p1() {
    grp_fu_5443_p1 =  (sc_lv<6>) (ap_const_lv12_1C);
}

void mnist_fp16_opt::thread_grp_fu_5507_p0() {
    grp_fu_5507_p0 =  (sc_lv<13>) (grp_fu_5507_p00.read());
}

void mnist_fp16_opt::thread_grp_fu_5507_p00() {
    grp_fu_5507_p00 = (!tmp_972_reg_23436.read()[0].is_01())? sc_lv<12>(): ((tmp_972_reg_23436.read()[0].to_bool())? neg_ti17_fu_5494_p2.read(): tmp_911_fu_5484_p1.read());
}

void mnist_fp16_opt::thread_grp_fu_5507_p1() {
    grp_fu_5507_p1 =  (sc_lv<6>) (ap_const_lv12_1C);
}

void mnist_fp16_opt::thread_grp_fu_5571_p0() {
    grp_fu_5571_p0 =  (sc_lv<13>) (grp_fu_5571_p00.read());
}

void mnist_fp16_opt::thread_grp_fu_5571_p00() {
    grp_fu_5571_p00 = (!tmp_1125_reg_23457.read()[0].is_01())? sc_lv<12>(): ((tmp_1125_reg_23457.read()[0].to_bool())? neg_ti22_fu_5558_p2.read(): tmp_964_fu_5548_p1.read());
}

void mnist_fp16_opt::thread_grp_fu_5571_p1() {
    grp_fu_5571_p1 =  (sc_lv<6>) (ap_const_lv12_1C);
}

void mnist_fp16_opt::thread_grp_fu_5635_p0() {
    grp_fu_5635_p0 =  (sc_lv<13>) (grp_fu_5635_p00.read());
}

void mnist_fp16_opt::thread_grp_fu_5635_p00() {
    grp_fu_5635_p00 = (!tmp_1265_reg_23478.read()[0].is_01())? sc_lv<12>(): ((tmp_1265_reg_23478.read()[0].to_bool())? neg_ti25_fu_5622_p2.read(): tmp_1001_fu_5612_p1.read());
}

void mnist_fp16_opt::thread_grp_fu_5635_p1() {
    grp_fu_5635_p1 =  (sc_lv<6>) (ap_const_lv12_1C);
}

void mnist_fp16_opt::thread_grp_fu_5699_p0() {
    grp_fu_5699_p0 =  (sc_lv<13>) (grp_fu_5699_p00.read());
}

void mnist_fp16_opt::thread_grp_fu_5699_p00() {
    grp_fu_5699_p00 = (!tmp_1323_reg_23499.read()[0].is_01())? sc_lv<12>(): ((tmp_1323_reg_23499.read()[0].to_bool())? neg_ti28_fu_5686_p2.read(): tmp_1047_fu_5676_p1.read());
}

void mnist_fp16_opt::thread_grp_fu_5699_p1() {
    grp_fu_5699_p1 =  (sc_lv<6>) (ap_const_lv12_1C);
}

void mnist_fp16_opt::thread_grp_fu_5779_p0() {
    grp_fu_5779_p0 =  (sc_lv<13>) (grp_fu_5779_p00.read());
}

void mnist_fp16_opt::thread_grp_fu_5779_p00() {
    grp_fu_5779_p00 = (!tmp_1346_reg_23520.read()[0].is_01())? sc_lv<12>(): ((tmp_1346_reg_23520.read()[0].to_bool())? neg_ti31_fu_5766_p2.read(): tmp_1083_fu_5756_p1.read());
}

void mnist_fp16_opt::thread_grp_fu_5779_p1() {
    grp_fu_5779_p1 =  (sc_lv<6>) (ap_const_lv12_1C);
}

void mnist_fp16_opt::thread_grp_fu_5827_p0() {
    grp_fu_5827_p0 =  (sc_lv<13>) (grp_fu_5827_p00.read());
}

void mnist_fp16_opt::thread_grp_fu_5827_p00() {
    grp_fu_5827_p00 = (!tmp_89_reg_23536.read()[0].is_01())? sc_lv<12>(): ((tmp_89_reg_23536.read()[0].to_bool())? neg_ti_fu_5814_p2.read(): tmp_103_fu_5804_p1.read());
}

void mnist_fp16_opt::thread_grp_fu_5827_p1() {
    grp_fu_5827_p1 =  (sc_lv<6>) (ap_const_lv12_1C);
}

void mnist_fp16_opt::thread_grp_fu_5891_p0() {
    grp_fu_5891_p0 =  (sc_lv<13>) (grp_fu_5891_p00.read());
}

void mnist_fp16_opt::thread_grp_fu_5891_p00() {
    grp_fu_5891_p00 = (!tmp_153_reg_23562.read()[0].is_01())? sc_lv<12>(): ((tmp_153_reg_23562.read()[0].to_bool())? neg_ti1_fu_5878_p2.read(): tmp_167_fu_5868_p1.read());
}

void mnist_fp16_opt::thread_grp_fu_5891_p1() {
    grp_fu_5891_p1 =  (sc_lv<6>) (ap_const_lv12_1C);
}

void mnist_fp16_opt::thread_grp_fu_8241_p0() {
    grp_fu_8241_p0 =  (sc_lv<14>) (grp_fu_8241_p00.read());
}

void mnist_fp16_opt::thread_grp_fu_8241_p00() {
    grp_fu_8241_p00 = (!tmp_418_reg_24156.read()[0].is_01())? sc_lv<13>(): ((tmp_418_reg_24156.read()[0].to_bool())? neg_ti4_fu_8228_p2.read(): tmp_426_fu_8218_p1.read());
}

void mnist_fp16_opt::thread_grp_fu_8241_p1() {
    grp_fu_8241_p1 =  (sc_lv<6>) (ap_const_lv13_1C);
}

void mnist_fp16_opt::thread_h1_V_fu_16465_p2() {
    h1_V_fu_16465_p2 = (!ap_const_lv3_1.is_01() || !p_59_mid_reg_26328.read().is_01())? sc_lv<3>(): (sc_biguint<3>(ap_const_lv3_1) + sc_biguint<3>(p_59_mid_reg_26328.read()));
}

void mnist_fp16_opt::thread_h_V_fu_10502_p2() {
    h_V_fu_10502_p2 = (!ap_const_lv4_1.is_01() || !p_27_mid_reg_24728.read().is_01())? sc_lv<4>(): (sc_biguint<4>(ap_const_lv4_1) + sc_biguint<4>(p_27_mid_reg_24728.read()));
}

void mnist_fp16_opt::thread_i1_V_fu_7866_p2() {
    i1_V_fu_7866_p2 = (!p_12_mid2_fu_7850_p3.read().is_01() || !ap_const_lv5_1.is_01())? sc_lv<5>(): (sc_biguint<5>(p_12_mid2_fu_7850_p3.read()) + sc_biguint<5>(ap_const_lv5_1));
}

void mnist_fp16_opt::thread_i4_V_fu_14052_p2() {
    i4_V_fu_14052_p2 = (!p_35_mid2_fu_13968_p3.read().is_01() || !ap_const_lv5_1.is_01())? sc_lv<5>(): (sc_biguint<5>(p_35_mid2_fu_13968_p3.read()) + sc_biguint<5>(ap_const_lv5_1));
}

void mnist_fp16_opt::thread_i7_V_fu_19182_p2() {
    i7_V_fu_19182_p2 = (!p_62_mid2_fu_19100_p3.read().is_01() || !ap_const_lv4_1.is_01())? sc_lv<4>(): (sc_biguint<4>(p_62_mid2_fu_19100_p3.read()) + sc_biguint<4>(ap_const_lv4_1));
}

void mnist_fp16_opt::thread_i_V_1_fu_22600_p2() {
    i_V_1_fu_22600_p2 = (!ap_phi_mux_index_V_phi_fu_4319_p4.read().is_01() || !ap_const_lv4_1.is_01())? sc_lv<4>(): (sc_biguint<4>(ap_phi_mux_index_V_phi_fu_4319_p4.read()) + sc_biguint<4>(ap_const_lv4_1));
}

void mnist_fp16_opt::thread_i_V_fu_4555_p2() {
    i_V_fu_4555_p2 = (!p_0_reg_2156.read().is_01() || !ap_const_lv5_1.is_01())? sc_lv<5>(): (sc_biguint<5>(p_0_reg_2156.read()) + sc_biguint<5>(ap_const_lv5_1));
}

void mnist_fp16_opt::thread_icmp10_fu_15706_p2() {
    icmp10_fu_15706_p2 = (!tmp_1283_fu_15696_p4.read().is_01() || !ap_const_lv26_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_1283_fu_15696_p4.read() == ap_const_lv26_0);
}

void mnist_fp16_opt::thread_icmp11_fu_15177_p2() {
    icmp11_fu_15177_p2 = (!tmp_1296_fu_15167_p4.read().is_01() || !ap_const_lv8_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_1296_fu_15167_p4.read() == ap_const_lv8_0);
}

void mnist_fp16_opt::thread_icmp12_fu_15262_p2() {
    icmp12_fu_15262_p2 = (!tmp_1303_fu_15252_p4.read().is_01() || !ap_const_lv8_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_1303_fu_15252_p4.read() == ap_const_lv8_0);
}

void mnist_fp16_opt::thread_icmp13_fu_16764_p2() {
    icmp13_fu_16764_p2 = (!tmp_1356_fu_16754_p4.read().is_01() || !ap_const_lv26_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_1356_fu_16754_p4.read() == ap_const_lv26_0);
}

void mnist_fp16_opt::thread_icmp14_fu_18794_p2() {
    icmp14_fu_18794_p2 = (!tmp_1393_fu_18784_p4.read().is_01() || !ap_const_lv8_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_1393_fu_18784_p4.read() == ap_const_lv8_0);
}

void mnist_fp16_opt::thread_icmp15_fu_19599_p2() {
    icmp15_fu_19599_p2 = (!tmp_1439_fu_19589_p4.read().is_01() || !ap_const_lv26_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_1439_fu_19589_p4.read() == ap_const_lv26_0);
}

void mnist_fp16_opt::thread_icmp16_fu_21297_p2() {
    icmp16_fu_21297_p2 = (!tmp_1451_fu_21287_p4.read().is_01() || !ap_const_lv8_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_1451_fu_21287_p4.read() == ap_const_lv8_0);
}

void mnist_fp16_opt::thread_icmp17_fu_20838_p2() {
    icmp17_fu_20838_p2 = (!tmp_1457_fu_20828_p4.read().is_01() || !ap_const_lv26_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_1457_fu_20828_p4.read() == ap_const_lv26_0);
}

void mnist_fp16_opt::thread_icmp18_fu_20309_p2() {
    icmp18_fu_20309_p2 = (!tmp_1472_fu_20299_p4.read().is_01() || !ap_const_lv8_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_1472_fu_20299_p4.read() == ap_const_lv8_0);
}

void mnist_fp16_opt::thread_icmp19_fu_20429_p2() {
    icmp19_fu_20429_p2 = (!tmp_1479_fu_20419_p4.read().is_01() || !ap_const_lv8_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_1479_fu_20419_p4.read() == ap_const_lv8_0);
}

void mnist_fp16_opt::thread_icmp1_fu_8470_p2() {
    icmp1_fu_8470_p2 = (!tmp_513_fu_8460_p4.read().is_01() || !ap_const_lv26_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_513_fu_8460_p4.read() == ap_const_lv26_0);
}

void mnist_fp16_opt::thread_icmp20_fu_22012_p2() {
    icmp20_fu_22012_p2 = (!tmp_1486_fu_22002_p4.read().is_01() || !ap_const_lv8_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_1486_fu_22002_p4.read() == ap_const_lv8_0);
}

void mnist_fp16_opt::thread_icmp21_fu_21674_p2() {
    icmp21_fu_21674_p2 = (!tmp_1493_fu_21664_p4.read().is_01() || !ap_const_lv26_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_1493_fu_21664_p4.read() == ap_const_lv26_0);
}

void mnist_fp16_opt::thread_icmp22_fu_22329_p2() {
    icmp22_fu_22329_p2 = (!tmp_1506_fu_22319_p4.read().is_01() || !ap_const_lv26_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_1506_fu_22319_p4.read() == ap_const_lv26_0);
}

void mnist_fp16_opt::thread_icmp2_fu_10211_p2() {
    icmp2_fu_10211_p2 = (!tmp_604_fu_10201_p4.read().is_01() || !ap_const_lv8_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_604_fu_10201_p4.read() == ap_const_lv8_0);
}

void mnist_fp16_opt::thread_icmp3_fu_9736_p2() {
    icmp3_fu_9736_p2 = (!tmp_626_fu_9726_p4.read().is_01() || !ap_const_lv26_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_626_fu_9726_p4.read() == ap_const_lv26_0);
}

void mnist_fp16_opt::thread_icmp4_fu_9207_p2() {
    icmp4_fu_9207_p2 = (!tmp_688_fu_9197_p4.read().is_01() || !ap_const_lv8_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_688_fu_9197_p4.read() == ap_const_lv8_0);
}

void mnist_fp16_opt::thread_icmp5_fu_9292_p2() {
    icmp5_fu_9292_p2 = (!tmp_695_fu_9282_p4.read().is_01() || !ap_const_lv8_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_695_fu_9282_p4.read() == ap_const_lv8_0);
}

void mnist_fp16_opt::thread_icmp6_fu_10809_p2() {
    icmp6_fu_10809_p2 = (!tmp_771_fu_10799_p4.read().is_01() || !ap_const_lv26_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_771_fu_10799_p4.read() == ap_const_lv26_0);
}

void mnist_fp16_opt::thread_icmp7_fu_13648_p2() {
    icmp7_fu_13648_p2 = (!tmp_897_fu_13638_p4.read().is_01() || !ap_const_lv8_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_897_fu_13638_p4.read() == ap_const_lv8_0);
}

void mnist_fp16_opt::thread_icmp8_fu_14504_p2() {
    icmp8_fu_14504_p2 = (!tmp_1174_fu_14494_p4.read().is_01() || !ap_const_lv26_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_1174_fu_14494_p4.read() == ap_const_lv26_0);
}

void mnist_fp16_opt::thread_icmp9_fu_16181_p2() {
    icmp9_fu_16181_p2 = (!tmp_1277_fu_16171_p4.read().is_01() || !ap_const_lv8_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_1277_fu_16171_p4.read() == ap_const_lv8_0);
}

void mnist_fp16_opt::thread_icmp_fu_7574_p2() {
    icmp_fu_7574_p2 = (!tmp_217_fu_7564_p4.read().is_01() || !ap_const_lv8_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_217_fu_7564_p4.read() == ap_const_lv8_0);
}

void mnist_fp16_opt::thread_image_0_0_address0() {
    if ((esl_seteq<1,1,1>(ap_enable_reg_pp3_iter2.read(), ap_const_logic_1) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage2.read()) && 
         esl_seteq<1,1,1>(ap_block_pp3_stage2.read(), ap_const_boolean_0))) {
        image_0_0_address0 =  (sc_lv<10>) (tmp_1089_cast_fu_6830_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_enable_reg_pp3_iter2.read(), ap_const_logic_1) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage1.read()) && 
                esl_seteq<1,1,1>(ap_block_pp3_stage1.read(), ap_const_boolean_0))) {
        image_0_0_address0 =  (sc_lv<10>) (tmp_299_cast_fu_6790_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage0.read()) && 
                esl_seteq<1,1,1>(ap_enable_reg_pp3_iter2.read(), ap_const_logic_1) && 
                esl_seteq<1,1,1>(ap_block_pp3_stage0.read(), ap_const_boolean_0))) {
        image_0_0_address0 =  (sc_lv<10>) (tmp_199_cast_fu_6707_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage14.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read()) && 
                esl_seteq<1,1,1>(ap_block_pp3_stage14.read(), ap_const_boolean_0))) {
        image_0_0_address0 =  (sc_lv<10>) (tmp_1053_cast_fu_6642_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage13.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read()) && 
                esl_seteq<1,1,1>(ap_block_pp3_stage13.read(), ap_const_boolean_0))) {
        image_0_0_address0 =  (sc_lv<10>) (tmp_1007_cast_fu_6572_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage12.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read()) && 
                esl_seteq<1,1,1>(ap_block_pp3_stage12.read(), ap_const_boolean_0))) {
        image_0_0_address0 =  (sc_lv<10>) (tmp_970_cast_fu_6502_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage11.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read()) && 
                esl_seteq<1,1,1>(ap_block_pp3_stage11.read(), ap_const_boolean_0))) {
        image_0_0_address0 =  (sc_lv<10>) (tmp_917_cast_fu_6432_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage10.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read()) && 
                esl_seteq<1,1,1>(ap_block_pp3_stage10.read(), ap_const_boolean_0))) {
        image_0_0_address0 =  (sc_lv<10>) (tmp_869_cast_fu_6362_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage9.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read()) && 
                esl_seteq<1,1,1>(ap_block_pp3_stage9.read(), ap_const_boolean_0))) {
        image_0_0_address0 =  (sc_lv<10>) (tmp_833_cast_fu_6292_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage8.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read()) && 
                esl_seteq<1,1,1>(ap_block_pp3_stage8.read(), ap_const_boolean_0))) {
        image_0_0_address0 =  (sc_lv<10>) (tmp_781_cast_fu_6222_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage7.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read()) && 
                esl_seteq<1,1,1>(ap_block_pp3_stage7.read(), ap_const_boolean_0))) {
        image_0_0_address0 =  (sc_lv<10>) (tmp_761_cast_fu_6152_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage6.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read()) && 
                esl_seteq<1,1,1>(ap_block_pp3_stage6.read(), ap_const_boolean_0))) {
        image_0_0_address0 =  (sc_lv<10>) (tmp_726_cast_fu_6082_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage5.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read()) && 
                esl_seteq<1,1,1>(ap_block_pp3_stage5.read(), ap_const_boolean_0))) {
        image_0_0_address0 =  (sc_lv<10>) (tmp_689_cast_fu_6012_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage4.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read()) && 
                esl_seteq<1,1,1>(ap_block_pp3_stage4.read(), ap_const_boolean_0))) {
        image_0_0_address0 =  (sc_lv<10>) (tmp_673_cast_fu_5942_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage0.read()) && 
                esl_seteq<1,1,1>(ap_enable_reg_pp1_iter1.read(), ap_const_logic_1) && 
                esl_seteq<1,1,1>(ap_block_pp1_stage0.read(), ap_const_boolean_0))) {
        image_0_0_address0 =  (sc_lv<10>) (tmp_258_cast_fu_4631_p1.read());
    } else {
        image_0_0_address0 =  (sc_lv<10>) ("XXXXXXXXXX");
    }
}

void mnist_fp16_opt::thread_image_0_0_address1() {
    if ((esl_seteq<1,1,1>(ap_enable_reg_pp3_iter2.read(), ap_const_logic_1) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage2.read()) && 
         esl_seteq<1,1,1>(ap_block_pp3_stage2.read(), ap_const_boolean_0))) {
        image_0_0_address1 =  (sc_lv<10>) (tmp_1088_cast_fu_6820_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_enable_reg_pp3_iter2.read(), ap_const_logic_1) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage1.read()) && 
                esl_seteq<1,1,1>(ap_block_pp3_stage1.read(), ap_const_boolean_0))) {
        image_0_0_address1 =  (sc_lv<10>) (tmp_296_cast_fu_6779_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage0.read()) && 
                esl_seteq<1,1,1>(ap_enable_reg_pp3_iter2.read(), ap_const_logic_1) && 
                esl_seteq<1,1,1>(ap_block_pp3_stage0.read(), ap_const_boolean_0))) {
        image_0_0_address1 =  (sc_lv<10>) (tmp_207_cast_fu_6718_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage14.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read()) && 
                esl_seteq<1,1,1>(ap_block_pp3_stage14.read(), ap_const_boolean_0))) {
        image_0_0_address1 =  (sc_lv<10>) (tmp_1052_cast_fu_6632_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage13.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read()) && 
                esl_seteq<1,1,1>(ap_block_pp3_stage13.read(), ap_const_boolean_0))) {
        image_0_0_address1 =  (sc_lv<10>) (tmp_1006_cast_fu_6562_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage12.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read()) && 
                esl_seteq<1,1,1>(ap_block_pp3_stage12.read(), ap_const_boolean_0))) {
        image_0_0_address1 =  (sc_lv<10>) (tmp_969_cast_fu_6492_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage11.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read()) && 
                esl_seteq<1,1,1>(ap_block_pp3_stage11.read(), ap_const_boolean_0))) {
        image_0_0_address1 =  (sc_lv<10>) (tmp_916_cast_fu_6422_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage10.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read()) && 
                esl_seteq<1,1,1>(ap_block_pp3_stage10.read(), ap_const_boolean_0))) {
        image_0_0_address1 =  (sc_lv<10>) (tmp_868_cast_fu_6352_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage9.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read()) && 
                esl_seteq<1,1,1>(ap_block_pp3_stage9.read(), ap_const_boolean_0))) {
        image_0_0_address1 =  (sc_lv<10>) (tmp_832_cast_fu_6282_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage8.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read()) && 
                esl_seteq<1,1,1>(ap_block_pp3_stage8.read(), ap_const_boolean_0))) {
        image_0_0_address1 =  (sc_lv<10>) (tmp_780_cast_fu_6212_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage7.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read()) && 
                esl_seteq<1,1,1>(ap_block_pp3_stage7.read(), ap_const_boolean_0))) {
        image_0_0_address1 =  (sc_lv<10>) (tmp_760_cast_fu_6142_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage6.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read()) && 
                esl_seteq<1,1,1>(ap_block_pp3_stage6.read(), ap_const_boolean_0))) {
        image_0_0_address1 =  (sc_lv<10>) (tmp_725_cast_fu_6072_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage5.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read()) && 
                esl_seteq<1,1,1>(ap_block_pp3_stage5.read(), ap_const_boolean_0))) {
        image_0_0_address1 =  (sc_lv<10>) (tmp_688_cast_fu_6002_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage4.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read()) && 
                esl_seteq<1,1,1>(ap_block_pp3_stage4.read(), ap_const_boolean_0))) {
        image_0_0_address1 =  (sc_lv<10>) (tmp_672_cast_fu_5932_p1.read());
    } else {
        image_0_0_address1 =  (sc_lv<10>) ("XXXXXXXXXX");
    }
}

void mnist_fp16_opt::thread_image_0_0_ce0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage0.read()) && 
          esl_seteq<1,1,1>(ap_enable_reg_pp3_iter2.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage0_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage0.read()) && 
          esl_seteq<1,1,1>(ap_enable_reg_pp1_iter1.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_block_pp1_stage0_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage5.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage5_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage4.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage4_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage6.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage6_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage7.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage7_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage8.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage8_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage9.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage9_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage10.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage10_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage11.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage11_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage12.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage12_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage13.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage13_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage14.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage14_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read())) || 
         (esl_seteq<1,1,1>(ap_enable_reg_pp3_iter2.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage1.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage1_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_enable_reg_pp3_iter2.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage2.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage2_11001.read(), ap_const_boolean_0)))) {
        image_0_0_ce0 = ap_const_logic_1;
    } else {
        image_0_0_ce0 = ap_const_logic_0;
    }
}

void mnist_fp16_opt::thread_image_0_0_ce1() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage0.read()) && 
          esl_seteq<1,1,1>(ap_enable_reg_pp3_iter2.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage0_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage5.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage5_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage4.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage4_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage6.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage6_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage7.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage7_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage8.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage8_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage9.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage9_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage10.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage10_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage11.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage11_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage12.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage12_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage13.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage13_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage14.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage14_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read())) || 
         (esl_seteq<1,1,1>(ap_enable_reg_pp3_iter2.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage1.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage1_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_enable_reg_pp3_iter2.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage2.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage2_11001.read(), ap_const_boolean_0)))) {
        image_0_0_ce1 = ap_const_logic_1;
    } else {
        image_0_0_ce1 = ap_const_logic_0;
    }
}

void mnist_fp16_opt::thread_image_0_0_d0() {
    image_0_0_d0 = ap_phi_mux_pixel_data_V_2_phi_fu_2230_p4.read();
}

void mnist_fp16_opt::thread_image_0_0_we0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage0.read()) && 
         esl_seteq<1,1,1>(ap_enable_reg_pp1_iter1.read(), ap_const_logic_1) && 
         esl_seteq<1,1,1>(ap_const_lv1_0, tmp_2_reg_23102.read()) && 
         esl_seteq<1,1,1>(ap_block_pp1_stage0_11001.read(), ap_const_boolean_0))) {
        image_0_0_we0 = ap_const_logic_1;
    } else {
        image_0_0_we0 = ap_const_logic_0;
    }
}

void mnist_fp16_opt::thread_index_V_1_fu_22701_p3() {
    index_V_1_fu_22701_p3 = (!tmp_671_reg_28101.read()[0].is_01())? sc_lv<4>(): ((tmp_671_reg_28101.read()[0].to_bool())? index_V_reg_4315_pp27_iter2_reg.read(): p_77_reg_4302.read());
}

void mnist_fp16_opt::thread_index_tuple1_V_fu_7838_p2() {
    index_tuple1_V_fu_7838_p2 = (!ap_const_lv5_1.is_01() || !p_3_mid_fu_7804_p3.read().is_01())? sc_lv<5>(): (sc_biguint<5>(ap_const_lv5_1) + sc_biguint<5>(p_3_mid_fu_7804_p3.read()));
}

void mnist_fp16_opt::thread_index_tuple2_V_fu_11630_p2() {
    index_tuple2_V_fu_11630_p2 = (!p_20_mid2_reg_24906.read().is_01() || !ap_const_lv5_1.is_01())? sc_lv<5>(): (sc_biguint<5>(p_20_mid2_reg_24906.read()) + sc_biguint<5>(ap_const_lv5_1));
}

void mnist_fp16_opt::thread_index_tuple3_V_fu_13956_p2() {
    index_tuple3_V_fu_13956_p2 = (!ap_const_lv5_1.is_01() || !p_31_mid_fu_13898_p3.read().is_01())? sc_lv<5>(): (sc_biguint<5>(ap_const_lv5_1) + sc_biguint<5>(p_31_mid_fu_13898_p3.read()));
}

void mnist_fp16_opt::thread_index_tuple4_V_fu_17374_p2() {
    index_tuple4_V_fu_17374_p2 = (!p_46_mid2_reg_26501.read().is_01() || !ap_const_lv4_1.is_01())? sc_lv<4>(): (sc_biguint<4>(p_46_mid2_reg_26501.read()) + sc_biguint<4>(ap_const_lv4_1));
}

void mnist_fp16_opt::thread_index_tuple5_V_fu_19088_p2() {
    index_tuple5_V_fu_19088_p2 = (!p_57_mid_fu_19022_p3.read().is_01() || !ap_const_lv4_1.is_01())? sc_lv<4>(): (sc_biguint<4>(p_57_mid_fu_19022_p3.read()) + sc_biguint<4>(ap_const_lv4_1));
}

void mnist_fp16_opt::thread_index_tuple_V_fu_4636_p2() {
    index_tuple_V_fu_4636_p2 = (!ap_phi_mux_p_s_phi_fu_2277_p4.read().is_01() || !ap_const_lv5_1.is_01())? sc_lv<5>(): (sc_biguint<5>(ap_phi_mux_p_s_phi_fu_2277_p4.read()) + sc_biguint<5>(ap_const_lv5_1));
}

void mnist_fp16_opt::thread_indvar_flatten122_op_fu_9638_p2() {
    indvar_flatten122_op_fu_9638_p2 = (!ap_const_lv10_1.is_01() || !indvar_flatten8_reg_2719.read().is_01())? sc_lv<10>(): (sc_biguint<10>(ap_const_lv10_1) + sc_biguint<10>(indvar_flatten8_reg_2719.read()));
}

void mnist_fp16_opt::thread_indvar_flatten148_op_fu_9998_p2() {
    indvar_flatten148_op_fu_9998_p2 = (!ap_const_lv10_1.is_01() || !indvar_flatten10_reg_2842.read().is_01())? sc_lv<10>(): (sc_biguint<10>(ap_const_lv10_1) + sc_biguint<10>(indvar_flatten10_reg_2842.read()));
}

void mnist_fp16_opt::thread_indvar_flatten181_op_fu_11021_p2() {
    indvar_flatten181_op_fu_11021_p2 = (!indvar_flatten14_reg_2897.read().is_01() || !ap_const_lv8_1.is_01())? sc_lv<8>(): (sc_biguint<8>(indvar_flatten14_reg_2897.read()) + sc_biguint<8>(ap_const_lv8_1));
}

void mnist_fp16_opt::thread_indvar_flatten218_op_fu_13198_p2() {
    indvar_flatten218_op_fu_13198_p2 = (!ap_const_lv4_1.is_01() || !ap_phi_mux_indvar_flatten22_phi_fu_3182_p4.read().is_01())? sc_lv<4>(): (sc_biguint<4>(ap_const_lv4_1) + sc_biguint<4>(ap_phi_mux_indvar_flatten22_phi_fu_3182_p4.read()));
}

void mnist_fp16_opt::thread_indvar_flatten247_op_fu_13282_p2() {
    indvar_flatten247_op_fu_13282_p2 = (!indvar_flatten18_reg_3122.read().is_01() || !ap_const_lv8_1.is_01())? sc_lv<8>(): (sc_biguint<8>(indvar_flatten18_reg_3122.read()) + sc_biguint<8>(ap_const_lv8_1));
}

void mnist_fp16_opt::thread_indvar_flatten273_op_fu_13435_p2() {
    indvar_flatten273_op_fu_13435_p2 = (!ap_const_lv8_1.is_01() || !indvar_flatten20_reg_3246.read().is_01())? sc_lv<8>(): (sc_biguint<8>(ap_const_lv8_1) + sc_biguint<8>(indvar_flatten20_reg_3246.read()));
}

void mnist_fp16_opt::thread_indvar_flatten294_op_fu_14058_p2() {
    indvar_flatten294_op_fu_14058_p2 = (!indvar_flatten24_reg_3301.read().is_01() || !ap_const_lv10_1.is_01())? sc_lv<10>(): (sc_biguint<10>(indvar_flatten24_reg_3301.read()) + sc_biguint<10>(ap_const_lv10_1));
}

void mnist_fp16_opt::thread_indvar_flatten337_op_fu_14965_p2() {
    indvar_flatten337_op_fu_14965_p2 = (!ap_const_lv4_1.is_01() || !indvar_flatten30_reg_3427.read().is_01())? sc_lv<4>(): (sc_biguint<4>(ap_const_lv4_1) + sc_biguint<4>(indvar_flatten30_reg_3427.read()));
}

void mnist_fp16_opt::thread_indvar_flatten33_op_fu_7361_p2() {
    indvar_flatten33_op_fu_7361_p2 = (!ap_const_lv10_1.is_01() || !indvar_flatten3_reg_2590.read().is_01())? sc_lv<10>(): (sc_biguint<10>(ap_const_lv10_1) + sc_biguint<10>(indvar_flatten3_reg_2590.read()));
}

void mnist_fp16_opt::thread_indvar_flatten366_op_fu_15608_p2() {
    indvar_flatten366_op_fu_15608_p2 = (!ap_const_lv8_1.is_01() || !indvar_flatten26_reg_3371.read().is_01())? sc_lv<8>(): (sc_biguint<8>(ap_const_lv8_1) + sc_biguint<8>(indvar_flatten26_reg_3371.read()));
}

void mnist_fp16_opt::thread_indvar_flatten392_op_fu_15968_p2() {
    indvar_flatten392_op_fu_15968_p2 = (!ap_const_lv8_1.is_01() || !indvar_flatten28_reg_3494.read().is_01())? sc_lv<8>(): (sc_biguint<8>(ap_const_lv8_1) + sc_biguint<8>(indvar_flatten28_reg_3494.read()));
}

void mnist_fp16_opt::thread_indvar_flatten420_op_fu_16976_p2() {
    indvar_flatten420_op_fu_16976_p2 = (!indvar_flatten32_reg_3549.read().is_01() || !ap_const_lv6_1.is_01())? sc_lv<6>(): (sc_biguint<6>(indvar_flatten32_reg_3549.read()) + sc_biguint<6>(ap_const_lv6_1));
}

void mnist_fp16_opt::thread_indvar_flatten457_op_fu_18275_p2() {
    indvar_flatten457_op_fu_18275_p2 = (!ap_const_lv4_1.is_01() || !ap_phi_mux_indvar_flatten40_phi_fu_3797_p4.read().is_01())? sc_lv<4>(): (sc_biguint<4>(ap_const_lv4_1) + sc_biguint<4>(ap_phi_mux_indvar_flatten40_phi_fu_3797_p4.read()));
}

void mnist_fp16_opt::thread_indvar_flatten486_op_fu_18444_p2() {
    indvar_flatten486_op_fu_18444_p2 = (!indvar_flatten36_reg_3737.read().is_01() || !ap_const_lv6_1.is_01())? sc_lv<6>(): (sc_biguint<6>(indvar_flatten36_reg_3737.read()) + sc_biguint<6>(ap_const_lv6_1));
}

void mnist_fp16_opt::thread_indvar_flatten514_op_fu_18589_p2() {
    indvar_flatten514_op_fu_18589_p2 = (!ap_const_lv6_1.is_01() || !indvar_flatten38_reg_3861.read().is_01())? sc_lv<6>(): (sc_biguint<6>(ap_const_lv6_1) + sc_biguint<6>(indvar_flatten38_reg_3861.read()));
}

void mnist_fp16_opt::thread_indvar_flatten535_op_fu_19188_p2() {
    indvar_flatten535_op_fu_19188_p2 = (!indvar_flatten42_reg_3916.read().is_01() || !ap_const_lv8_1.is_01())? sc_lv<8>(): (sc_biguint<8>(indvar_flatten42_reg_3916.read()) + sc_biguint<8>(ap_const_lv8_1));
}

void mnist_fp16_opt::thread_indvar_flatten54_op_fu_7872_p2() {
    indvar_flatten54_op_fu_7872_p2 = (!indvar_flatten5_reg_2645.read().is_01() || !ap_const_lv10_1.is_01())? sc_lv<10>(): (sc_biguint<10>(indvar_flatten5_reg_2645.read()) + sc_biguint<10>(ap_const_lv10_1));
}

void mnist_fp16_opt::thread_indvar_flatten572_op_fu_20012_p2() {
    indvar_flatten572_op_fu_20012_p2 = (!ap_const_lv4_1.is_01() || !indvar_flatten48_reg_4042.read().is_01())? sc_lv<4>(): (sc_biguint<4>(ap_const_lv4_1) + sc_biguint<4>(indvar_flatten48_reg_4042.read()));
}

void mnist_fp16_opt::thread_indvar_flatten601_op_fu_20740_p2() {
    indvar_flatten601_op_fu_20740_p2 = (!ap_const_lv6_1.is_01() || !indvar_flatten44_reg_3986.read().is_01())? sc_lv<6>(): (sc_biguint<6>(ap_const_lv6_1) + sc_biguint<6>(indvar_flatten44_reg_3986.read()));
}

void mnist_fp16_opt::thread_indvar_flatten629_op_fu_21092_p2() {
    indvar_flatten629_op_fu_21092_p2 = (!ap_const_lv6_1.is_01() || !indvar_flatten46_reg_4109.read().is_01())? sc_lv<6>(): (sc_biguint<6>(ap_const_lv6_1) + sc_biguint<6>(indvar_flatten46_reg_4109.read()));
}

void mnist_fp16_opt::thread_indvar_flatten7_op_fu_7208_p2() {
    indvar_flatten7_op_fu_7208_p2 = (!indvar_flatten7_reg_2488.read().is_01() || !ap_const_lv10_1.is_01())? sc_lv<10>(): (sc_biguint<10>(indvar_flatten7_reg_2488.read()) + sc_biguint<10>(ap_const_lv10_1));
}

void mnist_fp16_opt::thread_indvar_flatten93_op_fu_8978_p2() {
    indvar_flatten93_op_fu_8978_p2 = (!ap_const_lv4_1.is_01() || !indvar_flatten12_reg_2775.read().is_01())? sc_lv<4>(): (sc_biguint<4>(ap_const_lv4_1) + sc_biguint<4>(indvar_flatten12_reg_2775.read()));
}

void mnist_fp16_opt::thread_indvar_flatten_next1_10_fu_13301_p2() {
    indvar_flatten_next1_10_fu_13301_p2 = (!indvar_flatten19_reg_3224.read().is_01() || !ap_const_lv11_1.is_01())? sc_lv<11>(): (sc_biguint<11>(indvar_flatten19_reg_3224.read()) + sc_biguint<11>(ap_const_lv11_1));
}

void mnist_fp16_opt::thread_indvar_flatten_next1_1_fu_7878_p3() {
    indvar_flatten_next1_1_fu_7878_p3 = (!exitcond_flatten6_fu_7798_p2.read()[0].is_01())? sc_lv<10>(): ((exitcond_flatten6_fu_7798_p2.read()[0].to_bool())? ap_const_lv10_1: indvar_flatten54_op_fu_7872_p2.read());
}

void mnist_fp16_opt::thread_indvar_flatten_next1_2_fu_7786_p2() {
    indvar_flatten_next1_2_fu_7786_p2 = (!indvar_flatten4_reg_2623.read().is_01() || !ap_const_lv12_1.is_01())? sc_lv<12>(): (sc_biguint<12>(indvar_flatten4_reg_2623.read()) + sc_biguint<12>(ap_const_lv12_1));
}

void mnist_fp16_opt::thread_indvar_flatten_next1_3_fu_8762_p2() {
    indvar_flatten_next1_3_fu_8762_p2 = (!indvar_flatten11_reg_2753.read().is_01() || !ap_const_lv6_1.is_01())? sc_lv<6>(): (sc_biguint<6>(indvar_flatten11_reg_2753.read()) + sc_biguint<6>(ap_const_lv6_1));
}

void mnist_fp16_opt::thread_indvar_flatten_next1_4_fu_11027_p3() {
    indvar_flatten_next1_4_fu_11027_p3 = (!exitcond_flatten14_reg_24720.read()[0].is_01())? sc_lv<8>(): ((exitcond_flatten14_reg_24720.read()[0].to_bool())? ap_const_lv8_1: indvar_flatten181_op_fu_11021_p2.read());
}

void mnist_fp16_opt::thread_indvar_flatten_next1_5_fu_10373_p2() {
    indvar_flatten_next1_5_fu_10373_p2 = (!indvar_flatten13_reg_2875.read().is_01() || !ap_const_lv10_1.is_01())? sc_lv<10>(): (sc_biguint<10>(indvar_flatten13_reg_2875.read()) + sc_biguint<10>(ap_const_lv10_1));
}

void mnist_fp16_opt::thread_indvar_flatten_next1_6_fu_10612_p2() {
    indvar_flatten_next1_6_fu_10612_p2 = (!indvar_flatten16_reg_2933.read().is_01() || !ap_const_lv3_1.is_01())? sc_lv<3>(): (sc_biguint<3>(indvar_flatten16_reg_2933.read()) + sc_biguint<3>(ap_const_lv3_1));
}

void mnist_fp16_opt::thread_indvar_flatten_next1_7_fu_13288_p3() {
    indvar_flatten_next1_7_fu_13288_p3 = (!exitcond_flatten18_reg_25418.read()[0].is_01())? sc_lv<8>(): ((exitcond_flatten18_reg_25418.read()[0].to_bool())? ap_const_lv8_1: indvar_flatten247_op_fu_13282_p2.read());
}

void mnist_fp16_opt::thread_indvar_flatten_next1_8_fu_12864_p2() {
    indvar_flatten_next1_8_fu_12864_p2 = (!indvar_flatten17_reg_3100.read().is_01() || !ap_const_lv11_1.is_01())? sc_lv<11>(): (sc_biguint<11>(indvar_flatten17_reg_3100.read()) + sc_biguint<11>(ap_const_lv11_1));
}

void mnist_fp16_opt::thread_indvar_flatten_next1_9_fu_13441_p3() {
    indvar_flatten_next1_9_fu_13441_p3 = (!exitcond_flatten21_fu_13313_p2.read()[0].is_01())? sc_lv<8>(): ((exitcond_flatten21_fu_13313_p2.read()[0].to_bool())? ap_const_lv8_1: indvar_flatten273_op_fu_13435_p2.read());
}

void mnist_fp16_opt::thread_indvar_flatten_next1_fu_7367_p3() {
    indvar_flatten_next1_fu_7367_p3 = (!exitcond_flatten4_fu_7239_p2.read()[0].is_01())? sc_lv<10>(): ((exitcond_flatten4_fu_7239_p2.read()[0].to_bool())? ap_const_lv10_1: indvar_flatten33_op_fu_7361_p2.read());
}

void mnist_fp16_opt::thread_indvar_flatten_next2_10_fu_15834_p2() {
    indvar_flatten_next2_10_fu_15834_p2 = (!indvar_flatten27_reg_3472.read().is_01() || !ap_const_lv11_1.is_01())? sc_lv<11>(): (sc_biguint<11>(indvar_flatten27_reg_3472.read()) + sc_biguint<11>(ap_const_lv11_1));
}

void mnist_fp16_opt::thread_indvar_flatten_next2_1_fu_13204_p3() {
    indvar_flatten_next2_1_fu_13204_p3 = (!exitcond_flatten22_fu_13057_p2.read()[0].is_01())? sc_lv<4>(): ((exitcond_flatten22_fu_13057_p2.read()[0].to_bool())? ap_const_lv4_1: indvar_flatten218_op_fu_13198_p2.read());
}

void mnist_fp16_opt::thread_indvar_flatten_next2_2_fu_13045_p2() {
    indvar_flatten_next2_2_fu_13045_p2 = (!ap_phi_mux_indvar_flatten21_phi_fu_3160_p4.read().is_01() || !ap_const_lv6_1.is_01())? sc_lv<6>(): (sc_biguint<6>(ap_phi_mux_indvar_flatten21_phi_fu_3160_p4.read()) + sc_biguint<6>(ap_const_lv6_1));
}

void mnist_fp16_opt::thread_indvar_flatten_next2_3_fu_14064_p3() {
    indvar_flatten_next2_3_fu_14064_p3 = (!exitcond_flatten24_fu_13892_p2.read()[0].is_01())? sc_lv<10>(): ((exitcond_flatten24_fu_13892_p2.read()[0].to_bool())? ap_const_lv10_1: indvar_flatten294_op_fu_14058_p2.read());
}

void mnist_fp16_opt::thread_indvar_flatten_next2_4_fu_13880_p2() {
    indvar_flatten_next2_4_fu_13880_p2 = (!ap_const_lv12_1.is_01() || !indvar_flatten23_reg_3279.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_1) + sc_biguint<12>(indvar_flatten23_reg_3279.read()));
}

void mnist_fp16_opt::thread_indvar_flatten_next2_5_fu_14971_p3() {
    indvar_flatten_next2_5_fu_14971_p3 = (!exitcond_flatten30_fu_14818_p2.read()[0].is_01())? sc_lv<4>(): ((exitcond_flatten30_fu_14818_p2.read()[0].to_bool())? ap_const_lv4_1: indvar_flatten337_op_fu_14965_p2.read());
}

void mnist_fp16_opt::thread_indvar_flatten_next2_6_fu_14806_p2() {
    indvar_flatten_next2_6_fu_14806_p2 = (!indvar_flatten29_reg_3405.read().is_01() || !ap_const_lv7_1.is_01())? sc_lv<7>(): (sc_biguint<7>(indvar_flatten29_reg_3405.read()) + sc_biguint<7>(ap_const_lv7_1));
}

void mnist_fp16_opt::thread_indvar_flatten_next2_7_fu_15614_p3() {
    indvar_flatten_next2_7_fu_15614_p3 = (!exitcond_flatten26_reg_25880.read()[0].is_01())? sc_lv<8>(): ((exitcond_flatten26_reg_25880.read()[0].to_bool())? ap_const_lv8_1: indvar_flatten366_op_fu_15608_p2.read());
}

void mnist_fp16_opt::thread_indvar_flatten_next2_8_fu_14625_p2() {
    indvar_flatten_next2_8_fu_14625_p2 = (!indvar_flatten25_reg_3349.read().is_01() || !ap_const_lv11_1.is_01())? sc_lv<11>(): (sc_biguint<11>(indvar_flatten25_reg_3349.read()) + sc_biguint<11>(ap_const_lv11_1));
}

void mnist_fp16_opt::thread_indvar_flatten_next2_9_fu_15974_p3() {
    indvar_flatten_next2_9_fu_15974_p3 = (!exitcond_flatten29_fu_15846_p2.read()[0].is_01())? sc_lv<8>(): ((exitcond_flatten29_fu_15846_p2.read()[0].to_bool())? ap_const_lv8_1: indvar_flatten392_op_fu_15968_p2.read());
}

void mnist_fp16_opt::thread_indvar_flatten_next2_fu_7227_p2() {
    indvar_flatten_next2_fu_7227_p2 = (!indvar_flatten2_reg_2568.read().is_01() || !ap_const_lv12_1.is_01())? sc_lv<12>(): (sc_biguint<12>(indvar_flatten2_reg_2568.read()) + sc_biguint<12>(ap_const_lv12_1));
}

void mnist_fp16_opt::thread_indvar_flatten_next3_10_fu_18463_p2() {
    indvar_flatten_next3_10_fu_18463_p2 = (!indvar_flatten37_reg_3839.read().is_01() || !ap_const_lv10_1.is_01())? sc_lv<10>(): (sc_biguint<10>(indvar_flatten37_reg_3839.read()) + sc_biguint<10>(ap_const_lv10_1));
}

void mnist_fp16_opt::thread_indvar_flatten_next3_1_fu_11040_p2() {
    indvar_flatten_next3_1_fu_11040_p2 = (!ap_phi_mux_indvar_flatten15_phi_fu_2983_p4.read().is_01() || !ap_const_lv7_1.is_01())? sc_lv<7>(): (sc_biguint<7>(ap_phi_mux_indvar_flatten15_phi_fu_2983_p4.read()) + sc_biguint<7>(ap_const_lv7_1));
}

void mnist_fp16_opt::thread_indvar_flatten_next3_2_fu_16982_p3() {
    indvar_flatten_next3_2_fu_16982_p3 = (!exitcond_flatten32_reg_26320.read()[0].is_01())? sc_lv<6>(): ((exitcond_flatten32_reg_26320.read()[0].to_bool())? ap_const_lv6_1: indvar_flatten420_op_fu_16976_p2.read());
}

void mnist_fp16_opt::thread_indvar_flatten_next3_3_fu_16343_p2() {
    indvar_flatten_next3_3_fu_16343_p2 = (!indvar_flatten31_reg_3527.read().is_01() || !ap_const_lv9_1.is_01())? sc_lv<9>(): (sc_biguint<9>(indvar_flatten31_reg_3527.read()) + sc_biguint<9>(ap_const_lv9_1));
}

void mnist_fp16_opt::thread_indvar_flatten_next3_4_fu_16567_p2() {
    indvar_flatten_next3_4_fu_16567_p2 = (!indvar_flatten34_reg_3585.read().is_01() || !ap_const_lv3_1.is_01())? sc_lv<3>(): (sc_biguint<3>(indvar_flatten34_reg_3585.read()) + sc_biguint<3>(ap_const_lv3_1));
}

void mnist_fp16_opt::thread_indvar_flatten_next3_5_fu_18281_p3() {
    indvar_flatten_next3_5_fu_18281_p3 = (!exitcond_flatten40_fu_18174_p2.read()[0].is_01())? sc_lv<4>(): ((exitcond_flatten40_fu_18174_p2.read()[0].to_bool())? ap_const_lv4_1: indvar_flatten457_op_fu_18275_p2.read());
}

void mnist_fp16_opt::thread_indvar_flatten_next3_6_fu_18162_p2() {
    indvar_flatten_next3_6_fu_18162_p2 = (!ap_phi_mux_indvar_flatten39_phi_fu_3775_p4.read().is_01() || !ap_const_lv7_1.is_01())? sc_lv<7>(): (sc_biguint<7>(ap_phi_mux_indvar_flatten39_phi_fu_3775_p4.read()) + sc_biguint<7>(ap_const_lv7_1));
}

void mnist_fp16_opt::thread_indvar_flatten_next3_7_fu_18450_p3() {
    indvar_flatten_next3_7_fu_18450_p3 = (!exitcond_flatten36_reg_26810.read()[0].is_01())? sc_lv<6>(): ((exitcond_flatten36_reg_26810.read()[0].to_bool())? ap_const_lv6_1: indvar_flatten486_op_fu_18444_p2.read());
}

void mnist_fp16_opt::thread_indvar_flatten_next3_8_fu_18001_p2() {
    indvar_flatten_next3_8_fu_18001_p2 = (!indvar_flatten35_reg_3715.read().is_01() || !ap_const_lv10_1.is_01())? sc_lv<10>(): (sc_biguint<10>(indvar_flatten35_reg_3715.read()) + sc_biguint<10>(ap_const_lv10_1));
}

void mnist_fp16_opt::thread_indvar_flatten_next3_9_fu_18595_p3() {
    indvar_flatten_next3_9_fu_18595_p3 = (!exitcond_flatten39_fu_18475_p2.read()[0].is_01())? sc_lv<6>(): ((exitcond_flatten39_fu_18475_p2.read()[0].to_bool())? ap_const_lv6_1: indvar_flatten514_op_fu_18589_p2.read());
}

void mnist_fp16_opt::thread_indvar_flatten_next3_fu_6881_p2() {
    indvar_flatten_next3_fu_6881_p2 = (!indvar_flatten1_reg_2465.read().is_01() || !ap_const_lv12_1.is_01())? sc_lv<12>(): (sc_biguint<12>(indvar_flatten1_reg_2465.read()) + sc_biguint<12>(ap_const_lv12_1));
}

void mnist_fp16_opt::thread_indvar_flatten_next4_10_fu_21501_p2() {
    indvar_flatten_next4_10_fu_21501_p2 = (!indvar_flatten49_reg_4153.read().is_01() || !ap_const_lv6_1.is_01())? sc_lv<6>(): (sc_biguint<6>(indvar_flatten49_reg_4153.read()) + sc_biguint<6>(ap_const_lv6_1));
}

void mnist_fp16_opt::thread_indvar_flatten_next4_1_fu_16995_p2() {
    indvar_flatten_next4_1_fu_16995_p2 = (!ap_phi_mux_indvar_flatten33_phi_fu_3635_p4.read().is_01() || !ap_const_lv7_1.is_01())? sc_lv<7>(): (sc_biguint<7>(ap_phi_mux_indvar_flatten33_phi_fu_3635_p4.read()) + sc_biguint<7>(ap_const_lv7_1));
}

void mnist_fp16_opt::thread_indvar_flatten_next4_2_fu_19194_p3() {
    indvar_flatten_next4_2_fu_19194_p3 = (!exitcond_flatten42_fu_19016_p2.read()[0].is_01())? sc_lv<8>(): ((exitcond_flatten42_fu_19016_p2.read()[0].to_bool())? ap_const_lv8_1: indvar_flatten535_op_fu_19188_p2.read());
}

void mnist_fp16_opt::thread_indvar_flatten_next4_3_fu_19004_p2() {
    indvar_flatten_next4_3_fu_19004_p2 = (!indvar_flatten41_reg_3894.read().is_01() || !ap_const_lv11_1.is_01())? sc_lv<11>(): (sc_biguint<11>(indvar_flatten41_reg_3894.read()) + sc_biguint<11>(ap_const_lv11_1));
}

void mnist_fp16_opt::thread_indvar_flatten_next4_4_fu_20018_p3() {
    indvar_flatten_next4_4_fu_20018_p3 = (!exitcond_flatten48_fu_19905_p2.read()[0].is_01())? sc_lv<4>(): ((exitcond_flatten48_fu_19905_p2.read()[0].to_bool())? ap_const_lv4_1: indvar_flatten572_op_fu_20012_p2.read());
}

void mnist_fp16_opt::thread_indvar_flatten_next4_5_fu_19893_p2() {
    indvar_flatten_next4_5_fu_19893_p2 = (!indvar_flatten47_reg_4020.read().is_01() || !ap_const_lv8_1.is_01())? sc_lv<8>(): (sc_biguint<8>(indvar_flatten47_reg_4020.read()) + sc_biguint<8>(ap_const_lv8_1));
}

void mnist_fp16_opt::thread_indvar_flatten_next4_6_fu_20746_p3() {
    indvar_flatten_next4_6_fu_20746_p3 = (!exitcond_flatten44_reg_27258.read()[0].is_01())? sc_lv<6>(): ((exitcond_flatten44_reg_27258.read()[0].to_bool())? ap_const_lv6_1: indvar_flatten601_op_fu_20740_p2.read());
}

void mnist_fp16_opt::thread_indvar_flatten_next4_7_fu_19720_p2() {
    indvar_flatten_next4_7_fu_19720_p2 = (!indvar_flatten43_reg_3964.read().is_01() || !ap_const_lv10_1.is_01())? sc_lv<10>(): (sc_biguint<10>(indvar_flatten43_reg_3964.read()) + sc_biguint<10>(ap_const_lv10_1));
}

void mnist_fp16_opt::thread_indvar_flatten_next4_8_fu_21098_p3() {
    indvar_flatten_next4_8_fu_21098_p3 = (!exitcond_flatten47_fu_20978_p2.read()[0].is_01())? sc_lv<6>(): ((exitcond_flatten47_fu_20978_p2.read()[0].to_bool())? ap_const_lv6_1: indvar_flatten629_op_fu_21092_p2.read());
}

void mnist_fp16_opt::thread_indvar_flatten_next4_9_fu_20966_p2() {
    indvar_flatten_next4_9_fu_20966_p2 = (!indvar_flatten45_reg_4087.read().is_01() || !ap_const_lv10_1.is_01())? sc_lv<10>(): (sc_biguint<10>(indvar_flatten45_reg_4087.read()) + sc_biguint<10>(ap_const_lv10_1));
}

void mnist_fp16_opt::thread_indvar_flatten_next4_fu_10004_p3() {
    indvar_flatten_next4_fu_10004_p3 = (!exitcond_flatten11_fu_9876_p2.read()[0].is_01())? sc_lv<10>(): ((exitcond_flatten11_fu_9876_p2.read()[0].to_bool())? ap_const_lv10_1: indvar_flatten148_op_fu_9998_p2.read());
}

void mnist_fp16_opt::thread_indvar_flatten_next5_fu_9864_p2() {
    indvar_flatten_next5_fu_9864_p2 = (!indvar_flatten9_reg_2820.read().is_01() || !ap_const_lv12_1.is_01())? sc_lv<12>(): (sc_biguint<12>(indvar_flatten9_reg_2820.read()) + sc_biguint<12>(ap_const_lv12_1));
}

void mnist_fp16_opt::thread_indvar_flatten_next6_fu_9644_p3() {
    indvar_flatten_next6_fu_9644_p3 = (!exitcond_flatten8_reg_24270.read()[0].is_01())? sc_lv<10>(): ((exitcond_flatten8_reg_24270.read()[0].to_bool())? ap_const_lv10_1: indvar_flatten122_op_fu_9638_p2.read());
}

void mnist_fp16_opt::thread_indvar_flatten_next7_fu_8591_p2() {
    indvar_flatten_next7_fu_8591_p2 = (!indvar_flatten6_reg_2696.read().is_01() || !ap_const_lv12_1.is_01())? sc_lv<12>(): (sc_biguint<12>(indvar_flatten6_reg_2696.read()) + sc_biguint<12>(ap_const_lv12_1));
}

void mnist_fp16_opt::thread_indvar_flatten_next8_fu_7214_p3() {
    indvar_flatten_next8_fu_7214_p3 = (!exitcond_flatten2_reg_23809.read()[0].is_01())? sc_lv<10>(): ((exitcond_flatten2_reg_23809.read()[0].to_bool())? ap_const_lv10_1: indvar_flatten7_op_fu_7208_p2.read());
}

void mnist_fp16_opt::thread_indvar_flatten_next9_fu_8984_p3() {
    indvar_flatten_next9_fu_8984_p3 = (!exitcond_flatten12_fu_8774_p2.read()[0].is_01())? sc_lv<4>(): ((exitcond_flatten12_fu_8774_p2.read()[0].to_bool())? ap_const_lv4_1: indvar_flatten93_op_fu_8978_p2.read());
}

void mnist_fp16_opt::thread_indvar_flatten_next_fu_7057_p2() {
    indvar_flatten_next_fu_7057_p2 = (!ap_phi_mux_indvar_flatten_phi_fu_2526_p4.read().is_01() || !ap_const_lv4_1.is_01())? sc_lv<4>(): (sc_biguint<4>(ap_phi_mux_indvar_flatten_phi_fu_2526_p4.read()) + sc_biguint<4>(ap_const_lv4_1));
}

void mnist_fp16_opt::thread_ireg_V_12_fu_9056_p1() {
    ireg_V_12_fu_9056_p1 = grp_fu_4358_p1.read();
}

void mnist_fp16_opt::thread_ireg_V_13_fu_9092_p1() {
    ireg_V_13_fu_9092_p1 = grp_fu_4361_p1.read();
}

void mnist_fp16_opt::thread_ireg_V_14_fu_15026_p1() {
    ireg_V_14_fu_15026_p1 = grp_fu_4358_p1.read();
}

void mnist_fp16_opt::thread_ireg_V_15_fu_15062_p1() {
    ireg_V_15_fu_15062_p1 = grp_fu_4361_p1.read();
}

void mnist_fp16_opt::thread_ireg_V_16_fu_20158_p1() {
    ireg_V_16_fu_20158_p1 = grp_fu_4358_p1.read();
}

void mnist_fp16_opt::thread_ireg_V_17_fu_20194_p1() {
    ireg_V_17_fu_20194_p1 = grp_fu_4361_p1.read();
}

void mnist_fp16_opt::thread_ireg_V_1_fu_21178_p3() {
    ireg_V_1_fu_21178_p3 = (!tmp_631_fu_21169_p2.read()[0].is_01())? sc_lv<64>(): ((tmp_631_fu_21169_p2.read()[0].to_bool())? ap_const_lv64_0: tmp_633_fu_21174_p1.read());
}

void mnist_fp16_opt::thread_ireg_V_3_fu_10092_p3() {
    ireg_V_3_fu_10092_p3 = (!tmp_253_fu_10083_p2.read()[0].is_01())? sc_lv<64>(): ((tmp_253_fu_10083_p2.read()[0].to_bool())? ap_const_lv64_0: tmp_272_fu_10088_p1.read());
}

void mnist_fp16_opt::thread_ireg_V_4_fu_13529_p3() {
    ireg_V_4_fu_13529_p3 = (!tmp_409_fu_13520_p2.read()[0].is_01())? sc_lv<64>(): ((tmp_409_fu_13520_p2.read()[0].to_bool())? ap_const_lv64_0: tmp_416_fu_13525_p1.read());
}

void mnist_fp16_opt::thread_ireg_V_7_fu_16062_p3() {
    ireg_V_7_fu_16062_p3 = (!tmp_531_fu_16053_p2.read()[0].is_01())? sc_lv<64>(): ((tmp_531_fu_16053_p2.read()[0].to_bool())? ap_const_lv64_0: tmp_544_fu_16058_p1.read());
}

void mnist_fp16_opt::thread_ireg_V_8_fu_18675_p3() {
    ireg_V_8_fu_18675_p3 = (!tmp_607_fu_18666_p2.read()[0].is_01())? sc_lv<64>(): ((tmp_607_fu_18666_p2.read()[0].to_bool())? ap_const_lv64_0: tmp_609_fu_18671_p1.read());
}

void mnist_fp16_opt::thread_ireg_V_fu_7455_p3() {
    ireg_V_fu_7455_p3 = (!tmp_51_fu_7446_p2.read()[0].is_01())? sc_lv<64>(): ((tmp_51_fu_7446_p2.read()[0].to_bool())? ap_const_lv64_0: tmp_68_fu_7451_p1.read());
}

void mnist_fp16_opt::thread_ireg_V_s_fu_21898_p1() {
    ireg_V_s_fu_21898_p1 = grp_fu_4358_p1.read();
}

void mnist_fp16_opt::thread_is_neg_1_fu_9651_p3() {
    is_neg_1_fu_9651_p3 = p_Val2_18_reg_2797.read().range(15, 15);
}

void mnist_fp16_opt::thread_is_neg_2_fu_14427_p3() {
    is_neg_2_fu_14427_p3 = p_Val2_23_reg_25815.read().range(15, 15);
}

void mnist_fp16_opt::thread_is_neg_3_fu_15621_p3() {
    is_neg_3_fu_15621_p3 = p_Val2_43_reg_3449.read().range(15, 15);
}

void mnist_fp16_opt::thread_is_neg_4_fu_19522_p3() {
    is_neg_4_fu_19522_p3 = p_Val2_46_reg_27193.read().range(15, 15);
}

void mnist_fp16_opt::thread_is_neg_5_fu_20753_p3() {
    is_neg_5_fu_20753_p3 = p_Val2_54_reg_4064.read().range(15, 15);
}

void mnist_fp16_opt::thread_is_neg_fu_8393_p3() {
    is_neg_fu_8393_p3 = p_Val2_s_reg_24205.read().range(15, 15);
}

void mnist_fp16_opt::thread_j1_V_fu_22583_p2() {
    j1_V_fu_22583_p2 = (!p_76_reg_4291.read().is_01() || !ap_const_lv4_1.is_01())? sc_lv<4>(): (sc_biguint<4>(p_76_reg_4291.read()) + sc_biguint<4>(ap_const_lv4_1));
}

void mnist_fp16_opt::thread_j_V_1_fu_22175_p2() {
    j_V_1_fu_22175_p2 = (!p_85_reg_4210.read().is_01() || !ap_const_lv4_1.is_01())? sc_lv<4>(): (sc_biguint<4>(p_85_reg_4210.read()) + sc_biguint<4>(ap_const_lv4_1));
}

void mnist_fp16_opt::thread_j_V_fu_4597_p2() {
    j_V_fu_4597_p2 = (!ap_phi_mux_p_1_phi_fu_2171_p4.read().is_01() || !ap_const_lv5_1.is_01())? sc_lv<5>(): (sc_biguint<5>(ap_phi_mux_p_1_phi_fu_2171_p4.read()) + sc_biguint<5>(ap_const_lv5_1));
}

void mnist_fp16_opt::thread_lhs_V_10_mid2_cast_fu_17029_p1() {
    lhs_V_10_mid2_cast_fu_17029_p1 = esl_zext<8,4>(lhs_V_10_mid2_v_fu_17021_p3.read());
}

void mnist_fp16_opt::thread_lhs_V_10_mid2_v_fu_17021_p3() {
    lhs_V_10_mid2_v_fu_17021_p3 = (!exitcond21_fu_17007_p2.read()[0].is_01())? sc_lv<4>(): ((exitcond21_fu_17007_p2.read()[0].to_bool())? not_zero3_V_fu_17001_p2.read(): ap_phi_mux_p_42_phi_fu_3646_p4.read());
}

void mnist_fp16_opt::thread_lhs_V_11_mid2_cast_fu_16490_p1() {
    lhs_V_11_mid2_cast_fu_16490_p1 = esl_zext<9,3>(lhs_V_11_mid2_fu_16483_p3.read());
}

void mnist_fp16_opt::thread_lhs_V_11_mid2_fu_16483_p3() {
    lhs_V_11_mid2_fu_16483_p3 = (!exitcond66_mid_fu_16459_p2.read()[0].is_01())? sc_lv<3>(): ((exitcond66_mid_fu_16459_p2.read()[0].to_bool())? h1_V_fu_16465_p2.read(): p_59_mid_reg_26328.read());
}

void mnist_fp16_opt::thread_lhs_V_14_mid2_cast_fu_19202_p1() {
    lhs_V_14_mid2_cast_fu_19202_p1 = esl_zext<9,5>(lhs_V_14_mid2_v_reg_27085.read());
}

void mnist_fp16_opt::thread_lhs_V_14_mid2_v_fu_19030_p3() {
    lhs_V_14_mid2_v_fu_19030_p3 = (!exitcond_flatten42_fu_19016_p2.read()[0].is_01())? sc_lv<5>(): ((exitcond_flatten42_fu_19016_p2.read()[0].to_bool())? not_zero4_V_fu_19010_p2.read(): ap_phi_mux_p_52_phi_fu_3909_p4.read());
}

void mnist_fp16_opt::thread_lhs_V_15_mid2_cast_fu_19222_p1() {
    lhs_V_15_mid2_cast_fu_19222_p1 = esl_zext<9,4>(lhs_V_15_mid2_reg_27099.read());
}

void mnist_fp16_opt::thread_lhs_V_15_mid2_fu_19108_p3() {
    lhs_V_15_mid2_fu_19108_p3 = (!exitcond63_mid_fu_19082_p2.read()[0].is_01())? sc_lv<4>(): ((exitcond63_mid_fu_19082_p2.read()[0].to_bool())? index_tuple5_V_fu_19088_p2.read(): p_57_mid_fu_19022_p3.read());
}

void mnist_fp16_opt::thread_lhs_V_18_cast_fu_14170_p1() {
    lhs_V_18_cast_fu_14170_p1 = esl_sext<6,5>(lhs_V_9_fu_14165_p2.read());
}

void mnist_fp16_opt::thread_lhs_V_1_cast_fu_11086_p1() {
    lhs_V_1_cast_fu_11086_p1 = esl_zext<8,5>(p_20_mid2_fu_11058_p3.read());
}

void mnist_fp16_opt::thread_lhs_V_1_mid2_cast_fu_7939_p1() {
    lhs_V_1_mid2_cast_fu_7939_p1 = esl_zext<10,5>(lhs_V_1_mid2_reg_24103.read());
}

void mnist_fp16_opt::thread_lhs_V_1_mid2_fu_7858_p3() {
    lhs_V_1_mid2_fu_7858_p3 = (!exitcond13_mid_fu_7832_p2.read()[0].is_01())? sc_lv<5>(): ((exitcond13_mid_fu_7832_p2.read()[0].to_bool())? index_tuple1_V_fu_7838_p2.read(): p_3_mid_fu_7804_p3.read());
}

void mnist_fp16_opt::thread_lhs_V_22_cast_mid2_c_fu_18107_p1() {
    lhs_V_22_cast_mid2_c_fu_18107_p1 = esl_zext<4,3>(lhs_V_22_cast_mid2_fu_18099_p3.read());
}

void mnist_fp16_opt::thread_lhs_V_22_cast_mid2_fu_18099_p3() {
    lhs_V_22_cast_mid2_fu_18099_p3 = (!exitcond54_mid_fu_18073_p2.read()[0].is_01())? sc_lv<3>(): ((exitcond54_mid_fu_18073_p2.read()[0].to_bool())? yy4_V_fu_18079_p2.read(): p_49_mid_fu_18019_p3.read());
}

void mnist_fp16_opt::thread_lhs_V_24_cast_fu_18143_p1() {
    lhs_V_24_cast_fu_18143_p1 = esl_zext<4,3>(p_53_mid2_fu_18091_p3.read());
}

void mnist_fp16_opt::thread_lhs_V_26_cast_mid2_c_fu_19838_p1() {
    lhs_V_26_cast_mid2_c_fu_19838_p1 = esl_zext<4,3>(lhs_V_26_cast_mid2_fu_19830_p3.read());
}

void mnist_fp16_opt::thread_lhs_V_26_cast_mid2_fu_19830_p3() {
    lhs_V_26_cast_mid2_fu_19830_p3 = (!exitcond65_mid_fu_19804_p2.read()[0].is_01())? sc_lv<3>(): ((exitcond65_mid_fu_19804_p2.read()[0].to_bool())? yy5_V_fu_19810_p2.read(): p_61_mid_fu_19738_p3.read());
}

void mnist_fp16_opt::thread_lhs_V_27_cast_fu_19874_p1() {
    lhs_V_27_cast_fu_19874_p1 = esl_zext<4,3>(p_64_mid2_fu_19822_p3.read());
}

void mnist_fp16_opt::thread_lhs_V_29_cast_fu_19281_p1() {
    lhs_V_29_cast_fu_19281_p1 = esl_sext<5,4>(lhs_V_s_fu_19276_p2.read());
}

void mnist_fp16_opt::thread_lhs_V_2_mid2_v_fu_7812_p3() {
    lhs_V_2_mid2_v_fu_7812_p3 = (!exitcond_flatten6_fu_7798_p2.read()[0].is_01())? sc_lv<3>(): ((exitcond_flatten6_fu_7798_p2.read()[0].to_bool())? not_zero_V_fu_7792_p2.read(): ap_phi_mux_p_6_phi_fu_2638_p4.read());
}

void mnist_fp16_opt::thread_lhs_V_3_cast_fu_10583_p1() {
    lhs_V_3_cast_fu_10583_p1 = esl_zext<11,4>(p_33_mid2_fu_10512_p3.read());
}

void mnist_fp16_opt::thread_lhs_V_5_cast_fu_16538_p1() {
    lhs_V_5_cast_fu_16538_p1 = esl_zext<10,3>(p_65_mid2_fu_16475_p3.read());
}

void mnist_fp16_opt::thread_lhs_V_5_mid2_v_fu_11066_p3() {
    lhs_V_5_mid2_v_fu_11066_p3 = (!exitcond11_fu_11052_p2.read()[0].is_01())? sc_lv<3>(): ((exitcond11_fu_11052_p2.read()[0].to_bool())? not_zero1_V_fu_11046_p2.read(): ap_phi_mux_p_15_phi_fu_2994_p4.read());
}

void mnist_fp16_opt::thread_lhs_V_6_cast_fu_8136_p1() {
    lhs_V_6_cast_fu_8136_p1 = esl_sext<7,6>(lhs_V_6_fu_8130_p2.read());
}

void mnist_fp16_opt::thread_lhs_V_6_fu_8130_p2() {
    lhs_V_6_fu_8130_p2 = (!tmp_67_cast_fu_8123_p1.read().is_01() || !tmp_68_cast_fu_8126_p1.read().is_01())? sc_lv<6>(): (sc_biguint<6>(tmp_67_cast_fu_8123_p1.read()) - sc_biguint<6>(tmp_68_cast_fu_8126_p1.read()));
}

void mnist_fp16_opt::thread_lhs_V_7_cast_fu_17051_p1() {
    lhs_V_7_cast_fu_17051_p1 = esl_zext<8,4>(p_46_mid2_fu_17013_p3.read());
}

void mnist_fp16_opt::thread_lhs_V_7_mid2_v_fu_13906_p3() {
    lhs_V_7_mid2_v_fu_13906_p3 = (!exitcond_flatten24_fu_13892_p2.read()[0].is_01())? sc_lv<4>(): ((exitcond_flatten24_fu_13892_p2.read()[0].to_bool())? not_zero2_V_fu_13886_p2.read(): ap_phi_mux_p_26_phi_fu_3294_p4.read());
}

void mnist_fp16_opt::thread_lhs_V_8_mid2_cast_fu_14088_p1() {
    lhs_V_8_mid2_cast_fu_14088_p1 = esl_zext<9,5>(lhs_V_8_mid2_reg_25711.read());
}

void mnist_fp16_opt::thread_lhs_V_8_mid2_fu_13988_p3() {
    lhs_V_8_mid2_fu_13988_p3 = (!exitcond36_mid_fu_13950_p2.read()[0].is_01())? sc_lv<5>(): ((exitcond36_mid_fu_13950_p2.read()[0].to_bool())? index_tuple3_V_fu_13956_p2.read(): p_31_mid_fu_13898_p3.read());
}

void mnist_fp16_opt::thread_lhs_V_9_fu_14165_p2() {
    lhs_V_9_fu_14165_p2 = (!p_35_mid2_reg_25702.read().is_01() || !tmp_246_cast_fu_14161_p1.read().is_01())? sc_lv<5>(): (sc_biguint<5>(p_35_mid2_reg_25702.read()) - sc_biguint<5>(tmp_246_cast_fu_14161_p1.read()));
}

void mnist_fp16_opt::thread_lhs_V_mid2_cast_fu_10527_p1() {
    lhs_V_mid2_cast_fu_10527_p1 = esl_zext<9,4>(lhs_V_mid2_fu_10520_p3.read());
}

void mnist_fp16_opt::thread_lhs_V_mid2_fu_10520_p3() {
    lhs_V_mid2_fu_10520_p3 = (!exitcond34_mid_fu_10497_p2.read()[0].is_01())? sc_lv<4>(): ((exitcond34_mid_fu_10497_p2.read()[0].to_bool())? h_V_fu_10502_p2.read(): p_27_mid_reg_24728.read());
}

void mnist_fp16_opt::thread_lhs_V_s_fu_19276_p2() {
    lhs_V_s_fu_19276_p2 = (!p_62_mid2_reg_27092.read().is_01() || !tmp_423_cast_fu_19272_p1.read().is_01())? sc_lv<4>(): (sc_biguint<4>(p_62_mid2_reg_27092.read()) - sc_biguint<4>(tmp_423_cast_fu_19272_p1.read()));
}

void mnist_fp16_opt::thread_man_V_11_fu_15112_p2() {
    man_V_11_fu_15112_p2 = (!ap_const_lv54_0.is_01() || !p_Result_74_fu_15108_p1.read().is_01())? sc_lv<54>(): (sc_biguint<54>(ap_const_lv54_0) - sc_biguint<54>(p_Result_74_fu_15108_p1.read()));
}

void mnist_fp16_opt::thread_man_V_12_fu_15118_p3() {
    man_V_12_fu_15118_p3 = (!isneg_2_reg_25986.read()[0].is_01())? sc_lv<54>(): ((isneg_2_reg_25986.read()[0].to_bool())? man_V_11_fu_15112_p2.read(): p_Result_74_fu_15108_p1.read());
}

void mnist_fp16_opt::thread_man_V_13_fu_16116_p2() {
    man_V_13_fu_16116_p2 = (!ap_const_lv54_0.is_01() || !p_Result_25_fu_16112_p1.read().is_01())? sc_lv<54>(): (sc_biguint<54>(ap_const_lv54_0) - sc_biguint<54>(p_Result_25_fu_16112_p1.read()));
}

void mnist_fp16_opt::thread_man_V_14_fu_15197_p2() {
    man_V_14_fu_15197_p2 = (!ap_const_lv54_0.is_01() || !p_Result_75_fu_15193_p1.read().is_01())? sc_lv<54>(): (sc_biguint<54>(ap_const_lv54_0) - sc_biguint<54>(p_Result_75_fu_15193_p1.read()));
}

void mnist_fp16_opt::thread_man_V_15_fu_15203_p3() {
    man_V_15_fu_15203_p3 = (!isneg_3_reg_26008.read()[0].is_01())? sc_lv<54>(): ((isneg_3_reg_26008.read()[0].to_bool())? man_V_14_fu_15197_p2.read(): p_Result_75_fu_15193_p1.read());
}

void mnist_fp16_opt::thread_man_V_16_fu_16122_p3() {
    man_V_16_fu_16122_p3 = (!tmp_1274_reg_26255.read()[0].is_01())? sc_lv<54>(): ((tmp_1274_reg_26255.read()[0].to_bool())? man_V_13_fu_16116_p2.read(): p_Result_25_fu_16112_p1.read());
}

void mnist_fp16_opt::thread_man_V_18_fu_18729_p2() {
    man_V_18_fu_18729_p2 = (!ap_const_lv54_0.is_01() || !p_Result_38_fu_18725_p1.read().is_01())? sc_lv<54>(): (sc_biguint<54>(ap_const_lv54_0) - sc_biguint<54>(p_Result_38_fu_18725_p1.read()));
}

void mnist_fp16_opt::thread_man_V_19_fu_20244_p2() {
    man_V_19_fu_20244_p2 = (!ap_const_lv54_0.is_01() || !p_Result_82_fu_20240_p1.read().is_01())? sc_lv<54>(): (sc_biguint<54>(ap_const_lv54_0) - sc_biguint<54>(p_Result_82_fu_20240_p1.read()));
}

void mnist_fp16_opt::thread_man_V_1_fu_7509_p2() {
    man_V_1_fu_7509_p2 = (!ap_const_lv54_0.is_01() || !p_Result_4_fu_7505_p1.read().is_01())? sc_lv<54>(): (sc_biguint<54>(ap_const_lv54_0) - sc_biguint<54>(p_Result_4_fu_7505_p1.read()));
}

void mnist_fp16_opt::thread_man_V_20_fu_20250_p3() {
    man_V_20_fu_20250_p3 = (!isneg_4_reg_27381.read()[0].is_01())? sc_lv<54>(): ((isneg_4_reg_27381.read()[0].to_bool())? man_V_19_fu_20244_p2.read(): p_Result_82_fu_20240_p1.read());
}

void mnist_fp16_opt::thread_man_V_21_fu_18735_p3() {
    man_V_21_fu_18735_p3 = (!tmp_1390_reg_27020.read()[0].is_01())? sc_lv<54>(): ((tmp_1390_reg_27020.read()[0].to_bool())? man_V_18_fu_18729_p2.read(): p_Result_38_fu_18725_p1.read());
}

void mnist_fp16_opt::thread_man_V_23_fu_21942_p2() {
    man_V_23_fu_21942_p2 = (!ap_const_lv54_0.is_01() || !p_Result_54_fu_21938_p1.read().is_01())? sc_lv<54>(): (sc_biguint<54>(ap_const_lv54_0) - sc_biguint<54>(p_Result_54_fu_21938_p1.read()));
}

void mnist_fp16_opt::thread_man_V_24_fu_20364_p2() {
    man_V_24_fu_20364_p2 = (!ap_const_lv54_0.is_01() || !p_Result_83_fu_20360_p1.read().is_01())? sc_lv<54>(): (sc_biguint<54>(ap_const_lv54_0) - sc_biguint<54>(p_Result_83_fu_20360_p1.read()));
}

void mnist_fp16_opt::thread_man_V_25_fu_20370_p3() {
    man_V_25_fu_20370_p3 = (!isneg_5_reg_27403.read()[0].is_01())? sc_lv<54>(): ((isneg_5_reg_27403.read()[0].to_bool())? man_V_24_fu_20364_p2.read(): p_Result_83_fu_20360_p1.read());
}

void mnist_fp16_opt::thread_man_V_26_fu_21948_p3() {
    man_V_26_fu_21948_p3 = (!tmp_1483_reg_27848.read()[0].is_01())? sc_lv<54>(): ((tmp_1483_reg_27848.read()[0].to_bool())? man_V_23_fu_21942_p2.read(): p_Result_54_fu_21938_p1.read());
}

void mnist_fp16_opt::thread_man_V_27_fu_21232_p2() {
    man_V_27_fu_21232_p2 = (!ap_const_lv54_0.is_01() || !p_Result_45_fu_21228_p1.read().is_01())? sc_lv<54>(): (sc_biguint<54>(ap_const_lv54_0) - sc_biguint<54>(p_Result_45_fu_21228_p1.read()));
}

void mnist_fp16_opt::thread_man_V_28_fu_21238_p3() {
    man_V_28_fu_21238_p3 = (!tmp_1448_reg_27650.read()[0].is_01())? sc_lv<54>(): ((tmp_1448_reg_27650.read()[0].to_bool())? man_V_27_fu_21232_p2.read(): p_Result_45_fu_21228_p1.read());
}

void mnist_fp16_opt::thread_man_V_2_fu_7515_p3() {
    man_V_2_fu_7515_p3 = (!tmp_211_reg_23987.read()[0].is_01())? sc_lv<54>(): ((tmp_211_reg_23987.read()[0].to_bool())? man_V_1_fu_7509_p2.read(): p_Result_4_fu_7505_p1.read());
}

void mnist_fp16_opt::thread_man_V_3_fu_9142_p2() {
    man_V_3_fu_9142_p2 = (!ap_const_lv54_0.is_01() || !p_Result_66_fu_9138_p1.read().is_01())? sc_lv<54>(): (sc_biguint<54>(ap_const_lv54_0) - sc_biguint<54>(p_Result_66_fu_9138_p1.read()));
}

void mnist_fp16_opt::thread_man_V_4_fu_9148_p3() {
    man_V_4_fu_9148_p3 = (!isneg_reg_24386.read()[0].is_01())? sc_lv<54>(): ((isneg_reg_24386.read()[0].to_bool())? man_V_3_fu_9142_p2.read(): p_Result_66_fu_9138_p1.read());
}

void mnist_fp16_opt::thread_man_V_5_fu_10146_p2() {
    man_V_5_fu_10146_p2 = (!ap_const_lv54_0.is_01() || !p_Result_7_fu_10142_p1.read().is_01())? sc_lv<54>(): (sc_biguint<54>(ap_const_lv54_0) - sc_biguint<54>(p_Result_7_fu_10142_p1.read()));
}

void mnist_fp16_opt::thread_man_V_6_fu_10152_p3() {
    man_V_6_fu_10152_p3 = (!tmp_594_reg_24655.read()[0].is_01())? sc_lv<54>(): ((tmp_594_reg_24655.read()[0].to_bool())? man_V_5_fu_10146_p2.read(): p_Result_7_fu_10142_p1.read());
}

void mnist_fp16_opt::thread_man_V_7_fu_13589_p3() {
    man_V_7_fu_13589_p3 = (!tmp_885_reg_25611.read()[0].is_01())? sc_lv<54>(): ((tmp_885_reg_25611.read()[0].to_bool())? man_V_s_fu_13583_p2.read(): p_Result_19_fu_13579_p1.read());
}

void mnist_fp16_opt::thread_man_V_8_fu_9227_p2() {
    man_V_8_fu_9227_p2 = (!ap_const_lv54_0.is_01() || !p_Result_67_fu_9223_p1.read().is_01())? sc_lv<54>(): (sc_biguint<54>(ap_const_lv54_0) - sc_biguint<54>(p_Result_67_fu_9223_p1.read()));
}

void mnist_fp16_opt::thread_man_V_9_fu_9233_p3() {
    man_V_9_fu_9233_p3 = (!isneg_1_reg_24408.read()[0].is_01())? sc_lv<54>(): ((isneg_1_reg_24408.read()[0].to_bool())? man_V_8_fu_9227_p2.read(): p_Result_67_fu_9223_p1.read());
}

void mnist_fp16_opt::thread_man_V_s_fu_13583_p2() {
    man_V_s_fu_13583_p2 = (!ap_const_lv54_0.is_01() || !p_Result_19_fu_13579_p1.read().is_01())? sc_lv<54>(): (sc_biguint<54>(ap_const_lv54_0) - sc_biguint<54>(p_Result_19_fu_13579_p1.read()));
}

void mnist_fp16_opt::thread_max_pool_0_0_0_address0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp24_stage0.read()) && 
         esl_seteq<1,1,1>(ap_enable_reg_pp24_iter0.read(), ap_const_logic_1) && 
         esl_seteq<1,1,1>(ap_block_pp24_stage0.read(), ap_const_boolean_0))) {
        max_pool_0_0_0_address0 =  (sc_lv<4>) (tmp_433_fu_21893_p1.read());
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state388.read())) {
        max_pool_0_0_0_address0 =  (sc_lv<4>) (tmp_417_reg_27715.read());
    } else {
        max_pool_0_0_0_address0 =  (sc_lv<4>) ("XXXX");
    }
}

void mnist_fp16_opt::thread_max_pool_0_0_0_ce0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp24_stage0.read()) && 
          esl_seteq<1,1,1>(ap_block_pp24_stage0_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_enable_reg_pp24_iter0.read(), ap_const_logic_1)) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state388.read()))) {
        max_pool_0_0_0_ce0 = ap_const_logic_1;
    } else {
        max_pool_0_0_0_ce0 = ap_const_logic_0;
    }
}

void mnist_fp16_opt::thread_max_pool_0_0_0_we0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state388.read())) {
        max_pool_0_0_0_we0 = ap_const_logic_1;
    } else {
        max_pool_0_0_0_we0 = ap_const_logic_0;
    }
}

void mnist_fp16_opt::thread_msb_idx_10_cast_fu_16750_p1() {
    msb_idx_10_cast_fu_16750_p1 = esl_zext<32,31>(msb_idx_10_fu_16744_p3.read());
}

void mnist_fp16_opt::thread_msb_idx_10_fu_16744_p3() {
    msb_idx_10_fu_16744_p3 = (!tmp_1355_reg_26450.read()[0].is_01())? sc_lv<31>(): ((tmp_1355_reg_26450.read()[0].to_bool())? ap_const_lv31_0: tmp_1354_reg_26445.read());
}

void mnist_fp16_opt::thread_msb_idx_11_fu_19561_p2() {
    msb_idx_11_fu_19561_p2 = (!ap_const_lv32_F.is_01() || !num_zeros_6_fu_19553_p3.read().is_01())? sc_lv<32>(): (sc_biguint<32>(ap_const_lv32_F) - sc_biguint<32>(num_zeros_6_fu_19553_p3.read()));
}

void mnist_fp16_opt::thread_msb_idx_12_cast_fu_19585_p1() {
    msb_idx_12_cast_fu_19585_p1 = esl_zext<32,31>(msb_idx_12_fu_19579_p3.read());
}

void mnist_fp16_opt::thread_msb_idx_12_fu_19579_p3() {
    msb_idx_12_fu_19579_p3 = (!tmp_1438_reg_27229.read()[0].is_01())? sc_lv<31>(): ((tmp_1438_reg_27229.read()[0].to_bool())? ap_const_lv31_0: tmp_1437_reg_27224.read());
}

void mnist_fp16_opt::thread_msb_idx_13_fu_20794_p2() {
    msb_idx_13_fu_20794_p2 = (!ap_const_lv32_F.is_01() || !num_zeros_7_fu_20786_p3.read().is_01())? sc_lv<32>(): (sc_biguint<32>(ap_const_lv32_F) - sc_biguint<32>(num_zeros_7_fu_20786_p3.read()));
}

void mnist_fp16_opt::thread_msb_idx_14_cast_fu_20824_p1() {
    msb_idx_14_cast_fu_20824_p1 = esl_zext<32,31>(msb_idx_14_fu_20818_p3.read());
}

void mnist_fp16_opt::thread_msb_idx_14_fu_20818_p3() {
    msb_idx_14_fu_20818_p3 = (!tmp_1456_reg_27549.read()[0].is_01())? sc_lv<31>(): ((tmp_1456_reg_27549.read()[0].to_bool())? ap_const_lv31_0: tmp_1455_reg_27544.read());
}

void mnist_fp16_opt::thread_msb_idx_15_fu_21636_p2() {
    msb_idx_15_fu_21636_p2 = (!ap_const_lv32_F.is_01() || !num_zeros_8_fu_21628_p3.read().is_01())? sc_lv<32>(): (sc_biguint<32>(ap_const_lv32_F) - sc_biguint<32>(num_zeros_8_fu_21628_p3.read()));
}

void mnist_fp16_opt::thread_msb_idx_16_cast_fu_21660_p1() {
    msb_idx_16_cast_fu_21660_p1 = esl_zext<32,31>(msb_idx_16_fu_21654_p3.read());
}

void mnist_fp16_opt::thread_msb_idx_16_fu_21654_p3() {
    msb_idx_16_fu_21654_p3 = (!tmp_1492_reg_27792.read()[0].is_01())? sc_lv<31>(): ((tmp_1492_reg_27792.read()[0].to_bool())? ap_const_lv31_0: tmp_1491_reg_27787.read());
}

void mnist_fp16_opt::thread_msb_idx_17_fu_22291_p2() {
    msb_idx_17_fu_22291_p2 = (!ap_const_lv32_F.is_01() || !num_zeros_9_fu_22283_p3.read().is_01())? sc_lv<32>(): (sc_biguint<32>(ap_const_lv32_F) - sc_biguint<32>(num_zeros_9_fu_22283_p3.read()));
}

void mnist_fp16_opt::thread_msb_idx_18_cast_fu_22315_p1() {
    msb_idx_18_cast_fu_22315_p1 = esl_zext<32,31>(msb_idx_18_fu_22309_p3.read());
}

void mnist_fp16_opt::thread_msb_idx_18_fu_22309_p3() {
    msb_idx_18_fu_22309_p3 = (!tmp_1505_reg_27978.read()[0].is_01())? sc_lv<31>(): ((tmp_1505_reg_27978.read()[0].to_bool())? ap_const_lv31_0: tmp_1504_reg_27973.read());
}

void mnist_fp16_opt::thread_msb_idx_1_cast_fu_8456_p1() {
    msb_idx_1_cast_fu_8456_p1 = esl_zext<32,31>(msb_idx_1_fu_8450_p3.read());
}

void mnist_fp16_opt::thread_msb_idx_1_fu_8450_p3() {
    msb_idx_1_fu_8450_p3 = (!tmp_512_reg_24241.read()[0].is_01())? sc_lv<31>(): ((tmp_512_reg_24241.read()[0].to_bool())? ap_const_lv31_0: tmp_511_reg_24236.read());
}

void mnist_fp16_opt::thread_msb_idx_2_fu_9692_p2() {
    msb_idx_2_fu_9692_p2 = (!ap_const_lv32_F.is_01() || !num_zeros_1_fu_9684_p3.read().is_01())? sc_lv<32>(): (sc_biguint<32>(ap_const_lv32_F) - sc_biguint<32>(num_zeros_1_fu_9684_p3.read()));
}

void mnist_fp16_opt::thread_msb_idx_3_cast_fu_9722_p1() {
    msb_idx_3_cast_fu_9722_p1 = esl_zext<32,31>(msb_idx_3_fu_9716_p3.read());
}

void mnist_fp16_opt::thread_msb_idx_3_fu_9716_p3() {
    msb_idx_3_fu_9716_p3 = (!tmp_625_reg_24554.read()[0].is_01())? sc_lv<31>(): ((tmp_625_reg_24554.read()[0].to_bool())? ap_const_lv31_0: tmp_622_reg_24549.read());
}

void mnist_fp16_opt::thread_msb_idx_4_fu_10771_p2() {
    msb_idx_4_fu_10771_p2 = (!ap_const_lv32_F.is_01() || !num_zeros_2_fu_10763_p3.read().is_01())? sc_lv<32>(): (sc_biguint<32>(ap_const_lv32_F) - sc_biguint<32>(num_zeros_2_fu_10763_p3.read()));
}

void mnist_fp16_opt::thread_msb_idx_5_cast_fu_10795_p1() {
    msb_idx_5_cast_fu_10795_p1 = esl_zext<32,31>(msb_idx_5_fu_10789_p3.read());
}

void mnist_fp16_opt::thread_msb_idx_5_fu_10789_p3() {
    msb_idx_5_fu_10789_p3 = (!tmp_770_reg_24855.read()[0].is_01())? sc_lv<31>(): ((tmp_770_reg_24855.read()[0].to_bool())? ap_const_lv31_0: tmp_769_reg_24850.read());
}

void mnist_fp16_opt::thread_msb_idx_6_fu_14466_p2() {
    msb_idx_6_fu_14466_p2 = (!ap_const_lv32_F.is_01() || !num_zeros_3_fu_14458_p3.read().is_01())? sc_lv<32>(): (sc_biguint<32>(ap_const_lv32_F) - sc_biguint<32>(num_zeros_3_fu_14458_p3.read()));
}

void mnist_fp16_opt::thread_msb_idx_7_cast_fu_14490_p1() {
    msb_idx_7_cast_fu_14490_p1 = esl_zext<32,31>(msb_idx_7_fu_14484_p3.read());
}

void mnist_fp16_opt::thread_msb_idx_7_fu_14484_p3() {
    msb_idx_7_fu_14484_p3 = (!tmp_1165_reg_25851.read()[0].is_01())? sc_lv<31>(): ((tmp_1165_reg_25851.read()[0].to_bool())? ap_const_lv31_0: tmp_1162_reg_25846.read());
}

void mnist_fp16_opt::thread_msb_idx_8_fu_15662_p2() {
    msb_idx_8_fu_15662_p2 = (!ap_const_lv32_F.is_01() || !num_zeros_4_fu_15654_p3.read().is_01())? sc_lv<32>(): (sc_biguint<32>(ap_const_lv32_F) - sc_biguint<32>(num_zeros_4_fu_15654_p3.read()));
}

void mnist_fp16_opt::thread_msb_idx_9_cast_fu_15692_p1() {
    msb_idx_9_cast_fu_15692_p1 = esl_zext<32,31>(msb_idx_9_fu_15686_p3.read());
}

void mnist_fp16_opt::thread_msb_idx_9_fu_15686_p3() {
    msb_idx_9_fu_15686_p3 = (!tmp_1282_reg_26154.read()[0].is_01())? sc_lv<31>(): ((tmp_1282_reg_26154.read()[0].to_bool())? ap_const_lv31_0: tmp_1281_reg_26149.read());
}

void mnist_fp16_opt::thread_msb_idx_fu_8432_p2() {
    msb_idx_fu_8432_p2 = (!ap_const_lv32_F.is_01() || !num_zeros_fu_8424_p3.read().is_01())? sc_lv<32>(): (sc_biguint<32>(ap_const_lv32_F) - sc_biguint<32>(num_zeros_fu_8424_p3.read()));
}

void mnist_fp16_opt::thread_msb_idx_s_fu_16726_p2() {
    msb_idx_s_fu_16726_p2 = (!ap_const_lv32_F.is_01() || !num_zeros_5_fu_16718_p3.read().is_01())? sc_lv<32>(): (sc_biguint<32>(ap_const_lv32_F) - sc_biguint<32>(num_zeros_5_fu_16718_p3.read()));
}

void mnist_fp16_opt::thread_mul10_fu_22909_p0() {
    mul10_fu_22909_p0 =  (sc_lv<14>) (ap_const_lv26_14E6);
}

void mnist_fp16_opt::thread_mul10_fu_22909_p1() {
    mul10_fu_22909_p1 =  (sc_lv<12>) (sext9_cast_reg_24965_pp10_iter1_reg.read());
}

void mnist_fp16_opt::thread_mul11_fu_22748_p0() {
    mul11_fu_22748_p0 =  (sc_lv<14>) (ap_const_lv26_124A);
}

void mnist_fp16_opt::thread_mul12_fu_22861_p0() {
    mul12_fu_22861_p0 =  (sc_lv<14>) (ap_const_lv26_124A);
}

void mnist_fp16_opt::thread_mul13_fu_22916_p0() {
    mul13_fu_22916_p0 =  (sc_lv<14>) (ap_const_lv26_14E6);
}

void mnist_fp16_opt::thread_mul13_fu_22916_p1() {
    mul13_fu_22916_p1 =  (sc_lv<12>) (sext12_cast_reg_24988_pp10_iter1_reg.read());
}

void mnist_fp16_opt::thread_mul14_fu_22756_p0() {
    mul14_fu_22756_p0 =  (sc_lv<14>) (ap_const_lv26_124A);
}

void mnist_fp16_opt::thread_mul15_fu_22869_p0() {
    mul15_fu_22869_p0 =  (sc_lv<14>) (ap_const_lv26_124A);
}

void mnist_fp16_opt::thread_mul16_fu_22923_p0() {
    mul16_fu_22923_p0 =  (sc_lv<14>) (ap_const_lv26_14E6);
}

void mnist_fp16_opt::thread_mul16_fu_22923_p1() {
    mul16_fu_22923_p1 =  (sc_lv<12>) (sext15_cast_reg_25021_pp10_iter1_reg.read());
}

void mnist_fp16_opt::thread_mul17_fu_22764_p0() {
    mul17_fu_22764_p0 =  (sc_lv<14>) (ap_const_lv26_124A);
}

void mnist_fp16_opt::thread_mul18_fu_22958_p0() {
    mul18_fu_22958_p0 =  (sc_lv<14>) (ap_const_lv26_124A);
}

void mnist_fp16_opt::thread_mul18_fu_22958_p1() {
    mul18_fu_22958_p1 =  (sc_lv<12>) (sext18_cast_fu_14197_p1.read());
}

void mnist_fp16_opt::thread_mul19_fu_22966_p0() {
    mul19_fu_22966_p0 =  (sc_lv<14>) (ap_const_lv26_14E6);
}

void mnist_fp16_opt::thread_mul19_fu_22966_p1() {
    mul19_fu_22966_p1 =  (sc_lv<12>) (sext18_cast_fu_14197_p1.read());
}

void mnist_fp16_opt::thread_mul1_fu_22812_p0() {
    mul1_fu_22812_p0 =  (sc_lv<14>) (ap_const_lv26_124A);
}

void mnist_fp16_opt::thread_mul20_fu_22877_p0() {
    mul20_fu_22877_p0 =  (sc_lv<14>) (ap_const_lv26_124A);
}

void mnist_fp16_opt::thread_mul21_fu_22930_p0() {
    mul21_fu_22930_p0 =  (sc_lv<14>) (ap_const_lv26_14E6);
}

void mnist_fp16_opt::thread_mul21_fu_22930_p1() {
    mul21_fu_22930_p1 =  (sc_lv<12>) (sext20_cast_reg_25044_pp10_iter1_reg.read());
}

void mnist_fp16_opt::thread_mul22_fu_22772_p0() {
    mul22_fu_22772_p0 =  (sc_lv<14>) (ap_const_lv26_124A);
}

void mnist_fp16_opt::thread_mul23_fu_22885_p0() {
    mul23_fu_22885_p0 =  (sc_lv<14>) (ap_const_lv26_124A);
}

void mnist_fp16_opt::thread_mul24_fu_22937_p0() {
    mul24_fu_22937_p0 =  (sc_lv<14>) (ap_const_lv26_14E6);
}

void mnist_fp16_opt::thread_mul24_fu_22937_p1() {
    mul24_fu_22937_p1 =  (sc_lv<12>) (sext23_cast_reg_25077_pp10_iter1_reg.read());
}

void mnist_fp16_opt::thread_mul25_fu_22780_p0() {
    mul25_fu_22780_p0 =  (sc_lv<14>) (ap_const_lv26_124A);
}

void mnist_fp16_opt::thread_mul26_fu_22893_p0() {
    mul26_fu_22893_p0 =  (sc_lv<14>) (ap_const_lv26_124A);
}

void mnist_fp16_opt::thread_mul27_fu_22944_p0() {
    mul27_fu_22944_p0 =  (sc_lv<14>) (ap_const_lv26_14E6);
}

void mnist_fp16_opt::thread_mul27_fu_22944_p1() {
    mul27_fu_22944_p1 =  (sc_lv<12>) (sext26_cast_reg_25100_pp10_iter1_reg.read());
}

void mnist_fp16_opt::thread_mul28_fu_22788_p0() {
    mul28_fu_22788_p0 =  (sc_lv<14>) (ap_const_lv26_124A);
}

void mnist_fp16_opt::thread_mul29_fu_22901_p0() {
    mul29_fu_22901_p0 =  (sc_lv<14>) (ap_const_lv26_124A);
}

void mnist_fp16_opt::thread_mul2_fu_22708_p0() {
    mul2_fu_22708_p0 =  (sc_lv<14>) (ap_const_lv26_124A);
}

void mnist_fp16_opt::thread_mul30_fu_22951_p0() {
    mul30_fu_22951_p0 =  (sc_lv<14>) (ap_const_lv26_14E6);
}

void mnist_fp16_opt::thread_mul30_fu_22951_p1() {
    mul30_fu_22951_p1 =  (sc_lv<12>) (sext29_cast_reg_25146_pp10_iter1_reg.read());
}

void mnist_fp16_opt::thread_mul31_fu_22796_p0() {
    mul31_fu_22796_p0 =  (sc_lv<14>) (ap_const_lv26_124A);
}

void mnist_fp16_opt::thread_mul32_fu_22983_p0() {
    mul32_fu_22983_p0 =  (sc_lv<12>) (ap_const_lv22_493);
}

void mnist_fp16_opt::thread_mul33_fu_23015_p0() {
    mul33_fu_23015_p0 =  (sc_lv<12>) (ap_const_lv22_53A);
}

void mnist_fp16_opt::thread_mul33_fu_23015_p1() {
    mul33_fu_23015_p1 =  (sc_lv<10>) (sext32_cast_reg_26568_pp17_iter2_reg.read());
}

void mnist_fp16_opt::thread_mul34_fu_22991_p0() {
    mul34_fu_22991_p0 =  (sc_lv<12>) (ap_const_lv22_493);
}

void mnist_fp16_opt::thread_mul35_fu_23029_p0() {
    mul35_fu_23029_p0 =  (sc_lv<12>) (ap_const_lv22_53A);
}

void mnist_fp16_opt::thread_mul35_fu_23029_p1() {
    mul35_fu_23029_p1 =  (sc_lv<10>) (sext34_cast_reg_26583_pp17_iter2_reg.read());
}

void mnist_fp16_opt::thread_mul36_fu_22999_p0() {
    mul36_fu_22999_p0 =  (sc_lv<12>) (ap_const_lv22_493);
}

void mnist_fp16_opt::thread_mul37_fu_23036_p0() {
    mul37_fu_23036_p0 =  (sc_lv<12>) (ap_const_lv22_53A);
}

void mnist_fp16_opt::thread_mul37_fu_23036_p1() {
    mul37_fu_23036_p1 =  (sc_lv<10>) (sext36_cast_reg_26624_pp17_iter2_reg.read());
}

void mnist_fp16_opt::thread_mul38_fu_23043_p0() {
    mul38_fu_23043_p0 =  (sc_lv<13>) (ap_const_lv24_925);
}

void mnist_fp16_opt::thread_mul38_fu_23043_p1() {
    mul38_fu_23043_p1 =  (sc_lv<11>) (sext38_cast_fu_19317_p1.read());
}

void mnist_fp16_opt::thread_mul39_fu_23051_p0() {
    mul39_fu_23051_p0 =  (sc_lv<13>) (ap_const_lv24_A73);
}

void mnist_fp16_opt::thread_mul39_fu_23051_p1() {
    mul39_fu_23051_p1 =  (sc_lv<11>) (sext38_cast_fu_19317_p1.read());
}

void mnist_fp16_opt::thread_mul3_fu_22716_p0() {
    mul3_fu_22716_p0 =  (sc_lv<14>) (ap_const_lv26_124A);
}

void mnist_fp16_opt::thread_mul40_fu_23007_p0() {
    mul40_fu_23007_p0 =  (sc_lv<12>) (ap_const_lv22_493);
}

void mnist_fp16_opt::thread_mul41_fu_23022_p0() {
    mul41_fu_23022_p0 =  (sc_lv<12>) (ap_const_lv22_53A);
}

void mnist_fp16_opt::thread_mul41_fu_23022_p1() {
    mul41_fu_23022_p1 =  (sc_lv<10>) (sext40_cast_reg_26639_pp17_iter2_reg.read());
}

void mnist_fp16_opt::thread_mul4_fu_22820_p0() {
    mul4_fu_22820_p0 =  (sc_lv<15>) (ap_const_lv28_2493);
}

void mnist_fp16_opt::thread_mul4_fu_22820_p1() {
    mul4_fu_22820_p1 =  (sc_lv<13>) (sext4_cast_fu_8163_p1.read());
}

void mnist_fp16_opt::thread_mul5_fu_22828_p0() {
    mul5_fu_22828_p0 =  (sc_lv<15>) (ap_const_lv28_29CC);
}

void mnist_fp16_opt::thread_mul5_fu_22828_p1() {
    mul5_fu_22828_p1 =  (sc_lv<13>) (sext4_cast_fu_8163_p1.read());
}

void mnist_fp16_opt::thread_mul6_fu_22724_p0() {
    mul6_fu_22724_p0 =  (sc_lv<14>) (ap_const_lv26_124A);
}

void mnist_fp16_opt::thread_mul7_fu_22732_p0() {
    mul7_fu_22732_p0 =  (sc_lv<14>) (ap_const_lv26_124A);
}

void mnist_fp16_opt::thread_mul8_fu_22740_p0() {
    mul8_fu_22740_p0 =  (sc_lv<14>) (ap_const_lv26_124A);
}

void mnist_fp16_opt::thread_mul9_fu_22853_p0() {
    mul9_fu_22853_p0 =  (sc_lv<14>) (ap_const_lv26_124A);
}

void mnist_fp16_opt::thread_mul_fu_22804_p0() {
    mul_fu_22804_p0 =  (sc_lv<14>) (ap_const_lv26_124A);
}

void mnist_fp16_opt::thread_neg_mul10_fu_11657_p2() {
    neg_mul10_fu_11657_p2 = (!ap_const_lv25_0.is_01() || !tmp_791_reg_25171.read().is_01())? sc_lv<25>(): (sc_biguint<25>(ap_const_lv25_0) - sc_biguint<25>(tmp_791_reg_25171.read()));
}

void mnist_fp16_opt::thread_neg_mul11_fu_5337_p2() {
    neg_mul11_fu_5337_p2 = (!ap_const_lv25_0.is_01() || !tmp_807_reg_23389.read().is_01())? sc_lv<25>(): (sc_biguint<25>(ap_const_lv25_0) - sc_biguint<25>(tmp_807_reg_23389.read()));
}

void mnist_fp16_opt::thread_neg_mul12_fu_11267_p2() {
    neg_mul12_fu_11267_p2 = (!ap_const_lv25_0.is_01() || !tmp_832_reg_24993.read().is_01())? sc_lv<25>(): (sc_biguint<25>(ap_const_lv25_0) - sc_biguint<25>(tmp_832_reg_24993.read()));
}

void mnist_fp16_opt::thread_neg_mul13_fu_11731_p2() {
    neg_mul13_fu_11731_p2 = (!ap_const_lv25_0.is_01() || !tmp_840_reg_25187.read().is_01())? sc_lv<25>(): (sc_biguint<25>(ap_const_lv25_0) - sc_biguint<25>(tmp_840_reg_25187.read()));
}

void mnist_fp16_opt::thread_neg_mul14_fu_5401_p2() {
    neg_mul14_fu_5401_p2 = (!ap_const_lv25_0.is_01() || !tmp_857_reg_23410.read().is_01())? sc_lv<25>(): (sc_biguint<25>(ap_const_lv25_0) - sc_biguint<25>(tmp_857_reg_23410.read()));
}

void mnist_fp16_opt::thread_neg_mul15_fu_11346_p2() {
    neg_mul15_fu_11346_p2 = (!ap_const_lv25_0.is_01() || !tmp_933_reg_25026.read().is_01())? sc_lv<25>(): (sc_biguint<25>(ap_const_lv25_0) - sc_biguint<25>(tmp_933_reg_25026.read()));
}

void mnist_fp16_opt::thread_neg_mul16_fu_11781_p2() {
    neg_mul16_fu_11781_p2 = (!ap_const_lv25_0.is_01() || !tmp_942_reg_25197.read().is_01())? sc_lv<25>(): (sc_biguint<25>(ap_const_lv25_0) - sc_biguint<25>(tmp_942_reg_25197.read()));
}

void mnist_fp16_opt::thread_neg_mul17_fu_5465_p2() {
    neg_mul17_fu_5465_p2 = (!ap_const_lv25_0.is_01() || !tmp_967_reg_23431.read().is_01())? sc_lv<25>(): (sc_biguint<25>(ap_const_lv25_0) - sc_biguint<25>(tmp_967_reg_23431.read()));
}

void mnist_fp16_opt::thread_neg_mul18_fu_14233_p2() {
    neg_mul18_fu_14233_p2 = (!ap_const_lv25_0.is_01() || !tmp_1019_reg_25761.read().is_01())? sc_lv<25>(): (sc_biguint<25>(ap_const_lv25_0) - sc_biguint<25>(tmp_1019_reg_25761.read()));
}

void mnist_fp16_opt::thread_neg_mul19_fu_14281_p2() {
    neg_mul19_fu_14281_p2 = (!ap_const_lv25_0.is_01() || !tmp_1041_reg_25779.read().is_01())? sc_lv<25>(): (sc_biguint<25>(ap_const_lv25_0) - sc_biguint<25>(tmp_1041_reg_25779.read()));
}

void mnist_fp16_opt::thread_neg_mul1_fu_5849_p2() {
    neg_mul1_fu_5849_p2 = (!ap_const_lv25_0.is_01() || !tmp_152_reg_23557.read().is_01())? sc_lv<25>(): (sc_biguint<25>(ap_const_lv25_0) - sc_biguint<25>(tmp_152_reg_23557.read()));
}

void mnist_fp16_opt::thread_neg_mul20_fu_11388_p2() {
    neg_mul20_fu_11388_p2 = (!ap_const_lv25_0.is_01() || !tmp_1071_reg_25049.read().is_01())? sc_lv<25>(): (sc_biguint<25>(ap_const_lv25_0) - sc_biguint<25>(tmp_1071_reg_25049.read()));
}

void mnist_fp16_opt::thread_neg_mul21_fu_11855_p2() {
    neg_mul21_fu_11855_p2 = (!ap_const_lv25_0.is_01() || !tmp_1093_reg_25219.read().is_01())? sc_lv<25>(): (sc_biguint<25>(ap_const_lv25_0) - sc_biguint<25>(tmp_1093_reg_25219.read()));
}

void mnist_fp16_opt::thread_neg_mul22_fu_5529_p2() {
    neg_mul22_fu_5529_p2 = (!ap_const_lv25_0.is_01() || !tmp_1124_reg_23452.read().is_01())? sc_lv<25>(): (sc_biguint<25>(ap_const_lv25_0) - sc_biguint<25>(tmp_1124_reg_23452.read()));
}

void mnist_fp16_opt::thread_neg_mul23_fu_11479_p2() {
    neg_mul23_fu_11479_p2 = (!ap_const_lv25_0.is_01() || !tmp_1212_reg_25082.read().is_01())? sc_lv<25>(): (sc_biguint<25>(ap_const_lv25_0) - sc_biguint<25>(tmp_1212_reg_25082.read()));
}

void mnist_fp16_opt::thread_neg_mul24_fu_11905_p2() {
    neg_mul24_fu_11905_p2 = (!ap_const_lv25_0.is_01() || !tmp_1228_reg_25229.read().is_01())? sc_lv<25>(): (sc_biguint<25>(ap_const_lv25_0) - sc_biguint<25>(tmp_1228_reg_25229.read()));
}

void mnist_fp16_opt::thread_neg_mul25_fu_5593_p2() {
    neg_mul25_fu_5593_p2 = (!ap_const_lv25_0.is_01() || !tmp_1264_reg_23473.read().is_01())? sc_lv<25>(): (sc_biguint<25>(ap_const_lv25_0) - sc_biguint<25>(tmp_1264_reg_23473.read()));
}

void mnist_fp16_opt::thread_neg_mul26_fu_11521_p2() {
    neg_mul26_fu_11521_p2 = (!ap_const_lv25_0.is_01() || !tmp_1310_reg_25105.read().is_01())? sc_lv<25>(): (sc_biguint<25>(ap_const_lv25_0) - sc_biguint<25>(tmp_1310_reg_25105.read()));
}

void mnist_fp16_opt::thread_neg_mul27_fu_11979_p2() {
    neg_mul27_fu_11979_p2 = (!ap_const_lv25_0.is_01() || !tmp_1315_reg_25251.read().is_01())? sc_lv<25>(): (sc_biguint<25>(ap_const_lv25_0) - sc_biguint<25>(tmp_1315_reg_25251.read()));
}

void mnist_fp16_opt::thread_neg_mul28_fu_5657_p2() {
    neg_mul28_fu_5657_p2 = (!ap_const_lv25_0.is_01() || !tmp_1322_reg_23494.read().is_01())? sc_lv<25>(): (sc_biguint<25>(ap_const_lv25_0) - sc_biguint<25>(tmp_1322_reg_23494.read()));
}

void mnist_fp16_opt::thread_neg_mul29_fu_11583_p2() {
    neg_mul29_fu_11583_p2 = (!ap_const_lv25_0.is_01() || !tmp_1333_reg_25151.read().is_01())? sc_lv<25>(): (sc_biguint<25>(ap_const_lv25_0) - sc_biguint<25>(tmp_1333_reg_25151.read()));
}

void mnist_fp16_opt::thread_neg_mul2_fu_4777_p2() {
    neg_mul2_fu_4777_p2 = (!ap_const_lv25_0.is_01() || !tmp_251_reg_23236.read().is_01())? sc_lv<25>(): (sc_biguint<25>(ap_const_lv25_0) - sc_biguint<25>(tmp_251_reg_23236.read()));
}

void mnist_fp16_opt::thread_neg_mul30_fu_12029_p2() {
    neg_mul30_fu_12029_p2 = (!ap_const_lv25_0.is_01() || !tmp_1338_reg_25261.read().is_01())? sc_lv<25>(): (sc_biguint<25>(ap_const_lv25_0) - sc_biguint<25>(tmp_1338_reg_25261.read()));
}

void mnist_fp16_opt::thread_neg_mul31_fu_5737_p2() {
    neg_mul31_fu_5737_p2 = (!ap_const_lv25_0.is_01() || !tmp_1345_reg_23515.read().is_01())? sc_lv<25>(): (sc_biguint<25>(ap_const_lv25_0) - sc_biguint<25>(tmp_1345_reg_23515.read()));
}

void mnist_fp16_opt::thread_neg_mul32_fu_17163_p2() {
    neg_mul32_fu_17163_p2 = (!ap_const_lv21_0.is_01() || !tmp_1364_reg_26573.read().is_01())? sc_lv<21>(): (sc_biguint<21>(ap_const_lv21_0) - sc_biguint<21>(tmp_1364_reg_26573.read()));
}

void mnist_fp16_opt::thread_neg_mul33_fu_17413_p2() {
    neg_mul33_fu_17413_p2 = (!ap_const_lv21_0.is_01() || !tmp_1369_reg_26669.read().is_01())? sc_lv<21>(): (sc_biguint<21>(ap_const_lv21_0) - sc_biguint<21>(tmp_1369_reg_26669.read()));
}

void mnist_fp16_opt::thread_neg_mul34_fu_17211_p2() {
    neg_mul34_fu_17211_p2 = (!ap_const_lv21_0.is_01() || !tmp_1376_reg_26588.read().is_01())? sc_lv<21>(): (sc_biguint<21>(ap_const_lv21_0) - sc_biguint<21>(tmp_1376_reg_26588.read()));
}

void mnist_fp16_opt::thread_neg_mul35_fu_17593_p2() {
    neg_mul35_fu_17593_p2 = (!ap_const_lv21_0.is_01() || !tmp_1381_reg_26695.read().is_01())? sc_lv<21>(): (sc_biguint<21>(ap_const_lv21_0) - sc_biguint<21>(tmp_1381_reg_26695.read()));
}

void mnist_fp16_opt::thread_neg_mul36_fu_17290_p2() {
    neg_mul36_fu_17290_p2 = (!ap_const_lv21_0.is_01() || !tmp_1401_reg_26629.read().is_01())? sc_lv<21>(): (sc_biguint<21>(ap_const_lv21_0) - sc_biguint<21>(tmp_1401_reg_26629.read()));
}

void mnist_fp16_opt::thread_neg_mul37_fu_17643_p2() {
    neg_mul37_fu_17643_p2 = (!ap_const_lv21_0.is_01() || !tmp_1406_reg_26705.read().is_01())? sc_lv<21>(): (sc_biguint<21>(ap_const_lv21_0) - sc_biguint<21>(tmp_1406_reg_26705.read()));
}

void mnist_fp16_opt::thread_neg_mul38_fu_19344_p2() {
    neg_mul38_fu_19344_p2 = (!ap_const_lv23_0.is_01() || !tmp_1413_reg_27147.read().is_01())? sc_lv<23>(): (sc_biguint<23>(ap_const_lv23_0) - sc_biguint<23>(tmp_1413_reg_27147.read()));
}

void mnist_fp16_opt::thread_neg_mul39_fu_19392_p2() {
    neg_mul39_fu_19392_p2 = (!ap_const_lv23_0.is_01() || !tmp_1418_reg_27157.read().is_01())? sc_lv<23>(): (sc_biguint<23>(ap_const_lv23_0) - sc_biguint<23>(tmp_1418_reg_27157.read()));
}

void mnist_fp16_opt::thread_neg_mul3_fu_4871_p2() {
    neg_mul3_fu_4871_p2 = (!ap_const_lv25_0.is_01() || !tmp_359_reg_23261.read().is_01())? sc_lv<25>(): (sc_biguint<25>(ap_const_lv25_0) - sc_biguint<25>(tmp_359_reg_23261.read()));
}

void mnist_fp16_opt::thread_neg_mul40_fu_17332_p2() {
    neg_mul40_fu_17332_p2 = (!ap_const_lv21_0.is_01() || !tmp_1424_reg_26644.read().is_01())? sc_lv<21>(): (sc_biguint<21>(ap_const_lv21_0) - sc_biguint<21>(tmp_1424_reg_26644.read()));
}

void mnist_fp16_opt::thread_neg_mul41_fu_17487_p2() {
    neg_mul41_fu_17487_p2 = (!ap_const_lv21_0.is_01() || !tmp_1429_reg_26679.read().is_01())? sc_lv<21>(): (sc_biguint<21>(ap_const_lv21_0) - sc_biguint<21>(tmp_1429_reg_26679.read()));
}

void mnist_fp16_opt::thread_neg_mul4_fu_8199_p2() {
    neg_mul4_fu_8199_p2 = (!ap_const_lv27_0.is_01() || !tmp_415_reg_24151.read().is_01())? sc_lv<27>(): (sc_biguint<27>(ap_const_lv27_0) - sc_biguint<27>(tmp_415_reg_24151.read()));
}

void mnist_fp16_opt::thread_neg_mul5_fu_8247_p2() {
    neg_mul5_fu_8247_p2 = (!ap_const_lv27_0.is_01() || !tmp_438_reg_24169.read().is_01())? sc_lv<27>(): (sc_biguint<27>(ap_const_lv27_0) - sc_biguint<27>(tmp_438_reg_24169.read()));
}

void mnist_fp16_opt::thread_neg_mul6_fu_5145_p2() {
    neg_mul6_fu_5145_p2 = (!ap_const_lv25_0.is_01() || !tmp_534_reg_23290.read().is_01())? sc_lv<25>(): (sc_biguint<25>(ap_const_lv25_0) - sc_biguint<25>(tmp_534_reg_23290.read()));
}

void mnist_fp16_opt::thread_neg_mul7_fu_5209_p2() {
    neg_mul7_fu_5209_p2 = (!ap_const_lv25_0.is_01() || !tmp_702_reg_23347.read().is_01())? sc_lv<25>(): (sc_biguint<25>(ap_const_lv25_0) - sc_biguint<25>(tmp_702_reg_23347.read()));
}

void mnist_fp16_opt::thread_neg_mul8_fu_5273_p2() {
    neg_mul8_fu_5273_p2 = (!ap_const_lv25_0.is_01() || !tmp_730_reg_23368.read().is_01())? sc_lv<25>(): (sc_biguint<25>(ap_const_lv25_0) - sc_biguint<25>(tmp_730_reg_23368.read()));
}

void mnist_fp16_opt::thread_neg_mul9_fu_11219_p2() {
    neg_mul9_fu_11219_p2 = (!ap_const_lv25_0.is_01() || !tmp_783_reg_24970.read().is_01())? sc_lv<25>(): (sc_biguint<25>(ap_const_lv25_0) - sc_biguint<25>(tmp_783_reg_24970.read()));
}

void mnist_fp16_opt::thread_neg_mul_fu_5785_p2() {
    neg_mul_fu_5785_p2 = (!ap_const_lv25_0.is_01() || !tmp_88_reg_23531.read().is_01())? sc_lv<25>(): (sc_biguint<25>(ap_const_lv25_0) - sc_biguint<25>(tmp_88_reg_23531.read()));
}

void mnist_fp16_opt::thread_neg_ti10_fu_11690_p2() {
    neg_ti10_fu_11690_p2 = (!ap_const_lv2_0.is_01() || !tmp_796_fu_11686_p1.read().is_01())? sc_lv<2>(): (sc_biguint<2>(ap_const_lv2_0) - sc_biguint<2>(tmp_796_fu_11686_p1.read()));
}

void mnist_fp16_opt::thread_neg_ti11_fu_5366_p2() {
    neg_ti11_fu_5366_p2 = (!ap_const_lv12_0.is_01() || !tmp_813_fu_5359_p3.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_0) - sc_biguint<12>(tmp_813_fu_5359_p3.read()));
}

void mnist_fp16_opt::thread_neg_ti12_fu_11296_p2() {
    neg_ti12_fu_11296_p2 = (!ap_const_lv12_0.is_01() || !tmp_838_fu_11289_p3.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_0) - sc_biguint<12>(tmp_838_fu_11289_p3.read()));
}

void mnist_fp16_opt::thread_neg_ti13_fu_11764_p2() {
    neg_ti13_fu_11764_p2 = (!ap_const_lv2_0.is_01() || !tmp_845_fu_11760_p1.read().is_01())? sc_lv<2>(): (sc_biguint<2>(ap_const_lv2_0) - sc_biguint<2>(tmp_845_fu_11760_p1.read()));
}

void mnist_fp16_opt::thread_neg_ti14_fu_5430_p2() {
    neg_ti14_fu_5430_p2 = (!ap_const_lv12_0.is_01() || !tmp_863_fu_5423_p3.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_0) - sc_biguint<12>(tmp_863_fu_5423_p3.read()));
}

void mnist_fp16_opt::thread_neg_ti15_fu_11375_p2() {
    neg_ti15_fu_11375_p2 = (!ap_const_lv12_0.is_01() || !tmp_894_fu_11368_p3.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_0) - sc_biguint<12>(tmp_894_fu_11368_p3.read()));
}

void mnist_fp16_opt::thread_neg_ti16_fu_11814_p2() {
    neg_ti16_fu_11814_p2 = (!ap_const_lv2_0.is_01() || !tmp_952_fu_11810_p1.read().is_01())? sc_lv<2>(): (sc_biguint<2>(ap_const_lv2_0) - sc_biguint<2>(tmp_952_fu_11810_p1.read()));
}

void mnist_fp16_opt::thread_neg_ti17_fu_5494_p2() {
    neg_ti17_fu_5494_p2 = (!ap_const_lv12_0.is_01() || !tmp_912_fu_5487_p3.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_0) - sc_biguint<12>(tmp_912_fu_5487_p3.read()));
}

void mnist_fp16_opt::thread_neg_ti18_fu_14262_p2() {
    neg_ti18_fu_14262_p2 = (!ap_const_lv12_0.is_01() || !tmp_930_fu_14255_p3.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_0) - sc_biguint<12>(tmp_930_fu_14255_p3.read()));
}

void mnist_fp16_opt::thread_neg_ti19_fu_14314_p2() {
    neg_ti19_fu_14314_p2 = (!ap_const_lv3_0.is_01() || !tmp_1050_fu_14310_p1.read().is_01())? sc_lv<3>(): (sc_biguint<3>(ap_const_lv3_0) - sc_biguint<3>(tmp_1050_fu_14310_p1.read()));
}

void mnist_fp16_opt::thread_neg_ti1_fu_5878_p2() {
    neg_ti1_fu_5878_p2 = (!ap_const_lv12_0.is_01() || !tmp_169_fu_5871_p3.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_0) - sc_biguint<12>(tmp_169_fu_5871_p3.read()));
}

void mnist_fp16_opt::thread_neg_ti20_fu_11417_p2() {
    neg_ti20_fu_11417_p2 = (!ap_const_lv12_0.is_01() || !tmp_947_fu_11410_p3.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_0) - sc_biguint<12>(tmp_947_fu_11410_p3.read()));
}

void mnist_fp16_opt::thread_neg_ti21_fu_11888_p2() {
    neg_ti21_fu_11888_p2 = (!ap_const_lv2_0.is_01() || !tmp_1113_fu_11884_p1.read().is_01())? sc_lv<2>(): (sc_biguint<2>(ap_const_lv2_0) - sc_biguint<2>(tmp_1113_fu_11884_p1.read()));
}

void mnist_fp16_opt::thread_neg_ti22_fu_5558_p2() {
    neg_ti22_fu_5558_p2 = (!ap_const_lv12_0.is_01() || !tmp_965_fu_5551_p3.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_0) - sc_biguint<12>(tmp_965_fu_5551_p3.read()));
}

void mnist_fp16_opt::thread_neg_ti23_fu_11508_p2() {
    neg_ti23_fu_11508_p2 = (!ap_const_lv12_0.is_01() || !tmp_984_fu_11501_p3.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_0) - sc_biguint<12>(tmp_984_fu_11501_p3.read()));
}

void mnist_fp16_opt::thread_neg_ti24_fu_11938_p2() {
    neg_ti24_fu_11938_p2 = (!ap_const_lv2_0.is_01() || !tmp_1246_fu_11934_p1.read().is_01())? sc_lv<2>(): (sc_biguint<2>(ap_const_lv2_0) - sc_biguint<2>(tmp_1246_fu_11934_p1.read()));
}

void mnist_fp16_opt::thread_neg_ti25_fu_5622_p2() {
    neg_ti25_fu_5622_p2 = (!ap_const_lv12_0.is_01() || !tmp_1002_fu_5615_p3.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_0) - sc_biguint<12>(tmp_1002_fu_5615_p3.read()));
}

void mnist_fp16_opt::thread_neg_ti26_fu_11550_p2() {
    neg_ti26_fu_11550_p2 = (!ap_const_lv12_0.is_01() || !tmp_1030_fu_11543_p3.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_0) - sc_biguint<12>(tmp_1030_fu_11543_p3.read()));
}

void mnist_fp16_opt::thread_neg_ti27_fu_12012_p2() {
    neg_ti27_fu_12012_p2 = (!ap_const_lv2_0.is_01() || !tmp_1318_fu_12008_p1.read().is_01())? sc_lv<2>(): (sc_biguint<2>(ap_const_lv2_0) - sc_biguint<2>(tmp_1318_fu_12008_p1.read()));
}

void mnist_fp16_opt::thread_neg_ti28_fu_5686_p2() {
    neg_ti28_fu_5686_p2 = (!ap_const_lv12_0.is_01() || !tmp_1048_fu_5679_p3.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_0) - sc_biguint<12>(tmp_1048_fu_5679_p3.read()));
}

void mnist_fp16_opt::thread_neg_ti29_fu_11612_p2() {
    neg_ti29_fu_11612_p2 = (!ap_const_lv12_0.is_01() || !tmp_1066_fu_11605_p3.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_0) - sc_biguint<12>(tmp_1066_fu_11605_p3.read()));
}

void mnist_fp16_opt::thread_neg_ti2_fu_4806_p2() {
    neg_ti2_fu_4806_p2 = (!ap_const_lv12_0.is_01() || !tmp_266_fu_4799_p3.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_0) - sc_biguint<12>(tmp_266_fu_4799_p3.read()));
}

void mnist_fp16_opt::thread_neg_ti30_fu_12062_p2() {
    neg_ti30_fu_12062_p2 = (!ap_const_lv2_0.is_01() || !tmp_1341_fu_12058_p1.read().is_01())? sc_lv<2>(): (sc_biguint<2>(ap_const_lv2_0) - sc_biguint<2>(tmp_1341_fu_12058_p1.read()));
}

void mnist_fp16_opt::thread_neg_ti31_fu_5766_p2() {
    neg_ti31_fu_5766_p2 = (!ap_const_lv12_0.is_01() || !tmp_1084_fu_5759_p3.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_0) - sc_biguint<12>(tmp_1084_fu_5759_p3.read()));
}

void mnist_fp16_opt::thread_neg_ti32_fu_17192_p2() {
    neg_ti32_fu_17192_p2 = (!ap_const_lv10_0.is_01() || !tmp_1110_fu_17185_p3.read().is_01())? sc_lv<10>(): (sc_biguint<10>(ap_const_lv10_0) - sc_biguint<10>(tmp_1110_fu_17185_p3.read()));
}

void mnist_fp16_opt::thread_neg_ti33_fu_17446_p2() {
    neg_ti33_fu_17446_p2 = (!ap_const_lv3_0.is_01() || !tmp_1372_fu_17442_p1.read().is_01())? sc_lv<3>(): (sc_biguint<3>(ap_const_lv3_0) - sc_biguint<3>(tmp_1372_fu_17442_p1.read()));
}

void mnist_fp16_opt::thread_neg_ti34_fu_17240_p2() {
    neg_ti34_fu_17240_p2 = (!ap_const_lv10_0.is_01() || !tmp_1133_fu_17233_p3.read().is_01())? sc_lv<10>(): (sc_biguint<10>(ap_const_lv10_0) - sc_biguint<10>(tmp_1133_fu_17233_p3.read()));
}

void mnist_fp16_opt::thread_neg_ti35_fu_17626_p2() {
    neg_ti35_fu_17626_p2 = (!ap_const_lv3_0.is_01() || !tmp_1384_fu_17622_p1.read().is_01())? sc_lv<3>(): (sc_biguint<3>(ap_const_lv3_0) - sc_biguint<3>(tmp_1384_fu_17622_p1.read()));
}

void mnist_fp16_opt::thread_neg_ti36_fu_17319_p2() {
    neg_ti36_fu_17319_p2 = (!ap_const_lv10_0.is_01() || !tmp_1170_fu_17312_p3.read().is_01())? sc_lv<10>(): (sc_biguint<10>(ap_const_lv10_0) - sc_biguint<10>(tmp_1170_fu_17312_p3.read()));
}

void mnist_fp16_opt::thread_neg_ti37_fu_17676_p2() {
    neg_ti37_fu_17676_p2 = (!ap_const_lv3_0.is_01() || !tmp_1409_fu_17672_p1.read().is_01())? sc_lv<3>(): (sc_biguint<3>(ap_const_lv3_0) - sc_biguint<3>(tmp_1409_fu_17672_p1.read()));
}

void mnist_fp16_opt::thread_neg_ti38_fu_19373_p2() {
    neg_ti38_fu_19373_p2 = (!ap_const_lv11_0.is_01() || !tmp_1192_fu_19366_p3.read().is_01())? sc_lv<11>(): (sc_biguint<11>(ap_const_lv11_0) - sc_biguint<11>(tmp_1192_fu_19366_p3.read()));
}

void mnist_fp16_opt::thread_neg_ti39_fu_19425_p2() {
    neg_ti39_fu_19425_p2 = (!ap_const_lv4_0.is_01() || !tmp_1421_fu_19421_p1.read().is_01())? sc_lv<4>(): (sc_biguint<4>(ap_const_lv4_0) - sc_biguint<4>(tmp_1421_fu_19421_p1.read()));
}

void mnist_fp16_opt::thread_neg_ti3_fu_4900_p2() {
    neg_ti3_fu_4900_p2 = (!ap_const_lv12_0.is_01() || !tmp_374_fu_4893_p3.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_0) - sc_biguint<12>(tmp_374_fu_4893_p3.read()));
}

void mnist_fp16_opt::thread_neg_ti40_fu_17361_p2() {
    neg_ti40_fu_17361_p2 = (!ap_const_lv10_0.is_01() || !tmp_1207_fu_17354_p3.read().is_01())? sc_lv<10>(): (sc_biguint<10>(ap_const_lv10_0) - sc_biguint<10>(tmp_1207_fu_17354_p3.read()));
}

void mnist_fp16_opt::thread_neg_ti41_fu_17520_p2() {
    neg_ti41_fu_17520_p2 = (!ap_const_lv3_0.is_01() || !tmp_1432_fu_17516_p1.read().is_01())? sc_lv<3>(): (sc_biguint<3>(ap_const_lv3_0) - sc_biguint<3>(tmp_1432_fu_17516_p1.read()));
}

void mnist_fp16_opt::thread_neg_ti4_fu_8228_p2() {
    neg_ti4_fu_8228_p2 = (!ap_const_lv13_0.is_01() || !tmp_428_fu_8221_p3.read().is_01())? sc_lv<13>(): (sc_biguint<13>(ap_const_lv13_0) - sc_biguint<13>(tmp_428_fu_8221_p3.read()));
}

void mnist_fp16_opt::thread_neg_ti5_fu_8280_p2() {
    neg_ti5_fu_8280_p2 = (!ap_const_lv2_0.is_01() || !tmp_448_fu_8276_p1.read().is_01())? sc_lv<2>(): (sc_biguint<2>(ap_const_lv2_0) - sc_biguint<2>(tmp_448_fu_8276_p1.read()));
}

void mnist_fp16_opt::thread_neg_ti6_fu_5174_p2() {
    neg_ti6_fu_5174_p2 = (!ap_const_lv12_0.is_01() || !tmp_545_fu_5167_p3.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_0) - sc_biguint<12>(tmp_545_fu_5167_p3.read()));
}

void mnist_fp16_opt::thread_neg_ti7_fu_5238_p2() {
    neg_ti7_fu_5238_p2 = (!ap_const_lv12_0.is_01() || !tmp_708_fu_5231_p3.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_0) - sc_biguint<12>(tmp_708_fu_5231_p3.read()));
}

void mnist_fp16_opt::thread_neg_ti8_fu_5302_p2() {
    neg_ti8_fu_5302_p2 = (!ap_const_lv12_0.is_01() || !tmp_736_fu_5295_p3.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_0) - sc_biguint<12>(tmp_736_fu_5295_p3.read()));
}

void mnist_fp16_opt::thread_neg_ti9_fu_11248_p2() {
    neg_ti9_fu_11248_p2 = (!ap_const_lv12_0.is_01() || !tmp_789_fu_11241_p3.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_0) - sc_biguint<12>(tmp_789_fu_11241_p3.read()));
}

void mnist_fp16_opt::thread_neg_ti_fu_5814_p2() {
    neg_ti_fu_5814_p2 = (!ap_const_lv12_0.is_01() || !tmp_104_fu_5807_p3.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_0) - sc_biguint<12>(tmp_104_fu_5807_p3.read()));
}

void mnist_fp16_opt::thread_newSel10_fu_9425_p3() {
    newSel10_fu_9425_p3 = (!or_cond6_fu_9406_p2.read()[0].is_01())? sc_lv<16>(): ((or_cond6_fu_9406_p2.read()[0].to_bool())? newSel8_fu_9398_p3.read(): newSel9_fu_9412_p3.read());
}

void mnist_fp16_opt::thread_newSel11_fu_9547_p3() {
    newSel11_fu_9547_p3 = (!sel_tmp35_fu_9542_p2.read()[0].is_01())? sc_lv<16>(): ((sel_tmp35_fu_9542_p2.read()[0].to_bool())? tmp_697_fu_9484_p1.read(): tmp_696_fu_9464_p1.read());
}

void mnist_fp16_opt::thread_newSel12_fu_9561_p3() {
    newSel12_fu_9561_p3 = (!sel_tmp32_fu_9519_p2.read()[0].is_01())? sc_lv<16>(): ((sel_tmp32_fu_9519_p2.read()[0].to_bool())? storemerge6_fu_9468_p3.read(): tmp_694_reg_24487.read());
}

void mnist_fp16_opt::thread_newSel13_fu_9574_p3() {
    newSel13_fu_9574_p3 = (!or_cond9_fu_9555_p2.read()[0].is_01())? sc_lv<16>(): ((or_cond9_fu_9555_p2.read()[0].to_bool())? newSel11_fu_9547_p3.read(): newSel12_fu_9561_p3.read());
}

void mnist_fp16_opt::thread_newSel14_fu_13756_p3() {
    newSel14_fu_13756_p3 = (!sel_tmp44_reg_25661.read()[0].is_01())? sc_lv<16>(): ((sel_tmp44_reg_25661.read()[0].to_bool())? tmp_899_fu_13726_p1.read(): tmp_898_fu_13706_p1.read());
}

void mnist_fp16_opt::thread_newSel15_fu_13768_p3() {
    newSel15_fu_13768_p3 = (!sel_tmp41_fu_13746_p2.read()[0].is_01())? sc_lv<16>(): ((sel_tmp41_fu_13746_p2.read()[0].to_bool())? storemerge8_fu_13710_p3.read(): tmp_891_reg_25649.read());
}

void mnist_fp16_opt::thread_newSel16_fu_13781_p3() {
    newSel16_fu_13781_p3 = (!or_cond12_fu_13763_p2.read()[0].is_01())? sc_lv<16>(): ((or_cond12_fu_13763_p2.read()[0].to_bool())? newSel14_fu_13756_p3.read(): newSel15_fu_13768_p3.read());
}

void mnist_fp16_opt::thread_newSel18_fu_16289_p3() {
    newSel18_fu_16289_p3 = (!sel_tmp53_reg_26305.read()[0].is_01())? sc_lv<16>(): ((sel_tmp53_reg_26305.read()[0].to_bool())? tmp_1279_fu_16259_p1.read(): tmp_1278_fu_16239_p1.read());
}

void mnist_fp16_opt::thread_newSel19_fu_16301_p3() {
    newSel19_fu_16301_p3 = (!sel_tmp50_fu_16279_p2.read()[0].is_01())? sc_lv<16>(): ((sel_tmp50_fu_16279_p2.read()[0].to_bool())? storemerge5_fu_16243_p3.read(): tmp_1276_reg_26293.read());
}

void mnist_fp16_opt::thread_newSel1_fu_7694_p3() {
    newSel1_fu_7694_p3 = (!sel_tmp9_fu_7672_p2.read()[0].is_01())? sc_lv<16>(): ((sel_tmp9_fu_7672_p2.read()[0].to_bool())? storemerge_fu_7636_p3.read(): tmp_214_reg_24025.read());
}

void mnist_fp16_opt::thread_newSel20_fu_16314_p3() {
    newSel20_fu_16314_p3 = (!or_cond15_fu_16296_p2.read()[0].is_01())? sc_lv<16>(): ((or_cond15_fu_16296_p2.read()[0].to_bool())? newSel18_fu_16289_p3.read(): newSel19_fu_16301_p3.read());
}

void mnist_fp16_opt::thread_newSel22_fu_15403_p3() {
    newSel22_fu_15403_p3 = (!sel_tmp62_fu_15398_p2.read()[0].is_01())? sc_lv<16>(): ((sel_tmp62_fu_15398_p2.read()[0].to_bool())? tmp_1298_fu_15340_p1.read(): tmp_1297_fu_15320_p1.read());
}

void mnist_fp16_opt::thread_newSel23_fu_15417_p3() {
    newSel23_fu_15417_p3 = (!sel_tmp59_fu_15375_p2.read()[0].is_01())? sc_lv<16>(): ((sel_tmp59_fu_15375_p2.read()[0].to_bool())? storemerge1_fu_15324_p3.read(): tmp_1295_reg_26053.read());
}

void mnist_fp16_opt::thread_newSel24_fu_15430_p3() {
    newSel24_fu_15430_p3 = (!or_cond18_fu_15411_p2.read()[0].is_01())? sc_lv<16>(): ((or_cond18_fu_15411_p2.read()[0].to_bool())? newSel22_fu_15403_p3.read(): newSel23_fu_15417_p3.read());
}

void mnist_fp16_opt::thread_newSel25_fu_15519_p3() {
    newSel25_fu_15519_p3 = (!sel_tmp71_reg_26092.read()[0].is_01())? sc_lv<16>(): ((sel_tmp71_reg_26092.read()[0].to_bool())? tmp_1305_fu_15489_p1.read(): tmp_1304_fu_15469_p1.read());
}

void mnist_fp16_opt::thread_newSel26_fu_15531_p3() {
    newSel26_fu_15531_p3 = (!sel_tmp68_fu_15509_p2.read()[0].is_01())? sc_lv<16>(): ((sel_tmp68_fu_15509_p2.read()[0].to_bool())? storemerge3_fu_15473_p3.read(): tmp_1302_reg_26080.read());
}

void mnist_fp16_opt::thread_newSel27_fu_15544_p3() {
    newSel27_fu_15544_p3 = (!or_cond21_fu_15526_p2.read()[0].is_01())? sc_lv<16>(): ((or_cond21_fu_15526_p2.read()[0].to_bool())? newSel25_fu_15519_p3.read(): newSel26_fu_15531_p3.read());
}

void mnist_fp16_opt::thread_newSel28_fu_18902_p3() {
    newSel28_fu_18902_p3 = (!sel_tmp80_reg_27070.read()[0].is_01())? sc_lv<16>(): ((sel_tmp80_reg_27070.read()[0].to_bool())? tmp_1395_fu_18872_p1.read(): tmp_1394_fu_18852_p1.read());
}

void mnist_fp16_opt::thread_newSel29_fu_18914_p3() {
    newSel29_fu_18914_p3 = (!sel_tmp77_fu_18892_p2.read()[0].is_01())? sc_lv<16>(): ((sel_tmp77_fu_18892_p2.read()[0].to_bool())? storemerge7_fu_18856_p3.read(): tmp_1392_reg_27058.read());
}

void mnist_fp16_opt::thread_newSel2_fu_7707_p3() {
    newSel2_fu_7707_p3 = (!or_cond_fu_7689_p2.read()[0].is_01())? sc_lv<16>(): ((or_cond_fu_7689_p2.read()[0].to_bool())? newSel_fu_7682_p3.read(): newSel1_fu_7694_p3.read());
}

void mnist_fp16_opt::thread_newSel30_fu_18927_p3() {
    newSel30_fu_18927_p3 = (!or_cond24_fu_18909_p2.read()[0].is_01())? sc_lv<16>(): ((or_cond24_fu_18909_p2.read()[0].to_bool())? newSel28_fu_18902_p3.read(): newSel29_fu_18914_p3.read());
}

void mnist_fp16_opt::thread_newSel32_fu_21405_p3() {
    newSel32_fu_21405_p3 = (!sel_tmp98_reg_27700.read()[0].is_01())? sc_lv<16>(): ((sel_tmp98_reg_27700.read()[0].to_bool())? tmp_1453_fu_21375_p1.read(): tmp_1452_fu_21355_p1.read());
}

void mnist_fp16_opt::thread_newSel33_fu_21417_p3() {
    newSel33_fu_21417_p3 = (!sel_tmp95_fu_21395_p2.read()[0].is_01())? sc_lv<16>(): ((sel_tmp95_fu_21395_p2.read()[0].to_bool())? storemerge10_fu_21359_p3.read(): tmp_1450_reg_27688.read());
}

void mnist_fp16_opt::thread_newSel34_fu_21430_p3() {
    newSel34_fu_21430_p3 = (!or_cond30_fu_21412_p2.read()[0].is_01())? sc_lv<16>(): ((or_cond30_fu_21412_p2.read()[0].to_bool())? newSel32_fu_21405_p3.read(): newSel33_fu_21417_p3.read());
}

void mnist_fp16_opt::thread_newSel36_fu_22121_p3() {
    newSel36_fu_22121_p3 = (!sel_tmp89_reg_27897.read()[0].is_01())? sc_lv<16>(): ((sel_tmp89_reg_27897.read()[0].to_bool())? tmp_1488_fu_22091_p1.read(): tmp_1487_fu_22071_p1.read());
}

void mnist_fp16_opt::thread_newSel37_fu_22133_p3() {
    newSel37_fu_22133_p3 = (!sel_tmp86_fu_22111_p2.read()[0].is_01())? sc_lv<16>(): ((sel_tmp86_fu_22111_p2.read()[0].to_bool())? storemerge9_fu_22075_p3.read(): tmp_1485_reg_27885.read());
}

void mnist_fp16_opt::thread_newSel38_fu_22146_p3() {
    newSel38_fu_22146_p3 = (!or_cond27_fu_22128_p2.read()[0].is_01())? sc_lv<16>(): ((or_cond27_fu_22128_p2.read()[0].to_bool())? newSel36_fu_22121_p3.read(): newSel37_fu_22133_p3.read());
}

void mnist_fp16_opt::thread_newSel40_fu_20502_p3() {
    newSel40_fu_20502_p3 = (!sel_tmp107_reg_27453.read()[0].is_01())? sc_lv<16>(): ((sel_tmp107_reg_27453.read()[0].to_bool())? tmp_1474_fu_20472_p1.read(): tmp_1473_fu_20452_p1.read());
}

void mnist_fp16_opt::thread_newSel41_fu_20514_p3() {
    newSel41_fu_20514_p3 = (!sel_tmp104_fu_20492_p2.read()[0].is_01())? sc_lv<16>(): ((sel_tmp104_fu_20492_p2.read()[0].to_bool())? storemerge11_fu_20456_p3.read(): tmp_1471_reg_27441.read());
}

void mnist_fp16_opt::thread_newSel42_fu_20527_p3() {
    newSel42_fu_20527_p3 = (!or_cond33_fu_20509_p2.read()[0].is_01())? sc_lv<16>(): ((or_cond33_fu_20509_p2.read()[0].to_bool())? newSel40_fu_20502_p3.read(): newSel41_fu_20514_p3.read());
}

void mnist_fp16_opt::thread_newSel43_fu_20649_p3() {
    newSel43_fu_20649_p3 = (!sel_tmp116_fu_20644_p2.read()[0].is_01())? sc_lv<16>(): ((sel_tmp116_fu_20644_p2.read()[0].to_bool())? tmp_1481_fu_20586_p1.read(): tmp_1480_fu_20566_p1.read());
}

void mnist_fp16_opt::thread_newSel44_fu_20663_p3() {
    newSel44_fu_20663_p3 = (!sel_tmp113_fu_20621_p2.read()[0].is_01())? sc_lv<16>(): ((sel_tmp113_fu_20621_p2.read()[0].to_bool())? storemerge12_fu_20570_p3.read(): tmp_1478_reg_27482.read());
}

void mnist_fp16_opt::thread_newSel45_fu_20676_p3() {
    newSel45_fu_20676_p3 = (!or_cond36_fu_20657_p2.read()[0].is_01())? sc_lv<16>(): ((or_cond36_fu_20657_p2.read()[0].to_bool())? newSel43_fu_20649_p3.read(): newSel44_fu_20663_p3.read());
}

void mnist_fp16_opt::thread_newSel4_fu_10319_p3() {
    newSel4_fu_10319_p3 = (!sel_tmp17_reg_24705.read()[0].is_01())? sc_lv<16>(): ((sel_tmp17_reg_24705.read()[0].to_bool())? tmp_616_fu_10289_p1.read(): tmp_614_fu_10269_p1.read());
}

void mnist_fp16_opt::thread_newSel5_fu_10331_p3() {
    newSel5_fu_10331_p3 = (!sel_tmp14_fu_10309_p2.read()[0].is_01())? sc_lv<16>(): ((sel_tmp14_fu_10309_p2.read()[0].to_bool())? storemerge2_fu_10273_p3.read(): tmp_602_reg_24693.read());
}

void mnist_fp16_opt::thread_newSel6_fu_10344_p3() {
    newSel6_fu_10344_p3 = (!or_cond3_fu_10326_p2.read()[0].is_01())? sc_lv<16>(): ((or_cond3_fu_10326_p2.read()[0].to_bool())? newSel4_fu_10319_p3.read(): newSel5_fu_10331_p3.read());
}

void mnist_fp16_opt::thread_newSel8_fu_9398_p3() {
    newSel8_fu_9398_p3 = (!sel_tmp26_fu_9393_p2.read()[0].is_01())? sc_lv<16>(): ((sel_tmp26_fu_9393_p2.read()[0].to_bool())? tmp_690_fu_9335_p1.read(): tmp_689_fu_9315_p1.read());
}

void mnist_fp16_opt::thread_newSel9_fu_9412_p3() {
    newSel9_fu_9412_p3 = (!sel_tmp23_fu_9370_p2.read()[0].is_01())? sc_lv<16>(): ((sel_tmp23_fu_9370_p2.read()[0].to_bool())? storemerge4_fu_9319_p3.read(): tmp_687_reg_24453.read());
}

void mnist_fp16_opt::thread_newSel_fu_7682_p3() {
    newSel_fu_7682_p3 = (!sel_tmp4_reg_24037.read()[0].is_01())? sc_lv<16>(): ((sel_tmp4_reg_24037.read()[0].to_bool())? tmp_223_fu_7652_p1.read(): tmp_220_fu_7632_p1.read());
}

void mnist_fp16_opt::thread_not_exitcond_flatten_10_fu_14705_p2() {
    not_exitcond_flatten_10_fu_14705_p2 = (exitcond_flatten26_fu_14637_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp16_opt::thread_not_exitcond_flatten_11_fu_15902_p2() {
    not_exitcond_flatten_11_fu_15902_p2 = (exitcond_flatten29_fu_15846_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp16_opt::thread_not_exitcond_flatten_12_fu_14878_p2() {
    not_exitcond_flatten_12_fu_14878_p2 = (exitcond_flatten30_fu_14818_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp16_opt::thread_not_exitcond_flatten_13_fu_16448_p2() {
    not_exitcond_flatten_13_fu_16448_p2 = (exitcond_flatten32_reg_26320.read() ^ ap_const_lv1_1);
}

void mnist_fp16_opt::thread_not_exitcond_flatten_14_fu_18061_p2() {
    not_exitcond_flatten_14_fu_18061_p2 = (exitcond_flatten36_fu_18013_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp16_opt::thread_not_exitcond_flatten_15_fu_18523_p2() {
    not_exitcond_flatten_15_fu_18523_p2 = (exitcond_flatten39_fu_18475_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp16_opt::thread_not_exitcond_flatten_16_fu_18205_p2() {
    not_exitcond_flatten_16_fu_18205_p2 = (exitcond_flatten40_fu_18174_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp16_opt::thread_not_exitcond_flatten_17_fu_19056_p2() {
    not_exitcond_flatten_17_fu_19056_p2 = (exitcond_flatten42_fu_19016_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp16_opt::thread_not_exitcond_flatten_18_fu_19792_p2() {
    not_exitcond_flatten_18_fu_19792_p2 = (exitcond_flatten44_fu_19732_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp16_opt::thread_not_exitcond_flatten_19_fu_21026_p2() {
    not_exitcond_flatten_19_fu_21026_p2 = (exitcond_flatten47_fu_20978_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp16_opt::thread_not_exitcond_flatten_1_fu_7820_p2() {
    not_exitcond_flatten_1_fu_7820_p2 = (exitcond_flatten6_fu_7798_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp16_opt::thread_not_exitcond_flatten_20_fu_19936_p2() {
    not_exitcond_flatten_20_fu_19936_p2 = (exitcond_flatten48_fu_19905_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp16_opt::thread_not_exitcond_flatten_21_fu_6901_p2() {
    not_exitcond_flatten_21_fu_6901_p2 = (exitcond_flatten2_fu_6887_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp16_opt::thread_not_exitcond_flatten_2_fu_8611_p2() {
    not_exitcond_flatten_2_fu_8611_p2 = (exitcond_flatten8_fu_8597_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp16_opt::thread_not_exitcond_flatten_3_fu_9932_p2() {
    not_exitcond_flatten_3_fu_9932_p2 = (exitcond_flatten11_fu_9876_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp16_opt::thread_not_exitcond_flatten_4_fu_8861_p2() {
    not_exitcond_flatten_4_fu_8861_p2 = (exitcond_flatten12_fu_8774_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp16_opt::thread_not_exitcond_flatten_5_fu_10492_p2() {
    not_exitcond_flatten_5_fu_10492_p2 = (exitcond_flatten14_reg_24720.read() ^ ap_const_lv1_1);
}

void mnist_fp16_opt::thread_not_exitcond_flatten_6_fu_12944_p2() {
    not_exitcond_flatten_6_fu_12944_p2 = (exitcond_flatten18_fu_12876_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp16_opt::thread_not_exitcond_flatten_7_fu_13369_p2() {
    not_exitcond_flatten_7_fu_13369_p2 = (exitcond_flatten21_fu_13313_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp16_opt::thread_not_exitcond_flatten_8_fu_13117_p2() {
    not_exitcond_flatten_8_fu_13117_p2 = (exitcond_flatten22_fu_13057_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp16_opt::thread_not_exitcond_flatten_9_fu_13932_p2() {
    not_exitcond_flatten_9_fu_13932_p2 = (exitcond_flatten24_fu_13892_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp16_opt::thread_not_exitcond_flatten_fu_7295_p2() {
    not_exitcond_flatten_fu_7295_p2 = (exitcond_flatten4_fu_7239_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp16_opt::thread_not_zero1_V_fu_11046_p2() {
    not_zero1_V_fu_11046_p2 = (!ap_const_lv3_1.is_01() || !ap_phi_mux_p_15_phi_fu_2994_p4.read().is_01())? sc_lv<3>(): (sc_biguint<3>(ap_const_lv3_1) + sc_biguint<3>(ap_phi_mux_p_15_phi_fu_2994_p4.read()));
}

void mnist_fp16_opt::thread_not_zero2_V_fu_13886_p2() {
    not_zero2_V_fu_13886_p2 = (!ap_const_lv4_1.is_01() || !ap_phi_mux_p_26_phi_fu_3294_p4.read().is_01())? sc_lv<4>(): (sc_biguint<4>(ap_const_lv4_1) + sc_biguint<4>(ap_phi_mux_p_26_phi_fu_3294_p4.read()));
}

void mnist_fp16_opt::thread_not_zero3_V_fu_17001_p2() {
    not_zero3_V_fu_17001_p2 = (!ap_phi_mux_p_42_phi_fu_3646_p4.read().is_01() || !ap_const_lv4_1.is_01())? sc_lv<4>(): (sc_biguint<4>(ap_phi_mux_p_42_phi_fu_3646_p4.read()) + sc_biguint<4>(ap_const_lv4_1));
}

void mnist_fp16_opt::thread_not_zero4_V_fu_19010_p2() {
    not_zero4_V_fu_19010_p2 = (!ap_phi_mux_p_52_phi_fu_3909_p4.read().is_01() || !ap_const_lv5_1.is_01())? sc_lv<5>(): (sc_biguint<5>(ap_phi_mux_p_52_phi_fu_3909_p4.read()) + sc_biguint<5>(ap_const_lv5_1));
}

void mnist_fp16_opt::thread_not_zero_V_fu_7792_p2() {
    not_zero_V_fu_7792_p2 = (!ap_const_lv3_1.is_01() || !ap_phi_mux_p_6_phi_fu_2638_p4.read().is_01())? sc_lv<3>(): (sc_biguint<3>(ap_const_lv3_1) + sc_biguint<3>(ap_phi_mux_p_6_phi_fu_2638_p4.read()));
}

void mnist_fp16_opt::thread_notlhs10_fu_21826_p2() {
    notlhs10_fu_21826_p2 = (!tmp_640_fu_21794_p4.read().is_01() || !ap_const_lv8_FF.is_01())? sc_lv<1>(): sc_lv<1>(tmp_640_fu_21794_p4.read() != ap_const_lv8_FF);
}

void mnist_fp16_opt::thread_notlhs11_fu_21844_p2() {
    notlhs11_fu_21844_p2 = (!tmp_642_fu_21812_p4.read().is_01() || !ap_const_lv8_FF.is_01())? sc_lv<1>(): sc_lv<1>(tmp_642_fu_21812_p4.read() != ap_const_lv8_FF);
}

void mnist_fp16_opt::thread_notlhs12_fu_22504_p2() {
    notlhs12_fu_22504_p2 = (!tmp_649_fu_22472_p4.read().is_01() || !ap_const_lv8_FF.is_01())? sc_lv<1>(): sc_lv<1>(tmp_649_fu_22472_p4.read() != ap_const_lv8_FF);
}

void mnist_fp16_opt::thread_notlhs13_fu_22522_p2() {
    notlhs13_fu_22522_p2 = (!tmp_651_fu_22490_p4.read().is_01() || !ap_const_lv8_FF.is_01())? sc_lv<1>(): sc_lv<1>(tmp_651_fu_22490_p4.read() != ap_const_lv8_FF);
}

void mnist_fp16_opt::thread_notlhs14_fu_22646_p2() {
    notlhs14_fu_22646_p2 = (!tmp_663_fu_22614_p4.read().is_01() || !ap_const_lv8_FF.is_01())? sc_lv<1>(): sc_lv<1>(tmp_663_fu_22614_p4.read() != ap_const_lv8_FF);
}

void mnist_fp16_opt::thread_notlhs15_fu_22664_p2() {
    notlhs15_fu_22664_p2 = (!tmp_665_fu_22632_p4.read().is_01() || !ap_const_lv8_FF.is_01())? sc_lv<1>(): sc_lv<1>(tmp_665_fu_22632_p4.read() != ap_const_lv8_FF);
}

void mnist_fp16_opt::thread_notlhs1_fu_10067_p2() {
    notlhs1_fu_10067_p2 = (!tmp_232_fu_10053_p4.read().is_01() || !ap_const_lv8_FF.is_01())? sc_lv<1>(): sc_lv<1>(tmp_232_fu_10053_p4.read() != ap_const_lv8_FF);
}

void mnist_fp16_opt::thread_notlhs2_fu_10961_p2() {
    notlhs2_fu_10961_p2 = (!tmp_323_fu_10929_p4.read().is_01() || !ap_const_lv8_FF.is_01())? sc_lv<1>(): sc_lv<1>(tmp_323_fu_10929_p4.read() != ap_const_lv8_FF);
}

void mnist_fp16_opt::thread_notlhs3_fu_10979_p2() {
    notlhs3_fu_10979_p2 = (!tmp_327_fu_10947_p4.read().is_01() || !ap_const_lv8_FF.is_01())? sc_lv<1>(): sc_lv<1>(tmp_327_fu_10947_p4.read() != ap_const_lv8_FF);
}

void mnist_fp16_opt::thread_notlhs4_fu_13504_p2() {
    notlhs4_fu_13504_p2 = (!tmp_370_fu_13490_p4.read().is_01() || !ap_const_lv8_FF.is_01())? sc_lv<1>(): sc_lv<1>(tmp_370_fu_13490_p4.read() != ap_const_lv8_FF);
}

void mnist_fp16_opt::thread_notlhs5_fu_16037_p2() {
    notlhs5_fu_16037_p2 = (!tmp_490_fu_16023_p4.read().is_01() || !ap_const_lv8_FF.is_01())? sc_lv<1>(): sc_lv<1>(tmp_490_fu_16023_p4.read() != ap_const_lv8_FF);
}

void mnist_fp16_opt::thread_notlhs6_fu_16916_p2() {
    notlhs6_fu_16916_p2 = (!tmp_593_fu_16884_p4.read().is_01() || !ap_const_lv8_FF.is_01())? sc_lv<1>(): sc_lv<1>(tmp_593_fu_16884_p4.read() != ap_const_lv8_FF);
}

void mnist_fp16_opt::thread_notlhs7_fu_16934_p2() {
    notlhs7_fu_16934_p2 = (!tmp_595_fu_16902_p4.read().is_01() || !ap_const_lv8_FF.is_01())? sc_lv<1>(): sc_lv<1>(tmp_595_fu_16902_p4.read() != ap_const_lv8_FF);
}

void mnist_fp16_opt::thread_notlhs8_fu_18650_p2() {
    notlhs8_fu_18650_p2 = (!tmp_603_fu_18636_p4.read().is_01() || !ap_const_lv8_FF.is_01())? sc_lv<1>(): sc_lv<1>(tmp_603_fu_18636_p4.read() != ap_const_lv8_FF);
}

void mnist_fp16_opt::thread_notlhs9_fu_21153_p2() {
    notlhs9_fu_21153_p2 = (!tmp_627_fu_21139_p4.read().is_01() || !ap_const_lv8_FF.is_01())? sc_lv<1>(): sc_lv<1>(tmp_627_fu_21139_p4.read() != ap_const_lv8_FF);
}

void mnist_fp16_opt::thread_notlhs_fu_7430_p2() {
    notlhs_fu_7430_p2 = (!tmp_45_fu_7416_p4.read().is_01() || !ap_const_lv8_FF.is_01())? sc_lv<1>(): sc_lv<1>(tmp_45_fu_7416_p4.read() != ap_const_lv8_FF);
}

void mnist_fp16_opt::thread_notrhs10_fu_21832_p2() {
    notrhs10_fu_21832_p2 = (!tmp_1499_fu_21804_p1.read().is_01() || !ap_const_lv23_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_1499_fu_21804_p1.read() == ap_const_lv23_0);
}

void mnist_fp16_opt::thread_notrhs11_fu_21850_p2() {
    notrhs11_fu_21850_p2 = (!tmp_1500_fu_21822_p1.read().is_01() || !ap_const_lv23_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_1500_fu_21822_p1.read() == ap_const_lv23_0);
}

void mnist_fp16_opt::thread_notrhs12_fu_22510_p2() {
    notrhs12_fu_22510_p2 = (!tmp_1501_fu_22482_p1.read().is_01() || !ap_const_lv23_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_1501_fu_22482_p1.read() == ap_const_lv23_0);
}

void mnist_fp16_opt::thread_notrhs13_fu_22528_p2() {
    notrhs13_fu_22528_p2 = (!tmp_1502_fu_22500_p1.read().is_01() || !ap_const_lv23_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_1502_fu_22500_p1.read() == ap_const_lv23_0);
}

void mnist_fp16_opt::thread_notrhs14_fu_22652_p2() {
    notrhs14_fu_22652_p2 = (!tmp_1512_fu_22624_p1.read().is_01() || !ap_const_lv23_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_1512_fu_22624_p1.read() == ap_const_lv23_0);
}

void mnist_fp16_opt::thread_notrhs15_fu_22670_p2() {
    notrhs15_fu_22670_p2 = (!tmp_1513_fu_22642_p1.read().is_01() || !ap_const_lv23_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_1513_fu_22642_p1.read() == ap_const_lv23_0);
}

void mnist_fp16_opt::thread_notrhs1_fu_10073_p2() {
    notrhs1_fu_10073_p2 = (!tmp_587_fu_10063_p1.read().is_01() || !ap_const_lv23_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_587_fu_10063_p1.read() == ap_const_lv23_0);
}

void mnist_fp16_opt::thread_notrhs2_fu_10967_p2() {
    notrhs2_fu_10967_p2 = (!tmp_777_fu_10939_p1.read().is_01() || !ap_const_lv23_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_777_fu_10939_p1.read() == ap_const_lv23_0);
}

void mnist_fp16_opt::thread_notrhs3_fu_10985_p2() {
    notrhs3_fu_10985_p2 = (!tmp_778_fu_10957_p1.read().is_01() || !ap_const_lv23_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_778_fu_10957_p1.read() == ap_const_lv23_0);
}

void mnist_fp16_opt::thread_notrhs4_fu_13510_p2() {
    notrhs4_fu_13510_p2 = (!tmp_878_fu_13500_p1.read().is_01() || !ap_const_lv23_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_878_fu_13500_p1.read() == ap_const_lv23_0);
}

void mnist_fp16_opt::thread_notrhs5_fu_16043_p2() {
    notrhs5_fu_16043_p2 = (!tmp_1272_fu_16033_p1.read().is_01() || !ap_const_lv23_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_1272_fu_16033_p1.read() == ap_const_lv23_0);
}

void mnist_fp16_opt::thread_notrhs6_fu_16922_p2() {
    notrhs6_fu_16922_p2 = (!tmp_1362_fu_16894_p1.read().is_01() || !ap_const_lv23_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_1362_fu_16894_p1.read() == ap_const_lv23_0);
}

void mnist_fp16_opt::thread_notrhs7_fu_16940_p2() {
    notrhs7_fu_16940_p2 = (!tmp_1363_fu_16912_p1.read().is_01() || !ap_const_lv23_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_1363_fu_16912_p1.read() == ap_const_lv23_0);
}

void mnist_fp16_opt::thread_notrhs8_fu_18656_p2() {
    notrhs8_fu_18656_p2 = (!tmp_1388_fu_18646_p1.read().is_01() || !ap_const_lv23_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_1388_fu_18646_p1.read() == ap_const_lv23_0);
}

void mnist_fp16_opt::thread_notrhs9_fu_21159_p2() {
    notrhs9_fu_21159_p2 = (!tmp_1446_fu_21149_p1.read().is_01() || !ap_const_lv23_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_1446_fu_21149_p1.read() == ap_const_lv23_0);
}

void mnist_fp16_opt::thread_notrhs_fu_7436_p2() {
    notrhs_fu_7436_p2 = (!tmp_205_fu_7426_p1.read().is_01() || !ap_const_lv23_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_205_fu_7426_p1.read() == ap_const_lv23_0);
}

void mnist_fp16_opt::thread_num_zeros_1_fu_9684_p3() {
    num_zeros_1_fu_9684_p3 = esl_cttz<32,32>(p_Result_69_fu_9676_p3.read());
}

void mnist_fp16_opt::thread_num_zeros_2_fu_10763_p3() {
    num_zeros_2_fu_10763_p3 = esl_cttz<32,32>(p_Result_15_fu_10755_p3.read());
}

void mnist_fp16_opt::thread_num_zeros_3_fu_14458_p3() {
    num_zeros_3_fu_14458_p3 = esl_cttz<32,32>(p_Result_72_fu_14450_p3.read());
}

void mnist_fp16_opt::thread_num_zeros_4_fu_15654_p3() {
    num_zeros_4_fu_15654_p3 = esl_cttz<32,32>(p_Result_77_fu_15646_p3.read());
}

void mnist_fp16_opt::thread_num_zeros_5_fu_16718_p3() {
    num_zeros_5_fu_16718_p3 = esl_cttz<32,32>(p_Result_34_fu_16710_p3.read());
}

void mnist_fp16_opt::thread_num_zeros_6_fu_19553_p3() {
    num_zeros_6_fu_19553_p3 = esl_cttz<32,32>(p_Result_80_fu_19545_p3.read());
}

void mnist_fp16_opt::thread_num_zeros_7_fu_20786_p3() {
    num_zeros_7_fu_20786_p3 = esl_cttz<32,32>(p_Result_85_fu_20778_p3.read());
}

void mnist_fp16_opt::thread_num_zeros_8_fu_21628_p3() {
    num_zeros_8_fu_21628_p3 = esl_cttz<32,32>(p_Result_56_fu_21620_p3.read());
}

void mnist_fp16_opt::thread_num_zeros_9_fu_22283_p3() {
    num_zeros_9_fu_22283_p3 = esl_cttz<32,32>(p_Result_60_fu_22275_p3.read());
}

void mnist_fp16_opt::thread_num_zeros_fu_8424_p3() {
    num_zeros_fu_8424_p3 = esl_cttz<32,32>(p_Result_64_fu_8416_p3.read());
}

void mnist_fp16_opt::thread_or_cond10_fu_9568_p2() {
    or_cond10_fu_9568_p2 = (sel_tmp32_fu_9519_p2.read() | sel_tmp28_fu_9493_p2.read());
}

void mnist_fp16_opt::thread_or_cond11_fu_9582_p2() {
    or_cond11_fu_9582_p2 = (or_cond9_fu_9555_p2.read() | or_cond10_fu_9568_p2.read());
}

void mnist_fp16_opt::thread_or_cond12_fu_13763_p2() {
    or_cond12_fu_13763_p2 = (sel_tmp44_reg_25661.read() | sel_tmp42_fu_13751_p2.read());
}

void mnist_fp16_opt::thread_or_cond13_fu_13775_p2() {
    or_cond13_fu_13775_p2 = (sel_tmp41_fu_13746_p2.read() | sel_tmp37_fu_13735_p2.read());
}

void mnist_fp16_opt::thread_or_cond14_fu_13789_p2() {
    or_cond14_fu_13789_p2 = (or_cond12_fu_13763_p2.read() | or_cond13_fu_13775_p2.read());
}

void mnist_fp16_opt::thread_or_cond15_fu_16296_p2() {
    or_cond15_fu_16296_p2 = (sel_tmp53_reg_26305.read() | sel_tmp51_fu_16284_p2.read());
}

void mnist_fp16_opt::thread_or_cond16_fu_16308_p2() {
    or_cond16_fu_16308_p2 = (sel_tmp50_fu_16279_p2.read() | sel_tmp46_fu_16268_p2.read());
}

void mnist_fp16_opt::thread_or_cond17_fu_16322_p2() {
    or_cond17_fu_16322_p2 = (or_cond15_fu_16296_p2.read() | or_cond16_fu_16308_p2.read());
}

void mnist_fp16_opt::thread_or_cond18_fu_15411_p2() {
    or_cond18_fu_15411_p2 = (sel_tmp62_fu_15398_p2.read() | sel_tmp60_fu_15381_p2.read());
}

void mnist_fp16_opt::thread_or_cond19_fu_15424_p2() {
    or_cond19_fu_15424_p2 = (sel_tmp59_fu_15375_p2.read() | sel_tmp55_fu_15349_p2.read());
}

void mnist_fp16_opt::thread_or_cond1_58_fu_14127_p2() {
    or_cond1_58_fu_14127_p2 = (tmp358_fu_14121_p2.read() & tmp357_mid2_reg_25717.read());
}

void mnist_fp16_opt::thread_or_cond1_fu_7701_p2() {
    or_cond1_fu_7701_p2 = (sel_tmp9_fu_7672_p2.read() | sel_tmp2_fu_7661_p2.read());
}

void mnist_fp16_opt::thread_or_cond20_fu_15438_p2() {
    or_cond20_fu_15438_p2 = (or_cond18_fu_15411_p2.read() | or_cond19_fu_15424_p2.read());
}

void mnist_fp16_opt::thread_or_cond21_fu_15526_p2() {
    or_cond21_fu_15526_p2 = (sel_tmp71_reg_26092.read() | sel_tmp69_fu_15514_p2.read());
}

void mnist_fp16_opt::thread_or_cond22_fu_15538_p2() {
    or_cond22_fu_15538_p2 = (sel_tmp68_fu_15509_p2.read() | sel_tmp64_fu_15498_p2.read());
}

void mnist_fp16_opt::thread_or_cond23_fu_15552_p2() {
    or_cond23_fu_15552_p2 = (or_cond21_fu_15526_p2.read() | or_cond22_fu_15538_p2.read());
}

void mnist_fp16_opt::thread_or_cond24_fu_18909_p2() {
    or_cond24_fu_18909_p2 = (sel_tmp80_reg_27070.read() | sel_tmp78_fu_18897_p2.read());
}

void mnist_fp16_opt::thread_or_cond25_fu_18921_p2() {
    or_cond25_fu_18921_p2 = (sel_tmp77_fu_18892_p2.read() | sel_tmp73_fu_18881_p2.read());
}

void mnist_fp16_opt::thread_or_cond26_fu_18935_p2() {
    or_cond26_fu_18935_p2 = (or_cond24_fu_18909_p2.read() | or_cond25_fu_18921_p2.read());
}

void mnist_fp16_opt::thread_or_cond27_fu_22128_p2() {
    or_cond27_fu_22128_p2 = (sel_tmp89_reg_27897.read() | sel_tmp87_fu_22116_p2.read());
}

void mnist_fp16_opt::thread_or_cond28_fu_22140_p2() {
    or_cond28_fu_22140_p2 = (sel_tmp86_fu_22111_p2.read() | sel_tmp82_fu_22100_p2.read());
}

void mnist_fp16_opt::thread_or_cond29_fu_22154_p2() {
    or_cond29_fu_22154_p2 = (or_cond27_fu_22128_p2.read() | or_cond28_fu_22140_p2.read());
}

void mnist_fp16_opt::thread_or_cond2_fu_7715_p2() {
    or_cond2_fu_7715_p2 = (or_cond_fu_7689_p2.read() | or_cond1_fu_7701_p2.read());
}

void mnist_fp16_opt::thread_or_cond30_fu_21412_p2() {
    or_cond30_fu_21412_p2 = (sel_tmp98_reg_27700.read() | sel_tmp96_fu_21400_p2.read());
}

void mnist_fp16_opt::thread_or_cond31_fu_21424_p2() {
    or_cond31_fu_21424_p2 = (sel_tmp95_fu_21395_p2.read() | sel_tmp91_fu_21384_p2.read());
}

void mnist_fp16_opt::thread_or_cond32_fu_21438_p2() {
    or_cond32_fu_21438_p2 = (or_cond30_fu_21412_p2.read() | or_cond31_fu_21424_p2.read());
}

void mnist_fp16_opt::thread_or_cond33_fu_20509_p2() {
    or_cond33_fu_20509_p2 = (sel_tmp107_reg_27453.read() | sel_tmp105_fu_20497_p2.read());
}

void mnist_fp16_opt::thread_or_cond34_fu_20521_p2() {
    or_cond34_fu_20521_p2 = (sel_tmp104_fu_20492_p2.read() | sel_tmp100_fu_20481_p2.read());
}

void mnist_fp16_opt::thread_or_cond35_fu_20535_p2() {
    or_cond35_fu_20535_p2 = (or_cond33_fu_20509_p2.read() | or_cond34_fu_20521_p2.read());
}

void mnist_fp16_opt::thread_or_cond36_fu_20657_p2() {
    or_cond36_fu_20657_p2 = (sel_tmp116_fu_20644_p2.read() | sel_tmp114_fu_20627_p2.read());
}

void mnist_fp16_opt::thread_or_cond37_fu_20670_p2() {
    or_cond37_fu_20670_p2 = (sel_tmp113_fu_20621_p2.read() | sel_tmp109_fu_20595_p2.read());
}

void mnist_fp16_opt::thread_or_cond38_fu_20684_p2() {
    or_cond38_fu_20684_p2 = (or_cond36_fu_20657_p2.read() | or_cond37_fu_20670_p2.read());
}

void mnist_fp16_opt::thread_or_cond3_fu_10326_p2() {
    or_cond3_fu_10326_p2 = (sel_tmp17_reg_24705.read() | sel_tmp15_fu_10314_p2.read());
}

void mnist_fp16_opt::thread_or_cond4_fu_10338_p2() {
    or_cond4_fu_10338_p2 = (sel_tmp14_fu_10309_p2.read() | sel_tmp10_fu_10298_p2.read());
}

void mnist_fp16_opt::thread_or_cond5_fu_10352_p2() {
    or_cond5_fu_10352_p2 = (or_cond3_fu_10326_p2.read() | or_cond4_fu_10338_p2.read());
}

void mnist_fp16_opt::thread_or_cond6_fu_9406_p2() {
    or_cond6_fu_9406_p2 = (sel_tmp26_fu_9393_p2.read() | sel_tmp24_fu_9376_p2.read());
}

void mnist_fp16_opt::thread_or_cond7_69_fu_19176_p2() {
    or_cond7_69_fu_19176_p2 = (tmp_624_fu_19170_p2.read() & tmp_623_mid2_fu_19148_p3.read());
}

void mnist_fp16_opt::thread_or_cond7_fu_9419_p2() {
    or_cond7_fu_9419_p2 = (sel_tmp23_fu_9370_p2.read() | sel_tmp19_fu_9344_p2.read());
}

void mnist_fp16_opt::thread_or_cond8_1_fu_11173_p2() {
    or_cond8_1_fu_11173_p2 = (tmp_75_fu_11160_p2.read() & tmp_76_fu_11165_p2.read());
}

void mnist_fp16_opt::thread_or_cond8_fu_9433_p2() {
    or_cond8_fu_9433_p2 = (or_cond6_fu_9406_p2.read() | or_cond7_fu_9419_p2.read());
}

void mnist_fp16_opt::thread_or_cond9_fu_9555_p2() {
    or_cond9_fu_9555_p2 = (sel_tmp35_fu_9542_p2.read() | sel_tmp33_fu_9525_p2.read());
}

void mnist_fp16_opt::thread_or_cond_fu_7689_p2() {
    or_cond_fu_7689_p2 = (sel_tmp4_reg_24037.read() | sel_tmp_fu_7677_p2.read());
}

void mnist_fp16_opt::thread_p_012_0_i2_fu_10853_p3() {
    p_012_0_i2_fu_10853_p3 = (!icmp6_fu_10809_p2.read()[0].is_01())? sc_lv<32>(): ((icmp6_fu_10809_p2.read()[0].to_bool())? tmp32_V_3_fu_10824_p2.read(): tmp32_V_4_fu_10849_p1.read());
}

void mnist_fp16_opt::thread_p_012_0_i5_fu_16808_p3() {
    p_012_0_i5_fu_16808_p3 = (!icmp13_fu_16764_p2.read()[0].is_01())? sc_lv<32>(): ((icmp13_fu_16764_p2.read()[0].to_bool())? tmp32_V_18_fu_16779_p2.read(): tmp32_V_19_fu_16804_p1.read());
}

void mnist_fp16_opt::thread_p_012_0_i8_fu_21718_p3() {
    p_012_0_i8_fu_21718_p3 = (!icmp21_fu_21674_p2.read()[0].is_01())? sc_lv<32>(): ((icmp21_fu_21674_p2.read()[0].to_bool())? tmp32_V_30_fu_21689_p2.read(): tmp32_V_31_fu_21714_p1.read());
}

void mnist_fp16_opt::thread_p_012_0_i9_fu_22373_p3() {
    p_012_0_i9_fu_22373_p3 = (!icmp22_fu_22329_p2.read()[0].is_01())? sc_lv<32>(): ((icmp22_fu_22329_p2.read()[0].to_bool())? tmp32_V_34_fu_22344_p2.read(): tmp32_V_35_fu_22369_p1.read());
}

void mnist_fp16_opt::thread_p_03_i1_fu_10919_p3() {
    p_03_i1_fu_10919_p3 = (!tmp_321_reg_24834_pp9_iter10_reg.read()[0].is_01())? sc_lv<32>(): ((tmp_321_reg_24834_pp9_iter10_reg.read()[0].to_bool())? ap_const_lv32_0: f_4_fu_10915_p1.read());
}

void mnist_fp16_opt::thread_p_03_i1_to_int_fu_10926_p1() {
    p_03_i1_to_int_fu_10926_p1 = p_03_i1_reg_24875.read();
}

void mnist_fp16_opt::thread_p_03_i3_fu_16874_p3() {
    p_03_i3_fu_16874_p3 = (!tmp_591_reg_26429_pp16_iter10_reg.read()[0].is_01())? sc_lv<32>(): ((tmp_591_reg_26429_pp16_iter10_reg.read()[0].to_bool())? ap_const_lv32_0: f_8_fu_16870_p1.read());
}

void mnist_fp16_opt::thread_p_03_i3_to_int_fu_16881_p1() {
    p_03_i3_to_int_fu_16881_p1 = p_03_i3_reg_26470.read();
}

void mnist_fp16_opt::thread_p_03_i5_fu_21784_p3() {
    p_03_i5_fu_21784_p3 = (!tmp_467_reg_27771_pp23_iter10_reg.read()[0].is_01())? sc_lv<32>(): ((tmp_467_reg_27771_pp23_iter10_reg.read()[0].to_bool())? ap_const_lv32_0: f_14_fu_21780_p1.read());
}

void mnist_fp16_opt::thread_p_03_i5_to_int_fu_21791_p1() {
    p_03_i5_to_int_fu_21791_p1 = p_03_i5_reg_27812.read();
}

void mnist_fp16_opt::thread_p_03_i6_fu_22443_p3() {
    p_03_i6_fu_22443_p3 = (!tmp_659_reg_27957.read()[0].is_01())? sc_lv<32>(): ((tmp_659_reg_27957.read()[0].to_bool())? ap_const_lv32_0: f_16_fu_22439_p1.read());
}

void mnist_fp16_opt::thread_p_10_mid_fu_8603_p3() {
    p_10_mid_fu_8603_p3 = (!exitcond_flatten8_fu_8597_p2.read()[0].is_01())? sc_lv<5>(): ((exitcond_flatten8_fu_8597_p2.read()[0].to_bool())? ap_const_lv5_0: p_10_reg_2731.read());
}

void mnist_fp16_opt::thread_p_11_mid_fu_7245_p3() {
    p_11_mid_fu_7245_p3 = (!exitcond_flatten4_fu_7239_p2.read()[0].is_01())? sc_lv<5>(): ((exitcond_flatten4_fu_7239_p2.read()[0].to_bool())? ap_const_lv5_0: ap_phi_mux_p_11_phi_fu_2605_p4.read());
}

void mnist_fp16_opt::thread_p_12_mid2_fu_7850_p3() {
    p_12_mid2_fu_7850_p3 = (!tmp_282_fu_7844_p2.read()[0].is_01())? sc_lv<5>(): ((tmp_282_fu_7844_p2.read()[0].to_bool())? ap_const_lv5_0: p_12_reg_2667.read());
}

void mnist_fp16_opt::thread_p_13_mid2_fu_7075_p3() {
    p_13_mid2_fu_7075_p3 = (!exitcond5_fu_7069_p2.read()[0].is_01())? sc_lv<2>(): ((exitcond5_fu_7069_p2.read()[0].to_bool())? ap_const_lv2_0: ap_phi_mux_p_13_phi_fu_2548_p4.read());
}

void mnist_fp16_opt::thread_p_14_mid2_fu_8641_p3() {
    p_14_mid2_fu_8641_p3 = (!tmp_392_fu_8635_p2.read()[0].is_01())? sc_lv<5>(): ((tmp_392_fu_8635_p2.read()[0].to_bool())? ap_const_lv5_0: p_14_reg_2742.read());
}

void mnist_fp16_opt::thread_p_16_mid2_fu_7325_p3() {
    p_16_mid2_fu_7325_p3 = (!tmp_194_fu_7319_p2.read()[0].is_01())? sc_lv<5>(): ((tmp_194_fu_7319_p2.read()[0].to_bool())? ap_const_lv5_0: p_16_reg_2612.read());
}

void mnist_fp16_opt::thread_p_20_mid2_fu_11058_p3() {
    p_20_mid2_fu_11058_p3 = (!exitcond11_fu_11052_p2.read()[0].is_01())? sc_lv<5>(): ((exitcond11_fu_11052_p2.read()[0].to_bool())? ap_const_lv5_0: ap_phi_mux_p_20_phi_fu_3005_p4.read());
}

void mnist_fp16_opt::thread_p_21_mid_fu_8780_p3() {
    p_21_mid_fu_8780_p3 = (!exitcond_flatten12_fu_8774_p2.read()[0].is_01())? sc_lv<2>(): ((exitcond_flatten12_fu_8774_p2.read()[0].to_bool())? ap_const_lv2_0: ap_phi_mux_p_21_phi_fu_2790_p4.read());
}

void mnist_fp16_opt::thread_p_23_mid_fu_9882_p3() {
    p_23_mid_fu_9882_p3 = (!exitcond_flatten11_fu_9876_p2.read()[0].is_01())? sc_lv<5>(): ((exitcond_flatten11_fu_9876_p2.read()[0].to_bool())? ap_const_lv5_0: ap_phi_mux_p_23_phi_fu_2857_p4.read());
}

void mnist_fp16_opt::thread_p_24_mid_fu_12882_p3() {
    p_24_mid_fu_12882_p3 = (!exitcond_flatten18_fu_12876_p2.read()[0].is_01())? sc_lv<4>(): ((exitcond_flatten18_fu_12876_p2.read()[0].to_bool())? ap_const_lv4_0: p_24_reg_3134.read());
}

void mnist_fp16_opt::thread_p_25_mid2_fu_8891_p3() {
    p_25_mid2_fu_8891_p3 = (!tmp_672_fu_8885_p2.read()[0].is_01())? sc_lv<2>(): ((tmp_672_fu_8885_p2.read()[0].to_bool())? ap_const_lv2_0: p_25_reg_2809.read());
}

void mnist_fp16_opt::thread_p_27_mid_fu_10391_p3() {
    p_27_mid_fu_10391_p3 = (!exitcond_flatten14_fu_10385_p2.read()[0].is_01())? sc_lv<4>(): ((exitcond_flatten14_fu_10385_p2.read()[0].to_bool())? ap_const_lv4_0: p_27_reg_2909.read());
}

void mnist_fp16_opt::thread_p_28_mid2_fu_9962_p3() {
    p_28_mid2_fu_9962_p3 = (!tmp_571_fu_9956_p2.read()[0].is_01())? sc_lv<5>(): ((tmp_571_fu_9956_p2.read()[0].to_bool())? ap_const_lv5_0: p_28_reg_2864.read());
}

void mnist_fp16_opt::thread_p_29_mid2_fu_12974_p3() {
    p_29_mid2_fu_12974_p3 = (!tmp_823_fu_12968_p2.read()[0].is_01())? sc_lv<4>(): ((tmp_823_fu_12968_p2.read()[0].to_bool())? ap_const_lv4_0: p_29_reg_3145.read());
}

void mnist_fp16_opt::thread_p_31_mid_fu_13898_p3() {
    p_31_mid_fu_13898_p3 = (!exitcond_flatten24_fu_13892_p2.read()[0].is_01())? sc_lv<5>(): ((exitcond_flatten24_fu_13892_p2.read()[0].to_bool())? ap_const_lv5_0: ap_phi_mux_p_31_phi_fu_3316_p4.read());
}

void mnist_fp16_opt::thread_p_33_mid2_fu_10512_p3() {
    p_33_mid2_fu_10512_p3 = (!tmp_720_fu_10507_p2.read()[0].is_01())? sc_lv<4>(): ((tmp_720_fu_10507_p2.read()[0].to_bool())? ap_const_lv4_0: p_33_reg_2921.read());
}

void mnist_fp16_opt::thread_p_34_mid_fu_14643_p3() {
    p_34_mid_fu_14643_p3 = (!exitcond_flatten26_fu_14637_p2.read()[0].is_01())? sc_lv<4>(): ((exitcond_flatten26_fu_14637_p2.read()[0].to_bool())? ap_const_lv4_0: p_34_reg_3383.read());
}

void mnist_fp16_opt::thread_p_35_mid2_fu_13968_p3() {
    p_35_mid2_fu_13968_p3 = (!tmp_920_fu_13962_p2.read()[0].is_01())? sc_lv<5>(): ((tmp_920_fu_13962_p2.read()[0].to_bool())? ap_const_lv5_0: p_35_reg_3323.read());
}

void mnist_fp16_opt::thread_p_37_mid_fu_13063_p3() {
    p_37_mid_fu_13063_p3 = (!exitcond_flatten22_fu_13057_p2.read()[0].is_01())? sc_lv<2>(): ((exitcond_flatten22_fu_13057_p2.read()[0].to_bool())? ap_const_lv2_0: ap_phi_mux_p_37_phi_fu_3193_p4.read());
}

void mnist_fp16_opt::thread_p_39_mid2_fu_14735_p3() {
    p_39_mid2_fu_14735_p3 = (!tmp_976_fu_14729_p2.read()[0].is_01())? sc_lv<4>(): ((tmp_976_fu_14729_p2.read()[0].to_bool())? ap_const_lv4_0: p_39_reg_3394.read());
}

void mnist_fp16_opt::thread_p_3_mid_fu_7804_p3() {
    p_3_mid_fu_7804_p3 = (!exitcond_flatten6_fu_7798_p2.read()[0].is_01())? sc_lv<5>(): ((exitcond_flatten6_fu_7798_p2.read()[0].to_bool())? ap_const_lv5_0: ap_phi_mux_p_3_phi_fu_2660_p4.read());
}

void mnist_fp16_opt::thread_p_40_mid2_fu_13147_p3() {
    p_40_mid2_fu_13147_p3 = (!tmp_883_fu_13141_p2.read()[0].is_01())? sc_lv<2>(): ((tmp_883_fu_13141_p2.read()[0].to_bool())? ap_const_lv2_0: ap_phi_mux_p_40_phi_fu_3204_p4.read());
}

void mnist_fp16_opt::thread_p_41_mid_fu_13319_p3() {
    p_41_mid_fu_13319_p3 = (!exitcond_flatten21_fu_13313_p2.read()[0].is_01())? sc_lv<4>(): ((exitcond_flatten21_fu_13313_p2.read()[0].to_bool())? ap_const_lv4_0: ap_phi_mux_p_41_phi_fu_3261_p4.read());
}

void mnist_fp16_opt::thread_p_43_mid2_fu_10630_p3() {
    p_43_mid2_fu_10630_p3 = (!exitcond12_fu_10624_p2.read()[0].is_01())? sc_lv<2>(): ((exitcond12_fu_10624_p2.read()[0].to_bool())? ap_const_lv2_0: p_43_reg_2968.read());
}

void mnist_fp16_opt::thread_p_46_mid2_fu_17013_p3() {
    p_46_mid2_fu_17013_p3 = (!exitcond21_fu_17007_p2.read()[0].is_01())? sc_lv<4>(): ((exitcond21_fu_17007_p2.read()[0].to_bool())? ap_const_lv4_0: ap_phi_mux_p_46_phi_fu_3657_p4.read());
}

void mnist_fp16_opt::thread_p_47_mid2_fu_13399_p3() {
    p_47_mid2_fu_13399_p3 = (!tmp_872_fu_13393_p2.read()[0].is_01())? sc_lv<4>(): ((tmp_872_fu_13393_p2.read()[0].to_bool())? ap_const_lv4_0: p_47_reg_3268.read());
}

void mnist_fp16_opt::thread_p_48_mid_fu_14824_p3() {
    p_48_mid_fu_14824_p3 = (!exitcond_flatten30_fu_14818_p2.read()[0].is_01())? sc_lv<2>(): ((exitcond_flatten30_fu_14818_p2.read()[0].to_bool())? ap_const_lv2_0: ap_phi_mux_p_48_phi_fu_3442_p4.read());
}

void mnist_fp16_opt::thread_p_49_mid_fu_18019_p3() {
    p_49_mid_fu_18019_p3 = (!exitcond_flatten36_fu_18013_p2.read()[0].is_01())? sc_lv<3>(): ((exitcond_flatten36_fu_18013_p2.read()[0].to_bool())? ap_const_lv3_0: p_49_reg_3749.read());
}

void mnist_fp16_opt::thread_p_51_mid2_fu_14908_p3() {
    p_51_mid2_fu_14908_p3 = (!tmp_1021_fu_14902_p2.read()[0].is_01())? sc_lv<2>(): ((tmp_1021_fu_14902_p2.read()[0].to_bool())? ap_const_lv2_0: p_51_reg_3461.read());
}

void mnist_fp16_opt::thread_p_53_mid2_fu_18091_p3() {
    p_53_mid2_fu_18091_p3 = (!tmp_1127_fu_18085_p2.read()[0].is_01())? sc_lv<3>(): ((tmp_1127_fu_18085_p2.read()[0].to_bool())? ap_const_lv3_0: p_53_reg_3760.read());
}

void mnist_fp16_opt::thread_p_55_mid_fu_15852_p3() {
    p_55_mid_fu_15852_p3 = (!exitcond_flatten29_fu_15846_p2.read()[0].is_01())? sc_lv<4>(): ((exitcond_flatten29_fu_15846_p2.read()[0].to_bool())? ap_const_lv4_0: ap_phi_mux_p_55_phi_fu_3509_p4.read());
}

void mnist_fp16_opt::thread_p_57_mid_fu_19022_p3() {
    p_57_mid_fu_19022_p3 = (!exitcond_flatten42_fu_19016_p2.read()[0].is_01())? sc_lv<4>(): ((exitcond_flatten42_fu_19016_p2.read()[0].to_bool())? ap_const_lv4_0: ap_phi_mux_p_57_phi_fu_3931_p4.read());
}

void mnist_fp16_opt::thread_p_59_mid_fu_16361_p3() {
    p_59_mid_fu_16361_p3 = (!exitcond_flatten32_fu_16355_p2.read()[0].is_01())? sc_lv<3>(): ((exitcond_flatten32_fu_16355_p2.read()[0].to_bool())? ap_const_lv3_0: p_59_reg_3561.read());
}

void mnist_fp16_opt::thread_p_5_mid_fu_6893_p3() {
    p_5_mid_fu_6893_p3 = (!exitcond_flatten2_fu_6887_p2.read()[0].is_01())? sc_lv<5>(): ((exitcond_flatten2_fu_6887_p2.read()[0].to_bool())? ap_const_lv5_0: p_5_reg_2500.read());
}

void mnist_fp16_opt::thread_p_60_mid2_fu_15932_p3() {
    p_60_mid2_fu_15932_p3 = (!tmp_1011_fu_15926_p2.read()[0].is_01())? sc_lv<4>(): ((tmp_1011_fu_15926_p2.read()[0].to_bool())? ap_const_lv4_0: p_60_reg_3516.read());
}

void mnist_fp16_opt::thread_p_61_mid_fu_19738_p3() {
    p_61_mid_fu_19738_p3 = (!exitcond_flatten44_fu_19732_p2.read()[0].is_01())? sc_lv<3>(): ((exitcond_flatten44_fu_19732_p2.read()[0].to_bool())? ap_const_lv3_0: p_61_reg_3998.read());
}

void mnist_fp16_opt::thread_p_62_mid2_fu_19100_p3() {
    p_62_mid2_fu_19100_p3 = (!tmp_1186_fu_19094_p2.read()[0].is_01())? sc_lv<4>(): ((tmp_1186_fu_19094_p2.read()[0].to_bool())? ap_const_lv4_0: p_62_reg_3938.read());
}

void mnist_fp16_opt::thread_p_63_mid_fu_18180_p3() {
    p_63_mid_fu_18180_p3 = (!exitcond_flatten40_fu_18174_p2.read()[0].is_01())? sc_lv<2>(): ((exitcond_flatten40_fu_18174_p2.read()[0].to_bool())? ap_const_lv2_0: ap_phi_mux_p_63_phi_fu_3808_p4.read());
}

void mnist_fp16_opt::thread_p_64_mid2_fu_19822_p3() {
    p_64_mid2_fu_19822_p3 = (!tmp_1224_fu_19816_p2.read()[0].is_01())? sc_lv<3>(): ((tmp_1224_fu_19816_p2.read()[0].to_bool())? ap_const_lv3_0: p_64_reg_4009.read());
}

void mnist_fp16_opt::thread_p_65_mid2_fu_16475_p3() {
    p_65_mid2_fu_16475_p3 = (!tmp_1059_fu_16470_p2.read()[0].is_01())? sc_lv<3>(): ((tmp_1059_fu_16470_p2.read()[0].to_bool())? ap_const_lv3_0: p_65_reg_3573.read());
}

void mnist_fp16_opt::thread_p_66_mid2_fu_18235_p3() {
    p_66_mid2_fu_18235_p3 = (!tmp_1159_fu_18229_p2.read()[0].is_01())? sc_lv<2>(): ((tmp_1159_fu_18229_p2.read()[0].to_bool())? ap_const_lv2_0: ap_phi_mux_p_66_phi_fu_3819_p4.read());
}

void mnist_fp16_opt::thread_p_69_mid_fu_19911_p3() {
    p_69_mid_fu_19911_p3 = (!exitcond_flatten48_fu_19905_p2.read()[0].is_01())? sc_lv<2>(): ((exitcond_flatten48_fu_19905_p2.read()[0].to_bool())? ap_const_lv2_0: ap_phi_mux_p_69_phi_fu_4057_p4.read());
}

void mnist_fp16_opt::thread_p_72_mid2_fu_19966_p3() {
    p_72_mid2_fu_19966_p3 = (!tmp_1242_fu_19960_p2.read()[0].is_01())? sc_lv<2>(): ((tmp_1242_fu_19960_p2.read()[0].to_bool())? ap_const_lv2_0: p_72_reg_4076.read());
}

void mnist_fp16_opt::thread_p_73_mid_fu_18481_p3() {
    p_73_mid_fu_18481_p3 = (!exitcond_flatten39_fu_18475_p2.read()[0].is_01())? sc_lv<3>(): ((exitcond_flatten39_fu_18475_p2.read()[0].to_bool())? ap_const_lv3_0: ap_phi_mux_p_73_phi_fu_3876_p4.read());
}

void mnist_fp16_opt::thread_p_75_mid2_fu_16585_p3() {
    p_75_mid2_fu_16585_p3 = (!exitcond22_fu_16579_p2.read()[0].is_01())? sc_lv<2>(): ((exitcond22_fu_16579_p2.read()[0].to_bool())? ap_const_lv2_0: p_75_reg_3620.read());
}

void mnist_fp16_opt::thread_p_78_mid2_fu_18553_p3() {
    p_78_mid2_fu_18553_p3 = (!tmp_1149_fu_18547_p2.read()[0].is_01())? sc_lv<3>(): ((tmp_1149_fu_18547_p2.read()[0].to_bool())? ap_const_lv3_0: p_78_reg_3883.read());
}

void mnist_fp16_opt::thread_p_81_mid_fu_20984_p3() {
    p_81_mid_fu_20984_p3 = (!exitcond_flatten47_fu_20978_p2.read()[0].is_01())? sc_lv<3>(): ((exitcond_flatten47_fu_20978_p2.read()[0].to_bool())? ap_const_lv3_0: ap_phi_mux_p_81_phi_fu_4124_p4.read());
}

void mnist_fp16_opt::thread_p_84_mid2_fu_21056_p3() {
    p_84_mid2_fu_21056_p3 = (!tmp_1231_fu_21050_p2.read()[0].is_01())? sc_lv<3>(): ((tmp_1231_fu_21050_p2.read()[0].to_bool())? ap_const_lv3_0: p_84_reg_4131.read());
}

void mnist_fp16_opt::thread_p_86_mid2_fu_21519_p3() {
    p_86_mid2_fu_21519_p3 = (!exitcond32_fu_21513_p2.read()[0].is_01())? sc_lv<3>(): ((exitcond32_fu_21513_p2.read()[0].to_bool())? ap_const_lv3_0: p_86_reg_4188.read());
}

void mnist_fp16_opt::thread_p_8_mid2_fu_6931_p3() {
    p_8_mid2_fu_6931_p3 = (!tmp_136_fu_6925_p2.read()[0].is_01())? sc_lv<5>(): ((tmp_136_fu_6925_p2.read()[0].to_bool())? ap_const_lv5_0: p_8_reg_2511.read());
}

void mnist_fp16_opt::thread_p_Repl2_10_trunc_fu_14586_p2() {
    p_Repl2_10_trunc_fu_14586_p2 = (!tmp362_cast_cast_fu_14579_p3.read().is_01() || !tmp_1197_fu_14576_p1.read().is_01())? sc_lv<8>(): (sc_biguint<8>(tmp362_cast_cast_fu_14579_p3.read()) + sc_biguint<8>(tmp_1197_fu_14576_p1.read()));
}

void mnist_fp16_opt::thread_p_Repl2_13_trunc_fu_15788_p2() {
    p_Repl2_13_trunc_fu_15788_p2 = (!tmp_1288_fu_15778_p1.read().is_01() || !tmp363_cast_cast_fu_15781_p3.read().is_01())? sc_lv<8>(): (sc_biguint<8>(tmp_1288_fu_15778_p1.read()) + sc_biguint<8>(tmp363_cast_cast_fu_15781_p3.read()));
}

void mnist_fp16_opt::thread_p_Repl2_16_trunc_fu_16846_p2() {
    p_Repl2_16_trunc_fu_16846_p2 = (!tmp_1361_fu_16836_p1.read().is_01() || !tmp364_cast_cast_fu_16839_p3.read().is_01())? sc_lv<8>(): (sc_biguint<8>(tmp_1361_fu_16836_p1.read()) + sc_biguint<8>(tmp364_cast_cast_fu_16839_p3.read()));
}

void mnist_fp16_opt::thread_p_Repl2_19_trunc_fu_19681_p2() {
    p_Repl2_19_trunc_fu_19681_p2 = (!tmp382_cast_cast_fu_19674_p3.read().is_01() || !tmp_1444_fu_19671_p1.read().is_01())? sc_lv<8>(): (sc_biguint<8>(tmp382_cast_cast_fu_19674_p3.read()) + sc_biguint<8>(tmp_1444_fu_19671_p1.read()));
}

void mnist_fp16_opt::thread_p_Repl2_1_trunc_fu_8552_p2() {
    p_Repl2_1_trunc_fu_8552_p2 = (!tmp347_cast_cast_fu_8545_p3.read().is_01() || !tmp_522_fu_8542_p1.read().is_01())? sc_lv<8>(): (sc_biguint<8>(tmp347_cast_cast_fu_8545_p3.read()) + sc_biguint<8>(tmp_522_fu_8542_p1.read()));
}

void mnist_fp16_opt::thread_p_Repl2_22_trunc_fu_20920_p2() {
    p_Repl2_22_trunc_fu_20920_p2 = (!tmp_1462_fu_20910_p1.read().is_01() || !tmp383_cast_cast_fu_20913_p3.read().is_01())? sc_lv<8>(): (sc_biguint<8>(tmp_1462_fu_20910_p1.read()) + sc_biguint<8>(tmp383_cast_cast_fu_20913_p3.read()));
}

void mnist_fp16_opt::thread_p_Repl2_26_trunc_fu_21756_p2() {
    p_Repl2_26_trunc_fu_21756_p2 = (!tmp384_cast_cast_fu_21749_p3.read().is_01() || !tmp_1498_fu_21746_p1.read().is_01())? sc_lv<8>(): (sc_biguint<8>(tmp384_cast_cast_fu_21749_p3.read()) + sc_biguint<8>(tmp_1498_fu_21746_p1.read()));
}

void mnist_fp16_opt::thread_p_Repl2_28_trunc_fu_22415_p2() {
    p_Repl2_28_trunc_fu_22415_p2 = (!tmp385_cast_cast_fu_22408_p3.read().is_01() || !tmp_1511_fu_22405_p1.read().is_01())? sc_lv<8>(): (sc_biguint<8>(tmp385_cast_cast_fu_22408_p3.read()) + sc_biguint<8>(tmp_1511_fu_22405_p1.read()));
}

void mnist_fp16_opt::thread_p_Repl2_4_trunc_fu_9818_p2() {
    p_Repl2_4_trunc_fu_9818_p2 = (!tmp_641_fu_9808_p1.read().is_01() || !tmp348_cast_cast_fu_9811_p3.read().is_01())? sc_lv<8>(): (sc_biguint<8>(tmp_641_fu_9808_p1.read()) + sc_biguint<8>(tmp348_cast_cast_fu_9811_p3.read()));
}

void mnist_fp16_opt::thread_p_Repl2_7_trunc_fu_10891_p2() {
    p_Repl2_7_trunc_fu_10891_p2 = (!tmp_776_fu_10881_p1.read().is_01() || !tmp349_cast_cast_fu_10884_p3.read().is_01())? sc_lv<8>(): (sc_biguint<8>(tmp_776_fu_10881_p1.read()) + sc_biguint<8>(tmp349_cast_cast_fu_10884_p3.read()));
}

void mnist_fp16_opt::thread_p_Result_14_fu_10745_p4() {
    p_Result_14_fu_10745_p4 = tmp_V_2_fu_10740_p3.read().range(0, 15);
}

void mnist_fp16_opt::thread_p_Result_15_fu_10755_p3() {
    p_Result_15_fu_10755_p3 = esl_concat<16,16>(ap_const_lv16_FFFF, p_Result_14_fu_10745_p4.read());
}

void mnist_fp16_opt::thread_p_Result_16_fu_10865_p4() {
    p_Result_16_fu_10865_p4 = tmp32_V_8_fu_10861_p1.read().range(30, 23);
}

void mnist_fp16_opt::thread_p_Result_17_fu_10904_p5() {
    p_Result_17_fu_10904_p5 = esl_partset<32,32,9,32,32>(tmp32_V_8_reg_24865.read(), tmp_233_fu_10897_p3.read(), ap_const_lv32_17, ap_const_lv32_1F);
}

void mnist_fp16_opt::thread_p_Result_19_fu_13579_p1() {
    p_Result_19_fu_13579_p1 = esl_zext<54,53>(tmp_209_fu_13572_p3.read());
}

void mnist_fp16_opt::thread_p_Result_21_fu_14539_p2() {
    p_Result_21_fu_14539_p2 = (!tmp_1181_fu_14535_p1.read().is_01())? sc_lv<16>(): p_Val2_31_reg_25835.read() >> (unsigned short)tmp_1181_fu_14535_p1.read().to_uint();
}

void mnist_fp16_opt::thread_p_Result_22_fu_14560_p4() {
    p_Result_22_fu_14560_p4 = tmp32_V_53_fu_14556_p1.read().range(30, 23);
}

void mnist_fp16_opt::thread_p_Result_25_fu_16112_p1() {
    p_Result_25_fu_16112_p1 = esl_zext<54,53>(tmp_295_fu_16105_p3.read());
}

void mnist_fp16_opt::thread_p_Result_28_fu_15762_p4() {
    p_Result_28_fu_15762_p4 = tmp32_V_54_fu_15758_p1.read().range(30, 23);
}

void mnist_fp16_opt::thread_p_Result_29_fu_15741_p2() {
    p_Result_29_fu_15741_p2 = (!tmp_1286_fu_15737_p1.read().is_01())? sc_lv<16>(): p_Val2_34_reg_26138.read() >> (unsigned short)tmp_1286_fu_15737_p1.read().to_uint();
}

void mnist_fp16_opt::thread_p_Result_33_fu_16700_p4() {
    p_Result_33_fu_16700_p4 = tmp_V_5_fu_16695_p3.read().range(0, 15);
}

void mnist_fp16_opt::thread_p_Result_34_fu_16710_p3() {
    p_Result_34_fu_16710_p3 = esl_concat<16,16>(ap_const_lv16_FFFF, p_Result_33_fu_16700_p4.read());
}

void mnist_fp16_opt::thread_p_Result_35_fu_16820_p4() {
    p_Result_35_fu_16820_p4 = tmp32_V_20_fu_16816_p1.read().range(30, 23);
}

void mnist_fp16_opt::thread_p_Result_36_fu_16859_p5() {
    p_Result_36_fu_16859_p5 = esl_partset<32,32,9,32,32>(tmp32_V_20_reg_26460.read(), tmp_411_fu_16852_p3.read(), ap_const_lv32_17, ap_const_lv32_1F);
}

void mnist_fp16_opt::thread_p_Result_38_fu_18725_p1() {
    p_Result_38_fu_18725_p1 = esl_zext<54,53>(tmp_389_fu_18718_p3.read());
}

void mnist_fp16_opt::thread_p_Result_41_fu_19634_p2() {
    p_Result_41_fu_19634_p2 = (!tmp_1442_fu_19630_p1.read().is_01())? sc_lv<16>(): p_Val2_48_reg_27213.read() >> (unsigned short)tmp_1442_fu_19630_p1.read().to_uint();
}

void mnist_fp16_opt::thread_p_Result_42_fu_19655_p4() {
    p_Result_42_fu_19655_p4 = tmp32_V_55_fu_19651_p1.read().range(30, 23);
}

void mnist_fp16_opt::thread_p_Result_45_fu_21228_p1() {
    p_Result_45_fu_21228_p1 = esl_zext<54,53>(tmp_468_fu_21221_p3.read());
}

void mnist_fp16_opt::thread_p_Result_48_fu_20873_p2() {
    p_Result_48_fu_20873_p2 = (!tmp_1460_fu_20869_p1.read().is_01())? sc_lv<16>(): p_Val2_50_reg_27533.read() >> (unsigned short)tmp_1460_fu_20869_p1.read().to_uint();
}

void mnist_fp16_opt::thread_p_Result_49_fu_20894_p4() {
    p_Result_49_fu_20894_p4 = tmp32_V_56_fu_20890_p1.read().range(30, 23);
}

void mnist_fp16_opt::thread_p_Result_4_fu_7505_p1() {
    p_Result_4_fu_7505_p1 = esl_zext<54,53>(tmp_56_fu_7498_p3.read());
}

void mnist_fp16_opt::thread_p_Result_54_fu_21938_p1() {
    p_Result_54_fu_21938_p1 = esl_zext<54,53>(tmp_432_fu_21931_p3.read());
}

void mnist_fp16_opt::thread_p_Result_55_fu_21610_p4() {
    p_Result_55_fu_21610_p4 = tmp_V_8_fu_21605_p3.read().range(0, 15);
}

void mnist_fp16_opt::thread_p_Result_56_fu_21620_p3() {
    p_Result_56_fu_21620_p3 = esl_concat<16,16>(ap_const_lv16_FFFF, p_Result_55_fu_21610_p4.read());
}

void mnist_fp16_opt::thread_p_Result_57_fu_21730_p4() {
    p_Result_57_fu_21730_p4 = tmp32_V_32_fu_21726_p1.read().range(30, 23);
}

void mnist_fp16_opt::thread_p_Result_58_fu_21769_p5() {
    p_Result_58_fu_21769_p5 = esl_partset<32,32,9,32,32>(tmp32_V_32_reg_27802.read(), tmp_533_fu_21762_p3.read(), ap_const_lv32_17, ap_const_lv32_1F);
}

void mnist_fp16_opt::thread_p_Result_59_fu_22265_p4() {
    p_Result_59_fu_22265_p4 = tmp_V_9_fu_22260_p3.read().range(0, 15);
}

void mnist_fp16_opt::thread_p_Result_60_fu_22275_p3() {
    p_Result_60_fu_22275_p3 = esl_concat<16,16>(ap_const_lv16_FFFF, p_Result_59_fu_22265_p4.read());
}

void mnist_fp16_opt::thread_p_Result_61_fu_22389_p4() {
    p_Result_61_fu_22389_p4 = tmp32_V_36_fu_22385_p1.read().range(30, 23);
}

void mnist_fp16_opt::thread_p_Result_62_fu_22428_p5() {
    p_Result_62_fu_22428_p5 = esl_partset<32,32,9,32,32>(tmp32_V_36_reg_27993.read(), tmp_580_fu_22421_p3.read(), ap_const_lv32_17, ap_const_lv32_1F);
}

void mnist_fp16_opt::thread_p_Result_63_fu_8406_p4() {
    p_Result_63_fu_8406_p4 = p_Val2_4_fu_8400_p3.read().range(0, 15);
}

void mnist_fp16_opt::thread_p_Result_64_fu_8416_p3() {
    p_Result_64_fu_8416_p3 = esl_concat<16,16>(ap_const_lv16_FFFF, p_Result_63_fu_8406_p4.read());
}

void mnist_fp16_opt::thread_p_Result_65_fu_8565_p5() {
    p_Result_65_fu_8565_p5 = esl_partset<32,32,9,32,32>(tmp32_V_51_reg_24251.read(), tmp_96_fu_8558_p3.read(), ap_const_lv32_17, ap_const_lv32_1F);
}

void mnist_fp16_opt::thread_p_Result_66_fu_9138_p1() {
    p_Result_66_fu_9138_p1 = esl_zext<54,53>(tmp_128_fu_9131_p3.read());
}

void mnist_fp16_opt::thread_p_Result_67_fu_9223_p1() {
    p_Result_67_fu_9223_p1 = esl_zext<54,53>(tmp_206_fu_9216_p3.read());
}

void mnist_fp16_opt::thread_p_Result_68_fu_9666_p4() {
    p_Result_68_fu_9666_p4 = p_Val2_9_fu_9659_p3.read().range(0, 15);
}

void mnist_fp16_opt::thread_p_Result_69_fu_9676_p3() {
    p_Result_69_fu_9676_p3 = esl_concat<16,16>(ap_const_lv16_FFFF, p_Result_68_fu_9666_p4.read());
}

void mnist_fp16_opt::thread_p_Result_70_fu_9831_p5() {
    p_Result_70_fu_9831_p5 = esl_partset<32,32,9,32,32>(tmp32_V_52_reg_24569.read(), tmp_116_fu_9824_p3.read(), ap_const_lv32_17, ap_const_lv32_1F);
}

void mnist_fp16_opt::thread_p_Result_71_fu_14440_p4() {
    p_Result_71_fu_14440_p4 = p_Val2_31_fu_14434_p3.read().range(0, 15);
}

void mnist_fp16_opt::thread_p_Result_72_fu_14450_p3() {
    p_Result_72_fu_14450_p3 = esl_concat<16,16>(ap_const_lv16_FFFF, p_Result_71_fu_14440_p4.read());
}

void mnist_fp16_opt::thread_p_Result_73_fu_14599_p5() {
    p_Result_73_fu_14599_p5 = esl_partset<32,32,9,32,32>(tmp32_V_53_reg_25861.read(), tmp_283_fu_14592_p3.read(), ap_const_lv32_17, ap_const_lv32_1F);
}

void mnist_fp16_opt::thread_p_Result_74_fu_15108_p1() {
    p_Result_74_fu_15108_p1 = esl_zext<54,53>(tmp_334_fu_15101_p3.read());
}

void mnist_fp16_opt::thread_p_Result_75_fu_15193_p1() {
    p_Result_75_fu_15193_p1 = esl_zext<54,53>(tmp_386_fu_15186_p3.read());
}

void mnist_fp16_opt::thread_p_Result_76_fu_15636_p4() {
    p_Result_76_fu_15636_p4 = p_Val2_34_fu_15629_p3.read().range(0, 15);
}

void mnist_fp16_opt::thread_p_Result_77_fu_15646_p3() {
    p_Result_77_fu_15646_p3 = esl_concat<16,16>(ap_const_lv16_FFFF, p_Result_76_fu_15636_p4.read());
}

void mnist_fp16_opt::thread_p_Result_78_fu_15801_p5() {
    p_Result_78_fu_15801_p5 = esl_partset<32,32,9,32,32>(tmp32_V_54_reg_26169.read(), tmp_315_fu_15794_p3.read(), ap_const_lv32_17, ap_const_lv32_1F);
}

void mnist_fp16_opt::thread_p_Result_79_fu_19535_p4() {
    p_Result_79_fu_19535_p4 = p_Val2_48_fu_19529_p3.read().range(0, 15);
}

void mnist_fp16_opt::thread_p_Result_7_fu_10142_p1() {
    p_Result_7_fu_10142_p1 = esl_zext<54,53>(tmp_100_fu_10135_p3.read());
}

void mnist_fp16_opt::thread_p_Result_80_fu_19545_p3() {
    p_Result_80_fu_19545_p3 = esl_concat<16,16>(ap_const_lv16_FFFF, p_Result_79_fu_19535_p4.read());
}

void mnist_fp16_opt::thread_p_Result_81_fu_19694_p5() {
    p_Result_81_fu_19694_p5 = esl_partset<32,32,9,32,32>(tmp32_V_55_reg_27239.read(), tmp_458_fu_19687_p3.read(), ap_const_lv32_17, ap_const_lv32_1F);
}

void mnist_fp16_opt::thread_p_Result_82_fu_20240_p1() {
    p_Result_82_fu_20240_p1 = esl_zext<54,53>(tmp_517_fu_20233_p3.read());
}

void mnist_fp16_opt::thread_p_Result_83_fu_20360_p1() {
    p_Result_83_fu_20360_p1 = esl_zext<54,53>(tmp_584_fu_20353_p3.read());
}

void mnist_fp16_opt::thread_p_Result_84_fu_20768_p4() {
    p_Result_84_fu_20768_p4 = p_Val2_50_fu_20761_p3.read().range(0, 15);
}

void mnist_fp16_opt::thread_p_Result_85_fu_20778_p3() {
    p_Result_85_fu_20778_p3 = esl_concat<16,16>(ap_const_lv16_FFFF, p_Result_84_fu_20768_p4.read());
}

void mnist_fp16_opt::thread_p_Result_86_fu_20933_p5() {
    p_Result_86_fu_20933_p5 = esl_partset<32,32,9,32,32>(tmp32_V_56_reg_27564.read(), tmp_502_fu_20926_p3.read(), ap_const_lv32_17, ap_const_lv32_1F);
}

void mnist_fp16_opt::thread_p_Result_8_fu_9771_p2() {
    p_Result_8_fu_9771_p2 = (!tmp_638_fu_9767_p1.read().is_01())? sc_lv<16>(): p_Val2_9_reg_24538.read() >> (unsigned short)tmp_638_fu_9767_p1.read().to_uint();
}

void mnist_fp16_opt::thread_p_Result_9_fu_8526_p4() {
    p_Result_9_fu_8526_p4 = tmp32_V_51_fu_8522_p1.read().range(30, 23);
}

void mnist_fp16_opt::thread_p_Result_s_48_fu_9792_p4() {
    p_Result_s_48_fu_9792_p4 = tmp32_V_52_fu_9788_p1.read().range(30, 23);
}

void mnist_fp16_opt::thread_p_Result_s_fu_8505_p2() {
    p_Result_s_fu_8505_p2 = (!tmp_518_fu_8501_p1.read().is_01())? sc_lv<16>(): p_Val2_4_reg_24225.read() >> (unsigned short)tmp_518_fu_8501_p1.read().to_uint();
}

void mnist_fp16_opt::thread_p_Val2_13_fu_15558_p3() {
    p_Val2_13_fu_15558_p3 = (!or_cond23_fu_15552_p2.read()[0].is_01())? sc_lv<16>(): ((or_cond23_fu_15552_p2.read()[0].to_bool())? newSel27_fu_15544_p3.read(): ap_const_lv16_0);
}

void mnist_fp16_opt::thread_p_Val2_15_fu_20541_p3() {
    p_Val2_15_fu_20541_p3 = (!or_cond35_fu_20535_p2.read()[0].is_01())? sc_lv<16>(): ((or_cond35_fu_20535_p2.read()[0].to_bool())? newSel42_fu_20527_p3.read(): ap_const_lv16_0);
}

void mnist_fp16_opt::thread_p_Val2_17_fu_20690_p3() {
    p_Val2_17_fu_20690_p3 = (!or_cond38_fu_20684_p2.read()[0].is_01())? sc_lv<16>(): ((or_cond38_fu_20684_p2.read()[0].to_bool())? newSel45_fu_20676_p3.read(): ap_const_lv16_0);
}

void mnist_fp16_opt::thread_p_Val2_2_fu_9439_p3() {
    p_Val2_2_fu_9439_p3 = (!or_cond8_fu_9433_p2.read()[0].is_01())? sc_lv<16>(): ((or_cond8_fu_9433_p2.read()[0].to_bool())? newSel10_fu_9425_p3.read(): ap_const_lv16_0);
}

void mnist_fp16_opt::thread_p_Val2_31_fu_14434_p3() {
    p_Val2_31_fu_14434_p3 = (!is_neg_2_fu_14427_p3.read()[0].is_01())? sc_lv<16>(): ((is_neg_2_fu_14427_p3.read()[0].to_bool())? tmp_230_reg_25825.read(): p_Val2_23_reg_25815.read());
}

void mnist_fp16_opt::thread_p_Val2_34_fu_15629_p3() {
    p_Val2_34_fu_15629_p3 = (!is_neg_3_fu_15621_p3.read()[0].is_01())? sc_lv<16>(): ((is_neg_3_fu_15621_p3.read()[0].to_bool())? tmp_257_reg_26113.read(): p_Val2_43_reg_3449.read());
}

void mnist_fp16_opt::thread_p_Val2_48_fu_19529_p3() {
    p_Val2_48_fu_19529_p3 = (!is_neg_4_fu_19522_p3.read()[0].is_01())? sc_lv<16>(): ((is_neg_4_fu_19522_p3.read()[0].to_bool())? tmp_408_reg_27203.read(): p_Val2_46_reg_27193.read());
}

void mnist_fp16_opt::thread_p_Val2_4_fu_8400_p3() {
    p_Val2_4_fu_8400_p3 = (!is_neg_fu_8393_p3.read()[0].is_01())? sc_lv<16>(): ((is_neg_fu_8393_p3.read()[0].to_bool())? tmp_55_reg_24215.read(): p_Val2_s_reg_24205.read());
}

void mnist_fp16_opt::thread_p_Val2_50_fu_20761_p3() {
    p_Val2_50_fu_20761_p3 = (!is_neg_5_fu_20753_p3.read()[0].is_01())? sc_lv<16>(): ((is_neg_5_fu_20753_p3.read()[0].to_bool())? tmp_441_reg_27508.read(): p_Val2_54_reg_4064.read());
}

void mnist_fp16_opt::thread_p_Val2_5_fu_9588_p3() {
    p_Val2_5_fu_9588_p3 = (!or_cond11_fu_9582_p2.read()[0].is_01())? sc_lv<16>(): ((or_cond11_fu_9582_p2.read()[0].to_bool())? newSel13_fu_9574_p3.read(): ap_const_lv16_0);
}

void mnist_fp16_opt::thread_p_Val2_7_fu_15444_p3() {
    p_Val2_7_fu_15444_p3 = (!or_cond20_fu_15438_p2.read()[0].is_01())? sc_lv<16>(): ((or_cond20_fu_15438_p2.read()[0].to_bool())? newSel24_fu_15430_p3.read(): ap_const_lv16_0);
}

void mnist_fp16_opt::thread_p_Val2_9_fu_9659_p3() {
    p_Val2_9_fu_9659_p3 = (!is_neg_1_fu_9651_p3.read()[0].is_01())? sc_lv<16>(): ((is_neg_1_fu_9651_p3.read()[0].to_bool())? tmp_79_reg_24513.read(): p_Val2_18_reg_2797.read());
}

void mnist_fp16_opt::thread_p_a_assign_load_to_i_fu_22468_p1() {
    p_a_assign_load_to_i_fu_22468_p1 = reg_4515.read();
}

void mnist_fp16_opt::thread_p_shl100_cast_fu_12401_p1() {
    p_shl100_cast_fu_12401_p1 = esl_zext<7,6>(tmp_954_fu_12394_p3.read());
}

void mnist_fp16_opt::thread_p_shl101_cast_fu_12412_p1() {
    p_shl101_cast_fu_12412_p1 = esl_zext<7,3>(tmp_955_fu_12405_p3.read());
}

void mnist_fp16_opt::thread_p_shl102_cast_fu_12436_p3() {
    p_shl102_cast_fu_12436_p3 = esl_concat<7,4>(tmp_1115_fu_12432_p1.read(), ap_const_lv4_0);
}

void mnist_fp16_opt::thread_p_shl103_cast_fu_12452_p1() {
    p_shl103_cast_fu_12452_p1 = esl_sext<11,9>(tmp_1120_fu_12444_p3.read());
}

void mnist_fp16_opt::thread_p_shl104_cast_fu_6441_p3() {
    p_shl104_cast_fu_6441_p3 = esl_concat<6,5>(tmp_1137_fu_6437_p1.read(), ap_const_lv5_0);
}

void mnist_fp16_opt::thread_p_shl105_cast_fu_6453_p3() {
    p_shl105_cast_fu_6453_p3 = esl_concat<9,2>(tmp_1138_fu_6449_p1.read(), ap_const_lv2_0);
}

void mnist_fp16_opt::thread_p_shl106_cast_fu_14679_p1() {
    p_shl106_cast_fu_14679_p1 = esl_zext<9,8>(tmp_973_fu_14671_p3.read());
}

void mnist_fp16_opt::thread_p_shl107_cast_fu_14691_p1() {
    p_shl107_cast_fu_14691_p1 = esl_zext<9,5>(tmp_974_fu_14683_p3.read());
}

void mnist_fp16_opt::thread_p_shl108_cast_fu_14765_p3() {
    p_shl108_cast_fu_14765_p3 = esl_concat<8,4>(tmp_1143_fu_14761_p1.read(), ap_const_lv4_0);
}

void mnist_fp16_opt::thread_p_shl109_cast_fu_14781_p1() {
    p_shl109_cast_fu_14781_p1 = esl_sext<12,11>(tmp_1151_fu_14773_p3.read());
}

void mnist_fp16_opt::thread_p_shl10_cast_fu_4668_p1() {
    p_shl10_cast_fu_4668_p1 = esl_zext<11,10>(tmp_16_fu_4660_p3.read());
}

void mnist_fp16_opt::thread_p_shl110_cast_fu_12513_p1() {
    p_shl110_cast_fu_12513_p1 = esl_zext<7,6>(tmp_991_fu_12506_p3.read());
}

void mnist_fp16_opt::thread_p_shl111_cast_fu_12524_p1() {
    p_shl111_cast_fu_12524_p1 = esl_zext<7,3>(tmp_992_fu_12517_p3.read());
}

void mnist_fp16_opt::thread_p_shl112_cast_fu_12548_p3() {
    p_shl112_cast_fu_12548_p3 = esl_concat<7,4>(tmp_1256_fu_12544_p1.read(), ap_const_lv4_0);
}

void mnist_fp16_opt::thread_p_shl113_cast_fu_12564_p1() {
    p_shl113_cast_fu_12564_p1 = esl_sext<11,9>(tmp_1263_fu_12556_p3.read());
}

void mnist_fp16_opt::thread_p_shl114_cast_fu_6511_p3() {
    p_shl114_cast_fu_6511_p3 = esl_concat<6,5>(tmp_1268_fu_6507_p1.read(), ap_const_lv5_0);
}

void mnist_fp16_opt::thread_p_shl115_cast_fu_6523_p3() {
    p_shl115_cast_fu_6523_p3 = esl_concat<9,2>(tmp_1269_fu_6519_p1.read(), ap_const_lv2_0);
}

void mnist_fp16_opt::thread_p_shl116_cast_fu_15876_p1() {
    p_shl116_cast_fu_15876_p1 = esl_zext<9,8>(tmp_1008_fu_15868_p3.read());
}

void mnist_fp16_opt::thread_p_shl117_cast_fu_15888_p1() {
    p_shl117_cast_fu_15888_p1 = esl_zext<9,5>(tmp_1009_fu_15880_p3.read());
}

void mnist_fp16_opt::thread_p_shl118_cast_fu_15982_p3() {
    p_shl118_cast_fu_15982_p3 = esl_concat<8,4>(tmp_1270_reg_26208.read(), ap_const_lv4_0);
}

void mnist_fp16_opt::thread_p_shl119_cast_fu_15996_p1() {
    p_shl119_cast_fu_15996_p1 = esl_sext<12,11>(tmp_1271_fu_15989_p3.read());
}

void mnist_fp16_opt::thread_p_shl11_cast_fu_4680_p1() {
    p_shl11_cast_fu_4680_p1 = esl_zext<11,6>(tmp_17_fu_4672_p3.read());
}

void mnist_fp16_opt::thread_p_shl121_cast_fu_14979_p3() {
    p_shl121_cast_fu_14979_p3 = esl_concat<9,2>(tmp_1291_reg_25951.read(), ap_const_lv2_0);
}

void mnist_fp16_opt::thread_p_shl122_cast_fu_12625_p1() {
    p_shl122_cast_fu_12625_p1 = esl_zext<7,6>(tmp_1037_fu_12618_p3.read());
}

void mnist_fp16_opt::thread_p_shl123_cast_fu_12636_p1() {
    p_shl123_cast_fu_12636_p1 = esl_zext<7,3>(tmp_1038_fu_12629_p3.read());
}

void mnist_fp16_opt::thread_p_shl124_cast_fu_12660_p3() {
    p_shl124_cast_fu_12660_p3 = esl_concat<7,4>(tmp_1320_fu_12656_p1.read(), ap_const_lv4_0);
}

void mnist_fp16_opt::thread_p_shl125_cast_fu_12676_p1() {
    p_shl125_cast_fu_12676_p1 = esl_sext<11,9>(tmp_1321_fu_12668_p3.read());
}

void mnist_fp16_opt::thread_p_shl126_cast_fu_6581_p3() {
    p_shl126_cast_fu_6581_p3 = esl_concat<6,5>(tmp_1326_fu_6577_p1.read(), ap_const_lv5_0);
}

void mnist_fp16_opt::thread_p_shl127_cast_fu_6593_p3() {
    p_shl127_cast_fu_6593_p3 = esl_concat<9,2>(tmp_1327_fu_6589_p1.read(), ap_const_lv2_0);
}

void mnist_fp16_opt::thread_p_shl128_cast_fu_16387_p1() {
    p_shl128_cast_fu_16387_p1 = esl_zext<9,8>(tmp_1054_fu_16380_p3.read());
}

void mnist_fp16_opt::thread_p_shl129_cast_fu_16398_p1() {
    p_shl129_cast_fu_16398_p1 = esl_zext<9,5>(tmp_1055_fu_16391_p3.read());
}

void mnist_fp16_opt::thread_p_shl12_cast_fu_6681_p3() {
    p_shl12_cast_fu_6681_p3 = esl_concat<6,5>(tmp_105_fu_6677_p1.read(), ap_const_lv5_0);
}

void mnist_fp16_opt::thread_p_shl130_cast_fu_16419_p1() {
    p_shl130_cast_fu_16419_p1 = esl_zext<8,7>(tmp_1057_fu_16412_p3.read());
}

void mnist_fp16_opt::thread_p_shl131_cast_fu_16508_p3() {
    p_shl131_cast_fu_16508_p3 = esl_concat<7,3>(tmp_1328_fu_16504_p1.read(), ap_const_lv3_0);
}

void mnist_fp16_opt::thread_p_shl132_cast_fu_12717_p1() {
    p_shl132_cast_fu_12717_p1 = esl_zext<7,6>(tmp_1073_fu_12710_p3.read());
}

void mnist_fp16_opt::thread_p_shl133_cast_fu_12728_p1() {
    p_shl133_cast_fu_12728_p1 = esl_zext<7,3>(tmp_1074_fu_12721_p3.read());
}

void mnist_fp16_opt::thread_p_shl134_cast_fu_12752_p3() {
    p_shl134_cast_fu_12752_p3 = esl_concat<7,4>(tmp_1343_fu_12748_p1.read(), ap_const_lv4_0);
}

void mnist_fp16_opt::thread_p_shl135_cast_fu_12768_p1() {
    p_shl135_cast_fu_12768_p1 = esl_sext<11,9>(tmp_1344_fu_12760_p3.read());
}

void mnist_fp16_opt::thread_p_shl136_cast_fu_6651_p3() {
    p_shl136_cast_fu_6651_p3 = esl_concat<6,5>(tmp_1349_fu_6647_p1.read(), ap_const_lv5_0);
}

void mnist_fp16_opt::thread_p_shl137_cast_fu_6663_p3() {
    p_shl137_cast_fu_6663_p3 = esl_concat<9,2>(tmp_1350_fu_6659_p1.read(), ap_const_lv2_0);
}

void mnist_fp16_opt::thread_p_shl138_cast_fu_17041_p1() {
    p_shl138_cast_fu_17041_p1 = esl_zext<8,7>(tmp_1090_fu_17033_p3.read());
}

void mnist_fp16_opt::thread_p_shl139_cast_fu_17075_p3() {
    p_shl139_cast_fu_17075_p3 = esl_concat<8,3>(tmp_1092_reg_26519.read(), ap_const_lv3_0);
}

void mnist_fp16_opt::thread_p_shl13_cast_fu_6693_p3() {
    p_shl13_cast_fu_6693_p3 = esl_concat<9,2>(tmp_111_fu_6689_p1.read(), ap_const_lv2_0);
}

void mnist_fp16_opt::thread_p_shl140_cast_fu_16638_p3() {
    p_shl140_cast_fu_16638_p3 = esl_concat<8,4>(tmp_1351_reg_26392.read(), ap_const_lv4_0);
}

void mnist_fp16_opt::thread_p_shl141_cast_fu_16652_p1() {
    p_shl141_cast_fu_16652_p1 = esl_sext<12,11>(tmp_1352_fu_16645_p3.read());
}

void mnist_fp16_opt::thread_p_shl142_cast_fu_17551_p1() {
    p_shl142_cast_fu_17551_p1 = esl_zext<7,6>(tmp_1117_fu_17544_p3.read());
}

void mnist_fp16_opt::thread_p_shl143_cast_fu_17579_p3() {
    p_shl143_cast_fu_17579_p3 = esl_concat<7,3>(tmp_1374_fu_17575_p1.read(), ap_const_lv3_0);
}

void mnist_fp16_opt::thread_p_shl145_cast_fu_18129_p3() {
    p_shl145_cast_fu_18129_p3 = esl_concat<8,3>(tmp_1375_fu_18125_p1.read(), ap_const_lv3_0);
}

void mnist_fp16_opt::thread_p_shl146_cast_fu_17721_p1() {
    p_shl146_cast_fu_17721_p1 = esl_zext<7,6>(tmp_1140_fu_17714_p3.read());
}

void mnist_fp16_opt::thread_p_shl147_cast_fu_17749_p3() {
    p_shl147_cast_fu_17749_p3 = esl_concat<7,3>(tmp_1386_fu_17745_p1.read(), ap_const_lv3_0);
}

void mnist_fp16_opt::thread_p_shl148_cast_fu_18509_p1() {
    p_shl148_cast_fu_18509_p1 = esl_zext<9,8>(tmp_1147_fu_18501_p3.read());
}

void mnist_fp16_opt::thread_p_shl149_cast_fu_18606_p3() {
    p_shl149_cast_fu_18606_p3 = esl_concat<8,3>(tmp_1387_reg_26973.read(), ap_const_lv3_0);
}

void mnist_fp16_opt::thread_p_shl14_cast1_fu_6972_p1() {
    p_shl14_cast1_fu_6972_p1 = esl_zext<9,5>(tmp_120_fu_6964_p3.read());
}

void mnist_fp16_opt::thread_p_shl14_cast_fu_6976_p1() {
    p_shl14_cast_fu_6976_p1 = esl_zext<6,5>(tmp_120_fu_6964_p3.read());
}

void mnist_fp16_opt::thread_p_shl151_cast_fu_18319_p1() {
    p_shl151_cast_fu_18319_p1 = esl_zext<8,7>(tmp_1157_fu_18312_p3.read());
}

void mnist_fp16_opt::thread_p_shl152_cast_fu_18381_p3() {
    p_shl152_cast_fu_18381_p3 = esl_concat<8,3>(tmp_1160_reg_26898.read(), ap_const_lv3_0);
}

void mnist_fp16_opt::thread_p_shl153_cast_fu_18355_p3() {
    p_shl153_cast_fu_18355_p3 = esl_concat<10,2>(tmp_1400_fu_18351_p1.read(), ap_const_lv2_0);
}

void mnist_fp16_opt::thread_p_shl154_cast_fu_17817_p1() {
    p_shl154_cast_fu_17817_p1 = esl_zext<7,6>(tmp_1177_fu_17810_p3.read());
}

void mnist_fp16_opt::thread_p_shl155_cast_fu_17845_p3() {
    p_shl155_cast_fu_17845_p3 = esl_concat<7,3>(tmp_1411_fu_17841_p1.read(), ap_const_lv3_0);
}

void mnist_fp16_opt::thread_p_shl156_cast_fu_19212_p1() {
    p_shl156_cast_fu_19212_p1 = esl_zext<9,8>(tmp_1184_fu_19205_p3.read());
}

void mnist_fp16_opt::thread_p_shl157_cast_fu_19235_p3() {
    p_shl157_cast_fu_19235_p3 = esl_concat<9,3>(tmp_1187_fu_19225_p2.read(), ap_const_lv3_0);
}

void mnist_fp16_opt::thread_p_shl158_cast_fu_19456_p1() {
    p_shl158_cast_fu_19456_p1 = esl_zext<8,7>(tmp_1195_fu_19449_p3.read());
}

void mnist_fp16_opt::thread_p_shl159_cast_fu_19486_p3() {
    p_shl159_cast_fu_19486_p3 = esl_concat<8,3>(tmp_1423_reg_27183.read(), ap_const_lv3_0);
}

void mnist_fp16_opt::thread_p_shl15_cast_fu_6998_p1() {
    p_shl15_cast_fu_6998_p1 = esl_zext<9,8>(tmp_129_fu_6990_p3.read());
}

void mnist_fp16_opt::thread_p_shl160_cast_fu_17913_p1() {
    p_shl160_cast_fu_17913_p1 = esl_zext<7,6>(tmp_1211_fu_17906_p3.read());
}

void mnist_fp16_opt::thread_p_shl161_cast_fu_17941_p3() {
    p_shl161_cast_fu_17941_p3 = esl_concat<7,3>(tmp_1434_fu_17937_p1.read(), ap_const_lv3_0);
}

void mnist_fp16_opt::thread_p_shl162_cast_fu_19778_p1() {
    p_shl162_cast_fu_19778_p1 = esl_zext<9,8>(tmp_1221_fu_19770_p3.read());
}

void mnist_fp16_opt::thread_p_shl163_cast_fu_19860_p3() {
    p_shl163_cast_fu_19860_p3 = esl_concat<8,3>(tmp_1435_fu_19856_p1.read(), ap_const_lv3_0);
}

void mnist_fp16_opt::thread_p_shl164_cast_fu_21012_p1() {
    p_shl164_cast_fu_21012_p1 = esl_zext<9,8>(tmp_1229_fu_21004_p3.read());
}

void mnist_fp16_opt::thread_p_shl165_cast_fu_21109_p3() {
    p_shl165_cast_fu_21109_p3 = esl_concat<8,3>(tmp_1445_reg_27603.read(), ap_const_lv3_0);
}

void mnist_fp16_opt::thread_p_shl167_cast_fu_20056_p1() {
    p_shl167_cast_fu_20056_p1 = esl_zext<9,8>(tmp_1239_fu_20049_p3.read());
}

void mnist_fp16_opt::thread_p_shl168_cast_fu_20118_p3() {
    p_shl168_cast_fu_20118_p3 = esl_concat<9,3>(tmp_1243_reg_27350.read(), ap_const_lv3_0);
}

void mnist_fp16_opt::thread_p_shl169_cast_fu_20092_p3() {
    p_shl169_cast_fu_20092_p3 = esl_concat<11,2>(tmp_1467_fu_20088_p1.read(), ap_const_lv2_0);
}

void mnist_fp16_opt::thread_p_shl170_cast_fu_21481_p1() {
    p_shl170_cast_fu_21481_p1 = esl_zext<9,8>(tmp_1251_fu_21473_p3.read());
}

void mnist_fp16_opt::thread_p_shl171_cast_fu_21552_p3() {
    p_shl171_cast_fu_21552_p3 = esl_concat<8,3>(tmp_1489_fu_21548_p1.read(), ap_const_lv3_0);
}

void mnist_fp16_opt::thread_p_shl172_cast_fu_22214_p1() {
    p_shl172_cast_fu_22214_p1 = esl_zext<9,8>(tmp_1257_fu_22206_p3.read());
}

void mnist_fp16_opt::thread_p_shl173_cast_fu_22226_p1() {
    p_shl173_cast_fu_22226_p1 = esl_zext<9,6>(tmp_1258_fu_22218_p3.read());
}

void mnist_fp16_opt::thread_p_shl17_cast_fu_7025_p3() {
    p_shl17_cast_fu_7025_p3 = esl_concat<8,5>(tmp_138_fu_7021_p1.read(), ap_const_lv5_0);
}

void mnist_fp16_opt::thread_p_shl18_cast_fu_7041_p1() {
    p_shl18_cast_fu_7041_p1 = esl_sext<13,12>(tmp_139_fu_7033_p3.read());
}

void mnist_fp16_opt::thread_p_shl19_cast_fu_6747_p3() {
    p_shl19_cast_fu_6747_p3 = esl_concat<6,5>(tmp_180_fu_6743_p1.read(), ap_const_lv5_0);
}

void mnist_fp16_opt::thread_p_shl1_cast_fu_11132_p1() {
    p_shl1_cast_fu_11132_p1 = esl_zext<9,8>(p_shl1_fu_11124_p3.read());
}

void mnist_fp16_opt::thread_p_shl1_fu_11124_p3() {
    p_shl1_fu_11124_p3 = esl_concat<4,4>(tmp_761_fu_11120_p1.read(), ap_const_lv4_0);
}

void mnist_fp16_opt::thread_p_shl20_cast_fu_6759_p3() {
    p_shl20_cast_fu_6759_p3 = esl_concat<9,2>(tmp_181_fu_6755_p1.read(), ap_const_lv2_0);
}

void mnist_fp16_opt::thread_p_shl21_cast_fu_7269_p1() {
    p_shl21_cast_fu_7269_p1 = esl_zext<9,8>(tmp_187_fu_7261_p3.read());
}

void mnist_fp16_opt::thread_p_shl22_cast_fu_7281_p1() {
    p_shl22_cast_fu_7281_p1 = esl_zext<9,5>(tmp_189_fu_7273_p3.read());
}

void mnist_fp16_opt::thread_p_shl23_cast_fu_7375_p3() {
    p_shl23_cast_fu_7375_p3 = esl_concat<8,5>(tmp_199_reg_23940.read(), ap_const_lv5_0);
}

void mnist_fp16_opt::thread_p_shl24_cast_fu_7389_p1() {
    p_shl24_cast_fu_7389_p1 = esl_sext<13,12>(tmp_201_fu_7382_p3.read());
}

void mnist_fp16_opt::thread_p_shl25_cast_fu_7116_p1() {
    p_shl25_cast_fu_7116_p1 = esl_zext<11,10>(tmp_227_fu_7109_p3.read());
}

void mnist_fp16_opt::thread_p_shl26_cast_fu_7127_p1() {
    p_shl26_cast_fu_7127_p1 = esl_zext<11,6>(tmp_228_fu_7120_p3.read());
}

void mnist_fp16_opt::thread_p_shl28_cast_fu_5901_p3() {
    p_shl28_cast_fu_5901_p3 = esl_concat<6,5>(tmp_268_fu_5897_p1.read(), ap_const_lv5_0);
}

void mnist_fp16_opt::thread_p_shl29_cast_fu_5913_p3() {
    p_shl29_cast_fu_5913_p3 = esl_concat<9,2>(tmp_269_fu_5909_p1.read(), ap_const_lv2_0);
}

void mnist_fp16_opt::thread_p_shl2_cast_fu_11142_p1() {
    p_shl2_cast_fu_11142_p1 = esl_zext<9,5>(tmp_762_fu_11136_p2.read());
}

void mnist_fp16_opt::thread_p_shl2_fu_13100_p1() {
    p_shl2_fu_13100_p1 = esl_zext<64,9>(tmp_905_fu_13092_p3.read());
}

void mnist_fp16_opt::thread_p_shl30_cast_fu_7893_p1() {
    p_shl30_cast_fu_7893_p1 = esl_zext<9,8>(tmp_277_fu_7886_p3.read());
}

void mnist_fp16_opt::thread_p_shl31_cast_fu_7904_p1() {
    p_shl31_cast_fu_7904_p1 = esl_zext<9,4>(tmp_279_fu_7897_p3.read());
}

void mnist_fp16_opt::thread_p_shl32_cast_fu_7952_p3() {
    p_shl32_cast_fu_7952_p3 = esl_concat<8,5>(tmp_296_fu_7948_p1.read(), ap_const_lv5_0);
}

void mnist_fp16_opt::thread_p_shl33_cast_fu_7968_p1() {
    p_shl33_cast_fu_7968_p1 = esl_sext<13,11>(tmp_299_fu_7960_p3.read());
}

void mnist_fp16_opt::thread_p_shl34_cast_fu_5951_p3() {
    p_shl34_cast_fu_5951_p3 = esl_concat<6,5>(tmp_375_fu_5947_p1.read(), ap_const_lv5_0);
}

void mnist_fp16_opt::thread_p_shl35_cast_fu_5963_p3() {
    p_shl35_cast_fu_5963_p3 = esl_concat<9,2>(tmp_376_fu_5959_p1.read(), ap_const_lv2_0);
}

void mnist_fp16_opt::thread_p_shl36_cast_fu_8694_p1() {
    p_shl36_cast_fu_8694_p1 = esl_zext<9,8>(tmp_385_fu_8686_p3.read());
}

void mnist_fp16_opt::thread_p_shl38_cast_fu_8721_p3() {
    p_shl38_cast_fu_8721_p3 = esl_concat<8,5>(tmp_398_fu_8717_p1.read(), ap_const_lv5_0);
}

void mnist_fp16_opt::thread_p_shl39_cast_fu_8737_p1() {
    p_shl39_cast_fu_8737_p1 = esl_sext<13,12>(tmp_401_fu_8729_p3.read());
}

void mnist_fp16_opt::thread_p_shl3_cast_fu_13838_p1() {
    p_shl3_cast_fu_13838_p1 = esl_zext<9,8>(p_shl3_fu_13830_p3.read());
}

void mnist_fp16_opt::thread_p_shl3_cast_mid1_fu_14008_p1() {
    p_shl3_cast_mid1_fu_14008_p1 = esl_zext<9,8>(p_shl3_mid1_fu_14000_p3.read());
}

}

