#include "mnist_fp16_opt.h"
#include "AESL_pkg.h"

using namespace std;

namespace ap_rtl {

void mnist_fp16_opt::thread_p_shl3_fu_13830_p3() {
    p_shl3_fu_13830_p3 = esl_concat<4,4>(tmp_924_fu_13826_p1.read(), ap_const_lv4_0);
}

void mnist_fp16_opt::thread_p_shl3_mid1_fu_14000_p3() {
    p_shl3_mid1_fu_14000_p3 = esl_concat<4,4>(tmp_989_fu_13996_p1.read(), ap_const_lv4_0);
}

void mnist_fp16_opt::thread_p_shl40_cast_fu_8308_p1() {
    p_shl40_cast_fu_8308_p1 = esl_zext<8,7>(tmp_451_fu_8301_p3.read());
}

void mnist_fp16_opt::thread_p_shl41_cast_fu_8319_p1() {
    p_shl41_cast_fu_8319_p1 = esl_zext<8,4>(tmp_461_fu_8312_p3.read());
}

void mnist_fp16_opt::thread_p_shl42_cast_fu_8346_p3() {
    p_shl42_cast_fu_8346_p3 = esl_concat<8,5>(tmp_473_reg_24195.read(), ap_const_lv5_0);
}

void mnist_fp16_opt::thread_p_shl43_cast_fu_8360_p1() {
    p_shl43_cast_fu_8360_p1 = esl_sext<13,11>(tmp_474_fu_8353_p3.read());
}

void mnist_fp16_opt::thread_p_shl44_cast_fu_6021_p3() {
    p_shl44_cast_fu_6021_p3 = esl_concat<6,5>(tmp_551_fu_6017_p1.read(), ap_const_lv5_0);
}

void mnist_fp16_opt::thread_p_shl45_cast_fu_6033_p3() {
    p_shl45_cast_fu_6033_p3 = esl_concat<9,2>(tmp_552_fu_6029_p1.read(), ap_const_lv2_0);
}

void mnist_fp16_opt::thread_p_shl46_cast_fu_9906_p1() {
    p_shl46_cast_fu_9906_p1 = esl_zext<9,8>(tmp_564_fu_9898_p3.read());
}

void mnist_fp16_opt::thread_p_shl47_cast_fu_9918_p1() {
    p_shl47_cast_fu_9918_p1 = esl_zext<9,5>(tmp_565_fu_9910_p3.read());
}

void mnist_fp16_opt::thread_p_shl48_cast_fu_10012_p3() {
    p_shl48_cast_fu_10012_p3 = esl_concat<8,5>(tmp_577_reg_24608.read(), ap_const_lv5_0);
}

void mnist_fp16_opt::thread_p_shl49_cast_fu_10026_p1() {
    p_shl49_cast_fu_10026_p1 = esl_sext<13,12>(tmp_581_fu_10019_p3.read());
}

void mnist_fp16_opt::thread_p_shl4_cast_fu_13848_p1() {
    p_shl4_cast_fu_13848_p1 = esl_zext<9,5>(tmp_925_fu_13842_p2.read());
}

void mnist_fp16_opt::thread_p_shl4_cast_mid1_fu_14018_p1() {
    p_shl4_cast_mid1_fu_14018_p1 = esl_zext<9,5>(tmp_995_fu_14012_p2.read());
}

void mnist_fp16_opt::thread_p_shl4_fu_14861_p1() {
    p_shl4_fu_14861_p1 = esl_zext<64,10>(tmp_1289_fu_14853_p3.read());
}

void mnist_fp16_opt::thread_p_shl51_cast_fu_8835_p1() {
    p_shl51_cast_fu_8835_p1 = esl_zext<9,8>(tmp_652_fu_8827_p3.read());
}

void mnist_fp16_opt::thread_p_shl52_cast_fu_8847_p1() {
    p_shl52_cast_fu_8847_p1 = esl_zext<9,4>(tmp_664_fu_8839_p3.read());
}

void mnist_fp16_opt::thread_p_shl53_cast_fu_8992_p3() {
    p_shl53_cast_fu_8992_p3 = esl_concat<8,5>(tmp_676_reg_24331.read(), ap_const_lv5_0);
}

void mnist_fp16_opt::thread_p_shl54_cast_fu_9006_p1() {
    p_shl54_cast_fu_9006_p1 = esl_sext<13,11>(tmp_677_fu_8999_p3.read());
}

void mnist_fp16_opt::thread_p_shl55_cast_fu_9016_p3() {
    p_shl55_cast_fu_9016_p3 = esl_concat<7,2>(tmp_681_reg_24346.read(), ap_const_lv2_0);
}

void mnist_fp16_opt::thread_p_shl56_cast_fu_6091_p3() {
    p_shl56_cast_fu_6091_p3 = esl_concat<6,5>(tmp_709_fu_6087_p1.read(), ap_const_lv5_0);
}

void mnist_fp16_opt::thread_p_shl57_cast_fu_6103_p3() {
    p_shl57_cast_fu_6103_p3 = esl_concat<9,2>(tmp_710_fu_6099_p1.read(), ap_const_lv2_0);
}

void mnist_fp16_opt::thread_p_shl58_cast_fu_10420_p1() {
    p_shl58_cast_fu_10420_p1 = esl_zext<9,8>(tmp_714_fu_10413_p3.read());
}

void mnist_fp16_opt::thread_p_shl59_cast_fu_10431_p1() {
    p_shl59_cast_fu_10431_p1 = esl_zext<9,5>(tmp_715_fu_10424_p3.read());
}

void mnist_fp16_opt::thread_p_shl5_cast_fu_4569_p1() {
    p_shl5_cast_fu_4569_p1 = esl_zext<11,10>(tmp_1_fu_4561_p3.read());
}

void mnist_fp16_opt::thread_p_shl5_fu_18302_p1() {
    p_shl5_fu_18302_p1 = esl_zext<64,11>(tmp_1396_fu_18295_p3.read());
}

void mnist_fp16_opt::thread_p_shl60_cast_fu_10452_p1() {
    p_shl60_cast_fu_10452_p1 = esl_zext<8,7>(tmp_717_fu_10445_p3.read());
}

void mnist_fp16_opt::thread_p_shl61_cast_fu_10463_p1() {
    p_shl61_cast_fu_10463_p1 = esl_zext<8,4>(tmp_718_fu_10456_p3.read());
}

void mnist_fp16_opt::thread_p_shl62_cast_fu_10541_p3() {
    p_shl62_cast_fu_10541_p3 = esl_concat<7,4>(tmp_722_fu_10537_p1.read(), ap_const_lv4_0);
}

void mnist_fp16_opt::thread_p_shl63_cast_fu_10557_p1() {
    p_shl63_cast_fu_10557_p1 = esl_sext<11,10>(tmp_723_fu_10549_p3.read());
}

void mnist_fp16_opt::thread_p_shl64_cast_fu_6161_p3() {
    p_shl64_cast_fu_6161_p3 = esl_concat<6,5>(tmp_737_fu_6157_p1.read(), ap_const_lv5_0);
}

void mnist_fp16_opt::thread_p_shl65_cast_fu_6173_p3() {
    p_shl65_cast_fu_6173_p3 = esl_concat<9,2>(tmp_738_fu_6169_p1.read(), ap_const_lv2_0);
}

void mnist_fp16_opt::thread_p_shl66_cast_fu_10683_p3() {
    p_shl66_cast_fu_10683_p3 = esl_concat<8,5>(tmp_764_reg_24797.read(), ap_const_lv5_0);
}

void mnist_fp16_opt::thread_p_shl67_cast_fu_10697_p1() {
    p_shl67_cast_fu_10697_p1 = esl_sext<13,12>(tmp_765_fu_10690_p3.read());
}

void mnist_fp16_opt::thread_p_shl68_cast_fu_12089_p1() {
    p_shl68_cast_fu_12089_p1 = esl_zext<7,6>(tmp_799_fu_12082_p3.read());
}

void mnist_fp16_opt::thread_p_shl69_cast_fu_12100_p1() {
    p_shl69_cast_fu_12100_p1 = esl_zext<7,3>(tmp_800_fu_12093_p3.read());
}

void mnist_fp16_opt::thread_p_shl6_cast_fu_4581_p1() {
    p_shl6_cast_fu_4581_p1 = esl_zext<11,7>(tmp_7_fu_4573_p3.read());
}

void mnist_fp16_opt::thread_p_shl6_fu_20039_p1() {
    p_shl6_fu_20039_p1 = esl_zext<64,12>(tmp_1463_fu_20032_p3.read());
}

void mnist_fp16_opt::thread_p_shl70_cast_fu_12124_p3() {
    p_shl70_cast_fu_12124_p3 = esl_concat<7,4>(tmp_803_fu_12120_p1.read(), ap_const_lv4_0);
}

void mnist_fp16_opt::thread_p_shl71_cast_fu_12140_p1() {
    p_shl71_cast_fu_12140_p1 = esl_sext<11,9>(tmp_804_fu_12132_p3.read());
}

void mnist_fp16_opt::thread_p_shl72_cast_fu_6231_p3() {
    p_shl72_cast_fu_6231_p3 = esl_concat<6,5>(tmp_814_fu_6227_p1.read(), ap_const_lv5_0);
}

void mnist_fp16_opt::thread_p_shl73_cast_fu_6243_p3() {
    p_shl73_cast_fu_6243_p3 = esl_concat<9,2>(tmp_815_fu_6239_p1.read(), ap_const_lv2_0);
}

void mnist_fp16_opt::thread_p_shl74_cast_fu_12918_p1() {
    p_shl74_cast_fu_12918_p1 = esl_zext<9,8>(tmp_820_fu_12910_p3.read());
}

void mnist_fp16_opt::thread_p_shl75_cast_fu_12930_p1() {
    p_shl75_cast_fu_12930_p1 = esl_zext<9,5>(tmp_821_fu_12922_p3.read());
}

void mnist_fp16_opt::thread_p_shl76_cast_fu_13004_p3() {
    p_shl76_cast_fu_13004_p3 = esl_concat<8,4>(tmp_825_fu_13000_p1.read(), ap_const_lv4_0);
}

void mnist_fp16_opt::thread_p_shl77_cast_fu_13020_p1() {
    p_shl77_cast_fu_13020_p1 = esl_sext<12,11>(tmp_826_fu_13012_p3.read());
}

void mnist_fp16_opt::thread_p_shl78_cast_fu_12177_p1() {
    p_shl78_cast_fu_12177_p1 = esl_zext<7,6>(tmp_848_fu_12170_p3.read());
}

void mnist_fp16_opt::thread_p_shl79_cast_fu_12188_p1() {
    p_shl79_cast_fu_12188_p1 = esl_zext<7,3>(tmp_849_fu_12181_p3.read());
}

void mnist_fp16_opt::thread_p_shl7_cast_fu_4714_p1() {
    p_shl7_cast_fu_4714_p1 = esl_zext<11,7>(p_shl7_fu_4706_p3.read());
}

void mnist_fp16_opt::thread_p_shl7_fu_4706_p3() {
    p_shl7_fu_4706_p3 = esl_concat<5,2>(ap_phi_mux_p_s_phi_fu_2277_p4.read(), ap_const_lv2_0);
}

void mnist_fp16_opt::thread_p_shl80_cast_fu_12212_p3() {
    p_shl80_cast_fu_12212_p3 = esl_concat<7,4>(tmp_852_fu_12208_p1.read(), ap_const_lv4_0);
}

void mnist_fp16_opt::thread_p_shl81_cast_fu_12228_p1() {
    p_shl81_cast_fu_12228_p1 = esl_sext<11,9>(tmp_853_fu_12220_p3.read());
}

void mnist_fp16_opt::thread_p_shl82_cast_fu_6301_p3() {
    p_shl82_cast_fu_6301_p3 = esl_concat<6,5>(tmp_864_fu_6297_p1.read(), ap_const_lv5_0);
}

void mnist_fp16_opt::thread_p_shl83_cast_fu_6313_p3() {
    p_shl83_cast_fu_6313_p3 = esl_concat<9,2>(tmp_865_fu_6309_p1.read(), ap_const_lv2_0);
}

void mnist_fp16_opt::thread_p_shl84_cast_fu_13343_p1() {
    p_shl84_cast_fu_13343_p1 = esl_zext<9,8>(tmp_869_fu_13335_p3.read());
}

void mnist_fp16_opt::thread_p_shl85_cast_fu_13355_p1() {
    p_shl85_cast_fu_13355_p1 = esl_zext<9,5>(tmp_870_fu_13347_p3.read());
}

void mnist_fp16_opt::thread_p_shl86_cast_fu_13449_p3() {
    p_shl86_cast_fu_13449_p3 = esl_concat<8,4>(tmp_874_reg_25564.read(), ap_const_lv4_0);
}

void mnist_fp16_opt::thread_p_shl87_cast_fu_13463_p1() {
    p_shl87_cast_fu_13463_p1 = esl_sext<12,11>(tmp_875_fu_13456_p3.read());
}

void mnist_fp16_opt::thread_p_shl89_cast_fu_13212_p3() {
    p_shl89_cast_fu_13212_p3 = esl_concat<8,2>(tmp_913_reg_25490.read(), ap_const_lv2_0);
}

void mnist_fp16_opt::thread_p_shl8_cast_fu_7748_p1() {
    p_shl8_cast_fu_7748_p1 = esl_zext<11,10>(p_shl8_fu_7740_p3.read());
}

void mnist_fp16_opt::thread_p_shl8_cast_mid1_fu_7985_p1() {
    p_shl8_cast_mid1_fu_7985_p1 = esl_zext<11,10>(p_shl8_mid1_fu_7978_p3.read());
}

void mnist_fp16_opt::thread_p_shl8_fu_7740_p3() {
    p_shl8_fu_7740_p3 = esl_concat<5,5>(ap_phi_mux_p_3_phi_fu_2660_p4.read(), ap_const_lv5_0);
}

void mnist_fp16_opt::thread_p_shl8_mid1_fu_7978_p3() {
    p_shl8_mid1_fu_7978_p3 = esl_concat<5,5>(index_tuple1_V_reg_24085.read(), ap_const_lv5_0);
}

void mnist_fp16_opt::thread_p_shl90_cast_fu_12289_p1() {
    p_shl90_cast_fu_12289_p1 = esl_zext<7,6>(tmp_901_fu_12282_p3.read());
}

void mnist_fp16_opt::thread_p_shl91_cast_fu_12300_p1() {
    p_shl91_cast_fu_12300_p1 = esl_zext<7,3>(tmp_902_fu_12293_p3.read());
}

void mnist_fp16_opt::thread_p_shl92_cast_fu_12324_p3() {
    p_shl92_cast_fu_12324_p3 = esl_concat<7,4>(tmp_959_fu_12320_p1.read(), ap_const_lv4_0);
}

void mnist_fp16_opt::thread_p_shl93_cast_fu_12340_p1() {
    p_shl93_cast_fu_12340_p1 = esl_sext<11,9>(tmp_966_fu_12332_p3.read());
}

void mnist_fp16_opt::thread_p_shl94_cast_fu_6371_p3() {
    p_shl94_cast_fu_6371_p3 = esl_concat<6,5>(tmp_987_fu_6367_p1.read(), ap_const_lv5_0);
}

void mnist_fp16_opt::thread_p_shl95_cast_fu_6383_p3() {
    p_shl95_cast_fu_6383_p3 = esl_concat<9,2>(tmp_988_fu_6379_p1.read(), ap_const_lv2_0);
}

void mnist_fp16_opt::thread_p_shl96_cast_fu_14342_p1() {
    p_shl96_cast_fu_14342_p1 = esl_zext<8,7>(tmp_937_fu_14335_p3.read());
}

void mnist_fp16_opt::thread_p_shl97_cast_fu_14353_p1() {
    p_shl97_cast_fu_14353_p1 = esl_zext<8,4>(tmp_938_fu_14346_p3.read());
}

void mnist_fp16_opt::thread_p_shl98_cast_fu_14380_p3() {
    p_shl98_cast_fu_14380_p3 = esl_concat<8,4>(tmp_1069_reg_25805.read(), ap_const_lv4_0);
}

void mnist_fp16_opt::thread_p_shl99_cast_fu_14394_p1() {
    p_shl99_cast_fu_14394_p1 = esl_sext<12,10>(tmp_1070_fu_14387_p3.read());
}

void mnist_fp16_opt::thread_p_shl9_cast_fu_7760_p1() {
    p_shl9_cast_fu_7760_p1 = esl_zext<11,7>(p_shl9_fu_7752_p3.read());
}

void mnist_fp16_opt::thread_p_shl9_cast_mid1_fu_7996_p1() {
    p_shl9_cast_mid1_fu_7996_p1 = esl_zext<11,7>(p_shl9_mid1_fu_7989_p3.read());
}

void mnist_fp16_opt::thread_p_shl9_fu_7752_p3() {
    p_shl9_fu_7752_p3 = esl_concat<5,2>(ap_phi_mux_p_3_phi_fu_2660_p4.read(), ap_const_lv2_0);
}

void mnist_fp16_opt::thread_p_shl9_mid1_fu_7989_p3() {
    p_shl9_mid1_fu_7989_p3 = esl_concat<5,2>(index_tuple1_V_reg_24085.read(), ap_const_lv2_0);
}

void mnist_fp16_opt::thread_p_shl_fu_8817_p1() {
    p_shl_fu_8817_p1 = esl_zext<64,8>(tmp_643_fu_8809_p3.read());
}

void mnist_fp16_opt::thread_p_v10_fu_17615_p3() {
    p_v10_fu_17615_p3 = (!tmp_1377_reg_26560_pp17_iter2_reg.read()[0].is_01())? sc_lv<11>(): ((tmp_1377_reg_26560_pp17_iter2_reg.read()[0].to_bool())? tmp_1134_fu_17608_p1.read(): tmp_1135_fu_17612_p1.read());
}

void mnist_fp16_opt::thread_p_v11_fu_17665_p3() {
    p_v11_fu_17665_p3 = (!tmp_1402_reg_26598_pp17_iter2_reg.read()[0].is_01())? sc_lv<11>(): ((tmp_1402_reg_26598_pp17_iter2_reg.read()[0].to_bool())? tmp_1171_fu_17658_p1.read(): tmp_1172_fu_17662_p1.read());
}

void mnist_fp16_opt::thread_p_v12_fu_19414_p3() {
    p_v12_fu_19414_p3 = (!tmp_1414_reg_27134_pp20_iter2_reg.read()[0].is_01())? sc_lv<12>(): ((tmp_1414_reg_27134_pp20_iter2_reg.read()[0].to_bool())? tmp_1193_fu_19407_p1.read(): tmp_1194_fu_19411_p1.read());
}

void mnist_fp16_opt::thread_p_v13_fu_17509_p3() {
    p_v13_fu_17509_p3 = (!tmp_1425_reg_26606_pp17_iter2_reg.read()[0].is_01())? sc_lv<11>(): ((tmp_1425_reg_26606_pp17_iter2_reg.read()[0].to_bool())? tmp_1208_fu_17502_p1.read(): tmp_1209_fu_17506_p1.read());
}

void mnist_fp16_opt::thread_p_v1_fu_11679_p3() {
    p_v1_fu_11679_p3 = (!tmp_784_reg_24975_pp10_iter1_reg.read()[0].is_01())? sc_lv<13>(): ((tmp_784_reg_24975_pp10_iter1_reg.read()[0].to_bool())? tmp_793_fu_11672_p1.read(): tmp_795_fu_11676_p1.read());
}

void mnist_fp16_opt::thread_p_v2_fu_11753_p3() {
    p_v2_fu_11753_p3 = (!tmp_833_reg_24998_pp10_iter1_reg.read()[0].is_01())? sc_lv<13>(): ((tmp_833_reg_24998_pp10_iter1_reg.read()[0].to_bool())? tmp_842_fu_11746_p1.read(): tmp_844_fu_11750_p1.read());
}

void mnist_fp16_opt::thread_p_v3_fu_11803_p3() {
    p_v3_fu_11803_p3 = (!tmp_934_reg_25031_pp10_iter1_reg.read()[0].is_01())? sc_lv<13>(): ((tmp_934_reg_25031_pp10_iter1_reg.read()[0].to_bool())? tmp_895_fu_11796_p1.read(): tmp_896_fu_11800_p1.read());
}

void mnist_fp16_opt::thread_p_v4_fu_14303_p3() {
    p_v4_fu_14303_p3 = (!tmp_1023_reg_25766.read()[0].is_01())? sc_lv<13>(): ((tmp_1023_reg_25766.read()[0].to_bool())? tmp_931_fu_14296_p1.read(): tmp_932_fu_14300_p1.read());
}

void mnist_fp16_opt::thread_p_v5_fu_11877_p3() {
    p_v5_fu_11877_p3 = (!tmp_1077_reg_25054_pp10_iter1_reg.read()[0].is_01())? sc_lv<13>(): ((tmp_1077_reg_25054_pp10_iter1_reg.read()[0].to_bool())? tmp_948_fu_11870_p1.read(): tmp_949_fu_11874_p1.read());
}

void mnist_fp16_opt::thread_p_v6_fu_11927_p3() {
    p_v6_fu_11927_p3 = (!tmp_1213_reg_25087_pp10_iter1_reg.read()[0].is_01())? sc_lv<13>(): ((tmp_1213_reg_25087_pp10_iter1_reg.read()[0].to_bool())? tmp_985_fu_11920_p1.read(): tmp_986_fu_11924_p1.read());
}

void mnist_fp16_opt::thread_p_v7_fu_12001_p3() {
    p_v7_fu_12001_p3 = (!tmp_1311_reg_25110_pp10_iter1_reg.read()[0].is_01())? sc_lv<13>(): ((tmp_1311_reg_25110_pp10_iter1_reg.read()[0].to_bool())? tmp_1031_fu_11994_p1.read(): tmp_1032_fu_11998_p1.read());
}

void mnist_fp16_opt::thread_p_v8_fu_12051_p3() {
    p_v8_fu_12051_p3 = (!tmp_1334_reg_25128_pp10_iter1_reg.read()[0].is_01())? sc_lv<13>(): ((tmp_1334_reg_25128_pp10_iter1_reg.read()[0].to_bool())? tmp_1067_fu_12044_p1.read(): tmp_1068_fu_12048_p1.read());
}

void mnist_fp16_opt::thread_p_v9_fu_17435_p3() {
    p_v9_fu_17435_p3 = (!tmp_1365_reg_26552_pp17_iter2_reg.read()[0].is_01())? sc_lv<11>(): ((tmp_1365_reg_26552_pp17_iter2_reg.read()[0].to_bool())? tmp_1111_fu_17428_p1.read(): tmp_1112_fu_17432_p1.read());
}

void mnist_fp16_opt::thread_p_v_fu_8269_p3() {
    p_v_fu_8269_p3 = (!tmp_418_reg_24156.read()[0].is_01())? sc_lv<14>(): ((tmp_418_reg_24156.read()[0].to_bool())? tmp_443_fu_8262_p1.read(): tmp_447_fu_8266_p1.read());
}

void mnist_fp16_opt::thread_pad_temp1_0_address0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp7_stage0.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp7_iter1.read()) && 
         esl_seteq<1,1,1>(ap_block_pp7_stage0.read(), ap_const_boolean_0))) {
        pad_temp1_0_address0 =  (sc_lv<12>) (tmp_751_cast_fu_9037_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_block_pp6_stage0.read(), ap_const_boolean_0) && 
                esl_seteq<1,1,1>(ap_enable_reg_pp6_iter31.read(), ap_const_logic_1))) {
        pad_temp1_0_address0 =  (sc_lv<12>) (tmp_753_cast_fu_8581_p1.read());
    } else {
        pad_temp1_0_address0 = "XXXXXXXXXXXX";
    }
}

void mnist_fp16_opt::thread_pad_temp1_0_ce0() {
    if (((esl_seteq<1,1,1>(ap_block_pp6_stage0_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_enable_reg_pp6_iter31.read(), ap_const_logic_1)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp7_stage0.read()) && 
          esl_seteq<1,1,1>(ap_block_pp7_stage0_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp7_iter1.read())))) {
        pad_temp1_0_ce0 = ap_const_logic_1;
    } else {
        pad_temp1_0_ce0 = ap_const_logic_0;
    }
}

void mnist_fp16_opt::thread_pad_temp1_0_we0() {
    if ((esl_seteq<1,1,1>(ap_block_pp6_stage0_11001.read(), ap_const_boolean_0) && 
         esl_seteq<1,1,1>(ap_enable_reg_pp6_iter31.read(), ap_const_logic_1) && 
         esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_flatten5_reg_24053_pp6_iter30_reg.read()))) {
        pad_temp1_0_we0 = ap_const_logic_1;
    } else {
        pad_temp1_0_we0 = ap_const_logic_0;
    }
}

void mnist_fp16_opt::thread_pad_temp2_0_address0() {
    if (((esl_seteq<1,1,1>(ap_enable_reg_pp10_iter3.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_flatten15_reg_24897_pp10_iter3_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage3.read()) && 
          esl_seteq<1,1,1>(ap_block_pp10_stage3.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, or_cond8_1_reg_24954_pp10_iter3_reg.read())) || 
         (esl_seteq<1,1,1>(ap_enable_reg_pp10_iter3.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_flatten15_reg_24897_pp10_iter3_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage3.read()) && 
          esl_seteq<1,1,1>(ap_block_pp10_stage3.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, or_cond8_1_reg_24954_pp10_iter3_reg.read())))) {
        pad_temp2_0_address0 =  (sc_lv<10>) (tmp_799_cast_fu_12843_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_enable_reg_pp10_iter3.read(), ap_const_logic_1) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage2.read()) && 
                esl_seteq<1,1,1>(ap_block_pp10_stage2.read(), ap_const_boolean_0))) {
        pad_temp2_0_address0 =  (sc_lv<10>) (tmp_798_cast_fu_12833_p1.read());
    } else if (((esl_seteq<1,1,1>(ap_enable_reg_pp10_iter3.read(), ap_const_logic_1) && 
                 esl_seteq<1,1,1>(ap_const_lv1_1, or_cond8_1_reg_24954_pp10_iter2_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage1.read()) && 
                 esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_flatten15_reg_24897_pp10_iter3_reg.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp10_stage1.read(), ap_const_boolean_0)) || 
                (esl_seteq<1,1,1>(ap_enable_reg_pp10_iter3.read(), ap_const_logic_1) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage1.read()) && 
                 esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_flatten15_reg_24897_pp10_iter3_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_lv1_0, or_cond8_1_reg_24954_pp10_iter2_reg.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp10_stage1.read(), ap_const_boolean_0)))) {
        pad_temp2_0_address0 =  (sc_lv<10>) (tmp_795_cast_fu_12783_p1.read());
    } else if (((esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_flatten15_reg_24897_pp10_iter2_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_lv1_1, or_cond8_1_reg_24954_pp10_iter2_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage7.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp10_iter2.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp10_stage7.read(), ap_const_boolean_0)) || 
                (esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_flatten15_reg_24897_pp10_iter2_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage7.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp10_iter2.read()) && 
                 esl_seteq<1,1,1>(ap_const_lv1_0, or_cond8_1_reg_24954_pp10_iter2_reg.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp10_stage7.read(), ap_const_boolean_0)))) {
        pad_temp2_0_address0 =  (sc_lv<10>) (tmp_793_cast_fu_12579_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage6.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp10_iter2.read()) && 
                esl_seteq<1,1,1>(ap_block_pp10_stage6.read(), ap_const_boolean_0))) {
        pad_temp2_0_address0 =  (sc_lv<10>) (tmp_792_cast_fu_12477_p1.read());
    } else if (((esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_flatten15_reg_24897_pp10_iter2_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_lv1_1, or_cond8_1_reg_24954_pp10_iter2_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage5.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp10_iter2.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp10_stage5.read(), ap_const_boolean_0)) || 
                (esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_flatten15_reg_24897_pp10_iter2_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage5.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp10_iter2.read()) && 
                 esl_seteq<1,1,1>(ap_const_lv1_0, or_cond8_1_reg_24954_pp10_iter2_reg.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp10_stage5.read(), ap_const_boolean_0)))) {
        pad_temp2_0_address0 =  (sc_lv<10>) (tmp_789_cast_fu_12355_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage4.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp10_iter2.read()) && 
                esl_seteq<1,1,1>(ap_block_pp10_stage4.read(), ap_const_boolean_0))) {
        pad_temp2_0_address0 =  (sc_lv<10>) (tmp_788_cast_fu_12253_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage0.read()) && 
                esl_seteq<1,1,1>(ap_enable_reg_pp10_iter0.read(), ap_const_logic_1) && 
                esl_seteq<1,1,1>(ap_block_pp10_stage0.read(), ap_const_boolean_0))) {
        pad_temp2_0_address0 =  (sc_lv<10>) (tmp_745_fu_11104_p1.read());
    } else {
        pad_temp2_0_address0 =  (sc_lv<10>) ("XXXXXXXXXX");
    }
}

void mnist_fp16_opt::thread_pad_temp2_0_address1() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp11_stage1.read()) && 
         esl_seteq<1,1,1>(ap_enable_reg_pp11_iter0.read(), ap_const_logic_1) && 
         esl_seteq<1,1,1>(ap_block_pp11_stage1.read(), ap_const_boolean_0))) {
        pad_temp2_0_address1 =  (sc_lv<10>) (tmp_888_fu_13240_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_enable_reg_pp10_iter3.read(), ap_const_logic_1) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage3.read()) && 
                esl_seteq<1,1,1>(ap_block_pp10_stage3.read(), ap_const_boolean_0))) {
        pad_temp2_0_address1 =  (sc_lv<10>) (tmp_800_cast_fu_12853_p1.read());
    } else if (((esl_seteq<1,1,1>(ap_enable_reg_pp10_iter3.read(), ap_const_logic_1) && 
                 esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_flatten15_reg_24897_pp10_iter3_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage2.read()) && 
                 esl_seteq<1,1,1>(ap_const_lv1_0, or_cond8_1_reg_24954_pp10_iter2_reg.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp10_stage2.read(), ap_const_boolean_0)) || 
                (esl_seteq<1,1,1>(ap_enable_reg_pp10_iter3.read(), ap_const_logic_1) && 
                 esl_seteq<1,1,1>(ap_const_lv1_1, or_cond8_1_reg_24954_pp10_iter2_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_flatten15_reg_24897_pp10_iter3_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage2.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp10_stage2.read(), ap_const_boolean_0)))) {
        pad_temp2_0_address1 =  (sc_lv<10>) (tmp_797_cast_fu_12823_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_enable_reg_pp10_iter3.read(), ap_const_logic_1) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage1.read()) && 
                esl_seteq<1,1,1>(ap_block_pp10_stage1.read(), ap_const_boolean_0))) {
        pad_temp2_0_address1 =  (sc_lv<10>) (tmp_796_cast_fu_12793_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage7.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp10_iter2.read()) && 
                esl_seteq<1,1,1>(ap_block_pp10_stage7.read(), ap_const_boolean_0))) {
        pad_temp2_0_address1 =  (sc_lv<10>) (tmp_794_cast_fu_12589_p1.read());
    } else if (((esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_flatten15_reg_24897_pp10_iter2_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_lv1_1, or_cond8_1_reg_24954_pp10_iter2_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage6.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp10_iter2.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp10_stage6.read(), ap_const_boolean_0)) || 
                (esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_flatten15_reg_24897_pp10_iter2_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage6.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp10_iter2.read()) && 
                 esl_seteq<1,1,1>(ap_const_lv1_0, or_cond8_1_reg_24954_pp10_iter2_reg.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp10_stage6.read(), ap_const_boolean_0)))) {
        pad_temp2_0_address1 =  (sc_lv<10>) (tmp_791_cast_fu_12467_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage5.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp10_iter2.read()) && 
                esl_seteq<1,1,1>(ap_block_pp10_stage5.read(), ap_const_boolean_0))) {
        pad_temp2_0_address1 =  (sc_lv<10>) (tmp_790_cast_fu_12365_p1.read());
    } else if (((esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_flatten15_reg_24897_pp10_iter2_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_lv1_1, or_cond8_1_reg_24954_pp10_iter2_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage4.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp10_iter2.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp10_stage4.read(), ap_const_boolean_0)) || 
                (esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_flatten15_reg_24897_pp10_iter2_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage4.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp10_iter2.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp10_stage4.read(), ap_const_boolean_0) && 
                 esl_seteq<1,1,1>(ap_const_lv1_0, or_cond8_1_reg_24954_pp10_iter2_reg.read())))) {
        pad_temp2_0_address1 =  (sc_lv<10>) (tmp_787_cast_fu_12243_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage0.read()) && 
                esl_seteq<1,1,1>(ap_enable_reg_pp10_iter0.read(), ap_const_logic_1) && 
                esl_seteq<1,1,1>(ap_block_pp10_stage0.read(), ap_const_boolean_0))) {
        pad_temp2_0_address1 =  (sc_lv<10>) (tmp_801_cast_fu_11115_p1.read());
    } else {
        pad_temp2_0_address1 =  (sc_lv<10>) ("XXXXXXXXXX");
    }
}

void mnist_fp16_opt::thread_pad_temp2_0_ce0() {
    if (((esl_seteq<1,1,1>(ap_enable_reg_pp10_iter3.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, or_cond8_1_reg_24954_pp10_iter2_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage1.read()) && 
          esl_seteq<1,1,1>(ap_block_pp10_stage1_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_flatten15_reg_24897_pp10_iter3_reg.read())) || 
         (esl_seteq<1,1,1>(ap_enable_reg_pp10_iter3.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage2.read()) && 
          esl_seteq<1,1,1>(ap_block_pp10_stage2_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage0.read()) && 
          esl_seteq<1,1,1>(ap_block_pp10_stage0_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_enable_reg_pp10_iter0.read(), ap_const_logic_1)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage4.read()) && 
          esl_seteq<1,1,1>(ap_block_pp10_stage4_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp10_iter2.read())) || 
         (esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_flatten15_reg_24897_pp10_iter2_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, or_cond8_1_reg_24954_pp10_iter2_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage5.read()) && 
          esl_seteq<1,1,1>(ap_block_pp10_stage5_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp10_iter2.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage6.read()) && 
          esl_seteq<1,1,1>(ap_block_pp10_stage6_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp10_iter2.read())) || 
         (esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_flatten15_reg_24897_pp10_iter2_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage5.read()) && 
          esl_seteq<1,1,1>(ap_block_pp10_stage5_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp10_iter2.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, or_cond8_1_reg_24954_pp10_iter2_reg.read())) || 
         (esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_flatten15_reg_24897_pp10_iter2_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, or_cond8_1_reg_24954_pp10_iter2_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage7.read()) && 
          esl_seteq<1,1,1>(ap_block_pp10_stage7_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp10_iter2.read())) || 
         (esl_seteq<1,1,1>(ap_enable_reg_pp10_iter3.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage1.read()) && 
          esl_seteq<1,1,1>(ap_block_pp10_stage1_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_flatten15_reg_24897_pp10_iter3_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, or_cond8_1_reg_24954_pp10_iter2_reg.read())) || 
         (esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_flatten15_reg_24897_pp10_iter2_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage7.read()) && 
          esl_seteq<1,1,1>(ap_block_pp10_stage7_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp10_iter2.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, or_cond8_1_reg_24954_pp10_iter2_reg.read())) || 
         (esl_seteq<1,1,1>(ap_enable_reg_pp10_iter3.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_flatten15_reg_24897_pp10_iter3_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage3.read()) && 
          esl_seteq<1,1,1>(ap_block_pp10_stage3_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, or_cond8_1_reg_24954_pp10_iter3_reg.read())) || 
         (esl_seteq<1,1,1>(ap_enable_reg_pp10_iter3.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_flatten15_reg_24897_pp10_iter3_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage3.read()) && 
          esl_seteq<1,1,1>(ap_block_pp10_stage3_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, or_cond8_1_reg_24954_pp10_iter3_reg.read())))) {
        pad_temp2_0_ce0 = ap_const_logic_1;
    } else {
        pad_temp2_0_ce0 = ap_const_logic_0;
    }
}

void mnist_fp16_opt::thread_pad_temp2_0_ce1() {
    if (((esl_seteq<1,1,1>(ap_enable_reg_pp10_iter3.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage1.read()) && 
          esl_seteq<1,1,1>(ap_block_pp10_stage1_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_enable_reg_pp10_iter3.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, or_cond8_1_reg_24954_pp10_iter2_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_flatten15_reg_24897_pp10_iter3_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage2.read()) && 
          esl_seteq<1,1,1>(ap_block_pp10_stage2_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage0.read()) && 
          esl_seteq<1,1,1>(ap_block_pp10_stage0_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_enable_reg_pp10_iter0.read(), ap_const_logic_1)) || 
         (esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_flatten15_reg_24897_pp10_iter2_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, or_cond8_1_reg_24954_pp10_iter2_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage4.read()) && 
          esl_seteq<1,1,1>(ap_block_pp10_stage4_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp10_iter2.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage5.read()) && 
          esl_seteq<1,1,1>(ap_block_pp10_stage5_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp10_iter2.read())) || 
         (esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_flatten15_reg_24897_pp10_iter2_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage4.read()) && 
          esl_seteq<1,1,1>(ap_block_pp10_stage4_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp10_iter2.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, or_cond8_1_reg_24954_pp10_iter2_reg.read())) || 
         (esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_flatten15_reg_24897_pp10_iter2_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, or_cond8_1_reg_24954_pp10_iter2_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage6.read()) && 
          esl_seteq<1,1,1>(ap_block_pp10_stage6_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp10_iter2.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage7.read()) && 
          esl_seteq<1,1,1>(ap_block_pp10_stage7_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp10_iter2.read())) || 
         (esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_flatten15_reg_24897_pp10_iter2_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage6.read()) && 
          esl_seteq<1,1,1>(ap_block_pp10_stage6_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp10_iter2.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, or_cond8_1_reg_24954_pp10_iter2_reg.read())) || 
         (esl_seteq<1,1,1>(ap_enable_reg_pp10_iter3.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage3.read()) && 
          esl_seteq<1,1,1>(ap_block_pp10_stage3_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_enable_reg_pp10_iter3.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_flatten15_reg_24897_pp10_iter3_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage2.read()) && 
          esl_seteq<1,1,1>(ap_block_pp10_stage2_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, or_cond8_1_reg_24954_pp10_iter2_reg.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp11_stage1.read()) && 
          esl_seteq<1,1,1>(ap_block_pp11_stage1_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_enable_reg_pp11_iter0.read(), ap_const_logic_1)))) {
        pad_temp2_0_ce1 = ap_const_logic_1;
    } else {
        pad_temp2_0_ce1 = ap_const_logic_0;
    }
}

void mnist_fp16_opt::thread_pad_temp2_0_d0() {
    if ((esl_seteq<1,1,1>(ap_enable_reg_pp10_iter3.read(), ap_const_logic_1) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage2.read()) && 
         esl_seteq<1,1,1>(ap_block_pp10_stage2.read(), ap_const_boolean_0))) {
        pad_temp2_0_d0 = ap_phi_reg_pp10_iter3_tmp_119_1_reg_3076.read();
    } else if (((esl_seteq<1,1,1>(ap_enable_reg_pp10_iter3.read(), ap_const_logic_1) && 
                 esl_seteq<1,1,1>(ap_const_lv1_1, or_cond8_1_reg_24954_pp10_iter2_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage1.read()) && 
                 esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_flatten15_reg_24897_pp10_iter3_reg.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp10_stage1.read(), ap_const_boolean_0)) || 
                (esl_seteq<1,1,1>(ap_enable_reg_pp10_iter3.read(), ap_const_logic_1) && 
                 esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_flatten15_reg_24897_pp10_iter3_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage3.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp10_stage3.read(), ap_const_boolean_0) && 
                 esl_seteq<1,1,1>(ap_const_lv1_1, or_cond8_1_reg_24954_pp10_iter3_reg.read())))) {
        pad_temp2_0_d0 = reg_4500.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage6.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp10_iter2.read()) && 
                esl_seteq<1,1,1>(ap_block_pp10_stage6.read(), ap_const_boolean_0))) {
        pad_temp2_0_d0 = ap_phi_mux_tmp_119_6_phi_fu_3042_p4.read();
    } else if (((esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_flatten15_reg_24897_pp10_iter2_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_lv1_1, or_cond8_1_reg_24954_pp10_iter2_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage5.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp10_iter2.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp10_stage5.read(), ap_const_boolean_0)) || 
                (esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_flatten15_reg_24897_pp10_iter2_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_lv1_1, or_cond8_1_reg_24954_pp10_iter2_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage7.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp10_iter2.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp10_stage7.read(), ap_const_boolean_0)))) {
        pad_temp2_0_d0 = pool1_0_q1.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage4.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp10_iter2.read()) && 
                esl_seteq<1,1,1>(ap_block_pp10_stage4.read(), ap_const_boolean_0))) {
        pad_temp2_0_d0 = ap_phi_mux_tmp_119_2_phi_fu_3016_p4.read();
    } else if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage0.read()) && 
                 esl_seteq<1,1,1>(ap_enable_reg_pp10_iter0.read(), ap_const_logic_1) && 
                 esl_seteq<1,1,1>(ap_block_pp10_stage0.read(), ap_const_boolean_0)) || 
                (esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_flatten15_reg_24897_pp10_iter2_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage5.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp10_iter2.read()) && 
                 esl_seteq<1,1,1>(ap_const_lv1_0, or_cond8_1_reg_24954_pp10_iter2_reg.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp10_stage5.read(), ap_const_boolean_0)) || 
                (esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_flatten15_reg_24897_pp10_iter2_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage7.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp10_iter2.read()) && 
                 esl_seteq<1,1,1>(ap_const_lv1_0, or_cond8_1_reg_24954_pp10_iter2_reg.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp10_stage7.read(), ap_const_boolean_0)) || 
                (esl_seteq<1,1,1>(ap_enable_reg_pp10_iter3.read(), ap_const_logic_1) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage1.read()) && 
                 esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_flatten15_reg_24897_pp10_iter3_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_lv1_0, or_cond8_1_reg_24954_pp10_iter2_reg.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp10_stage1.read(), ap_const_boolean_0)) || 
                (esl_seteq<1,1,1>(ap_enable_reg_pp10_iter3.read(), ap_const_logic_1) && 
                 esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_flatten15_reg_24897_pp10_iter3_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage3.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp10_stage3.read(), ap_const_boolean_0) && 
                 esl_seteq<1,1,1>(ap_const_lv1_0, or_cond8_1_reg_24954_pp10_iter3_reg.read())))) {
        pad_temp2_0_d0 = ap_const_lv32_0;
    } else {
        pad_temp2_0_d0 =  (sc_lv<32>) ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
    }
}

void mnist_fp16_opt::thread_pad_temp2_0_d1() {
    if ((esl_seteq<1,1,1>(ap_enable_reg_pp10_iter3.read(), ap_const_logic_1) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage3.read()) && 
         esl_seteq<1,1,1>(ap_block_pp10_stage3.read(), ap_const_boolean_0))) {
        pad_temp2_0_d1 = ap_phi_reg_pp10_iter3_tmp_119_3_reg_3088.read();
    } else if ((esl_seteq<1,1,1>(ap_enable_reg_pp10_iter3.read(), ap_const_logic_1) && 
                esl_seteq<1,1,1>(ap_const_lv1_1, or_cond8_1_reg_24954_pp10_iter2_reg.read()) && 
                esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_flatten15_reg_24897_pp10_iter3_reg.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage2.read()) && 
                esl_seteq<1,1,1>(ap_block_pp10_stage2.read(), ap_const_boolean_0))) {
        pad_temp2_0_d1 = reg_4500.read();
    } else if ((esl_seteq<1,1,1>(ap_enable_reg_pp10_iter3.read(), ap_const_logic_1) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage1.read()) && 
                esl_seteq<1,1,1>(ap_block_pp10_stage1.read(), ap_const_boolean_0))) {
        pad_temp2_0_d1 = ap_phi_reg_pp10_iter3_tmp_119_s_reg_3064.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage7.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp10_iter2.read()) && 
                esl_seteq<1,1,1>(ap_block_pp10_stage7.read(), ap_const_boolean_0))) {
        pad_temp2_0_d1 = ap_phi_mux_tmp_119_8_phi_fu_3055_p4.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage5.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp10_iter2.read()) && 
                esl_seteq<1,1,1>(ap_block_pp10_stage5.read(), ap_const_boolean_0))) {
        pad_temp2_0_d1 = ap_phi_mux_tmp_119_4_phi_fu_3029_p4.read();
    } else if (((esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_flatten15_reg_24897_pp10_iter2_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_lv1_1, or_cond8_1_reg_24954_pp10_iter2_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage4.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp10_iter2.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp10_stage4.read(), ap_const_boolean_0)) || 
                (esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_flatten15_reg_24897_pp10_iter2_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_lv1_1, or_cond8_1_reg_24954_pp10_iter2_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage6.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp10_iter2.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp10_stage6.read(), ap_const_boolean_0)))) {
        pad_temp2_0_d1 = pool1_0_q1.read();
    } else if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage0.read()) && 
                 esl_seteq<1,1,1>(ap_enable_reg_pp10_iter0.read(), ap_const_logic_1) && 
                 esl_seteq<1,1,1>(ap_block_pp10_stage0.read(), ap_const_boolean_0)) || 
                (esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_flatten15_reg_24897_pp10_iter2_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage4.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp10_iter2.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp10_stage4.read(), ap_const_boolean_0) && 
                 esl_seteq<1,1,1>(ap_const_lv1_0, or_cond8_1_reg_24954_pp10_iter2_reg.read())) || 
                (esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_flatten15_reg_24897_pp10_iter2_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage6.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp10_iter2.read()) && 
                 esl_seteq<1,1,1>(ap_const_lv1_0, or_cond8_1_reg_24954_pp10_iter2_reg.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp10_stage6.read(), ap_const_boolean_0)) || 
                (esl_seteq<1,1,1>(ap_enable_reg_pp10_iter3.read(), ap_const_logic_1) && 
                 esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_flatten15_reg_24897_pp10_iter3_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage2.read()) && 
                 esl_seteq<1,1,1>(ap_const_lv1_0, or_cond8_1_reg_24954_pp10_iter2_reg.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp10_stage2.read(), ap_const_boolean_0)))) {
        pad_temp2_0_d1 = ap_const_lv32_0;
    } else {
        pad_temp2_0_d1 =  (sc_lv<32>) ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
    }
}

void mnist_fp16_opt::thread_pad_temp2_0_we0() {
    if (((esl_seteq<1,1,1>(ap_enable_reg_pp10_iter3.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, or_cond8_1_reg_24954_pp10_iter2_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage1.read()) && 
          esl_seteq<1,1,1>(ap_block_pp10_stage1_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_flatten15_reg_24897_pp10_iter3_reg.read())) || 
         (esl_seteq<1,1,1>(ap_enable_reg_pp10_iter3.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage2.read()) && 
          esl_seteq<1,1,1>(ap_block_pp10_stage2_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage0.read()) && 
          esl_seteq<1,1,1>(ap_block_pp10_stage0_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_enable_reg_pp10_iter0.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_flatten15_fu_11034_p2.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage4.read()) && 
          esl_seteq<1,1,1>(ap_block_pp10_stage4_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp10_iter2.read())) || 
         (esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_flatten15_reg_24897_pp10_iter2_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, or_cond8_1_reg_24954_pp10_iter2_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage5.read()) && 
          esl_seteq<1,1,1>(ap_block_pp10_stage5_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp10_iter2.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage6.read()) && 
          esl_seteq<1,1,1>(ap_block_pp10_stage6_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp10_iter2.read())) || 
         (esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_flatten15_reg_24897_pp10_iter2_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage5.read()) && 
          esl_seteq<1,1,1>(ap_block_pp10_stage5_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp10_iter2.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, or_cond8_1_reg_24954_pp10_iter2_reg.read())) || 
         (esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_flatten15_reg_24897_pp10_iter2_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, or_cond8_1_reg_24954_pp10_iter2_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage7.read()) && 
          esl_seteq<1,1,1>(ap_block_pp10_stage7_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp10_iter2.read())) || 
         (esl_seteq<1,1,1>(ap_enable_reg_pp10_iter3.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage1.read()) && 
          esl_seteq<1,1,1>(ap_block_pp10_stage1_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_flatten15_reg_24897_pp10_iter3_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, or_cond8_1_reg_24954_pp10_iter2_reg.read())) || 
         (esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_flatten15_reg_24897_pp10_iter2_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage7.read()) && 
          esl_seteq<1,1,1>(ap_block_pp10_stage7_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp10_iter2.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, or_cond8_1_reg_24954_pp10_iter2_reg.read())) || 
         (esl_seteq<1,1,1>(ap_enable_reg_pp10_iter3.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_flatten15_reg_24897_pp10_iter3_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage3.read()) && 
          esl_seteq<1,1,1>(ap_block_pp10_stage3_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, or_cond8_1_reg_24954_pp10_iter3_reg.read())) || 
         (esl_seteq<1,1,1>(ap_enable_reg_pp10_iter3.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_flatten15_reg_24897_pp10_iter3_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage3.read()) && 
          esl_seteq<1,1,1>(ap_block_pp10_stage3_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, or_cond8_1_reg_24954_pp10_iter3_reg.read())))) {
        pad_temp2_0_we0 = ap_const_logic_1;
    } else {
        pad_temp2_0_we0 = ap_const_logic_0;
    }
}

void mnist_fp16_opt::thread_pad_temp2_0_we1() {
    if (((esl_seteq<1,1,1>(ap_enable_reg_pp10_iter3.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage1.read()) && 
          esl_seteq<1,1,1>(ap_block_pp10_stage1_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_enable_reg_pp10_iter3.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, or_cond8_1_reg_24954_pp10_iter2_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_flatten15_reg_24897_pp10_iter3_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage2.read()) && 
          esl_seteq<1,1,1>(ap_block_pp10_stage2_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage0.read()) && 
          esl_seteq<1,1,1>(ap_block_pp10_stage0_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_enable_reg_pp10_iter0.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_flatten15_fu_11034_p2.read())) || 
         (esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_flatten15_reg_24897_pp10_iter2_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, or_cond8_1_reg_24954_pp10_iter2_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage4.read()) && 
          esl_seteq<1,1,1>(ap_block_pp10_stage4_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp10_iter2.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage5.read()) && 
          esl_seteq<1,1,1>(ap_block_pp10_stage5_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp10_iter2.read())) || 
         (esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_flatten15_reg_24897_pp10_iter2_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage4.read()) && 
          esl_seteq<1,1,1>(ap_block_pp10_stage4_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp10_iter2.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, or_cond8_1_reg_24954_pp10_iter2_reg.read())) || 
         (esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_flatten15_reg_24897_pp10_iter2_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, or_cond8_1_reg_24954_pp10_iter2_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage6.read()) && 
          esl_seteq<1,1,1>(ap_block_pp10_stage6_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp10_iter2.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage7.read()) && 
          esl_seteq<1,1,1>(ap_block_pp10_stage7_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp10_iter2.read())) || 
         (esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_flatten15_reg_24897_pp10_iter2_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage6.read()) && 
          esl_seteq<1,1,1>(ap_block_pp10_stage6_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp10_iter2.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, or_cond8_1_reg_24954_pp10_iter2_reg.read())) || 
         (esl_seteq<1,1,1>(ap_enable_reg_pp10_iter3.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_flatten15_reg_24897_pp10_iter3_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage2.read()) && 
          esl_seteq<1,1,1>(ap_block_pp10_stage2_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, or_cond8_1_reg_24954_pp10_iter2_reg.read())) || 
         (esl_seteq<1,1,1>(ap_enable_reg_pp10_iter3.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_flatten15_reg_24897_pp10_iter3_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage3.read()) && 
          esl_seteq<1,1,1>(ap_block_pp10_stage3_11001.read(), ap_const_boolean_0)))) {
        pad_temp2_0_we1 = ap_const_logic_1;
    } else {
        pad_temp2_0_we1 = ap_const_logic_0;
    }
}

void mnist_fp16_opt::thread_pad_temp3_0_address0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp14_stage0.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp14_iter1.read()) && 
         esl_seteq<1,1,1>(ap_block_pp14_stage0.read(), ap_const_boolean_0))) {
        pad_temp3_0_address0 =  (sc_lv<11>) (tmp_1026_fu_15007_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_block_pp13_stage0.read(), ap_const_boolean_0) && 
                esl_seteq<1,1,1>(ap_enable_reg_pp13_iter29.read(), ap_const_logic_1))) {
        pad_temp3_0_address0 =  (sc_lv<11>) (tmp_981_cast_fu_14615_p1.read());
    } else {
        pad_temp3_0_address0 =  (sc_lv<11>) ("XXXXXXXXXXX");
    }
}

void mnist_fp16_opt::thread_pad_temp3_0_ce0() {
    if (((esl_seteq<1,1,1>(ap_block_pp13_stage0_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_enable_reg_pp13_iter29.read(), ap_const_logic_1)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp14_stage0.read()) && 
          esl_seteq<1,1,1>(ap_block_pp14_stage0_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp14_iter1.read())))) {
        pad_temp3_0_ce0 = ap_const_logic_1;
    } else {
        pad_temp3_0_ce0 = ap_const_logic_0;
    }
}

void mnist_fp16_opt::thread_pad_temp3_0_we0() {
    if ((esl_seteq<1,1,1>(ap_block_pp13_stage0_11001.read(), ap_const_boolean_0) && 
         esl_seteq<1,1,1>(ap_enable_reg_pp13_iter29.read(), ap_const_logic_1) && 
         esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_flatten23_reg_25672_pp13_iter28_reg.read()))) {
        pad_temp3_0_we0 = ap_const_logic_1;
    } else {
        pad_temp3_0_we0 = ap_const_logic_0;
    }
}

void mnist_fp16_opt::thread_pad_temp4_0_address0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp18_stage2.read()) && 
         esl_seteq<1,1,1>(ap_enable_reg_pp18_iter0.read(), ap_const_logic_1) && 
         esl_seteq<1,1,1>(ap_block_pp18_stage2.read(), ap_const_boolean_0))) {
        pad_temp4_0_address0 =  (sc_lv<10>) (tmp_1167_cast_fu_18412_p1.read());
    } else if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp17_stage0.read()) && 
                 esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_flatten33_reg_26492_pp17_iter3_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_lv1_1, tmp_585_reg_26548_pp17_iter3_reg.read()) && 
                 esl_seteq<1,1,1>(ap_enable_reg_pp17_iter4.read(), ap_const_logic_1) && 
                 esl_seteq<1,1,1>(ap_block_pp17_stage0.read(), ap_const_boolean_0)) || 
                (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp17_stage0.read()) && 
                 esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_flatten33_reg_26492_pp17_iter3_reg.read()) && 
                 esl_seteq<1,1,1>(ap_enable_reg_pp17_iter4.read(), ap_const_logic_1) && 
                 esl_seteq<1,1,1>(ap_block_pp17_stage0.read(), ap_const_boolean_0) && 
                 esl_seteq<1,1,1>(ap_const_lv1_0, tmp_585_reg_26548_pp17_iter3_reg.read())))) {
        pad_temp4_0_address0 =  (sc_lv<10>) (tmp_1099_cast_fu_17966_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp17_stage4.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp17_iter3.read()) && 
                esl_seteq<1,1,1>(ap_block_pp17_stage4.read(), ap_const_boolean_0))) {
        pad_temp4_0_address0 =  (sc_lv<10>) (tmp_1098_cast_fu_17874_p1.read());
    } else if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp17_stage3.read()) && 
                 esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_flatten33_reg_26492_pp17_iter3_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_lv1_1, tmp_585_reg_26548_pp17_iter3_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp17_iter3.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp17_stage3.read(), ap_const_boolean_0)) || 
                (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp17_stage3.read()) && 
                 esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_flatten33_reg_26492_pp17_iter3_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp17_iter3.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp17_stage3.read(), ap_const_boolean_0) && 
                 esl_seteq<1,1,1>(ap_const_lv1_0, tmp_585_reg_26548_pp17_iter3_reg.read())))) {
        pad_temp4_0_address0 =  (sc_lv<10>) (tmp_1095_cast_fu_17768_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp17_stage1.read()) && 
                esl_seteq<1,1,1>(ap_enable_reg_pp17_iter0.read(), ap_const_logic_1) && 
                esl_seteq<1,1,1>(ap_block_pp17_stage1.read(), ap_const_boolean_0))) {
        pad_temp4_0_address0 =  (sc_lv<10>) (tmp_1102_cast_fu_17099_p1.read());
    } else {
        pad_temp4_0_address0 =  (sc_lv<10>) ("XXXXXXXXXX");
    }
}

void mnist_fp16_opt::thread_pad_temp4_0_address1() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp17_stage2.read()) && 
         esl_seteq<1,1,1>(ap_enable_reg_pp17_iter4.read(), ap_const_logic_1) && 
         esl_seteq<1,1,1>(ap_block_pp17_stage2.read(), ap_const_boolean_0))) {
        pad_temp4_0_address1 = pad_temp4_0_addr_7_reg_26786.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp17_stage0.read()) && 
                esl_seteq<1,1,1>(ap_enable_reg_pp17_iter4.read(), ap_const_logic_1) && 
                esl_seteq<1,1,1>(ap_block_pp17_stage0.read(), ap_const_boolean_0))) {
        pad_temp4_0_address1 =  (sc_lv<10>) (tmp_1100_cast_fu_17976_p1.read());
    } else if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp17_stage4.read()) && 
                 esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_flatten33_reg_26492_pp17_iter3_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_lv1_1, tmp_585_reg_26548_pp17_iter3_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp17_iter3.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp17_stage4.read(), ap_const_boolean_0)) || 
                (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp17_stage4.read()) && 
                 esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_flatten33_reg_26492_pp17_iter3_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp17_iter3.read()) && 
                 esl_seteq<1,1,1>(ap_const_lv1_0, tmp_585_reg_26548_pp17_iter3_reg.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp17_stage4.read(), ap_const_boolean_0)))) {
        pad_temp4_0_address1 =  (sc_lv<10>) (tmp_1097_cast_fu_17864_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp17_stage3.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp17_iter3.read()) && 
                esl_seteq<1,1,1>(ap_block_pp17_stage3.read(), ap_const_boolean_0))) {
        pad_temp4_0_address1 =  (sc_lv<10>) (tmp_1096_cast_fu_17778_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp17_stage1.read()) && 
                esl_seteq<1,1,1>(ap_enable_reg_pp17_iter0.read(), ap_const_logic_1) && 
                esl_seteq<1,1,1>(ap_block_pp17_stage1.read(), ap_const_boolean_0))) {
        pad_temp4_0_address1 =  (sc_lv<10>) (tmp_1094_cast_fu_17088_p1.read());
    } else {
        pad_temp4_0_address1 =  (sc_lv<10>) ("XXXXXXXXXX");
    }
}

void mnist_fp16_opt::thread_pad_temp4_0_ce0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp17_stage1.read()) && 
          esl_seteq<1,1,1>(ap_enable_reg_pp17_iter0.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_block_pp17_stage1_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp17_stage3.read()) && 
          esl_seteq<1,1,1>(ap_block_pp17_stage3_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_flatten33_reg_26492_pp17_iter3_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, tmp_585_reg_26548_pp17_iter3_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp17_iter3.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp17_stage4.read()) && 
          esl_seteq<1,1,1>(ap_block_pp17_stage4_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp17_iter3.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp17_stage3.read()) && 
          esl_seteq<1,1,1>(ap_block_pp17_stage3_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_flatten33_reg_26492_pp17_iter3_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp17_iter3.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, tmp_585_reg_26548_pp17_iter3_reg.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp17_stage0.read()) && 
          esl_seteq<1,1,1>(ap_block_pp17_stage0_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_flatten33_reg_26492_pp17_iter3_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, tmp_585_reg_26548_pp17_iter3_reg.read()) && 
          esl_seteq<1,1,1>(ap_enable_reg_pp17_iter4.read(), ap_const_logic_1)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp17_stage0.read()) && 
          esl_seteq<1,1,1>(ap_block_pp17_stage0_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_flatten33_reg_26492_pp17_iter3_reg.read()) && 
          esl_seteq<1,1,1>(ap_enable_reg_pp17_iter4.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, tmp_585_reg_26548_pp17_iter3_reg.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp18_stage2.read()) && 
          esl_seteq<1,1,1>(ap_block_pp18_stage2_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_enable_reg_pp18_iter0.read(), ap_const_logic_1)))) {
        pad_temp4_0_ce0 = ap_const_logic_1;
    } else {
        pad_temp4_0_ce0 = ap_const_logic_0;
    }
}

void mnist_fp16_opt::thread_pad_temp4_0_ce1() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp17_stage1.read()) && 
          esl_seteq<1,1,1>(ap_enable_reg_pp17_iter0.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_block_pp17_stage1_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp17_stage3.read()) && 
          esl_seteq<1,1,1>(ap_block_pp17_stage3_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp17_iter3.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp17_stage4.read()) && 
          esl_seteq<1,1,1>(ap_block_pp17_stage4_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_flatten33_reg_26492_pp17_iter3_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, tmp_585_reg_26548_pp17_iter3_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp17_iter3.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp17_stage0.read()) && 
          esl_seteq<1,1,1>(ap_block_pp17_stage0_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_enable_reg_pp17_iter4.read(), ap_const_logic_1)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp17_stage4.read()) && 
          esl_seteq<1,1,1>(ap_block_pp17_stage4_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_flatten33_reg_26492_pp17_iter3_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp17_iter3.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, tmp_585_reg_26548_pp17_iter3_reg.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp17_stage2.read()) && 
          esl_seteq<1,1,1>(ap_block_pp17_stage2_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_enable_reg_pp17_iter4.read(), ap_const_logic_1)))) {
        pad_temp4_0_ce1 = ap_const_logic_1;
    } else {
        pad_temp4_0_ce1 = ap_const_logic_0;
    }
}

void mnist_fp16_opt::thread_pad_temp4_0_d0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp17_stage0.read()) && 
         esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_flatten33_reg_26492_pp17_iter3_reg.read()) && 
         esl_seteq<1,1,1>(ap_const_lv1_1, tmp_585_reg_26548_pp17_iter3_reg.read()) && 
         esl_seteq<1,1,1>(ap_enable_reg_pp17_iter4.read(), ap_const_logic_1) && 
         esl_seteq<1,1,1>(ap_block_pp17_stage0.read(), ap_const_boolean_0))) {
        pad_temp4_0_d0 = pool2_0_q1.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp17_stage4.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp17_iter3.read()) && 
                esl_seteq<1,1,1>(ap_block_pp17_stage4.read(), ap_const_boolean_0))) {
        pad_temp4_0_d0 = ap_phi_mux_tmp_297_4_phi_fu_3681_p4.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp17_stage3.read()) && 
                esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_flatten33_reg_26492_pp17_iter3_reg.read()) && 
                esl_seteq<1,1,1>(ap_const_lv1_1, tmp_585_reg_26548_pp17_iter3_reg.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp17_iter3.read()) && 
                esl_seteq<1,1,1>(ap_block_pp17_stage3.read(), ap_const_boolean_0))) {
        pad_temp4_0_d0 = pool2_0_q0.read();
    } else if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp17_stage1.read()) && 
                 esl_seteq<1,1,1>(ap_enable_reg_pp17_iter0.read(), ap_const_logic_1) && 
                 esl_seteq<1,1,1>(ap_block_pp17_stage1.read(), ap_const_boolean_0)) || 
                (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp17_stage3.read()) && 
                 esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_flatten33_reg_26492_pp17_iter3_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp17_iter3.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp17_stage3.read(), ap_const_boolean_0) && 
                 esl_seteq<1,1,1>(ap_const_lv1_0, tmp_585_reg_26548_pp17_iter3_reg.read())) || 
                (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp17_stage0.read()) && 
                 esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_flatten33_reg_26492_pp17_iter3_reg.read()) && 
                 esl_seteq<1,1,1>(ap_enable_reg_pp17_iter4.read(), ap_const_logic_1) && 
                 esl_seteq<1,1,1>(ap_block_pp17_stage0.read(), ap_const_boolean_0) && 
                 esl_seteq<1,1,1>(ap_const_lv1_0, tmp_585_reg_26548_pp17_iter3_reg.read())))) {
        pad_temp4_0_d0 = ap_const_lv32_0;
    } else {
        pad_temp4_0_d0 =  (sc_lv<32>) ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
    }
}

void mnist_fp16_opt::thread_pad_temp4_0_d1() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp17_stage2.read()) && 
         esl_seteq<1,1,1>(ap_enable_reg_pp17_iter4.read(), ap_const_logic_1) && 
         esl_seteq<1,1,1>(ap_block_pp17_stage2.read(), ap_const_boolean_0))) {
        pad_temp4_0_d1 = ap_phi_reg_pp17_iter4_tmp_297_7_reg_3703.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp17_stage0.read()) && 
                esl_seteq<1,1,1>(ap_enable_reg_pp17_iter4.read(), ap_const_logic_1) && 
                esl_seteq<1,1,1>(ap_block_pp17_stage0.read(), ap_const_boolean_0))) {
        pad_temp4_0_d1 = ap_phi_mux_tmp_297_6_phi_fu_3694_p4.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp17_stage4.read()) && 
                esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_flatten33_reg_26492_pp17_iter3_reg.read()) && 
                esl_seteq<1,1,1>(ap_const_lv1_1, tmp_585_reg_26548_pp17_iter3_reg.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp17_iter3.read()) && 
                esl_seteq<1,1,1>(ap_block_pp17_stage4.read(), ap_const_boolean_0))) {
        pad_temp4_0_d1 = pool2_0_q1.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp17_stage3.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp17_iter3.read()) && 
                esl_seteq<1,1,1>(ap_block_pp17_stage3.read(), ap_const_boolean_0))) {
        pad_temp4_0_d1 = ap_phi_mux_tmp_297_2_phi_fu_3668_p4.read();
    } else if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp17_stage1.read()) && 
                 esl_seteq<1,1,1>(ap_enable_reg_pp17_iter0.read(), ap_const_logic_1) && 
                 esl_seteq<1,1,1>(ap_block_pp17_stage1.read(), ap_const_boolean_0)) || 
                (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp17_stage4.read()) && 
                 esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_flatten33_reg_26492_pp17_iter3_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp17_iter3.read()) && 
                 esl_seteq<1,1,1>(ap_const_lv1_0, tmp_585_reg_26548_pp17_iter3_reg.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp17_stage4.read(), ap_const_boolean_0)))) {
        pad_temp4_0_d1 = ap_const_lv32_0;
    } else {
        pad_temp4_0_d1 =  (sc_lv<32>) ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
    }
}

void mnist_fp16_opt::thread_pad_temp4_0_we0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp17_stage3.read()) && 
          esl_seteq<1,1,1>(ap_block_pp17_stage3_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_flatten33_reg_26492_pp17_iter3_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, tmp_585_reg_26548_pp17_iter3_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp17_iter3.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp17_stage4.read()) && 
          esl_seteq<1,1,1>(ap_block_pp17_stage4_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp17_iter3.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp17_stage3.read()) && 
          esl_seteq<1,1,1>(ap_block_pp17_stage3_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_flatten33_reg_26492_pp17_iter3_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp17_iter3.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, tmp_585_reg_26548_pp17_iter3_reg.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp17_stage0.read()) && 
          esl_seteq<1,1,1>(ap_block_pp17_stage0_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_flatten33_reg_26492_pp17_iter3_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, tmp_585_reg_26548_pp17_iter3_reg.read()) && 
          esl_seteq<1,1,1>(ap_enable_reg_pp17_iter4.read(), ap_const_logic_1)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp17_stage1.read()) && 
          esl_seteq<1,1,1>(ap_enable_reg_pp17_iter0.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_block_pp17_stage1_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_flatten33_reg_26492.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp17_stage0.read()) && 
          esl_seteq<1,1,1>(ap_block_pp17_stage0_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_flatten33_reg_26492_pp17_iter3_reg.read()) && 
          esl_seteq<1,1,1>(ap_enable_reg_pp17_iter4.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, tmp_585_reg_26548_pp17_iter3_reg.read())))) {
        pad_temp4_0_we0 = ap_const_logic_1;
    } else {
        pad_temp4_0_we0 = ap_const_logic_0;
    }
}

void mnist_fp16_opt::thread_pad_temp4_0_we1() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp17_stage3.read()) && 
          esl_seteq<1,1,1>(ap_block_pp17_stage3_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp17_iter3.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp17_stage4.read()) && 
          esl_seteq<1,1,1>(ap_block_pp17_stage4_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_flatten33_reg_26492_pp17_iter3_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, tmp_585_reg_26548_pp17_iter3_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp17_iter3.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp17_stage4.read()) && 
          esl_seteq<1,1,1>(ap_block_pp17_stage4_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_flatten33_reg_26492_pp17_iter3_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp17_iter3.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, tmp_585_reg_26548_pp17_iter3_reg.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp17_stage1.read()) && 
          esl_seteq<1,1,1>(ap_enable_reg_pp17_iter0.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_block_pp17_stage1_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_flatten33_reg_26492.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp17_stage0.read()) && 
          esl_seteq<1,1,1>(ap_block_pp17_stage0_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_flatten33_reg_26492_pp17_iter3_reg.read()) && 
          esl_seteq<1,1,1>(ap_enable_reg_pp17_iter4.read(), ap_const_logic_1)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp17_stage2.read()) && 
          esl_seteq<1,1,1>(ap_block_pp17_stage2_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_enable_reg_pp17_iter4.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_flatten33_reg_26492_pp17_iter4_reg.read())))) {
        pad_temp4_0_we1 = ap_const_logic_1;
    } else {
        pad_temp4_0_we1 = ap_const_logic_0;
    }
}

void mnist_fp16_opt::thread_pad_temp5_0_address0() {
    if ((esl_seteq<1,1,1>(ap_block_pp21_stage0.read(), ap_const_boolean_0) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp21_iter2.read()))) {
        pad_temp5_0_address0 =  (sc_lv<11>) (tmp_1251_cast_fu_20149_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_block_pp20_stage0.read(), ap_const_boolean_0) && 
                esl_seteq<1,1,1>(ap_enable_reg_pp20_iter28.read(), ap_const_logic_1))) {
        pad_temp5_0_address0 =  (sc_lv<11>) (tmp_1230_cast_fu_19710_p1.read());
    } else {
        pad_temp5_0_address0 =  (sc_lv<11>) ("XXXXXXXXXXX");
    }
}

void mnist_fp16_opt::thread_pad_temp5_0_ce0() {
    if (((esl_seteq<1,1,1>(ap_block_pp20_stage0_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_enable_reg_pp20_iter28.read(), ap_const_logic_1)) || 
         (esl_seteq<1,1,1>(ap_block_pp21_stage0_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp21_iter2.read())))) {
        pad_temp5_0_ce0 = ap_const_logic_1;
    } else {
        pad_temp5_0_ce0 = ap_const_logic_0;
    }
}

void mnist_fp16_opt::thread_pad_temp5_0_we0() {
    if ((esl_seteq<1,1,1>(ap_block_pp20_stage0_11001.read(), ap_const_boolean_0) && 
         esl_seteq<1,1,1>(ap_enable_reg_pp20_iter28.read(), ap_const_logic_1) && 
         esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_flatten41_reg_27076_pp20_iter27_reg.read()))) {
        pad_temp5_0_we0 = ap_const_logic_1;
    } else {
        pad_temp5_0_we0 = ap_const_logic_0;
    }
}

void mnist_fp16_opt::thread_pad_temp_0_0_address0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp4_stage1.read()) && 
         esl_seteq<1,1,1>(ap_enable_reg_pp4_iter0.read(), ap_const_logic_1) && 
         esl_seteq<1,1,1>(ap_block_pp4_stage1.read(), ap_const_boolean_0))) {
        pad_temp_0_0_address0 =  (sc_lv<10>) (tmp_426_cast_fu_7166_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_enable_reg_pp3_iter2.read(), ap_const_logic_1) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage4.read()) && 
                esl_seteq<1,1,1>(ap_block_pp3_stage4.read(), ap_const_boolean_0))) {
        pad_temp_0_0_address0 =  (sc_lv<10>) (tmp_147_cast_fu_6870_p1.read());
    } else if (((esl_seteq<1,1,1>(ap_enable_reg_pp3_iter2.read(), ap_const_logic_1) && 
                 esl_seteq<1,1,1>(ap_const_lv1_0, tmp_1309_reg_23334_pp3_iter1_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage3.read()) && 
                 esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter2_reg.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp3_stage3.read(), ap_const_boolean_0)) || 
                (esl_seteq<1,1,1>(ap_enable_reg_pp3_iter2.read(), ap_const_logic_1) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage3.read()) && 
                 esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter2_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_lv1_1, tmp_1309_reg_23334_pp3_iter1_reg.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp3_stage3.read(), ap_const_boolean_0)))) {
        pad_temp_0_0_address0 =  (sc_lv<10>) (tmp_136_cast_fu_6840_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_enable_reg_pp3_iter2.read(), ap_const_logic_1) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage2.read()) && 
                esl_seteq<1,1,1>(ap_block_pp3_stage2.read(), ap_const_boolean_0))) {
        pad_temp_0_0_address0 =  (sc_lv<10>) (tmp_27_cast_fu_6810_p1.read());
    } else if (((esl_seteq<1,1,1>(ap_enable_reg_pp3_iter2.read(), ap_const_logic_1) && 
                 esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter2_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage1.read()) && 
                 esl_seteq<1,1,1>(ap_const_lv1_0, tmp_87_reg_23223_pp3_iter2_reg.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp3_stage1.read(), ap_const_boolean_0)) || 
                (esl_seteq<1,1,1>(ap_enable_reg_pp3_iter2.read(), ap_const_logic_1) && 
                 esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter2_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage1.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp3_stage1.read(), ap_const_boolean_0) && 
                 esl_seteq<1,1,1>(ap_const_lv1_1, tmp_87_reg_23223_pp3_iter2_reg.read())))) {
        pad_temp_0_0_address0 =  (sc_lv<10>) (tmp_21_cast_fu_6728_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage14.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read()) && 
                esl_seteq<1,1,1>(ap_block_pp3_stage14.read(), ap_const_boolean_0))) {
        pad_temp_0_0_address0 =  (sc_lv<10>) (tmp_134_cast_fu_6622_p1.read());
    } else if (((esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter1_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage13.read()) && 
                 esl_seteq<1,1,1>(ap_const_lv1_0, tmp_1013_reg_23326_pp3_iter1_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp3_stage13.read(), ap_const_boolean_0)) || 
                (esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter1_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage13.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp3_stage13.read(), ap_const_boolean_0) && 
                 esl_seteq<1,1,1>(ap_const_lv1_1, tmp_1013_reg_23326_pp3_iter1_reg.read())))) {
        pad_temp_0_0_address0 =  (sc_lv<10>) (tmp_111_cast_fu_6542_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage12.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read()) && 
                esl_seteq<1,1,1>(ap_block_pp3_stage12.read(), ap_const_boolean_0))) {
        pad_temp_0_0_address0 =  (sc_lv<10>) (tmp_105_cast_fu_6482_p1.read());
    } else if (((esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter1_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage11.read()) && 
                 esl_seteq<1,1,1>(ap_const_lv1_0, tmp_831_reg_23318_pp3_iter1_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp3_stage11.read(), ap_const_boolean_0)) || 
                (esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter1_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage11.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp3_stage11.read(), ap_const_boolean_0) && 
                 esl_seteq<1,1,1>(ap_const_lv1_1, tmp_831_reg_23318_pp3_iter1_reg.read())))) {
        pad_temp_0_0_address0 =  (sc_lv<10>) (tmp_83_cast_fu_6402_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage10.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read()) && 
                esl_seteq<1,1,1>(ap_block_pp3_stage10.read(), ap_const_boolean_0))) {
        pad_temp_0_0_address0 =  (sc_lv<10>) (tmp_80_cast_fu_6342_p1.read());
    } else if (((esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter1_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage9.read()) && 
                 esl_seteq<1,1,1>(ap_const_lv1_0, tmp_729_reg_23310_pp3_iter1_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp3_stage9.read(), ap_const_boolean_0)) || 
                (esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter1_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage9.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp3_stage9.read(), ap_const_boolean_0) && 
                 esl_seteq<1,1,1>(ap_const_lv1_1, tmp_729_reg_23310_pp3_iter1_reg.read())))) {
        pad_temp_0_0_address0 =  (sc_lv<10>) (tmp_64_cast_fu_6262_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage8.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read()) && 
                esl_seteq<1,1,1>(ap_block_pp3_stage8.read(), ap_const_boolean_0))) {
        pad_temp_0_0_address0 =  (sc_lv<10>) (tmp_62_cast_fu_6202_p1.read());
    } else if (((esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter1_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage7.read()) && 
                 esl_seteq<1,1,1>(ap_const_lv1_0, tmp_506_reg_23286_pp3_iter1_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp3_stage7.read(), ap_const_boolean_0)) || 
                (esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter1_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage7.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp3_stage7.read(), ap_const_boolean_0) && 
                 esl_seteq<1,1,1>(ap_const_lv1_1, tmp_506_reg_23286_pp3_iter1_reg.read())))) {
        pad_temp_0_0_address0 =  (sc_lv<10>) (tmp_53_cast_fu_6122_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage6.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read()) && 
                esl_seteq<1,1,1>(ap_block_pp3_stage6.read(), ap_const_boolean_0))) {
        pad_temp_0_0_address0 =  (sc_lv<10>) (tmp_49_cast_fu_6062_p1.read());
    } else if (((esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter1_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage5.read()) && 
                 esl_seteq<1,1,1>(ap_const_lv1_0, tmp_248_reg_23232_pp3_iter1_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp3_stage5.read(), ap_const_boolean_0)) || 
                (esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter1_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage5.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp3_stage5.read(), ap_const_boolean_0) && 
                 esl_seteq<1,1,1>(ap_const_lv1_1, tmp_248_reg_23232_pp3_iter1_reg.read())))) {
        pad_temp_0_0_address0 =  (sc_lv<10>) (tmp_28_cast_fu_5982_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage0.read()) && 
                esl_seteq<1,1,1>(ap_enable_reg_pp3_iter0.read(), ap_const_logic_1) && 
                esl_seteq<1,1,1>(ap_block_pp3_stage0.read(), ap_const_boolean_0))) {
        pad_temp_0_0_address0 =  (sc_lv<10>) (tmp_19_cast_fu_4690_p1.read());
    } else {
        pad_temp_0_0_address0 =  (sc_lv<10>) ("XXXXXXXXXX");
    }
}

void mnist_fp16_opt::thread_pad_temp_0_0_address1() {
    if (((esl_seteq<1,1,1>(ap_enable_reg_pp3_iter2.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter2_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage4.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage4.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, tmp_1332_reg_23338_pp3_iter2_reg.read())) || 
         (esl_seteq<1,1,1>(ap_enable_reg_pp3_iter2.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter2_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage4.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage4.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, tmp_1332_reg_23338_pp3_iter2_reg.read())))) {
        pad_temp_0_0_address1 =  (sc_lv<10>) (tmp_141_cast_fu_6860_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_enable_reg_pp3_iter2.read(), ap_const_logic_1) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage3.read()) && 
                esl_seteq<1,1,1>(ap_block_pp3_stage3.read(), ap_const_boolean_0))) {
        pad_temp_0_0_address1 =  (sc_lv<10>) (tmp_138_cast_fu_6850_p1.read());
    } else if (((esl_seteq<1,1,1>(ap_enable_reg_pp3_iter2.read(), ap_const_logic_1) && 
                 esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter2_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage2.read()) && 
                 esl_seteq<1,1,1>(ap_const_lv1_0, tmp_150_reg_23277_pp3_iter1_reg.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp3_stage2.read(), ap_const_boolean_0)) || 
                (esl_seteq<1,1,1>(ap_enable_reg_pp3_iter2.read(), ap_const_logic_1) && 
                 esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter2_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage2.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp3_stage2.read(), ap_const_boolean_0) && 
                 esl_seteq<1,1,1>(ap_const_lv1_1, tmp_150_reg_23277_pp3_iter1_reg.read())))) {
        pad_temp_0_0_address1 =  (sc_lv<10>) (tmp_26_cast_fu_6800_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_enable_reg_pp3_iter2.read(), ap_const_logic_1) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage1.read()) && 
                esl_seteq<1,1,1>(ap_block_pp3_stage1.read(), ap_const_boolean_0))) {
        pad_temp_0_0_address1 =  (sc_lv<10>) (tmp_23_cast_fu_6738_p1.read());
    } else if (((esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter1_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage14.read()) && 
                 esl_seteq<1,1,1>(ap_const_lv1_0, tmp_1204_reg_23330_pp3_iter1_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp3_stage14.read(), ap_const_boolean_0)) || 
                (esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter1_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage14.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp3_stage14.read(), ap_const_boolean_0) && 
                 esl_seteq<1,1,1>(ap_const_lv1_1, tmp_1204_reg_23330_pp3_iter1_reg.read())))) {
        pad_temp_0_0_address1 =  (sc_lv<10>) (tmp_121_cast_fu_6612_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage13.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read()) && 
                esl_seteq<1,1,1>(ap_block_pp3_stage13.read(), ap_const_boolean_0))) {
        pad_temp_0_0_address1 =  (sc_lv<10>) (tmp_119_cast_fu_6552_p1.read());
    } else if (((esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter1_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage12.read()) && 
                 esl_seteq<1,1,1>(ap_const_lv1_0, tmp_923_reg_23322_pp3_iter1_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp3_stage12.read(), ap_const_boolean_0)) || 
                (esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter1_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage12.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp3_stage12.read(), ap_const_boolean_0) && 
                 esl_seteq<1,1,1>(ap_const_lv1_1, tmp_923_reg_23322_pp3_iter1_reg.read())))) {
        pad_temp_0_0_address1 =  (sc_lv<10>) (tmp_87_cast_fu_6472_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage11.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read()) && 
                esl_seteq<1,1,1>(ap_block_pp3_stage11.read(), ap_const_boolean_0))) {
        pad_temp_0_0_address1 =  (sc_lv<10>) (tmp_84_cast_fu_6412_p1.read());
    } else if (((esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter1_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage10.read()) && 
                 esl_seteq<1,1,1>(ap_const_lv1_0, tmp_782_reg_23314_pp3_iter1_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp3_stage10.read(), ap_const_boolean_0)) || 
                (esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter1_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage10.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp3_stage10.read(), ap_const_boolean_0) && 
                 esl_seteq<1,1,1>(ap_const_lv1_1, tmp_782_reg_23314_pp3_iter1_reg.read())))) {
        pad_temp_0_0_address1 =  (sc_lv<10>) (tmp_74_cast_fu_6332_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage9.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read()) && 
                esl_seteq<1,1,1>(ap_block_pp3_stage9.read(), ap_const_boolean_0))) {
        pad_temp_0_0_address1 =  (sc_lv<10>) (tmp_73_cast_fu_6272_p1.read());
    } else if (((esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter1_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage8.read()) && 
                 esl_seteq<1,1,1>(ap_const_lv1_0, tmp_701_reg_23306_pp3_iter1_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp3_stage8.read(), ap_const_boolean_0)) || 
                (esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter1_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage8.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp3_stage8.read(), ap_const_boolean_0) && 
                 esl_seteq<1,1,1>(ap_const_lv1_1, tmp_701_reg_23306_pp3_iter1_reg.read())))) {
        pad_temp_0_0_address1 =  (sc_lv<10>) (tmp_61_cast_fu_6192_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage7.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read()) && 
                esl_seteq<1,1,1>(ap_block_pp3_stage7.read(), ap_const_boolean_0))) {
        pad_temp_0_0_address1 =  (sc_lv<10>) (tmp_60_cast_fu_6132_p1.read());
    } else if (((esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter1_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage6.read()) && 
                 esl_seteq<1,1,1>(ap_const_lv1_0, tmp_331_reg_23257_pp3_iter1_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp3_stage6.read(), ap_const_boolean_0)) || 
                (esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter1_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage6.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp3_stage6.read(), ap_const_boolean_0) && 
                 esl_seteq<1,1,1>(ap_const_lv1_1, tmp_331_reg_23257_pp3_iter1_reg.read())))) {
        pad_temp_0_0_address1 =  (sc_lv<10>) (tmp_44_cast_fu_6052_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage5.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read()) && 
                esl_seteq<1,1,1>(ap_block_pp3_stage5.read(), ap_const_boolean_0))) {
        pad_temp_0_0_address1 =  (sc_lv<10>) (tmp_40_cast_fu_5992_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage0.read()) && 
                esl_seteq<1,1,1>(ap_enable_reg_pp3_iter0.read(), ap_const_logic_1) && 
                esl_seteq<1,1,1>(ap_block_pp3_stage0.read(), ap_const_boolean_0))) {
        pad_temp_0_0_address1 =  (sc_lv<10>) (tmp_152_cast_fu_4701_p1.read());
    } else {
        pad_temp_0_0_address1 =  (sc_lv<10>) ("XXXXXXXXXX");
    }
}

void mnist_fp16_opt::thread_pad_temp_0_0_ce0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage0.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage0_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_enable_reg_pp3_iter0.read(), ap_const_logic_1)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp4_stage1.read()) && 
          esl_seteq<1,1,1>(ap_block_pp4_stage1_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_enable_reg_pp4_iter0.read(), ap_const_logic_1)) || 
         (esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter1_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage5.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage5_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, tmp_248_reg_23232_pp3_iter1_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage6.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage6_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read())) || 
         (esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter1_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage7.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage7_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, tmp_506_reg_23286_pp3_iter1_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage8.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage8_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read())) || 
         (esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter1_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage9.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage9_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, tmp_729_reg_23310_pp3_iter1_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage10.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage10_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read())) || 
         (esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter1_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage11.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage11_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, tmp_831_reg_23318_pp3_iter1_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage12.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage12_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read())) || 
         (esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter1_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage13.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage13_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, tmp_1013_reg_23326_pp3_iter1_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage14.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage14_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read())) || 
         (esl_seteq<1,1,1>(ap_enable_reg_pp3_iter2.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter2_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage1.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage1_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, tmp_87_reg_23223_pp3_iter2_reg.read())) || 
         (esl_seteq<1,1,1>(ap_enable_reg_pp3_iter2.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage2.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage2_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_enable_reg_pp3_iter2.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage4.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage4_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter1_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage5.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage5_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, tmp_248_reg_23232_pp3_iter1_reg.read())) || 
         (esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter1_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage7.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage7_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, tmp_506_reg_23286_pp3_iter1_reg.read())) || 
         (esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter1_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage9.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage9_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, tmp_729_reg_23310_pp3_iter1_reg.read())) || 
         (esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter1_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage11.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage11_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, tmp_831_reg_23318_pp3_iter1_reg.read())) || 
         (esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter1_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage13.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage13_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, tmp_1013_reg_23326_pp3_iter1_reg.read())) || 
         (esl_seteq<1,1,1>(ap_enable_reg_pp3_iter2.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter2_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage1.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage1_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, tmp_87_reg_23223_pp3_iter2_reg.read())) || 
         (esl_seteq<1,1,1>(ap_enable_reg_pp3_iter2.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, tmp_1309_reg_23334_pp3_iter1_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage3.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage3_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter2_reg.read())) || 
         (esl_seteq<1,1,1>(ap_enable_reg_pp3_iter2.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage3.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage3_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter2_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, tmp_1309_reg_23334_pp3_iter1_reg.read())))) {
        pad_temp_0_0_ce0 = ap_const_logic_1;
    } else {
        pad_temp_0_0_ce0 = ap_const_logic_0;
    }
}

void mnist_fp16_opt::thread_pad_temp_0_0_ce1() {
    if (((esl_seteq<1,1,1>(ap_enable_reg_pp3_iter2.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage3.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage3_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage0.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage0_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_enable_reg_pp3_iter0.read(), ap_const_logic_1)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage5.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage5_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read())) || 
         (esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter1_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage6.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage6_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, tmp_331_reg_23257_pp3_iter1_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage7.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage7_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read())) || 
         (esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter1_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage8.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage8_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, tmp_701_reg_23306_pp3_iter1_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage9.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage9_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read())) || 
         (esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter1_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage10.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage10_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, tmp_782_reg_23314_pp3_iter1_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage11.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage11_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read())) || 
         (esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter1_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage12.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage12_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, tmp_923_reg_23322_pp3_iter1_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage13.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage13_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read())) || 
         (esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter1_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage14.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage14_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, tmp_1204_reg_23330_pp3_iter1_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read())) || 
         (esl_seteq<1,1,1>(ap_enable_reg_pp3_iter2.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage1.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage1_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_enable_reg_pp3_iter2.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter2_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage2.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage2_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, tmp_150_reg_23277_pp3_iter1_reg.read())) || 
         (esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter1_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage6.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage6_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, tmp_331_reg_23257_pp3_iter1_reg.read())) || 
         (esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter1_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage8.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage8_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, tmp_701_reg_23306_pp3_iter1_reg.read())) || 
         (esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter1_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage10.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage10_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, tmp_782_reg_23314_pp3_iter1_reg.read())) || 
         (esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter1_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage12.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage12_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, tmp_923_reg_23322_pp3_iter1_reg.read())) || 
         (esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter1_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage14.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage14_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, tmp_1204_reg_23330_pp3_iter1_reg.read())) || 
         (esl_seteq<1,1,1>(ap_enable_reg_pp3_iter2.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter2_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage2.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage2_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, tmp_150_reg_23277_pp3_iter1_reg.read())) || 
         (esl_seteq<1,1,1>(ap_enable_reg_pp3_iter2.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter2_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage4.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage4_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, tmp_1332_reg_23338_pp3_iter2_reg.read())) || 
         (esl_seteq<1,1,1>(ap_enable_reg_pp3_iter2.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter2_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage4.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage4_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, tmp_1332_reg_23338_pp3_iter2_reg.read())))) {
        pad_temp_0_0_ce1 = ap_const_logic_1;
    } else {
        pad_temp_0_0_ce1 = ap_const_logic_0;
    }
}

void mnist_fp16_opt::thread_pad_temp_0_0_d0() {
    if ((esl_seteq<1,1,1>(ap_enable_reg_pp3_iter2.read(), ap_const_logic_1) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage4.read()) && 
         esl_seteq<1,1,1>(ap_block_pp3_stage4.read(), ap_const_boolean_0))) {
        pad_temp_0_0_d0 = ap_phi_reg_pp3_iter2_tmp_16_13_reg_2453.read();
    } else if ((esl_seteq<1,1,1>(ap_enable_reg_pp3_iter2.read(), ap_const_logic_1) && 
                esl_seteq<1,1,1>(ap_const_lv1_0, tmp_1309_reg_23334_pp3_iter1_reg.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage3.read()) && 
                esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter2_reg.read()) && 
                esl_seteq<1,1,1>(ap_block_pp3_stage3.read(), ap_const_boolean_0))) {
        pad_temp_0_0_d0 = reg_4480.read();
    } else if ((esl_seteq<1,1,1>(ap_enable_reg_pp3_iter2.read(), ap_const_logic_1) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage2.read()) && 
                esl_seteq<1,1,1>(ap_block_pp3_stage2.read(), ap_const_boolean_0))) {
        pad_temp_0_0_d0 = ap_phi_mux_tmp_16_4_phi_fu_2432_p4.read();
    } else if ((esl_seteq<1,1,1>(ap_enable_reg_pp3_iter2.read(), ap_const_logic_1) && 
                esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter2_reg.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage1.read()) && 
                esl_seteq<1,1,1>(ap_const_lv1_0, tmp_87_reg_23223_pp3_iter2_reg.read()) && 
                esl_seteq<1,1,1>(ap_block_pp3_stage1.read(), ap_const_boolean_0))) {
        pad_temp_0_0_d0 = image_0_0_q0.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage14.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read()) && 
                esl_seteq<1,1,1>(ap_block_pp3_stage14.read(), ap_const_boolean_0))) {
        pad_temp_0_0_d0 = ap_phi_mux_tmp_16_11_phi_fu_2406_p4.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage12.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read()) && 
                esl_seteq<1,1,1>(ap_block_pp3_stage12.read(), ap_const_boolean_0))) {
        pad_temp_0_0_d0 = ap_phi_mux_tmp_16_9_phi_fu_2380_p4.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage10.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read()) && 
                esl_seteq<1,1,1>(ap_block_pp3_stage10.read(), ap_const_boolean_0))) {
        pad_temp_0_0_d0 = ap_phi_mux_tmp_16_5_phi_fu_2354_p4.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage8.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read()) && 
                esl_seteq<1,1,1>(ap_block_pp3_stage8.read(), ap_const_boolean_0))) {
        pad_temp_0_0_d0 = ap_phi_mux_tmp_16_1_phi_fu_2328_p4.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage6.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read()) && 
                esl_seteq<1,1,1>(ap_block_pp3_stage6.read(), ap_const_boolean_0))) {
        pad_temp_0_0_d0 = ap_phi_mux_tmp_16_8_phi_fu_2302_p4.read();
    } else if (((esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter1_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage5.read()) && 
                 esl_seteq<1,1,1>(ap_const_lv1_0, tmp_248_reg_23232_pp3_iter1_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp3_stage5.read(), ap_const_boolean_0)) || 
                (esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter1_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage7.read()) && 
                 esl_seteq<1,1,1>(ap_const_lv1_0, tmp_506_reg_23286_pp3_iter1_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp3_stage7.read(), ap_const_boolean_0)) || 
                (esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter1_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage9.read()) && 
                 esl_seteq<1,1,1>(ap_const_lv1_0, tmp_729_reg_23310_pp3_iter1_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp3_stage9.read(), ap_const_boolean_0)) || 
                (esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter1_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage11.read()) && 
                 esl_seteq<1,1,1>(ap_const_lv1_0, tmp_831_reg_23318_pp3_iter1_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp3_stage11.read(), ap_const_boolean_0)) || 
                (esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter1_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage13.read()) && 
                 esl_seteq<1,1,1>(ap_const_lv1_0, tmp_1013_reg_23326_pp3_iter1_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp3_stage13.read(), ap_const_boolean_0)))) {
        pad_temp_0_0_d0 = image_0_0_q1.read();
    } else if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage0.read()) && 
                 esl_seteq<1,1,1>(ap_enable_reg_pp3_iter0.read(), ap_const_logic_1) && 
                 esl_seteq<1,1,1>(ap_block_pp3_stage0.read(), ap_const_boolean_0)) || 
                (esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter1_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage5.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp3_stage5.read(), ap_const_boolean_0) && 
                 esl_seteq<1,1,1>(ap_const_lv1_1, tmp_248_reg_23232_pp3_iter1_reg.read())) || 
                (esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter1_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage7.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp3_stage7.read(), ap_const_boolean_0) && 
                 esl_seteq<1,1,1>(ap_const_lv1_1, tmp_506_reg_23286_pp3_iter1_reg.read())) || 
                (esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter1_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage9.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp3_stage9.read(), ap_const_boolean_0) && 
                 esl_seteq<1,1,1>(ap_const_lv1_1, tmp_729_reg_23310_pp3_iter1_reg.read())) || 
                (esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter1_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage11.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp3_stage11.read(), ap_const_boolean_0) && 
                 esl_seteq<1,1,1>(ap_const_lv1_1, tmp_831_reg_23318_pp3_iter1_reg.read())) || 
                (esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter1_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage13.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp3_stage13.read(), ap_const_boolean_0) && 
                 esl_seteq<1,1,1>(ap_const_lv1_1, tmp_1013_reg_23326_pp3_iter1_reg.read())) || 
                (esl_seteq<1,1,1>(ap_enable_reg_pp3_iter2.read(), ap_const_logic_1) && 
                 esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter2_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage1.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp3_stage1.read(), ap_const_boolean_0) && 
                 esl_seteq<1,1,1>(ap_const_lv1_1, tmp_87_reg_23223_pp3_iter2_reg.read())) || 
                (esl_seteq<1,1,1>(ap_enable_reg_pp3_iter2.read(), ap_const_logic_1) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage3.read()) && 
                 esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter2_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_lv1_1, tmp_1309_reg_23334_pp3_iter1_reg.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp3_stage3.read(), ap_const_boolean_0)))) {
        pad_temp_0_0_d0 = ap_const_lv32_0;
    } else {
        pad_temp_0_0_d0 =  (sc_lv<32>) ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
    }
}

void mnist_fp16_opt::thread_pad_temp_0_0_d1() {
    if ((esl_seteq<1,1,1>(ap_enable_reg_pp3_iter2.read(), ap_const_logic_1) && 
         esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter2_reg.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage4.read()) && 
         esl_seteq<1,1,1>(ap_block_pp3_stage4.read(), ap_const_boolean_0) && 
         esl_seteq<1,1,1>(ap_const_lv1_0, tmp_1332_reg_23338_pp3_iter2_reg.read()))) {
        pad_temp_0_0_d1 = reg_4480.read();
    } else if ((esl_seteq<1,1,1>(ap_enable_reg_pp3_iter2.read(), ap_const_logic_1) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage3.read()) && 
                esl_seteq<1,1,1>(ap_block_pp3_stage3.read(), ap_const_boolean_0))) {
        pad_temp_0_0_d1 = ap_phi_reg_pp3_iter2_tmp_16_12_reg_2441.read();
    } else if ((esl_seteq<1,1,1>(ap_enable_reg_pp3_iter2.read(), ap_const_logic_1) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage1.read()) && 
                esl_seteq<1,1,1>(ap_block_pp3_stage1.read(), ap_const_boolean_0))) {
        pad_temp_0_0_d1 = ap_phi_mux_tmp_16_2_phi_fu_2419_p4.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage13.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read()) && 
                esl_seteq<1,1,1>(ap_block_pp3_stage13.read(), ap_const_boolean_0))) {
        pad_temp_0_0_d1 = ap_phi_mux_tmp_16_10_phi_fu_2393_p4.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage11.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read()) && 
                esl_seteq<1,1,1>(ap_block_pp3_stage11.read(), ap_const_boolean_0))) {
        pad_temp_0_0_d1 = ap_phi_mux_tmp_16_7_phi_fu_2367_p4.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage9.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read()) && 
                esl_seteq<1,1,1>(ap_block_pp3_stage9.read(), ap_const_boolean_0))) {
        pad_temp_0_0_d1 = ap_phi_mux_tmp_16_3_phi_fu_2341_p4.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage7.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read()) && 
                esl_seteq<1,1,1>(ap_block_pp3_stage7.read(), ap_const_boolean_0))) {
        pad_temp_0_0_d1 = ap_phi_mux_tmp_16_s_phi_fu_2315_p4.read();
    } else if (((esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter1_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage6.read()) && 
                 esl_seteq<1,1,1>(ap_const_lv1_0, tmp_331_reg_23257_pp3_iter1_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp3_stage6.read(), ap_const_boolean_0)) || 
                (esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter1_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage8.read()) && 
                 esl_seteq<1,1,1>(ap_const_lv1_0, tmp_701_reg_23306_pp3_iter1_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp3_stage8.read(), ap_const_boolean_0)) || 
                (esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter1_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage10.read()) && 
                 esl_seteq<1,1,1>(ap_const_lv1_0, tmp_782_reg_23314_pp3_iter1_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp3_stage10.read(), ap_const_boolean_0)) || 
                (esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter1_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage12.read()) && 
                 esl_seteq<1,1,1>(ap_const_lv1_0, tmp_923_reg_23322_pp3_iter1_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp3_stage12.read(), ap_const_boolean_0)) || 
                (esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter1_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage14.read()) && 
                 esl_seteq<1,1,1>(ap_const_lv1_0, tmp_1204_reg_23330_pp3_iter1_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp3_stage14.read(), ap_const_boolean_0)) || 
                (esl_seteq<1,1,1>(ap_enable_reg_pp3_iter2.read(), ap_const_logic_1) && 
                 esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter2_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage2.read()) && 
                 esl_seteq<1,1,1>(ap_const_lv1_0, tmp_150_reg_23277_pp3_iter1_reg.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp3_stage2.read(), ap_const_boolean_0)))) {
        pad_temp_0_0_d1 = image_0_0_q1.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage5.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read()) && 
                esl_seteq<1,1,1>(ap_block_pp3_stage5.read(), ap_const_boolean_0))) {
        pad_temp_0_0_d1 = ap_phi_mux_tmp_16_6_phi_fu_2289_p4.read();
    } else if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage0.read()) && 
                 esl_seteq<1,1,1>(ap_enable_reg_pp3_iter0.read(), ap_const_logic_1) && 
                 esl_seteq<1,1,1>(ap_block_pp3_stage0.read(), ap_const_boolean_0)) || 
                (esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter1_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage6.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp3_stage6.read(), ap_const_boolean_0) && 
                 esl_seteq<1,1,1>(ap_const_lv1_1, tmp_331_reg_23257_pp3_iter1_reg.read())) || 
                (esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter1_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage8.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp3_stage8.read(), ap_const_boolean_0) && 
                 esl_seteq<1,1,1>(ap_const_lv1_1, tmp_701_reg_23306_pp3_iter1_reg.read())) || 
                (esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter1_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage10.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp3_stage10.read(), ap_const_boolean_0) && 
                 esl_seteq<1,1,1>(ap_const_lv1_1, tmp_782_reg_23314_pp3_iter1_reg.read())) || 
                (esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter1_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage12.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp3_stage12.read(), ap_const_boolean_0) && 
                 esl_seteq<1,1,1>(ap_const_lv1_1, tmp_923_reg_23322_pp3_iter1_reg.read())) || 
                (esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter1_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage14.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp3_stage14.read(), ap_const_boolean_0) && 
                 esl_seteq<1,1,1>(ap_const_lv1_1, tmp_1204_reg_23330_pp3_iter1_reg.read())) || 
                (esl_seteq<1,1,1>(ap_enable_reg_pp3_iter2.read(), ap_const_logic_1) && 
                 esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter2_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage2.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp3_stage2.read(), ap_const_boolean_0) && 
                 esl_seteq<1,1,1>(ap_const_lv1_1, tmp_150_reg_23277_pp3_iter1_reg.read())) || 
                (esl_seteq<1,1,1>(ap_enable_reg_pp3_iter2.read(), ap_const_logic_1) && 
                 esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter2_reg.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage4.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp3_stage4.read(), ap_const_boolean_0) && 
                 esl_seteq<1,1,1>(ap_const_lv1_1, tmp_1332_reg_23338_pp3_iter2_reg.read())))) {
        pad_temp_0_0_d1 = ap_const_lv32_0;
    } else {
        pad_temp_0_0_d1 =  (sc_lv<32>) ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
    }
}

void mnist_fp16_opt::thread_pad_temp_0_0_we0() {
    if (((esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter1_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage5.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage5_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, tmp_248_reg_23232_pp3_iter1_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage6.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage6_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read())) || 
         (esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter1_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage7.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage7_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, tmp_506_reg_23286_pp3_iter1_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage8.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage8_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read())) || 
         (esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter1_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage9.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage9_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, tmp_729_reg_23310_pp3_iter1_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage10.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage10_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read())) || 
         (esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter1_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage11.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage11_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, tmp_831_reg_23318_pp3_iter1_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage12.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage12_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read())) || 
         (esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter1_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage13.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage13_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, tmp_1013_reg_23326_pp3_iter1_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage14.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage14_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read())) || 
         (esl_seteq<1,1,1>(ap_enable_reg_pp3_iter2.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter2_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage1.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage1_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, tmp_87_reg_23223_pp3_iter2_reg.read())) || 
         (esl_seteq<1,1,1>(ap_enable_reg_pp3_iter2.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage2.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage2_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage0.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage0_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_enable_reg_pp3_iter0.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, grp_fu_4400_p2.read())) || 
         (esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter1_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage5.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage5_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, tmp_248_reg_23232_pp3_iter1_reg.read())) || 
         (esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter1_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage7.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage7_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, tmp_506_reg_23286_pp3_iter1_reg.read())) || 
         (esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter1_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage9.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage9_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, tmp_729_reg_23310_pp3_iter1_reg.read())) || 
         (esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter1_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage11.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage11_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, tmp_831_reg_23318_pp3_iter1_reg.read())) || 
         (esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter1_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage13.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage13_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, tmp_1013_reg_23326_pp3_iter1_reg.read())) || 
         (esl_seteq<1,1,1>(ap_enable_reg_pp3_iter2.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter2_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage1.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage1_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, tmp_87_reg_23223_pp3_iter2_reg.read())) || 
         (esl_seteq<1,1,1>(ap_enable_reg_pp3_iter2.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, tmp_1309_reg_23334_pp3_iter1_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage3.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage3_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter2_reg.read())) || 
         (esl_seteq<1,1,1>(ap_enable_reg_pp3_iter2.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage3.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage3_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter2_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, tmp_1309_reg_23334_pp3_iter1_reg.read())) || 
         (esl_seteq<1,1,1>(ap_enable_reg_pp3_iter2.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter2_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage4.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage4_11001.read(), ap_const_boolean_0)))) {
        pad_temp_0_0_we0 = ap_const_logic_1;
    } else {
        pad_temp_0_0_we0 = ap_const_logic_0;
    }
}

void mnist_fp16_opt::thread_pad_temp_0_0_we1() {
    if (((esl_seteq<1,1,1>(ap_enable_reg_pp3_iter2.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage3.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage3_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage5.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage5_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read())) || 
         (esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter1_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage6.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage6_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, tmp_331_reg_23257_pp3_iter1_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage7.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage7_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read())) || 
         (esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter1_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage8.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage8_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, tmp_701_reg_23306_pp3_iter1_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage9.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage9_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read())) || 
         (esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter1_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage10.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage10_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, tmp_782_reg_23314_pp3_iter1_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage11.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage11_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read())) || 
         (esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter1_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage12.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage12_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, tmp_923_reg_23322_pp3_iter1_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage13.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage13_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read())) || 
         (esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter1_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage14.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage14_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, tmp_1204_reg_23330_pp3_iter1_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read())) || 
         (esl_seteq<1,1,1>(ap_enable_reg_pp3_iter2.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage1.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage1_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_enable_reg_pp3_iter2.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter2_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage2.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage2_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, tmp_150_reg_23277_pp3_iter1_reg.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage0.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage0_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_enable_reg_pp3_iter0.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, grp_fu_4400_p2.read())) || 
         (esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter1_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage6.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage6_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, tmp_331_reg_23257_pp3_iter1_reg.read())) || 
         (esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter1_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage8.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage8_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, tmp_701_reg_23306_pp3_iter1_reg.read())) || 
         (esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter1_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage10.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage10_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, tmp_782_reg_23314_pp3_iter1_reg.read())) || 
         (esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter1_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage12.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage12_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, tmp_923_reg_23322_pp3_iter1_reg.read())) || 
         (esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter1_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage14.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage14_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp3_iter1.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, tmp_1204_reg_23330_pp3_iter1_reg.read())) || 
         (esl_seteq<1,1,1>(ap_enable_reg_pp3_iter2.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter2_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage2.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage2_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, tmp_150_reg_23277_pp3_iter1_reg.read())) || 
         (esl_seteq<1,1,1>(ap_enable_reg_pp3_iter2.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter2_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage4.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage4_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, tmp_1332_reg_23338_pp3_iter2_reg.read())) || 
         (esl_seteq<1,1,1>(ap_enable_reg_pp3_iter2.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, exitcond2_reg_23126_pp3_iter2_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp3_stage4.read()) && 
          esl_seteq<1,1,1>(ap_block_pp3_stage4_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, tmp_1332_reg_23338_pp3_iter2_reg.read())))) {
        pad_temp_0_0_we1 = ap_const_logic_1;
    } else {
        pad_temp_0_0_we1 = ap_const_logic_0;
    }
}

void mnist_fp16_opt::thread_pool1_0_address0() {
    if ((esl_seteq<1,1,1>(ap_enable_reg_pp10_iter3.read(), ap_const_logic_1) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage1.read()) && 
         esl_seteq<1,1,1>(ap_block_pp10_stage1.read(), ap_const_boolean_0))) {
        pool1_0_address0 =  (sc_lv<10>) (tmp_1081_cast_fu_12813_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage0.read()) && 
                esl_seteq<1,1,1>(ap_enable_reg_pp10_iter3.read(), ap_const_logic_1) && 
                esl_seteq<1,1,1>(ap_block_pp10_stage0.read(), ap_const_boolean_0))) {
        pool1_0_address0 =  (sc_lv<10>) (tmp_1044_cast_fu_12691_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage7.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp10_iter2.read()) && 
                esl_seteq<1,1,1>(ap_block_pp10_stage7.read(), ap_const_boolean_0))) {
        pool1_0_address0 =  (sc_lv<10>) (tmp_999_cast_fu_12609_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage6.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp10_iter2.read()) && 
                esl_seteq<1,1,1>(ap_block_pp10_stage6.read(), ap_const_boolean_0))) {
        pool1_0_address0 =  (sc_lv<10>) (tmp_962_cast_fu_12497_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage5.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp10_iter2.read()) && 
                esl_seteq<1,1,1>(ap_block_pp10_stage5.read(), ap_const_boolean_0))) {
        pool1_0_address0 =  (sc_lv<10>) (tmp_909_cast_fu_12385_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage4.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp10_iter2.read()) && 
                esl_seteq<1,1,1>(ap_block_pp10_stage4.read(), ap_const_boolean_0))) {
        pool1_0_address0 =  (sc_lv<10>) (tmp_861_cast_fu_12273_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage3.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp10_iter2.read()) && 
                esl_seteq<1,1,1>(ap_block_pp10_stage3.read(), ap_const_boolean_0))) {
        pool1_0_address0 =  (sc_lv<10>) (tmp_825_cast_fu_12161_p1.read());
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state145.read())) {
        pool1_0_address0 = pool1_0_addr_2_reg_24768.read();
    } else {
        pool1_0_address0 =  (sc_lv<10>) ("XXXXXXXXXX");
    }
}

void mnist_fp16_opt::thread_pool1_0_address1() {
    if ((esl_seteq<1,1,1>(ap_enable_reg_pp10_iter3.read(), ap_const_logic_1) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage1.read()) && 
         esl_seteq<1,1,1>(ap_block_pp10_stage1.read(), ap_const_boolean_0))) {
        pool1_0_address1 =  (sc_lv<10>) (tmp_1080_cast_fu_12803_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage0.read()) && 
                esl_seteq<1,1,1>(ap_enable_reg_pp10_iter3.read(), ap_const_logic_1) && 
                esl_seteq<1,1,1>(ap_block_pp10_stage0.read(), ap_const_boolean_0))) {
        pool1_0_address1 =  (sc_lv<10>) (tmp_1045_cast_fu_12701_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage7.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp10_iter2.read()) && 
                esl_seteq<1,1,1>(ap_block_pp10_stage7.read(), ap_const_boolean_0))) {
        pool1_0_address1 =  (sc_lv<10>) (tmp_998_cast_fu_12599_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage6.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp10_iter2.read()) && 
                esl_seteq<1,1,1>(ap_block_pp10_stage6.read(), ap_const_boolean_0))) {
        pool1_0_address1 =  (sc_lv<10>) (tmp_961_cast_fu_12487_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage5.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp10_iter2.read()) && 
                esl_seteq<1,1,1>(ap_block_pp10_stage5.read(), ap_const_boolean_0))) {
        pool1_0_address1 =  (sc_lv<10>) (tmp_908_cast_fu_12375_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage4.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp10_iter2.read()) && 
                esl_seteq<1,1,1>(ap_block_pp10_stage4.read(), ap_const_boolean_0))) {
        pool1_0_address1 =  (sc_lv<10>) (tmp_860_cast_fu_12263_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage3.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp10_iter2.read()) && 
                esl_seteq<1,1,1>(ap_block_pp10_stage3.read(), ap_const_boolean_0))) {
        pool1_0_address1 =  (sc_lv<10>) (tmp_824_cast_fu_12150_p1.read());
    } else {
        pool1_0_address1 =  (sc_lv<10>) ("XXXXXXXXXX");
    }
}

void mnist_fp16_opt::thread_pool1_0_ce0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage0.read()) && 
          esl_seteq<1,1,1>(ap_enable_reg_pp10_iter3.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_block_pp10_stage0_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_enable_reg_pp10_iter3.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage1.read()) && 
          esl_seteq<1,1,1>(ap_block_pp10_stage1_11001.read(), ap_const_boolean_0)) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state145.read()) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage4.read()) && 
          esl_seteq<1,1,1>(ap_block_pp10_stage4_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp10_iter2.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage3.read()) && 
          esl_seteq<1,1,1>(ap_block_pp10_stage3_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp10_iter2.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage5.read()) && 
          esl_seteq<1,1,1>(ap_block_pp10_stage5_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp10_iter2.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage6.read()) && 
          esl_seteq<1,1,1>(ap_block_pp10_stage6_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp10_iter2.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage7.read()) && 
          esl_seteq<1,1,1>(ap_block_pp10_stage7_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp10_iter2.read())))) {
        pool1_0_ce0 = ap_const_logic_1;
    } else {
        pool1_0_ce0 = ap_const_logic_0;
    }
}

void mnist_fp16_opt::thread_pool1_0_ce1() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage0.read()) && 
          esl_seteq<1,1,1>(ap_enable_reg_pp10_iter3.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_block_pp10_stage0_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_enable_reg_pp10_iter3.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage1.read()) && 
          esl_seteq<1,1,1>(ap_block_pp10_stage1_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage4.read()) && 
          esl_seteq<1,1,1>(ap_block_pp10_stage4_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp10_iter2.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage3.read()) && 
          esl_seteq<1,1,1>(ap_block_pp10_stage3_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp10_iter2.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage5.read()) && 
          esl_seteq<1,1,1>(ap_block_pp10_stage5_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp10_iter2.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage6.read()) && 
          esl_seteq<1,1,1>(ap_block_pp10_stage6_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp10_iter2.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp10_stage7.read()) && 
          esl_seteq<1,1,1>(ap_block_pp10_stage7_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp10_iter2.read())))) {
        pool1_0_ce1 = ap_const_logic_1;
    } else {
        pool1_0_ce1 = ap_const_logic_0;
    }
}

void mnist_fp16_opt::thread_pool1_0_we0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state145.read())) {
        pool1_0_we0 = ap_const_logic_1;
    } else {
        pool1_0_we0 = ap_const_logic_0;
    }
}

void mnist_fp16_opt::thread_pool2_0_address0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp17_stage0.read()) && 
         esl_seteq<1,1,1>(ap_enable_reg_pp17_iter4.read(), ap_const_logic_1) && 
         esl_seteq<1,1,1>(ap_block_pp17_stage0.read(), ap_const_boolean_0))) {
        pool2_0_address0 =  (sc_lv<9>) (tmp_1221_cast_fu_17991_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp17_stage4.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp17_iter3.read()) && 
                esl_seteq<1,1,1>(ap_block_pp17_stage4.read(), ap_const_boolean_0))) {
        pool2_0_address0 =  (sc_lv<9>) (tmp_1184_cast_fu_17894_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp17_stage3.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp17_iter3.read()) && 
                esl_seteq<1,1,1>(ap_block_pp17_stage3.read(), ap_const_boolean_0))) {
        pool2_0_address0 =  (sc_lv<9>) (tmp_1146_cast_fu_17798_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp17_stage2.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp17_iter3.read()) && 
                esl_seteq<1,1,1>(ap_block_pp17_stage2.read(), ap_const_boolean_0))) {
        pool2_0_address0 =  (sc_lv<9>) (tmp_1121_cast_fu_17693_p1.read());
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state269.read())) {
        pool2_0_address0 = pool2_0_addr_2_reg_26363.read();
    } else {
        pool2_0_address0 =  (sc_lv<9>) ("XXXXXXXXX");
    }
}

void mnist_fp16_opt::thread_pool2_0_address1() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp17_iter3.read())) {
        if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp17_stage4.read()) && 
             esl_seteq<1,1,1>(ap_block_pp17_stage4.read(), ap_const_boolean_0))) {
            pool2_0_address1 =  (sc_lv<9>) (tmp_1183_cast_fu_17884_p1.read());
        } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp17_stage3.read()) && 
                    esl_seteq<1,1,1>(ap_block_pp17_stage3.read(), ap_const_boolean_0))) {
            pool2_0_address1 =  (sc_lv<9>) (tmp_1145_cast_fu_17788_p1.read());
        } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp17_stage2.read()) && 
                    esl_seteq<1,1,1>(ap_block_pp17_stage2.read(), ap_const_boolean_0))) {
            pool2_0_address1 =  (sc_lv<9>) (tmp_1122_cast_fu_17702_p1.read());
        } else {
            pool2_0_address1 =  (sc_lv<9>) ("XXXXXXXXX");
        }
    } else {
        pool2_0_address1 =  (sc_lv<9>) ("XXXXXXXXX");
    }
}

void mnist_fp16_opt::thread_pool2_0_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state269.read()) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp17_stage3.read()) && 
          esl_seteq<1,1,1>(ap_block_pp17_stage3_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp17_iter3.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp17_stage2.read()) && 
          esl_seteq<1,1,1>(ap_block_pp17_stage2_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp17_iter3.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp17_stage4.read()) && 
          esl_seteq<1,1,1>(ap_block_pp17_stage4_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp17_iter3.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp17_stage0.read()) && 
          esl_seteq<1,1,1>(ap_block_pp17_stage0_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_enable_reg_pp17_iter4.read(), ap_const_logic_1)))) {
        pool2_0_ce0 = ap_const_logic_1;
    } else {
        pool2_0_ce0 = ap_const_logic_0;
    }
}

void mnist_fp16_opt::thread_pool2_0_ce1() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp17_stage3.read()) && 
          esl_seteq<1,1,1>(ap_block_pp17_stage3_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp17_iter3.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp17_stage2.read()) && 
          esl_seteq<1,1,1>(ap_block_pp17_stage2_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp17_iter3.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp17_stage4.read()) && 
          esl_seteq<1,1,1>(ap_block_pp17_stage4_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp17_iter3.read())))) {
        pool2_0_ce1 = ap_const_logic_1;
    } else {
        pool2_0_ce1 = ap_const_logic_0;
    }
}

void mnist_fp16_opt::thread_pool2_0_we0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state269.read())) {
        pool2_0_we0 = ap_const_logic_1;
    } else {
        pool2_0_we0 = ap_const_logic_0;
    }
}

void mnist_fp16_opt::thread_pool3_0_V_address0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state395.read())) {
        pool3_0_V_address0 =  (sc_lv<4>) (tmp_658_fu_22201_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_block_pp24_stage0.read(), ap_const_boolean_0) && 
                esl_seteq<1,1,1>(ap_enable_reg_pp24_iter3.read(), ap_const_logic_1))) {
        pool3_0_V_address0 =  (sc_lv<4>) (tmp_433_reg_27833_pp24_iter2_reg.read());
    } else {
        pool3_0_V_address0 =  (sc_lv<4>) ("XXXX");
    }
}

void mnist_fp16_opt::thread_pool3_0_V_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state395.read()) || 
         (esl_seteq<1,1,1>(ap_block_pp24_stage0_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_enable_reg_pp24_iter3.read(), ap_const_logic_1)))) {
        pool3_0_V_ce0 = ap_const_logic_1;
    } else {
        pool3_0_V_ce0 = ap_const_logic_0;
    }
}

void mnist_fp16_opt::thread_pool3_0_V_d0() {
    pool3_0_V_d0 = (!or_cond29_fu_22154_p2.read()[0].is_01())? sc_lv<16>(): ((or_cond29_fu_22154_p2.read()[0].to_bool())? newSel38_fu_22146_p3.read(): ap_const_lv16_0);
}

void mnist_fp16_opt::thread_pool3_0_V_we0() {
    if ((esl_seteq<1,1,1>(ap_block_pp24_stage0_11001.read(), ap_const_boolean_0) && 
         esl_seteq<1,1,1>(ap_enable_reg_pp24_iter3.read(), ap_const_logic_1) && 
         esl_seteq<1,1,1>(ap_const_lv1_0, exitcond31_reg_27824_pp24_iter2_reg.read()))) {
        pool3_0_V_we0 = ap_const_logic_1;
    } else {
        pool3_0_V_we0 = ap_const_logic_0;
    }
}

void mnist_fp16_opt::thread_pred_1_fu_22694_p3() {
    pred_1_fu_22694_p3 = (!tmp_671_fu_22688_p2.read()[0].is_01())? sc_lv<32>(): ((tmp_671_fu_22688_p2.read()[0].to_bool())? pred_2_reg_28094.read(): ap_phi_mux_pred_phi_fu_4331_p4.read());
}

void mnist_fp16_opt::thread_pred_2_to_int_fu_22611_p1() {
    pred_2_to_int_fu_22611_p1 = pred_2_reg_28094.read();
}

void mnist_fp16_opt::thread_pred_to_int_fu_22628_p1() {
    pred_to_int_fu_22628_p1 = ap_phi_mux_pred_phi_fu_4331_p4.read();
}

void mnist_fp16_opt::thread_preds_0_address0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp27_stage0.read()) && 
         esl_seteq<1,1,1>(ap_enable_reg_pp27_iter0.read(), ap_const_logic_1) && 
         esl_seteq<1,1,1>(ap_block_pp27_stage0.read(), ap_const_boolean_0))) {
        preds_0_address0 =  (sc_lv<4>) (tmp_560_fu_22606_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_block_pp26_stage0.read(), ap_const_boolean_0) && 
                esl_seteq<1,1,1>(ap_enable_reg_pp26_iter58.read(), ap_const_logic_1))) {
        preds_0_address0 =  (sc_lv<4>) (tmp_523_reg_28060_pp26_iter57_reg.read());
    } else {
        preds_0_address0 =  (sc_lv<4>) ("XXXX");
    }
}

void mnist_fp16_opt::thread_preds_0_ce0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp27_stage0.read()) && 
          esl_seteq<1,1,1>(ap_block_pp27_stage0_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_enable_reg_pp27_iter0.read(), ap_const_logic_1)) || 
         (esl_seteq<1,1,1>(ap_block_pp26_stage0_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_enable_reg_pp26_iter58.read(), ap_const_logic_1)))) {
        preds_0_ce0 = ap_const_logic_1;
    } else {
        preds_0_ce0 = ap_const_logic_0;
    }
}

void mnist_fp16_opt::thread_preds_0_we0() {
    if ((esl_seteq<1,1,1>(ap_block_pp26_stage0_11001.read(), ap_const_boolean_0) && 
         esl_seteq<1,1,1>(ap_enable_reg_pp26_iter58.read(), ap_const_logic_1) && 
         esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_reg_28051_pp26_iter57_reg.read()))) {
        preds_0_we0 = ap_const_logic_1;
    } else {
        preds_0_we0 = ap_const_logic_0;
    }
}

void mnist_fp16_opt::thread_r_V_100_tr_fu_19295_p2() {
    r_V_100_tr_fu_19295_p2 = (!tmp380_mid2_reg_27105.read().is_01() || !tmp381_cast_fu_19291_p1.read().is_01())? sc_lv<11>(): (sc_biguint<11>(tmp380_mid2_reg_27105.read()) + sc_bigint<11>(tmp381_cast_fu_19291_p1.read()));
}

void mnist_fp16_opt::thread_r_V_10_fu_13852_p2() {
    r_V_10_fu_13852_p2 = (!p_shl3_cast_fu_13838_p1.read().is_01() || !p_shl4_cast_fu_13848_p1.read().is_01())? sc_lv<9>(): (sc_biguint<9>(p_shl3_cast_fu_13838_p1.read()) - sc_biguint<9>(p_shl4_cast_fu_13848_p1.read()));
}

void mnist_fp16_opt::thread_r_V_10_mid1_fu_14022_p2() {
    r_V_10_mid1_fu_14022_p2 = (!p_shl3_cast_mid1_fu_14008_p1.read().is_01() || !p_shl4_cast_mid1_fu_14018_p1.read().is_01())? sc_lv<9>(): (sc_biguint<9>(p_shl3_cast_mid1_fu_14008_p1.read()) - sc_biguint<9>(p_shl4_cast_mid1_fu_14018_p1.read()));
}

void mnist_fp16_opt::thread_r_V_11_fu_13227_p2() {
    r_V_11_fu_13227_p2 = (!p_29_mid2_reg_25433.read().is_01() || !rhs_V_11_cast_fu_13224_p1.read().is_01())? sc_lv<4>(): (sc_biguint<4>(p_29_mid2_reg_25433.read()) + sc_biguint<4>(rhs_V_11_cast_fu_13224_p1.read()));
}

void mnist_fp16_opt::thread_r_V_11_mid1_fu_13159_p2() {
    r_V_11_mid1_fu_13159_p2 = (!tmp_95_mid2_reg_25440.read().is_01() || !rhs_V_10_cast_mid1_fu_13155_p1.read().is_01())? sc_lv<4>(): (sc_biguint<4>(tmp_95_mid2_reg_25440.read()) + sc_biguint<4>(rhs_V_10_cast_mid1_fu_13155_p1.read()));
}

void mnist_fp16_opt::thread_r_V_12_fu_14135_p2() {
    r_V_12_fu_14135_p2 = (!ap_const_lv4_F.is_01() || !tmp_1014_fu_14132_p1.read().is_01())? sc_lv<4>(): (sc_bigint<4>(ap_const_lv4_F) + sc_biguint<4>(tmp_1014_fu_14132_p1.read()));
}

void mnist_fp16_opt::thread_r_V_13_fu_14324_p3() {
    r_V_13_fu_14324_p3 = (!tmp_1023_reg_25766.read()[0].is_01())? sc_lv<3>(): ((tmp_1023_reg_25766.read()[0].to_bool())? neg_ti19_fu_14314_p2.read(): tmp_1061_fu_14320_p1.read());
}

void mnist_fp16_opt::thread_r_V_14_fu_14795_p2() {
    r_V_14_fu_14795_p2 = (!tmp_181_mid2_reg_25902.read().is_01() || !rhs_V_14_cast_fu_14791_p1.read().is_01())? sc_lv<4>(): (sc_biguint<4>(tmp_181_mid2_reg_25902.read()) + sc_biguint<4>(rhs_V_14_cast_fu_14791_p1.read()));
}

void mnist_fp16_opt::thread_r_V_15_fu_14994_p2() {
    r_V_15_fu_14994_p2 = (!p_39_mid2_reg_25895.read().is_01() || !rhs_V_15_cast_fu_14991_p1.read().is_01())? sc_lv<4>(): (sc_biguint<4>(p_39_mid2_reg_25895.read()) + sc_biguint<4>(rhs_V_15_cast_fu_14991_p1.read()));
}

void mnist_fp16_opt::thread_r_V_15_mid1_fu_16522_p3() {
    r_V_15_mid1_fu_16522_p3 = esl_concat<3,1>(h1_V_fu_16465_p2.read(), ap_const_lv1_0);
}

void mnist_fp16_opt::thread_r_V_15_mid2_fu_16530_p3() {
    r_V_15_mid2_fu_16530_p3 = (!exitcond66_mid_fu_16459_p2.read()[0].is_01())? sc_lv<4>(): ((exitcond66_mid_fu_16459_p2.read()[0].to_bool())? r_V_15_mid1_fu_16522_p3.read(): r_V_15_mid_fu_16441_p3.read());
}

void mnist_fp16_opt::thread_r_V_15_mid_fu_16441_p3() {
    r_V_15_mid_fu_16441_p3 = (!exitcond_flatten32_reg_26320.read()[0].is_01())? sc_lv<4>(): ((exitcond_flatten32_reg_26320.read()[0].to_bool())? ap_const_lv4_0: r_V_17_fu_16433_p3.read());
}

void mnist_fp16_opt::thread_r_V_16_fu_16553_p3() {
    r_V_16_fu_16553_p3 = esl_concat<3,1>(p_65_mid2_fu_16475_p3.read(), ap_const_lv1_0);
}

void mnist_fp16_opt::thread_r_V_17_fu_16433_p3() {
    r_V_17_fu_16433_p3 = esl_concat<3,1>(p_59_reg_3561.read(), ap_const_lv1_0);
}

void mnist_fp16_opt::thread_r_V_17_mid1_fu_14920_p2() {
    r_V_17_mid1_fu_14920_p2 = (!rhs_V_14_cast_mid1_fu_14916_p1.read().is_01() || !tmp_181_mid2_reg_25902.read().is_01())? sc_lv<4>(): (sc_biguint<4>(rhs_V_14_cast_mid1_fu_14916_p1.read()) + sc_biguint<4>(tmp_181_mid2_reg_25902.read()));
}

void mnist_fp16_opt::thread_r_V_17_tr_fu_8158_p2() {
    r_V_17_tr_fu_8158_p2 = (!tmp346_cast_fu_8155_p1.read().is_01() || !tmp69_mid2_reg_24123.read().is_01())? sc_lv<13>(): (sc_bigint<13>(tmp346_cast_fu_8155_p1.read()) + sc_biguint<13>(tmp69_mid2_reg_24123.read()));
}

void mnist_fp16_opt::thread_r_V_18_fu_18151_p2() {
    r_V_18_fu_18151_p2 = (!lhs_V_22_cast_mid2_c_reg_26836.read().is_01() || !rhs_V_18_cast_fu_18147_p1.read().is_01())? sc_lv<4>(): (sc_biguint<4>(lhs_V_22_cast_mid2_c_reg_26836.read()) + sc_biguint<4>(rhs_V_18_cast_fu_18147_p1.read()));
}

void mnist_fp16_opt::thread_r_V_19_fu_18397_p2() {
    r_V_19_fu_18397_p2 = (!lhs_V_24_cast_reg_26848.read().is_01() || !rhs_V_19_cast_fu_18394_p1.read().is_01())? sc_lv<4>(): (sc_biguint<4>(lhs_V_24_cast_reg_26848.read()) + sc_biguint<4>(rhs_V_19_cast_fu_18394_p1.read()));
}

void mnist_fp16_opt::thread_r_V_19_mid1_fu_18247_p2() {
    r_V_19_mid1_fu_18247_p2 = (!lhs_V_22_cast_mid2_c_reg_26836.read().is_01() || !rhs_V_18_cast_mid1_fu_18243_p1.read().is_01())? sc_lv<4>(): (sc_biguint<4>(lhs_V_22_cast_mid2_c_reg_26836.read()) + sc_biguint<4>(rhs_V_18_cast_mid1_fu_18243_p1.read()));
}

void mnist_fp16_opt::thread_r_V_1_fu_7104_p2() {
    r_V_1_fu_7104_p2 = (!p_8_mid2_reg_23815.read().is_01() || !rhs_V_cast_fu_7100_p1.read().is_01())? sc_lv<5>(): (sc_biguint<5>(p_8_mid2_reg_23815.read()) + sc_biguint<5>(rhs_V_cast_fu_7100_p1.read()));
}

void mnist_fp16_opt::thread_r_V_20_fu_19252_p2() {
    r_V_20_fu_19252_p2 = (!ap_const_lv3_7.is_01() || !tmp_1412_fu_19249_p1.read().is_01())? sc_lv<3>(): (sc_bigint<3>(ap_const_lv3_7) + sc_biguint<3>(tmp_1412_fu_19249_p1.read()));
}

void mnist_fp16_opt::thread_r_V_21_fu_19435_p3() {
    r_V_21_fu_19435_p3 = (!tmp_1414_reg_27134_pp20_iter2_reg.read()[0].is_01())? sc_lv<4>(): ((tmp_1414_reg_27134_pp20_iter2_reg.read()[0].to_bool())? neg_ti39_fu_19425_p2.read(): tmp_1422_fu_19431_p1.read());
}

void mnist_fp16_opt::thread_r_V_22_fu_19882_p2() {
    r_V_22_fu_19882_p2 = (!lhs_V_26_cast_mid2_c_reg_27284.read().is_01() || !rhs_V_20_cast_fu_19878_p1.read().is_01())? sc_lv<4>(): (sc_biguint<4>(lhs_V_26_cast_mid2_c_reg_27284.read()) + sc_biguint<4>(rhs_V_20_cast_fu_19878_p1.read()));
}

void mnist_fp16_opt::thread_r_V_23_fu_20134_p2() {
    r_V_23_fu_20134_p2 = (!lhs_V_27_cast_reg_27296.read().is_01() || !rhs_V_21_cast_fu_20131_p1.read().is_01())? sc_lv<4>(): (sc_biguint<4>(lhs_V_27_cast_reg_27296.read()) + sc_biguint<4>(rhs_V_21_cast_fu_20131_p1.read()));
}

void mnist_fp16_opt::thread_r_V_23_mid1_fu_19978_p2() {
    r_V_23_mid1_fu_19978_p2 = (!rhs_V_20_cast_mid1_fu_19974_p1.read().is_01() || !lhs_V_26_cast_mid2_c_reg_27284.read().is_01())? sc_lv<4>(): (sc_biguint<4>(rhs_V_20_cast_mid1_fu_19974_p1.read()) + sc_biguint<4>(lhs_V_26_cast_mid2_c_reg_27284.read()));
}

void mnist_fp16_opt::thread_r_V_2_fu_8099_p2() {
    r_V_2_fu_8099_p2 = (!ap_const_lv5_1F.is_01() || !p_12_mid2_reg_24091.read().is_01())? sc_lv<5>(): (sc_bigint<5>(ap_const_lv5_1F) + sc_biguint<5>(p_12_mid2_reg_24091.read()));
}

void mnist_fp16_opt::thread_r_V_31_tr14_2_fu_11462_p2() {
    r_V_31_tr14_2_fu_11462_p2 = (!tmp350_cast_mid2_reg_24947.read().is_01() || !tmp_307_reg_24958.read().is_01())? sc_lv<12>(): (sc_bigint<12>(tmp350_cast_mid2_reg_24947.read()) + sc_biguint<12>(tmp_307_reg_24958.read()));
}

void mnist_fp16_opt::thread_r_V_3_fu_7764_p2() {
    r_V_3_fu_7764_p2 = (!p_shl8_cast_fu_7748_p1.read().is_01() || !p_shl9_cast_fu_7760_p1.read().is_01())? sc_lv<11>(): (sc_biguint<11>(p_shl8_cast_fu_7748_p1.read()) - sc_biguint<11>(p_shl9_cast_fu_7760_p1.read()));
}

void mnist_fp16_opt::thread_r_V_4_fu_8290_p3() {
    r_V_4_fu_8290_p3 = (!tmp_418_reg_24156.read()[0].is_01())? sc_lv<2>(): ((tmp_418_reg_24156.read()[0].to_bool())? neg_ti5_fu_8280_p2.read(): tmp_449_fu_8286_p1.read());
}

void mnist_fp16_opt::thread_r_V_4_mid1_fu_8000_p2() {
    r_V_4_mid1_fu_8000_p2 = (!p_shl8_cast_mid1_fu_7985_p1.read().is_01() || !p_shl9_cast_mid1_fu_7996_p1.read().is_01())? sc_lv<11>(): (sc_biguint<11>(p_shl8_cast_mid1_fu_7985_p1.read()) - sc_biguint<11>(p_shl9_cast_mid1_fu_7996_p1.read()));
}

void mnist_fp16_opt::thread_r_V_5_fu_8967_p2() {
    r_V_5_fu_8967_p2 = (!rhs_V_4_cast_fu_8963_p1.read().is_01() || !p_14_mid2_reg_24276.read().is_01())? sc_lv<5>(): (sc_biguint<5>(rhs_V_4_cast_fu_8963_p1.read()) + sc_biguint<5>(p_14_mid2_reg_24276.read()));
}

void mnist_fp16_opt::thread_r_V_6_fu_11146_p2() {
    r_V_6_fu_11146_p2 = (!p_shl1_cast_fu_11132_p1.read().is_01() || !p_shl2_cast_fu_11142_p1.read().is_01())? sc_lv<9>(): (sc_biguint<9>(p_shl1_cast_fu_11132_p1.read()) - sc_biguint<9>(p_shl2_cast_fu_11142_p1.read()));
}

void mnist_fp16_opt::thread_r_V_7_fu_10598_p3() {
    r_V_7_fu_10598_p3 = esl_concat<4,1>(p_33_mid2_fu_10512_p3.read(), ap_const_lv1_0);
}

void mnist_fp16_opt::thread_r_V_85_tr_fu_14192_p2() {
    r_V_85_tr_fu_14192_p2 = (!tmp361_cast_fu_14189_p1.read().is_01() || !tmp360_mid2_reg_25737.read().is_01())? sc_lv<12>(): (sc_bigint<12>(tmp361_cast_fu_14189_p1.read()) + sc_biguint<12>(tmp360_mid2_reg_25737.read()));
}

void mnist_fp16_opt::thread_r_V_8_fu_8751_p2() {
    r_V_8_fu_8751_p2 = (!tmp_29_mid2_reg_24283.read().is_01() || !rhs_V_7_cast_fu_8747_p1.read().is_01())? sc_lv<5>(): (sc_biguint<5>(tmp_29_mid2_reg_24283.read()) + sc_biguint<5>(rhs_V_7_cast_fu_8747_p1.read()));
}

void mnist_fp16_opt::thread_r_V_8_mid1_fu_8903_p2() {
    r_V_8_mid1_fu_8903_p2 = (!tmp_29_mid2_reg_24283.read().is_01() || !rhs_V_7_cast_mid1_fu_8899_p1.read().is_01())? sc_lv<5>(): (sc_biguint<5>(tmp_29_mid2_reg_24283.read()) + sc_biguint<5>(rhs_V_7_cast_mid1_fu_8899_p1.read()));
}

void mnist_fp16_opt::thread_r_V_9_fu_13034_p2() {
    r_V_9_fu_13034_p2 = (!tmp_95_mid2_reg_25440.read().is_01() || !rhs_V_10_cast_fu_13030_p1.read().is_01())? sc_lv<4>(): (sc_biguint<4>(tmp_95_mid2_reg_25440.read()) + sc_biguint<4>(rhs_V_10_cast_fu_13030_p1.read()));
}

void mnist_fp16_opt::thread_r_V_fu_4718_p2() {
    r_V_fu_4718_p2 = (!p_shl10_cast_fu_4668_p1.read().is_01() || !p_shl7_cast_fu_4714_p1.read().is_01())? sc_lv<11>(): (sc_biguint<11>(p_shl10_cast_fu_4668_p1.read()) - sc_biguint<11>(p_shl7_cast_fu_4714_p1.read()));
}

void mnist_fp16_opt::thread_r_V_mid1_fu_10567_p3() {
    r_V_mid1_fu_10567_p3 = esl_concat<4,1>(h_V_fu_10502_p2.read(), ap_const_lv1_0);
}

void mnist_fp16_opt::thread_r_V_mid2_fu_10575_p3() {
    r_V_mid2_fu_10575_p3 = (!exitcond34_mid_fu_10497_p2.read()[0].is_01())? sc_lv<5>(): ((exitcond34_mid_fu_10497_p2.read()[0].to_bool())? r_V_mid1_fu_10567_p3.read(): r_V_mid_fu_10485_p3.read());
}

void mnist_fp16_opt::thread_r_V_mid_fu_10485_p3() {
    r_V_mid_fu_10485_p3 = (!exitcond_flatten14_reg_24720.read()[0].is_01())? sc_lv<5>(): ((exitcond_flatten14_reg_24720.read()[0].to_bool())? ap_const_lv5_0: r_V_s_fu_10477_p3.read());
}

void mnist_fp16_opt::thread_r_V_s_fu_10477_p3() {
    r_V_s_fu_10477_p3 = esl_concat<4,1>(p_27_reg_2909.read(), ap_const_lv1_0);
}

void mnist_fp16_opt::thread_ra63_V_fu_10618_p2() {
    ra63_V_fu_10618_p2 = (!ap_const_lv2_1.is_01() || !ap_phi_mux_p_38_phi_fu_2948_p4.read().is_01())? sc_lv<2>(): (sc_biguint<2>(ap_const_lv2_1) + sc_biguint<2>(ap_phi_mux_p_38_phi_fu_2948_p4.read()));
}

void mnist_fp16_opt::thread_ra64_V_fu_10677_p2() {
    ra64_V_fu_10677_p2 = (!ap_const_lv2_1.is_01() || !p_43_mid2_fu_10630_p3.read().is_01())? sc_lv<2>(): (sc_biguint<2>(ap_const_lv2_1) + sc_biguint<2>(p_43_mid2_fu_10630_p3.read()));
}

void mnist_fp16_opt::thread_ra65_V_fu_16573_p2() {
    ra65_V_fu_16573_p2 = (!ap_const_lv2_1.is_01() || !ap_phi_mux_p_70_phi_fu_3600_p4.read().is_01())? sc_lv<2>(): (sc_biguint<2>(ap_const_lv2_1) + sc_biguint<2>(ap_phi_mux_p_70_phi_fu_3600_p4.read()));
}

void mnist_fp16_opt::thread_ra66_V_fu_16632_p2() {
    ra66_V_fu_16632_p2 = (!ap_const_lv2_1.is_01() || !p_75_mid2_fu_16585_p3.read().is_01())? sc_lv<2>(): (sc_biguint<2>(ap_const_lv2_1) + sc_biguint<2>(p_75_mid2_fu_16585_p3.read()));
}

void mnist_fp16_opt::thread_ra67_V_fu_21507_p2() {
    ra67_V_fu_21507_p2 = (!ap_const_lv3_1.is_01() || !ap_phi_mux_p_83_phi_fu_4168_p4.read().is_01())? sc_lv<3>(): (sc_biguint<3>(ap_const_lv3_1) + sc_biguint<3>(ap_phi_mux_p_83_phi_fu_4168_p4.read()));
}

void mnist_fp16_opt::thread_ra68_V_fu_21576_p2() {
    ra68_V_fu_21576_p2 = (!ap_const_lv3_1.is_01() || !p_86_mid2_fu_21519_p3.read().is_01())? sc_lv<3>(): (sc_biguint<3>(ap_const_lv3_1) + sc_biguint<3>(p_86_mid2_fu_21519_p3.read()));
}

void mnist_fp16_opt::thread_ra69_V_fu_22195_p2() {
    ra69_V_fu_22195_p2 = (!p_87_reg_4221.read().is_01() || !ap_const_lv5_1.is_01())? sc_lv<5>(): (sc_biguint<5>(p_87_reg_4221.read()) + sc_biguint<5>(ap_const_lv5_1));
}

void mnist_fp16_opt::thread_ra70_V_fu_22457_p2() {
    ra70_V_fu_22457_p2 = (!p_71_reg_4257.read().is_01() || !ap_const_lv4_1.is_01())? sc_lv<4>(): (sc_biguint<4>(p_71_reg_4257.read()) + sc_biguint<4>(ap_const_lv4_1));
}

void mnist_fp16_opt::thread_ra71_V_fu_22566_p2() {
    ra71_V_fu_22566_p2 = (!p_74_reg_4268.read().is_01() || !ap_const_lv4_1.is_01())? sc_lv<4>(): (sc_biguint<4>(p_74_reg_4268.read()) + sc_biguint<4>(ap_const_lv4_1));
}

void mnist_fp16_opt::thread_rc1_V_fu_13051_p2() {
    rc1_V_fu_13051_p2 = (!ap_const_lv3_1.is_01() || !ap_phi_mux_p_32_phi_fu_3171_p4.read().is_01())? sc_lv<3>(): (sc_biguint<3>(ap_const_lv3_1) + sc_biguint<3>(ap_phi_mux_p_32_phi_fu_3171_p4.read()));
}

void mnist_fp16_opt::thread_rc2_V_fu_14812_p2() {
    rc2_V_fu_14812_p2 = (!ap_const_lv4_1.is_01() || !ap_phi_mux_p_44_phi_fu_3420_p4.read().is_01())? sc_lv<4>(): (sc_biguint<4>(ap_const_lv4_1) + sc_biguint<4>(ap_phi_mux_p_44_phi_fu_3420_p4.read()));
}

void mnist_fp16_opt::thread_rc3_V_fu_18168_p2() {
    rc3_V_fu_18168_p2 = (!ap_const_lv4_1.is_01() || !ap_phi_mux_p_58_phi_fu_3786_p4.read().is_01())? sc_lv<4>(): (sc_biguint<4>(ap_const_lv4_1) + sc_biguint<4>(ap_phi_mux_p_58_phi_fu_3786_p4.read()));
}

void mnist_fp16_opt::thread_rc4_V_fu_19899_p2() {
    rc4_V_fu_19899_p2 = (!ap_const_lv5_1.is_01() || !ap_phi_mux_p_67_phi_fu_4035_p4.read().is_01())? sc_lv<5>(): (sc_biguint<5>(ap_const_lv5_1) + sc_biguint<5>(ap_phi_mux_p_67_phi_fu_4035_p4.read()));
}

void mnist_fp16_opt::thread_rc_V_fu_8768_p2() {
    rc_V_fu_8768_p2 = (!ap_const_lv3_1.is_01() || !ap_phi_mux_p_17_phi_fu_2768_p4.read().is_01())? sc_lv<3>(): (sc_biguint<3>(ap_const_lv3_1) + sc_biguint<3>(ap_phi_mux_p_17_phi_fu_2768_p4.read()));
}

void mnist_fp16_opt::thread_reducer4_fu_11009_p3() {
    reducer4_fu_11009_p3 = (!tmp_362_fu_11003_p2.read()[0].is_01())? sc_lv<32>(): ((tmp_362_fu_11003_p2.read()[0].to_bool())? tmp_313_reg_2955.read(): p_03_i1_reg_24875.read());
}

void mnist_fp16_opt::thread_reducer7_fu_16964_p3() {
    reducer7_fu_16964_p3 = (!tmp_601_fu_16958_p2.read()[0].is_01())? sc_lv<32>(): ((tmp_601_fu_16958_p2.read()[0].to_bool())? tmp_588_reg_3607.read(): p_03_i3_reg_26470.read());
}

void mnist_fp16_opt::thread_reducer94_fu_22552_p3() {
    reducer94_fu_22552_p3 = (!tmp_657_fu_22546_p2.read()[0].is_01())? sc_lv<32>(): ((tmp_657_fu_22546_p2.read()[0].to_bool())? compute14_reg_4245.read(): reg_4515.read());
}

void mnist_fp16_opt::thread_reducer9_fu_21874_p3() {
    reducer9_fu_21874_p3 = (!tmp_648_fu_21868_p2.read()[0].is_01())? sc_lv<32>(): ((tmp_648_fu_21868_p2.read()[0].to_bool())? tmp_457_reg_4175.read(): p_03_i5_reg_27812.read());
}

void mnist_fp16_opt::thread_relu1_0_V_address0() {
    if ((esl_seteq<1,1,1>(ap_block_pp6_stage0.read(), ap_const_boolean_0) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp6_iter21.read()))) {
        relu1_0_V_address0 =  (sc_lv<12>) (tmp_718_cast_fu_8376_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_block_pp5_stage0.read(), ap_const_boolean_0) && 
                esl_seteq<1,1,1>(ap_enable_reg_pp5_iter6.read(), ap_const_logic_1))) {
        relu1_0_V_address0 =  (sc_lv<12>) (tmp_364_cast_reg_23955_pp5_iter5_reg.read());
    } else {
        relu1_0_V_address0 = "XXXXXXXXXXXX";
    }
}

void mnist_fp16_opt::thread_relu1_0_V_ce0() {
    if (((esl_seteq<1,1,1>(ap_block_pp6_stage0_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp6_iter21.read())) || 
         (esl_seteq<1,1,1>(ap_block_pp5_stage0_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_enable_reg_pp5_iter6.read(), ap_const_logic_1)))) {
        relu1_0_V_ce0 = ap_const_logic_1;
    } else {
        relu1_0_V_ce0 = ap_const_logic_0;
    }
}

void mnist_fp16_opt::thread_relu1_0_V_d0() {
    relu1_0_V_d0 = (!or_cond2_fu_7715_p2.read()[0].is_01())? sc_lv<16>(): ((or_cond2_fu_7715_p2.read()[0].to_bool())? newSel2_fu_7707_p3.read(): ap_const_lv16_0);
}

void mnist_fp16_opt::thread_relu1_0_V_we0() {
    if ((esl_seteq<1,1,1>(ap_block_pp5_stage0_11001.read(), ap_const_boolean_0) && 
         esl_seteq<1,1,1>(ap_enable_reg_pp5_iter6.read(), ap_const_logic_1) && 
         esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_flatten3_reg_23911_pp5_iter5_reg.read()))) {
        relu1_0_V_we0 = ap_const_logic_1;
    } else {
        relu1_0_V_we0 = ap_const_logic_0;
    }
}

void mnist_fp16_opt::thread_relu2_0_V_address0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp9_stage0.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp9_iter1.read()) && 
         esl_seteq<1,1,1>(ap_block_pp9_stage0.read(), ap_const_boolean_0))) {
        relu2_0_V_address0 =  (sc_lv<12>) (tmp_808_cast_fu_10716_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_block_pp8_stage0.read(), ap_const_boolean_0) && 
                esl_seteq<1,1,1>(ap_enable_reg_pp8_iter6.read(), ap_const_logic_1))) {
        relu2_0_V_address0 =  (sc_lv<12>) (tmp_735_cast_reg_24623_pp8_iter5_reg.read());
    } else {
        relu2_0_V_address0 = "XXXXXXXXXXXX";
    }
}

void mnist_fp16_opt::thread_relu2_0_V_ce0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp9_stage0.read()) && 
          esl_seteq<1,1,1>(ap_block_pp9_stage0_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp9_iter1.read())) || 
         (esl_seteq<1,1,1>(ap_block_pp8_stage0_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_enable_reg_pp8_iter6.read(), ap_const_logic_1)))) {
        relu2_0_V_ce0 = ap_const_logic_1;
    } else {
        relu2_0_V_ce0 = ap_const_logic_0;
    }
}

void mnist_fp16_opt::thread_relu2_0_V_d0() {
    relu2_0_V_d0 = (!or_cond5_fu_10352_p2.read()[0].is_01())? sc_lv<16>(): ((or_cond5_fu_10352_p2.read()[0].to_bool())? newSel6_fu_10344_p3.read(): ap_const_lv16_0);
}

void mnist_fp16_opt::thread_relu2_0_V_we0() {
    if ((esl_seteq<1,1,1>(ap_block_pp8_stage0_11001.read(), ap_const_boolean_0) && 
         esl_seteq<1,1,1>(ap_enable_reg_pp8_iter6.read(), ap_const_logic_1) && 
         esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_flatten9_reg_24579_pp8_iter5_reg.read()))) {
        relu2_0_V_we0 = ap_const_logic_1;
    } else {
        relu2_0_V_we0 = ap_const_logic_0;
    }
}

void mnist_fp16_opt::thread_relu3_0_V_address0() {
    if ((esl_seteq<1,1,1>(ap_block_pp13_stage0.read(), ap_const_boolean_0) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp13_iter19.read()))) {
        relu3_0_V_address0 =  (sc_lv<11>) (tmp_944_cast_fu_14410_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_block_pp12_stage0.read(), ap_const_boolean_0) && 
                esl_seteq<1,1,1>(ap_enable_reg_pp12_iter6.read(), ap_const_logic_1))) {
        relu3_0_V_address0 =  (sc_lv<11>) (tmp_878_cast_reg_25579_pp12_iter5_reg.read());
    } else {
        relu3_0_V_address0 =  (sc_lv<11>) ("XXXXXXXXXXX");
    }
}

void mnist_fp16_opt::thread_relu3_0_V_ce0() {
    if (((esl_seteq<1,1,1>(ap_block_pp13_stage0_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp13_iter19.read())) || 
         (esl_seteq<1,1,1>(ap_block_pp12_stage0_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_enable_reg_pp12_iter6.read(), ap_const_logic_1)))) {
        relu3_0_V_ce0 = ap_const_logic_1;
    } else {
        relu3_0_V_ce0 = ap_const_logic_0;
    }
}

void mnist_fp16_opt::thread_relu3_0_V_d0() {
    relu3_0_V_d0 = (!or_cond14_fu_13789_p2.read()[0].is_01())? sc_lv<16>(): ((or_cond14_fu_13789_p2.read()[0].to_bool())? newSel16_fu_13781_p3.read(): ap_const_lv16_0);
}

void mnist_fp16_opt::thread_relu3_0_V_we0() {
    if ((esl_seteq<1,1,1>(ap_block_pp12_stage0_11001.read(), ap_const_boolean_0) && 
         esl_seteq<1,1,1>(ap_enable_reg_pp12_iter6.read(), ap_const_logic_1) && 
         esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_flatten19_reg_25535_pp12_iter5_reg.read()))) {
        relu3_0_V_we0 = ap_const_logic_1;
    } else {
        relu3_0_V_we0 = ap_const_logic_0;
    }
}

void mnist_fp16_opt::thread_relu4_0_V_address0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp16_stage0.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp16_iter1.read()) && 
         esl_seteq<1,1,1>(ap_block_pp16_stage0.read(), ap_const_boolean_0))) {
        relu4_0_V_address0 =  (sc_lv<11>) (tmp_1107_cast_fu_16671_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_block_pp15_stage0.read(), ap_const_boolean_0) && 
                esl_seteq<1,1,1>(ap_enable_reg_pp15_iter6.read(), ap_const_logic_1))) {
        relu4_0_V_address0 =  (sc_lv<11>) (tmp_1016_cast_reg_26223_pp15_iter5_reg.read());
    } else {
        relu4_0_V_address0 =  (sc_lv<11>) ("XXXXXXXXXXX");
    }
}

void mnist_fp16_opt::thread_relu4_0_V_ce0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp16_stage0.read()) && 
          esl_seteq<1,1,1>(ap_block_pp16_stage0_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp16_iter1.read())) || 
         (esl_seteq<1,1,1>(ap_block_pp15_stage0_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_enable_reg_pp15_iter6.read(), ap_const_logic_1)))) {
        relu4_0_V_ce0 = ap_const_logic_1;
    } else {
        relu4_0_V_ce0 = ap_const_logic_0;
    }
}

void mnist_fp16_opt::thread_relu4_0_V_d0() {
    relu4_0_V_d0 = (!or_cond17_fu_16322_p2.read()[0].is_01())? sc_lv<16>(): ((or_cond17_fu_16322_p2.read()[0].to_bool())? newSel20_fu_16314_p3.read(): ap_const_lv16_0);
}

void mnist_fp16_opt::thread_relu4_0_V_we0() {
    if ((esl_seteq<1,1,1>(ap_block_pp15_stage0_11001.read(), ap_const_boolean_0) && 
         esl_seteq<1,1,1>(ap_enable_reg_pp15_iter6.read(), ap_const_logic_1) && 
         esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_flatten27_reg_26179_pp15_iter5_reg.read()))) {
        relu4_0_V_we0 = ap_const_logic_1;
    } else {
        relu4_0_V_we0 = ap_const_logic_0;
    }
}

void mnist_fp16_opt::thread_relu5_0_V_address0() {
    if ((esl_seteq<1,1,1>(ap_block_pp20_stage0.read(), ap_const_boolean_0) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp20_iter18.read()))) {
        relu5_0_V_address0 =  (sc_lv<10>) (tmp_1206_cast_fu_19505_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_block_pp19_stage0.read(), ap_const_boolean_0) && 
                esl_seteq<1,1,1>(ap_enable_reg_pp19_iter6.read(), ap_const_logic_1))) {
        relu5_0_V_address0 =  (sc_lv<10>) (tmp_1153_cast_reg_26988_pp19_iter5_reg.read());
    } else {
        relu5_0_V_address0 =  (sc_lv<10>) ("XXXXXXXXXX");
    }
}

void mnist_fp16_opt::thread_relu5_0_V_ce0() {
    if (((esl_seteq<1,1,1>(ap_block_pp20_stage0_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp20_iter18.read())) || 
         (esl_seteq<1,1,1>(ap_block_pp19_stage0_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_enable_reg_pp19_iter6.read(), ap_const_logic_1)))) {
        relu5_0_V_ce0 = ap_const_logic_1;
    } else {
        relu5_0_V_ce0 = ap_const_logic_0;
    }
}

void mnist_fp16_opt::thread_relu5_0_V_d0() {
    relu5_0_V_d0 = (!or_cond26_fu_18935_p2.read()[0].is_01())? sc_lv<16>(): ((or_cond26_fu_18935_p2.read()[0].to_bool())? newSel30_fu_18927_p3.read(): ap_const_lv16_0);
}

void mnist_fp16_opt::thread_relu5_0_V_we0() {
    if ((esl_seteq<1,1,1>(ap_block_pp19_stage0_11001.read(), ap_const_boolean_0) && 
         esl_seteq<1,1,1>(ap_enable_reg_pp19_iter6.read(), ap_const_logic_1) && 
         esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_flatten37_reg_26944_pp19_iter5_reg.read()))) {
        relu5_0_V_we0 = ap_const_logic_1;
    } else {
        relu5_0_V_we0 = ap_const_logic_0;
    }
}

void mnist_fp16_opt::thread_relu6_0_V_address0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp23_stage0.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp23_iter1.read()) && 
         esl_seteq<1,1,1>(ap_block_pp23_stage0.read(), ap_const_boolean_0))) {
        relu6_0_V_address0 =  (sc_lv<10>) (tmp_1258_cast_fu_21582_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_block_pp22_stage0.read(), ap_const_boolean_0) && 
                esl_seteq<1,1,1>(ap_enable_reg_pp22_iter6.read(), ap_const_logic_1))) {
        relu6_0_V_address0 =  (sc_lv<10>) (tmp_1237_cast_reg_27618_pp22_iter5_reg.read());
    } else {
        relu6_0_V_address0 =  (sc_lv<10>) ("XXXXXXXXXX");
    }
}

void mnist_fp16_opt::thread_relu6_0_V_ce0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp23_stage0.read()) && 
          esl_seteq<1,1,1>(ap_block_pp23_stage0_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp23_iter1.read())) || 
         (esl_seteq<1,1,1>(ap_block_pp22_stage0_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_enable_reg_pp22_iter6.read(), ap_const_logic_1)))) {
        relu6_0_V_ce0 = ap_const_logic_1;
    } else {
        relu6_0_V_ce0 = ap_const_logic_0;
    }
}

void mnist_fp16_opt::thread_relu6_0_V_d0() {
    relu6_0_V_d0 = (!or_cond32_fu_21438_p2.read()[0].is_01())? sc_lv<16>(): ((or_cond32_fu_21438_p2.read()[0].to_bool())? newSel34_fu_21430_p3.read(): ap_const_lv16_0);
}

void mnist_fp16_opt::thread_relu6_0_V_we0() {
    if ((esl_seteq<1,1,1>(ap_block_pp22_stage0_11001.read(), ap_const_boolean_0) && 
         esl_seteq<1,1,1>(ap_enable_reg_pp22_iter6.read(), ap_const_logic_1) && 
         esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_flatten45_reg_27574_pp22_iter5_reg.read()))) {
        relu6_0_V_we0 = ap_const_logic_1;
    } else {
        relu6_0_V_we0 = ap_const_logic_0;
    }
}

void mnist_fp16_opt::thread_result_V() {
    result_V = p_77_reg_4302.read();
}

void mnist_fp16_opt::thread_result_V_ap_vld() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state514.read())) {
        result_V_ap_vld = ap_const_logic_1;
    } else {
        result_V_ap_vld = ap_const_logic_0;
    }
}

void mnist_fp16_opt::thread_rhs_V_10_cast_fu_13030_p1() {
    rhs_V_10_cast_fu_13030_p1 = esl_zext<4,2>(ap_phi_mux_p_37_phi_fu_3193_p4.read());
}

void mnist_fp16_opt::thread_rhs_V_10_cast_mid1_fu_13155_p1() {
    rhs_V_10_cast_mid1_fu_13155_p1 = esl_zext<4,2>(ry2_V_fu_13135_p2.read());
}

void mnist_fp16_opt::thread_rhs_V_11_cast_fu_13224_p1() {
    rhs_V_11_cast_fu_13224_p1 = esl_zext<4,2>(p_40_mid2_reg_25468.read());
}

void mnist_fp16_opt::thread_rhs_V_13_cast_fu_17120_p1() {
    rhs_V_13_cast_fu_17120_p1 = esl_sext<10,8>(rhs_V_4_fu_17115_p2.read());
}

void mnist_fp16_opt::thread_rhs_V_14_cast_fu_14791_p1() {
    rhs_V_14_cast_fu_14791_p1 = esl_zext<4,2>(ap_phi_mux_p_48_phi_fu_3442_p4.read());
}

void mnist_fp16_opt::thread_rhs_V_14_cast_mid1_fu_14916_p1() {
    rhs_V_14_cast_mid1_fu_14916_p1 = esl_zext<4,2>(ry3_V_fu_14896_p2.read());
}

void mnist_fp16_opt::thread_rhs_V_15_cast_fu_14991_p1() {
    rhs_V_15_cast_fu_14991_p1 = esl_zext<4,2>(p_51_mid2_reg_25930.read());
}

void mnist_fp16_opt::thread_rhs_V_17_cast_fu_18982_p1() {
    rhs_V_17_cast_fu_18982_p1 = esl_sext<11,8>(rhs_V_7_fu_18976_p2.read());
}

void mnist_fp16_opt::thread_rhs_V_17_cast_mid1_fu_19138_p1() {
    rhs_V_17_cast_mid1_fu_19138_p1 = esl_sext<11,8>(rhs_V_7_mid1_fu_19132_p2.read());
}

void mnist_fp16_opt::thread_rhs_V_18_cast_fu_18147_p1() {
    rhs_V_18_cast_fu_18147_p1 = esl_zext<4,2>(ap_phi_mux_p_63_phi_fu_3808_p4.read());
}

void mnist_fp16_opt::thread_rhs_V_18_cast_mid1_fu_18243_p1() {
    rhs_V_18_cast_mid1_fu_18243_p1 = esl_zext<4,2>(ry4_V_fu_18223_p2.read());
}

void mnist_fp16_opt::thread_rhs_V_19_cast_fu_18394_p1() {
    rhs_V_19_cast_fu_18394_p1 = esl_zext<4,2>(p_66_mid2_reg_26875.read());
}

void mnist_fp16_opt::thread_rhs_V_1_fu_4736_p1() {
    rhs_V_1_fu_4736_p1 = esl_sext<12,11>(r_V_reg_23218.read());
}

void mnist_fp16_opt::thread_rhs_V_20_cast_fu_19878_p1() {
    rhs_V_20_cast_fu_19878_p1 = esl_zext<4,2>(ap_phi_mux_p_69_phi_fu_4057_p4.read());
}

void mnist_fp16_opt::thread_rhs_V_20_cast_mid1_fu_19974_p1() {
    rhs_V_20_cast_mid1_fu_19974_p1 = esl_zext<4,2>(ry5_V_fu_19954_p2.read());
}

void mnist_fp16_opt::thread_rhs_V_21_cast_fu_20131_p1() {
    rhs_V_21_cast_fu_20131_p1 = esl_zext<4,2>(p_72_mid2_reg_27323_pp21_iter1_reg.read());
}

void mnist_fp16_opt::thread_rhs_V_2_mid2_fu_17064_p0() {
    rhs_V_2_mid2_fu_17064_p0 =  (sc_lv<4>) (rhs_V_2_mid2_fu_17064_p00.read());
}

void mnist_fp16_opt::thread_rhs_V_2_mid2_fu_17064_p00() {
    rhs_V_2_mid2_fu_17064_p00 = esl_zext<10,4>(lhs_V_10_mid2_v_reg_26508.read());
}

void mnist_fp16_opt::thread_rhs_V_2_mid2_fu_17064_p2() {
    rhs_V_2_mid2_fu_17064_p2 = (!rhs_V_2_mid2_fu_17064_p0.read().is_01() || !ap_const_lv10_31.is_01())? sc_lv<10>(): sc_biguint<4>(rhs_V_2_mid2_fu_17064_p0.read()) * sc_biguint<10>(ap_const_lv10_31);
}

void mnist_fp16_opt::thread_rhs_V_3_fu_11170_p1() {
    rhs_V_3_fu_11170_p1 = esl_sext<12,9>(r_V_6_reg_24937.read());
}

void mnist_fp16_opt::thread_rhs_V_4_cast_fu_8963_p1() {
    rhs_V_4_cast_fu_8963_p1 = esl_zext<5,2>(p_25_mid2_fu_8891_p3.read());
}

void mnist_fp16_opt::thread_rhs_V_4_fu_17115_p2() {
    rhs_V_4_fu_17115_p2 = (!tmp_294_cast_fu_17111_p1.read().is_01() || !lhs_V_7_cast_reg_26514.read().is_01())? sc_lv<8>(): (sc_biguint<8>(tmp_294_cast_fu_17111_p1.read()) - sc_biguint<8>(lhs_V_7_cast_reg_26514.read()));
}

void mnist_fp16_opt::thread_rhs_V_5_cast_fu_7770_p1() {
    rhs_V_5_cast_fu_7770_p1 = esl_sext<13,11>(r_V_3_fu_7764_p2.read());
}

void mnist_fp16_opt::thread_rhs_V_5_cast_mid1_fu_8006_p1() {
    rhs_V_5_cast_mid1_fu_8006_p1 = esl_sext<13,11>(r_V_4_mid1_fu_8000_p2.read());
}

void mnist_fp16_opt::thread_rhs_V_5_fu_18954_p0() {
    rhs_V_5_fu_18954_p0 =  (sc_lv<5>) (rhs_V_5_fu_18954_p00.read());
}

void mnist_fp16_opt::thread_rhs_V_5_fu_18954_p00() {
    rhs_V_5_fu_18954_p00 = esl_zext<11,5>(ap_phi_mux_p_52_phi_fu_3909_p4.read());
}

void mnist_fp16_opt::thread_rhs_V_5_fu_18954_p2() {
    rhs_V_5_fu_18954_p2 = (!rhs_V_5_fu_18954_p0.read().is_01() || !ap_const_lv11_31.is_01())? sc_lv<11>(): sc_biguint<5>(rhs_V_5_fu_18954_p0.read()) * sc_biguint<11>(ap_const_lv11_31);
}

void mnist_fp16_opt::thread_rhs_V_5_mid1_fu_19042_p0() {
    rhs_V_5_mid1_fu_19042_p0 =  (sc_lv<5>) (rhs_V_5_mid1_fu_19042_p00.read());
}

void mnist_fp16_opt::thread_rhs_V_5_mid1_fu_19042_p00() {
    rhs_V_5_mid1_fu_19042_p00 = esl_zext<11,5>(not_zero4_V_fu_19010_p2.read());
}

void mnist_fp16_opt::thread_rhs_V_5_mid1_fu_19042_p2() {
    rhs_V_5_mid1_fu_19042_p2 = (!rhs_V_5_mid1_fu_19042_p0.read().is_01() || !ap_const_lv11_31.is_01())? sc_lv<11>(): sc_biguint<5>(rhs_V_5_mid1_fu_19042_p0.read()) * sc_biguint<11>(ap_const_lv11_31);
}

void mnist_fp16_opt::thread_rhs_V_5_mid2_fu_19048_p3() {
    rhs_V_5_mid2_fu_19048_p3 = (!exitcond_flatten42_fu_19016_p2.read()[0].is_01())? sc_lv<11>(): ((exitcond_flatten42_fu_19016_p2.read()[0].to_bool())? rhs_V_5_mid1_fu_19042_p2.read(): rhs_V_5_fu_18954_p2.read());
}

void mnist_fp16_opt::thread_rhs_V_6_fu_7734_p0() {
    rhs_V_6_fu_7734_p0 =  (sc_lv<3>) (rhs_V_6_fu_7734_p00.read());
}

void mnist_fp16_opt::thread_rhs_V_6_fu_7734_p00() {
    rhs_V_6_fu_7734_p00 = esl_zext<13,3>(ap_phi_mux_p_6_phi_fu_2638_p4.read());
}

void mnist_fp16_opt::thread_rhs_V_6_fu_7734_p2() {
    rhs_V_6_fu_7734_p2 = (!rhs_V_6_fu_7734_p0.read().is_01() || !ap_const_lv13_310.is_01())? sc_lv<13>(): sc_biguint<3>(rhs_V_6_fu_7734_p0.read()) * sc_biguint<13>(ap_const_lv13_310);
}

void mnist_fp16_opt::thread_rhs_V_6_mid1_fu_7921_p1() {
    rhs_V_6_mid1_fu_7921_p1 =  (sc_lv<3>) (rhs_V_6_mid1_fu_7921_p10.read());
}

void mnist_fp16_opt::thread_rhs_V_6_mid1_fu_7921_p10() {
    rhs_V_6_mid1_fu_7921_p10 = esl_zext<13,3>(not_zero_V_reg_24062.read());
}

void mnist_fp16_opt::thread_rhs_V_6_mid1_fu_7921_p2() {
    rhs_V_6_mid1_fu_7921_p2 = (!ap_const_lv13_310.is_01() || !rhs_V_6_mid1_fu_7921_p1.read().is_01())? sc_lv<13>(): sc_biguint<13>(ap_const_lv13_310) * sc_biguint<3>(rhs_V_6_mid1_fu_7921_p1.read());
}

void mnist_fp16_opt::thread_rhs_V_6_mid2_fu_7927_p3() {
    rhs_V_6_mid2_fu_7927_p3 = (!exitcond_flatten6_reg_24067.read()[0].is_01())? sc_lv<13>(): ((exitcond_flatten6_reg_24067.read()[0].to_bool())? rhs_V_6_mid1_fu_7921_p2.read(): rhs_V_6_reg_24043.read());
}

void mnist_fp16_opt::thread_rhs_V_7_cast_fu_8747_p1() {
    rhs_V_7_cast_fu_8747_p1 = esl_zext<5,2>(ap_phi_mux_p_21_phi_fu_2790_p4.read());
}

void mnist_fp16_opt::thread_rhs_V_7_cast_mid1_fu_8899_p1() {
    rhs_V_7_cast_mid1_fu_8899_p1 = esl_zext<5,2>(ry1_V_fu_8879_p2.read());
}

void mnist_fp16_opt::thread_rhs_V_7_fu_18976_p2() {
    rhs_V_7_fu_18976_p2 = (!tmp_363_cast_fu_18972_p1.read().is_01() || !tmp_362_cast1_fu_18960_p1.read().is_01())? sc_lv<8>(): (sc_biguint<8>(tmp_363_cast_fu_18972_p1.read()) - sc_biguint<8>(tmp_362_cast1_fu_18960_p1.read()));
}

void mnist_fp16_opt::thread_rhs_V_7_mid1_fu_19132_p2() {
    rhs_V_7_mid1_fu_19132_p2 = (!tmp_363_cast_mid1_fu_19128_p1.read().is_01() || !tmp_362_cast1_mid1_fu_19116_p1.read().is_01())? sc_lv<8>(): (sc_biguint<8>(tmp_363_cast_mid1_fu_19128_p1.read()) - sc_biguint<8>(tmp_362_cast1_mid1_fu_19116_p1.read()));
}

void mnist_fp16_opt::thread_rhs_V_8_fu_13808_p1() {
    rhs_V_8_fu_13808_p1 =  (sc_lv<4>) (rhs_V_8_fu_13808_p10.read());
}

void mnist_fp16_opt::thread_rhs_V_8_fu_13808_p10() {
    rhs_V_8_fu_13808_p10 = esl_zext<12,4>(ap_phi_mux_p_26_phi_fu_3294_p4.read());
}

void mnist_fp16_opt::thread_rhs_V_8_fu_13808_p2() {
    rhs_V_8_fu_13808_p2 = (!ap_const_lv12_C4.is_01() || !rhs_V_8_fu_13808_p1.read().is_01())? sc_lv<12>(): sc_biguint<12>(ap_const_lv12_C4) * sc_biguint<4>(rhs_V_8_fu_13808_p1.read());
}

void mnist_fp16_opt::thread_rhs_V_8_mid1_fu_13918_p1() {
    rhs_V_8_mid1_fu_13918_p1 =  (sc_lv<4>) (rhs_V_8_mid1_fu_13918_p10.read());
}

void mnist_fp16_opt::thread_rhs_V_8_mid1_fu_13918_p10() {
    rhs_V_8_mid1_fu_13918_p10 = esl_zext<12,4>(not_zero2_V_fu_13886_p2.read());
}

void mnist_fp16_opt::thread_rhs_V_8_mid1_fu_13918_p2() {
    rhs_V_8_mid1_fu_13918_p2 = (!ap_const_lv12_C4.is_01() || !rhs_V_8_mid1_fu_13918_p1.read().is_01())? sc_lv<12>(): sc_biguint<12>(ap_const_lv12_C4) * sc_biguint<4>(rhs_V_8_mid1_fu_13918_p1.read());
}

void mnist_fp16_opt::thread_rhs_V_8_mid2_fu_13924_p3() {
    rhs_V_8_mid2_fu_13924_p3 = (!exitcond_flatten24_fu_13892_p2.read()[0].is_01())? sc_lv<12>(): ((exitcond_flatten24_fu_13892_p2.read()[0].to_bool())? rhs_V_8_mid1_fu_13918_p2.read(): rhs_V_8_fu_13808_p2.read());
}

void mnist_fp16_opt::thread_rhs_V_9_cast_fu_13858_p1() {
    rhs_V_9_cast_fu_13858_p1 = esl_sext<12,9>(r_V_10_fu_13852_p2.read());
}

void mnist_fp16_opt::thread_rhs_V_9_cast_mid1_fu_14028_p1() {
    rhs_V_9_cast_mid1_fu_14028_p1 = esl_sext<12,9>(r_V_10_mid1_fu_14022_p2.read());
}

void mnist_fp16_opt::thread_rhs_V_cast_fu_7100_p1() {
    rhs_V_cast_fu_7100_p1 = esl_zext<5,2>(p_13_mid2_fu_7075_p3.read());
}

void mnist_fp16_opt::thread_rx1_V_fu_8972_p2() {
    rx1_V_fu_8972_p2 = (!ap_const_lv2_1.is_01() || !p_25_mid2_fu_8891_p3.read().is_01())? sc_lv<2>(): (sc_biguint<2>(ap_const_lv2_1) + sc_biguint<2>(p_25_mid2_fu_8891_p3.read()));
}

void mnist_fp16_opt::thread_rx2_V_fu_13259_p2() {
    rx2_V_fu_13259_p2 = (!ap_const_lv2_1.is_01() || !p_40_mid2_reg_25468.read().is_01())? sc_lv<2>(): (sc_biguint<2>(ap_const_lv2_1) + sc_biguint<2>(p_40_mid2_reg_25468.read()));
}

void mnist_fp16_opt::thread_rx3_V_fu_14959_p2() {
    rx3_V_fu_14959_p2 = (!ap_const_lv2_1.is_01() || !p_51_mid2_fu_14908_p3.read().is_01())? sc_lv<2>(): (sc_biguint<2>(ap_const_lv2_1) + sc_biguint<2>(p_51_mid2_fu_14908_p3.read()));
}

void mnist_fp16_opt::thread_rx4_V_fu_18421_p2() {
    rx4_V_fu_18421_p2 = (!ap_const_lv2_1.is_01() || !p_66_mid2_reg_26875.read().is_01())? sc_lv<2>(): (sc_biguint<2>(ap_const_lv2_1) + sc_biguint<2>(p_66_mid2_reg_26875.read()));
}

void mnist_fp16_opt::thread_rx5_V_fu_20006_p2() {
    rx5_V_fu_20006_p2 = (!ap_const_lv2_1.is_01() || !p_72_mid2_fu_19966_p3.read().is_01())? sc_lv<2>(): (sc_biguint<2>(ap_const_lv2_1) + sc_biguint<2>(p_72_mid2_fu_19966_p3.read()));
}

void mnist_fp16_opt::thread_rx_V_fu_7185_p2() {
    rx_V_fu_7185_p2 = (!ap_const_lv2_1.is_01() || !p_13_mid2_reg_23853.read().is_01())? sc_lv<2>(): (sc_biguint<2>(ap_const_lv2_1) + sc_biguint<2>(p_13_mid2_reg_23853.read()));
}

void mnist_fp16_opt::thread_ry1_V_fu_8879_p2() {
    ry1_V_fu_8879_p2 = (!ap_const_lv2_1.is_01() || !p_21_mid_fu_8780_p3.read().is_01())? sc_lv<2>(): (sc_biguint<2>(ap_const_lv2_1) + sc_biguint<2>(p_21_mid_fu_8780_p3.read()));
}

void mnist_fp16_opt::thread_ry2_V_fu_13135_p2() {
    ry2_V_fu_13135_p2 = (!ap_const_lv2_1.is_01() || !p_37_mid_fu_13063_p3.read().is_01())? sc_lv<2>(): (sc_biguint<2>(ap_const_lv2_1) + sc_biguint<2>(p_37_mid_fu_13063_p3.read()));
}

void mnist_fp16_opt::thread_ry3_V_fu_14896_p2() {
    ry3_V_fu_14896_p2 = (!ap_const_lv2_1.is_01() || !p_48_mid_fu_14824_p3.read().is_01())? sc_lv<2>(): (sc_biguint<2>(ap_const_lv2_1) + sc_biguint<2>(p_48_mid_fu_14824_p3.read()));
}

void mnist_fp16_opt::thread_ry4_V_fu_18223_p2() {
    ry4_V_fu_18223_p2 = (!ap_const_lv2_1.is_01() || !p_63_mid_fu_18180_p3.read().is_01())? sc_lv<2>(): (sc_biguint<2>(ap_const_lv2_1) + sc_biguint<2>(p_63_mid_fu_18180_p3.read()));
}

void mnist_fp16_opt::thread_ry5_V_fu_19954_p2() {
    ry5_V_fu_19954_p2 = (!ap_const_lv2_1.is_01() || !p_69_mid_fu_19911_p3.read().is_01())? sc_lv<2>(): (sc_biguint<2>(ap_const_lv2_1) + sc_biguint<2>(p_69_mid_fu_19911_p3.read()));
}

void mnist_fp16_opt::thread_ry_V_fu_7063_p2() {
    ry_V_fu_7063_p2 = (!ap_const_lv2_1.is_01() || !ap_phi_mux_p_4_phi_fu_2537_p4.read().is_01())? sc_lv<2>(): (sc_biguint<2>(ap_const_lv2_1) + sc_biguint<2>(ap_phi_mux_p_4_phi_fu_2537_p4.read()));
}

void mnist_fp16_opt::thread_sel_tmp100_fu_20481_p2() {
    sel_tmp100_fu_20481_p2 = (tmp_494_reg_27436.read() & sel_tmp99_fu_20476_p2.read());
}

void mnist_fp16_opt::thread_sel_tmp101_fu_20320_p2() {
    sel_tmp101_fu_20320_p2 = (sel_tmp242_demorgan_fu_20315_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp16_opt::thread_sel_tmp102_fu_20326_p2() {
    sel_tmp102_fu_20326_p2 = (tmp_491_fu_20263_p2.read() & sel_tmp101_fu_20320_p2.read());
}

void mnist_fp16_opt::thread_sel_tmp103_fu_20486_p2() {
    sel_tmp103_fu_20486_p2 = (tmp_520_fu_20438_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp16_opt::thread_sel_tmp104_fu_20492_p2() {
    sel_tmp104_fu_20492_p2 = (sel_tmp102_reg_27447.read() & sel_tmp103_fu_20486_p2.read());
}

void mnist_fp16_opt::thread_sel_tmp105_fu_20497_p2() {
    sel_tmp105_fu_20497_p2 = (sel_tmp102_reg_27447.read() & tmp_520_fu_20438_p2.read());
}

void mnist_fp16_opt::thread_sel_tmp106_fu_20338_p2() {
    sel_tmp106_fu_20338_p2 = (sel_tmp257_demorgan_fu_20332_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp16_opt::thread_sel_tmp107_fu_20344_p2() {
    sel_tmp107_fu_20344_p2 = (icmp18_fu_20309_p2.read() & sel_tmp106_fu_20338_p2.read());
}

void mnist_fp16_opt::thread_sel_tmp108_fu_20590_p2() {
    sel_tmp108_fu_20590_p2 = (tmp_549_reg_27419_pp21_iter5_reg.read() ^ ap_const_lv1_1);
}

void mnist_fp16_opt::thread_sel_tmp109_fu_20595_p2() {
    sel_tmp109_fu_20595_p2 = (tmp_559_reg_27476.read() & sel_tmp108_fu_20590_p2.read());
}

void mnist_fp16_opt::thread_sel_tmp10_fu_10298_p2() {
    sel_tmp10_fu_10298_p2 = (tmp_126_reg_24688.read() & sel_tmp5_fu_10293_p2.read());
}

void mnist_fp16_opt::thread_sel_tmp110_demorgan_fu_13654_p2() {
    sel_tmp110_demorgan_fu_13654_p2 = (tmp_212_reg_25627.read() | tmp_445_fu_13628_p2.read());
}

void mnist_fp16_opt::thread_sel_tmp110_fu_20604_p2() {
    sel_tmp110_fu_20604_p2 = (sel_tmp266_demorgan_fu_20600_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp16_opt::thread_sel_tmp111_fu_20610_p2() {
    sel_tmp111_fu_20610_p2 = (tmp_556_reg_27464.read() & sel_tmp110_fu_20604_p2.read());
}

void mnist_fp16_opt::thread_sel_tmp112_fu_20615_p2() {
    sel_tmp112_fu_20615_p2 = (tmp_568_fu_20552_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp16_opt::thread_sel_tmp113_fu_20621_p2() {
    sel_tmp113_fu_20621_p2 = (sel_tmp111_fu_20610_p2.read() & sel_tmp112_fu_20615_p2.read());
}

void mnist_fp16_opt::thread_sel_tmp114_fu_20627_p2() {
    sel_tmp114_fu_20627_p2 = (sel_tmp111_fu_20610_p2.read() & tmp_568_fu_20552_p2.read());
}

void mnist_fp16_opt::thread_sel_tmp115_fu_20638_p2() {
    sel_tmp115_fu_20638_p2 = (sel_tmp281_demorgan_fu_20633_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp16_opt::thread_sel_tmp116_fu_20644_p2() {
    sel_tmp116_fu_20644_p2 = (icmp19_reg_27488.read() & sel_tmp115_fu_20638_p2.read());
}

void mnist_fp16_opt::thread_sel_tmp11_fu_10222_p2() {
    sel_tmp11_fu_10222_p2 = (sel_tmp83_demorgan_fu_10217_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp16_opt::thread_sel_tmp125_demorgan_fu_13671_p2() {
    sel_tmp125_demorgan_fu_13671_p2 = (sel_tmp110_demorgan_fu_13654_p2.read() | tmp_423_fu_13602_p2.read());
}

void mnist_fp16_opt::thread_sel_tmp12_fu_10228_p2() {
    sel_tmp12_fu_10228_p2 = (tmp_123_fu_10165_p2.read() & sel_tmp11_fu_10222_p2.read());
}

void mnist_fp16_opt::thread_sel_tmp137_demorgan_fu_15354_p2() {
    sel_tmp137_demorgan_fu_15354_p2 = (tmp_286_reg_26002_pp14_iter4_reg.read() | tmp_311_reg_26047.read());
}

void mnist_fp16_opt::thread_sel_tmp13_fu_10303_p2() {
    sel_tmp13_fu_10303_p2 = (tmp_278_fu_10255_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp16_opt::thread_sel_tmp14_fu_10309_p2() {
    sel_tmp14_fu_10309_p2 = (sel_tmp12_reg_24699.read() & sel_tmp13_fu_10303_p2.read());
}

void mnist_fp16_opt::thread_sel_tmp152_demorgan_fu_15387_p2() {
    sel_tmp152_demorgan_fu_15387_p2 = (sel_tmp137_demorgan_fu_15354_p2.read() | tmp_308_reg_26035.read());
}

void mnist_fp16_opt::thread_sel_tmp15_fu_10314_p2() {
    sel_tmp15_fu_10314_p2 = (sel_tmp12_reg_24699.read() & tmp_278_fu_10255_p2.read());
}

void mnist_fp16_opt::thread_sel_tmp161_demorgan_fu_15268_p2() {
    sel_tmp161_demorgan_fu_15268_p2 = (tmp_351_reg_26024.read() | tmp_357_fu_15242_p2.read());
}

void mnist_fp16_opt::thread_sel_tmp16_fu_10240_p2() {
    sel_tmp16_fu_10240_p2 = (sel_tmp98_demorgan_fu_10234_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp16_opt::thread_sel_tmp176_demorgan_fu_15285_p2() {
    sel_tmp176_demorgan_fu_15285_p2 = (sel_tmp161_demorgan_fu_15268_p2.read() | tmp_354_fu_15216_p2.read());
}

void mnist_fp16_opt::thread_sel_tmp17_fu_10246_p2() {
    sel_tmp17_fu_10246_p2 = (icmp2_fu_10211_p2.read() & sel_tmp16_fu_10240_p2.read());
}

void mnist_fp16_opt::thread_sel_tmp188_demorgan_fu_16187_p2() {
    sel_tmp188_demorgan_fu_16187_p2 = (tmp_555_reg_26271.read() | tmp_304_fu_16161_p2.read());
}

void mnist_fp16_opt::thread_sel_tmp18_fu_9339_p2() {
    sel_tmp18_fu_9339_p2 = (tmp_108_reg_24402_pp7_iter4_reg.read() ^ ap_const_lv1_1);
}

void mnist_fp16_opt::thread_sel_tmp19_fu_9344_p2() {
    sel_tmp19_fu_9344_p2 = (tmp_133_reg_24447.read() & sel_tmp18_fu_9339_p2.read());
}

void mnist_fp16_opt::thread_sel_tmp1_fu_7656_p2() {
    sel_tmp1_fu_7656_p2 = (tmp_94_reg_24003_pp5_iter5_reg.read() ^ ap_const_lv1_1);
}

void mnist_fp16_opt::thread_sel_tmp203_demorgan_fu_16204_p2() {
    sel_tmp203_demorgan_fu_16204_p2 = (sel_tmp188_demorgan_fu_16187_p2.read() | tmp_301_fu_16135_p2.read());
}

void mnist_fp16_opt::thread_sel_tmp20_fu_9353_p2() {
    sel_tmp20_fu_9353_p2 = (sel_tmp32_demorgan_fu_9349_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp16_opt::thread_sel_tmp215_demorgan_fu_18800_p2() {
    sel_tmp215_demorgan_fu_18800_p2 = (tmp_390_reg_27036.read() | tmp_613_fu_18774_p2.read());
}

void mnist_fp16_opt::thread_sel_tmp21_demorgan_fu_7597_p2() {
    sel_tmp21_demorgan_fu_7597_p2 = (sel_tmp6_demorgan_fu_7580_p2.read() | tmp_107_fu_7528_p2.read());
}

void mnist_fp16_opt::thread_sel_tmp21_fu_9359_p2() {
    sel_tmp21_fu_9359_p2 = (tmp_130_reg_24435.read() & sel_tmp20_fu_9353_p2.read());
}

void mnist_fp16_opt::thread_sel_tmp22_fu_9364_p2() {
    sel_tmp22_fu_9364_p2 = (tmp_144_fu_9301_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp16_opt::thread_sel_tmp230_demorgan_fu_18817_p2() {
    sel_tmp230_demorgan_fu_18817_p2 = (sel_tmp215_demorgan_fu_18800_p2.read() | tmp_610_fu_18748_p2.read());
}

void mnist_fp16_opt::thread_sel_tmp23_fu_9370_p2() {
    sel_tmp23_fu_9370_p2 = (sel_tmp21_fu_9359_p2.read() & sel_tmp22_fu_9364_p2.read());
}

void mnist_fp16_opt::thread_sel_tmp242_demorgan_fu_20315_p2() {
    sel_tmp242_demorgan_fu_20315_p2 = (tmp_478_reg_27397.read() | tmp_494_fu_20289_p2.read());
}

void mnist_fp16_opt::thread_sel_tmp24_fu_9376_p2() {
    sel_tmp24_fu_9376_p2 = (sel_tmp21_fu_9359_p2.read() & tmp_144_fu_9301_p2.read());
}

void mnist_fp16_opt::thread_sel_tmp257_demorgan_fu_20332_p2() {
    sel_tmp257_demorgan_fu_20332_p2 = (sel_tmp242_demorgan_fu_20315_p2.read() | tmp_491_fu_20263_p2.read());
}

void mnist_fp16_opt::thread_sel_tmp25_fu_9387_p2() {
    sel_tmp25_fu_9387_p2 = (sel_tmp47_demorgan_fu_9382_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp16_opt::thread_sel_tmp266_demorgan_fu_20600_p2() {
    sel_tmp266_demorgan_fu_20600_p2 = (tmp_549_reg_27419_pp21_iter5_reg.read() | tmp_559_reg_27476.read());
}

void mnist_fp16_opt::thread_sel_tmp26_fu_9393_p2() {
    sel_tmp26_fu_9393_p2 = (icmp4_reg_24459.read() & sel_tmp25_fu_9387_p2.read());
}

void mnist_fp16_opt::thread_sel_tmp27_fu_9488_p2() {
    sel_tmp27_fu_9488_p2 = (tmp_173_reg_24424_pp7_iter4_reg.read() ^ ap_const_lv1_1);
}

void mnist_fp16_opt::thread_sel_tmp281_demorgan_fu_20633_p2() {
    sel_tmp281_demorgan_fu_20633_p2 = (sel_tmp266_demorgan_fu_20600_p2.read() | tmp_556_reg_27464.read());
}

void mnist_fp16_opt::thread_sel_tmp28_fu_9493_p2() {
    sel_tmp28_fu_9493_p2 = (tmp_179_reg_24481.read() & sel_tmp27_fu_9488_p2.read());
}

void mnist_fp16_opt::thread_sel_tmp293_demorgan_fu_21303_p2() {
    sel_tmp293_demorgan_fu_21303_p2 = (tmp_471_reg_27666.read() | tmp_487_fu_21277_p2.read());
}

void mnist_fp16_opt::thread_sel_tmp29_fu_9502_p2() {
    sel_tmp29_fu_9502_p2 = (sel_tmp56_demorgan_fu_9498_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp16_opt::thread_sel_tmp2_fu_7661_p2() {
    sel_tmp2_fu_7661_p2 = (tmp_122_reg_24020.read() & sel_tmp1_fu_7656_p2.read());
}

void mnist_fp16_opt::thread_sel_tmp308_demorgan_fu_21320_p2() {
    sel_tmp308_demorgan_fu_21320_p2 = (sel_tmp293_demorgan_fu_21303_p2.read() | tmp_484_fu_21251_p2.read());
}

void mnist_fp16_opt::thread_sel_tmp30_fu_9508_p2() {
    sel_tmp30_fu_9508_p2 = (tmp_176_reg_24469.read() & sel_tmp29_fu_9502_p2.read());
}

void mnist_fp16_opt::thread_sel_tmp31_fu_9513_p2() {
    sel_tmp31_fu_9513_p2 = (tmp_203_fu_9450_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp16_opt::thread_sel_tmp321_demorgan_fu_22018_p2() {
    sel_tmp321_demorgan_fu_22018_p2 = (tmp_437_fu_21955_p2.read() | tmp_456_fu_21992_p2.read());
}

void mnist_fp16_opt::thread_sel_tmp32_demorgan_fu_9349_p2() {
    sel_tmp32_demorgan_fu_9349_p2 = (tmp_108_reg_24402_pp7_iter4_reg.read() | tmp_133_reg_24447.read());
}

void mnist_fp16_opt::thread_sel_tmp32_fu_9519_p2() {
    sel_tmp32_fu_9519_p2 = (sel_tmp30_fu_9508_p2.read() & sel_tmp31_fu_9513_p2.read());
}

void mnist_fp16_opt::thread_sel_tmp336_demorgan_fu_22036_p2() {
    sel_tmp336_demorgan_fu_22036_p2 = (sel_tmp321_demorgan_fu_22018_p2.read() | tmp_453_fu_21966_p2.read());
}

void mnist_fp16_opt::thread_sel_tmp33_fu_9525_p2() {
    sel_tmp33_fu_9525_p2 = (sel_tmp30_fu_9508_p2.read() & tmp_203_fu_9450_p2.read());
}

void mnist_fp16_opt::thread_sel_tmp34_fu_9536_p2() {
    sel_tmp34_fu_9536_p2 = (sel_tmp71_demorgan_fu_9531_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp16_opt::thread_sel_tmp35_fu_9542_p2() {
    sel_tmp35_fu_9542_p2 = (icmp5_reg_24493.read() & sel_tmp34_fu_9536_p2.read());
}

void mnist_fp16_opt::thread_sel_tmp36_fu_13730_p2() {
    sel_tmp36_fu_13730_p2 = (tmp_212_reg_25627_pp12_iter5_reg.read() ^ ap_const_lv1_1);
}

void mnist_fp16_opt::thread_sel_tmp37_fu_13735_p2() {
    sel_tmp37_fu_13735_p2 = (tmp_445_reg_25644.read() & sel_tmp36_fu_13730_p2.read());
}

void mnist_fp16_opt::thread_sel_tmp38_fu_13659_p2() {
    sel_tmp38_fu_13659_p2 = (sel_tmp110_demorgan_fu_13654_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp16_opt::thread_sel_tmp39_fu_13665_p2() {
    sel_tmp39_fu_13665_p2 = (tmp_423_fu_13602_p2.read() & sel_tmp38_fu_13659_p2.read());
}

void mnist_fp16_opt::thread_sel_tmp3_fu_7603_p2() {
    sel_tmp3_fu_7603_p2 = (sel_tmp21_demorgan_fu_7597_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp16_opt::thread_sel_tmp40_fu_13740_p2() {
    sel_tmp40_fu_13740_p2 = (tmp_450_fu_13692_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp16_opt::thread_sel_tmp41_fu_13746_p2() {
    sel_tmp41_fu_13746_p2 = (sel_tmp39_reg_25655.read() & sel_tmp40_fu_13740_p2.read());
}

void mnist_fp16_opt::thread_sel_tmp42_fu_13751_p2() {
    sel_tmp42_fu_13751_p2 = (sel_tmp39_reg_25655.read() & tmp_450_fu_13692_p2.read());
}

void mnist_fp16_opt::thread_sel_tmp43_fu_13677_p2() {
    sel_tmp43_fu_13677_p2 = (sel_tmp125_demorgan_fu_13671_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp16_opt::thread_sel_tmp44_fu_13683_p2() {
    sel_tmp44_fu_13683_p2 = (icmp7_fu_13648_p2.read() & sel_tmp43_fu_13677_p2.read());
}

void mnist_fp16_opt::thread_sel_tmp45_fu_16263_p2() {
    sel_tmp45_fu_16263_p2 = (tmp_555_reg_26271_pp15_iter5_reg.read() ^ ap_const_lv1_1);
}

void mnist_fp16_opt::thread_sel_tmp46_fu_16268_p2() {
    sel_tmp46_fu_16268_p2 = (tmp_304_reg_26288.read() & sel_tmp45_fu_16263_p2.read());
}

void mnist_fp16_opt::thread_sel_tmp47_demorgan_fu_9382_p2() {
    sel_tmp47_demorgan_fu_9382_p2 = (sel_tmp32_demorgan_fu_9349_p2.read() | tmp_130_reg_24435.read());
}

void mnist_fp16_opt::thread_sel_tmp47_fu_16192_p2() {
    sel_tmp47_fu_16192_p2 = (sel_tmp188_demorgan_fu_16187_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp16_opt::thread_sel_tmp48_fu_16198_p2() {
    sel_tmp48_fu_16198_p2 = (tmp_301_fu_16135_p2.read() & sel_tmp47_fu_16192_p2.read());
}

void mnist_fp16_opt::thread_sel_tmp49_fu_16273_p2() {
    sel_tmp49_fu_16273_p2 = (tmp_562_fu_16225_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp16_opt::thread_sel_tmp4_fu_7609_p2() {
    sel_tmp4_fu_7609_p2 = (icmp_fu_7574_p2.read() & sel_tmp3_fu_7603_p2.read());
}

void mnist_fp16_opt::thread_sel_tmp50_fu_16279_p2() {
    sel_tmp50_fu_16279_p2 = (sel_tmp48_reg_26299.read() & sel_tmp49_fu_16273_p2.read());
}

void mnist_fp16_opt::thread_sel_tmp51_fu_16284_p2() {
    sel_tmp51_fu_16284_p2 = (sel_tmp48_reg_26299.read() & tmp_562_fu_16225_p2.read());
}

void mnist_fp16_opt::thread_sel_tmp52_fu_16210_p2() {
    sel_tmp52_fu_16210_p2 = (sel_tmp203_demorgan_fu_16204_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp16_opt::thread_sel_tmp53_fu_16216_p2() {
    sel_tmp53_fu_16216_p2 = (icmp9_fu_16181_p2.read() & sel_tmp52_fu_16210_p2.read());
}

void mnist_fp16_opt::thread_sel_tmp54_fu_15344_p2() {
    sel_tmp54_fu_15344_p2 = (tmp_286_reg_26002_pp14_iter4_reg.read() ^ ap_const_lv1_1);
}

void mnist_fp16_opt::thread_sel_tmp55_fu_15349_p2() {
    sel_tmp55_fu_15349_p2 = (tmp_311_reg_26047.read() & sel_tmp54_fu_15344_p2.read());
}

void mnist_fp16_opt::thread_sel_tmp56_demorgan_fu_9498_p2() {
    sel_tmp56_demorgan_fu_9498_p2 = (tmp_173_reg_24424_pp7_iter4_reg.read() | tmp_179_reg_24481.read());
}

void mnist_fp16_opt::thread_sel_tmp56_fu_15358_p2() {
    sel_tmp56_fu_15358_p2 = (sel_tmp137_demorgan_fu_15354_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp16_opt::thread_sel_tmp57_fu_15364_p2() {
    sel_tmp57_fu_15364_p2 = (tmp_308_reg_26035.read() & sel_tmp56_fu_15358_p2.read());
}

void mnist_fp16_opt::thread_sel_tmp58_fu_15369_p2() {
    sel_tmp58_fu_15369_p2 = (tmp_322_fu_15306_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp16_opt::thread_sel_tmp59_fu_15375_p2() {
    sel_tmp59_fu_15375_p2 = (sel_tmp57_fu_15364_p2.read() & sel_tmp58_fu_15369_p2.read());
}

void mnist_fp16_opt::thread_sel_tmp5_fu_10293_p2() {
    sel_tmp5_fu_10293_p2 = (tmp_101_reg_24671_pp8_iter5_reg.read() ^ ap_const_lv1_1);
}

void mnist_fp16_opt::thread_sel_tmp60_fu_15381_p2() {
    sel_tmp60_fu_15381_p2 = (sel_tmp57_fu_15364_p2.read() & tmp_322_fu_15306_p2.read());
}

void mnist_fp16_opt::thread_sel_tmp61_fu_15392_p2() {
    sel_tmp61_fu_15392_p2 = (sel_tmp152_demorgan_fu_15387_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp16_opt::thread_sel_tmp62_fu_15398_p2() {
    sel_tmp62_fu_15398_p2 = (icmp11_reg_26059.read() & sel_tmp61_fu_15392_p2.read());
}

void mnist_fp16_opt::thread_sel_tmp63_fu_15493_p2() {
    sel_tmp63_fu_15493_p2 = (tmp_351_reg_26024_pp14_iter4_reg.read() ^ ap_const_lv1_1);
}

void mnist_fp16_opt::thread_sel_tmp64_fu_15498_p2() {
    sel_tmp64_fu_15498_p2 = (tmp_357_reg_26075.read() & sel_tmp63_fu_15493_p2.read());
}

void mnist_fp16_opt::thread_sel_tmp65_fu_15273_p2() {
    sel_tmp65_fu_15273_p2 = (sel_tmp161_demorgan_fu_15268_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp16_opt::thread_sel_tmp66_fu_15279_p2() {
    sel_tmp66_fu_15279_p2 = (tmp_354_fu_15216_p2.read() & sel_tmp65_fu_15273_p2.read());
}

void mnist_fp16_opt::thread_sel_tmp67_fu_15503_p2() {
    sel_tmp67_fu_15503_p2 = (tmp_381_fu_15455_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp16_opt::thread_sel_tmp68_fu_15509_p2() {
    sel_tmp68_fu_15509_p2 = (sel_tmp66_reg_26086.read() & sel_tmp67_fu_15503_p2.read());
}

void mnist_fp16_opt::thread_sel_tmp69_fu_15514_p2() {
    sel_tmp69_fu_15514_p2 = (sel_tmp66_reg_26086.read() & tmp_381_fu_15455_p2.read());
}

void mnist_fp16_opt::thread_sel_tmp6_demorgan_fu_7580_p2() {
    sel_tmp6_demorgan_fu_7580_p2 = (tmp_94_reg_24003.read() | tmp_122_fu_7554_p2.read());
}

void mnist_fp16_opt::thread_sel_tmp6_fu_7585_p2() {
    sel_tmp6_fu_7585_p2 = (sel_tmp6_demorgan_fu_7580_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp16_opt::thread_sel_tmp70_fu_15291_p2() {
    sel_tmp70_fu_15291_p2 = (sel_tmp176_demorgan_fu_15285_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp16_opt::thread_sel_tmp71_demorgan_fu_9531_p2() {
    sel_tmp71_demorgan_fu_9531_p2 = (sel_tmp56_demorgan_fu_9498_p2.read() | tmp_176_reg_24469.read());
}

void mnist_fp16_opt::thread_sel_tmp71_fu_15297_p2() {
    sel_tmp71_fu_15297_p2 = (icmp12_fu_15262_p2.read() & sel_tmp70_fu_15291_p2.read());
}

void mnist_fp16_opt::thread_sel_tmp72_fu_18876_p2() {
    sel_tmp72_fu_18876_p2 = (tmp_390_reg_27036_pp19_iter5_reg.read() ^ ap_const_lv1_1);
}

void mnist_fp16_opt::thread_sel_tmp73_fu_18881_p2() {
    sel_tmp73_fu_18881_p2 = (tmp_613_reg_27053.read() & sel_tmp72_fu_18876_p2.read());
}

void mnist_fp16_opt::thread_sel_tmp74_fu_18805_p2() {
    sel_tmp74_fu_18805_p2 = (sel_tmp215_demorgan_fu_18800_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp16_opt::thread_sel_tmp75_fu_18811_p2() {
    sel_tmp75_fu_18811_p2 = (tmp_610_fu_18748_p2.read() & sel_tmp74_fu_18805_p2.read());
}

void mnist_fp16_opt::thread_sel_tmp76_fu_18886_p2() {
    sel_tmp76_fu_18886_p2 = (tmp_615_fu_18838_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp16_opt::thread_sel_tmp77_fu_18892_p2() {
    sel_tmp77_fu_18892_p2 = (sel_tmp75_reg_27064.read() & sel_tmp76_fu_18886_p2.read());
}

void mnist_fp16_opt::thread_sel_tmp78_fu_18897_p2() {
    sel_tmp78_fu_18897_p2 = (sel_tmp75_reg_27064.read() & tmp_615_fu_18838_p2.read());
}

void mnist_fp16_opt::thread_sel_tmp79_fu_18823_p2() {
    sel_tmp79_fu_18823_p2 = (sel_tmp230_demorgan_fu_18817_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp16_opt::thread_sel_tmp7_fu_7591_p2() {
    sel_tmp7_fu_7591_p2 = (tmp_107_fu_7528_p2.read() & sel_tmp6_fu_7585_p2.read());
}

void mnist_fp16_opt::thread_sel_tmp80_fu_18829_p2() {
    sel_tmp80_fu_18829_p2 = (icmp14_fu_18794_p2.read() & sel_tmp79_fu_18823_p2.read());
}

void mnist_fp16_opt::thread_sel_tmp81_fu_22095_p2() {
    sel_tmp81_fu_22095_p2 = (tmp_437_reg_27869.read() ^ ap_const_lv1_1);
}

void mnist_fp16_opt::thread_sel_tmp82_fu_22100_p2() {
    sel_tmp82_fu_22100_p2 = (tmp_456_reg_27880.read() & sel_tmp81_fu_22095_p2.read());
}

void mnist_fp16_opt::thread_sel_tmp83_demorgan_fu_10217_p2() {
    sel_tmp83_demorgan_fu_10217_p2 = (tmp_101_reg_24671.read() | tmp_126_fu_10191_p2.read());
}

void mnist_fp16_opt::thread_sel_tmp83_fu_22024_p2() {
    sel_tmp83_fu_22024_p2 = (sel_tmp321_demorgan_fu_22018_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp16_opt::thread_sel_tmp84_fu_22030_p2() {
    sel_tmp84_fu_22030_p2 = (tmp_453_fu_21966_p2.read() & sel_tmp83_fu_22024_p2.read());
}

void mnist_fp16_opt::thread_sel_tmp85_fu_22105_p2() {
    sel_tmp85_fu_22105_p2 = (tmp_479_fu_22057_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp16_opt::thread_sel_tmp86_fu_22111_p2() {
    sel_tmp86_fu_22111_p2 = (sel_tmp84_reg_27891.read() & sel_tmp85_fu_22105_p2.read());
}

void mnist_fp16_opt::thread_sel_tmp87_fu_22116_p2() {
    sel_tmp87_fu_22116_p2 = (sel_tmp84_reg_27891.read() & tmp_479_fu_22057_p2.read());
}

void mnist_fp16_opt::thread_sel_tmp88_fu_22042_p2() {
    sel_tmp88_fu_22042_p2 = (sel_tmp336_demorgan_fu_22036_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp16_opt::thread_sel_tmp89_fu_22048_p2() {
    sel_tmp89_fu_22048_p2 = (icmp20_fu_22012_p2.read() & sel_tmp88_fu_22042_p2.read());
}

void mnist_fp16_opt::thread_sel_tmp8_fu_7666_p2() {
    sel_tmp8_fu_7666_p2 = (tmp_135_fu_7618_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp16_opt::thread_sel_tmp90_fu_21379_p2() {
    sel_tmp90_fu_21379_p2 = (tmp_471_reg_27666_pp22_iter5_reg.read() ^ ap_const_lv1_1);
}

void mnist_fp16_opt::thread_sel_tmp91_fu_21384_p2() {
    sel_tmp91_fu_21384_p2 = (tmp_487_reg_27683.read() & sel_tmp90_fu_21379_p2.read());
}

void mnist_fp16_opt::thread_sel_tmp92_fu_21308_p2() {
    sel_tmp92_fu_21308_p2 = (sel_tmp293_demorgan_fu_21303_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp16_opt::thread_sel_tmp93_fu_21314_p2() {
    sel_tmp93_fu_21314_p2 = (tmp_484_fu_21251_p2.read() & sel_tmp92_fu_21308_p2.read());
}

void mnist_fp16_opt::thread_sel_tmp94_fu_21389_p2() {
    sel_tmp94_fu_21389_p2 = (tmp_634_fu_21341_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp16_opt::thread_sel_tmp95_fu_21395_p2() {
    sel_tmp95_fu_21395_p2 = (sel_tmp93_reg_27694.read() & sel_tmp94_fu_21389_p2.read());
}

void mnist_fp16_opt::thread_sel_tmp96_fu_21400_p2() {
    sel_tmp96_fu_21400_p2 = (sel_tmp93_reg_27694.read() & tmp_634_fu_21341_p2.read());
}

void mnist_fp16_opt::thread_sel_tmp97_fu_21326_p2() {
    sel_tmp97_fu_21326_p2 = (sel_tmp308_demorgan_fu_21320_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp16_opt::thread_sel_tmp98_demorgan_fu_10234_p2() {
    sel_tmp98_demorgan_fu_10234_p2 = (sel_tmp83_demorgan_fu_10217_p2.read() | tmp_123_fu_10165_p2.read());
}

void mnist_fp16_opt::thread_sel_tmp98_fu_21332_p2() {
    sel_tmp98_fu_21332_p2 = (icmp16_fu_21297_p2.read() & sel_tmp97_fu_21326_p2.read());
}

void mnist_fp16_opt::thread_sel_tmp99_fu_20476_p2() {
    sel_tmp99_fu_20476_p2 = (tmp_478_reg_27397_pp21_iter5_reg.read() ^ ap_const_lv1_1);
}

void mnist_fp16_opt::thread_sel_tmp9_fu_7672_p2() {
    sel_tmp9_fu_7672_p2 = (sel_tmp7_reg_24031.read() & sel_tmp8_fu_7666_p2.read());
}

void mnist_fp16_opt::thread_sel_tmp_fu_7677_p2() {
    sel_tmp_fu_7677_p2 = (sel_tmp7_reg_24031.read() & tmp_135_fu_7618_p2.read());
}

void mnist_fp16_opt::thread_sext12_cast_fu_11203_p1() {
    sext12_cast_fu_11203_p1 = esl_sext<26,12>(grp_fu_4432_p2.read());
}

void mnist_fp16_opt::thread_sext15_cast_fu_11309_p1() {
    sext15_cast_fu_11309_p1 = esl_sext<26,12>(grp_fu_4420_p2.read());
}

void mnist_fp16_opt::thread_sext18_cast_fu_14197_p1() {
    sext18_cast_fu_14197_p1 = esl_sext<26,12>(r_V_85_tr_fu_14192_p2.read());
}

void mnist_fp16_opt::thread_sext20_cast_fu_11325_p1() {
    sext20_cast_fu_11325_p1 = esl_sext<26,12>(grp_fu_4432_p2.read());
}

void mnist_fp16_opt::thread_sext23_cast_fu_11430_p1() {
    sext23_cast_fu_11430_p1 = esl_sext<26,12>(grp_fu_4420_p2.read());
}

void mnist_fp16_opt::thread_sext26_cast_fu_11446_p1() {
    sext26_cast_fu_11446_p1 = esl_sext<26,12>(grp_fu_4432_p2.read());
}

void mnist_fp16_opt::thread_sext29_cast_fu_11563_p1() {
    sext29_cast_fu_11563_p1 = esl_sext<26,12>(r_V_31_tr14_2_reg_25123.read());
}

void mnist_fp16_opt::thread_sext32_cast_fu_17131_p1() {
    sext32_cast_fu_17131_p1 = esl_sext<22,10>(reg_4507.read());
}

void mnist_fp16_opt::thread_sext34_cast_fu_17147_p1() {
    sext34_cast_fu_17147_p1 = esl_sext<22,10>(reg_4511.read());
}

void mnist_fp16_opt::thread_sext36_cast_fu_17253_p1() {
    sext36_cast_fu_17253_p1 = esl_sext<22,10>(reg_4507.read());
}

void mnist_fp16_opt::thread_sext38_cast_fu_19317_p1() {
    sext38_cast_fu_19317_p1 = esl_sext<24,11>(r_V_100_tr_reg_27129.read());
}

void mnist_fp16_opt::thread_sext40_cast_fu_17269_p1() {
    sext40_cast_fu_17269_p1 = esl_sext<22,10>(reg_4511.read());
}

void mnist_fp16_opt::thread_sext4_cast_fu_8163_p1() {
    sext4_cast_fu_8163_p1 = esl_sext<28,13>(r_V_17_tr_fu_8158_p2.read());
}

void mnist_fp16_opt::thread_sext9_cast_fu_11187_p1() {
    sext9_cast_fu_11187_p1 = esl_sext<26,12>(grp_fu_4420_p2.read());
}

void mnist_fp16_opt::thread_sh_amt_10_cast_fu_20435_p1() {
    sh_amt_10_cast_fu_20435_p1 = esl_sext<32,12>(sh_amt_10_reg_27430.read());
}

void mnist_fp16_opt::thread_sh_amt_10_fu_20281_p3() {
    sh_amt_10_fu_20281_p3 = (!tmp_491_fu_20263_p2.read()[0].is_01())? sc_lv<12>(): ((tmp_491_fu_20263_p2.read()[0].to_bool())? tmp_492_fu_20269_p2.read(): tmp_493_fu_20275_p2.read());
}

void mnist_fp16_opt::thread_sh_amt_11_cast_fu_20549_p1() {
    sh_amt_11_cast_fu_20549_p1 = esl_sext<32,12>(sh_amt_11_reg_27470.read());
}

void mnist_fp16_opt::thread_sh_amt_11_fu_20401_p3() {
    sh_amt_11_fu_20401_p3 = (!tmp_556_fu_20383_p2.read()[0].is_01())? sc_lv<12>(): ((tmp_556_fu_20383_p2.read()[0].to_bool())? tmp_557_fu_20389_p2.read(): tmp_558_fu_20395_p2.read());
}

void mnist_fp16_opt::thread_sh_amt_1_cast_fu_10252_p1() {
    sh_amt_1_cast_fu_10252_p1 = esl_sext<32,12>(sh_amt_1_reg_24682.read());
}

void mnist_fp16_opt::thread_sh_amt_1_fu_10183_p3() {
    sh_amt_1_fu_10183_p3 = (!tmp_123_fu_10165_p2.read()[0].is_01())? sc_lv<12>(): ((tmp_123_fu_10165_p2.read()[0].to_bool())? tmp_124_fu_10171_p2.read(): tmp_125_fu_10177_p2.read());
}

void mnist_fp16_opt::thread_sh_amt_2_cast_fu_9298_p1() {
    sh_amt_2_cast_fu_9298_p1 = esl_sext<32,12>(sh_amt_2_reg_24441.read());
}

void mnist_fp16_opt::thread_sh_amt_2_fu_9179_p3() {
    sh_amt_2_fu_9179_p3 = (!tmp_130_fu_9161_p2.read()[0].is_01())? sc_lv<12>(): ((tmp_130_fu_9161_p2.read()[0].to_bool())? tmp_131_fu_9167_p2.read(): tmp_132_fu_9173_p2.read());
}

void mnist_fp16_opt::thread_sh_amt_3_cast_fu_9447_p1() {
    sh_amt_3_cast_fu_9447_p1 = esl_sext<32,12>(sh_amt_3_reg_24475.read());
}

void mnist_fp16_opt::thread_sh_amt_3_fu_9264_p3() {
    sh_amt_3_fu_9264_p3 = (!tmp_176_fu_9246_p2.read()[0].is_01())? sc_lv<12>(): ((tmp_176_fu_9246_p2.read()[0].to_bool())? tmp_177_fu_9252_p2.read(): tmp_178_fu_9258_p2.read());
}

void mnist_fp16_opt::thread_sh_amt_4_cast_fu_13689_p1() {
    sh_amt_4_cast_fu_13689_p1 = esl_sext<32,12>(sh_amt_4_reg_25638.read());
}

void mnist_fp16_opt::thread_sh_amt_4_fu_13620_p3() {
    sh_amt_4_fu_13620_p3 = (!tmp_423_fu_13602_p2.read()[0].is_01())? sc_lv<12>(): ((tmp_423_fu_13602_p2.read()[0].to_bool())? tmp_436_fu_13608_p2.read(): tmp_440_fu_13614_p2.read());
}

void mnist_fp16_opt::thread_sh_amt_5_cast_fu_16222_p1() {
    sh_amt_5_cast_fu_16222_p1 = esl_sext<32,12>(sh_amt_5_reg_26282.read());
}

void mnist_fp16_opt::thread_sh_amt_5_fu_16153_p3() {
    sh_amt_5_fu_16153_p3 = (!tmp_301_fu_16135_p2.read()[0].is_01())? sc_lv<12>(): ((tmp_301_fu_16135_p2.read()[0].to_bool())? tmp_302_fu_16141_p2.read(): tmp_303_fu_16147_p2.read());
}

void mnist_fp16_opt::thread_sh_amt_6_cast_fu_15303_p1() {
    sh_amt_6_cast_fu_15303_p1 = esl_sext<32,12>(sh_amt_6_reg_26041.read());
}

void mnist_fp16_opt::thread_sh_amt_6_fu_15149_p3() {
    sh_amt_6_fu_15149_p3 = (!tmp_308_fu_15131_p2.read()[0].is_01())? sc_lv<12>(): ((tmp_308_fu_15131_p2.read()[0].to_bool())? tmp_309_fu_15137_p2.read(): tmp_310_fu_15143_p2.read());
}

void mnist_fp16_opt::thread_sh_amt_7_cast_fu_15452_p1() {
    sh_amt_7_cast_fu_15452_p1 = esl_sext<32,12>(sh_amt_7_reg_26069.read());
}

void mnist_fp16_opt::thread_sh_amt_7_fu_15234_p3() {
    sh_amt_7_fu_15234_p3 = (!tmp_354_fu_15216_p2.read()[0].is_01())? sc_lv<12>(): ((tmp_354_fu_15216_p2.read()[0].to_bool())? tmp_355_fu_15222_p2.read(): tmp_356_fu_15228_p2.read());
}

void mnist_fp16_opt::thread_sh_amt_8_cast_fu_18835_p1() {
    sh_amt_8_cast_fu_18835_p1 = esl_sext<32,12>(sh_amt_8_reg_27047.read());
}

void mnist_fp16_opt::thread_sh_amt_8_fu_18766_p3() {
    sh_amt_8_fu_18766_p3 = (!tmp_610_fu_18748_p2.read()[0].is_01())? sc_lv<12>(): ((tmp_610_fu_18748_p2.read()[0].to_bool())? tmp_611_fu_18754_p2.read(): tmp_612_fu_18760_p2.read());
}

void mnist_fp16_opt::thread_sh_amt_9_cast_fu_22054_p1() {
    sh_amt_9_cast_fu_22054_p1 = esl_sext<32,12>(sh_amt_9_reg_27874.read());
}

void mnist_fp16_opt::thread_sh_amt_9_fu_21984_p3() {
    sh_amt_9_fu_21984_p3 = (!tmp_453_fu_21966_p2.read()[0].is_01())? sc_lv<12>(): ((tmp_453_fu_21966_p2.read()[0].to_bool())? tmp_454_fu_21972_p2.read(): tmp_455_fu_21978_p2.read());
}

void mnist_fp16_opt::thread_sh_amt_cast_72_fu_21338_p1() {
    sh_amt_cast_72_fu_21338_p1 = esl_sext<32,12>(sh_amt_s_reg_27677.read());
}

void mnist_fp16_opt::thread_sh_amt_cast_fu_7615_p1() {
    sh_amt_cast_fu_7615_p1 = esl_sext<32,12>(sh_amt_reg_24014.read());
}

void mnist_fp16_opt::thread_sh_amt_fu_7546_p3() {
    sh_amt_fu_7546_p3 = (!tmp_107_fu_7528_p2.read()[0].is_01())? sc_lv<12>(): ((tmp_107_fu_7528_p2.read()[0].to_bool())? tmp_114_fu_7534_p2.read(): tmp_115_fu_7540_p2.read());
}

void mnist_fp16_opt::thread_sh_amt_s_fu_21269_p3() {
    sh_amt_s_fu_21269_p3 = (!tmp_484_fu_21251_p2.read()[0].is_01())? sc_lv<12>(): ((tmp_484_fu_21251_p2.read()[0].to_bool())? tmp_485_fu_21257_p2.read(): tmp_486_fu_21263_p2.read());
}

void mnist_fp16_opt::thread_storemerge10_fu_21359_p3() {
    storemerge10_fu_21359_p3 = (!tmp_1448_reg_27650_pp22_iter5_reg.read()[0].is_01())? sc_lv<16>(): ((tmp_1448_reg_27650_pp22_iter5_reg.read()[0].to_bool())? ap_const_lv16_FFFF: ap_const_lv16_0);
}

void mnist_fp16_opt::thread_storemerge11_fu_20456_p3() {
    storemerge11_fu_20456_p3 = (!isneg_4_reg_27381_pp21_iter5_reg.read()[0].is_01())? sc_lv<16>(): ((isneg_4_reg_27381_pp21_iter5_reg.read()[0].to_bool())? ap_const_lv16_FFFF: ap_const_lv16_0);
}

void mnist_fp16_opt::thread_storemerge12_fu_20570_p3() {
    storemerge12_fu_20570_p3 = (!isneg_5_reg_27403_pp21_iter5_reg.read()[0].is_01())? sc_lv<16>(): ((isneg_5_reg_27403_pp21_iter5_reg.read()[0].to_bool())? ap_const_lv16_FFFF: ap_const_lv16_0);
}

void mnist_fp16_opt::thread_storemerge1_fu_15324_p3() {
    storemerge1_fu_15324_p3 = (!isneg_2_reg_25986_pp14_iter4_reg.read()[0].is_01())? sc_lv<16>(): ((isneg_2_reg_25986_pp14_iter4_reg.read()[0].to_bool())? ap_const_lv16_FFFF: ap_const_lv16_0);
}

void mnist_fp16_opt::thread_storemerge2_fu_10273_p3() {
    storemerge2_fu_10273_p3 = (!tmp_594_reg_24655_pp8_iter5_reg.read()[0].is_01())? sc_lv<16>(): ((tmp_594_reg_24655_pp8_iter5_reg.read()[0].to_bool())? ap_const_lv16_FFFF: ap_const_lv16_0);
}

void mnist_fp16_opt::thread_storemerge3_fu_15473_p3() {
    storemerge3_fu_15473_p3 = (!isneg_3_reg_26008_pp14_iter4_reg.read()[0].is_01())? sc_lv<16>(): ((isneg_3_reg_26008_pp14_iter4_reg.read()[0].to_bool())? ap_const_lv16_FFFF: ap_const_lv16_0);
}

void mnist_fp16_opt::thread_storemerge4_fu_9319_p3() {
    storemerge4_fu_9319_p3 = (!isneg_reg_24386_pp7_iter4_reg.read()[0].is_01())? sc_lv<16>(): ((isneg_reg_24386_pp7_iter4_reg.read()[0].to_bool())? ap_const_lv16_FFFF: ap_const_lv16_0);
}

void mnist_fp16_opt::thread_storemerge5_fu_16243_p3() {
    storemerge5_fu_16243_p3 = (!tmp_1274_reg_26255_pp15_iter5_reg.read()[0].is_01())? sc_lv<16>(): ((tmp_1274_reg_26255_pp15_iter5_reg.read()[0].to_bool())? ap_const_lv16_FFFF: ap_const_lv16_0);
}

void mnist_fp16_opt::thread_storemerge6_fu_9468_p3() {
    storemerge6_fu_9468_p3 = (!isneg_1_reg_24408_pp7_iter4_reg.read()[0].is_01())? sc_lv<16>(): ((isneg_1_reg_24408_pp7_iter4_reg.read()[0].to_bool())? ap_const_lv16_FFFF: ap_const_lv16_0);
}

void mnist_fp16_opt::thread_storemerge7_fu_18856_p3() {
    storemerge7_fu_18856_p3 = (!tmp_1390_reg_27020_pp19_iter5_reg.read()[0].is_01())? sc_lv<16>(): ((tmp_1390_reg_27020_pp19_iter5_reg.read()[0].to_bool())? ap_const_lv16_FFFF: ap_const_lv16_0);
}

void mnist_fp16_opt::thread_storemerge8_fu_13710_p3() {
    storemerge8_fu_13710_p3 = (!tmp_885_reg_25611_pp12_iter5_reg.read()[0].is_01())? sc_lv<16>(): ((tmp_885_reg_25611_pp12_iter5_reg.read()[0].to_bool())? ap_const_lv16_FFFF: ap_const_lv16_0);
}

void mnist_fp16_opt::thread_storemerge9_fu_22075_p3() {
    storemerge9_fu_22075_p3 = (!tmp_1483_reg_27848_pp24_iter2_reg.read()[0].is_01())? sc_lv<16>(): ((tmp_1483_reg_27848_pp24_iter2_reg.read()[0].to_bool())? ap_const_lv16_FFFF: ap_const_lv16_0);
}

void mnist_fp16_opt::thread_storemerge_fu_7636_p3() {
    storemerge_fu_7636_p3 = (!tmp_211_reg_23987_pp5_iter5_reg.read()[0].is_01())? sc_lv<16>(): ((tmp_211_reg_23987_pp5_iter5_reg.read()[0].to_bool())? ap_const_lv16_FFFF: ap_const_lv16_0);
}

void mnist_fp16_opt::thread_stream_in_TDATA_blk_n() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state2.read()) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage0.read()) && 
          esl_seteq<1,1,1>(ap_enable_reg_pp1_iter1.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_block_pp1_stage0.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, tmp_2_reg_23102.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, brmerge_reg_23111.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp2_stage0.read()) && 
          esl_seteq<1,1,1>(ap_enable_reg_pp2_iter1.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_block_pp2_stage0.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, eol_2_reg_2238.read())))) {
        stream_in_TDATA_blk_n = stream_in_V_data_V_0_state.read()[0];
    } else {
        stream_in_TDATA_blk_n = ap_const_logic_1;
    }
}

void mnist_fp16_opt::thread_stream_in_TREADY() {
    stream_in_TREADY = stream_in_V_dest_V_0_state.read()[1];
}

void mnist_fp16_opt::thread_stream_in_V_data_V_0_ack_in() {
    stream_in_V_data_V_0_ack_in = stream_in_V_data_V_0_state.read()[1];
}

void mnist_fp16_opt::thread_stream_in_V_data_V_0_ack_out() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state2.read()) && 
          esl_seteq<1,1,1>(stream_in_V_data_V_0_vld_out.read(), ap_const_logic_1)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp2_stage0.read()) && 
          esl_seteq<1,1,1>(ap_enable_reg_pp2_iter1.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, eol_2_reg_2238.read()) && 
          esl_seteq<1,1,1>(ap_block_pp2_stage0_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage0.read()) && 
          esl_seteq<1,1,1>(ap_enable_reg_pp1_iter1.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op598_read_state6.read()) && 
          esl_seteq<1,1,1>(ap_block_pp1_stage0_11001.read(), ap_const_boolean_0)))) {
        stream_in_V_data_V_0_ack_out = ap_const_logic_1;
    } else {
        stream_in_V_data_V_0_ack_out = ap_const_logic_0;
    }
}

void mnist_fp16_opt::thread_stream_in_V_data_V_0_data_out() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, stream_in_V_data_V_0_sel.read())) {
        stream_in_V_data_V_0_data_out = stream_in_V_data_V_0_payload_B.read();
    } else {
        stream_in_V_data_V_0_data_out = stream_in_V_data_V_0_payload_A.read();
    }
}

void mnist_fp16_opt::thread_stream_in_V_data_V_0_load_A() {
    stream_in_V_data_V_0_load_A = (stream_in_V_data_V_0_state_cmp_full.read() & ~stream_in_V_data_V_0_sel_wr.read());
}

void mnist_fp16_opt::thread_stream_in_V_data_V_0_load_B() {
    stream_in_V_data_V_0_load_B = (stream_in_V_data_V_0_sel_wr.read() & stream_in_V_data_V_0_state_cmp_full.read());
}

void mnist_fp16_opt::thread_stream_in_V_data_V_0_sel() {
    stream_in_V_data_V_0_sel = stream_in_V_data_V_0_sel_rd.read();
}

void mnist_fp16_opt::thread_stream_in_V_data_V_0_state_cmp_full() {
    stream_in_V_data_V_0_state_cmp_full =  (sc_logic) ((!stream_in_V_data_V_0_state.read().is_01() || !ap_const_lv2_1.is_01())? sc_lv<1>(): sc_lv<1>(stream_in_V_data_V_0_state.read() != ap_const_lv2_1))[0];
}

void mnist_fp16_opt::thread_stream_in_V_data_V_0_vld_in() {
    stream_in_V_data_V_0_vld_in = stream_in_TVALID.read();
}

void mnist_fp16_opt::thread_stream_in_V_data_V_0_vld_out() {
    stream_in_V_data_V_0_vld_out = stream_in_V_data_V_0_state.read()[0];
}

void mnist_fp16_opt::thread_stream_in_V_dest_V_0_ack_out() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state2.read()) && 
          esl_seteq<1,1,1>(stream_in_V_data_V_0_vld_out.read(), ap_const_logic_1)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp2_stage0.read()) && 
          esl_seteq<1,1,1>(ap_enable_reg_pp2_iter1.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, eol_2_reg_2238.read()) && 
          esl_seteq<1,1,1>(ap_block_pp2_stage0_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage0.read()) && 
          esl_seteq<1,1,1>(ap_enable_reg_pp1_iter1.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op598_read_state6.read()) && 
          esl_seteq<1,1,1>(ap_block_pp1_stage0_11001.read(), ap_const_boolean_0)))) {
        stream_in_V_dest_V_0_ack_out = ap_const_logic_1;
    } else {
        stream_in_V_dest_V_0_ack_out = ap_const_logic_0;
    }
}

void mnist_fp16_opt::thread_stream_in_V_dest_V_0_vld_in() {
    stream_in_V_dest_V_0_vld_in = stream_in_TVALID.read();
}

void mnist_fp16_opt::thread_stream_in_V_last_V_0_ack_in() {
    stream_in_V_last_V_0_ack_in = stream_in_V_last_V_0_state.read()[1];
}

void mnist_fp16_opt::thread_stream_in_V_last_V_0_ack_out() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state2.read()) && 
          esl_seteq<1,1,1>(stream_in_V_data_V_0_vld_out.read(), ap_const_logic_1)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp2_stage0.read()) && 
          esl_seteq<1,1,1>(ap_enable_reg_pp2_iter1.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, eol_2_reg_2238.read()) && 
          esl_seteq<1,1,1>(ap_block_pp2_stage0_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage0.read()) && 
          esl_seteq<1,1,1>(ap_enable_reg_pp1_iter1.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op598_read_state6.read()) && 
          esl_seteq<1,1,1>(ap_block_pp1_stage0_11001.read(), ap_const_boolean_0)))) {
        stream_in_V_last_V_0_ack_out = ap_const_logic_1;
    } else {
        stream_in_V_last_V_0_ack_out = ap_const_logic_0;
    }
}

void mnist_fp16_opt::thread_stream_in_V_last_V_0_data_out() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, stream_in_V_last_V_0_sel.read())) {
        stream_in_V_last_V_0_data_out = stream_in_V_last_V_0_payload_B.read();
    } else {
        stream_in_V_last_V_0_data_out = stream_in_V_last_V_0_payload_A.read();
    }
}

void mnist_fp16_opt::thread_stream_in_V_last_V_0_load_A() {
    stream_in_V_last_V_0_load_A = (stream_in_V_last_V_0_state_cmp_full.read() & ~stream_in_V_last_V_0_sel_wr.read());
}

void mnist_fp16_opt::thread_stream_in_V_last_V_0_load_B() {
    stream_in_V_last_V_0_load_B = (stream_in_V_last_V_0_sel_wr.read() & stream_in_V_last_V_0_state_cmp_full.read());
}

void mnist_fp16_opt::thread_stream_in_V_last_V_0_sel() {
    stream_in_V_last_V_0_sel = stream_in_V_last_V_0_sel_rd.read();
}

void mnist_fp16_opt::thread_stream_in_V_last_V_0_state_cmp_full() {
    stream_in_V_last_V_0_state_cmp_full =  (sc_logic) ((!stream_in_V_last_V_0_state.read().is_01() || !ap_const_lv2_1.is_01())? sc_lv<1>(): sc_lv<1>(stream_in_V_last_V_0_state.read() != ap_const_lv2_1))[0];
}

void mnist_fp16_opt::thread_stream_in_V_last_V_0_vld_in() {
    stream_in_V_last_V_0_vld_in = stream_in_TVALID.read();
}

void mnist_fp16_opt::thread_stream_in_V_last_V_0_vld_out() {
    stream_in_V_last_V_0_vld_out = stream_in_V_last_V_0_state.read()[0];
}

void mnist_fp16_opt::thread_stream_in_V_user_V_0_ack_in() {
    stream_in_V_user_V_0_ack_in = stream_in_V_user_V_0_state.read()[1];
}

void mnist_fp16_opt::thread_stream_in_V_user_V_0_ack_out() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state2.read()) && 
          esl_seteq<1,1,1>(stream_in_V_data_V_0_vld_out.read(), ap_const_logic_1)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp2_stage0.read()) && 
          esl_seteq<1,1,1>(ap_enable_reg_pp2_iter1.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, eol_2_reg_2238.read()) && 
          esl_seteq<1,1,1>(ap_block_pp2_stage0_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage0.read()) && 
          esl_seteq<1,1,1>(ap_enable_reg_pp1_iter1.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op598_read_state6.read()) && 
          esl_seteq<1,1,1>(ap_block_pp1_stage0_11001.read(), ap_const_boolean_0)))) {
        stream_in_V_user_V_0_ack_out = ap_const_logic_1;
    } else {
        stream_in_V_user_V_0_ack_out = ap_const_logic_0;
    }
}

void mnist_fp16_opt::thread_stream_in_V_user_V_0_data_out() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, stream_in_V_user_V_0_sel.read())) {
        stream_in_V_user_V_0_data_out = stream_in_V_user_V_0_payload_B.read();
    } else {
        stream_in_V_user_V_0_data_out = stream_in_V_user_V_0_payload_A.read();
    }
}

void mnist_fp16_opt::thread_stream_in_V_user_V_0_load_A() {
    stream_in_V_user_V_0_load_A = (stream_in_V_user_V_0_state_cmp_full.read() & ~stream_in_V_user_V_0_sel_wr.read());
}

void mnist_fp16_opt::thread_stream_in_V_user_V_0_load_B() {
    stream_in_V_user_V_0_load_B = (stream_in_V_user_V_0_sel_wr.read() & stream_in_V_user_V_0_state_cmp_full.read());
}

void mnist_fp16_opt::thread_stream_in_V_user_V_0_sel() {
    stream_in_V_user_V_0_sel = stream_in_V_user_V_0_sel_rd.read();
}

void mnist_fp16_opt::thread_stream_in_V_user_V_0_state_cmp_full() {
    stream_in_V_user_V_0_state_cmp_full =  (sc_logic) ((!stream_in_V_user_V_0_state.read().is_01() || !ap_const_lv2_1.is_01())? sc_lv<1>(): sc_lv<1>(stream_in_V_user_V_0_state.read() != ap_const_lv2_1))[0];
}

void mnist_fp16_opt::thread_stream_in_V_user_V_0_vld_in() {
    stream_in_V_user_V_0_vld_in = stream_in_TVALID.read();
}

void mnist_fp16_opt::thread_stream_in_V_user_V_0_vld_out() {
    stream_in_V_user_V_0_vld_out = stream_in_V_user_V_0_state.read()[0];
}

void mnist_fp16_opt::thread_tmp32_V_10_fu_14519_p2() {
    tmp32_V_10_fu_14519_p2 = (!tmp_247_fu_14513_p2.read().is_01())? sc_lv<32>(): tmp32_V_9_fu_14510_p1.read() << (unsigned short)tmp_247_fu_14513_p2.read().to_uint();
}

void mnist_fp16_opt::thread_tmp32_V_11_fu_14544_p1() {
    tmp32_V_11_fu_14544_p1 = esl_zext<32,16>(p_Result_21_fu_14539_p2.read());
}

void mnist_fp16_opt::thread_tmp32_V_12_fu_8514_p3() {
    tmp32_V_12_fu_8514_p3 = (!icmp1_fu_8470_p2.read()[0].is_01())? sc_lv<32>(): ((icmp1_fu_8470_p2.read()[0].to_bool())? tmp32_V_1_fu_8485_p2.read(): tmp32_V_2_fu_8510_p1.read());
}

void mnist_fp16_opt::thread_tmp32_V_13_fu_15712_p1() {
    tmp32_V_13_fu_15712_p1 = esl_zext<32,16>(p_Val2_34_reg_26138.read());
}

void mnist_fp16_opt::thread_tmp32_V_14_fu_15721_p2() {
    tmp32_V_14_fu_15721_p2 = (!tmp_280_fu_15715_p2.read().is_01())? sc_lv<32>(): tmp32_V_13_fu_15712_p1.read() << (unsigned short)tmp_280_fu_15715_p2.read().to_uint();
}

void mnist_fp16_opt::thread_tmp32_V_15_fu_15746_p1() {
    tmp32_V_15_fu_15746_p1 = esl_zext<32,16>(p_Result_29_fu_15741_p2.read());
}

void mnist_fp16_opt::thread_tmp32_V_17_fu_16770_p1() {
    tmp32_V_17_fu_16770_p1 = esl_zext<32,16>(tmp_V_5_reg_26434.read());
}

void mnist_fp16_opt::thread_tmp32_V_18_fu_16779_p2() {
    tmp32_V_18_fu_16779_p2 = (!tmp_394_fu_16773_p2.read().is_01())? sc_lv<32>(): tmp32_V_17_fu_16770_p1.read() << (unsigned short)tmp_394_fu_16773_p2.read().to_uint();
}

void mnist_fp16_opt::thread_tmp32_V_19_fu_16804_p1() {
    tmp32_V_19_fu_16804_p1 = esl_zext<32,16>(tmp_1360_fu_16799_p2.read());
}

void mnist_fp16_opt::thread_tmp32_V_1_fu_8485_p2() {
    tmp32_V_1_fu_8485_p2 = (!tmp_69_fu_8479_p2.read().is_01())? sc_lv<32>(): tmp32_V_fu_8476_p1.read() << (unsigned short)tmp_69_fu_8479_p2.read().to_uint();
}

void mnist_fp16_opt::thread_tmp32_V_20_fu_16816_p1() {
    tmp32_V_20_fu_16816_p1 = grp_fu_4352_p1.read();
}

void mnist_fp16_opt::thread_tmp32_V_21_fu_19605_p1() {
    tmp32_V_21_fu_19605_p1 = esl_zext<32,16>(p_Val2_48_reg_27213.read());
}

void mnist_fp16_opt::thread_tmp32_V_22_fu_19614_p2() {
    tmp32_V_22_fu_19614_p2 = (!tmp_427_fu_19608_p2.read().is_01())? sc_lv<32>(): tmp32_V_21_fu_19605_p1.read() << (unsigned short)tmp_427_fu_19608_p2.read().to_uint();
}

void mnist_fp16_opt::thread_tmp32_V_23_fu_19639_p1() {
    tmp32_V_23_fu_19639_p1 = esl_zext<32,16>(p_Result_41_fu_19634_p2.read());
}

void mnist_fp16_opt::thread_tmp32_V_25_fu_20844_p1() {
    tmp32_V_25_fu_20844_p1 = esl_zext<32,16>(p_Val2_50_reg_27533.read());
}

void mnist_fp16_opt::thread_tmp32_V_26_fu_20853_p2() {
    tmp32_V_26_fu_20853_p2 = (!tmp_472_fu_20847_p2.read().is_01())? sc_lv<32>(): tmp32_V_25_fu_20844_p1.read() << (unsigned short)tmp_472_fu_20847_p2.read().to_uint();
}

void mnist_fp16_opt::thread_tmp32_V_27_fu_20878_p1() {
    tmp32_V_27_fu_20878_p1 = esl_zext<32,16>(p_Result_48_fu_20873_p2.read());
}

void mnist_fp16_opt::thread_tmp32_V_28_fu_9780_p3() {
    tmp32_V_28_fu_9780_p3 = (!icmp3_fu_9736_p2.read()[0].is_01())? sc_lv<32>(): ((icmp3_fu_9736_p2.read()[0].to_bool())? tmp32_V_6_fu_9751_p2.read(): tmp32_V_7_fu_9776_p1.read());
}

void mnist_fp16_opt::thread_tmp32_V_29_fu_21680_p1() {
    tmp32_V_29_fu_21680_p1 = esl_zext<32,16>(tmp_V_8_reg_27776.read());
}

void mnist_fp16_opt::thread_tmp32_V_2_fu_8510_p1() {
    tmp32_V_2_fu_8510_p1 = esl_zext<32,16>(p_Result_s_fu_8505_p2.read());
}

void mnist_fp16_opt::thread_tmp32_V_30_fu_21689_p2() {
    tmp32_V_30_fu_21689_p2 = (!tmp_530_fu_21683_p2.read().is_01())? sc_lv<32>(): tmp32_V_29_fu_21680_p1.read() << (unsigned short)tmp_530_fu_21683_p2.read().to_uint();
}

void mnist_fp16_opt::thread_tmp32_V_31_fu_21714_p1() {
    tmp32_V_31_fu_21714_p1 = esl_zext<32,16>(tmp_1497_fu_21709_p2.read());
}

void mnist_fp16_opt::thread_tmp32_V_32_fu_21726_p1() {
    tmp32_V_32_fu_21726_p1 = grp_fu_4352_p1.read();
}

void mnist_fp16_opt::thread_tmp32_V_33_fu_22335_p1() {
    tmp32_V_33_fu_22335_p1 = esl_zext<32,16>(tmp_V_9_reg_27962.read());
}

void mnist_fp16_opt::thread_tmp32_V_34_fu_22344_p2() {
    tmp32_V_34_fu_22344_p2 = (!tmp_661_fu_22338_p2.read().is_01())? sc_lv<32>(): tmp32_V_33_fu_22335_p1.read() << (unsigned short)tmp_661_fu_22338_p2.read().to_uint();
}

void mnist_fp16_opt::thread_tmp32_V_35_fu_22369_p1() {
    tmp32_V_35_fu_22369_p1 = esl_zext<32,16>(tmp_1510_fu_22364_p2.read());
}

void mnist_fp16_opt::thread_tmp32_V_36_fu_22385_p1() {
    tmp32_V_36_fu_22385_p1 = grp_fu_4352_p1.read();
}

void mnist_fp16_opt::thread_tmp32_V_38_fu_14548_p3() {
    tmp32_V_38_fu_14548_p3 = (!icmp8_fu_14504_p2.read()[0].is_01())? sc_lv<32>(): ((icmp8_fu_14504_p2.read()[0].to_bool())? tmp32_V_10_fu_14519_p2.read(): tmp32_V_11_fu_14544_p1.read());
}

void mnist_fp16_opt::thread_tmp32_V_3_fu_10824_p2() {
    tmp32_V_3_fu_10824_p2 = (!tmp_216_fu_10818_p2.read().is_01())? sc_lv<32>(): tmp32_V_s_fu_10815_p1.read() << (unsigned short)tmp_216_fu_10818_p2.read().to_uint();
}

void mnist_fp16_opt::thread_tmp32_V_42_fu_15750_p3() {
    tmp32_V_42_fu_15750_p3 = (!icmp10_fu_15706_p2.read()[0].is_01())? sc_lv<32>(): ((icmp10_fu_15706_p2.read()[0].to_bool())? tmp32_V_14_fu_15721_p2.read(): tmp32_V_15_fu_15746_p1.read());
}

void mnist_fp16_opt::thread_tmp32_V_45_fu_19643_p3() {
    tmp32_V_45_fu_19643_p3 = (!icmp15_fu_19599_p2.read()[0].is_01())? sc_lv<32>(): ((icmp15_fu_19599_p2.read()[0].to_bool())? tmp32_V_22_fu_19614_p2.read(): tmp32_V_23_fu_19639_p1.read());
}

void mnist_fp16_opt::thread_tmp32_V_47_fu_20882_p3() {
    tmp32_V_47_fu_20882_p3 = (!icmp17_fu_20838_p2.read()[0].is_01())? sc_lv<32>(): ((icmp17_fu_20838_p2.read()[0].to_bool())? tmp32_V_26_fu_20853_p2.read(): tmp32_V_27_fu_20878_p1.read());
}

void mnist_fp16_opt::thread_tmp32_V_4_fu_10849_p1() {
    tmp32_V_4_fu_10849_p1 = esl_zext<32,16>(tmp_775_fu_10844_p2.read());
}

void mnist_fp16_opt::thread_tmp32_V_51_fu_8522_p1() {
    tmp32_V_51_fu_8522_p1 = grp_fu_4352_p1.read();
}

void mnist_fp16_opt::thread_tmp32_V_52_fu_9788_p1() {
    tmp32_V_52_fu_9788_p1 = grp_fu_4352_p1.read();
}

void mnist_fp16_opt::thread_tmp32_V_53_fu_14556_p1() {
    tmp32_V_53_fu_14556_p1 = grp_fu_4352_p1.read();
}

void mnist_fp16_opt::thread_tmp32_V_54_fu_15758_p1() {
    tmp32_V_54_fu_15758_p1 = grp_fu_4352_p1.read();
}

void mnist_fp16_opt::thread_tmp32_V_55_fu_19651_p1() {
    tmp32_V_55_fu_19651_p1 = grp_fu_4352_p1.read();
}

void mnist_fp16_opt::thread_tmp32_V_56_fu_20890_p1() {
    tmp32_V_56_fu_20890_p1 = grp_fu_4352_p1.read();
}

void mnist_fp16_opt::thread_tmp32_V_5_fu_9742_p1() {
    tmp32_V_5_fu_9742_p1 = esl_zext<32,16>(p_Val2_9_reg_24538.read());
}

void mnist_fp16_opt::thread_tmp32_V_6_fu_9751_p2() {
    tmp32_V_6_fu_9751_p2 = (!tmp_102_fu_9745_p2.read().is_01())? sc_lv<32>(): tmp32_V_5_fu_9742_p1.read() << (unsigned short)tmp_102_fu_9745_p2.read().to_uint();
}

void mnist_fp16_opt::thread_tmp32_V_7_fu_9776_p1() {
    tmp32_V_7_fu_9776_p1 = esl_zext<32,16>(p_Result_8_fu_9771_p2.read());
}

void mnist_fp16_opt::thread_tmp32_V_8_fu_10861_p1() {
    tmp32_V_8_fu_10861_p1 = grp_fu_4352_p1.read();
}

void mnist_fp16_opt::thread_tmp32_V_9_fu_14510_p1() {
    tmp32_V_9_fu_14510_p1 = esl_zext<32,16>(p_Val2_31_reg_25835.read());
}

void mnist_fp16_opt::thread_tmp32_V_fu_8476_p1() {
    tmp32_V_fu_8476_p1 = esl_zext<32,16>(p_Val2_4_reg_24225.read());
}

void mnist_fp16_opt::thread_tmp32_V_s_fu_10815_p1() {
    tmp32_V_s_fu_10815_p1 = esl_zext<32,16>(tmp_V_2_reg_24839.read());
}

void mnist_fp16_opt::thread_tmp346_cast_fu_8155_p1() {
    tmp346_cast_fu_8155_p1 = esl_sext<13,7>(tmp70_reg_24141.read());
}

void mnist_fp16_opt::thread_tmp347_cast_cast_fu_8545_p3() {
    tmp347_cast_cast_fu_8545_p3 = (!tmp_82_reg_24256.read()[0].is_01())? sc_lv<8>(): ((tmp_82_reg_24256.read()[0].to_bool())? ap_const_lv8_72: ap_const_lv8_71);
}

void mnist_fp16_opt::thread_tmp348_cast_cast_fu_9811_p3() {
    tmp348_cast_cast_fu_9811_p3 = (!tmp_112_reg_24574.read()[0].is_01())? sc_lv<8>(): ((tmp_112_reg_24574.read()[0].to_bool())? ap_const_lv8_72: ap_const_lv8_71);
}

void mnist_fp16_opt::thread_tmp349_cast_cast_fu_10884_p3() {
    tmp349_cast_cast_fu_10884_p3 = (!tmp_236_reg_24870.read()[0].is_01())? sc_lv<8>(): ((tmp_236_reg_24870.read()[0].to_bool())? ap_const_lv8_72: ap_const_lv8_71);
}

void mnist_fp16_opt::thread_tmp350_cast_mid2_fu_11155_p1() {
    tmp350_cast_mid2_fu_11155_p1 = esl_sext<12,11>(tmp350_cast_mid2_v_reg_24942.read());
}

void mnist_fp16_opt::thread_tmp357_fu_13862_p2() {
    tmp357_fu_13862_p2 = (tmp_148_fu_13814_p2.read() & tmp_149_fu_13820_p2.read());
}

void mnist_fp16_opt::thread_tmp357_mid1_fu_14032_p2() {
    tmp357_mid1_fu_14032_p2 = (tmp_148_mid1_fu_13976_p2.read() & tmp_149_mid1_fu_13982_p2.read());
}

void mnist_fp16_opt::thread_tmp357_mid2_fu_14038_p3() {
    tmp357_mid2_fu_14038_p3 = (!exitcond36_mid_fu_13950_p2.read()[0].is_01())? sc_lv<1>(): ((exitcond36_mid_fu_13950_p2.read()[0].to_bool())? tmp357_mid1_fu_14032_p2.read(): tmp357_mid_fu_13938_p2.read());
}

void mnist_fp16_opt::thread_tmp357_mid_fu_13938_p2() {
    tmp357_mid_fu_13938_p2 = (tmp357_fu_13862_p2.read() & not_exitcond_flatten_9_fu_13932_p2.read());
}

void mnist_fp16_opt::thread_tmp358_fu_14121_p2() {
    tmp358_fu_14121_p2 = (tmp_182_fu_14111_p2.read() & tmp_183_fu_14116_p2.read());
}

void mnist_fp16_opt::thread_tmp360_fu_13868_p2() {
    tmp360_fu_13868_p2 = (!rhs_V_9_cast_fu_13858_p1.read().is_01() || !rhs_V_8_fu_13808_p2.read().is_01())? sc_lv<12>(): (sc_bigint<12>(rhs_V_9_cast_fu_13858_p1.read()) + sc_biguint<12>(rhs_V_8_fu_13808_p2.read()));
}

void mnist_fp16_opt::thread_tmp360_mid1_fu_14046_p2() {
    tmp360_mid1_fu_14046_p2 = (!rhs_V_8_mid2_fu_13924_p3.read().is_01() || !rhs_V_9_cast_mid1_fu_14028_p1.read().is_01())? sc_lv<12>(): (sc_biguint<12>(rhs_V_8_mid2_fu_13924_p3.read()) + sc_bigint<12>(rhs_V_9_cast_mid1_fu_14028_p1.read()));
}

void mnist_fp16_opt::thread_tmp360_mid2_fu_14105_p3() {
    tmp360_mid2_fu_14105_p3 = (!exitcond36_mid_reg_25697.read()[0].is_01())? sc_lv<12>(): ((exitcond36_mid_reg_25697.read()[0].to_bool())? tmp360_mid1_reg_25722.read(): tmp360_mid_fu_14083_p3.read());
}

void mnist_fp16_opt::thread_tmp360_mid_fu_14083_p3() {
    tmp360_mid_fu_14083_p3 = (!exitcond_flatten24_reg_25681.read()[0].is_01())? sc_lv<12>(): ((exitcond_flatten24_reg_25681.read()[0].to_bool())? rhs_V_8_mid1_reg_25692.read(): tmp360_reg_25667.read());
}

void mnist_fp16_opt::thread_tmp361_cast_fu_14189_p1() {
    tmp361_cast_fu_14189_p1 = esl_sext<12,6>(tmp361_reg_25751.read());
}

void mnist_fp16_opt::thread_tmp361_fu_14174_p2() {
    tmp361_fu_14174_p2 = (!ap_const_lv6_31.is_01() || !lhs_V_18_cast_fu_14170_p1.read().is_01())? sc_lv<6>(): (sc_bigint<6>(ap_const_lv6_31) + sc_bigint<6>(lhs_V_18_cast_fu_14170_p1.read()));
}

void mnist_fp16_opt::thread_tmp362_cast_cast_fu_14579_p3() {
    tmp362_cast_cast_fu_14579_p3 = (!tmp_260_reg_25866.read()[0].is_01())? sc_lv<8>(): ((tmp_260_reg_25866.read()[0].to_bool())? ap_const_lv8_72: ap_const_lv8_71);
}

void mnist_fp16_opt::thread_tmp363_cast_cast_fu_15781_p3() {
    tmp363_cast_cast_fu_15781_p3 = (!tmp_305_reg_26174.read()[0].is_01())? sc_lv<8>(): ((tmp_305_reg_26174.read()[0].to_bool())? ap_const_lv8_72: ap_const_lv8_71);
}

void mnist_fp16_opt::thread_tmp364_cast_cast_fu_16839_p3() {
    tmp364_cast_cast_fu_16839_p3 = (!tmp_414_reg_26465.read()[0].is_01())? sc_lv<8>(): ((tmp_414_reg_26465.read()[0].to_bool())? ap_const_lv8_72: ap_const_lv8_71);
}

void mnist_fp16_opt::thread_tmp380_fu_18992_p2() {
    tmp380_fu_18992_p2 = (!rhs_V_17_cast_fu_18982_p1.read().is_01() || !rhs_V_5_fu_18954_p2.read().is_01())? sc_lv<11>(): (sc_bigint<11>(rhs_V_17_cast_fu_18982_p1.read()) + sc_biguint<11>(rhs_V_5_fu_18954_p2.read()));
}

void mnist_fp16_opt::thread_tmp380_mid1_fu_19156_p2() {
    tmp380_mid1_fu_19156_p2 = (!rhs_V_5_mid2_fu_19048_p3.read().is_01() || !rhs_V_17_cast_mid1_fu_19138_p1.read().is_01())? sc_lv<11>(): (sc_biguint<11>(rhs_V_5_mid2_fu_19048_p3.read()) + sc_bigint<11>(rhs_V_17_cast_mid1_fu_19138_p1.read()));
}

void mnist_fp16_opt::thread_tmp380_mid2_fu_19162_p3() {
    tmp380_mid2_fu_19162_p3 = (!exitcond63_mid_fu_19082_p2.read()[0].is_01())? sc_lv<11>(): ((exitcond63_mid_fu_19082_p2.read()[0].to_bool())? tmp380_mid1_fu_19156_p2.read(): tmp380_mid_fu_19068_p3.read());
}

void mnist_fp16_opt::thread_tmp380_mid_fu_19068_p3() {
    tmp380_mid_fu_19068_p3 = (!exitcond_flatten42_fu_19016_p2.read()[0].is_01())? sc_lv<11>(): ((exitcond_flatten42_fu_19016_p2.read()[0].to_bool())? rhs_V_5_mid1_fu_19042_p2.read(): tmp380_fu_18992_p2.read());
}

void mnist_fp16_opt::thread_tmp381_cast_fu_19291_p1() {
    tmp381_cast_fu_19291_p1 = esl_sext<11,5>(tmp381_fu_19285_p2.read());
}

void mnist_fp16_opt::thread_tmp381_fu_19285_p2() {
    tmp381_fu_19285_p2 = (!ap_const_lv5_18.is_01() || !lhs_V_29_cast_fu_19281_p1.read().is_01())? sc_lv<5>(): (sc_bigint<5>(ap_const_lv5_18) + sc_bigint<5>(lhs_V_29_cast_fu_19281_p1.read()));
}

void mnist_fp16_opt::thread_tmp382_cast_cast_fu_19674_p3() {
    tmp382_cast_cast_fu_19674_p3 = (!tmp_444_reg_27244.read()[0].is_01())? sc_lv<8>(): ((tmp_444_reg_27244.read()[0].to_bool())? ap_const_lv8_72: ap_const_lv8_71);
}

void mnist_fp16_opt::thread_tmp383_cast_cast_fu_20913_p3() {
    tmp383_cast_cast_fu_20913_p3 = (!tmp_475_reg_27569.read()[0].is_01())? sc_lv<8>(): ((tmp_475_reg_27569.read()[0].to_bool())? ap_const_lv8_72: ap_const_lv8_71);
}

void mnist_fp16_opt::thread_tmp384_cast_cast_fu_21749_p3() {
    tmp384_cast_cast_fu_21749_p3 = (!tmp_553_reg_27807.read()[0].is_01())? sc_lv<8>(): ((tmp_553_reg_27807.read()[0].to_bool())? ap_const_lv8_72: ap_const_lv8_71);
}

void mnist_fp16_opt::thread_tmp385_cast_cast_fu_22408_p3() {
    tmp385_cast_cast_fu_22408_p3 = (!tmp_566_reg_27998.read()[0].is_01())? sc_lv<8>(): ((tmp_566_reg_27998.read()[0].to_bool())? ap_const_lv8_72: ap_const_lv8_71);
}

void mnist_fp16_opt::thread_tmp69_fu_7774_p2() {
    tmp69_fu_7774_p2 = (!rhs_V_5_cast_fu_7770_p1.read().is_01() || !rhs_V_6_fu_7734_p2.read().is_01())? sc_lv<13>(): (sc_bigint<13>(rhs_V_5_cast_fu_7770_p1.read()) + sc_biguint<13>(rhs_V_6_fu_7734_p2.read()));
}

void mnist_fp16_opt::thread_tmp69_mid1_fu_8010_p2() {
    tmp69_mid1_fu_8010_p2 = (!rhs_V_6_mid2_fu_7927_p3.read().is_01() || !rhs_V_5_cast_mid1_fu_8006_p1.read().is_01())? sc_lv<13>(): (sc_biguint<13>(rhs_V_6_mid2_fu_7927_p3.read()) + sc_bigint<13>(rhs_V_5_cast_mid1_fu_8006_p1.read()));
}

void mnist_fp16_opt::thread_tmp69_mid2_fu_8016_p3() {
    tmp69_mid2_fu_8016_p3 = (!exitcond13_mid_reg_24080.read()[0].is_01())? sc_lv<13>(): ((exitcond13_mid_reg_24080.read()[0].to_bool())? tmp69_mid1_fu_8010_p2.read(): tmp69_mid_fu_7933_p3.read());
}

void mnist_fp16_opt::thread_tmp69_mid_fu_7933_p3() {
    tmp69_mid_fu_7933_p3 = (!exitcond_flatten6_reg_24067.read()[0].is_01())? sc_lv<13>(): ((exitcond_flatten6_reg_24067.read()[0].to_bool())? rhs_V_6_mid1_fu_7921_p2.read(): tmp69_reg_24048.read());
}

void mnist_fp16_opt::thread_tmp70_fu_8140_p2() {
    tmp70_fu_8140_p2 = (!ap_const_lv7_63.is_01() || !lhs_V_6_cast_fu_8136_p1.read().is_01())? sc_lv<7>(): (sc_bigint<7>(ap_const_lv7_63) + sc_bigint<7>(lhs_V_6_cast_fu_8136_p1.read()));
}

void mnist_fp16_opt::thread_tmp_1000_fu_5608_p1() {
    tmp_1000_fu_5608_p1 = esl_sext<12,8>(tmp_1266_fu_5598_p4.read());
}

void mnist_fp16_opt::thread_tmp_1001_fu_5612_p1() {
    tmp_1001_fu_5612_p1 = esl_sext<12,9>(tmp_1267_reg_23484.read());
}

void mnist_fp16_opt::thread_tmp_1002_fu_5615_p3() {
    tmp_1002_fu_5615_p3 = (!tmp_1265_reg_23478.read()[0].is_01())? sc_lv<12>(): ((tmp_1265_reg_23478.read()[0].to_bool())? tmp_1000_fu_5608_p1.read(): tmp_1001_fu_5612_p1.read());
}

void mnist_fp16_opt::thread_tmp_1003_fu_5067_p2() {
    tmp_1003_fu_5067_p2 = (tmp_996_fu_5061_p2.read() | tmp_6_reg_23135.read());
}

void mnist_fp16_opt::thread_tmp_1004_fu_5072_p2() {
    tmp_1004_fu_5072_p2 = (tmp_11_reg_23152.read() | tmp_1003_fu_5067_p2.read());
}

void mnist_fp16_opt::thread_tmp_1005_fu_6531_p2() {
    tmp_1005_fu_6531_p2 = (!p_shl114_cast_fu_6511_p3.read().is_01() || !p_shl115_cast_fu_6523_p3.read().is_01())? sc_lv<11>(): (sc_biguint<11>(p_shl114_cast_fu_6511_p3.read()) - sc_biguint<11>(p_shl115_cast_fu_6523_p3.read()));
}

void mnist_fp16_opt::thread_tmp_1006_cast_fu_6562_p1() {
    tmp_1006_cast_fu_6562_p1 = esl_sext<64,11>(tmp_1006_fu_6557_p2.read());
}

void mnist_fp16_opt::thread_tmp_1006_fu_6557_p2() {
    tmp_1006_fu_6557_p2 = (!ap_const_lv11_16.is_01() || !tmp_1005_reg_23722.read().is_01())? sc_lv<11>(): (sc_biguint<11>(ap_const_lv11_16) + sc_biguint<11>(tmp_1005_reg_23722.read()));
}

void mnist_fp16_opt::thread_tmp_1007_cast_fu_6572_p1() {
    tmp_1007_cast_fu_6572_p1 = esl_sext<64,11>(tmp_1007_fu_6567_p2.read());
}

void mnist_fp16_opt::thread_tmp_1007_fu_6567_p2() {
    tmp_1007_fu_6567_p2 = (!ap_const_lv11_17.is_01() || !tmp_1005_reg_23722.read().is_01())? sc_lv<11>(): (sc_biguint<11>(ap_const_lv11_17) + sc_biguint<11>(tmp_1005_reg_23722.read()));
}

void mnist_fp16_opt::thread_tmp_1008_fu_15868_p3() {
    tmp_1008_fu_15868_p3 = esl_concat<4,4>(tmp_207_mid2_v_fu_15860_p3.read(), ap_const_lv4_0);
}

void mnist_fp16_opt::thread_tmp_1009_fu_15880_p3() {
    tmp_1009_fu_15880_p3 = esl_concat<4,1>(tmp_207_mid2_v_fu_15860_p3.read(), ap_const_lv1_0);
}

void mnist_fp16_opt::thread_tmp_100_fu_10135_p3() {
    tmp_100_fu_10135_p3 = esl_concat<1,52>(ap_const_lv1_1, tmp_596_reg_24666.read());
}

void mnist_fp16_opt::thread_tmp_1010_cast_fu_15898_p1() {
    tmp_1010_cast_fu_15898_p1 = esl_sext<10,9>(tmp_1010_fu_15892_p2.read());
}

void mnist_fp16_opt::thread_tmp_1010_fu_15892_p2() {
    tmp_1010_fu_15892_p2 = (!p_shl116_cast_fu_15876_p1.read().is_01() || !p_shl117_cast_fu_15888_p1.read().is_01())? sc_lv<9>(): (sc_biguint<9>(p_shl116_cast_fu_15876_p1.read()) - sc_biguint<9>(p_shl117_cast_fu_15888_p1.read()));
}

void mnist_fp16_opt::thread_tmp_1011_fu_15926_p2() {
    tmp_1011_fu_15926_p2 = (exitcond61_mid_fu_15914_p2.read() | exitcond_flatten29_fu_15846_p2.read());
}

void mnist_fp16_opt::thread_tmp_1012_fu_15952_p2() {
    tmp_1012_fu_15952_p2 = (!tmp_488_mid2_cast_fu_15948_p1.read().is_01() || !tmp_1010_cast_fu_15898_p1.read().is_01())? sc_lv<10>(): (sc_biguint<10>(tmp_488_mid2_cast_fu_15948_p1.read()) + sc_bigint<10>(tmp_1010_cast_fu_15898_p1.read()));
}

void mnist_fp16_opt::thread_tmp_1013_fu_5077_p2() {
    tmp_1013_fu_5077_p2 = (tmp_25_reg_23169.read() | tmp_1004_fu_5072_p2.read());
}

void mnist_fp16_opt::thread_tmp_1014_fu_14132_p1() {
    tmp_1014_fu_14132_p1 = p_35_mid2_reg_25702.read().range(4-1, 0);
}

void mnist_fp16_opt::thread_tmp_1015_fu_16000_p2() {
    tmp_1015_fu_16000_p2 = (!p_shl118_cast_fu_15982_p3.read().is_01() || !p_shl119_cast_fu_15996_p1.read().is_01())? sc_lv<12>(): (sc_biguint<12>(p_shl118_cast_fu_15982_p3.read()) - sc_bigint<12>(p_shl119_cast_fu_15996_p1.read()));
}

void mnist_fp16_opt::thread_tmp_1016_cast_fu_16015_p1() {
    tmp_1016_cast_fu_16015_p1 = esl_zext<64,12>(tmp_1016_fu_16009_p2.read());
}

void mnist_fp16_opt::thread_tmp_1016_fu_16009_p2() {
    tmp_1016_fu_16009_p2 = (!tmp_489_cast_fu_16006_p1.read().is_01() || !tmp_1015_fu_16000_p2.read().is_01())? sc_lv<12>(): (sc_biguint<12>(tmp_489_cast_fu_16006_p1.read()) + sc_biguint<12>(tmp_1015_fu_16000_p2.read()));
}

void mnist_fp16_opt::thread_tmp_1017_cast_fu_15824_p1() {
    tmp_1017_cast_fu_15824_p1 = esl_zext<64,12>(tmp_1017_reg_26118.read());
}

void mnist_fp16_opt::thread_tmp_1017_fu_15598_p2() {
    tmp_1017_fu_15598_p2 = (!tmp_980_reg_25910.read().is_01() || !tmp_316_cast_fu_15595_p1.read().is_01())? sc_lv<12>(): (sc_biguint<12>(tmp_980_reg_25910.read()) + sc_biguint<12>(tmp_316_cast_fu_15595_p1.read()));
}

void mnist_fp16_opt::thread_tmp_1018_cast_fu_14849_p1() {
    tmp_1018_cast_fu_14849_p1 = esl_zext<64,8>(tmp_1018_fu_14844_p2.read());
}

void mnist_fp16_opt::thread_tmp_1018_fu_14844_p2() {
    tmp_1018_fu_14844_p2 = (!tmp_972_cast_reg_25890.read().is_01() || !tmp_241_mid2_cast_fu_14840_p1.read().is_01())? sc_lv<8>(): (sc_biguint<8>(tmp_972_cast_reg_25890.read()) + sc_biguint<8>(tmp_241_mid2_cast_fu_14840_p1.read()));
}

void mnist_fp16_opt::thread_tmp_1019_fu_14201_p1() {
    tmp_1019_fu_14201_p1 = mul18_fu_22958_p2.read().range(25-1, 0);
}

void mnist_fp16_opt::thread_tmp_101_fu_10126_p2() {
    tmp_101_fu_10126_p2 = (!tmp_589_fu_10100_p1.read().is_01() || !ap_const_lv63_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_589_fu_10100_p1.read() == ap_const_lv63_0);
}

void mnist_fp16_opt::thread_tmp_1020_fu_14865_p2() {
    tmp_1020_fu_14865_p2 = (!p_shl4_fu_14861_p1.read().is_01() || !tmp_1018_cast_fu_14849_p1.read().is_01())? sc_lv<64>(): (sc_biguint<64>(p_shl4_fu_14861_p1.read()) - sc_biguint<64>(tmp_1018_cast_fu_14849_p1.read()));
}

void mnist_fp16_opt::thread_tmp_1021_fu_14902_p2() {
    tmp_1021_fu_14902_p2 = (exitcond52_mid_fu_14890_p2.read() | exitcond_flatten30_fu_14818_p2.read());
}

void mnist_fp16_opt::thread_tmp_1022_fu_14945_p2() {
    tmp_1022_fu_14945_p2 = (!tmp_1020_fu_14865_p2.read().is_01() || !tmp_259_mid2_cast_fu_14941_p1.read().is_01())? sc_lv<64>(): (sc_biguint<64>(tmp_1020_fu_14865_p2.read()) + sc_biguint<64>(tmp_259_mid2_cast_fu_14941_p1.read()));
}

void mnist_fp16_opt::thread_tmp_1024_fu_14986_p2() {
    tmp_1024_fu_14986_p2 = (!p_shl121_cast_fu_14979_p3.read().is_01() || !tmp_1290_reg_25946.read().is_01())? sc_lv<11>(): (sc_biguint<11>(p_shl121_cast_fu_14979_p3.read()) - sc_biguint<11>(tmp_1290_reg_25946.read()));
}

void mnist_fp16_opt::thread_tmp_1025_fu_14999_p4() {
    tmp_1025_fu_14999_p4 = esl_concat<8,4>(esl_concat<4,4>(tmp_241_mid2_v_reg_25924.read(), tmp_258_mid2_v_reg_25936.read()), r_V_15_fu_14994_p2.read());
}

void mnist_fp16_opt::thread_tmp_1026_fu_15007_p1() {
    tmp_1026_fu_15007_p1 = esl_zext<64,12>(tmp_1025_fu_14999_p4.read());
}

void mnist_fp16_opt::thread_tmp_1027_cast_fu_15021_p1() {
    tmp_1027_cast_fu_15021_p1 = esl_zext<64,11>(tmp_1027_fu_15015_p2.read());
}

void mnist_fp16_opt::thread_tmp_1027_fu_15015_p2() {
    tmp_1027_fu_15015_p2 = (!tmp_1024_fu_14986_p2.read().is_01() || !tmp_347_cast_fu_15012_p1.read().is_01())? sc_lv<11>(): (sc_biguint<11>(tmp_1024_fu_14986_p2.read()) + sc_biguint<11>(tmp_347_cast_fu_15012_p1.read()));
}

void mnist_fp16_opt::thread_tmp_1028_fu_11536_p1() {
    tmp_1028_fu_11536_p1 = esl_sext<12,9>(tmp_1312_fu_11526_p4.read());
}

void mnist_fp16_opt::thread_tmp_1029_fu_11540_p1() {
    tmp_1029_fu_11540_p1 = esl_sext<12,10>(tmp_1313_reg_25118.read());
}

void mnist_fp16_opt::thread_tmp_102_fu_9745_p2() {
    tmp_102_fu_9745_p2 = (!ap_const_lv32_1F.is_01() || !msb_idx_3_cast_fu_9722_p1.read().is_01())? sc_lv<32>(): (sc_biguint<32>(ap_const_lv32_1F) - sc_biguint<32>(msb_idx_3_cast_fu_9722_p1.read()));
}

void mnist_fp16_opt::thread_tmp_1030_fu_11543_p3() {
    tmp_1030_fu_11543_p3 = (!tmp_1311_reg_25110.read()[0].is_01())? sc_lv<12>(): ((tmp_1311_reg_25110.read()[0].to_bool())? tmp_1028_fu_11536_p1.read(): tmp_1029_fu_11540_p1.read());
}

void mnist_fp16_opt::thread_tmp_1031_fu_11994_p1() {
    tmp_1031_fu_11994_p1 = esl_sext<13,5>(tmp_1316_fu_11984_p4.read());
}

void mnist_fp16_opt::thread_tmp_1032_fu_11998_p1() {
    tmp_1032_fu_11998_p1 = esl_sext<13,6>(tmp_1317_reg_25256.read());
}

void mnist_fp16_opt::thread_tmp_1033_fu_14238_p4() {
    tmp_1033_fu_14238_p4 = neg_mul18_fu_14233_p2.read().range(24, 16);
}

void mnist_fp16_opt::thread_tmp_1035_fu_14331_p1() {
    tmp_1035_fu_14331_p1 = grp_fu_14275_p2.read().range(9-1, 0);
}

void mnist_fp16_opt::thread_tmp_1036_fu_12022_p3() {
    tmp_1036_fu_12022_p3 = (!tmp_1311_reg_25110_pp10_iter1_reg.read()[0].is_01())? sc_lv<2>(): ((tmp_1311_reg_25110_pp10_iter1_reg.read()[0].to_bool())? neg_ti27_fu_12012_p2.read(): tmp_1319_fu_12018_p1.read());
}

void mnist_fp16_opt::thread_tmp_1037_fu_12618_p3() {
    tmp_1037_fu_12618_p3 = esl_concat<2,4>(tmp_1036_reg_25271.read(), ap_const_lv4_0);
}

void mnist_fp16_opt::thread_tmp_1038_fu_12629_p3() {
    tmp_1038_fu_12629_p3 = esl_concat<2,1>(tmp_1036_reg_25271.read(), ap_const_lv1_0);
}

void mnist_fp16_opt::thread_tmp_1039_cast_fu_12646_p1() {
    tmp_1039_cast_fu_12646_p1 = esl_sext<8,7>(tmp_1039_fu_12640_p2.read());
}

void mnist_fp16_opt::thread_tmp_1039_fu_12640_p2() {
    tmp_1039_fu_12640_p2 = (!p_shl122_cast_fu_12625_p1.read().is_01() || !p_shl123_cast_fu_12636_p1.read().is_01())? sc_lv<7>(): (sc_biguint<7>(p_shl122_cast_fu_12625_p1.read()) - sc_biguint<7>(p_shl123_cast_fu_12636_p1.read()));
}

void mnist_fp16_opt::thread_tmp_103_fu_5804_p1() {
    tmp_103_fu_5804_p1 = esl_sext<12,9>(tmp_95_reg_23542.read());
}

void mnist_fp16_opt::thread_tmp_1040_fu_12650_p2() {
    tmp_1040_fu_12650_p2 = (!tmp_1314_fu_12614_p1.read().is_01() || !tmp_1039_cast_fu_12646_p1.read().is_01())? sc_lv<8>(): (sc_biguint<8>(tmp_1314_fu_12614_p1.read()) + sc_bigint<8>(tmp_1039_cast_fu_12646_p1.read()));
}

void mnist_fp16_opt::thread_tmp_1041_fu_14221_p1() {
    tmp_1041_fu_14221_p1 = mul19_fu_22966_p2.read().range(25-1, 0);
}

void mnist_fp16_opt::thread_tmp_1042_fu_14286_p4() {
    tmp_1042_fu_14286_p4 = neg_mul19_fu_14281_p2.read().range(24, 20);
}

void mnist_fp16_opt::thread_tmp_1043_fu_12680_p2() {
    tmp_1043_fu_12680_p2 = (!p_shl124_cast_fu_12660_p3.read().is_01() || !p_shl125_cast_fu_12676_p1.read().is_01())? sc_lv<11>(): (sc_biguint<11>(p_shl124_cast_fu_12660_p3.read()) - sc_bigint<11>(p_shl125_cast_fu_12676_p1.read()));
}

void mnist_fp16_opt::thread_tmp_1044_cast_fu_12691_p1() {
    tmp_1044_cast_fu_12691_p1 = esl_zext<64,11>(tmp_1044_fu_12686_p2.read());
}

void mnist_fp16_opt::thread_tmp_1044_fu_12686_p2() {
    tmp_1044_fu_12686_p2 = (!ap_const_lv11_A.is_01() || !tmp_1043_reg_25362.read().is_01())? sc_lv<11>(): (sc_biguint<11>(ap_const_lv11_A) + sc_biguint<11>(tmp_1043_reg_25362.read()));
}

void mnist_fp16_opt::thread_tmp_1045_cast_fu_12701_p1() {
    tmp_1045_cast_fu_12701_p1 = esl_zext<64,11>(tmp_1045_fu_12696_p2.read());
}

void mnist_fp16_opt::thread_tmp_1045_fu_12696_p2() {
    tmp_1045_fu_12696_p2 = (!ap_const_lv11_B.is_01() || !tmp_1043_reg_25362.read().is_01())? sc_lv<11>(): (sc_biguint<11>(ap_const_lv11_B) + sc_biguint<11>(tmp_1043_reg_25362.read()));
}

void mnist_fp16_opt::thread_tmp_1046_fu_5672_p1() {
    tmp_1046_fu_5672_p1 = esl_sext<12,8>(tmp_1324_fu_5662_p4.read());
}

void mnist_fp16_opt::thread_tmp_1047_fu_5676_p1() {
    tmp_1047_fu_5676_p1 = esl_sext<12,9>(tmp_1325_reg_23505.read());
}

void mnist_fp16_opt::thread_tmp_1048_fu_5679_p3() {
    tmp_1048_fu_5679_p3 = (!tmp_1323_reg_23499.read()[0].is_01())? sc_lv<12>(): ((tmp_1323_reg_23499.read()[0].to_bool())? tmp_1046_fu_5672_p1.read(): tmp_1047_fu_5676_p1.read());
}

void mnist_fp16_opt::thread_tmp_104_cast_fu_9028_p1() {
    tmp_104_cast_fu_9028_p1 = esl_zext<13,5>(r_V_5_reg_24351.read());
}

void mnist_fp16_opt::thread_tmp_104_fu_5807_p3() {
    tmp_104_fu_5807_p3 = (!tmp_89_reg_23536.read()[0].is_01())? sc_lv<12>(): ((tmp_89_reg_23536.read()[0].to_bool())? tmp_93_fu_5800_p1.read(): tmp_103_fu_5804_p1.read());
}

void mnist_fp16_opt::thread_tmp_1050_fu_14310_p1() {
    tmp_1050_fu_14310_p1 = p_v4_fu_14303_p3.read().range(3-1, 0);
}

void mnist_fp16_opt::thread_tmp_1051_fu_6601_p2() {
    tmp_1051_fu_6601_p2 = (!p_shl126_cast_fu_6581_p3.read().is_01() || !p_shl127_cast_fu_6593_p3.read().is_01())? sc_lv<11>(): (sc_biguint<11>(p_shl126_cast_fu_6581_p3.read()) - sc_biguint<11>(p_shl127_cast_fu_6593_p3.read()));
}

void mnist_fp16_opt::thread_tmp_1052_cast_fu_6632_p1() {
    tmp_1052_cast_fu_6632_p1 = esl_sext<64,11>(tmp_1052_fu_6627_p2.read());
}

void mnist_fp16_opt::thread_tmp_1052_fu_6627_p2() {
    tmp_1052_fu_6627_p2 = (!ap_const_lv11_18.is_01() || !tmp_1051_reg_23738.read().is_01())? sc_lv<11>(): (sc_biguint<11>(ap_const_lv11_18) + sc_biguint<11>(tmp_1051_reg_23738.read()));
}

void mnist_fp16_opt::thread_tmp_1053_cast_fu_6642_p1() {
    tmp_1053_cast_fu_6642_p1 = esl_sext<64,11>(tmp_1053_fu_6637_p2.read());
}

void mnist_fp16_opt::thread_tmp_1053_fu_6637_p2() {
    tmp_1053_fu_6637_p2 = (!ap_const_lv11_19.is_01() || !tmp_1051_reg_23738.read().is_01())? sc_lv<11>(): (sc_biguint<11>(ap_const_lv11_19) + sc_biguint<11>(tmp_1051_reg_23738.read()));
}

void mnist_fp16_opt::thread_tmp_1054_fu_16380_p3() {
    tmp_1054_fu_16380_p3 = esl_concat<4,4>(tmp_238_mid2_v_reg_26334.read(), ap_const_lv4_0);
}

void mnist_fp16_opt::thread_tmp_1055_fu_16391_p3() {
    tmp_1055_fu_16391_p3 = esl_concat<4,1>(tmp_238_mid2_v_reg_26334.read(), ap_const_lv1_0);
}

void mnist_fp16_opt::thread_tmp_1056_cast_fu_16408_p1() {
    tmp_1056_cast_fu_16408_p1 = esl_sext<10,9>(tmp_1056_fu_16402_p2.read());
}

void mnist_fp16_opt::thread_tmp_1056_fu_16402_p2() {
    tmp_1056_fu_16402_p2 = (!p_shl128_cast_fu_16387_p1.read().is_01() || !p_shl129_cast_fu_16398_p1.read().is_01())? sc_lv<9>(): (sc_biguint<9>(p_shl128_cast_fu_16387_p1.read()) - sc_biguint<9>(p_shl129_cast_fu_16398_p1.read()));
}

void mnist_fp16_opt::thread_tmp_1057_fu_16412_p3() {
    tmp_1057_fu_16412_p3 = esl_concat<4,3>(tmp_238_mid2_v_reg_26334.read(), ap_const_lv3_0);
}

void mnist_fp16_opt::thread_tmp_1058_cast_fu_16429_p1() {
    tmp_1058_cast_fu_16429_p1 = esl_sext<9,8>(tmp_1058_fu_16423_p2.read());
}

void mnist_fp16_opt::thread_tmp_1058_fu_16423_p2() {
    tmp_1058_fu_16423_p2 = (!p_shl130_cast_fu_16419_p1.read().is_01() || !tmp_238_mid2_cast_fu_16377_p1.read().is_01())? sc_lv<8>(): (sc_biguint<8>(p_shl130_cast_fu_16419_p1.read()) - sc_biguint<8>(tmp_238_mid2_cast_fu_16377_p1.read()));
}

void mnist_fp16_opt::thread_tmp_1059_fu_16470_p2() {
    tmp_1059_fu_16470_p2 = (exitcond66_mid_fu_16459_p2.read() | exitcond_flatten32_reg_26320.read());
}

void mnist_fp16_opt::thread_tmp_105_cast_fu_6482_p1() {
    tmp_105_cast_fu_6482_p1 = esl_sext<64,11>(tmp_62_fu_6477_p2.read());
}

void mnist_fp16_opt::thread_tmp_105_fu_6677_p1() {
    tmp_105_fu_6677_p1 = grp_fu_5827_p2.read().range(6-1, 0);
}

void mnist_fp16_opt::thread_tmp_1060_cast_fu_16500_p1() {
    tmp_1060_cast_fu_16500_p1 = esl_sext<10,9>(tmp_1060_fu_16494_p2.read());
}

void mnist_fp16_opt::thread_tmp_1060_fu_16494_p2() {
    tmp_1060_fu_16494_p2 = (!lhs_V_11_mid2_cast_fu_16490_p1.read().is_01() || !tmp_1058_cast_fu_16429_p1.read().is_01())? sc_lv<9>(): (sc_biguint<9>(lhs_V_11_mid2_cast_fu_16490_p1.read()) + sc_bigint<9>(tmp_1058_cast_fu_16429_p1.read()));
}

void mnist_fp16_opt::thread_tmp_1061_fu_14320_p1() {
    tmp_1061_fu_14320_p1 = p_v4_fu_14303_p3.read().range(3-1, 0);
}

void mnist_fp16_opt::thread_tmp_1062_fu_16516_p2() {
    tmp_1062_fu_16516_p2 = (!p_shl131_cast_fu_16508_p3.read().is_01() || !tmp_1060_cast_fu_16500_p1.read().is_01())? sc_lv<10>(): (sc_biguint<10>(p_shl131_cast_fu_16508_p3.read()) - sc_bigint<10>(tmp_1060_cast_fu_16500_p1.read()));
}

void mnist_fp16_opt::thread_tmp_1063_cast_fu_16548_p1() {
    tmp_1063_cast_fu_16548_p1 = esl_zext<64,10>(tmp_1063_fu_16542_p2.read());
}

void mnist_fp16_opt::thread_tmp_1063_fu_16542_p2() {
    tmp_1063_fu_16542_p2 = (!lhs_V_5_cast_fu_16538_p1.read().is_01() || !tmp_1062_fu_16516_p2.read().is_01())? sc_lv<10>(): (sc_biguint<10>(lhs_V_5_cast_fu_16538_p1.read()) + sc_biguint<10>(tmp_1062_fu_16516_p2.read()));
}

void mnist_fp16_opt::thread_tmp_1064_fu_11598_p1() {
    tmp_1064_fu_11598_p1 = esl_sext<12,9>(tmp_1335_fu_11588_p4.read());
}

void mnist_fp16_opt::thread_tmp_1065_fu_11602_p1() {
    tmp_1065_fu_11602_p1 = esl_sext<12,10>(tmp_1336_reg_25156.read());
}

void mnist_fp16_opt::thread_tmp_1066_fu_11605_p3() {
    tmp_1066_fu_11605_p3 = (!tmp_1334_reg_25128.read()[0].is_01())? sc_lv<12>(): ((tmp_1334_reg_25128.read()[0].to_bool())? tmp_1064_fu_11598_p1.read(): tmp_1065_fu_11602_p1.read());
}

void mnist_fp16_opt::thread_tmp_1067_fu_12044_p1() {
    tmp_1067_fu_12044_p1 = esl_sext<13,5>(tmp_1339_fu_12034_p4.read());
}

void mnist_fp16_opt::thread_tmp_1068_fu_12048_p1() {
    tmp_1068_fu_12048_p1 = esl_sext<13,6>(tmp_1340_reg_25266.read());
}

void mnist_fp16_opt::thread_tmp_1069_fu_14373_p1() {
    tmp_1069_fu_14373_p1 = tmp_940_fu_14367_p2.read().range(8-1, 0);
}

void mnist_fp16_opt::thread_tmp_106_fu_9128_p1() {
    tmp_106_fu_9128_p1 = esl_zext<12,11>(exp_tmp_V_reg_24392.read());
}

void mnist_fp16_opt::thread_tmp_1070_fu_14387_p3() {
    tmp_1070_fu_14387_p3 = esl_concat<9,1>(tmp_940_reg_25800.read(), ap_const_lv1_0);
}

void mnist_fp16_opt::thread_tmp_1071_fu_11329_p1() {
    tmp_1071_fu_11329_p1 = mul20_fu_22877_p2.read().range(25-1, 0);
}

void mnist_fp16_opt::thread_tmp_1072_fu_12072_p3() {
    tmp_1072_fu_12072_p3 = (!tmp_1334_reg_25128_pp10_iter1_reg.read()[0].is_01())? sc_lv<2>(): ((tmp_1334_reg_25128_pp10_iter1_reg.read()[0].to_bool())? neg_ti30_fu_12062_p2.read(): tmp_1342_fu_12068_p1.read());
}

void mnist_fp16_opt::thread_tmp_1073_fu_12710_p3() {
    tmp_1073_fu_12710_p3 = esl_concat<2,4>(tmp_1072_reg_25277.read(), ap_const_lv4_0);
}

void mnist_fp16_opt::thread_tmp_1074_fu_12721_p3() {
    tmp_1074_fu_12721_p3 = esl_concat<2,1>(tmp_1072_reg_25277.read(), ap_const_lv1_0);
}

void mnist_fp16_opt::thread_tmp_1075_cast_fu_12738_p1() {
    tmp_1075_cast_fu_12738_p1 = esl_sext<8,7>(tmp_1075_fu_12732_p2.read());
}

void mnist_fp16_opt::thread_tmp_1075_fu_12732_p2() {
    tmp_1075_fu_12732_p2 = (!p_shl132_cast_fu_12717_p1.read().is_01() || !p_shl133_cast_fu_12728_p1.read().is_01())? sc_lv<7>(): (sc_biguint<7>(p_shl132_cast_fu_12717_p1.read()) - sc_biguint<7>(p_shl133_cast_fu_12728_p1.read()));
}

void mnist_fp16_opt::thread_tmp_1076_fu_12742_p2() {
    tmp_1076_fu_12742_p2 = (!tmp_1337_fu_12706_p1.read().is_01() || !tmp_1075_cast_fu_12738_p1.read().is_01())? sc_lv<8>(): (sc_biguint<8>(tmp_1337_fu_12706_p1.read()) + sc_bigint<8>(tmp_1075_cast_fu_12738_p1.read()));
}

void mnist_fp16_opt::thread_tmp_1078_fu_11393_p4() {
    tmp_1078_fu_11393_p4 = neg_mul20_fu_11388_p2.read().range(24, 16);
}

void mnist_fp16_opt::thread_tmp_1079_fu_12772_p2() {
    tmp_1079_fu_12772_p2 = (!p_shl134_cast_fu_12752_p3.read().is_01() || !p_shl135_cast_fu_12768_p1.read().is_01())? sc_lv<11>(): (sc_biguint<11>(p_shl134_cast_fu_12752_p3.read()) - sc_bigint<11>(p_shl135_cast_fu_12768_p1.read()));
}

void mnist_fp16_opt::thread_tmp_107_fu_7528_p2() {
    tmp_107_fu_7528_p2 = (!F2_fu_7522_p2.read().is_01() || !ap_const_lv12_E.is_01())? sc_lv<1>(): (sc_bigint<12>(F2_fu_7522_p2.read()) > sc_bigint<12>(ap_const_lv12_E));
}

void mnist_fp16_opt::thread_tmp_1080_cast_fu_12803_p1() {
    tmp_1080_cast_fu_12803_p1 = esl_zext<64,11>(tmp_1080_fu_12798_p2.read());
}

void mnist_fp16_opt::thread_tmp_1080_fu_12798_p2() {
    tmp_1080_fu_12798_p2 = (!ap_const_lv11_C.is_01() || !tmp_1079_reg_25383.read().is_01())? sc_lv<11>(): (sc_biguint<11>(ap_const_lv11_C) + sc_biguint<11>(tmp_1079_reg_25383.read()));
}

void mnist_fp16_opt::thread_tmp_1081_cast_fu_12813_p1() {
    tmp_1081_cast_fu_12813_p1 = esl_zext<64,11>(tmp_1081_fu_12808_p2.read());
}

void mnist_fp16_opt::thread_tmp_1081_fu_12808_p2() {
    tmp_1081_fu_12808_p2 = (!ap_const_lv11_D.is_01() || !tmp_1079_reg_25383.read().is_01())? sc_lv<11>(): (sc_biguint<11>(ap_const_lv11_D) + sc_biguint<11>(tmp_1079_reg_25383.read()));
}

void mnist_fp16_opt::thread_tmp_1082_fu_5752_p1() {
    tmp_1082_fu_5752_p1 = esl_sext<12,8>(tmp_1347_fu_5742_p4.read());
}

void mnist_fp16_opt::thread_tmp_1083_fu_5756_p1() {
    tmp_1083_fu_5756_p1 = esl_sext<12,9>(tmp_1348_reg_23526.read());
}

void mnist_fp16_opt::thread_tmp_1084_fu_5759_p3() {
    tmp_1084_fu_5759_p3 = (!tmp_1346_reg_23520.read()[0].is_01())? sc_lv<12>(): ((tmp_1346_reg_23520.read()[0].to_bool())? tmp_1082_fu_5752_p1.read(): tmp_1083_fu_5756_p1.read());
}

void mnist_fp16_opt::thread_tmp_1086_fu_12390_p1() {
    tmp_1086_fu_12390_p1 = grp_fu_11578_p2.read().range(8-1, 0);
}

void mnist_fp16_opt::thread_tmp_1087_fu_6671_p2() {
    tmp_1087_fu_6671_p2 = (!p_shl136_cast_fu_6651_p3.read().is_01() || !p_shl137_cast_fu_6663_p3.read().is_01())? sc_lv<11>(): (sc_biguint<11>(p_shl136_cast_fu_6651_p3.read()) - sc_biguint<11>(p_shl137_cast_fu_6663_p3.read()));
}

void mnist_fp16_opt::thread_tmp_1088_cast_fu_6820_p1() {
    tmp_1088_cast_fu_6820_p1 = esl_sext<64,11>(tmp_1088_fu_6815_p2.read());
}

void mnist_fp16_opt::thread_tmp_1088_fu_6815_p2() {
    tmp_1088_fu_6815_p2 = (!ap_const_lv11_1A.is_01() || !tmp_1087_reg_23754.read().is_01())? sc_lv<11>(): (sc_biguint<11>(ap_const_lv11_1A) + sc_biguint<11>(tmp_1087_reg_23754.read()));
}

void mnist_fp16_opt::thread_tmp_1089_cast_fu_6830_p1() {
    tmp_1089_cast_fu_6830_p1 = esl_sext<64,11>(tmp_1089_fu_6825_p2.read());
}

void mnist_fp16_opt::thread_tmp_1089_fu_6825_p2() {
    tmp_1089_fu_6825_p2 = (!ap_const_lv11_1B.is_01() || !tmp_1087_reg_23754.read().is_01())? sc_lv<11>(): (sc_biguint<11>(ap_const_lv11_1B) + sc_biguint<11>(tmp_1087_reg_23754.read()));
}

void mnist_fp16_opt::thread_tmp_108_fu_9086_p2() {
    tmp_108_fu_9086_p2 = (!tmp_684_fu_9060_p1.read().is_01() || !ap_const_lv63_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_684_fu_9060_p1.read() == ap_const_lv63_0);
}

void mnist_fp16_opt::thread_tmp_1090_fu_17033_p3() {
    tmp_1090_fu_17033_p3 = esl_concat<4,3>(lhs_V_10_mid2_v_fu_17021_p3.read(), ap_const_lv3_0);
}

void mnist_fp16_opt::thread_tmp_1091_fu_17045_p2() {
    tmp_1091_fu_17045_p2 = (!lhs_V_10_mid2_cast_fu_17029_p1.read().is_01() || !p_shl138_cast_fu_17041_p1.read().is_01())? sc_lv<8>(): (sc_biguint<8>(lhs_V_10_mid2_cast_fu_17029_p1.read()) + sc_biguint<8>(p_shl138_cast_fu_17041_p1.read()));
}

void mnist_fp16_opt::thread_tmp_1092_cast_fu_17072_p1() {
    tmp_1092_cast_fu_17072_p1 = esl_zext<11,8>(tmp_1092_reg_26519.read());
}

void mnist_fp16_opt::thread_tmp_1092_fu_17055_p2() {
    tmp_1092_fu_17055_p2 = (!tmp_1091_fu_17045_p2.read().is_01() || !lhs_V_7_cast_fu_17051_p1.read().is_01())? sc_lv<8>(): (sc_biguint<8>(tmp_1091_fu_17045_p2.read()) + sc_biguint<8>(lhs_V_7_cast_fu_17051_p1.read()));
}

void mnist_fp16_opt::thread_tmp_1093_fu_11831_p1() {
    tmp_1093_fu_11831_p1 = mul21_fu_22930_p2.read().range(25-1, 0);
}

void mnist_fp16_opt::thread_tmp_1094_cast_fu_17088_p1() {
    tmp_1094_cast_fu_17088_p1 = esl_zext<64,11>(tmp_1094_fu_17082_p2.read());
}

void mnist_fp16_opt::thread_tmp_1094_fu_17082_p2() {
    tmp_1094_fu_17082_p2 = (!tmp_1092_cast_fu_17072_p1.read().is_01() || !p_shl139_cast_fu_17075_p3.read().is_01())? sc_lv<11>(): (sc_biguint<11>(tmp_1092_cast_fu_17072_p1.read()) + sc_biguint<11>(p_shl139_cast_fu_17075_p3.read()));
}

void mnist_fp16_opt::thread_tmp_1095_cast_fu_17768_p1() {
    tmp_1095_cast_fu_17768_p1 = esl_zext<64,11>(tmp_1095_fu_17763_p2.read());
}

void mnist_fp16_opt::thread_tmp_1095_fu_17763_p2() {
    tmp_1095_fu_17763_p2 = (!tmp_1094_reg_26531_pp17_iter3_reg.read().is_01() || !ap_const_lv11_1.is_01())? sc_lv<11>(): (sc_biguint<11>(tmp_1094_reg_26531_pp17_iter3_reg.read()) + sc_biguint<11>(ap_const_lv11_1));
}

void mnist_fp16_opt::thread_tmp_1096_cast_fu_17778_p1() {
    tmp_1096_cast_fu_17778_p1 = esl_zext<64,11>(tmp_1096_fu_17773_p2.read());
}

void mnist_fp16_opt::thread_tmp_1096_fu_17773_p2() {
    tmp_1096_fu_17773_p2 = (!tmp_1094_reg_26531_pp17_iter3_reg.read().is_01() || !ap_const_lv11_2.is_01())? sc_lv<11>(): (sc_biguint<11>(tmp_1094_reg_26531_pp17_iter3_reg.read()) + sc_biguint<11>(ap_const_lv11_2));
}

void mnist_fp16_opt::thread_tmp_1097_cast_fu_17864_p1() {
    tmp_1097_cast_fu_17864_p1 = esl_zext<64,11>(tmp_1097_fu_17859_p2.read());
}

void mnist_fp16_opt::thread_tmp_1097_fu_17859_p2() {
    tmp_1097_fu_17859_p2 = (!tmp_1094_reg_26531_pp17_iter3_reg.read().is_01() || !ap_const_lv11_3.is_01())? sc_lv<11>(): (sc_biguint<11>(tmp_1094_reg_26531_pp17_iter3_reg.read()) + sc_biguint<11>(ap_const_lv11_3));
}

void mnist_fp16_opt::thread_tmp_1098_cast_fu_17874_p1() {
    tmp_1098_cast_fu_17874_p1 = esl_zext<64,11>(tmp_1098_fu_17869_p2.read());
}

void mnist_fp16_opt::thread_tmp_1098_fu_17869_p2() {
    tmp_1098_fu_17869_p2 = (!tmp_1094_reg_26531_pp17_iter3_reg.read().is_01() || !ap_const_lv11_4.is_01())? sc_lv<11>(): (sc_biguint<11>(tmp_1094_reg_26531_pp17_iter3_reg.read()) + sc_biguint<11>(ap_const_lv11_4));
}

void mnist_fp16_opt::thread_tmp_1099_cast_fu_17966_p1() {
    tmp_1099_cast_fu_17966_p1 = esl_zext<64,11>(tmp_1099_fu_17961_p2.read());
}

void mnist_fp16_opt::thread_tmp_1099_fu_17961_p2() {
    tmp_1099_fu_17961_p2 = (!tmp_1094_reg_26531_pp17_iter3_reg.read().is_01() || !ap_const_lv11_5.is_01())? sc_lv<11>(): (sc_biguint<11>(tmp_1094_reg_26531_pp17_iter3_reg.read()) + sc_biguint<11>(ap_const_lv11_5));
}

void mnist_fp16_opt::thread_tmp_1100_cast_fu_17976_p1() {
    tmp_1100_cast_fu_17976_p1 = esl_zext<64,11>(tmp_1100_fu_17971_p2.read());
}

void mnist_fp16_opt::thread_tmp_1100_fu_17971_p2() {
    tmp_1100_fu_17971_p2 = (!tmp_1094_reg_26531_pp17_iter3_reg.read().is_01() || !ap_const_lv11_6.is_01())? sc_lv<11>(): (sc_biguint<11>(tmp_1094_reg_26531_pp17_iter3_reg.read()) + sc_biguint<11>(ap_const_lv11_6));
}

void mnist_fp16_opt::thread_tmp_1101_cast_fu_17986_p1() {
    tmp_1101_cast_fu_17986_p1 = esl_zext<64,11>(tmp_1101_fu_17981_p2.read());
}

void mnist_fp16_opt::thread_tmp_1101_fu_17981_p2() {
    tmp_1101_fu_17981_p2 = (!tmp_1094_reg_26531_pp17_iter3_reg.read().is_01() || !ap_const_lv11_7.is_01())? sc_lv<11>(): (sc_biguint<11>(tmp_1094_reg_26531_pp17_iter3_reg.read()) + sc_biguint<11>(ap_const_lv11_7));
}

void mnist_fp16_opt::thread_tmp_1102_cast_fu_17099_p1() {
    tmp_1102_cast_fu_17099_p1 = esl_zext<64,11>(tmp_1102_fu_17093_p2.read());
}

void mnist_fp16_opt::thread_tmp_1102_fu_17093_p2() {
    tmp_1102_fu_17093_p2 = (!tmp_1094_fu_17082_p2.read().is_01() || !ap_const_lv11_8.is_01())? sc_lv<11>(): (sc_biguint<11>(tmp_1094_fu_17082_p2.read()) + sc_biguint<11>(ap_const_lv11_8));
}

void mnist_fp16_opt::thread_tmp_1103_fu_16614_p2() {
    tmp_1103_fu_16614_p2 = (!tmp_1056_cast_reg_26343.read().is_01() || !tmp_640_cast_mid2_ca_fu_16610_p1.read().is_01())? sc_lv<10>(): (sc_bigint<10>(tmp_1056_cast_reg_26343.read()) + sc_biguint<10>(tmp_640_cast_mid2_ca_fu_16610_p1.read()));
}

void mnist_fp16_opt::thread_tmp_1104_fu_11860_p4() {
    tmp_1104_fu_11860_p4 = neg_mul21_fu_11855_p2.read().range(24, 20);
}

void mnist_fp16_opt::thread_tmp_1106_fu_16656_p2() {
    tmp_1106_fu_16656_p2 = (!p_shl140_cast_fu_16638_p3.read().is_01() || !p_shl141_cast_fu_16652_p1.read().is_01())? sc_lv<12>(): (sc_biguint<12>(p_shl140_cast_fu_16638_p3.read()) - sc_bigint<12>(p_shl141_cast_fu_16652_p1.read()));
}

void mnist_fp16_opt::thread_tmp_1107_cast_fu_16671_p1() {
    tmp_1107_cast_fu_16671_p1 = esl_zext<64,12>(tmp_1107_fu_16665_p2.read());
}

void mnist_fp16_opt::thread_tmp_1107_fu_16665_p2() {
    tmp_1107_fu_16665_p2 = (!tmp_1106_fu_16656_p2.read().is_01() || !tmp_650_cast_cast_fu_16662_p1.read().is_01())? sc_lv<12>(): (sc_biguint<12>(tmp_1106_fu_16656_p2.read()) + sc_biguint<12>(tmp_650_cast_cast_fu_16662_p1.read()));
}

void mnist_fp16_opt::thread_tmp_1108_fu_17178_p1() {
    tmp_1108_fu_17178_p1 = esl_sext<10,8>(tmp_1366_fu_17168_p4.read());
}

void mnist_fp16_opt::thread_tmp_1109_fu_17182_p1() {
    tmp_1109_fu_17182_p1 = esl_sext<10,9>(tmp_1367_reg_26578.read());
}

void mnist_fp16_opt::thread_tmp_1110_fu_17185_p3() {
    tmp_1110_fu_17185_p3 = (!tmp_1365_reg_26552.read()[0].is_01())? sc_lv<10>(): ((tmp_1365_reg_26552.read()[0].to_bool())? tmp_1108_fu_17178_p1.read(): tmp_1109_fu_17182_p1.read());
}

void mnist_fp16_opt::thread_tmp_1111_fu_17428_p1() {
    tmp_1111_fu_17428_p1 = esl_sext<11,5>(tmp_1370_fu_17418_p4.read());
}

void mnist_fp16_opt::thread_tmp_1112_fu_17432_p1() {
    tmp_1112_fu_17432_p1 = esl_sext<11,6>(tmp_1371_reg_26674.read());
}

void mnist_fp16_opt::thread_tmp_1113_fu_11884_p1() {
    tmp_1113_fu_11884_p1 = p_v5_fu_11877_p3.read().range(2-1, 0);
}

void mnist_fp16_opt::thread_tmp_1114_fu_11894_p1() {
    tmp_1114_fu_11894_p1 = p_v5_fu_11877_p3.read().range(2-1, 0);
}

void mnist_fp16_opt::thread_tmp_1115_fu_12432_p1() {
    tmp_1115_fu_12432_p1 = tmp_957_fu_12426_p2.read().range(7-1, 0);
}

void mnist_fp16_opt::thread_tmp_1116_fu_17456_p3() {
    tmp_1116_fu_17456_p3 = (!tmp_1365_reg_26552_pp17_iter2_reg.read()[0].is_01())? sc_lv<3>(): ((tmp_1365_reg_26552_pp17_iter2_reg.read()[0].to_bool())? neg_ti33_fu_17446_p2.read(): tmp_1373_fu_17452_p1.read());
}

void mnist_fp16_opt::thread_tmp_1117_fu_17544_p3() {
    tmp_1117_fu_17544_p3 = esl_concat<3,3>(tmp_1116_reg_26689.read(), ap_const_lv3_0);
}

void mnist_fp16_opt::thread_tmp_1118_cast_fu_17561_p1() {
    tmp_1118_cast_fu_17561_p1 = esl_sext<8,7>(tmp_1118_fu_17555_p2.read());
}

void mnist_fp16_opt::thread_tmp_1118_fu_17555_p2() {
    tmp_1118_fu_17555_p2 = (!p_shl142_cast_fu_17551_p1.read().is_01() || !tmp_294_1_cast_fu_17541_p1.read().is_01())? sc_lv<7>(): (sc_biguint<7>(p_shl142_cast_fu_17551_p1.read()) - sc_biguint<7>(tmp_294_1_cast_fu_17541_p1.read()));
}

void mnist_fp16_opt::thread_tmp_1119_cast_fu_17571_p1() {
    tmp_1119_cast_fu_17571_p1 = esl_sext<10,8>(tmp_1119_fu_17565_p2.read());
}

void mnist_fp16_opt::thread_tmp_1119_fu_17565_p2() {
    tmp_1119_fu_17565_p2 = (!tmp_1118_cast_fu_17561_p1.read().is_01() || !tmp_1368_fu_17537_p1.read().is_01())? sc_lv<8>(): (sc_bigint<8>(tmp_1118_cast_fu_17561_p1.read()) + sc_biguint<8>(tmp_1368_fu_17537_p1.read()));
}

void mnist_fp16_opt::thread_tmp_111_cast_fu_6542_p1() {
    tmp_111_cast_fu_6542_p1 = esl_sext<64,11>(tmp_64_fu_6537_p2.read());
}

void mnist_fp16_opt::thread_tmp_111_fu_6689_p1() {
    tmp_111_fu_6689_p1 = grp_fu_5827_p2.read().range(9-1, 0);
}

void mnist_fp16_opt::thread_tmp_1120_fu_12444_p3() {
    tmp_1120_fu_12444_p3 = esl_concat<8,1>(tmp_957_fu_12426_p2.read(), ap_const_lv1_0);
}

void mnist_fp16_opt::thread_tmp_1121_cast_fu_17693_p1() {
    tmp_1121_cast_fu_17693_p1 = esl_zext<64,10>(tmp_1121_reg_26721.read());
}

void mnist_fp16_opt::thread_tmp_1121_fu_17587_p2() {
    tmp_1121_fu_17587_p2 = (!p_shl143_cast_fu_17579_p3.read().is_01() || !tmp_1119_cast_fu_17571_p1.read().is_01())? sc_lv<10>(): (sc_biguint<10>(p_shl143_cast_fu_17579_p3.read()) - sc_bigint<10>(tmp_1119_cast_fu_17571_p1.read()));
}

void mnist_fp16_opt::thread_tmp_1122_cast_fu_17702_p1() {
    tmp_1122_cast_fu_17702_p1 = esl_zext<64,10>(tmp_1122_fu_17697_p2.read());
}

void mnist_fp16_opt::thread_tmp_1122_fu_17697_p2() {
    tmp_1122_fu_17697_p2 = (!ap_const_lv10_1.is_01() || !tmp_1121_reg_26721.read().is_01())? sc_lv<10>(): (sc_biguint<10>(ap_const_lv10_1) + sc_biguint<10>(tmp_1121_reg_26721.read()));
}

void mnist_fp16_opt::thread_tmp_1123_fu_18039_p3() {
    tmp_1123_fu_18039_p3 = esl_concat<5,3>(tmp_252_mid2_v_fu_18027_p3.read(), ap_const_lv3_0);
}

void mnist_fp16_opt::thread_tmp_1124_cast_fu_18047_p1() {
    tmp_1124_cast_fu_18047_p1 = esl_zext<9,8>(tmp_1123_fu_18039_p3.read());
}

void mnist_fp16_opt::thread_tmp_1124_fu_5517_p1() {
    tmp_1124_fu_5517_p1 = mul22_fu_22772_p2.read().range(25-1, 0);
}

void mnist_fp16_opt::thread_tmp_1126_cast_fu_18057_p1() {
    tmp_1126_cast_fu_18057_p1 = esl_sext<10,9>(tmp_1126_fu_18051_p2.read());
}

void mnist_fp16_opt::thread_tmp_1126_fu_18051_p2() {
    tmp_1126_fu_18051_p2 = (!tmp_1124_cast_fu_18047_p1.read().is_01() || !tmp_252_mid2_cast_fu_18035_p1.read().is_01())? sc_lv<9>(): (sc_biguint<9>(tmp_1124_cast_fu_18047_p1.read()) - sc_biguint<9>(tmp_252_mid2_cast_fu_18035_p1.read()));
}

void mnist_fp16_opt::thread_tmp_1127_fu_18085_p2() {
    tmp_1127_fu_18085_p2 = (exitcond54_mid_fu_18073_p2.read() | exitcond_flatten36_fu_18013_p2.read());
}

void mnist_fp16_opt::thread_tmp_1128_cast_fu_18121_p1() {
    tmp_1128_cast_fu_18121_p1 = esl_sext<11,10>(tmp_1128_fu_18115_p2.read());
}

void mnist_fp16_opt::thread_tmp_1128_fu_18115_p2() {
    tmp_1128_fu_18115_p2 = (!tmp_273_mid2_cast_fu_18111_p1.read().is_01() || !tmp_1126_cast_fu_18057_p1.read().is_01())? sc_lv<10>(): (sc_biguint<10>(tmp_273_mid2_cast_fu_18111_p1.read()) + sc_bigint<10>(tmp_1126_cast_fu_18057_p1.read()));
}

void mnist_fp16_opt::thread_tmp_1129_fu_5534_p4() {
    tmp_1129_fu_5534_p4 = neg_mul22_fu_5529_p2.read().range(24, 17);
}

void mnist_fp16_opt::thread_tmp_112_fu_9802_p2() {
    tmp_112_fu_9802_p2 = (!p_Result_s_48_fu_9792_p4.read().is_01() || !ap_const_lv8_9E.is_01())? sc_lv<1>(): sc_lv<1>(p_Result_s_48_fu_9792_p4.read() != ap_const_lv8_9E);
}

void mnist_fp16_opt::thread_tmp_1130_fu_18137_p2() {
    tmp_1130_fu_18137_p2 = (!p_shl145_cast_fu_18129_p3.read().is_01() || !tmp_1128_cast_fu_18121_p1.read().is_01())? sc_lv<11>(): (sc_biguint<11>(p_shl145_cast_fu_18129_p3.read()) - sc_bigint<11>(tmp_1128_cast_fu_18121_p1.read()));
}

void mnist_fp16_opt::thread_tmp_1131_fu_17226_p1() {
    tmp_1131_fu_17226_p1 = esl_sext<10,8>(tmp_1378_fu_17216_p4.read());
}

void mnist_fp16_opt::thread_tmp_1132_fu_17230_p1() {
    tmp_1132_fu_17230_p1 = esl_sext<10,9>(tmp_1379_reg_26593.read());
}

void mnist_fp16_opt::thread_tmp_1133_fu_17233_p3() {
    tmp_1133_fu_17233_p3 = (!tmp_1377_reg_26560.read()[0].is_01())? sc_lv<10>(): ((tmp_1377_reg_26560.read()[0].to_bool())? tmp_1131_fu_17226_p1.read(): tmp_1132_fu_17230_p1.read());
}

void mnist_fp16_opt::thread_tmp_1134_fu_17608_p1() {
    tmp_1134_fu_17608_p1 = esl_sext<11,5>(tmp_1382_fu_17598_p4.read());
}

void mnist_fp16_opt::thread_tmp_1135_fu_17612_p1() {
    tmp_1135_fu_17612_p1 = esl_sext<11,6>(tmp_1383_reg_26700.read());
}

void mnist_fp16_opt::thread_tmp_1137_fu_6437_p1() {
    tmp_1137_fu_6437_p1 = grp_fu_5571_p2.read().range(6-1, 0);
}

void mnist_fp16_opt::thread_tmp_1138_fu_6449_p1() {
    tmp_1138_fu_6449_p1 = grp_fu_5571_p2.read().range(9-1, 0);
}

void mnist_fp16_opt::thread_tmp_1139_fu_17636_p3() {
    tmp_1139_fu_17636_p3 = (!tmp_1377_reg_26560_pp17_iter2_reg.read()[0].is_01())? sc_lv<3>(): ((tmp_1377_reg_26560_pp17_iter2_reg.read()[0].to_bool())? neg_ti35_fu_17626_p2.read(): tmp_1385_fu_17632_p1.read());
}

void mnist_fp16_opt::thread_tmp_113_fu_6701_p2() {
    tmp_113_fu_6701_p2 = (!p_shl12_cast_fu_6681_p3.read().is_01() || !p_shl13_cast_fu_6693_p3.read().is_01())? sc_lv<11>(): (sc_biguint<11>(p_shl12_cast_fu_6681_p3.read()) - sc_biguint<11>(p_shl13_cast_fu_6693_p3.read()));
}

void mnist_fp16_opt::thread_tmp_1140_fu_17714_p3() {
    tmp_1140_fu_17714_p3 = esl_concat<3,3>(tmp_1139_reg_26727.read(), ap_const_lv3_0);
}

void mnist_fp16_opt::thread_tmp_1141_cast_fu_17731_p1() {
    tmp_1141_cast_fu_17731_p1 = esl_sext<8,7>(tmp_1141_fu_17725_p2.read());
}

void mnist_fp16_opt::thread_tmp_1141_fu_17725_p2() {
    tmp_1141_fu_17725_p2 = (!p_shl146_cast_fu_17721_p1.read().is_01() || !tmp_294_3_cast_fu_17711_p1.read().is_01())? sc_lv<7>(): (sc_biguint<7>(p_shl146_cast_fu_17721_p1.read()) - sc_biguint<7>(tmp_294_3_cast_fu_17711_p1.read()));
}

void mnist_fp16_opt::thread_tmp_1142_cast_fu_17741_p1() {
    tmp_1142_cast_fu_17741_p1 = esl_sext<10,8>(tmp_1142_fu_17735_p2.read());
}

void mnist_fp16_opt::thread_tmp_1142_fu_17735_p2() {
    tmp_1142_fu_17735_p2 = (!tmp_1141_cast_fu_17731_p1.read().is_01() || !tmp_1380_fu_17707_p1.read().is_01())? sc_lv<8>(): (sc_bigint<8>(tmp_1141_cast_fu_17731_p1.read()) + sc_biguint<8>(tmp_1380_fu_17707_p1.read()));
}

void mnist_fp16_opt::thread_tmp_1143_fu_14761_p1() {
    tmp_1143_fu_14761_p1 = tmp_977_fu_14755_p2.read().range(8-1, 0);
}

void mnist_fp16_opt::thread_tmp_1144_fu_17757_p2() {
    tmp_1144_fu_17757_p2 = (!p_shl147_cast_fu_17749_p3.read().is_01() || !tmp_1142_cast_fu_17741_p1.read().is_01())? sc_lv<10>(): (sc_biguint<10>(p_shl147_cast_fu_17749_p3.read()) - sc_bigint<10>(tmp_1142_cast_fu_17741_p1.read()));
}

void mnist_fp16_opt::thread_tmp_1145_cast_fu_17788_p1() {
    tmp_1145_cast_fu_17788_p1 = esl_zext<64,10>(tmp_1145_fu_17783_p2.read());
}

void mnist_fp16_opt::thread_tmp_1145_fu_17783_p2() {
    tmp_1145_fu_17783_p2 = (!ap_const_lv10_2.is_01() || !tmp_1144_reg_26749.read().is_01())? sc_lv<10>(): (sc_biguint<10>(ap_const_lv10_2) + sc_biguint<10>(tmp_1144_reg_26749.read()));
}

void mnist_fp16_opt::thread_tmp_1146_cast_fu_17798_p1() {
    tmp_1146_cast_fu_17798_p1 = esl_zext<64,10>(tmp_1146_fu_17793_p2.read());
}

void mnist_fp16_opt::thread_tmp_1146_fu_17793_p2() {
    tmp_1146_fu_17793_p2 = (!ap_const_lv10_3.is_01() || !tmp_1144_reg_26749.read().is_01())? sc_lv<10>(): (sc_biguint<10>(ap_const_lv10_3) + sc_biguint<10>(tmp_1144_reg_26749.read()));
}

void mnist_fp16_opt::thread_tmp_1147_fu_18501_p3() {
    tmp_1147_fu_18501_p3 = esl_concat<5,3>(tmp_312_mid2_v_fu_18489_p3.read(), ap_const_lv3_0);
}

void mnist_fp16_opt::thread_tmp_1148_cast_fu_18519_p1() {
    tmp_1148_cast_fu_18519_p1 = esl_sext<10,9>(tmp_1148_fu_18513_p2.read());
}

void mnist_fp16_opt::thread_tmp_1148_fu_18513_p2() {
    tmp_1148_fu_18513_p2 = (!p_shl148_cast_fu_18509_p1.read().is_01() || !tmp_312_mid2_cast_fu_18497_p1.read().is_01())? sc_lv<9>(): (sc_biguint<9>(p_shl148_cast_fu_18509_p1.read()) - sc_biguint<9>(tmp_312_mid2_cast_fu_18497_p1.read()));
}

void mnist_fp16_opt::thread_tmp_1149_fu_18547_p2() {
    tmp_1149_fu_18547_p2 = (exitcond77_mid_fu_18535_p2.read() | exitcond_flatten39_fu_18475_p2.read());
}

void mnist_fp16_opt::thread_tmp_114_fu_7534_p2() {
    tmp_114_fu_7534_p2 = (!ap_const_lv12_FF2.is_01() || !F2_fu_7522_p2.read().is_01())? sc_lv<12>(): (sc_bigint<12>(ap_const_lv12_FF2) + sc_biguint<12>(F2_fu_7522_p2.read()));
}

void mnist_fp16_opt::thread_tmp_1150_cast_fu_18603_p1() {
    tmp_1150_cast_fu_18603_p1 = esl_sext<11,10>(tmp_1150_reg_26968.read());
}

void mnist_fp16_opt::thread_tmp_1150_fu_18573_p2() {
    tmp_1150_fu_18573_p2 = (!tmp_1148_cast_fu_18519_p1.read().is_01() || !tmp_589_mid2_cast_fu_18569_p1.read().is_01())? sc_lv<10>(): (sc_bigint<10>(tmp_1148_cast_fu_18519_p1.read()) + sc_biguint<10>(tmp_589_mid2_cast_fu_18569_p1.read()));
}

void mnist_fp16_opt::thread_tmp_1151_fu_14773_p3() {
    tmp_1151_fu_14773_p3 = esl_concat<10,1>(tmp_977_fu_14755_p2.read(), ap_const_lv1_0);
}

void mnist_fp16_opt::thread_tmp_1152_fu_18613_p2() {
    tmp_1152_fu_18613_p2 = (!p_shl149_cast_fu_18606_p3.read().is_01() || !tmp_1150_cast_fu_18603_p1.read().is_01())? sc_lv<11>(): (sc_biguint<11>(p_shl149_cast_fu_18606_p3.read()) - sc_bigint<11>(tmp_1150_cast_fu_18603_p1.read()));
}

void mnist_fp16_opt::thread_tmp_1153_cast_fu_18628_p1() {
    tmp_1153_cast_fu_18628_p1 = esl_zext<64,11>(tmp_1153_fu_18622_p2.read());
}

void mnist_fp16_opt::thread_tmp_1153_fu_18622_p2() {
    tmp_1153_fu_18622_p2 = (!tmp_1152_fu_18613_p2.read().is_01() || !tmp_602_cast_fu_18619_p1.read().is_01())? sc_lv<11>(): (sc_biguint<11>(tmp_1152_fu_18613_p2.read()) + sc_biguint<11>(tmp_602_cast_fu_18619_p1.read()));
}

void mnist_fp16_opt::thread_tmp_1154_cast_fu_18434_p1() {
    tmp_1154_cast_fu_18434_p1 = esl_zext<64,11>(tmp_1154_fu_18429_p2.read());
}

void mnist_fp16_opt::thread_tmp_1154_fu_18429_p2() {
    tmp_1154_fu_18429_p2 = (!tmp_1130_reg_26843.read().is_01() || !tmp_329_cast_fu_18426_p1.read().is_01())? sc_lv<11>(): (sc_biguint<11>(tmp_1130_reg_26843.read()) + sc_biguint<11>(tmp_329_cast_fu_18426_p1.read()));
}

void mnist_fp16_opt::thread_tmp_1155_cast_fu_18292_p1() {
    tmp_1155_cast_fu_18292_p1 = esl_zext<64,9>(tmp_1155_reg_26869.read());
}

void mnist_fp16_opt::thread_tmp_1155_fu_18200_p2() {
    tmp_1155_fu_18200_p2 = (!tmp_330_mid2_cast_fu_18196_p1.read().is_01() || !tmp_1124_cast_reg_26820.read().is_01())? sc_lv<9>(): (sc_biguint<9>(tmp_330_mid2_cast_fu_18196_p1.read()) + sc_biguint<9>(tmp_1124_cast_reg_26820.read()));
}

void mnist_fp16_opt::thread_tmp_1156_fu_18306_p2() {
    tmp_1156_fu_18306_p2 = (!p_shl5_fu_18302_p1.read().is_01() || !tmp_1155_cast_fu_18292_p1.read().is_01())? sc_lv<64>(): (sc_biguint<64>(p_shl5_fu_18302_p1.read()) - sc_biguint<64>(tmp_1155_cast_fu_18292_p1.read()));
}

void mnist_fp16_opt::thread_tmp_1157_fu_18312_p3() {
    tmp_1157_fu_18312_p3 = esl_concat<4,3>(tmp_330_mid2_v_reg_26862.read(), ap_const_lv3_0);
}

void mnist_fp16_opt::thread_tmp_1158_fu_18323_p2() {
    tmp_1158_fu_18323_p2 = (!p_shl151_cast_fu_18319_p1.read().is_01() || !tmp_330_mid2_cast1_fu_18289_p1.read().is_01())? sc_lv<8>(): (sc_biguint<8>(p_shl151_cast_fu_18319_p1.read()) + sc_biguint<8>(tmp_330_mid2_cast1_fu_18289_p1.read()));
}

void mnist_fp16_opt::thread_tmp_1159_fu_18229_p2() {
    tmp_1159_fu_18229_p2 = (exitcond67_mid_fu_18217_p2.read() | exitcond_flatten40_fu_18174_p2.read());
}

void mnist_fp16_opt::thread_tmp_115_fu_7540_p2() {
    tmp_115_fu_7540_p2 = (!ap_const_lv12_E.is_01() || !F2_fu_7522_p2.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_E) - sc_biguint<12>(F2_fu_7522_p2.read()));
}

void mnist_fp16_opt::thread_tmp_1160_fu_18332_p2() {
    tmp_1160_fu_18332_p2 = (!tmp_364_mid2_cast_fu_18329_p1.read().is_01() || !tmp_1158_fu_18323_p2.read().is_01())? sc_lv<8>(): (sc_biguint<8>(tmp_364_mid2_cast_fu_18329_p1.read()) + sc_biguint<8>(tmp_1158_fu_18323_p2.read()));
}

void mnist_fp16_opt::thread_tmp_1161_cast_fu_18378_p1() {
    tmp_1161_cast_fu_18378_p1 = esl_zext<11,8>(tmp_1160_reg_26898.read());
}

void mnist_fp16_opt::thread_tmp_1161_fu_18388_p2() {
    tmp_1161_fu_18388_p2 = (!p_shl152_cast_fu_18381_p3.read().is_01() || !tmp_1161_cast_fu_18378_p1.read().is_01())? sc_lv<11>(): (sc_biguint<11>(p_shl152_cast_fu_18381_p3.read()) + sc_biguint<11>(tmp_1161_cast_fu_18378_p1.read()));
}

void mnist_fp16_opt::thread_tmp_1162_fu_14472_p1() {
    tmp_1162_fu_14472_p1 = msb_idx_6_fu_14466_p2.read().range(31-1, 0);
}

void mnist_fp16_opt::thread_tmp_1163_fu_18341_p2() {
    tmp_1163_fu_18341_p2 = (!tmp_365_mid2_cast_fu_18338_p1.read().is_01() || !tmp_1156_fu_18306_p2.read().is_01())? sc_lv<64>(): (sc_biguint<64>(tmp_365_mid2_cast_fu_18338_p1.read()) + sc_biguint<64>(tmp_1156_fu_18306_p2.read()));
}

void mnist_fp16_opt::thread_tmp_1164_fu_18363_p2() {
    tmp_1164_fu_18363_p2 = (!p_shl153_cast_fu_18355_p3.read().is_01() || !tmp_1399_fu_18347_p1.read().is_01())? sc_lv<12>(): (sc_biguint<12>(p_shl153_cast_fu_18355_p3.read()) - sc_biguint<12>(tmp_1399_fu_18347_p1.read()));
}

void mnist_fp16_opt::thread_tmp_1166_fu_18406_p2() {
    tmp_1166_fu_18406_p2 = (!tmp_391_cast_fu_18402_p1.read().is_01() || !tmp_1161_fu_18388_p2.read().is_01())? sc_lv<11>(): (sc_biguint<11>(tmp_391_cast_fu_18402_p1.read()) + sc_biguint<11>(tmp_1161_fu_18388_p2.read()));
}

void mnist_fp16_opt::thread_tmp_1167_cast_fu_18412_p1() {
    tmp_1167_cast_fu_18412_p1 = esl_zext<64,11>(tmp_1166_fu_18406_p2.read());
}

void mnist_fp16_opt::thread_tmp_1167_fu_18372_p2() {
    tmp_1167_fu_18372_p2 = (!tmp_392_cast_fu_18369_p1.read().is_01() || !tmp_1164_fu_18363_p2.read().is_01())? sc_lv<12>(): (sc_biguint<12>(tmp_392_cast_fu_18369_p1.read()) + sc_biguint<12>(tmp_1164_fu_18363_p2.read()));
}

void mnist_fp16_opt::thread_tmp_1168_cast_fu_18417_p1() {
    tmp_1168_cast_fu_18417_p1 = esl_zext<64,12>(tmp_1167_reg_26904.read());
}

void mnist_fp16_opt::thread_tmp_1168_fu_17305_p1() {
    tmp_1168_fu_17305_p1 = esl_sext<10,8>(tmp_1403_fu_17295_p4.read());
}

void mnist_fp16_opt::thread_tmp_1169_fu_17309_p1() {
    tmp_1169_fu_17309_p1 = esl_sext<10,9>(tmp_1404_reg_26634.read());
}

void mnist_fp16_opt::thread_tmp_116_fu_9824_p3() {
    tmp_116_fu_9824_p3 = esl_concat<1,8>(is_neg_1_reg_24533.read(), p_Repl2_4_trunc_fu_9818_p2.read());
}

void mnist_fp16_opt::thread_tmp_1170_fu_17312_p3() {
    tmp_1170_fu_17312_p3 = (!tmp_1402_reg_26598.read()[0].is_01())? sc_lv<10>(): ((tmp_1402_reg_26598.read()[0].to_bool())? tmp_1168_fu_17305_p1.read(): tmp_1169_fu_17309_p1.read());
}

void mnist_fp16_opt::thread_tmp_1171_fu_17658_p1() {
    tmp_1171_fu_17658_p1 = esl_sext<11,5>(tmp_1407_fu_17648_p4.read());
}

void mnist_fp16_opt::thread_tmp_1172_fu_17662_p1() {
    tmp_1172_fu_17662_p1 = esl_sext<11,6>(tmp_1408_reg_26710.read());
}

void mnist_fp16_opt::thread_tmp_1173_fu_17686_p3() {
    tmp_1173_fu_17686_p3 = (!tmp_1402_reg_26598_pp17_iter2_reg.read()[0].is_01())? sc_lv<3>(): ((tmp_1402_reg_26598_pp17_iter2_reg.read()[0].to_bool())? neg_ti37_fu_17676_p2.read(): tmp_1410_fu_17682_p1.read());
}

void mnist_fp16_opt::thread_tmp_1174_fu_14494_p4() {
    tmp_1174_fu_14494_p4 = msb_idx_7_fu_14484_p3.read().range(30, 5);
}

void mnist_fp16_opt::thread_tmp_1175_fu_14525_p1() {
    tmp_1175_fu_14525_p1 = msb_idx_7_fu_14484_p3.read().range(4-1, 0);
}

void mnist_fp16_opt::thread_tmp_1176_fu_14529_p2() {
    tmp_1176_fu_14529_p2 = (!ap_const_lv4_1.is_01() || !tmp_1175_fu_14525_p1.read().is_01())? sc_lv<4>(): (sc_biguint<4>(ap_const_lv4_1) + sc_biguint<4>(tmp_1175_fu_14525_p1.read()));
}

void mnist_fp16_opt::thread_tmp_1177_fu_17810_p3() {
    tmp_1177_fu_17810_p3 = esl_concat<3,3>(tmp_1173_reg_26733.read(), ap_const_lv3_0);
}

void mnist_fp16_opt::thread_tmp_1178_fu_17821_p2() {
    tmp_1178_fu_17821_p2 = (!p_shl154_cast_fu_17817_p1.read().is_01() || !tmp_294_5_cast_fu_17807_p1.read().is_01())? sc_lv<7>(): (sc_biguint<7>(p_shl154_cast_fu_17817_p1.read()) - sc_biguint<7>(tmp_294_5_cast_fu_17807_p1.read()));
}

void mnist_fp16_opt::thread_tmp_1179_cast_fu_17827_p1() {
    tmp_1179_cast_fu_17827_p1 = esl_sext<8,7>(tmp_1178_fu_17821_p2.read());
}

void mnist_fp16_opt::thread_tmp_1179_fu_17831_p2() {
    tmp_1179_fu_17831_p2 = (!tmp_1179_cast_fu_17827_p1.read().is_01() || !tmp_1405_fu_17803_p1.read().is_01())? sc_lv<8>(): (sc_bigint<8>(tmp_1179_cast_fu_17827_p1.read()) + sc_biguint<8>(tmp_1405_fu_17803_p1.read()));
}

void mnist_fp16_opt::thread_tmp_1180_cast_fu_17837_p1() {
    tmp_1180_cast_fu_17837_p1 = esl_sext<10,8>(tmp_1179_fu_17831_p2.read());
}

void mnist_fp16_opt::thread_tmp_1180_fu_17853_p2() {
    tmp_1180_fu_17853_p2 = (!p_shl155_cast_fu_17845_p3.read().is_01() || !tmp_1180_cast_fu_17837_p1.read().is_01())? sc_lv<10>(): (sc_biguint<10>(p_shl155_cast_fu_17845_p3.read()) - sc_bigint<10>(tmp_1180_cast_fu_17837_p1.read()));
}

void mnist_fp16_opt::thread_tmp_1181_fu_14535_p1() {
    tmp_1181_fu_14535_p1 = esl_zext<16,4>(tmp_1176_fu_14529_p2.read());
}

void mnist_fp16_opt::thread_tmp_1182_fu_17879_p2() {
    tmp_1182_fu_17879_p2 = (!ap_const_lv10_4.is_01() || !tmp_1180_reg_26765.read().is_01())? sc_lv<10>(): (sc_biguint<10>(ap_const_lv10_4) + sc_biguint<10>(tmp_1180_reg_26765.read()));
}

void mnist_fp16_opt::thread_tmp_1183_cast_fu_17884_p1() {
    tmp_1183_cast_fu_17884_p1 = esl_zext<64,10>(tmp_1182_fu_17879_p2.read());
}

void mnist_fp16_opt::thread_tmp_1183_fu_17889_p2() {
    tmp_1183_fu_17889_p2 = (!ap_const_lv10_5.is_01() || !tmp_1180_reg_26765.read().is_01())? sc_lv<10>(): (sc_biguint<10>(ap_const_lv10_5) + sc_biguint<10>(tmp_1180_reg_26765.read()));
}

void mnist_fp16_opt::thread_tmp_1184_cast_fu_17894_p1() {
    tmp_1184_cast_fu_17894_p1 = esl_zext<64,10>(tmp_1183_fu_17889_p2.read());
}

void mnist_fp16_opt::thread_tmp_1184_fu_19205_p3() {
    tmp_1184_fu_19205_p3 = esl_concat<5,3>(lhs_V_14_mid2_v_reg_27085.read(), ap_const_lv3_0);
}

void mnist_fp16_opt::thread_tmp_1185_fu_19216_p2() {
    tmp_1185_fu_19216_p2 = (!lhs_V_14_mid2_cast_fu_19202_p1.read().is_01() || !p_shl156_cast_fu_19212_p1.read().is_01())? sc_lv<9>(): (sc_biguint<9>(lhs_V_14_mid2_cast_fu_19202_p1.read()) + sc_biguint<9>(p_shl156_cast_fu_19212_p1.read()));
}

void mnist_fp16_opt::thread_tmp_1186_fu_19094_p2() {
    tmp_1186_fu_19094_p2 = (exitcond63_mid_fu_19082_p2.read() | exitcond_flatten42_fu_19016_p2.read());
}

void mnist_fp16_opt::thread_tmp_1187_fu_19225_p2() {
    tmp_1187_fu_19225_p2 = (!tmp_1185_fu_19216_p2.read().is_01() || !lhs_V_15_mid2_cast_fu_19222_p1.read().is_01())? sc_lv<9>(): (sc_biguint<9>(tmp_1185_fu_19216_p2.read()) + sc_biguint<9>(lhs_V_15_mid2_cast_fu_19222_p1.read()));
}

void mnist_fp16_opt::thread_tmp_1188_cast_fu_19231_p1() {
    tmp_1188_cast_fu_19231_p1 = esl_zext<12,9>(tmp_1187_fu_19225_p2.read());
}

void mnist_fp16_opt::thread_tmp_1188_fu_19243_p2() {
    tmp_1188_fu_19243_p2 = (!tmp_1188_cast_fu_19231_p1.read().is_01() || !p_shl157_cast_fu_19235_p3.read().is_01())? sc_lv<12>(): (sc_biguint<12>(tmp_1188_cast_fu_19231_p1.read()) + sc_biguint<12>(p_shl157_cast_fu_19235_p3.read()));
}

void mnist_fp16_opt::thread_tmp_1189_fu_19258_p2() {
    tmp_1189_fu_19258_p2 = (!tmp_1412_fu_19249_p1.read().is_01() || !ap_const_lv3_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_1412_fu_19249_p1.read() != ap_const_lv3_0);
}

void mnist_fp16_opt::thread_tmp_1190_fu_19359_p1() {
    tmp_1190_fu_19359_p1 = esl_sext<11,9>(tmp_1415_fu_19349_p4.read());
}

void mnist_fp16_opt::thread_tmp_1191_fu_19363_p1() {
    tmp_1191_fu_19363_p1 = esl_sext<11,10>(tmp_1416_reg_27152.read());
}

void mnist_fp16_opt::thread_tmp_1192_fu_19366_p3() {
    tmp_1192_fu_19366_p3 = (!tmp_1414_reg_27134_pp20_iter2_reg.read()[0].is_01())? sc_lv<11>(): ((tmp_1414_reg_27134_pp20_iter2_reg.read()[0].to_bool())? tmp_1190_fu_19359_p1.read(): tmp_1191_fu_19363_p1.read());
}

void mnist_fp16_opt::thread_tmp_1193_fu_19407_p1() {
    tmp_1193_fu_19407_p1 = esl_sext<12,6>(tmp_1419_fu_19397_p4.read());
}

void mnist_fp16_opt::thread_tmp_1194_fu_19411_p1() {
    tmp_1194_fu_19411_p1 = esl_sext<12,7>(tmp_1420_reg_27162.read());
}

void mnist_fp16_opt::thread_tmp_1195_fu_19449_p3() {
    tmp_1195_fu_19449_p3 = esl_concat<4,3>(r_V_21_reg_27172_pp20_iter16_reg.read(), ap_const_lv3_0);
}

void mnist_fp16_opt::thread_tmp_1196_fu_19460_p2() {
    tmp_1196_fu_19460_p2 = (!p_shl158_cast_fu_19456_p1.read().is_01() || !tmp_375_cast_fu_19446_p1.read().is_01())? sc_lv<8>(): (sc_biguint<8>(p_shl158_cast_fu_19456_p1.read()) - sc_biguint<8>(tmp_375_cast_fu_19446_p1.read()));
}

void mnist_fp16_opt::thread_tmp_1197_fu_14576_p1() {
    tmp_1197_fu_14576_p1 = msb_idx_6_reg_25841_pp13_iter28_reg.read().range(8-1, 0);
}

void mnist_fp16_opt::thread_tmp_1198_fu_5082_p2() {
    tmp_1198_fu_5082_p2 = (!p_s_reg_2273.read().is_01() || !ap_const_lv5_1E.is_01())? sc_lv<1>(): sc_lv<1>(p_s_reg_2273.read() == ap_const_lv5_1E);
}

void mnist_fp16_opt::thread_tmp_1199_fu_5088_p2() {
    tmp_1199_fu_5088_p2 = (tmp_1198_fu_5082_p2.read() | tmp_6_reg_23135.read());
}

void mnist_fp16_opt::thread_tmp_119_cast_fu_6552_p1() {
    tmp_119_cast_fu_6552_p1 = esl_sext<64,11>(tmp_65_fu_6547_p2.read());
}

void mnist_fp16_opt::thread_tmp_119_fu_6712_p2() {
    tmp_119_fu_6712_p2 = (tmp_113_fu_6701_p2.read() | ap_const_lv11_1);
}

void mnist_fp16_opt::thread_tmp_11_fu_4648_p2() {
    tmp_11_fu_4648_p2 = (!ap_phi_mux_p_s_phi_fu_2277_p4.read().is_01() || !ap_const_lv5_1D.is_01())? sc_lv<1>(): sc_lv<1>(ap_phi_mux_p_s_phi_fu_2277_p4.read() == ap_const_lv5_1D);
}

void mnist_fp16_opt::thread_tmp_1200_fu_5093_p2() {
    tmp_1200_fu_5093_p2 = (tmp_11_reg_23152.read() | tmp_1199_fu_5088_p2.read());
}

void mnist_fp16_opt::thread_tmp_1201_fu_19470_p2() {
    tmp_1201_fu_19470_p2 = (!tmp_1202_cast_fu_19466_p1.read().is_01() || !tmp_1417_fu_19442_p1.read().is_01())? sc_lv<9>(): (sc_bigint<9>(tmp_1202_cast_fu_19466_p1.read()) + sc_biguint<9>(tmp_1417_fu_19442_p1.read()));
}

void mnist_fp16_opt::thread_tmp_1202_cast_fu_19466_p1() {
    tmp_1202_cast_fu_19466_p1 = esl_sext<9,8>(tmp_1196_fu_19460_p2.read());
}

void mnist_fp16_opt::thread_tmp_1202_fu_19493_p2() {
    tmp_1202_fu_19493_p2 = (!p_shl159_cast_fu_19486_p3.read().is_01() || !tmp_1203_cast_fu_19483_p1.read().is_01())? sc_lv<11>(): (sc_biguint<11>(p_shl159_cast_fu_19486_p3.read()) - sc_bigint<11>(tmp_1203_cast_fu_19483_p1.read()));
}

void mnist_fp16_opt::thread_tmp_1203_cast_fu_19483_p1() {
    tmp_1203_cast_fu_19483_p1 = esl_sext<11,9>(tmp_1201_reg_27178.read());
}

void mnist_fp16_opt::thread_tmp_1203_fu_19499_p2() {
    tmp_1203_fu_19499_p2 = (!tmp_1202_fu_19493_p2.read().is_01() || !tmp_369_cast_fu_19480_p1.read().is_01())? sc_lv<11>(): (sc_biguint<11>(tmp_1202_fu_19493_p2.read()) + sc_biguint<11>(tmp_369_cast_fu_19480_p1.read()));
}

void mnist_fp16_opt::thread_tmp_1204_fu_5098_p2() {
    tmp_1204_fu_5098_p2 = (tmp_25_reg_23169.read() | tmp_1200_fu_5093_p2.read());
}

void mnist_fp16_opt::thread_tmp_1205_fu_17347_p1() {
    tmp_1205_fu_17347_p1 = esl_sext<10,8>(tmp_1426_fu_17337_p4.read());
}

void mnist_fp16_opt::thread_tmp_1206_cast_fu_19505_p1() {
    tmp_1206_cast_fu_19505_p1 = esl_zext<64,11>(tmp_1203_fu_19499_p2.read());
}

void mnist_fp16_opt::thread_tmp_1206_fu_17351_p1() {
    tmp_1206_fu_17351_p1 = esl_sext<10,9>(tmp_1427_reg_26649.read());
}

void mnist_fp16_opt::thread_tmp_1207_fu_17354_p3() {
    tmp_1207_fu_17354_p3 = (!tmp_1425_reg_26606.read()[0].is_01())? sc_lv<10>(): ((tmp_1425_reg_26606.read()[0].to_bool())? tmp_1205_fu_17347_p1.read(): tmp_1206_fu_17351_p1.read());
}

void mnist_fp16_opt::thread_tmp_1208_fu_17502_p1() {
    tmp_1208_fu_17502_p1 = esl_sext<11,5>(tmp_1430_fu_17492_p4.read());
}

void mnist_fp16_opt::thread_tmp_1209_fu_17506_p1() {
    tmp_1209_fu_17506_p1 = esl_sext<11,6>(tmp_1431_reg_26684.read());
}

void mnist_fp16_opt::thread_tmp_120_cast_fu_9625_p1() {
    tmp_120_cast_fu_9625_p1 = esl_zext<13,5>(p_14_mid2_reg_24276.read());
}

void mnist_fp16_opt::thread_tmp_120_fu_6964_p3() {
    tmp_120_fu_6964_p3 = esl_concat<3,2>(tmp_7_mid2_v_fu_6953_p3.read(), ap_const_lv2_0);
}

void mnist_fp16_opt::thread_tmp_1210_fu_17530_p3() {
    tmp_1210_fu_17530_p3 = (!tmp_1425_reg_26606_pp17_iter2_reg.read()[0].is_01())? sc_lv<3>(): ((tmp_1425_reg_26606_pp17_iter2_reg.read()[0].to_bool())? neg_ti41_fu_17520_p2.read(): tmp_1433_fu_17526_p1.read());
}

void mnist_fp16_opt::thread_tmp_1211_fu_17906_p3() {
    tmp_1211_fu_17906_p3 = esl_concat<3,3>(tmp_1210_reg_26715.read(), ap_const_lv3_0);
}

void mnist_fp16_opt::thread_tmp_1212_fu_11434_p1() {
    tmp_1212_fu_11434_p1 = mul23_fu_22885_p2.read().range(25-1, 0);
}

void mnist_fp16_opt::thread_tmp_1214_fu_11484_p4() {
    tmp_1214_fu_11484_p4 = neg_mul23_fu_11479_p2.read().range(24, 16);
}

void mnist_fp16_opt::thread_tmp_1215_fu_17917_p2() {
    tmp_1215_fu_17917_p2 = (!p_shl160_cast_fu_17913_p1.read().is_01() || !tmp_294_7_cast_fu_17903_p1.read().is_01())? sc_lv<7>(): (sc_biguint<7>(p_shl160_cast_fu_17913_p1.read()) - sc_biguint<7>(tmp_294_7_cast_fu_17903_p1.read()));
}

void mnist_fp16_opt::thread_tmp_1216_fu_17927_p2() {
    tmp_1216_fu_17927_p2 = (!tmp_1217_cast_fu_17923_p1.read().is_01() || !tmp_1428_fu_17899_p1.read().is_01())? sc_lv<8>(): (sc_bigint<8>(tmp_1217_cast_fu_17923_p1.read()) + sc_biguint<8>(tmp_1428_fu_17899_p1.read()));
}

void mnist_fp16_opt::thread_tmp_1217_cast_fu_17923_p1() {
    tmp_1217_cast_fu_17923_p1 = esl_sext<8,7>(tmp_1215_fu_17917_p2.read());
}

void mnist_fp16_opt::thread_tmp_1217_fu_17949_p2() {
    tmp_1217_fu_17949_p2 = (!p_shl161_cast_fu_17941_p3.read().is_01() || !tmp_1218_cast_fu_17933_p1.read().is_01())? sc_lv<10>(): (sc_biguint<10>(p_shl161_cast_fu_17941_p3.read()) - sc_bigint<10>(tmp_1218_cast_fu_17933_p1.read()));
}

void mnist_fp16_opt::thread_tmp_1218_cast_fu_17933_p1() {
    tmp_1218_cast_fu_17933_p1 = esl_sext<10,8>(tmp_1216_fu_17927_p2.read());
}

void mnist_fp16_opt::thread_tmp_1218_fu_17955_p2() {
    tmp_1218_fu_17955_p2 = (!ap_const_lv10_6.is_01() || !tmp_1217_fu_17949_p2.read().is_01())? sc_lv<10>(): (sc_biguint<10>(ap_const_lv10_6) + sc_biguint<10>(tmp_1217_fu_17949_p2.read()));
}

void mnist_fp16_opt::thread_tmp_121_cast_fu_6612_p1() {
    tmp_121_cast_fu_6612_p1 = esl_sext<64,11>(tmp_66_fu_6607_p2.read());
}

void mnist_fp16_opt::thread_tmp_121_fu_6980_p2() {
    tmp_121_fu_6980_p2 = (!p_shl14_cast_fu_6976_p1.read().is_01() || !tmp_7_mid2_cast_fu_6960_p1.read().is_01())? sc_lv<6>(): (sc_biguint<6>(p_shl14_cast_fu_6976_p1.read()) - sc_biguint<6>(tmp_7_mid2_cast_fu_6960_p1.read()));
}

void mnist_fp16_opt::thread_tmp_1220_fu_19758_p3() {
    tmp_1220_fu_19758_p3 = esl_concat<5,4>(tmp_325_mid2_v_fu_19746_p3.read(), ap_const_lv4_0);
}

void mnist_fp16_opt::thread_tmp_1221_cast_fu_17991_p1() {
    tmp_1221_cast_fu_17991_p1 = esl_zext<64,10>(tmp_1218_reg_26781.read());
}

void mnist_fp16_opt::thread_tmp_1221_fu_19770_p3() {
    tmp_1221_fu_19770_p3 = esl_concat<5,3>(tmp_325_mid2_v_fu_19746_p3.read(), ap_const_lv3_0);
}

void mnist_fp16_opt::thread_tmp_1222_fu_19782_p2() {
    tmp_1222_fu_19782_p2 = (!p_shl162_cast_fu_19778_p1.read().is_01() || !tmp_325_mid2_cast_fu_19754_p1.read().is_01())? sc_lv<9>(): (sc_biguint<9>(p_shl162_cast_fu_19778_p1.read()) - sc_biguint<9>(tmp_325_mid2_cast_fu_19754_p1.read()));
}

void mnist_fp16_opt::thread_tmp_1223_cast_fu_19766_p1() {
    tmp_1223_cast_fu_19766_p1 = esl_zext<10,9>(tmp_1220_fu_19758_p3.read());
}

void mnist_fp16_opt::thread_tmp_1223_fu_12502_p1() {
    tmp_1223_fu_12502_p1 = grp_fu_11625_p2.read().range(8-1, 0);
}

void mnist_fp16_opt::thread_tmp_1224_fu_19816_p2() {
    tmp_1224_fu_19816_p2 = (exitcond65_mid_fu_19804_p2.read() | exitcond_flatten44_fu_19732_p2.read());
}

void mnist_fp16_opt::thread_tmp_1225_cast_fu_19788_p1() {
    tmp_1225_cast_fu_19788_p1 = esl_sext<10,9>(tmp_1222_fu_19782_p2.read());
}

void mnist_fp16_opt::thread_tmp_1225_fu_19846_p2() {
    tmp_1225_fu_19846_p2 = (!tmp_359_mid2_cast_fu_19842_p1.read().is_01() || !tmp_1225_cast_fu_19788_p1.read().is_01())? sc_lv<10>(): (sc_biguint<10>(tmp_359_mid2_cast_fu_19842_p1.read()) + sc_bigint<10>(tmp_1225_cast_fu_19788_p1.read()));
}

void mnist_fp16_opt::thread_tmp_1226_fu_19868_p2() {
    tmp_1226_fu_19868_p2 = (!p_shl163_cast_fu_19860_p3.read().is_01() || !tmp_1227_cast_fu_19852_p1.read().is_01())? sc_lv<11>(): (sc_biguint<11>(p_shl163_cast_fu_19860_p3.read()) - sc_bigint<11>(tmp_1227_cast_fu_19852_p1.read()));
}

void mnist_fp16_opt::thread_tmp_1227_cast_fu_19852_p1() {
    tmp_1227_cast_fu_19852_p1 = esl_sext<11,10>(tmp_1225_fu_19846_p2.read());
}

void mnist_fp16_opt::thread_tmp_1227_fu_19311_p2() {
    tmp_1227_fu_19311_p2 = (!tmp_1188_fu_19243_p2.read().is_01() || !tmp_461_cast_fu_19308_p1.read().is_01())? sc_lv<12>(): (sc_biguint<12>(tmp_1188_fu_19243_p2.read()) + sc_biguint<12>(tmp_461_cast_fu_19308_p1.read()));
}

void mnist_fp16_opt::thread_tmp_1228_fu_11843_p1() {
    tmp_1228_fu_11843_p1 = mul24_fu_22937_p2.read().range(25-1, 0);
}

void mnist_fp16_opt::thread_tmp_1229_fu_21004_p3() {
    tmp_1229_fu_21004_p3 = esl_concat<5,3>(tmp_385_mid2_v_fu_20992_p3.read(), ap_const_lv3_0);
}

void mnist_fp16_opt::thread_tmp_122_fu_7554_p2() {
    tmp_122_fu_7554_p2 = (!F2_fu_7522_p2.read().is_01() || !ap_const_lv12_E.is_01())? sc_lv<1>(): sc_lv<1>(F2_fu_7522_p2.read() == ap_const_lv12_E);
}

void mnist_fp16_opt::thread_tmp_1230_cast_fu_19710_p1() {
    tmp_1230_cast_fu_19710_p1 = esl_zext<64,12>(tmp_1227_reg_27142_pp20_iter27_reg.read());
}

void mnist_fp16_opt::thread_tmp_1230_fu_21016_p2() {
    tmp_1230_fu_21016_p2 = (!p_shl164_cast_fu_21012_p1.read().is_01() || !tmp_385_mid2_cast_fu_21000_p1.read().is_01())? sc_lv<9>(): (sc_biguint<9>(p_shl164_cast_fu_21012_p1.read()) - sc_biguint<9>(tmp_385_mid2_cast_fu_21000_p1.read()));
}

void mnist_fp16_opt::thread_tmp_1231_fu_21050_p2() {
    tmp_1231_fu_21050_p2 = (exitcond83_mid_fu_21038_p2.read() | exitcond_flatten47_fu_20978_p2.read());
}

void mnist_fp16_opt::thread_tmp_1232_cast_fu_21022_p1() {
    tmp_1232_cast_fu_21022_p1 = esl_sext<10,9>(tmp_1230_fu_21016_p2.read());
}

void mnist_fp16_opt::thread_tmp_1232_fu_21076_p2() {
    tmp_1232_fu_21076_p2 = (!tmp_1232_cast_fu_21022_p1.read().is_01() || !tmp_418_mid2_cast_fu_21072_p1.read().is_01())? sc_lv<10>(): (sc_bigint<10>(tmp_1232_cast_fu_21022_p1.read()) + sc_biguint<10>(tmp_418_mid2_cast_fu_21072_p1.read()));
}

void mnist_fp16_opt::thread_tmp_1233_fu_21116_p2() {
    tmp_1233_fu_21116_p2 = (!p_shl165_cast_fu_21109_p3.read().is_01() || !tmp_1234_cast_fu_21106_p1.read().is_01())? sc_lv<11>(): (sc_biguint<11>(p_shl165_cast_fu_21109_p3.read()) - sc_bigint<11>(tmp_1234_cast_fu_21106_p1.read()));
}

void mnist_fp16_opt::thread_tmp_1234_cast_fu_21106_p1() {
    tmp_1234_cast_fu_21106_p1 = esl_sext<11,10>(tmp_1232_reg_27598.read());
}

void mnist_fp16_opt::thread_tmp_1234_fu_21125_p2() {
    tmp_1234_fu_21125_p2 = (!tmp_1233_fu_21116_p2.read().is_01() || !tmp_626_cast_fu_21122_p1.read().is_01())? sc_lv<11>(): (sc_biguint<11>(tmp_1233_fu_21116_p2.read()) + sc_biguint<11>(tmp_626_cast_fu_21122_p1.read()));
}

void mnist_fp16_opt::thread_tmp_1235_fu_11910_p4() {
    tmp_1235_fu_11910_p4 = neg_mul24_fu_11905_p2.read().range(24, 20);
}

void mnist_fp16_opt::thread_tmp_1236_fu_20730_p2() {
    tmp_1236_fu_20730_p2 = (!tmp_1226_reg_27291.read().is_01() || !tmp_512_cast_fu_20727_p1.read().is_01())? sc_lv<11>(): (sc_biguint<11>(tmp_1226_reg_27291.read()) + sc_biguint<11>(tmp_512_cast_fu_20727_p1.read()));
}

void mnist_fp16_opt::thread_tmp_1237_cast_fu_21131_p1() {
    tmp_1237_cast_fu_21131_p1 = esl_zext<64,11>(tmp_1234_fu_21125_p2.read());
}

void mnist_fp16_opt::thread_tmp_1237_fu_19931_p2() {
    tmp_1237_fu_19931_p2 = (!tmp_1223_cast_reg_27268.read().is_01() || !tmp_420_mid2_cast_fu_19927_p1.read().is_01())? sc_lv<10>(): (sc_biguint<10>(tmp_1223_cast_reg_27268.read()) + sc_biguint<10>(tmp_420_mid2_cast_fu_19927_p1.read()));
}

void mnist_fp16_opt::thread_tmp_1238_cast_fu_20956_p1() {
    tmp_1238_cast_fu_20956_p1 = esl_zext<64,11>(tmp_1236_reg_27513.read());
}

void mnist_fp16_opt::thread_tmp_1238_fu_20043_p2() {
    tmp_1238_fu_20043_p2 = (!p_shl6_fu_20039_p1.read().is_01() || !tmp_1239_cast_fu_20029_p1.read().is_01())? sc_lv<64>(): (sc_biguint<64>(p_shl6_fu_20039_p1.read()) - sc_biguint<64>(tmp_1239_cast_fu_20029_p1.read()));
}

void mnist_fp16_opt::thread_tmp_1239_cast_fu_20029_p1() {
    tmp_1239_cast_fu_20029_p1 = esl_zext<64,10>(tmp_1237_reg_27317.read());
}

void mnist_fp16_opt::thread_tmp_1239_fu_20049_p3() {
    tmp_1239_fu_20049_p3 = esl_concat<5,3>(tmp_420_mid2_v_reg_27310.read(), ap_const_lv3_0);
}

void mnist_fp16_opt::thread_tmp_123_fu_10165_p2() {
    tmp_123_fu_10165_p2 = (!F2_1_fu_10159_p2.read().is_01() || !ap_const_lv12_E.is_01())? sc_lv<1>(): (sc_bigint<12>(F2_1_fu_10159_p2.read()) > sc_bigint<12>(ap_const_lv12_E));
}

void mnist_fp16_opt::thread_tmp_1241_fu_20060_p2() {
    tmp_1241_fu_20060_p2 = (!tmp_420_mid2_cast1_fu_20026_p1.read().is_01() || !p_shl167_cast_fu_20056_p1.read().is_01())? sc_lv<9>(): (sc_biguint<9>(tmp_420_mid2_cast1_fu_20026_p1.read()) + sc_biguint<9>(p_shl167_cast_fu_20056_p1.read()));
}

void mnist_fp16_opt::thread_tmp_1242_fu_19960_p2() {
    tmp_1242_fu_19960_p2 = (exitcond73_mid_fu_19948_p2.read() | exitcond_flatten48_fu_19905_p2.read());
}

void mnist_fp16_opt::thread_tmp_1243_fu_20069_p2() {
    tmp_1243_fu_20069_p2 = (!tmp_1241_fu_20060_p2.read().is_01() || !tmp_442_mid2_cast_fu_20066_p1.read().is_01())? sc_lv<9>(): (sc_biguint<9>(tmp_1241_fu_20060_p2.read()) + sc_biguint<9>(tmp_442_mid2_cast_fu_20066_p1.read()));
}

void mnist_fp16_opt::thread_tmp_1244_fu_20125_p2() {
    tmp_1244_fu_20125_p2 = (!tmp_1245_cast_fu_20115_p1.read().is_01() || !p_shl168_cast_fu_20118_p3.read().is_01())? sc_lv<12>(): (sc_biguint<12>(tmp_1245_cast_fu_20115_p1.read()) + sc_biguint<12>(p_shl168_cast_fu_20118_p3.read()));
}

void mnist_fp16_opt::thread_tmp_1245_cast_fu_20115_p1() {
    tmp_1245_cast_fu_20115_p1 = esl_zext<12,9>(tmp_1243_reg_27350.read());
}

void mnist_fp16_opt::thread_tmp_1245_fu_20078_p2() {
    tmp_1245_fu_20078_p2 = (!tmp_1238_fu_20043_p2.read().is_01() || !tmp_443_mid2_cast_fu_20075_p1.read().is_01())? sc_lv<64>(): (sc_biguint<64>(tmp_1238_fu_20043_p2.read()) + sc_biguint<64>(tmp_443_mid2_cast_fu_20075_p1.read()));
}

void mnist_fp16_opt::thread_tmp_1246_fu_11934_p1() {
    tmp_1246_fu_11934_p1 = p_v6_fu_11927_p3.read().range(2-1, 0);
}

void mnist_fp16_opt::thread_tmp_1247_fu_20100_p2() {
    tmp_1247_fu_20100_p2 = (!p_shl169_cast_fu_20092_p3.read().is_01() || !tmp_1466_fu_20084_p1.read().is_01())? sc_lv<13>(): (sc_biguint<13>(p_shl169_cast_fu_20092_p3.read()) - sc_biguint<13>(tmp_1466_fu_20084_p1.read()));
}

void mnist_fp16_opt::thread_tmp_1248_fu_20143_p2() {
    tmp_1248_fu_20143_p2 = (!tmp_1244_fu_20125_p2.read().is_01() || !tmp_474_cast_fu_20139_p1.read().is_01())? sc_lv<12>(): (sc_biguint<12>(tmp_1244_fu_20125_p2.read()) + sc_biguint<12>(tmp_474_cast_fu_20139_p1.read()));
}

void mnist_fp16_opt::thread_tmp_1249_fu_11944_p1() {
    tmp_1249_fu_11944_p1 = p_v6_fu_11927_p3.read().range(2-1, 0);
}

void mnist_fp16_opt::thread_tmp_124_fu_10171_p2() {
    tmp_124_fu_10171_p2 = (!ap_const_lv12_FF2.is_01() || !F2_1_fu_10159_p2.read().is_01())? sc_lv<12>(): (sc_bigint<12>(ap_const_lv12_FF2) + sc_biguint<12>(F2_1_fu_10159_p2.read()));
}

void mnist_fp16_opt::thread_tmp_1250_fu_20109_p2() {
    tmp_1250_fu_20109_p2 = (!tmp_1247_fu_20100_p2.read().is_01() || !tmp_545_cast_fu_20106_p1.read().is_01())? sc_lv<13>(): (sc_biguint<13>(tmp_1247_fu_20100_p2.read()) + sc_biguint<13>(tmp_545_cast_fu_20106_p1.read()));
}

void mnist_fp16_opt::thread_tmp_1251_cast_fu_20149_p1() {
    tmp_1251_cast_fu_20149_p1 = esl_zext<64,12>(tmp_1248_fu_20143_p2.read());
}

void mnist_fp16_opt::thread_tmp_1251_fu_21473_p3() {
    tmp_1251_fu_21473_p3 = esl_concat<5,3>(p_80_reg_4142.read(), ap_const_lv3_0);
}

void mnist_fp16_opt::thread_tmp_1252_cast_fu_20154_p1() {
    tmp_1252_cast_fu_20154_p1 = esl_zext<64,13>(tmp_1250_reg_27356.read());
}

void mnist_fp16_opt::thread_tmp_1252_fu_21485_p2() {
    tmp_1252_fu_21485_p2 = (!p_shl170_cast_fu_21481_p1.read().is_01() || !tmp_417_cast_fu_21469_p1.read().is_01())? sc_lv<9>(): (sc_biguint<9>(p_shl170_cast_fu_21481_p1.read()) - sc_biguint<9>(tmp_417_cast_fu_21469_p1.read()));
}

void mnist_fp16_opt::thread_tmp_1253_fu_21539_p2() {
    tmp_1253_fu_21539_p2 = (!tmp_438_mid2_cast_fu_21535_p1.read().is_01() || !tmp_1254_cast_reg_27720.read().is_01())? sc_lv<10>(): (sc_biguint<10>(tmp_438_mid2_cast_fu_21535_p1.read()) + sc_bigint<10>(tmp_1254_cast_reg_27720.read()));
}

void mnist_fp16_opt::thread_tmp_1254_cast_fu_21491_p1() {
    tmp_1254_cast_fu_21491_p1 = esl_sext<10,9>(tmp_1252_fu_21485_p2.read());
}

void mnist_fp16_opt::thread_tmp_1254_fu_21560_p2() {
    tmp_1254_fu_21560_p2 = (!p_shl171_cast_fu_21552_p3.read().is_01() || !tmp_1255_cast_fu_21544_p1.read().is_01())? sc_lv<11>(): (sc_biguint<11>(p_shl171_cast_fu_21552_p3.read()) - sc_bigint<11>(tmp_1255_cast_fu_21544_p1.read()));
}

void mnist_fp16_opt::thread_tmp_1255_cast_fu_21544_p1() {
    tmp_1255_cast_fu_21544_p1 = esl_sext<11,10>(tmp_1253_fu_21539_p2.read());
}

void mnist_fp16_opt::thread_tmp_1255_fu_21570_p2() {
    tmp_1255_fu_21570_p2 = (!tmp_638_cast_fu_21566_p1.read().is_01() || !tmp_1254_fu_21560_p2.read().is_01())? sc_lv<11>(): (sc_biguint<11>(tmp_638_cast_fu_21566_p1.read()) + sc_biguint<11>(tmp_1254_fu_21560_p2.read()));
}

void mnist_fp16_opt::thread_tmp_1256_fu_12544_p1() {
    tmp_1256_fu_12544_p1 = tmp_994_fu_12538_p2.read().range(7-1, 0);
}

void mnist_fp16_opt::thread_tmp_1257_fu_22206_p3() {
    tmp_1257_fu_22206_p3 = esl_concat<5,3>(p_87_reg_4221.read(), ap_const_lv3_0);
}

void mnist_fp16_opt::thread_tmp_1258_cast_fu_21582_p1() {
    tmp_1258_cast_fu_21582_p1 = esl_zext<64,11>(tmp_1255_reg_27739.read());
}

void mnist_fp16_opt::thread_tmp_1258_fu_22218_p3() {
    tmp_1258_fu_22218_p3 = esl_concat<5,1>(p_87_reg_4221.read(), ap_const_lv1_0);
}

void mnist_fp16_opt::thread_tmp_1259_fu_22230_p2() {
    tmp_1259_fu_22230_p2 = (!p_shl172_cast_fu_22214_p1.read().is_01() || !p_shl173_cast_fu_22226_p1.read().is_01())? sc_lv<9>(): (sc_biguint<9>(p_shl172_cast_fu_22214_p1.read()) + sc_biguint<9>(p_shl173_cast_fu_22226_p1.read()));
}

void mnist_fp16_opt::thread_tmp_125_fu_10177_p2() {
    tmp_125_fu_10177_p2 = (!ap_const_lv12_E.is_01() || !F2_1_fu_10159_p2.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_E) - sc_biguint<12>(F2_1_fu_10159_p2.read()));
}

void mnist_fp16_opt::thread_tmp_1260_fu_22236_p2() {
    tmp_1260_fu_22236_p2 = (!tmp_464_cast_reg_27917.read().is_01() || !tmp_1259_fu_22230_p2.read().is_01())? sc_lv<9>(): (sc_biguint<9>(tmp_464_cast_reg_27917.read()) + sc_biguint<9>(tmp_1259_fu_22230_p2.read()));
}

void mnist_fp16_opt::thread_tmp_1262_cast_fu_22381_p1() {
    tmp_1262_cast_fu_22381_p1 = esl_zext<64,9>(tmp_1260_reg_27930.read());
}

void mnist_fp16_opt::thread_tmp_1263_fu_12556_p3() {
    tmp_1263_fu_12556_p3 = esl_concat<8,1>(tmp_994_fu_12538_p2.read(), ap_const_lv1_0);
}

void mnist_fp16_opt::thread_tmp_1264_fu_5581_p1() {
    tmp_1264_fu_5581_p1 = mul25_fu_22780_p2.read().range(25-1, 0);
}

void mnist_fp16_opt::thread_tmp_1266_fu_5598_p4() {
    tmp_1266_fu_5598_p4 = neg_mul25_fu_5593_p2.read().range(24, 17);
}

void mnist_fp16_opt::thread_tmp_1268_fu_6507_p1() {
    tmp_1268_fu_6507_p1 = grp_fu_5635_p2.read().range(6-1, 0);
}

void mnist_fp16_opt::thread_tmp_1269_fu_6519_p1() {
    tmp_1269_fu_6519_p1 = grp_fu_5635_p2.read().range(9-1, 0);
}

void mnist_fp16_opt::thread_tmp_126_fu_10191_p2() {
    tmp_126_fu_10191_p2 = (!F2_1_fu_10159_p2.read().is_01() || !ap_const_lv12_E.is_01())? sc_lv<1>(): sc_lv<1>(F2_1_fu_10159_p2.read() == ap_const_lv12_E);
}

void mnist_fp16_opt::thread_tmp_1270_fu_15958_p1() {
    tmp_1270_fu_15958_p1 = tmp_1012_fu_15952_p2.read().range(8-1, 0);
}

void mnist_fp16_opt::thread_tmp_1271_fu_15989_p3() {
    tmp_1271_fu_15989_p3 = esl_concat<10,1>(tmp_1012_reg_26203.read(), ap_const_lv1_0);
}

void mnist_fp16_opt::thread_tmp_1272_fu_16033_p1() {
    tmp_1272_fu_16033_p1 = conv4_0_load_to_int_fu_16020_p1.read().range(23-1, 0);
}

void mnist_fp16_opt::thread_tmp_1273_fu_16070_p1() {
    tmp_1273_fu_16070_p1 = ireg_V_7_fu_16062_p3.read().range(63-1, 0);
}

void mnist_fp16_opt::thread_tmp_1275_fu_16092_p1() {
    tmp_1275_fu_16092_p1 = ireg_V_7_fu_16062_p3.read().range(52-1, 0);
}

void mnist_fp16_opt::thread_tmp_1276_fu_16167_p1() {
    tmp_1276_fu_16167_p1 = man_V_16_fu_16122_p3.read().range(16-1, 0);
}

void mnist_fp16_opt::thread_tmp_1277_fu_16171_p4() {
    tmp_1277_fu_16171_p4 = sh_amt_5_fu_16153_p3.read().range(11, 4);
}

void mnist_fp16_opt::thread_tmp_1278_fu_16239_p1() {
    tmp_1278_fu_16239_p1 = tmp_569_fu_16234_p2.read().range(16-1, 0);
}

void mnist_fp16_opt::thread_tmp_1279_fu_16259_p1() {
    tmp_1279_fu_16259_p1 = tmp_579_fu_16253_p2.read().range(16-1, 0);
}

void mnist_fp16_opt::thread_tmp_1281_fu_15668_p1() {
    tmp_1281_fu_15668_p1 = msb_idx_8_fu_15662_p2.read().range(31-1, 0);
}

void mnist_fp16_opt::thread_tmp_1283_fu_15696_p4() {
    tmp_1283_fu_15696_p4 = msb_idx_9_fu_15686_p3.read().range(30, 5);
}

void mnist_fp16_opt::thread_tmp_1284_fu_15727_p1() {
    tmp_1284_fu_15727_p1 = msb_idx_9_fu_15686_p3.read().range(4-1, 0);
}

void mnist_fp16_opt::thread_tmp_1285_fu_15731_p2() {
    tmp_1285_fu_15731_p2 = (!ap_const_lv4_1.is_01() || !tmp_1284_fu_15727_p1.read().is_01())? sc_lv<4>(): (sc_biguint<4>(ap_const_lv4_1) + sc_biguint<4>(tmp_1284_fu_15727_p1.read()));
}

void mnist_fp16_opt::thread_tmp_1286_fu_15737_p1() {
    tmp_1286_fu_15737_p1 = esl_zext<16,4>(tmp_1285_fu_15731_p2.read());
}

void mnist_fp16_opt::thread_tmp_1288_fu_15778_p1() {
    tmp_1288_fu_15778_p1 = msb_idx_8_reg_26144.read().range(8-1, 0);
}

void mnist_fp16_opt::thread_tmp_1289_fu_14853_p3() {
    tmp_1289_fu_14853_p3 = esl_concat<8,2>(tmp_1018_fu_14844_p2.read(), ap_const_lv2_0);
}

void mnist_fp16_opt::thread_tmp_128_fu_9131_p3() {
    tmp_128_fu_9131_p3 = esl_concat<1,52>(ap_const_lv1_1, tmp_686_reg_24397.read());
}

void mnist_fp16_opt::thread_tmp_1290_fu_14951_p1() {
    tmp_1290_fu_14951_p1 = tmp_1022_fu_14945_p2.read().range(11-1, 0);
}

void mnist_fp16_opt::thread_tmp_1291_fu_14955_p1() {
    tmp_1291_fu_14955_p1 = tmp_1022_fu_14945_p2.read().range(9-1, 0);
}

void mnist_fp16_opt::thread_tmp_1292_fu_15030_p1() {
    tmp_1292_fu_15030_p1 = ireg_V_14_fu_15026_p1.read().range(63-1, 0);
}

void mnist_fp16_opt::thread_tmp_1294_fu_15052_p1() {
    tmp_1294_fu_15052_p1 = ireg_V_14_fu_15026_p1.read().range(52-1, 0);
}

void mnist_fp16_opt::thread_tmp_1295_fu_15163_p1() {
    tmp_1295_fu_15163_p1 = man_V_12_fu_15118_p3.read().range(16-1, 0);
}

void mnist_fp16_opt::thread_tmp_1296_fu_15167_p4() {
    tmp_1296_fu_15167_p4 = sh_amt_6_fu_15149_p3.read().range(11, 4);
}

void mnist_fp16_opt::thread_tmp_1297_fu_15320_p1() {
    tmp_1297_fu_15320_p1 = tmp_341_fu_15315_p2.read().range(16-1, 0);
}

void mnist_fp16_opt::thread_tmp_1298_fu_15340_p1() {
    tmp_1298_fu_15340_p1 = tmp_344_fu_15334_p2.read().range(16-1, 0);
}

void mnist_fp16_opt::thread_tmp_1299_fu_15066_p1() {
    tmp_1299_fu_15066_p1 = ireg_V_15_fu_15062_p1.read().range(63-1, 0);
}

void mnist_fp16_opt::thread_tmp_129_fu_6990_p3() {
    tmp_129_fu_6990_p3 = esl_concat<3,5>(tmp_7_mid2_v_fu_6953_p3.read(), ap_const_lv5_0);
}

void mnist_fp16_opt::thread_tmp_1301_fu_15088_p1() {
    tmp_1301_fu_15088_p1 = ireg_V_15_fu_15062_p1.read().range(52-1, 0);
}

void mnist_fp16_opt::thread_tmp_1302_fu_15248_p1() {
    tmp_1302_fu_15248_p1 = man_V_15_fu_15203_p3.read().range(16-1, 0);
}

void mnist_fp16_opt::thread_tmp_1303_fu_15252_p4() {
    tmp_1303_fu_15252_p4 = sh_amt_7_fu_15234_p3.read().range(11, 4);
}

void mnist_fp16_opt::thread_tmp_1304_fu_15469_p1() {
    tmp_1304_fu_15469_p1 = tmp_397_fu_15464_p2.read().range(16-1, 0);
}

void mnist_fp16_opt::thread_tmp_1305_fu_15489_p1() {
    tmp_1305_fu_15489_p1 = tmp_400_fu_15483_p2.read().range(16-1, 0);
}

void mnist_fp16_opt::thread_tmp_1306_fu_5103_p2() {
    tmp_1306_fu_5103_p2 = (!p_s_reg_2273.read().is_01() || !ap_const_lv5_1E.is_01())? sc_lv<1>(): sc_lv<1>(p_s_reg_2273.read() == ap_const_lv5_1E);
}

void mnist_fp16_opt::thread_tmp_1307_fu_5109_p2() {
    tmp_1307_fu_5109_p2 = (tmp_1306_fu_5103_p2.read() | tmp_6_reg_23135.read());
}

void mnist_fp16_opt::thread_tmp_1308_fu_5114_p2() {
    tmp_1308_fu_5114_p2 = (tmp_11_reg_23152.read() | tmp_1307_fu_5109_p2.read());
}

void mnist_fp16_opt::thread_tmp_1309_fu_5119_p2() {
    tmp_1309_fu_5119_p2 = (tmp_25_reg_23169.read() | tmp_1308_fu_5114_p2.read());
}

}

