#include "mnist_fp16_opt.h"
#include "AESL_pkg.h"

using namespace std;

namespace ap_rtl {

void mnist_fp16_opt::thread_hdltv_gen() {
    const char* dump_tv = std::getenv("AP_WRITE_TV");
    if (!(dump_tv && string(dump_tv) == "on")) return;

    wait();

    mHdltvinHandle << "[ " << endl;
    mHdltvoutHandle << "[ " << endl;
    int ap_cycleNo = 0;
    while (1) {
        wait();
        const char* mComma = ap_cycleNo == 0 ? " " : ", " ;
        mHdltvinHandle << mComma << "{"  <<  " \"ap_rst_n\" :  \"" << ap_rst_n.read() << "\" ";
        mHdltvinHandle << " , " <<  " \"ap_start\" :  \"" << ap_start.read() << "\" ";
        mHdltvoutHandle << mComma << "{"  <<  " \"ap_done\" :  \"" << ap_done.read() << "\" ";
        mHdltvoutHandle << " , " <<  " \"ap_idle\" :  \"" << ap_idle.read() << "\" ";
        mHdltvoutHandle << " , " <<  " \"ap_ready\" :  \"" << ap_ready.read() << "\" ";
        mHdltvinHandle << " , " <<  " \"stream_in_TDATA\" :  \"" << stream_in_TDATA.read() << "\" ";
        mHdltvinHandle << " , " <<  " \"stream_in_TVALID\" :  \"" << stream_in_TVALID.read() << "\" ";
        mHdltvoutHandle << " , " <<  " \"stream_in_TREADY\" :  \"" << stream_in_TREADY.read() << "\" ";
        mHdltvinHandle << " , " <<  " \"stream_in_TKEEP\" :  \"" << stream_in_TKEEP.read() << "\" ";
        mHdltvinHandle << " , " <<  " \"stream_in_TSTRB\" :  \"" << stream_in_TSTRB.read() << "\" ";
        mHdltvinHandle << " , " <<  " \"stream_in_TUSER\" :  \"" << stream_in_TUSER.read() << "\" ";
        mHdltvinHandle << " , " <<  " \"stream_in_TLAST\" :  \"" << stream_in_TLAST.read() << "\" ";
        mHdltvinHandle << " , " <<  " \"stream_in_TID\" :  \"" << stream_in_TID.read() << "\" ";
        mHdltvinHandle << " , " <<  " \"stream_in_TDEST\" :  \"" << stream_in_TDEST.read() << "\" ";
        mHdltvoutHandle << " , " <<  " \"result_V\" :  \"" << result_V.read() << "\" ";
        mHdltvoutHandle << " , " <<  " \"result_V_ap_vld\" :  \"" << result_V_ap_vld.read() << "\" ";
        mHdltvinHandle << "}" << std::endl;
        mHdltvoutHandle << "}" << std::endl;
        ap_cycleNo++;
    }
}

}

