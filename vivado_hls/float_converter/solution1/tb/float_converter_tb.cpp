#include <ap_axi_sdata.h>
#include <hls_opencv.h>
#include <hls_video.h>

#define TEST_IMAGE
#define PRINT_VALUES

typedef  ap_axiu<8,1,1,1> axi_u;
typedef ap_axiu<32,1,1,1> axi_f;

void float_converter(hls::stream<axi_u>& stream_in, hls::stream<axi_f>& stream_out);

typedef union {
    unsigned int u;
    float        f;
} union_t;

int main(void) {
#ifdef TEST_IMAGE
    // Load reference image
    IplImage *image_in = cvLoadImage("image_in.jpg", CV_LOAD_IMAGE_GRAYSCALE);
    CvSize size_in = cvGetSize(image_in);

    // Create output image
    IplImage *image_out = cvCreateImage(size_in, IPL_DEPTH_32F, 1);

    // Create streams.
    hls::stream<axi_u> stream_in;
    hls::stream<axi_f> stream_out;

    // Convert OpenCV data to AXI-Stream.
    IplImage2AXIvideo(image_in, stream_in);

    // Call the center_image() function.
    float_converter(stream_in, stream_out);

#ifdef PRINT_VALUES
    union_t uni;
    axi_f value_out;
    HLS_SIZE_T i, j;

    printf("const static ap_uint<32> input_image[28][28] =\n");

    for (i = 0; i < 28; ++i) {
        printf("{");
        for (j = 0; j < 28; ++j) {
            stream_out >> value_out;
            uni.u = value_out.data.to_int();

            if (j != 0) {
                printf(",");
            }

            printf(" 0x%X", uni.u);
        }
        printf(" },\n");
    }
    printf("};\n");
#else
    // Convert AXI-Stream data to OpenCV.
    AXIvideo2IplImage(stream_out, image_out);

    // Save output image.
    cvSaveImage("/home/faku/master-project/vivado_hls/float_converter/solution1/tb/image_out.jpg", image_out);
#endif
    cvReleaseImage(&image_in);
    cvReleaseImage(&image_out);
#else
    axi_u value_in;
    axi_f value_out;

    union_t uni;

    value_in.data = 0;
    value_in.dest = 1;
    value_in.id   = 1;
    value_in.keep = 1;
    value_in.last = 1;
    value_in.strb = 1;
    value_in.user = 1;

    memset(&value_out, 0, sizeof(value_out));

    hls::stream<axi_u> stream_in;
    hls::stream<axi_f> stream_out;

    value_in.data = 84;
    stream_in << value_in;

    float_converter(stream_in, stream_out);

    stream_out >> value_out;

    uni.u = value_out.data;

    printf("== value_out\n");
    printf(" data: 0x%X\n", value_out.data.to_int());
    printf(" dest: 0x%X\n", value_out.dest.to_int());
    printf(" id  : 0x%X\n", value_out.id.to_int());
    printf(" keep: 0x%X\n", value_out.keep.to_int());
    printf(" last: 0x%X\n", value_out.last.to_int());
    printf(" strb: 0x%X\n", value_out.strb.to_int());
    printf(" user: 0x%X\n", value_out.user.to_int());
    printf("uni.u: 0x%X, uni.f: %.5f\n", uni.u, uni.f);
#endif
}
