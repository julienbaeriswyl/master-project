#include <ap_axi_sdata.h>
#include <hls_video.h>
#include <ap_utils.h>

typedef  ap_axiu<8,1,1,1> axi_u;
typedef ap_axiu<32,1,1,1> axi_f;

union conv {
    int   u;
    float f;
};
typedef union conv conv_t;

void float_converter(hls::stream<axi_u>& stream_in, hls::stream<axi_f>& stream_out) {
#pragma HLS INTERFACE axis port=stream_in
#pragma HLS INTERFACE axis port=stream_out

#if 0
    axi_u udata;
    axi_f fdata;
    conv_t conv;

    stream_in >> udata;

    conv.f = udata.data.to_double() / 255.0f;

    fdata.data = conv.u;
    fdata.dest = udata.dest;
    fdata.id   = udata.id;
    fdata.keep = (udata.keep.to_int() == 0) ? 0x0 : 0xF;
    fdata.last = udata.last;
    fdata.strb = (udata.strb.to_int() == 0) ? 0x0 : 0xF;
    fdata.user = udata.user;

    stream_out << fdata;
#else
    axi_u udata;
    axi_f fdata;
    conv_t conv;

    bool sof = 0;
    loop_wait_for_start: while (!sof) {
#pragma HLS pipeline II=1
#pragma HLS loop_tripcount avg=0 max=0
        stream_in >> udata;
        sof = udata.user.to_int();
    }
    loop_height: for (HLS_SIZE_T i = 0; i < 28; i++) {
        bool eol = 0;

        loop_width: for (HLS_SIZE_T j = 0; j < 28; j++) {
#pragma HLS loop_flatten off
#pragma HLS pipeline II=1
            if (sof || eol) {
                sof = 0;
                eol = udata.last.to_int();
            } else {
                // If we didn't reach EOL, then read the next pixel
                stream_in >> udata;
                eol = udata.last.to_int();
            }

            conv.f = udata.data.to_double() / 255.0f;

            fdata.data = conv.u;
            fdata.dest = udata.dest;
            fdata.id   = udata.id;
            fdata.keep = (udata.keep.to_int() == 0) ? 0x0 : 0xF;
            fdata.last = udata.last;
            fdata.strb = (udata.strb.to_int() == 0) ? 0x0 : 0xF;
            fdata.user = udata.user;

            stream_out << fdata;
        }

        loop_wait_for_eol: while (!eol) {
#pragma HLS pipeline II=1
#pragma HLS loop_tripcount avg=0 max=0
            // Keep reading until we get to EOL
            stream_in >> udata;
            eol = udata.last.to_int();
        }
    }
#endif
}
