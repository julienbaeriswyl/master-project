#include "mnist_fp16.h"
#include "AESL_pkg.h"

using namespace std;

namespace ap_rtl {

void mnist_fp16::thread_p_shl47_cast_fu_6437_p1() {
    p_shl47_cast_fu_6437_p1 = esl_sext<11,10>(tmp_355_fu_6429_p3.read());
}

void mnist_fp16::thread_p_shl49_cast_fu_4923_p1() {
    p_shl49_cast_fu_4923_p1 = esl_zext<9,8>(tmp_418_fu_4915_p3.read());
}

void mnist_fp16::thread_p_shl4_cast_fu_6966_p1() {
    p_shl4_cast_fu_6966_p1 = esl_zext<9,8>(p_shl4_fu_6958_p3.read());
}

void mnist_fp16::thread_p_shl4_fu_6958_p3() {
    p_shl4_fu_6958_p3 = esl_concat<4,4>(tmp_453_fu_6954_p1.read(), ap_const_lv4_0);
}

void mnist_fp16::thread_p_shl50_cast_fu_4935_p1() {
    p_shl50_cast_fu_4935_p1 = esl_zext<9,4>(tmp_423_fu_4927_p3.read());
}

void mnist_fp16::thread_p_shl51_cast_fu_7358_p1() {
    p_shl51_cast_fu_7358_p1 = esl_zext<9,8>(tmp_442_fu_7350_p3.read());
}

void mnist_fp16::thread_p_shl52_cast_fu_7370_p1() {
    p_shl52_cast_fu_7370_p1 = esl_zext<9,5>(tmp_445_fu_7362_p3.read());
}

void mnist_fp16::thread_p_shl53_cast_fu_4990_p3() {
    p_shl53_cast_fu_4990_p3 = esl_concat<8,5>(tmp_466_fu_4986_p1.read(), ap_const_lv5_0);
}

void mnist_fp16::thread_p_shl54_cast_fu_5033_p3() {
    p_shl54_cast_fu_5033_p3 = esl_concat<7,2>(tmp_488_fu_5029_p1.read(), ap_const_lv2_0);
}

void mnist_fp16::thread_p_shl55_cast_fu_5006_p1() {
    p_shl55_cast_fu_5006_p1 = esl_sext<13,11>(tmp_468_fu_4998_p3.read());
}

void mnist_fp16::thread_p_shl56_cast_fu_7627_p1() {
    p_shl56_cast_fu_7627_p1 = esl_zext<9,8>(tmp_497_fu_7619_p3.read());
}

void mnist_fp16::thread_p_shl57_cast_fu_7639_p1() {
    p_shl57_cast_fu_7639_p1 = esl_zext<9,5>(tmp_499_fu_7631_p3.read());
}

void mnist_fp16::thread_p_shl58_cast_fu_7409_p3() {
    p_shl58_cast_fu_7409_p3 = esl_concat<8,4>(tmp_505_fu_7405_p1.read(), ap_const_lv4_0);
}

void mnist_fp16::thread_p_shl59_cast_fu_7425_p1() {
    p_shl59_cast_fu_7425_p1 = esl_sext<12,11>(tmp_507_fu_7417_p3.read());
}

void mnist_fp16::thread_p_shl5_cast_fu_6976_p1() {
    p_shl5_cast_fu_6976_p1 = esl_zext<9,5>(tmp_456_fu_6970_p2.read());
}

void mnist_fp16::thread_p_shl5_fu_7480_p1() {
    p_shl5_fu_7480_p1 = esl_zext<64,9>(tmp_632_fu_7472_p3.read());
}

void mnist_fp16::thread_p_shl60_cast_fu_6523_p3() {
    p_shl60_cast_fu_6523_p3 = esl_concat<8,5>(tmp_514_fu_6519_p1.read(), ap_const_lv5_0);
}

void mnist_fp16::thread_p_shl61_cast_fu_6539_p1() {
    p_shl61_cast_fu_6539_p1 = esl_sext<13,12>(tmp_522_fu_6531_p3.read());
}

void mnist_fp16::thread_p_shl62_cast_fu_7239_p1() {
    p_shl62_cast_fu_7239_p1 = esl_zext<7,6>(tmp_588_fu_7232_p3.read());
}

void mnist_fp16::thread_p_shl63_cast_fu_7250_p1() {
    p_shl63_cast_fu_7250_p1 = esl_zext<7,3>(tmp_589_fu_7243_p3.read());
}

void mnist_fp16::thread_p_shl64_cast_fu_7277_p3() {
    p_shl64_cast_fu_7277_p3 = esl_concat<7,4>(tmp_592_reg_16535.read(), ap_const_lv4_0);
}

void mnist_fp16::thread_p_shl65_cast_fu_7291_p1() {
    p_shl65_cast_fu_7291_p1 = esl_sext<11,9>(tmp_593_fu_7284_p3.read());
}

void mnist_fp16::thread_p_shl66_cast_fu_7678_p3() {
    p_shl66_cast_fu_7678_p3 = esl_concat<8,4>(tmp_599_fu_7674_p1.read(), ap_const_lv4_0);
}

void mnist_fp16::thread_p_shl67_cast_fu_7694_p1() {
    p_shl67_cast_fu_7694_p1 = esl_sext<12,11>(tmp_600_fu_7686_p3.read());
}

void mnist_fp16::thread_p_shl68_cast_fu_8725_p1() {
    p_shl68_cast_fu_8725_p1 = esl_zext<9,8>(tmp_615_fu_8717_p3.read());
}

void mnist_fp16::thread_p_shl69_cast_fu_8737_p1() {
    p_shl69_cast_fu_8737_p1 = esl_zext<9,5>(tmp_616_fu_8729_p3.read());
}

void mnist_fp16::thread_p_shl6_cast_fu_8134_p1() {
    p_shl6_cast_fu_8134_p1 = esl_zext<9,8>(p_shl6_fu_8126_p3.read());
}

void mnist_fp16::thread_p_shl6_fu_8126_p3() {
    p_shl6_fu_8126_p3 = esl_concat<4,4>(tmp_619_fu_8122_p1.read(), ap_const_lv4_0);
}

void mnist_fp16::thread_p_shl71_cast_fu_9765_p1() {
    p_shl71_cast_fu_9765_p1 = esl_zext<9,8>(tmp_634_fu_9757_p3.read());
}

void mnist_fp16::thread_p_shl72_cast_fu_9777_p1() {
    p_shl72_cast_fu_9777_p1 = esl_zext<9,5>(tmp_635_fu_9769_p3.read());
}

void mnist_fp16::thread_p_shl73_cast_fu_8776_p3() {
    p_shl73_cast_fu_8776_p3 = esl_concat<8,4>(tmp_638_fu_8772_p1.read(), ap_const_lv4_0);
}

void mnist_fp16::thread_p_shl74_cast_fu_8792_p1() {
    p_shl74_cast_fu_8792_p1 = esl_sext<12,11>(tmp_639_fu_8784_p3.read());
}

void mnist_fp16::thread_p_shl75_cast_fu_7543_p3() {
    p_shl75_cast_fu_7543_p3 = esl_concat<8,2>(tmp_643_fu_7539_p1.read(), ap_const_lv2_0);
}

void mnist_fp16::thread_p_shl76_cast_fu_8407_p1() {
    p_shl76_cast_fu_8407_p1 = esl_zext<8,7>(tmp_663_fu_8400_p3.read());
}

void mnist_fp16::thread_p_shl77_cast_fu_8418_p1() {
    p_shl77_cast_fu_8418_p1 = esl_zext<8,4>(tmp_664_fu_8411_p3.read());
}

void mnist_fp16::thread_p_shl78_cast_fu_8445_p3() {
    p_shl78_cast_fu_8445_p3 = esl_concat<8,4>(tmp_667_reg_16881.read(), ap_const_lv4_0);
}

void mnist_fp16::thread_p_shl79_cast_fu_8459_p1() {
    p_shl79_cast_fu_8459_p1 = esl_sext<12,10>(tmp_668_fu_8452_p3.read());
}

void mnist_fp16::thread_p_shl7_cast_fu_8144_p1() {
    p_shl7_cast_fu_8144_p1 = esl_zext<9,5>(tmp_620_fu_8138_p2.read());
}

void mnist_fp16::thread_p_shl7_fu_8847_p1() {
    p_shl7_fu_8847_p1 = esl_zext<64,10>(tmp_714_fu_8839_p3.read());
}

void mnist_fp16::thread_p_shl80_cast_fu_10209_p1() {
    p_shl80_cast_fu_10209_p1 = esl_zext<9,8>(tmp_671_fu_10201_p3.read());
}

void mnist_fp16::thread_p_shl81_cast_fu_10221_p1() {
    p_shl81_cast_fu_10221_p1 = esl_zext<9,5>(tmp_672_fu_10213_p3.read());
}

void mnist_fp16::thread_p_shl82_cast_fu_10243_p1() {
    p_shl82_cast_fu_10243_p1 = esl_zext<8,7>(tmp_674_fu_10235_p3.read());
}

void mnist_fp16::thread_p_shl83_cast_fu_9816_p3() {
    p_shl83_cast_fu_9816_p3 = esl_concat<8,4>(tmp_677_fu_9812_p1.read(), ap_const_lv4_0);
}

void mnist_fp16::thread_p_shl84_cast_fu_9832_p1() {
    p_shl84_cast_fu_9832_p1 = esl_sext<12,11>(tmp_678_fu_9824_p3.read());
}

void mnist_fp16::thread_p_shl85_cast_fu_10766_p1() {
    p_shl85_cast_fu_10766_p1 = esl_zext<8,7>(tmp_691_fu_10758_p3.read());
}

void mnist_fp16::thread_p_shl86_cast_fu_10286_p3() {
    p_shl86_cast_fu_10286_p3 = esl_concat<7,3>(tmp_694_fu_10282_p1.read(), ap_const_lv3_0);
}

void mnist_fp16::thread_p_shl89_cast_fu_10801_p3() {
    p_shl89_cast_fu_10801_p3 = esl_concat<8,3>(tmp_718_fu_10792_p2.read(), ap_const_lv3_0);
}

void mnist_fp16::thread_p_shl8_cast_fu_2954_p1() {
    p_shl8_cast_fu_2954_p1 = esl_zext<11,10>(tmp_1_fu_2946_p3.read());
}

void mnist_fp16::thread_p_shl8_fu_11277_p1() {
    p_shl8_fu_11277_p1 = esl_zext<64,11>(tmp_806_fu_11269_p3.read());
}

void mnist_fp16::thread_p_shl90_cast_fu_8902_p3() {
    p_shl90_cast_fu_8902_p3 = esl_concat<9,2>(tmp_723_fu_8898_p1.read(), ap_const_lv2_0);
}

void mnist_fp16::thread_p_shl91_cast_fu_11471_p1() {
    p_shl91_cast_fu_11471_p1 = esl_zext<9,8>(tmp_725_fu_11463_p3.read());
}

void mnist_fp16::thread_p_shl92_cast_fu_11210_p3() {
    p_shl92_cast_fu_11210_p3 = esl_concat<8,3>(tmp_728_fu_11206_p1.read(), ap_const_lv3_0);
}

void mnist_fp16::thread_p_shl93_cast_fu_10376_p3() {
    p_shl93_cast_fu_10376_p3 = esl_concat<8,4>(tmp_731_fu_10372_p1.read(), ap_const_lv4_0);
}

void mnist_fp16::thread_p_shl94_cast_fu_10392_p1() {
    p_shl94_cast_fu_10392_p1 = esl_sext<12,11>(tmp_732_fu_10384_p3.read());
}

void mnist_fp16::thread_p_shl95_cast_fu_11071_p1() {
    p_shl95_cast_fu_11071_p1 = esl_zext<7,6>(tmp_766_fu_11064_p3.read());
}

void mnist_fp16::thread_p_shl96_cast_fu_11101_p3() {
    p_shl96_cast_fu_11101_p3 = esl_concat<7,3>(tmp_769_reg_17615.read(), ap_const_lv3_0);
}

void mnist_fp16::thread_p_shl97_cast_fu_11905_p1() {
    p_shl97_cast_fu_11905_p1 = esl_zext<9,8>(tmp_773_fu_11897_p3.read());
}

void mnist_fp16::thread_p_shl98_cast_fu_11514_p3() {
    p_shl98_cast_fu_11514_p3 = esl_concat<8,3>(tmp_776_fu_11510_p1.read(), ap_const_lv3_0);
}

void mnist_fp16::thread_p_shl99_cast_fu_12517_p1() {
    p_shl99_cast_fu_12517_p1 = esl_zext<9,8>(tmp_791_fu_12509_p3.read());
}

void mnist_fp16::thread_p_shl9_cast_fu_2966_p1() {
    p_shl9_cast_fu_2966_p1 = esl_zext<11,7>(tmp_9_fu_2958_p3.read());
}

void mnist_fp16::thread_p_shl9_fu_12631_p1() {
    p_shl9_fu_12631_p1 = esl_zext<64,12>(tmp_903_fu_12623_p3.read());
}

void mnist_fp16::thread_p_shl_fu_4905_p1() {
    p_shl_fu_4905_p1 = esl_zext<64,8>(tmp_415_fu_4897_p3.read());
}

void mnist_fp16::thread_p_v1_fu_7200_p3() {
    p_v1_fu_7200_p3 = (!tmp_574_reg_16496.read()[0].is_01())? sc_lv<12>(): ((tmp_574_reg_16496.read()[0].to_bool())? tmp_583_fu_7193_p1.read(): tmp_585_fu_7197_p1.read());
}

void mnist_fp16::thread_p_v2_fu_8368_p3() {
    p_v2_fu_8368_p3 = (!tmp_649_reg_16842.read()[0].is_01())? sc_lv<13>(): ((tmp_649_reg_16842.read()[0].to_bool())? tmp_658_fu_8361_p1.read(): tmp_660_fu_8365_p1.read());
}

void mnist_fp16::thread_p_v3_fu_11029_p3() {
    p_v3_fu_11029_p3 = (!tmp_752_reg_17571.read()[0].is_01())? sc_lv<11>(): ((tmp_752_reg_17571.read()[0].to_bool())? tmp_761_fu_11022_p1.read(): tmp_763_fu_11026_p1.read());
}

void mnist_fp16::thread_p_v4_fu_12172_p3() {
    p_v4_fu_12172_p3 = (!tmp_824_reg_17932.read()[0].is_01())? sc_lv<12>(): ((tmp_824_reg_17932.read()[0].to_bool())? tmp_833_fu_12165_p1.read(): tmp_835_fu_12169_p1.read());
}

void mnist_fp16::thread_p_v_fu_4434_p3() {
    p_v_fu_4434_p3 = (!tmp_264_reg_15757.read()[0].is_01())? sc_lv<14>(): ((tmp_264_reg_15757.read()[0].to_bool())? tmp_290_fu_4427_p1.read(): tmp_292_fu_4431_p1.read());
}

void mnist_fp16::thread_pad_temp1_0_address0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state95.read())) {
        pad_temp1_0_address0 =  (sc_lv<12>) (tmp_678_cast_fu_5078_p1.read());
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state89.read())) {
        pad_temp1_0_address0 =  (sc_lv<12>) (tmp_648_cast_fu_4754_p1.read());
    } else {
        pad_temp1_0_address0 = "XXXXXXXXXXXX";
    }
}

void mnist_fp16::thread_pad_temp1_0_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state95.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state89.read()))) {
        pad_temp1_0_ce0 = ap_const_logic_1;
    } else {
        pad_temp1_0_ce0 = ap_const_logic_0;
    }
}

void mnist_fp16::thread_pad_temp1_0_we0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state89.read())) {
        pad_temp1_0_we0 = ap_const_logic_1;
    } else {
        pad_temp1_0_we0 = ap_const_logic_0;
    }
}

void mnist_fp16::thread_pad_temp2_0_address0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state161.read())) {
        pad_temp2_0_address0 =  (sc_lv<10>) (tmp_687_fu_7588_p1.read());
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state155.read())) {
        pad_temp2_0_address0 =  (sc_lv<10>) (tmp_699_cast_fu_7321_p1.read());
    } else {
        pad_temp2_0_address0 =  (sc_lv<10>) ("XXXXXXXXXX");
    }
}

void mnist_fp16::thread_pad_temp2_0_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state161.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state155.read()))) {
        pad_temp2_0_ce0 = ap_const_logic_1;
    } else {
        pad_temp2_0_ce0 = ap_const_logic_0;
    }
}

void mnist_fp16::thread_pad_temp2_0_we0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state155.read())) {
        pad_temp2_0_we0 = ap_const_logic_1;
    } else {
        pad_temp2_0_we0 = ap_const_logic_0;
    }
}

void mnist_fp16::thread_pad_temp3_0_address0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state217.read())) {
        pad_temp3_0_address0 =  (sc_lv<11>) (tmp_735_fu_8947_p1.read());
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state211.read())) {
        pad_temp3_0_address0 =  (sc_lv<11>) (tmp_763_cast_fu_8688_p1.read());
    } else {
        pad_temp3_0_address0 =  (sc_lv<11>) ("XXXXXXXXXXX");
    }
}

void mnist_fp16::thread_pad_temp3_0_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state217.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state211.read()))) {
        pad_temp3_0_ce0 = ap_const_logic_1;
    } else {
        pad_temp3_0_ce0 = ap_const_logic_0;
    }
}

void mnist_fp16::thread_pad_temp3_0_we0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state211.read())) {
        pad_temp3_0_we0 = ap_const_logic_1;
    } else {
        pad_temp3_0_we0 = ap_const_logic_0;
    }
}

void mnist_fp16::thread_pad_temp4_0_address0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state282.read())) {
        pad_temp4_0_address0 =  (sc_lv<10>) (tmp_866_cast_fu_11428_p1.read());
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state276.read())) {
        pad_temp4_0_address0 =  (sc_lv<10>) (tmp_813_cast_fu_11134_p1.read());
    } else {
        pad_temp4_0_address0 =  (sc_lv<10>) ("XXXXXXXXXX");
    }
}

void mnist_fp16::thread_pad_temp4_0_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state282.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state276.read()))) {
        pad_temp4_0_ce0 = ap_const_logic_1;
    } else {
        pad_temp4_0_ce0 = ap_const_logic_0;
    }
}

void mnist_fp16::thread_pad_temp4_0_we0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state276.read())) {
        pad_temp4_0_we0 = ap_const_logic_1;
    } else {
        pad_temp4_0_we0 = ap_const_logic_0;
    }
}

void mnist_fp16::thread_pad_temp5_0_address0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state337.read())) {
        pad_temp5_0_address0 =  (sc_lv<11>) (tmp_890_cast_fu_12783_p1.read());
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state331.read())) {
        pad_temp5_0_address0 =  (sc_lv<11>) (tmp_868_cast_fu_12476_p1.read());
    } else {
        pad_temp5_0_address0 =  (sc_lv<11>) ("XXXXXXXXXXX");
    }
}

void mnist_fp16::thread_pad_temp5_0_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state337.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state331.read()))) {
        pad_temp5_0_ce0 = ap_const_logic_1;
    } else {
        pad_temp5_0_ce0 = ap_const_logic_0;
    }
}

void mnist_fp16::thread_pad_temp5_0_we0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state331.read())) {
        pad_temp5_0_we0 = ap_const_logic_1;
    } else {
        pad_temp5_0_we0 = ap_const_logic_0;
    }
}

void mnist_fp16::thread_pad_temp_0_0_address0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state37.read())) {
        pad_temp_0_0_address0 =  (sc_lv<10>) (tmp_603_cast_fu_3581_p1.read());
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state32.read())) {
        pad_temp_0_0_address0 =  (sc_lv<10>) (tmp_571_cast_fu_3331_p1.read());
    } else {
        pad_temp_0_0_address0 =  (sc_lv<10>) ("XXXXXXXXXX");
    }
}

void mnist_fp16::thread_pad_temp_0_0_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state37.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state32.read()))) {
        pad_temp_0_0_ce0 = ap_const_logic_1;
    } else {
        pad_temp_0_0_ce0 = ap_const_logic_0;
    }
}

void mnist_fp16::thread_pad_temp_0_0_we0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state32.read())) {
        pad_temp_0_0_we0 = ap_const_logic_1;
    } else {
        pad_temp_0_0_we0 = ap_const_logic_0;
    }
}

void mnist_fp16::thread_phi_mul191_cast_fu_6879_p1() {
    phi_mul191_cast_fu_6879_p1 = esl_zext<11,10>(phi_mul1_reg_1760.read());
}

void mnist_fp16::thread_phi_mul193_cast_fu_8047_p1() {
    phi_mul193_cast_fu_8047_p1 = esl_zext<12,11>(phi_mul2_reg_1956.read());
}

void mnist_fp16::thread_phi_mul195_cast_fu_10732_p1() {
    phi_mul195_cast_fu_10732_p1 = esl_zext<10,9>(phi_mul3_reg_2234.read());
}

void mnist_fp16::thread_phi_mul197_cast_fu_11871_p1() {
    phi_mul197_cast_fu_11871_p1 = esl_zext<11,10>(phi_mul4_reg_2428.read());
}

void mnist_fp16::thread_phi_mul_cast_fu_4040_p1() {
    phi_mul_cast_fu_4040_p1 = esl_zext<13,12>(phi_mul_reg_1479.read());
}

void mnist_fp16::thread_pool1_0_address0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state153.read())) {
        pool1_0_address0 =  (sc_lv<10>) (tmp_698_cast_fu_7307_p1.read());
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state121.read())) {
        pool1_0_address0 = pool1_0_addr_1_reg_16318.read();
    } else {
        pool1_0_address0 =  (sc_lv<10>) ("XXXXXXXXXX");
    }
}

void mnist_fp16::thread_pool1_0_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state121.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state153.read()))) {
        pool1_0_ce0 = ap_const_logic_1;
    } else {
        pool1_0_ce0 = ap_const_logic_0;
    }
}

void mnist_fp16::thread_pool1_0_we0() {
    if ((esl_seteq<1,1,1>(exitcond39_fu_6489_p2.read(), ap_const_lv1_1) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state121.read()))) {
        pool1_0_we0 = ap_const_logic_1;
    } else {
        pool1_0_we0 = ap_const_logic_0;
    }
}

void mnist_fp16::thread_pool2_0_address0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state274.read())) {
        pool2_0_address0 =  (sc_lv<9>) (tmp_812_cast_fu_11120_p1.read());
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state243.read())) {
        pool2_0_address0 = pool2_0_addr_1_reg_17398.read();
    } else {
        pool2_0_address0 =  (sc_lv<9>) ("XXXXXXXXX");
    }
}

void mnist_fp16::thread_pool2_0_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state243.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state274.read()))) {
        pool2_0_ce0 = ap_const_logic_1;
    } else {
        pool2_0_ce0 = ap_const_logic_0;
    }
}

void mnist_fp16::thread_pool2_0_we0() {
    if ((esl_seteq<1,1,1>(exitcond71_fu_10342_p2.read(), ap_const_lv1_1) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state243.read()))) {
        pool2_0_we0 = ap_const_logic_1;
    } else {
        pool2_0_we0 = ap_const_logic_0;
    }
}

void mnist_fp16::thread_pool3_0_V_address0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state379.read())) {
        pool3_0_V_address0 =  (sc_lv<4>) (tmp_502_fu_14722_p1.read());
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state377.read())) {
        pool3_0_V_address0 =  (sc_lv<4>) (tmp_433_reg_18586.read());
    } else {
        pool3_0_V_address0 =  (sc_lv<4>) ("XXXX");
    }
}

void mnist_fp16::thread_pool3_0_V_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state379.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state377.read()))) {
        pool3_0_V_ce0 = ap_const_logic_1;
    } else {
        pool3_0_V_ce0 = ap_const_logic_0;
    }
}

void mnist_fp16::thread_pool3_0_V_d0() {
    pool3_0_V_d0 = (!or_cond29_fu_14675_p2.read()[0].is_01())? sc_lv<16>(): ((or_cond29_fu_14675_p2.read()[0].to_bool())? newSel34_fu_14667_p3.read(): ap_const_lv16_0);
}

void mnist_fp16::thread_pool3_0_V_we0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state377.read())) {
        pool3_0_V_we0 = ap_const_logic_1;
    } else {
        pool3_0_V_we0 = ap_const_logic_0;
    }
}

void mnist_fp16::thread_pred_1_fu_15215_p3() {
    pred_1_fu_15215_p3 = (!tmp_568_fu_15209_p2.read()[0].is_01())? sc_lv<32>(): ((tmp_568_fu_15209_p2.read()[0].to_bool())? pred_2_reg_18842.read(): pred_reg_2799.read());
}

void mnist_fp16::thread_pred_2_to_int_fu_15132_p1() {
    pred_2_to_int_fu_15132_p1 = pred_2_reg_18842.read();
}

void mnist_fp16::thread_pred_to_int_fu_15149_p1() {
    pred_to_int_fu_15149_p1 = pred_reg_2799.read();
}

void mnist_fp16::thread_preds_0_address0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state492.read())) {
        preds_0_address0 =  (sc_lv<4>) (tmp_458_fu_15127_p1.read());
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state491.read())) {
        preds_0_address0 =  (sc_lv<4>) (tmp_426_reg_18809.read());
    } else {
        preds_0_address0 =  (sc_lv<4>) ("XXXX");
    }
}

void mnist_fp16::thread_preds_0_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state492.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state491.read()))) {
        preds_0_ce0 = ap_const_logic_1;
    } else {
        preds_0_ce0 = ap_const_logic_0;
    }
}

void mnist_fp16::thread_preds_0_we0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state491.read())) {
        preds_0_we0 = ap_const_logic_1;
    } else {
        preds_0_we0 = ap_const_logic_0;
    }
}

void mnist_fp16::thread_r_V_100_tr_fu_12062_p2() {
    r_V_100_tr_fu_12062_p2 = (!tmp374_reg_17906.read().is_01() || !tmp375_cast_fu_12058_p1.read().is_01())? sc_lv<11>(): (sc_biguint<11>(tmp374_reg_17906.read()) + sc_bigint<11>(tmp375_cast_fu_12058_p1.read()));
}

void mnist_fp16::thread_r_V_10_fu_5063_p2() {
    r_V_10_fu_5063_p2 = (!rhs_V_3_cast_fu_5059_p1.read().is_01() || !p_18_reg_1555.read().is_01())? sc_lv<5>(): (sc_biguint<5>(rhs_V_3_cast_fu_5059_p1.read()) + sc_biguint<5>(p_18_reg_1555.read()));
}

void mnist_fp16::thread_r_V_11_fu_7040_p2() {
    r_V_11_fu_7040_p2 = (!ap_const_lv4_F.is_01() || !tmp_571_fu_7036_p1.read().is_01())? sc_lv<4>(): (sc_bigint<4>(ap_const_lv4_F) + sc_biguint<4>(tmp_571_fu_7036_p1.read()));
}

void mnist_fp16::thread_r_V_12_fu_7221_p3() {
    r_V_12_fu_7221_p3 = (!tmp_574_reg_16496.read()[0].is_01())? sc_lv<2>(): ((tmp_574_reg_16496.read()[0].to_bool())? neg_ti4_fu_7211_p2.read(): tmp_587_fu_7217_p1.read());
}

void mnist_fp16::thread_r_V_13_fu_8148_p2() {
    r_V_13_fu_8148_p2 = (!p_shl6_cast_fu_8134_p1.read().is_01() || !p_shl7_cast_fu_8144_p1.read().is_01())? sc_lv<9>(): (sc_biguint<9>(p_shl6_cast_fu_8134_p1.read()) - sc_biguint<9>(p_shl7_cast_fu_8144_p1.read()));
}

void mnist_fp16::thread_r_V_14_fu_7520_p2() {
    r_V_14_fu_7520_p2 = (!rhs_V_13_cast_fu_7516_p1.read().is_01() || !p_25_reg_1817.read().is_01())? sc_lv<4>(): (sc_biguint<4>(rhs_V_13_cast_fu_7516_p1.read()) + sc_biguint<4>(p_25_reg_1817.read()));
}

void mnist_fp16::thread_r_V_15_fu_8208_p2() {
    r_V_15_fu_8208_p2 = (!ap_const_lv4_F.is_01() || !tmp_645_fu_8204_p1.read().is_01())? sc_lv<4>(): (sc_bigint<4>(ap_const_lv4_F) + sc_biguint<4>(tmp_645_fu_8204_p1.read()));
}

void mnist_fp16::thread_r_V_16_fu_8389_p3() {
    r_V_16_fu_8389_p3 = (!tmp_649_reg_16842.read()[0].is_01())? sc_lv<3>(): ((tmp_649_reg_16842.read()[0].to_bool())? neg_ti6_fu_8379_p2.read(): tmp_662_fu_8385_p1.read());
}

void mnist_fp16::thread_r_V_17_fu_7573_p2() {
    r_V_17_fu_7573_p2 = (!rhs_V_8_cast_fu_7569_p1.read().is_01() || !p_32_reg_1829.read().is_01())? sc_lv<4>(): (sc_biguint<4>(rhs_V_8_cast_fu_7569_p1.read()) + sc_biguint<4>(p_32_reg_1829.read()));
}

void mnist_fp16::thread_r_V_17_tr_fu_4323_p2() {
    r_V_17_tr_fu_4323_p2 = (!tmp349_cast_fu_4320_p1.read().is_01() || !tmp32_reg_15723.read().is_01())? sc_lv<13>(): (sc_bigint<13>(tmp349_cast_fu_4320_p1.read()) + sc_biguint<13>(tmp32_reg_15723.read()));
}

void mnist_fp16::thread_r_V_18_fu_10300_p3() {
    r_V_18_fu_10300_p3 = esl_concat<3,1>(p_59_reg_2154.read(), ap_const_lv1_0);
}

void mnist_fp16::thread_r_V_19_fu_10334_p3() {
    r_V_19_fu_10334_p3 = esl_concat<3,1>(p_65_reg_2165.read(), ap_const_lv1_0);
}

void mnist_fp16::thread_r_V_1_fu_3069_p2() {
    r_V_1_fu_3069_p2 = (!p_shl10_cast_fu_3035_p1.read().is_01() || !p_shl1_cast_fu_3065_p1.read().is_01())? sc_lv<11>(): (sc_biguint<11>(p_shl10_cast_fu_3035_p1.read()) - sc_biguint<11>(p_shl1_cast_fu_3065_p1.read()));
}

void mnist_fp16::thread_r_V_20_fu_8879_p2() {
    r_V_20_fu_8879_p2 = (!rhs_V_17_cast_fu_8875_p1.read().is_01() || !p_35_reg_2016.read().is_01())? sc_lv<4>(): (sc_biguint<4>(rhs_V_17_cast_fu_8875_p1.read()) + sc_biguint<4>(p_35_reg_2016.read()));
}

void mnist_fp16::thread_r_V_21_fu_8932_p2() {
    r_V_21_fu_8932_p2 = (!rhs_V_15_cast_fu_8928_p1.read().is_01() || !p_44_reg_2028.read().is_01())? sc_lv<4>(): (sc_biguint<4>(rhs_V_15_cast_fu_8928_p1.read()) + sc_biguint<4>(p_44_reg_2028.read()));
}

void mnist_fp16::thread_r_V_22_fu_10875_p2() {
    r_V_22_fu_10875_p2 = (!ap_const_lv3_7.is_01() || !tmp_750_fu_10871_p1.read().is_01())? sc_lv<3>(): (sc_bigint<3>(ap_const_lv3_7) + sc_biguint<3>(tmp_750_fu_10871_p1.read()));
}

void mnist_fp16::thread_r_V_23_fu_11050_p3() {
    r_V_23_fu_11050_p3 = (!tmp_752_reg_17571.read()[0].is_01())? sc_lv<3>(): ((tmp_752_reg_17571.read()[0].to_bool())? neg_ti8_fu_11040_p2.read(): tmp_765_fu_11046_p1.read());
}

void mnist_fp16::thread_r_V_24_fu_11335_p2() {
    r_V_24_fu_11335_p2 = (!lhs_V_27_cast_reg_17656.read().is_01() || !rhs_V_19_cast_fu_11331_p1.read().is_01())? sc_lv<4>(): (sc_biguint<4>(lhs_V_27_cast_reg_17656.read()) + sc_biguint<4>(rhs_V_19_cast_fu_11331_p1.read()));
}

void mnist_fp16::thread_r_V_25_fu_12018_p2() {
    r_V_25_fu_12018_p2 = (!ap_const_lv3_7.is_01() || !tmp_821_fu_12014_p1.read().is_01())? sc_lv<3>(): (sc_bigint<3>(ap_const_lv3_7) + sc_biguint<3>(tmp_821_fu_12014_p1.read()));
}

void mnist_fp16::thread_r_V_26_fu_12193_p3() {
    r_V_26_fu_12193_p3 = (!tmp_824_reg_17932.read()[0].is_01())? sc_lv<4>(): ((tmp_824_reg_17932.read()[0].to_bool())? neg_ti10_fu_12183_p2.read(): tmp_837_fu_12189_p1.read());
}

void mnist_fp16::thread_r_V_27_fu_11414_p2() {
    r_V_27_fu_11414_p2 = (!rhs_V_18_cast_fu_11410_p1.read().is_01() || !lhs_V_29_cast_reg_17674.read().is_01())? sc_lv<4>(): (sc_biguint<4>(rhs_V_18_cast_fu_11410_p1.read()) + sc_biguint<4>(lhs_V_29_cast_reg_17674.read()));
}

void mnist_fp16::thread_r_V_28_fu_12690_p2() {
    r_V_28_fu_12690_p2 = (!lhs_V_32_cast_reg_18072.read().is_01() || !rhs_V_21_cast_fu_12686_p1.read().is_01())? sc_lv<4>(): (sc_biguint<4>(lhs_V_32_cast_reg_18072.read()) + sc_biguint<4>(rhs_V_21_cast_fu_12686_p1.read()));
}

void mnist_fp16::thread_r_V_29_fu_12769_p2() {
    r_V_29_fu_12769_p2 = (!rhs_V_20_cast_fu_12765_p1.read().is_01() || !lhs_V_33_cast_reg_18090.read().is_01())? sc_lv<4>(): (sc_biguint<4>(rhs_V_20_cast_fu_12765_p1.read()) + sc_biguint<4>(lhs_V_33_cast_reg_18090.read()));
}

void mnist_fp16::thread_r_V_2_fu_3566_p2() {
    r_V_2_fu_3566_p2 = (!rhs_V_cast_fu_3562_p1.read().is_01() || !p_8_reg_1376.read().is_01())? sc_lv<5>(): (sc_biguint<5>(rhs_V_cast_fu_3562_p1.read()) + sc_biguint<5>(p_8_reg_1376.read()));
}

void mnist_fp16::thread_r_V_31_tr_fu_7089_p2() {
    r_V_31_tr_fu_7089_p2 = (!tmp357_cast_fu_7086_p1.read().is_01() || !tmp138_reg_16465.read().is_01())? sc_lv<11>(): (sc_bigint<11>(tmp357_cast_fu_7086_p1.read()) + sc_biguint<11>(tmp138_reg_16465.read()));
}

void mnist_fp16::thread_r_V_3_fu_3479_p2() {
    r_V_3_fu_3479_p2 = (!p_5_reg_1364.read().is_01() || !rhs_V_2_cast_fu_3475_p1.read().is_01())? sc_lv<5>(): (sc_biguint<5>(p_5_reg_1364.read()) + sc_biguint<5>(rhs_V_2_cast_fu_3475_p1.read()));
}

void mnist_fp16::thread_r_V_4_fu_4270_p2() {
    r_V_4_fu_4270_p2 = (!ap_const_lv5_1F.is_01() || !p_14_reg_1502.read().is_01())? sc_lv<5>(): (sc_bigint<5>(ap_const_lv5_1F) + sc_biguint<5>(p_14_reg_1502.read()));
}

void mnist_fp16::thread_r_V_5_fu_4165_p2() {
    r_V_5_fu_4165_p2 = (!p_shl2_cast_fu_4149_p1.read().is_01() || !p_shl3_cast_fu_4161_p1.read().is_01())? sc_lv<11>(): (sc_biguint<11>(p_shl2_cast_fu_4149_p1.read()) - sc_biguint<11>(p_shl3_cast_fu_4161_p1.read()));
}

void mnist_fp16::thread_r_V_5_tr_fu_3215_p2() {
    r_V_5_tr_fu_3215_p2 = (!tmp1_reg_15415.read().is_01() || !lhs_V_cast_fu_3211_p1.read().is_01())? sc_lv<11>(): (sc_biguint<11>(tmp1_reg_15415.read()) + sc_bigint<11>(lhs_V_cast_fu_3211_p1.read()));
}

void mnist_fp16::thread_r_V_6_fu_4455_p3() {
    r_V_6_fu_4455_p3 = (!tmp_264_reg_15757.read()[0].is_01())? sc_lv<2>(): ((tmp_264_reg_15757.read()[0].to_bool())? neg_ti2_fu_4445_p2.read(): tmp_297_fu_4451_p1.read());
}

void mnist_fp16::thread_r_V_7_fu_6980_p2() {
    r_V_7_fu_6980_p2 = (!p_shl4_cast_fu_6966_p1.read().is_01() || !p_shl5_cast_fu_6976_p1.read().is_01())? sc_lv<9>(): (sc_biguint<9>(p_shl4_cast_fu_6966_p1.read()) - sc_biguint<9>(p_shl5_cast_fu_6976_p1.read()));
}

void mnist_fp16::thread_r_V_85_tr_fu_8257_p2() {
    r_V_85_tr_fu_8257_p2 = (!tmp362_cast_fu_8254_p1.read().is_01() || !tmp246_reg_16811.read().is_01())? sc_lv<12>(): (sc_bigint<12>(tmp362_cast_fu_8254_p1.read()) + sc_biguint<12>(tmp246_reg_16811.read()));
}

void mnist_fp16::thread_r_V_8_fu_6481_p3() {
    r_V_8_fu_6481_p3 = esl_concat<4,1>(p_33_reg_1691.read(), ap_const_lv1_0);
}

void mnist_fp16::thread_r_V_93_tr_fu_10919_p2() {
    r_V_93_tr_fu_10919_p2 = (!tmp320_reg_17545.read().is_01() || !tmp370_cast_fu_10915_p1.read().is_01())? sc_lv<10>(): (sc_biguint<10>(tmp320_reg_17545.read()) + sc_bigint<10>(tmp370_cast_fu_10915_p1.read()));
}

void mnist_fp16::thread_r_V_9_fu_4971_p2() {
    r_V_9_fu_4971_p2 = (!rhs_V_7_cast_fu_4967_p1.read().is_01() || !p_10_reg_1543.read().is_01())? sc_lv<5>(): (sc_biguint<5>(rhs_V_7_cast_fu_4967_p1.read()) + sc_biguint<5>(p_10_reg_1543.read()));
}

void mnist_fp16::thread_r_V_fu_3171_p2() {
    r_V_fu_3171_p2 = (!ap_const_lv5_1F.is_01() || !p_3_reg_1326.read().is_01())? sc_lv<5>(): (sc_bigint<5>(ap_const_lv5_1F) + sc_biguint<5>(p_3_reg_1326.read()));
}

void mnist_fp16::thread_r_V_s_fu_6447_p3() {
    r_V_s_fu_6447_p3 = esl_concat<4,1>(p_27_reg_1680.read(), ap_const_lv1_0);
}

void mnist_fp16::thread_ra63_V_fu_6495_p2() {
    ra63_V_fu_6495_p2 = (!p_38_reg_1715.read().is_01() || !ap_const_lv2_1.is_01())? sc_lv<2>(): (sc_biguint<2>(p_38_reg_1715.read()) + sc_biguint<2>(ap_const_lv2_1));
}

void mnist_fp16::thread_ra64_V_fu_6555_p2() {
    ra64_V_fu_6555_p2 = (!p_43_reg_1738.read().is_01() || !ap_const_lv2_1.is_01())? sc_lv<2>(): (sc_biguint<2>(p_43_reg_1738.read()) + sc_biguint<2>(ap_const_lv2_1));
}

void mnist_fp16::thread_ra65_V_fu_10348_p2() {
    ra65_V_fu_10348_p2 = (!p_70_reg_2189.read().is_01() || !ap_const_lv2_1.is_01())? sc_lv<2>(): (sc_biguint<2>(p_70_reg_2189.read()) + sc_biguint<2>(ap_const_lv2_1));
}

void mnist_fp16::thread_ra66_V_fu_10408_p2() {
    ra66_V_fu_10408_p2 = (!p_75_reg_2212.read().is_01() || !ap_const_lv2_1.is_01())? sc_lv<2>(): (sc_biguint<2>(p_75_reg_2212.read()) + sc_biguint<2>(ap_const_lv2_1));
}

void mnist_fp16::thread_ra67_V_fu_14044_p2() {
    ra67_V_fu_14044_p2 = (!p_86_reg_2637.read().is_01() || !ap_const_lv3_1.is_01())? sc_lv<3>(): (sc_biguint<3>(p_86_reg_2637.read()) + sc_biguint<3>(ap_const_lv3_1));
}

void mnist_fp16::thread_ra68_V_fu_14087_p2() {
    ra68_V_fu_14087_p2 = (!p_89_reg_2660.read().is_01() || !ap_const_lv3_1.is_01())? sc_lv<3>(): (sc_biguint<3>(p_89_reg_2660.read()) + sc_biguint<3>(ap_const_lv3_1));
}

void mnist_fp16::thread_ra69_V_fu_14716_p2() {
    ra69_V_fu_14716_p2 = (!p_90_reg_2693.read().is_01() || !ap_const_lv5_1.is_01())? sc_lv<5>(): (sc_biguint<5>(p_90_reg_2693.read()) + sc_biguint<5>(ap_const_lv5_1));
}

void mnist_fp16::thread_ra70_V_fu_14978_p2() {
    ra70_V_fu_14978_p2 = (!p_69_reg_2729.read().is_01() || !ap_const_lv4_1.is_01())? sc_lv<4>(): (sc_biguint<4>(p_69_reg_2729.read()) + sc_biguint<4>(ap_const_lv4_1));
}

void mnist_fp16::thread_ra71_V_fu_15087_p2() {
    ra71_V_fu_15087_p2 = (!p_72_reg_2740.read().is_01() || !ap_const_lv4_1.is_01())? sc_lv<4>(): (sc_biguint<4>(p_72_reg_2740.read()) + sc_biguint<4>(ap_const_lv4_1));
}

void mnist_fp16::thread_rc1_V_fu_7453_p2() {
    rc1_V_fu_7453_p2 = (!p_40_reg_1841.read().is_01() || !ap_const_lv3_1.is_01())? sc_lv<3>(): (sc_biguint<3>(p_40_reg_1841.read()) + sc_biguint<3>(ap_const_lv3_1));
}

void mnist_fp16::thread_rc2_V_fu_8820_p2() {
    rc2_V_fu_8820_p2 = (!p_51_reg_2052.read().is_01() || !ap_const_lv4_1.is_01())? sc_lv<4>(): (sc_biguint<4>(p_51_reg_2052.read()) + sc_biguint<4>(ap_const_lv4_1));
}

void mnist_fp16::thread_rc3_V_fu_11246_p2() {
    rc3_V_fu_11246_p2 = (!p_64_reg_2314.read().is_01() || !ap_const_lv4_1.is_01())? sc_lv<4>(): (sc_biguint<4>(p_64_reg_2314.read()) + sc_biguint<4>(ap_const_lv4_1));
}

void mnist_fp16::thread_rc4_V_fu_12600_p2() {
    rc4_V_fu_12600_p2 = (!p_74_reg_2523.read().is_01() || !ap_const_lv5_1.is_01())? sc_lv<5>(): (sc_biguint<5>(p_74_reg_2523.read()) + sc_biguint<5>(ap_const_lv5_1));
}

void mnist_fp16::thread_rc_V_fu_4878_p2() {
    rc_V_fu_4878_p2 = (!p_24_reg_1579.read().is_01() || !ap_const_lv3_1.is_01())? sc_lv<3>(): (sc_biguint<3>(p_24_reg_1579.read()) + sc_biguint<3>(ap_const_lv3_1));
}

void mnist_fp16::thread_reducer4_fu_6872_p3() {
    reducer4_fu_6872_p3 = (!tmp_223_fu_6866_p2.read()[0].is_01())? sc_lv<32>(): ((tmp_223_fu_6866_p2.read()[0].to_bool())? tmp_140_reg_1726.read(): p_03_i1_reg_16412.read());
}

void mnist_fp16::thread_reducer7_fu_10725_p3() {
    reducer7_fu_10725_p3 = (!tmp_389_fu_10719_p2.read()[0].is_01())? sc_lv<32>(): ((tmp_389_fu_10719_p2.read()[0].to_bool())? tmp_318_reg_2200.read(): p_03_i3_reg_17492.read());
}

void mnist_fp16::thread_reducer94_fu_15073_p3() {
    reducer94_fu_15073_p3 = (!tmp_538_fu_15067_p2.read()[0].is_01())? sc_lv<32>(): ((tmp_538_fu_15067_p2.read()[0].to_bool())? compute14_reg_2717.read(): reg_2900.read());
}

void mnist_fp16::thread_reducer9_fu_14395_p3() {
    reducer9_fu_14395_p3 = (!tmp_519_fu_14389_p2.read()[0].is_01())? sc_lv<32>(): ((tmp_519_fu_14389_p2.read()[0].to_bool())? tmp_457_reg_2648.read(): p_03_i5_reg_18566.read());
}

void mnist_fp16::thread_relu1_0_V_address0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state78.read())) {
        relu1_0_V_address0 =  (sc_lv<12>) (tmp_633_cast_fu_4541_p1.read());
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state55.read())) {
        relu1_0_V_address0 =  (sc_lv<12>) (tmp_588_cast_reg_15599.read());
    } else {
        relu1_0_V_address0 = "XXXXXXXXXXXX";
    }
}

void mnist_fp16::thread_relu1_0_V_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state78.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state55.read()))) {
        relu1_0_V_ce0 = ap_const_logic_1;
    } else {
        relu1_0_V_ce0 = ap_const_logic_0;
    }
}

void mnist_fp16::thread_relu1_0_V_d0() {
    relu1_0_V_d0 = (!or_cond2_fu_4025_p2.read()[0].is_01())? sc_lv<16>(): ((or_cond2_fu_4025_p2.read()[0].to_bool())? newSel2_fu_4017_p3.read(): ap_const_lv16_0);
}

void mnist_fp16::thread_relu1_0_V_we0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state55.read())) {
        relu1_0_V_we0 = ap_const_logic_1;
    } else {
        relu1_0_V_we0 = ap_const_logic_0;
    }
}

void mnist_fp16::thread_relu2_0_V_address0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state122.read())) {
        relu2_0_V_address0 =  (sc_lv<12>) (tmp_706_cast_fu_6579_p1.read());
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state117.read())) {
        relu2_0_V_address0 =  (sc_lv<12>) (tmp_640_cast_reg_16186.read());
    } else {
        relu2_0_V_address0 = "XXXXXXXXXXXX";
    }
}

void mnist_fp16::thread_relu2_0_V_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state122.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state117.read()))) {
        relu2_0_V_ce0 = ap_const_logic_1;
    } else {
        relu2_0_V_ce0 = ap_const_logic_0;
    }
}

void mnist_fp16::thread_relu2_0_V_d0() {
    relu2_0_V_d0 = (!or_cond5_fu_6301_p2.read()[0].is_01())? sc_lv<16>(): ((or_cond5_fu_6301_p2.read()[0].to_bool())? newSel6_fu_6293_p3.read(): ap_const_lv16_0);
}

void mnist_fp16::thread_relu2_0_V_we0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state117.read())) {
        relu2_0_V_we0 = ap_const_logic_1;
    } else {
        relu2_0_V_we0 = ap_const_logic_0;
    }
}

void mnist_fp16::thread_relu3_0_V_address0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state200.read())) {
        relu3_0_V_address0 =  (sc_lv<11>) (tmp_750_cast_fu_8475_p1.read());
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state179.read())) {
        relu3_0_V_address0 =  (sc_lv<11>) (tmp_717_cast_reg_16682.read());
    } else {
        relu3_0_V_address0 =  (sc_lv<11>) ("XXXXXXXXXXX");
    }
}

void mnist_fp16::thread_relu3_0_V_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state200.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state179.read()))) {
        relu3_0_V_ce0 = ap_const_logic_1;
    } else {
        relu3_0_V_ce0 = ap_const_logic_0;
    }
}

void mnist_fp16::thread_relu3_0_V_d0() {
    relu3_0_V_d0 = (!or_cond14_fu_8032_p2.read()[0].is_01())? sc_lv<16>(): ((or_cond14_fu_8032_p2.read()[0].to_bool())? newSel16_fu_8024_p3.read(): ap_const_lv16_0);
}

void mnist_fp16::thread_relu3_0_V_we0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state179.read())) {
        relu3_0_V_we0 = ap_const_logic_1;
    } else {
        relu3_0_V_we0 = ap_const_logic_0;
    }
}

void mnist_fp16::thread_relu4_0_V_address0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state244.read())) {
        relu4_0_V_address0 =  (sc_lv<11>) (tmp_819_cast_fu_10432_p1.read());
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state239.read())) {
        relu4_0_V_address0 =  (sc_lv<11>) (tmp_769_cast_reg_17266.read());
    } else {
        relu4_0_V_address0 =  (sc_lv<11>) ("XXXXXXXXXXX");
    }
}

void mnist_fp16::thread_relu4_0_V_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state244.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state239.read()))) {
        relu4_0_V_ce0 = ap_const_logic_1;
    } else {
        relu4_0_V_ce0 = ap_const_logic_0;
    }
}

void mnist_fp16::thread_relu4_0_V_d0() {
    relu4_0_V_d0 = (!or_cond17_fu_10170_p2.read()[0].is_01())? sc_lv<16>(): ((or_cond17_fu_10170_p2.read()[0].to_bool())? newSel20_fu_10162_p3.read(): ap_const_lv16_0);
}

void mnist_fp16::thread_relu4_0_V_we0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state239.read())) {
        relu4_0_V_we0 = ap_const_logic_1;
    } else {
        relu4_0_V_we0 = ap_const_logic_0;
    }
}

void mnist_fp16::thread_relu5_0_V_address0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state320.read())) {
        relu5_0_V_address0 =  (sc_lv<10>) (tmp_860_cast_fu_12263_p1.read());
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state300.read())) {
        relu5_0_V_address0 =  (sc_lv<10>) (tmp_827_cast_reg_17777.read());
    } else {
        relu5_0_V_address0 =  (sc_lv<10>) ("XXXXXXXXXX");
    }
}

void mnist_fp16::thread_relu5_0_V_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state320.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state300.read()))) {
        relu5_0_V_ce0 = ap_const_logic_1;
    } else {
        relu5_0_V_ce0 = ap_const_logic_0;
    }
}

void mnist_fp16::thread_relu5_0_V_d0() {
    relu5_0_V_d0 = (!or_cond26_fu_11856_p2.read()[0].is_01())? sc_lv<16>(): ((or_cond26_fu_11856_p2.read()[0].to_bool())? newSel30_fu_11848_p3.read(): ap_const_lv16_0);
}

void mnist_fp16::thread_relu5_0_V_we0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state300.read())) {
        relu5_0_V_we0 = ap_const_logic_1;
    } else {
        relu5_0_V_we0 = ap_const_logic_0;
    }
}

void mnist_fp16::thread_relu6_0_V_address0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state362.read())) {
        relu6_0_V_address0 =  (sc_lv<10>) (tmp_879_cast_fu_14102_p1.read());
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state359.read())) {
        relu6_0_V_address0 =  (sc_lv<10>) (tmp_872_cast_reg_18376.read());
    } else {
        relu6_0_V_address0 =  (sc_lv<10>) ("XXXXXXXXXX");
    }
}

void mnist_fp16::thread_relu6_0_V_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state362.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state359.read()))) {
        relu6_0_V_ce0 = ap_const_logic_1;
    } else {
        relu6_0_V_ce0 = ap_const_logic_0;
    }
}

void mnist_fp16::thread_relu6_0_V_d0() {
    relu6_0_V_d0 = (!or_cond32_fu_13981_p2.read()[0].is_01())? sc_lv<16>(): ((or_cond32_fu_13981_p2.read()[0].to_bool())? newSel38_fu_13973_p3.read(): ap_const_lv16_0);
}

void mnist_fp16::thread_relu6_0_V_we0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state359.read())) {
        relu6_0_V_we0 = ap_const_logic_1;
    } else {
        relu6_0_V_we0 = ap_const_logic_0;
    }
}

void mnist_fp16::thread_result_V() {
    result_V = p_80_reg_2774.read();
}

void mnist_fp16::thread_result_V_ap_vld() {
    if ((esl_seteq<1,1,1>(tmp_422_fu_15115_p2.read(), ap_const_lv1_1) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state492.read()))) {
        result_V_ap_vld = ap_const_logic_1;
    } else {
        result_V_ap_vld = ap_const_logic_0;
    }
}

void mnist_fp16::thread_rhs_V_10_cast_fu_8154_p1() {
    rhs_V_10_cast_fu_8154_p1 = esl_sext<12,9>(r_V_13_fu_8148_p2.read());
}

void mnist_fp16::thread_rhs_V_12_cast_fu_10833_p1() {
    rhs_V_12_cast_fu_10833_p1 = esl_sext<10,8>(rhs_V_2_fu_10827_p2.read());
}

void mnist_fp16::thread_rhs_V_13_cast_fu_7516_p1() {
    rhs_V_13_cast_fu_7516_p1 = esl_zext<4,2>(p_46_reg_1866.read());
}

void mnist_fp16::thread_rhs_V_15_cast_fu_8928_p1() {
    rhs_V_15_cast_fu_8928_p1 = esl_zext<4,2>(p_66_reg_2099.read());
}

void mnist_fp16::thread_rhs_V_16_cast_fu_11976_p1() {
    rhs_V_16_cast_fu_11976_p1 = esl_sext<11,8>(rhs_V_4_fu_11970_p2.read());
}

void mnist_fp16::thread_rhs_V_17_cast_fu_8875_p1() {
    rhs_V_17_cast_fu_8875_p1 = esl_zext<4,2>(p_61_reg_2076.read());
}

void mnist_fp16::thread_rhs_V_18_cast_fu_11410_p1() {
    rhs_V_18_cast_fu_11410_p1 = esl_zext<4,2>(p_76_reg_2361.read());
}

void mnist_fp16::thread_rhs_V_19_cast_fu_11331_p1() {
    rhs_V_19_cast_fu_11331_p1 = esl_zext<4,2>(p_71_reg_2338.read());
}

void mnist_fp16::thread_rhs_V_20_cast_fu_12765_p1() {
    rhs_V_20_cast_fu_12765_p1 = esl_zext<4,2>(p_81_reg_2569.read());
}

void mnist_fp16::thread_rhs_V_21_cast_fu_12686_p1() {
    rhs_V_21_cast_fu_12686_p1 = esl_zext<4,2>(p_78_reg_2546.read());
}

void mnist_fp16::thread_rhs_V_2_cast_fu_3475_p1() {
    rhs_V_2_cast_fu_3475_p1 = esl_zext<5,2>(p_12_reg_1388.read());
}

void mnist_fp16::thread_rhs_V_2_fu_10827_p2() {
    rhs_V_2_fu_10827_p2 = (!tmp_181_cast_fu_10823_p1.read().is_01() || !lhs_V_10_cast1_fu_10788_p1.read().is_01())? sc_lv<8>(): (sc_biguint<8>(tmp_181_cast_fu_10823_p1.read()) - sc_biguint<8>(lhs_V_10_cast1_fu_10788_p1.read()));
}

void mnist_fp16::thread_rhs_V_3_cast_fu_5059_p1() {
    rhs_V_3_cast_fu_5059_p1 = esl_zext<5,2>(p_34_reg_1625.read());
}

void mnist_fp16::thread_rhs_V_4_cast_fu_6986_p1() {
    rhs_V_4_cast_fu_6986_p1 = esl_sext<11,9>(r_V_7_fu_6980_p2.read());
}

void mnist_fp16::thread_rhs_V_4_fu_11970_p2() {
    rhs_V_4_fu_11970_p2 = (!tmp_252_cast_fu_11966_p1.read().is_01() || !tmp_248_cast1_fu_11954_p1.read().is_01())? sc_lv<8>(): (sc_biguint<8>(tmp_252_cast_fu_11966_p1.read()) - sc_biguint<8>(tmp_248_cast1_fu_11954_p1.read()));
}

void mnist_fp16::thread_rhs_V_5_cast_fu_4171_p1() {
    rhs_V_5_cast_fu_4171_p1 = esl_sext<13,11>(r_V_5_fu_4165_p2.read());
}

void mnist_fp16::thread_rhs_V_7_cast_fu_4967_p1() {
    rhs_V_7_cast_fu_4967_p1 = esl_zext<5,2>(p_29_reg_1602.read());
}

void mnist_fp16::thread_rhs_V_8_cast_fu_7569_p1() {
    rhs_V_8_cast_fu_7569_p1 = esl_zext<4,2>(p_52_reg_1889.read());
}

void mnist_fp16::thread_rhs_V_cast_fu_3562_p1() {
    rhs_V_cast_fu_3562_p1 = esl_zext<5,2>(p_17_reg_1412.read());
}

void mnist_fp16::thread_rx1_V_fu_5053_p2() {
    rx1_V_fu_5053_p2 = (!p_34_reg_1625.read().is_01() || !ap_const_lv2_1.is_01())? sc_lv<2>(): (sc_biguint<2>(p_34_reg_1625.read()) + sc_biguint<2>(ap_const_lv2_1));
}

void mnist_fp16::thread_rx2_V_fu_7563_p2() {
    rx2_V_fu_7563_p2 = (!p_52_reg_1889.read().is_01() || !ap_const_lv2_1.is_01())? sc_lv<2>(): (sc_biguint<2>(p_52_reg_1889.read()) + sc_biguint<2>(ap_const_lv2_1));
}

void mnist_fp16::thread_rx3_V_fu_8922_p2() {
    rx3_V_fu_8922_p2 = (!p_66_reg_2099.read().is_01() || !ap_const_lv2_1.is_01())? sc_lv<2>(): (sc_biguint<2>(p_66_reg_2099.read()) + sc_biguint<2>(ap_const_lv2_1));
}

void mnist_fp16::thread_rx4_V_fu_11404_p2() {
    rx4_V_fu_11404_p2 = (!p_76_reg_2361.read().is_01() || !ap_const_lv2_1.is_01())? sc_lv<2>(): (sc_biguint<2>(p_76_reg_2361.read()) + sc_biguint<2>(ap_const_lv2_1));
}

void mnist_fp16::thread_rx5_V_fu_12759_p2() {
    rx5_V_fu_12759_p2 = (!p_81_reg_2569.read().is_01() || !ap_const_lv2_1.is_01())? sc_lv<2>(): (sc_biguint<2>(p_81_reg_2569.read()) + sc_biguint<2>(ap_const_lv2_1));
}

void mnist_fp16::thread_rx_V_fu_3556_p2() {
    rx_V_fu_3556_p2 = (!p_17_reg_1412.read().is_01() || !ap_const_lv2_1.is_01())? sc_lv<2>(): (sc_biguint<2>(p_17_reg_1412.read()) + sc_biguint<2>(ap_const_lv2_1));
}

void mnist_fp16::thread_ry1_V_fu_4961_p2() {
    ry1_V_fu_4961_p2 = (!p_29_reg_1602.read().is_01() || !ap_const_lv2_1.is_01())? sc_lv<2>(): (sc_biguint<2>(p_29_reg_1602.read()) + sc_biguint<2>(ap_const_lv2_1));
}

void mnist_fp16::thread_ry2_V_fu_7510_p2() {
    ry2_V_fu_7510_p2 = (!p_46_reg_1866.read().is_01() || !ap_const_lv2_1.is_01())? sc_lv<2>(): (sc_biguint<2>(p_46_reg_1866.read()) + sc_biguint<2>(ap_const_lv2_1));
}

void mnist_fp16::thread_ry3_V_fu_8869_p2() {
    ry3_V_fu_8869_p2 = (!p_61_reg_2076.read().is_01() || !ap_const_lv2_1.is_01())? sc_lv<2>(): (sc_biguint<2>(p_61_reg_2076.read()) + sc_biguint<2>(ap_const_lv2_1));
}

void mnist_fp16::thread_ry4_V_fu_11325_p2() {
    ry4_V_fu_11325_p2 = (!p_71_reg_2338.read().is_01() || !ap_const_lv2_1.is_01())? sc_lv<2>(): (sc_biguint<2>(p_71_reg_2338.read()) + sc_biguint<2>(ap_const_lv2_1));
}

void mnist_fp16::thread_ry5_V_fu_12680_p2() {
    ry5_V_fu_12680_p2 = (!p_78_reg_2546.read().is_01() || !ap_const_lv2_1.is_01())? sc_lv<2>(): (sc_biguint<2>(p_78_reg_2546.read()) + sc_biguint<2>(ap_const_lv2_1));
}

void mnist_fp16::thread_ry_V_fu_3469_p2() {
    ry_V_fu_3469_p2 = (!p_12_reg_1388.read().is_01() || !ap_const_lv2_1.is_01())? sc_lv<2>(): (sc_biguint<2>(p_12_reg_1388.read()) + sc_biguint<2>(ap_const_lv2_1));
}

void mnist_fp16::thread_sel_tmp100_fu_13090_p2() {
    sel_tmp100_fu_13090_p2 = (tmp_494_reg_18230.read() & sel_tmp99_fu_13085_p2.read());
}

void mnist_fp16::thread_sel_tmp101_fu_13099_p2() {
    sel_tmp101_fu_13099_p2 = (sel_tmp242_demorgan_fu_13095_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp16::thread_sel_tmp102_fu_13105_p2() {
    sel_tmp102_fu_13105_p2 = (tmp_491_reg_18218.read() & sel_tmp101_fu_13099_p2.read());
}

void mnist_fp16::thread_sel_tmp103_fu_13110_p2() {
    sel_tmp103_fu_13110_p2 = (tmp_512_fu_13047_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp16::thread_sel_tmp104_fu_13116_p2() {
    sel_tmp104_fu_13116_p2 = (sel_tmp102_fu_13105_p2.read() & sel_tmp103_fu_13110_p2.read());
}

void mnist_fp16::thread_sel_tmp105_fu_13122_p2() {
    sel_tmp105_fu_13122_p2 = (sel_tmp102_fu_13105_p2.read() & tmp_512_fu_13047_p2.read());
}

void mnist_fp16::thread_sel_tmp106_fu_13133_p2() {
    sel_tmp106_fu_13133_p2 = (sel_tmp257_demorgan_fu_13128_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp16::thread_sel_tmp107_fu_13139_p2() {
    sel_tmp107_fu_13139_p2 = (icmp21_reg_18242.read() & sel_tmp106_fu_13133_p2.read());
}

void mnist_fp16::thread_sel_tmp108_fu_13234_p2() {
    sel_tmp108_fu_13234_p2 = (tmp_539_reg_18207.read() ^ ap_const_lv1_1);
}

void mnist_fp16::thread_sel_tmp109_fu_13239_p2() {
    sel_tmp109_fu_13239_p2 = (tmp_544_reg_18264.read() & sel_tmp108_fu_13234_p2.read());
}

void mnist_fp16::thread_sel_tmp10_fu_6247_p2() {
    sel_tmp10_fu_6247_p2 = (tmp_126_reg_16251.read() & sel_tmp5_fu_6242_p2.read());
}

void mnist_fp16::thread_sel_tmp110_demorgan_fu_7897_p2() {
    sel_tmp110_demorgan_fu_7897_p2 = (tmp_212_reg_16730.read() | tmp_235_fu_7871_p2.read());
}

void mnist_fp16::thread_sel_tmp110_fu_13248_p2() {
    sel_tmp110_fu_13248_p2 = (sel_tmp266_demorgan_fu_13244_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp16::thread_sel_tmp111_fu_13254_p2() {
    sel_tmp111_fu_13254_p2 = (tmp_541_reg_18252.read() & sel_tmp110_fu_13248_p2.read());
}

void mnist_fp16::thread_sel_tmp112_fu_13259_p2() {
    sel_tmp112_fu_13259_p2 = (tmp_546_fu_13196_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp16::thread_sel_tmp113_fu_13265_p2() {
    sel_tmp113_fu_13265_p2 = (sel_tmp111_fu_13254_p2.read() & sel_tmp112_fu_13259_p2.read());
}

void mnist_fp16::thread_sel_tmp114_fu_13271_p2() {
    sel_tmp114_fu_13271_p2 = (sel_tmp111_fu_13254_p2.read() & tmp_546_fu_13196_p2.read());
}

void mnist_fp16::thread_sel_tmp115_fu_13282_p2() {
    sel_tmp115_fu_13282_p2 = (sel_tmp281_demorgan_fu_13277_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp16::thread_sel_tmp116_fu_13288_p2() {
    sel_tmp116_fu_13288_p2 = (icmp22_reg_18276.read() & sel_tmp115_fu_13282_p2.read());
}

void mnist_fp16::thread_sel_tmp11_fu_6171_p2() {
    sel_tmp11_fu_6171_p2 = (sel_tmp83_demorgan_fu_6166_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp16::thread_sel_tmp125_demorgan_fu_7914_p2() {
    sel_tmp125_demorgan_fu_7914_p2 = (sel_tmp110_demorgan_fu_7897_p2.read() | tmp_232_fu_7845_p2.read());
}

void mnist_fp16::thread_sel_tmp12_fu_6177_p2() {
    sel_tmp12_fu_6177_p2 = (tmp_123_fu_6114_p2.read() & sel_tmp11_fu_6171_p2.read());
}

void mnist_fp16::thread_sel_tmp137_demorgan_fu_9259_p2() {
    sel_tmp137_demorgan_fu_9259_p2 = (tmp_286_reg_17070.read() | tmp_311_reg_17115.read());
}

void mnist_fp16::thread_sel_tmp13_fu_6252_p2() {
    sel_tmp13_fu_6252_p2 = (tmp_141_fu_6204_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp16::thread_sel_tmp14_fu_6258_p2() {
    sel_tmp14_fu_6258_p2 = (sel_tmp12_reg_16262.read() & sel_tmp13_fu_6252_p2.read());
}

void mnist_fp16::thread_sel_tmp152_demorgan_fu_9292_p2() {
    sel_tmp152_demorgan_fu_9292_p2 = (sel_tmp137_demorgan_fu_9259_p2.read() | tmp_308_reg_17103.read());
}

void mnist_fp16::thread_sel_tmp15_fu_6263_p2() {
    sel_tmp15_fu_6263_p2 = (sel_tmp12_reg_16262.read() & tmp_141_fu_6204_p2.read());
}

void mnist_fp16::thread_sel_tmp161_demorgan_fu_9408_p2() {
    sel_tmp161_demorgan_fu_9408_p2 = (tmp_338_reg_17092.read() | tmp_342_reg_17149.read());
}

void mnist_fp16::thread_sel_tmp16_fu_6189_p2() {
    sel_tmp16_fu_6189_p2 = (sel_tmp98_demorgan_fu_6183_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp16::thread_sel_tmp176_demorgan_fu_9441_p2() {
    sel_tmp176_demorgan_fu_9441_p2 = (sel_tmp161_demorgan_fu_9408_p2.read() | tmp_339_reg_17137.read());
}

void mnist_fp16::thread_sel_tmp17_fu_6195_p2() {
    sel_tmp17_fu_6195_p2 = (icmp2_fu_6160_p2.read() & sel_tmp16_fu_6189_p2.read());
}

void mnist_fp16::thread_sel_tmp188_demorgan_fu_10035_p2() {
    sel_tmp188_demorgan_fu_10035_p2 = (tmp_279_reg_17314.read() | tmp_296_fu_10009_p2.read());
}

void mnist_fp16::thread_sel_tmp18_fu_5380_p2() {
    sel_tmp18_fu_5380_p2 = (tmp_108_reg_15990.read() ^ ap_const_lv1_1);
}

void mnist_fp16::thread_sel_tmp19_fu_5385_p2() {
    sel_tmp19_fu_5385_p2 = (tmp_133_reg_16035.read() & sel_tmp18_fu_5380_p2.read());
}

void mnist_fp16::thread_sel_tmp1_fu_3966_p2() {
    sel_tmp1_fu_3966_p2 = (tmp_39_reg_15647.read() ^ ap_const_lv1_1);
}

void mnist_fp16::thread_sel_tmp203_demorgan_fu_10052_p2() {
    sel_tmp203_demorgan_fu_10052_p2 = (sel_tmp188_demorgan_fu_10035_p2.read() | tmp_285_fu_9983_p2.read());
}

void mnist_fp16::thread_sel_tmp20_fu_5394_p2() {
    sel_tmp20_fu_5394_p2 = (sel_tmp32_demorgan_fu_5390_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp16::thread_sel_tmp215_demorgan_fu_11721_p2() {
    sel_tmp215_demorgan_fu_11721_p2 = (tmp_390_reg_17825.read() | tmp_409_fu_11695_p2.read());
}

void mnist_fp16::thread_sel_tmp21_demorgan_fu_3907_p2() {
    sel_tmp21_demorgan_fu_3907_p2 = (sel_tmp6_demorgan_fu_3890_p2.read() | tmp_43_fu_3838_p2.read());
}

void mnist_fp16::thread_sel_tmp21_fu_5400_p2() {
    sel_tmp21_fu_5400_p2 = (tmp_130_reg_16023.read() & sel_tmp20_fu_5394_p2.read());
}

void mnist_fp16::thread_sel_tmp22_fu_5405_p2() {
    sel_tmp22_fu_5405_p2 = (tmp_144_fu_5342_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp16::thread_sel_tmp230_demorgan_fu_11738_p2() {
    sel_tmp230_demorgan_fu_11738_p2 = (sel_tmp215_demorgan_fu_11721_p2.read() | tmp_401_fu_11669_p2.read());
}

void mnist_fp16::thread_sel_tmp23_fu_5411_p2() {
    sel_tmp23_fu_5411_p2 = (sel_tmp21_fu_5400_p2.read() & sel_tmp22_fu_5405_p2.read());
}

void mnist_fp16::thread_sel_tmp242_demorgan_fu_13095_p2() {
    sel_tmp242_demorgan_fu_13095_p2 = (tmp_478_reg_18185.read() | tmp_494_reg_18230.read());
}

void mnist_fp16::thread_sel_tmp24_fu_5417_p2() {
    sel_tmp24_fu_5417_p2 = (sel_tmp21_fu_5400_p2.read() & tmp_144_fu_5342_p2.read());
}

void mnist_fp16::thread_sel_tmp257_demorgan_fu_13128_p2() {
    sel_tmp257_demorgan_fu_13128_p2 = (sel_tmp242_demorgan_fu_13095_p2.read() | tmp_491_reg_18218.read());
}

void mnist_fp16::thread_sel_tmp25_fu_5428_p2() {
    sel_tmp25_fu_5428_p2 = (sel_tmp47_demorgan_fu_5423_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp16::thread_sel_tmp266_demorgan_fu_13244_p2() {
    sel_tmp266_demorgan_fu_13244_p2 = (tmp_539_reg_18207.read() | tmp_544_reg_18264.read());
}

void mnist_fp16::thread_sel_tmp26_fu_5434_p2() {
    sel_tmp26_fu_5434_p2 = (icmp4_reg_16047.read() & sel_tmp25_fu_5428_p2.read());
}

void mnist_fp16::thread_sel_tmp27_fu_5529_p2() {
    sel_tmp27_fu_5529_p2 = (tmp_172_reg_16012.read() ^ ap_const_lv1_1);
}

void mnist_fp16::thread_sel_tmp281_demorgan_fu_13277_p2() {
    sel_tmp281_demorgan_fu_13277_p2 = (sel_tmp266_demorgan_fu_13244_p2.read() | tmp_541_reg_18252.read());
}

void mnist_fp16::thread_sel_tmp28_fu_5534_p2() {
    sel_tmp28_fu_5534_p2 = (tmp_176_reg_16069.read() & sel_tmp27_fu_5529_p2.read());
}

void mnist_fp16::thread_sel_tmp293_demorgan_fu_13846_p2() {
    sel_tmp293_demorgan_fu_13846_p2 = (tmp_471_reg_18424.read() | tmp_481_fu_13820_p2.read());
}

void mnist_fp16::thread_sel_tmp29_fu_5543_p2() {
    sel_tmp29_fu_5543_p2 = (sel_tmp56_demorgan_fu_5539_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp16::thread_sel_tmp2_fu_3971_p2() {
    sel_tmp2_fu_3971_p2 = (tmp_52_reg_15664.read() & sel_tmp1_fu_3966_p2.read());
}

void mnist_fp16::thread_sel_tmp308_demorgan_fu_13863_p2() {
    sel_tmp308_demorgan_fu_13863_p2 = (sel_tmp293_demorgan_fu_13846_p2.read() | tmp_477_fu_13794_p2.read());
}

void mnist_fp16::thread_sel_tmp30_fu_5549_p2() {
    sel_tmp30_fu_5549_p2 = (tmp_173_reg_16057.read() & sel_tmp29_fu_5543_p2.read());
}

void mnist_fp16::thread_sel_tmp31_fu_5554_p2() {
    sel_tmp31_fu_5554_p2 = (tmp_178_fu_5491_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp16::thread_sel_tmp321_demorgan_fu_14539_p2() {
    sel_tmp321_demorgan_fu_14539_p2 = (tmp_436_fu_14476_p2.read() | tmp_447_fu_14513_p2.read());
}

void mnist_fp16::thread_sel_tmp32_demorgan_fu_5390_p2() {
    sel_tmp32_demorgan_fu_5390_p2 = (tmp_108_reg_15990.read() | tmp_133_reg_16035.read());
}

void mnist_fp16::thread_sel_tmp32_fu_5560_p2() {
    sel_tmp32_fu_5560_p2 = (sel_tmp30_fu_5549_p2.read() & sel_tmp31_fu_5554_p2.read());
}

void mnist_fp16::thread_sel_tmp336_demorgan_fu_14557_p2() {
    sel_tmp336_demorgan_fu_14557_p2 = (sel_tmp321_demorgan_fu_14539_p2.read() | tmp_437_fu_14487_p2.read());
}

void mnist_fp16::thread_sel_tmp33_fu_5566_p2() {
    sel_tmp33_fu_5566_p2 = (sel_tmp30_fu_5549_p2.read() & tmp_178_fu_5491_p2.read());
}

void mnist_fp16::thread_sel_tmp34_fu_5577_p2() {
    sel_tmp34_fu_5577_p2 = (sel_tmp71_demorgan_fu_5572_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp16::thread_sel_tmp35_fu_5583_p2() {
    sel_tmp35_fu_5583_p2 = (icmp5_reg_16081.read() & sel_tmp34_fu_5577_p2.read());
}

void mnist_fp16::thread_sel_tmp36_fu_7973_p2() {
    sel_tmp36_fu_7973_p2 = (tmp_212_reg_16730.read() ^ ap_const_lv1_1);
}

void mnist_fp16::thread_sel_tmp37_fu_7978_p2() {
    sel_tmp37_fu_7978_p2 = (tmp_235_reg_16747.read() & sel_tmp36_fu_7973_p2.read());
}

void mnist_fp16::thread_sel_tmp38_fu_7902_p2() {
    sel_tmp38_fu_7902_p2 = (sel_tmp110_demorgan_fu_7897_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp16::thread_sel_tmp39_fu_7908_p2() {
    sel_tmp39_fu_7908_p2 = (tmp_232_fu_7845_p2.read() & sel_tmp38_fu_7902_p2.read());
}

void mnist_fp16::thread_sel_tmp3_fu_3913_p2() {
    sel_tmp3_fu_3913_p2 = (sel_tmp21_demorgan_fu_3907_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp16::thread_sel_tmp40_fu_7983_p2() {
    sel_tmp40_fu_7983_p2 = (tmp_245_fu_7935_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp16::thread_sel_tmp41_fu_7989_p2() {
    sel_tmp41_fu_7989_p2 = (sel_tmp39_reg_16758.read() & sel_tmp40_fu_7983_p2.read());
}

void mnist_fp16::thread_sel_tmp42_fu_7994_p2() {
    sel_tmp42_fu_7994_p2 = (sel_tmp39_reg_16758.read() & tmp_245_fu_7935_p2.read());
}

void mnist_fp16::thread_sel_tmp43_fu_7920_p2() {
    sel_tmp43_fu_7920_p2 = (sel_tmp125_demorgan_fu_7914_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp16::thread_sel_tmp44_fu_7926_p2() {
    sel_tmp44_fu_7926_p2 = (icmp7_fu_7891_p2.read() & sel_tmp43_fu_7920_p2.read());
}

void mnist_fp16::thread_sel_tmp45_fu_10111_p2() {
    sel_tmp45_fu_10111_p2 = (tmp_279_reg_17314.read() ^ ap_const_lv1_1);
}

void mnist_fp16::thread_sel_tmp46_fu_10116_p2() {
    sel_tmp46_fu_10116_p2 = (tmp_296_reg_17331.read() & sel_tmp45_fu_10111_p2.read());
}

void mnist_fp16::thread_sel_tmp47_demorgan_fu_5423_p2() {
    sel_tmp47_demorgan_fu_5423_p2 = (sel_tmp32_demorgan_fu_5390_p2.read() | tmp_130_reg_16023.read());
}

void mnist_fp16::thread_sel_tmp47_fu_10040_p2() {
    sel_tmp47_fu_10040_p2 = (sel_tmp188_demorgan_fu_10035_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp16::thread_sel_tmp48_fu_10046_p2() {
    sel_tmp48_fu_10046_p2 = (tmp_285_fu_9983_p2.read() & sel_tmp47_fu_10040_p2.read());
}

void mnist_fp16::thread_sel_tmp49_fu_10121_p2() {
    sel_tmp49_fu_10121_p2 = (tmp_300_fu_10073_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp16::thread_sel_tmp4_fu_3919_p2() {
    sel_tmp4_fu_3919_p2 = (icmp_fu_3884_p2.read() & sel_tmp3_fu_3913_p2.read());
}

void mnist_fp16::thread_sel_tmp50_fu_10127_p2() {
    sel_tmp50_fu_10127_p2 = (sel_tmp48_reg_17342.read() & sel_tmp49_fu_10121_p2.read());
}

void mnist_fp16::thread_sel_tmp51_fu_10132_p2() {
    sel_tmp51_fu_10132_p2 = (sel_tmp48_reg_17342.read() & tmp_300_fu_10073_p2.read());
}

void mnist_fp16::thread_sel_tmp52_fu_10058_p2() {
    sel_tmp52_fu_10058_p2 = (sel_tmp203_demorgan_fu_10052_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp16::thread_sel_tmp53_fu_10064_p2() {
    sel_tmp53_fu_10064_p2 = (icmp9_fu_10029_p2.read() & sel_tmp52_fu_10058_p2.read());
}

void mnist_fp16::thread_sel_tmp54_fu_9249_p2() {
    sel_tmp54_fu_9249_p2 = (tmp_286_reg_17070.read() ^ ap_const_lv1_1);
}

void mnist_fp16::thread_sel_tmp55_fu_9254_p2() {
    sel_tmp55_fu_9254_p2 = (tmp_311_reg_17115.read() & sel_tmp54_fu_9249_p2.read());
}

void mnist_fp16::thread_sel_tmp56_demorgan_fu_5539_p2() {
    sel_tmp56_demorgan_fu_5539_p2 = (tmp_172_reg_16012.read() | tmp_176_reg_16069.read());
}

void mnist_fp16::thread_sel_tmp56_fu_9263_p2() {
    sel_tmp56_fu_9263_p2 = (sel_tmp137_demorgan_fu_9259_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp16::thread_sel_tmp57_fu_9269_p2() {
    sel_tmp57_fu_9269_p2 = (tmp_308_reg_17103.read() & sel_tmp56_fu_9263_p2.read());
}

void mnist_fp16::thread_sel_tmp58_fu_9274_p2() {
    sel_tmp58_fu_9274_p2 = (tmp_320_fu_9211_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp16::thread_sel_tmp59_fu_9280_p2() {
    sel_tmp59_fu_9280_p2 = (sel_tmp57_fu_9269_p2.read() & sel_tmp58_fu_9274_p2.read());
}

void mnist_fp16::thread_sel_tmp5_fu_6242_p2() {
    sel_tmp5_fu_6242_p2 = (tmp_101_reg_16234.read() ^ ap_const_lv1_1);
}

void mnist_fp16::thread_sel_tmp60_fu_9286_p2() {
    sel_tmp60_fu_9286_p2 = (sel_tmp57_fu_9269_p2.read() & tmp_320_fu_9211_p2.read());
}

void mnist_fp16::thread_sel_tmp61_fu_9297_p2() {
    sel_tmp61_fu_9297_p2 = (sel_tmp152_demorgan_fu_9292_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp16::thread_sel_tmp62_fu_9303_p2() {
    sel_tmp62_fu_9303_p2 = (icmp11_reg_17127.read() & sel_tmp61_fu_9297_p2.read());
}

void mnist_fp16::thread_sel_tmp63_fu_9398_p2() {
    sel_tmp63_fu_9398_p2 = (tmp_338_reg_17092.read() ^ ap_const_lv1_1);
}

void mnist_fp16::thread_sel_tmp64_fu_9403_p2() {
    sel_tmp64_fu_9403_p2 = (tmp_342_reg_17149.read() & sel_tmp63_fu_9398_p2.read());
}

void mnist_fp16::thread_sel_tmp65_fu_9412_p2() {
    sel_tmp65_fu_9412_p2 = (sel_tmp161_demorgan_fu_9408_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp16::thread_sel_tmp66_fu_9418_p2() {
    sel_tmp66_fu_9418_p2 = (tmp_339_reg_17137.read() & sel_tmp65_fu_9412_p2.read());
}

void mnist_fp16::thread_sel_tmp67_fu_9423_p2() {
    sel_tmp67_fu_9423_p2 = (tmp_344_fu_9360_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp16::thread_sel_tmp68_fu_9429_p2() {
    sel_tmp68_fu_9429_p2 = (sel_tmp66_fu_9418_p2.read() & sel_tmp67_fu_9423_p2.read());
}

void mnist_fp16::thread_sel_tmp69_fu_9435_p2() {
    sel_tmp69_fu_9435_p2 = (sel_tmp66_fu_9418_p2.read() & tmp_344_fu_9360_p2.read());
}

void mnist_fp16::thread_sel_tmp6_demorgan_fu_3890_p2() {
    sel_tmp6_demorgan_fu_3890_p2 = (tmp_39_reg_15647.read() | tmp_52_fu_3864_p2.read());
}

void mnist_fp16::thread_sel_tmp6_fu_3895_p2() {
    sel_tmp6_fu_3895_p2 = (sel_tmp6_demorgan_fu_3890_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp16::thread_sel_tmp70_fu_9446_p2() {
    sel_tmp70_fu_9446_p2 = (sel_tmp176_demorgan_fu_9441_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp16::thread_sel_tmp71_demorgan_fu_5572_p2() {
    sel_tmp71_demorgan_fu_5572_p2 = (sel_tmp56_demorgan_fu_5539_p2.read() | tmp_173_reg_16057.read());
}

void mnist_fp16::thread_sel_tmp71_fu_9452_p2() {
    sel_tmp71_fu_9452_p2 = (icmp12_reg_17161.read() & sel_tmp70_fu_9446_p2.read());
}

void mnist_fp16::thread_sel_tmp72_fu_11797_p2() {
    sel_tmp72_fu_11797_p2 = (tmp_390_reg_17825.read() ^ ap_const_lv1_1);
}

void mnist_fp16::thread_sel_tmp73_fu_11802_p2() {
    sel_tmp73_fu_11802_p2 = (tmp_409_reg_17842.read() & sel_tmp72_fu_11797_p2.read());
}

void mnist_fp16::thread_sel_tmp74_fu_11726_p2() {
    sel_tmp74_fu_11726_p2 = (sel_tmp215_demorgan_fu_11721_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp16::thread_sel_tmp75_fu_11732_p2() {
    sel_tmp75_fu_11732_p2 = (tmp_401_fu_11669_p2.read() & sel_tmp74_fu_11726_p2.read());
}

void mnist_fp16::thread_sel_tmp76_fu_11807_p2() {
    sel_tmp76_fu_11807_p2 = (tmp_411_fu_11759_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp16::thread_sel_tmp77_fu_11813_p2() {
    sel_tmp77_fu_11813_p2 = (sel_tmp75_reg_17853.read() & sel_tmp76_fu_11807_p2.read());
}

void mnist_fp16::thread_sel_tmp78_fu_11818_p2() {
    sel_tmp78_fu_11818_p2 = (sel_tmp75_reg_17853.read() & tmp_411_fu_11759_p2.read());
}

void mnist_fp16::thread_sel_tmp79_fu_11744_p2() {
    sel_tmp79_fu_11744_p2 = (sel_tmp230_demorgan_fu_11738_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp16::thread_sel_tmp7_fu_3901_p2() {
    sel_tmp7_fu_3901_p2 = (tmp_43_fu_3838_p2.read() & sel_tmp6_fu_3895_p2.read());
}

void mnist_fp16::thread_sel_tmp80_fu_11750_p2() {
    sel_tmp80_fu_11750_p2 = (icmp14_fu_11715_p2.read() & sel_tmp79_fu_11744_p2.read());
}

void mnist_fp16::thread_sel_tmp81_fu_14616_p2() {
    sel_tmp81_fu_14616_p2 = (tmp_436_reg_18622.read() ^ ap_const_lv1_1);
}

void mnist_fp16::thread_sel_tmp82_fu_14621_p2() {
    sel_tmp82_fu_14621_p2 = (tmp_447_reg_18633.read() & sel_tmp81_fu_14616_p2.read());
}

void mnist_fp16::thread_sel_tmp83_demorgan_fu_6166_p2() {
    sel_tmp83_demorgan_fu_6166_p2 = (tmp_101_reg_16234.read() | tmp_126_fu_6140_p2.read());
}

void mnist_fp16::thread_sel_tmp83_fu_14545_p2() {
    sel_tmp83_fu_14545_p2 = (sel_tmp321_demorgan_fu_14539_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp16::thread_sel_tmp84_fu_14551_p2() {
    sel_tmp84_fu_14551_p2 = (tmp_437_fu_14487_p2.read() & sel_tmp83_fu_14545_p2.read());
}

void mnist_fp16::thread_sel_tmp85_fu_14626_p2() {
    sel_tmp85_fu_14626_p2 = (tmp_449_fu_14578_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp16::thread_sel_tmp86_fu_14632_p2() {
    sel_tmp86_fu_14632_p2 = (sel_tmp84_reg_18644.read() & sel_tmp85_fu_14626_p2.read());
}

void mnist_fp16::thread_sel_tmp87_fu_14637_p2() {
    sel_tmp87_fu_14637_p2 = (sel_tmp84_reg_18644.read() & tmp_449_fu_14578_p2.read());
}

void mnist_fp16::thread_sel_tmp88_fu_14563_p2() {
    sel_tmp88_fu_14563_p2 = (sel_tmp336_demorgan_fu_14557_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp16::thread_sel_tmp89_fu_14569_p2() {
    sel_tmp89_fu_14569_p2 = (icmp16_fu_14533_p2.read() & sel_tmp88_fu_14563_p2.read());
}

void mnist_fp16::thread_sel_tmp8_fu_3976_p2() {
    sel_tmp8_fu_3976_p2 = (tmp_54_fu_3928_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp16::thread_sel_tmp90_fu_13922_p2() {
    sel_tmp90_fu_13922_p2 = (tmp_471_reg_18424.read() ^ ap_const_lv1_1);
}

void mnist_fp16::thread_sel_tmp91_fu_13927_p2() {
    sel_tmp91_fu_13927_p2 = (tmp_481_reg_18441.read() & sel_tmp90_fu_13922_p2.read());
}

void mnist_fp16::thread_sel_tmp92_fu_13851_p2() {
    sel_tmp92_fu_13851_p2 = (sel_tmp293_demorgan_fu_13846_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp16::thread_sel_tmp93_fu_13857_p2() {
    sel_tmp93_fu_13857_p2 = (tmp_477_fu_13794_p2.read() & sel_tmp92_fu_13851_p2.read());
}

void mnist_fp16::thread_sel_tmp94_fu_13932_p2() {
    sel_tmp94_fu_13932_p2 = (tmp_484_fu_13884_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp16::thread_sel_tmp95_fu_13938_p2() {
    sel_tmp95_fu_13938_p2 = (sel_tmp93_reg_18452.read() & sel_tmp94_fu_13932_p2.read());
}

void mnist_fp16::thread_sel_tmp96_fu_13943_p2() {
    sel_tmp96_fu_13943_p2 = (sel_tmp93_reg_18452.read() & tmp_484_fu_13884_p2.read());
}

void mnist_fp16::thread_sel_tmp97_fu_13869_p2() {
    sel_tmp97_fu_13869_p2 = (sel_tmp308_demorgan_fu_13863_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp16::thread_sel_tmp98_demorgan_fu_6183_p2() {
    sel_tmp98_demorgan_fu_6183_p2 = (sel_tmp83_demorgan_fu_6166_p2.read() | tmp_123_fu_6114_p2.read());
}

void mnist_fp16::thread_sel_tmp98_fu_13875_p2() {
    sel_tmp98_fu_13875_p2 = (icmp17_fu_13840_p2.read() & sel_tmp97_fu_13869_p2.read());
}

void mnist_fp16::thread_sel_tmp99_fu_13085_p2() {
    sel_tmp99_fu_13085_p2 = (tmp_478_reg_18185.read() ^ ap_const_lv1_1);
}

void mnist_fp16::thread_sel_tmp9_fu_3982_p2() {
    sel_tmp9_fu_3982_p2 = (sel_tmp7_reg_15675.read() & sel_tmp8_fu_3976_p2.read());
}

void mnist_fp16::thread_sel_tmp_fu_3987_p2() {
    sel_tmp_fu_3987_p2 = (sel_tmp7_reg_15675.read() & tmp_54_fu_3928_p2.read());
}

void mnist_fp16::thread_sext1_cast_fu_4328_p1() {
    sext1_cast_fu_4328_p1 = esl_sext<28,13>(r_V_17_tr_fu_4323_p2.read());
}

void mnist_fp16::thread_sext3_cast_fu_7094_p1() {
    sext3_cast_fu_7094_p1 = esl_sext<24,11>(r_V_31_tr_fu_7089_p2.read());
}

void mnist_fp16::thread_sext5_cast_fu_8262_p1() {
    sext5_cast_fu_8262_p1 = esl_sext<26,12>(r_V_85_tr_fu_8257_p2.read());
}

void mnist_fp16::thread_sext7_cast_fu_10932_p1() {
    sext7_cast_fu_10932_p1 = esl_sext<22,10>(r_V_93_tr_reg_17566.read());
}

void mnist_fp16::thread_sext9_cast_fu_12075_p1() {
    sext9_cast_fu_12075_p1 = esl_sext<24,11>(r_V_100_tr_reg_17927.read());
}

void mnist_fp16::thread_sh_amt_10_cast_fu_13044_p1() {
    sh_amt_10_cast_fu_13044_p1 = esl_sext<32,12>(sh_amt_10_reg_18224.read());
}

void mnist_fp16::thread_sh_amt_10_fu_12925_p3() {
    sh_amt_10_fu_12925_p3 = (!tmp_491_fu_12907_p2.read()[0].is_01())? sc_lv<12>(): ((tmp_491_fu_12907_p2.read()[0].to_bool())? tmp_492_fu_12913_p2.read(): tmp_493_fu_12919_p2.read());
}

void mnist_fp16::thread_sh_amt_11_cast_fu_13193_p1() {
    sh_amt_11_cast_fu_13193_p1 = esl_sext<32,12>(sh_amt_11_reg_18258.read());
}

void mnist_fp16::thread_sh_amt_11_fu_13010_p3() {
    sh_amt_11_fu_13010_p3 = (!tmp_541_fu_12992_p2.read()[0].is_01())? sc_lv<12>(): ((tmp_541_fu_12992_p2.read()[0].to_bool())? tmp_542_fu_12998_p2.read(): tmp_543_fu_13004_p2.read());
}

void mnist_fp16::thread_sh_amt_1_cast_fu_6201_p1() {
    sh_amt_1_cast_fu_6201_p1 = esl_sext<32,12>(sh_amt_1_reg_16245.read());
}

void mnist_fp16::thread_sh_amt_1_fu_6132_p3() {
    sh_amt_1_fu_6132_p3 = (!tmp_123_fu_6114_p2.read()[0].is_01())? sc_lv<12>(): ((tmp_123_fu_6114_p2.read()[0].to_bool())? tmp_124_fu_6120_p2.read(): tmp_125_fu_6126_p2.read());
}

void mnist_fp16::thread_sh_amt_2_cast_fu_5339_p1() {
    sh_amt_2_cast_fu_5339_p1 = esl_sext<32,12>(sh_amt_2_reg_16029.read());
}

void mnist_fp16::thread_sh_amt_2_fu_5220_p3() {
    sh_amt_2_fu_5220_p3 = (!tmp_130_fu_5202_p2.read()[0].is_01())? sc_lv<12>(): ((tmp_130_fu_5202_p2.read()[0].to_bool())? tmp_131_fu_5208_p2.read(): tmp_132_fu_5214_p2.read());
}

void mnist_fp16::thread_sh_amt_3_cast_fu_5488_p1() {
    sh_amt_3_cast_fu_5488_p1 = esl_sext<32,12>(sh_amt_3_reg_16063.read());
}

void mnist_fp16::thread_sh_amt_3_fu_5305_p3() {
    sh_amt_3_fu_5305_p3 = (!tmp_173_fu_5287_p2.read()[0].is_01())? sc_lv<12>(): ((tmp_173_fu_5287_p2.read()[0].to_bool())? tmp_174_fu_5293_p2.read(): tmp_175_fu_5299_p2.read());
}

void mnist_fp16::thread_sh_amt_4_cast_fu_7932_p1() {
    sh_amt_4_cast_fu_7932_p1 = esl_sext<32,12>(sh_amt_4_reg_16741.read());
}

void mnist_fp16::thread_sh_amt_4_fu_7863_p3() {
    sh_amt_4_fu_7863_p3 = (!tmp_232_fu_7845_p2.read()[0].is_01())? sc_lv<12>(): ((tmp_232_fu_7845_p2.read()[0].to_bool())? tmp_233_fu_7851_p2.read(): tmp_234_fu_7857_p2.read());
}

void mnist_fp16::thread_sh_amt_5_cast_fu_10070_p1() {
    sh_amt_5_cast_fu_10070_p1 = esl_sext<32,12>(sh_amt_5_reg_17325.read());
}

void mnist_fp16::thread_sh_amt_5_fu_10001_p3() {
    sh_amt_5_fu_10001_p3 = (!tmp_285_fu_9983_p2.read()[0].is_01())? sc_lv<12>(): ((tmp_285_fu_9983_p2.read()[0].to_bool())? tmp_288_fu_9989_p2.read(): tmp_289_fu_9995_p2.read());
}

void mnist_fp16::thread_sh_amt_6_cast_fu_9208_p1() {
    sh_amt_6_cast_fu_9208_p1 = esl_sext<32,12>(sh_amt_6_reg_17109.read());
}

void mnist_fp16::thread_sh_amt_6_fu_9089_p3() {
    sh_amt_6_fu_9089_p3 = (!tmp_308_fu_9071_p2.read()[0].is_01())? sc_lv<12>(): ((tmp_308_fu_9071_p2.read()[0].to_bool())? tmp_309_fu_9077_p2.read(): tmp_310_fu_9083_p2.read());
}

void mnist_fp16::thread_sh_amt_7_cast_fu_9357_p1() {
    sh_amt_7_cast_fu_9357_p1 = esl_sext<32,12>(sh_amt_7_reg_17143.read());
}

void mnist_fp16::thread_sh_amt_7_fu_9174_p3() {
    sh_amt_7_fu_9174_p3 = (!tmp_339_fu_9156_p2.read()[0].is_01())? sc_lv<12>(): ((tmp_339_fu_9156_p2.read()[0].to_bool())? tmp_340_fu_9162_p2.read(): tmp_341_fu_9168_p2.read());
}

void mnist_fp16::thread_sh_amt_8_cast_fu_11756_p1() {
    sh_amt_8_cast_fu_11756_p1 = esl_sext<32,12>(sh_amt_8_reg_17836.read());
}

void mnist_fp16::thread_sh_amt_8_fu_11687_p3() {
    sh_amt_8_fu_11687_p3 = (!tmp_401_fu_11669_p2.read()[0].is_01())? sc_lv<12>(): ((tmp_401_fu_11669_p2.read()[0].to_bool())? tmp_403_fu_11675_p2.read(): tmp_406_fu_11681_p2.read());
}

void mnist_fp16::thread_sh_amt_9_cast_fu_14575_p1() {
    sh_amt_9_cast_fu_14575_p1 = esl_sext<32,12>(sh_amt_9_reg_18627.read());
}

void mnist_fp16::thread_sh_amt_9_fu_14505_p3() {
    sh_amt_9_fu_14505_p3 = (!tmp_437_fu_14487_p2.read()[0].is_01())? sc_lv<12>(): ((tmp_437_fu_14487_p2.read()[0].to_bool())? tmp_440_fu_14493_p2.read(): tmp_446_fu_14499_p2.read());
}

void mnist_fp16::thread_sh_amt_cast_47_fu_13881_p1() {
    sh_amt_cast_47_fu_13881_p1 = esl_sext<32,12>(sh_amt_s_reg_18435.read());
}

void mnist_fp16::thread_sh_amt_cast_fu_3925_p1() {
    sh_amt_cast_fu_3925_p1 = esl_sext<32,12>(sh_amt_reg_15658.read());
}

void mnist_fp16::thread_sh_amt_fu_3856_p3() {
    sh_amt_fu_3856_p3 = (!tmp_43_fu_3838_p2.read()[0].is_01())? sc_lv<12>(): ((tmp_43_fu_3838_p2.read()[0].to_bool())? tmp_45_fu_3844_p2.read(): tmp_46_fu_3850_p2.read());
}

void mnist_fp16::thread_sh_amt_s_fu_13812_p3() {
    sh_amt_s_fu_13812_p3 = (!tmp_477_fu_13794_p2.read()[0].is_01())? sc_lv<12>(): ((tmp_477_fu_13794_p2.read()[0].to_bool())? tmp_479_fu_13800_p2.read(): tmp_480_fu_13806_p2.read());
}

void mnist_fp16::thread_storemerge10_fu_13902_p3() {
    storemerge10_fu_13902_p3 = (!tmp_872_reg_18408.read()[0].is_01())? sc_lv<16>(): ((tmp_872_reg_18408.read()[0].to_bool())? ap_const_lv16_FFFF: ap_const_lv16_0);
}

void mnist_fp16::thread_storemerge11_fu_13065_p3() {
    storemerge11_fu_13065_p3 = (!isneg_4_reg_18169.read()[0].is_01())? sc_lv<16>(): ((isneg_4_reg_18169.read()[0].to_bool())? ap_const_lv16_FFFF: ap_const_lv16_0);
}

void mnist_fp16::thread_storemerge12_fu_13214_p3() {
    storemerge12_fu_13214_p3 = (!isneg_5_reg_18191.read()[0].is_01())? sc_lv<16>(): ((isneg_5_reg_18191.read()[0].to_bool())? ap_const_lv16_FFFF: ap_const_lv16_0);
}

void mnist_fp16::thread_storemerge1_fu_9229_p3() {
    storemerge1_fu_9229_p3 = (!isneg_2_reg_17054.read()[0].is_01())? sc_lv<16>(): ((isneg_2_reg_17054.read()[0].to_bool())? ap_const_lv16_FFFF: ap_const_lv16_0);
}

void mnist_fp16::thread_storemerge2_fu_6222_p3() {
    storemerge2_fu_6222_p3 = (!tmp_362_reg_16218.read()[0].is_01())? sc_lv<16>(): ((tmp_362_reg_16218.read()[0].to_bool())? ap_const_lv16_FFFF: ap_const_lv16_0);
}

void mnist_fp16::thread_storemerge3_fu_9378_p3() {
    storemerge3_fu_9378_p3 = (!isneg_3_reg_17076.read()[0].is_01())? sc_lv<16>(): ((isneg_3_reg_17076.read()[0].to_bool())? ap_const_lv16_FFFF: ap_const_lv16_0);
}

void mnist_fp16::thread_storemerge4_fu_5360_p3() {
    storemerge4_fu_5360_p3 = (!isneg_reg_15974.read()[0].is_01())? sc_lv<16>(): ((isneg_reg_15974.read()[0].to_bool())? ap_const_lv16_FFFF: ap_const_lv16_0);
}

void mnist_fp16::thread_storemerge5_fu_10091_p3() {
    storemerge5_fu_10091_p3 = (!tmp_699_reg_17298.read()[0].is_01())? sc_lv<16>(): ((tmp_699_reg_17298.read()[0].to_bool())? ap_const_lv16_FFFF: ap_const_lv16_0);
}

void mnist_fp16::thread_storemerge6_fu_5509_p3() {
    storemerge6_fu_5509_p3 = (!isneg_1_reg_15996.read()[0].is_01())? sc_lv<16>(): ((isneg_1_reg_15996.read()[0].to_bool())? ap_const_lv16_FFFF: ap_const_lv16_0);
}

void mnist_fp16::thread_storemerge7_fu_11777_p3() {
    storemerge7_fu_11777_p3 = (!tmp_798_reg_17809.read()[0].is_01())? sc_lv<16>(): ((tmp_798_reg_17809.read()[0].to_bool())? ap_const_lv16_FFFF: ap_const_lv16_0);
}

void mnist_fp16::thread_storemerge8_fu_7953_p3() {
    storemerge8_fu_7953_p3 = (!tmp_624_reg_16714.read()[0].is_01())? sc_lv<16>(): ((tmp_624_reg_16714.read()[0].to_bool())? ap_const_lv16_FFFF: ap_const_lv16_0);
}

void mnist_fp16::thread_storemerge9_fu_14596_p3() {
    storemerge9_fu_14596_p3 = (!tmp_860_reg_18601.read()[0].is_01())? sc_lv<16>(): ((tmp_860_reg_18601.read()[0].to_bool())? ap_const_lv16_FFFF: ap_const_lv16_0);
}

void mnist_fp16::thread_storemerge_fu_3946_p3() {
    storemerge_fu_3946_p3 = (!tmp_142_reg_15631.read()[0].is_01())? sc_lv<16>(): ((tmp_142_reg_15631.read()[0].to_bool())? ap_const_lv16_FFFF: ap_const_lv16_0);
}

void mnist_fp16::thread_stream_in_TDATA_blk_n() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state2.read()) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage0.read()) && 
          esl_seteq<1,1,1>(ap_enable_reg_pp1_iter1.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_block_pp1_stage0.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, tmp_2_reg_15378.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, brmerge_reg_15387.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp2_stage0.read()) && 
          esl_seteq<1,1,1>(ap_enable_reg_pp2_iter1.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_block_pp2_stage0.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, eol_2_reg_1279.read())))) {
        stream_in_TDATA_blk_n = stream_in_V_data_V_0_state.read()[0];
    } else {
        stream_in_TDATA_blk_n = ap_const_logic_1;
    }
}

void mnist_fp16::thread_stream_in_TREADY() {
    stream_in_TREADY = stream_in_V_dest_V_0_state.read()[1];
}

void mnist_fp16::thread_stream_in_V_data_V_0_ack_in() {
    stream_in_V_data_V_0_ack_in = stream_in_V_data_V_0_state.read()[1];
}

void mnist_fp16::thread_stream_in_V_data_V_0_ack_out() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state2.read()) && 
          esl_seteq<1,1,1>(stream_in_V_data_V_0_vld_out.read(), ap_const_logic_1)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp2_stage0.read()) && 
          esl_seteq<1,1,1>(ap_enable_reg_pp2_iter1.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, eol_2_reg_1279.read()) && 
          esl_seteq<1,1,1>(ap_block_pp2_stage0_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage0.read()) && 
          esl_seteq<1,1,1>(ap_enable_reg_pp1_iter1.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op586_read_state6.read()) && 
          esl_seteq<1,1,1>(ap_block_pp1_stage0_11001.read(), ap_const_boolean_0)))) {
        stream_in_V_data_V_0_ack_out = ap_const_logic_1;
    } else {
        stream_in_V_data_V_0_ack_out = ap_const_logic_0;
    }
}

void mnist_fp16::thread_stream_in_V_data_V_0_data_out() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, stream_in_V_data_V_0_sel.read())) {
        stream_in_V_data_V_0_data_out = stream_in_V_data_V_0_payload_B.read();
    } else {
        stream_in_V_data_V_0_data_out = stream_in_V_data_V_0_payload_A.read();
    }
}

void mnist_fp16::thread_stream_in_V_data_V_0_load_A() {
    stream_in_V_data_V_0_load_A = (stream_in_V_data_V_0_state_cmp_full.read() & ~stream_in_V_data_V_0_sel_wr.read());
}

void mnist_fp16::thread_stream_in_V_data_V_0_load_B() {
    stream_in_V_data_V_0_load_B = (stream_in_V_data_V_0_sel_wr.read() & stream_in_V_data_V_0_state_cmp_full.read());
}

void mnist_fp16::thread_stream_in_V_data_V_0_sel() {
    stream_in_V_data_V_0_sel = stream_in_V_data_V_0_sel_rd.read();
}

void mnist_fp16::thread_stream_in_V_data_V_0_state_cmp_full() {
    stream_in_V_data_V_0_state_cmp_full =  (sc_logic) ((!stream_in_V_data_V_0_state.read().is_01() || !ap_const_lv2_1.is_01())? sc_lv<1>(): sc_lv<1>(stream_in_V_data_V_0_state.read() != ap_const_lv2_1))[0];
}

void mnist_fp16::thread_stream_in_V_data_V_0_vld_in() {
    stream_in_V_data_V_0_vld_in = stream_in_TVALID.read();
}

void mnist_fp16::thread_stream_in_V_data_V_0_vld_out() {
    stream_in_V_data_V_0_vld_out = stream_in_V_data_V_0_state.read()[0];
}

void mnist_fp16::thread_stream_in_V_dest_V_0_ack_out() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state2.read()) && 
          esl_seteq<1,1,1>(stream_in_V_data_V_0_vld_out.read(), ap_const_logic_1)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp2_stage0.read()) && 
          esl_seteq<1,1,1>(ap_enable_reg_pp2_iter1.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, eol_2_reg_1279.read()) && 
          esl_seteq<1,1,1>(ap_block_pp2_stage0_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage0.read()) && 
          esl_seteq<1,1,1>(ap_enable_reg_pp1_iter1.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op586_read_state6.read()) && 
          esl_seteq<1,1,1>(ap_block_pp1_stage0_11001.read(), ap_const_boolean_0)))) {
        stream_in_V_dest_V_0_ack_out = ap_const_logic_1;
    } else {
        stream_in_V_dest_V_0_ack_out = ap_const_logic_0;
    }
}

void mnist_fp16::thread_stream_in_V_dest_V_0_vld_in() {
    stream_in_V_dest_V_0_vld_in = stream_in_TVALID.read();
}

void mnist_fp16::thread_stream_in_V_last_V_0_ack_in() {
    stream_in_V_last_V_0_ack_in = stream_in_V_last_V_0_state.read()[1];
}

void mnist_fp16::thread_stream_in_V_last_V_0_ack_out() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state2.read()) && 
          esl_seteq<1,1,1>(stream_in_V_data_V_0_vld_out.read(), ap_const_logic_1)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp2_stage0.read()) && 
          esl_seteq<1,1,1>(ap_enable_reg_pp2_iter1.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, eol_2_reg_1279.read()) && 
          esl_seteq<1,1,1>(ap_block_pp2_stage0_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage0.read()) && 
          esl_seteq<1,1,1>(ap_enable_reg_pp1_iter1.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op586_read_state6.read()) && 
          esl_seteq<1,1,1>(ap_block_pp1_stage0_11001.read(), ap_const_boolean_0)))) {
        stream_in_V_last_V_0_ack_out = ap_const_logic_1;
    } else {
        stream_in_V_last_V_0_ack_out = ap_const_logic_0;
    }
}

void mnist_fp16::thread_stream_in_V_last_V_0_data_out() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, stream_in_V_last_V_0_sel.read())) {
        stream_in_V_last_V_0_data_out = stream_in_V_last_V_0_payload_B.read();
    } else {
        stream_in_V_last_V_0_data_out = stream_in_V_last_V_0_payload_A.read();
    }
}

void mnist_fp16::thread_stream_in_V_last_V_0_load_A() {
    stream_in_V_last_V_0_load_A = (stream_in_V_last_V_0_state_cmp_full.read() & ~stream_in_V_last_V_0_sel_wr.read());
}

void mnist_fp16::thread_stream_in_V_last_V_0_load_B() {
    stream_in_V_last_V_0_load_B = (stream_in_V_last_V_0_sel_wr.read() & stream_in_V_last_V_0_state_cmp_full.read());
}

void mnist_fp16::thread_stream_in_V_last_V_0_sel() {
    stream_in_V_last_V_0_sel = stream_in_V_last_V_0_sel_rd.read();
}

void mnist_fp16::thread_stream_in_V_last_V_0_state_cmp_full() {
    stream_in_V_last_V_0_state_cmp_full =  (sc_logic) ((!stream_in_V_last_V_0_state.read().is_01() || !ap_const_lv2_1.is_01())? sc_lv<1>(): sc_lv<1>(stream_in_V_last_V_0_state.read() != ap_const_lv2_1))[0];
}

void mnist_fp16::thread_stream_in_V_last_V_0_vld_in() {
    stream_in_V_last_V_0_vld_in = stream_in_TVALID.read();
}

void mnist_fp16::thread_stream_in_V_last_V_0_vld_out() {
    stream_in_V_last_V_0_vld_out = stream_in_V_last_V_0_state.read()[0];
}

void mnist_fp16::thread_stream_in_V_user_V_0_ack_in() {
    stream_in_V_user_V_0_ack_in = stream_in_V_user_V_0_state.read()[1];
}

void mnist_fp16::thread_stream_in_V_user_V_0_ack_out() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state2.read()) && 
          esl_seteq<1,1,1>(stream_in_V_data_V_0_vld_out.read(), ap_const_logic_1)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp2_stage0.read()) && 
          esl_seteq<1,1,1>(ap_enable_reg_pp2_iter1.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, eol_2_reg_1279.read()) && 
          esl_seteq<1,1,1>(ap_block_pp2_stage0_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage0.read()) && 
          esl_seteq<1,1,1>(ap_enable_reg_pp1_iter1.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op586_read_state6.read()) && 
          esl_seteq<1,1,1>(ap_block_pp1_stage0_11001.read(), ap_const_boolean_0)))) {
        stream_in_V_user_V_0_ack_out = ap_const_logic_1;
    } else {
        stream_in_V_user_V_0_ack_out = ap_const_logic_0;
    }
}

void mnist_fp16::thread_stream_in_V_user_V_0_data_out() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, stream_in_V_user_V_0_sel.read())) {
        stream_in_V_user_V_0_data_out = stream_in_V_user_V_0_payload_B.read();
    } else {
        stream_in_V_user_V_0_data_out = stream_in_V_user_V_0_payload_A.read();
    }
}

void mnist_fp16::thread_stream_in_V_user_V_0_load_A() {
    stream_in_V_user_V_0_load_A = (stream_in_V_user_V_0_state_cmp_full.read() & ~stream_in_V_user_V_0_sel_wr.read());
}

void mnist_fp16::thread_stream_in_V_user_V_0_load_B() {
    stream_in_V_user_V_0_load_B = (stream_in_V_user_V_0_sel_wr.read() & stream_in_V_user_V_0_state_cmp_full.read());
}

void mnist_fp16::thread_stream_in_V_user_V_0_sel() {
    stream_in_V_user_V_0_sel = stream_in_V_user_V_0_sel_rd.read();
}

void mnist_fp16::thread_stream_in_V_user_V_0_state_cmp_full() {
    stream_in_V_user_V_0_state_cmp_full =  (sc_logic) ((!stream_in_V_user_V_0_state.read().is_01() || !ap_const_lv2_1.is_01())? sc_lv<1>(): sc_lv<1>(stream_in_V_user_V_0_state.read() != ap_const_lv2_1))[0];
}

void mnist_fp16::thread_stream_in_V_user_V_0_vld_in() {
    stream_in_V_user_V_0_vld_in = stream_in_TVALID.read();
}

void mnist_fp16::thread_stream_in_V_user_V_0_vld_out() {
    stream_in_V_user_V_0_vld_out = stream_in_V_user_V_0_state.read()[0];
}

void mnist_fp16::thread_tmp136_fu_6990_p2() {
    tmp136_fu_6990_p2 = (tmp_56_fu_6925_p2.read() & tmp_57_fu_6931_p2.read());
}

void mnist_fp16::thread_tmp137_fu_7025_p2() {
    tmp137_fu_7025_p2 = (tmp_70_fu_7013_p2.read() & tmp_71_fu_7019_p2.read());
}

void mnist_fp16::thread_tmp138_fu_6996_p2() {
    tmp138_fu_6996_p2 = (!rhs_V_4_cast_fu_6986_p1.read().is_01() || !phi_mul191_cast_reg_16424.read().is_01())? sc_lv<11>(): (sc_bigint<11>(rhs_V_4_cast_fu_6986_p1.read()) + sc_biguint<11>(phi_mul191_cast_reg_16424.read()));
}

void mnist_fp16::thread_tmp139_fu_7080_p2() {
    tmp139_fu_7080_p2 = (!ap_const_lv6_31.is_01() || !lhs_V_18_cast_fu_7076_p1.read().is_01())? sc_lv<6>(): (sc_bigint<6>(ap_const_lv6_31) + sc_bigint<6>(lhs_V_18_cast_fu_7076_p1.read()));
}

void mnist_fp16::thread_tmp1_fu_3075_p2() {
    tmp1_fu_3075_p2 = (!r_V_1_fu_3069_p2.read().is_01() || !ap_const_lv11_7E3.is_01())? sc_lv<11>(): (sc_biguint<11>(r_V_1_fu_3069_p2.read()) + sc_bigint<11>(ap_const_lv11_7E3));
}

void mnist_fp16::thread_tmp244_fu_8158_p2() {
    tmp244_fu_8158_p2 = (tmp_85_fu_8093_p2.read() & tmp_86_fu_8099_p2.read());
}

void mnist_fp16::thread_tmp245_fu_8193_p2() {
    tmp245_fu_8193_p2 = (tmp_117_fu_8181_p2.read() & tmp_118_fu_8187_p2.read());
}

void mnist_fp16::thread_tmp246_fu_8164_p2() {
    tmp246_fu_8164_p2 = (!rhs_V_10_cast_fu_8154_p1.read().is_01() || !phi_mul193_cast_reg_16770.read().is_01())? sc_lv<12>(): (sc_bigint<12>(rhs_V_10_cast_fu_8154_p1.read()) + sc_biguint<12>(phi_mul193_cast_reg_16770.read()));
}

void mnist_fp16::thread_tmp247_fu_8248_p2() {
    tmp247_fu_8248_p2 = (!ap_const_lv6_31.is_01() || !lhs_V_24_cast_fu_8244_p1.read().is_01())? sc_lv<6>(): (sc_bigint<6>(ap_const_lv6_31) + sc_bigint<6>(lhs_V_24_cast_fu_8244_p1.read()));
}

void mnist_fp16::thread_tmp320_fu_10843_p2() {
    tmp320_fu_10843_p2 = (!rhs_V_12_cast_fu_10833_p1.read().is_01() || !phi_mul195_cast_reg_17504.read().is_01())? sc_lv<10>(): (sc_bigint<10>(rhs_V_12_cast_fu_10833_p1.read()) + sc_biguint<10>(phi_mul195_cast_reg_17504.read()));
}

void mnist_fp16::thread_tmp321_fu_10909_p2() {
    tmp321_fu_10909_p2 = (!ap_const_lv5_18.is_01() || !lhs_V_31_cast_fu_10905_p1.read().is_01())? sc_lv<5>(): (sc_bigint<5>(ap_const_lv5_18) + sc_bigint<5>(lhs_V_31_cast_fu_10905_p1.read()));
}

void mnist_fp16::thread_tmp32_V_10_fu_8584_p2() {
    tmp32_V_10_fu_8584_p2 = (!tmp_195_fu_8578_p2.read().is_01())? sc_lv<32>(): tmp32_V_9_fu_8575_p1.read() << (unsigned short)tmp_195_fu_8578_p2.read().to_uint();
}

void mnist_fp16::thread_tmp32_V_11_fu_8609_p1() {
    tmp32_V_11_fu_8609_p1 = esl_zext<32,16>(p_Result_22_fu_8604_p2.read());
}

void mnist_fp16::thread_tmp32_V_12_fu_4679_p3() {
    tmp32_V_12_fu_4679_p3 = (!icmp1_fu_4635_p2.read()[0].is_01())? sc_lv<32>(): ((icmp1_fu_4635_p2.read()[0].to_bool())? tmp32_V_1_fu_4650_p2.read(): tmp32_V_2_fu_4675_p1.read());
}

void mnist_fp16::thread_tmp32_V_13_fu_9620_p1() {
    tmp32_V_13_fu_9620_p1 = esl_zext<32,16>(p_Val2_33_reg_17191.read());
}

void mnist_fp16::thread_tmp32_V_14_fu_9629_p2() {
    tmp32_V_14_fu_9629_p2 = (!tmp_272_fu_9623_p2.read().is_01())? sc_lv<32>(): tmp32_V_13_fu_9620_p1.read() << (unsigned short)tmp_272_fu_9623_p2.read().to_uint();
}

void mnist_fp16::thread_tmp32_V_15_fu_9654_p1() {
    tmp32_V_15_fu_9654_p1 = esl_zext<32,16>(p_Result_29_fu_9649_p2.read());
}

void mnist_fp16::thread_tmp32_V_17_fu_10531_p1() {
    tmp32_V_17_fu_10531_p1 = esl_zext<32,16>(tmp_V_5_reg_17456.read());
}

void mnist_fp16::thread_tmp32_V_18_fu_10540_p2() {
    tmp32_V_18_fu_10540_p2 = (!tmp_357_fu_10534_p2.read().is_01())? sc_lv<32>(): tmp32_V_17_fu_10531_p1.read() << (unsigned short)tmp_357_fu_10534_p2.read().to_uint();
}

void mnist_fp16::thread_tmp32_V_19_fu_10565_p1() {
    tmp32_V_19_fu_10565_p1 = esl_zext<32,16>(tmp_786_fu_10560_p2.read());
}

void mnist_fp16::thread_tmp32_V_1_fu_4650_p2() {
    tmp32_V_1_fu_4650_p2 = (!tmp_67_fu_4644_p2.read().is_01())? sc_lv<32>(): tmp32_V_fu_4641_p1.read() << (unsigned short)tmp_67_fu_4644_p2.read().to_uint();
}

void mnist_fp16::thread_tmp32_V_20_fu_10577_p1() {
    tmp32_V_20_fu_10577_p1 = grp_fu_2824_p1.read();
}

void mnist_fp16::thread_tmp32_V_21_fu_12363_p1() {
    tmp32_V_21_fu_12363_p1 = esl_zext<32,16>(p_Val2_42_reg_18005.read());
}

void mnist_fp16::thread_tmp32_V_22_fu_12372_p2() {
    tmp32_V_22_fu_12372_p2 = (!tmp_376_fu_12366_p2.read().is_01())? sc_lv<32>(): tmp32_V_21_fu_12363_p1.read() << (unsigned short)tmp_376_fu_12366_p2.read().to_uint();
}

void mnist_fp16::thread_tmp32_V_23_fu_12397_p1() {
    tmp32_V_23_fu_12397_p1 = esl_zext<32,16>(p_Result_41_fu_12392_p2.read());
}

void mnist_fp16::thread_tmp32_V_25_fu_13456_p1() {
    tmp32_V_25_fu_13456_p1 = esl_zext<32,16>(p_Val2_46_reg_18306.read());
}

void mnist_fp16::thread_tmp32_V_26_fu_13465_p2() {
    tmp32_V_26_fu_13465_p2 = (!tmp_444_fu_13459_p2.read().is_01())? sc_lv<32>(): tmp32_V_25_fu_13456_p1.read() << (unsigned short)tmp_444_fu_13459_p2.read().to_uint();
}

void mnist_fp16::thread_tmp32_V_27_fu_13490_p1() {
    tmp32_V_27_fu_13490_p1 = esl_zext<32,16>(p_Result_50_fu_13485_p2.read());
}

void mnist_fp16::thread_tmp32_V_28_fu_5789_p3() {
    tmp32_V_28_fu_5789_p3 = (!icmp3_fu_5745_p2.read()[0].is_01())? sc_lv<32>(): ((icmp3_fu_5745_p2.read()[0].to_bool())? tmp32_V_6_fu_5760_p2.read(): tmp32_V_7_fu_5785_p1.read());
}

void mnist_fp16::thread_tmp32_V_29_fu_14201_p1() {
    tmp32_V_29_fu_14201_p1 = esl_zext<32,16>(tmp_V_8_reg_18530.read());
}

void mnist_fp16::thread_tmp32_V_2_fu_4675_p1() {
    tmp32_V_2_fu_4675_p1 = esl_zext<32,16>(p_Result_s_fu_4670_p2.read());
}

void mnist_fp16::thread_tmp32_V_30_fu_14210_p2() {
    tmp32_V_30_fu_14210_p2 = (!tmp_506_fu_14204_p2.read().is_01())? sc_lv<32>(): tmp32_V_29_fu_14201_p1.read() << (unsigned short)tmp_506_fu_14204_p2.read().to_uint();
}

void mnist_fp16::thread_tmp32_V_31_fu_14235_p1() {
    tmp32_V_31_fu_14235_p1 = esl_zext<32,16>(tmp_911_fu_14230_p2.read());
}

void mnist_fp16::thread_tmp32_V_32_fu_14247_p1() {
    tmp32_V_32_fu_14247_p1 = grp_fu_2824_p1.read();
}

void mnist_fp16::thread_tmp32_V_33_fu_14856_p1() {
    tmp32_V_33_fu_14856_p1 = esl_zext<32,16>(tmp_V_9_reg_18714.read());
}

void mnist_fp16::thread_tmp32_V_34_fu_14865_p2() {
    tmp32_V_34_fu_14865_p2 = (!tmp_551_fu_14859_p2.read().is_01())? sc_lv<32>(): tmp32_V_33_fu_14856_p1.read() << (unsigned short)tmp_551_fu_14859_p2.read().to_uint();
}

void mnist_fp16::thread_tmp32_V_35_fu_14890_p1() {
    tmp32_V_35_fu_14890_p1 = esl_zext<32,16>(tmp_926_fu_14885_p2.read());
}

void mnist_fp16::thread_tmp32_V_36_fu_14906_p1() {
    tmp32_V_36_fu_14906_p1 = grp_fu_2824_p1.read();
}

void mnist_fp16::thread_tmp32_V_38_fu_8613_p3() {
    tmp32_V_38_fu_8613_p3 = (!icmp8_fu_8569_p2.read()[0].is_01())? sc_lv<32>(): ((icmp8_fu_8569_p2.read()[0].to_bool())? tmp32_V_10_fu_8584_p2.read(): tmp32_V_11_fu_8609_p1.read());
}

void mnist_fp16::thread_tmp32_V_3_fu_6687_p2() {
    tmp32_V_3_fu_6687_p2 = (!tmp_205_fu_6681_p2.read().is_01())? sc_lv<32>(): tmp32_V_s_fu_6678_p1.read() << (unsigned short)tmp_205_fu_6681_p2.read().to_uint();
}

void mnist_fp16::thread_tmp32_V_42_fu_9658_p3() {
    tmp32_V_42_fu_9658_p3 = (!icmp10_fu_9614_p2.read()[0].is_01())? sc_lv<32>(): ((icmp10_fu_9614_p2.read()[0].to_bool())? tmp32_V_14_fu_9629_p2.read(): tmp32_V_15_fu_9654_p1.read());
}

void mnist_fp16::thread_tmp32_V_45_fu_12401_p3() {
    tmp32_V_45_fu_12401_p3 = (!icmp15_fu_12357_p2.read()[0].is_01())? sc_lv<32>(): ((icmp15_fu_12357_p2.read()[0].to_bool())? tmp32_V_22_fu_12372_p2.read(): tmp32_V_23_fu_12397_p1.read());
}

void mnist_fp16::thread_tmp32_V_47_fu_13494_p3() {
    tmp32_V_47_fu_13494_p3 = (!icmp18_fu_13450_p2.read()[0].is_01())? sc_lv<32>(): ((icmp18_fu_13450_p2.read()[0].to_bool())? tmp32_V_26_fu_13465_p2.read(): tmp32_V_27_fu_13490_p1.read());
}

void mnist_fp16::thread_tmp32_V_4_fu_6712_p1() {
    tmp32_V_4_fu_6712_p1 = esl_zext<32,16>(tmp_610_fu_6707_p2.read());
}

void mnist_fp16::thread_tmp32_V_51_fu_4687_p1() {
    tmp32_V_51_fu_4687_p1 = grp_fu_2824_p1.read();
}

void mnist_fp16::thread_tmp32_V_52_fu_5806_p1() {
    tmp32_V_52_fu_5806_p1 = grp_fu_2824_p1.read();
}

void mnist_fp16::thread_tmp32_V_53_fu_8621_p1() {
    tmp32_V_53_fu_8621_p1 = grp_fu_2824_p1.read();
}

void mnist_fp16::thread_tmp32_V_54_fu_9675_p1() {
    tmp32_V_54_fu_9675_p1 = grp_fu_2824_p1.read();
}

void mnist_fp16::thread_tmp32_V_55_fu_12409_p1() {
    tmp32_V_55_fu_12409_p1 = grp_fu_2824_p1.read();
}

void mnist_fp16::thread_tmp32_V_56_fu_13502_p1() {
    tmp32_V_56_fu_13502_p1 = grp_fu_2824_p1.read();
}

void mnist_fp16::thread_tmp32_V_5_fu_5751_p1() {
    tmp32_V_5_fu_5751_p1 = esl_zext<32,16>(p_Val2_15_reg_16111.read());
}

void mnist_fp16::thread_tmp32_V_6_fu_5760_p2() {
    tmp32_V_6_fu_5760_p2 = (!tmp_102_fu_5754_p2.read().is_01())? sc_lv<32>(): tmp32_V_5_fu_5751_p1.read() << (unsigned short)tmp_102_fu_5754_p2.read().to_uint();
}

void mnist_fp16::thread_tmp32_V_7_fu_5785_p1() {
    tmp32_V_7_fu_5785_p1 = esl_zext<32,16>(p_Result_8_fu_5780_p2.read());
}

void mnist_fp16::thread_tmp32_V_8_fu_6724_p1() {
    tmp32_V_8_fu_6724_p1 = grp_fu_2824_p1.read();
}

void mnist_fp16::thread_tmp32_V_9_fu_8575_p1() {
    tmp32_V_9_fu_8575_p1 = esl_zext<32,16>(p_Val2_30_reg_16910.read());
}

void mnist_fp16::thread_tmp32_V_fu_4641_p1() {
    tmp32_V_fu_4641_p1 = esl_zext<32,16>(p_Val2_8_reg_15825.read());
}

void mnist_fp16::thread_tmp32_V_s_fu_6678_p1() {
    tmp32_V_s_fu_6678_p1 = esl_zext<32,16>(tmp_V_2_reg_16376.read());
}

void mnist_fp16::thread_tmp32_fu_4175_p2() {
    tmp32_fu_4175_p2 = (!rhs_V_5_cast_fu_4171_p1.read().is_01() || !phi_mul_cast_reg_15687.read().is_01())? sc_lv<13>(): (sc_bigint<13>(rhs_V_5_cast_fu_4171_p1.read()) + sc_biguint<13>(phi_mul_cast_reg_15687.read()));
}

void mnist_fp16::thread_tmp33_fu_4314_p2() {
    tmp33_fu_4314_p2 = (!ap_const_lv7_63.is_01() || !lhs_V_10_cast_fu_4310_p1.read().is_01())? sc_lv<7>(): (sc_bigint<7>(ap_const_lv7_63) + sc_bigint<7>(lhs_V_10_cast_fu_4310_p1.read()));
}

void mnist_fp16::thread_tmp349_cast_fu_4320_p1() {
    tmp349_cast_fu_4320_p1 = esl_sext<13,7>(tmp33_reg_15747.read());
}

void mnist_fp16::thread_tmp350_cast_cast_fu_4710_p3() {
    tmp350_cast_cast_fu_4710_p3 = (!tmp_76_reg_15856.read()[0].is_01())? sc_lv<8>(): ((tmp_76_reg_15856.read()[0].to_bool())? ap_const_lv8_72: ap_const_lv8_71);
}

void mnist_fp16::thread_tmp351_cast_cast_fu_5829_p3() {
    tmp351_cast_cast_fu_5829_p3 = (!tmp_112_reg_16147.read()[0].is_01())? sc_lv<8>(): ((tmp_112_reg_16147.read()[0].to_bool())? ap_const_lv8_72: ap_const_lv8_71);
}

void mnist_fp16::thread_tmp352_cast_cast_fu_6747_p3() {
    tmp352_cast_cast_fu_6747_p3 = (!tmp_209_reg_16407.read()[0].is_01())? sc_lv<8>(): ((tmp_209_reg_16407.read()[0].to_bool())? ap_const_lv8_72: ap_const_lv8_71);
}

void mnist_fp16::thread_tmp357_cast_fu_7086_p1() {
    tmp357_cast_fu_7086_p1 = esl_sext<11,6>(tmp139_reg_16486.read());
}

void mnist_fp16::thread_tmp362_cast_fu_8254_p1() {
    tmp362_cast_fu_8254_p1 = esl_sext<12,6>(tmp247_reg_16832.read());
}

void mnist_fp16::thread_tmp363_cast_cast_fu_8644_p3() {
    tmp363_cast_cast_fu_8644_p3 = (!tmp_230_reg_16941.read()[0].is_01())? sc_lv<8>(): ((tmp_230_reg_16941.read()[0].to_bool())? ap_const_lv8_72: ap_const_lv8_71);
}

void mnist_fp16::thread_tmp364_cast_cast_fu_9698_p3() {
    tmp364_cast_cast_fu_9698_p3 = (!tmp_275_reg_17227.read()[0].is_01())? sc_lv<8>(): ((tmp_275_reg_17227.read()[0].to_bool())? ap_const_lv8_72: ap_const_lv8_71);
}

void mnist_fp16::thread_tmp365_cast_cast_fu_10600_p3() {
    tmp365_cast_cast_fu_10600_p3 = (!tmp_366_reg_17487.read()[0].is_01())? sc_lv<8>(): ((tmp_366_reg_17487.read()[0].to_bool())? ap_const_lv8_72: ap_const_lv8_71);
}

void mnist_fp16::thread_tmp370_cast_fu_10915_p1() {
    tmp370_cast_fu_10915_p1 = esl_sext<10,5>(tmp321_fu_10909_p2.read());
}

void mnist_fp16::thread_tmp374_fu_11986_p2() {
    tmp374_fu_11986_p2 = (!rhs_V_16_cast_fu_11976_p1.read().is_01() || !phi_mul197_cast_reg_17865.read().is_01())? sc_lv<11>(): (sc_bigint<11>(rhs_V_16_cast_fu_11976_p1.read()) + sc_biguint<11>(phi_mul197_cast_reg_17865.read()));
}

void mnist_fp16::thread_tmp375_cast_fu_12058_p1() {
    tmp375_cast_fu_12058_p1 = esl_sext<11,5>(tmp375_fu_12052_p2.read());
}

void mnist_fp16::thread_tmp375_fu_12052_p2() {
    tmp375_fu_12052_p2 = (!ap_const_lv5_18.is_01() || !lhs_V_36_cast_fu_12048_p1.read().is_01())? sc_lv<5>(): (sc_bigint<5>(ap_const_lv5_18) + sc_bigint<5>(lhs_V_36_cast_fu_12048_p1.read()));
}

void mnist_fp16::thread_tmp376_cast_cast_fu_12432_p3() {
    tmp376_cast_cast_fu_12432_p3 = (!tmp_408_reg_18036.read()[0].is_01())? sc_lv<8>(): ((tmp_408_reg_18036.read()[0].to_bool())? ap_const_lv8_72: ap_const_lv8_71);
}

void mnist_fp16::thread_tmp377_cast_cast_fu_13525_p3() {
    tmp377_cast_cast_fu_13525_p3 = (!tmp_482_reg_18337.read()[0].is_01())? sc_lv<8>(): ((tmp_482_reg_18337.read()[0].to_bool())? ap_const_lv8_72: ap_const_lv8_71);
}

void mnist_fp16::thread_tmp378_cast_cast_fu_14270_p3() {
    tmp378_cast_cast_fu_14270_p3 = (!tmp_508_reg_18561.read()[0].is_01())? sc_lv<8>(): ((tmp_508_reg_18561.read()[0].to_bool())? ap_const_lv8_72: ap_const_lv8_71);
}

void mnist_fp16::thread_tmp379_cast_cast_fu_14929_p3() {
    tmp379_cast_cast_fu_14929_p3 = (!tmp_553_reg_18750.read()[0].is_01())? sc_lv<8>(): ((tmp_553_reg_18750.read()[0].to_bool())? ap_const_lv8_72: ap_const_lv8_71);
}

void mnist_fp16::thread_tmp_100_fu_6002_p4() {
    tmp_100_fu_6002_p4 = conv2_0_load_to_int_fu_5999_p1.read().range(30, 23);
}

void mnist_fp16::thread_tmp_101_fu_6075_p2() {
    tmp_101_fu_6075_p2 = (!tmp_361_fu_6049_p1.read().is_01() || !ap_const_lv63_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_361_fu_6049_p1.read() == ap_const_lv63_0);
}

void mnist_fp16::thread_tmp_102_fu_5754_p2() {
    tmp_102_fu_5754_p2 = (!ap_const_lv32_1F.is_01() || !msb_idx_3_cast_fu_5731_p1.read().is_01())? sc_lv<32>(): (sc_biguint<32>(ap_const_lv32_1F) - sc_biguint<32>(msb_idx_3_cast_fu_5731_p1.read()));
}

void mnist_fp16::thread_tmp_103_fu_3662_p2() {
    tmp_103_fu_3662_p2 = (!tmp_159_cast_reg_15573.read().is_01() || !tmp_18_cast_fu_3658_p1.read().is_01())? sc_lv<10>(): (sc_bigint<10>(tmp_159_cast_reg_15573.read()) + sc_biguint<10>(tmp_18_cast_fu_3658_p1.read()));
}

void mnist_fp16::thread_tmp_104_cast_fu_5069_p1() {
    tmp_104_cast_fu_5069_p1 = esl_zext<13,5>(r_V_10_fu_5063_p2.read());
}

void mnist_fp16::thread_tmp_104_fu_3667_p1() {
    tmp_104_fu_3667_p1 = tmp_103_fu_3662_p2.read().range(8-1, 0);
}

void mnist_fp16::thread_tmp_105_fu_3679_p3() {
    tmp_105_fu_3679_p3 = esl_concat<10,2>(tmp_103_fu_3662_p2.read(), ap_const_lv2_0);
}

void mnist_fp16::thread_tmp_106_fu_5169_p1() {
    tmp_106_fu_5169_p1 = esl_zext<12,11>(exp_tmp_V_reg_15980.read());
}

void mnist_fp16::thread_tmp_107_fu_3691_p2() {
    tmp_107_fu_3691_p2 = (!p_shl24_cast_fu_3671_p3.read().is_01() || !p_shl25_cast_fu_3687_p1.read().is_01())? sc_lv<13>(): (sc_biguint<13>(p_shl24_cast_fu_3671_p3.read()) - sc_bigint<13>(p_shl25_cast_fu_3687_p1.read()));
}

void mnist_fp16::thread_tmp_108_fu_5127_p2() {
    tmp_108_fu_5127_p2 = (!tmp_531_fu_5101_p1.read().is_01() || !ap_const_lv63_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_531_fu_5101_p1.read() == ap_const_lv63_0);
}

void mnist_fp16::thread_tmp_10_cast_fu_3291_p1() {
    tmp_10_cast_fu_3291_p1 = esl_zext<11,5>(tmp_8_reg_15434.read());
}

void mnist_fp16::thread_tmp_10_fu_3039_p3() {
    tmp_10_fu_3039_p3 = esl_concat<5,1>(p_s_reg_1314.read(), ap_const_lv1_0);
}

void mnist_fp16::thread_tmp_110_cast_fu_7312_p1() {
    tmp_110_cast_fu_7312_p1 = esl_zext<12,5>(p_30_reg_1782.read());
}

void mnist_fp16::thread_tmp_110_fu_4771_p3() {
    tmp_110_fu_4771_p3 = esl_concat<3,2>(p_9_reg_1532.read(), ap_const_lv2_0);
}

void mnist_fp16::thread_tmp_111_cast_fu_8763_p1() {
    tmp_111_cast_fu_8763_p1 = esl_zext<10,4>(p_35_reg_2016.read());
}

void mnist_fp16::thread_tmp_111_fu_4787_p3() {
    tmp_111_fu_4787_p3 = esl_concat<3,5>(p_9_reg_1532.read(), ap_const_lv5_0);
}

void mnist_fp16::thread_tmp_112_fu_5820_p2() {
    tmp_112_fu_5820_p2 = (!p_Result_s_42_fu_5810_p4.read().is_01() || !ap_const_lv8_9E.is_01())? sc_lv<1>(): sc_lv<1>(p_Result_s_42_fu_5810_p4.read() != ap_const_lv8_9E);
}

void mnist_fp16::thread_tmp_113_fu_6028_p2() {
    tmp_113_fu_6028_p2 = (notrhs1_reg_16208.read() | notlhs1_reg_16203.read());
}

void mnist_fp16::thread_tmp_115_fu_5842_p3() {
    tmp_115_fu_5842_p3 = esl_concat<1,8>(is_neg_1_reg_16106.read(), p_Repl2_4_trunc_fu_5836_p2.read());
}

void mnist_fp16::thread_tmp_116_fu_4799_p2() {
    tmp_116_fu_4799_p2 = (!p_shl34_cast_fu_4795_p1.read().is_01() || !tmp_580_cast_fu_4783_p1.read().is_01())? sc_lv<9>(): (sc_biguint<9>(p_shl34_cast_fu_4795_p1.read()) - sc_biguint<9>(tmp_580_cast_fu_4783_p1.read()));
}

void mnist_fp16::thread_tmp_117_fu_8181_p2() {
    tmp_117_fu_8181_p2 = (!p_39_reg_1978.read().is_01() || !ap_const_lv5_0.is_01())? sc_lv<1>(): sc_lv<1>(p_39_reg_1978.read() != ap_const_lv5_0);
}

void mnist_fp16::thread_tmp_118_fu_8187_p2() {
    tmp_118_fu_8187_p2 = (!p_39_reg_1978.read().is_01() || !ap_const_lv5_F.is_01())? sc_lv<1>(): (sc_biguint<5>(p_39_reg_1978.read()) < sc_biguint<5>(ap_const_lv5_F));
}

void mnist_fp16::thread_tmp_119_cast_fu_7490_p1() {
    tmp_119_cast_fu_7490_p1 = esl_zext<12,4>(p_32_reg_1829.read());
}

void mnist_fp16::thread_tmp_119_fu_4106_p2() {
    tmp_119_fu_4106_p2 = (!lhs_V_cast_41_fu_4102_p1.read().is_01() || !tmp_574_cast_reg_15705.read().is_01())? sc_lv<10>(): (sc_biguint<10>(lhs_V_cast_41_fu_4102_p1.read()) + sc_bigint<10>(tmp_574_cast_reg_15705.read()));
}

void mnist_fp16::thread_tmp_11_cast_fu_3412_p1() {
    tmp_11_cast_fu_3412_p1 = esl_zext<10,5>(p_5_reg_1364.read());
}

void mnist_fp16::thread_tmp_11_fu_3051_p2() {
    tmp_11_fu_3051_p2 = (!p_shl10_cast_fu_3035_p1.read().is_01() || !p_shl11_cast_fu_3047_p1.read().is_01())? sc_lv<11>(): (sc_biguint<11>(p_shl10_cast_fu_3035_p1.read()) - sc_biguint<11>(p_shl11_cast_fu_3047_p1.read()));
}

void mnist_fp16::thread_tmp_120_cast_fu_7459_p1() {
    tmp_120_cast_fu_7459_p1 = esl_zext<7,3>(p_40_reg_1841.read());
}

void mnist_fp16::thread_tmp_120_fu_4111_p1() {
    tmp_120_fu_4111_p1 = tmp_119_fu_4106_p2.read().range(8-1, 0);
}

void mnist_fp16::thread_tmp_122_fu_6032_p2() {
    tmp_122_fu_6032_p2 = (tmp_113_fu_6028_p2.read() & tmp_114_reg_16213.read());
}

void mnist_fp16::thread_tmp_123_fu_6114_p2() {
    tmp_123_fu_6114_p2 = (!F2_1_fu_6108_p2.read().is_01() || !ap_const_lv12_E.is_01())? sc_lv<1>(): (sc_bigint<12>(F2_1_fu_6108_p2.read()) > sc_bigint<12>(ap_const_lv12_E));
}

void mnist_fp16::thread_tmp_124_fu_6120_p2() {
    tmp_124_fu_6120_p2 = (!ap_const_lv12_FF2.is_01() || !F2_1_fu_6108_p2.read().is_01())? sc_lv<12>(): (sc_bigint<12>(ap_const_lv12_FF2) + sc_biguint<12>(F2_1_fu_6108_p2.read()));
}

void mnist_fp16::thread_tmp_125_fu_6126_p2() {
    tmp_125_fu_6126_p2 = (!ap_const_lv12_E.is_01() || !F2_1_fu_6108_p2.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_E) - sc_biguint<12>(F2_1_fu_6108_p2.read()));
}

void mnist_fp16::thread_tmp_126_fu_6140_p2() {
    tmp_126_fu_6140_p2 = (!F2_1_fu_6108_p2.read().is_01() || !ap_const_lv12_E.is_01())? sc_lv<1>(): sc_lv<1>(F2_1_fu_6108_p2.read() == ap_const_lv12_E);
}

void mnist_fp16::thread_tmp_127_cast_fu_5797_p1() {
    tmp_127_cast_fu_5797_p1 = esl_zext<13,5>(p_18_reg_1555.read());
}

void mnist_fp16::thread_tmp_127_fu_4123_p3() {
    tmp_127_fu_4123_p3 = esl_concat<10,1>(tmp_119_fu_4106_p2.read(), ap_const_lv1_0);
}

void mnist_fp16::thread_tmp_128_fu_5172_p3() {
    tmp_128_fu_5172_p3 = esl_concat<1,52>(ap_const_lv1_1, tmp_540_reg_15985.read());
}

void mnist_fp16::thread_tmp_130_fu_5202_p2() {
    tmp_130_fu_5202_p2 = (!F2_2_fu_5196_p2.read().is_01() || !ap_const_lv12_E.is_01())? sc_lv<1>(): (sc_bigint<12>(F2_2_fu_5196_p2.read()) > sc_bigint<12>(ap_const_lv12_E));
}

void mnist_fp16::thread_tmp_131_fu_5208_p2() {
    tmp_131_fu_5208_p2 = (!ap_const_lv12_FF2.is_01() || !F2_2_fu_5196_p2.read().is_01())? sc_lv<12>(): (sc_bigint<12>(ap_const_lv12_FF2) + sc_biguint<12>(F2_2_fu_5196_p2.read()));
}

void mnist_fp16::thread_tmp_132_fu_5214_p2() {
    tmp_132_fu_5214_p2 = (!ap_const_lv12_E.is_01() || !F2_2_fu_5196_p2.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_E) - sc_biguint<12>(F2_2_fu_5196_p2.read()));
}

void mnist_fp16::thread_tmp_133_fu_5228_p2() {
    tmp_133_fu_5228_p2 = (!F2_2_fu_5196_p2.read().is_01() || !ap_const_lv12_E.is_01())? sc_lv<1>(): sc_lv<1>(F2_2_fu_5196_p2.read() == ap_const_lv12_E);
}

void mnist_fp16::thread_tmp_134_fu_4135_p2() {
    tmp_134_fu_4135_p2 = (!p_shl28_cast_fu_4115_p3.read().is_01() || !p_shl29_cast_fu_4131_p1.read().is_01())? sc_lv<13>(): (sc_biguint<13>(p_shl28_cast_fu_4115_p3.read()) - sc_bigint<13>(p_shl29_cast_fu_4131_p1.read()));
}

void mnist_fp16::thread_tmp_135_cast_fu_6501_p1() {
    tmp_135_cast_fu_6501_p1 = esl_zext<5,2>(p_38_reg_1715.read());
}

void mnist_fp16::thread_tmp_135_fu_6037_p1() {
    tmp_135_fu_6037_p1 = grp_fu_2830_p1.read();
}

void mnist_fp16::thread_tmp_136_cast_cast_fu_6510_p1() {
    tmp_136_cast_cast_fu_6510_p1 = esl_zext<10,5>(tmp_136_fu_6505_p2.read());
}

void mnist_fp16::thread_tmp_136_fu_6505_p2() {
    tmp_136_fu_6505_p2 = (!tmp_135_cast_fu_6501_p1.read().is_01() || !r_V_s_reg_16305.read().is_01())? sc_lv<5>(): (sc_biguint<5>(tmp_135_cast_fu_6501_p1.read()) + sc_biguint<5>(r_V_s_reg_16305.read()));
}

void mnist_fp16::thread_tmp_137_fu_3713_p2() {
    tmp_137_fu_3713_p2 = (!tmp_20_cast_fu_3709_p1.read().is_01() || !tmp_107_reg_15586.read().is_01())? sc_lv<13>(): (sc_biguint<13>(tmp_20_cast_fu_3709_p1.read()) + sc_biguint<13>(tmp_107_reg_15586.read()));
}

void mnist_fp16::thread_tmp_138_cast_fu_11151_p1() {
    tmp_138_cast_fu_11151_p1 = esl_zext<9,5>(p_42_reg_2280.read());
}

void mnist_fp16::thread_tmp_138_fu_3736_p1() {
    tmp_138_fu_3736_p1 = conv1_0_load_to_int_fu_3723_p1.read().range(23-1, 0);
}

void mnist_fp16::thread_tmp_139_fu_3773_p1() {
    tmp_139_fu_3773_p1 = ireg_V_fu_3765_p3.read().range(63-1, 0);
}

void mnist_fp16::thread_tmp_13_fu_3352_p3() {
    tmp_13_fu_3352_p3 = esl_concat<3,2>(p_2_reg_1353.read(), ap_const_lv2_0);
}

void mnist_fp16::thread_tmp_140_to_int_fu_6806_p1() {
    tmp_140_to_int_fu_6806_p1 = tmp_140_reg_1726.read();
}

void mnist_fp16::thread_tmp_141_fu_6204_p2() {
    tmp_141_fu_6204_p2 = (!sh_amt_1_reg_16245.read().is_01() || !ap_const_lv12_36.is_01())? sc_lv<1>(): (sc_biguint<12>(sh_amt_1_reg_16245.read()) < sc_biguint<12>(ap_const_lv12_36));
}

void mnist_fp16::thread_tmp_143_fu_6209_p1() {
    tmp_143_fu_6209_p1 = esl_zext<54,32>(sh_amt_1_cast_fu_6201_p1.read());
}

void mnist_fp16::thread_tmp_144_fu_5342_p2() {
    tmp_144_fu_5342_p2 = (!sh_amt_2_reg_16029.read().is_01() || !ap_const_lv12_36.is_01())? sc_lv<1>(): (sc_biguint<12>(sh_amt_2_reg_16029.read()) < sc_biguint<12>(ap_const_lv12_36));
}

void mnist_fp16::thread_tmp_145_fu_6213_p2() {
    tmp_145_fu_6213_p2 = (!man_V_6_reg_16240.read().is_01() || !tmp_143_fu_6209_p1.read().is_01())? sc_lv<54>(): sc_bigint<54>(man_V_6_reg_16240.read()) >> (unsigned short)tmp_143_fu_6209_p1.read().to_uint();
}

void mnist_fp16::thread_tmp_146_fu_3795_p1() {
    tmp_146_fu_3795_p1 = ireg_V_fu_3765_p3.read().range(52-1, 0);
}

void mnist_fp16::thread_tmp_147_fu_8226_p3() {
    tmp_147_fu_8226_p3 = (!tmp_646_fu_8214_p2.read()[0].is_01())? sc_lv<4>(): ((tmp_646_fu_8214_p2.read()[0].to_bool())? r_V_15_fu_8208_p2.read(): tmp_647_fu_8220_p2.read());
}

void mnist_fp16::thread_tmp_148_cast_fu_8442_p1() {
    tmp_148_cast_fu_8442_p1 = esl_zext<12,4>(tmp_147_reg_16827.read());
}

void mnist_fp16::thread_tmp_148_fu_3870_p1() {
    tmp_148_fu_3870_p1 = man_V_2_fu_3825_p3.read().range(16-1, 0);
}

void mnist_fp16::thread_tmp_149_fu_3874_p4() {
    tmp_149_fu_3874_p4 = sh_amt_fu_3856_p3.read().range(11, 4);
}

void mnist_fp16::thread_tmp_150_cast_fu_7665_p1() {
    tmp_150_cast_fu_7665_p1 = esl_zext<10,4>(p_41_reg_1923.read());
}

void mnist_fp16::thread_tmp_150_fu_3942_p1() {
    tmp_150_fu_3942_p1 = tmp_82_fu_3937_p2.read().range(16-1, 0);
}

void mnist_fp16::thread_tmp_151_cast_fu_8234_p1() {
    tmp_151_cast_fu_8234_p1 = esl_zext<5,4>(tmp_147_fu_8226_p3.read());
}

void mnist_fp16::thread_tmp_151_fu_6229_p1() {
    tmp_151_fu_6229_p1 = esl_sext<32,16>(tmp_367_reg_16256.read());
}

void mnist_fp16::thread_tmp_153_cast_fu_6561_p1() {
    tmp_153_cast_fu_6561_p1 = esl_zext<5,2>(p_43_reg_1738.read());
}

void mnist_fp16::thread_tmp_153_fu_6232_p2() {
    tmp_153_fu_6232_p2 = (!sh_amt_1_cast_fu_6201_p1.read().is_01())? sc_lv<32>(): tmp_151_fu_6229_p1.read() << (unsigned short)sh_amt_1_cast_fu_6201_p1.read().to_uint();
}

void mnist_fp16::thread_tmp_154_cast_cast_fu_6570_p1() {
    tmp_154_cast_cast_fu_6570_p1 = esl_zext<13,5>(tmp_154_fu_6565_p2.read());
}

void mnist_fp16::thread_tmp_154_fu_6565_p2() {
    tmp_154_fu_6565_p2 = (!tmp_153_cast_fu_6561_p1.read().is_01() || !r_V_8_reg_16323.read().is_01())? sc_lv<5>(): (sc_biguint<5>(tmp_153_cast_fu_6561_p1.read()) + sc_biguint<5>(r_V_8_reg_16323.read()));
}

void mnist_fp16::thread_tmp_155_fu_6598_p2() {
    tmp_155_fu_6598_p2 = (!relu2_0_V_load_reg_16354.read().is_01() || !ap_const_lv16_0.is_01())? sc_lv<1>(): sc_lv<1>(relu2_0_V_load_reg_16354.read() == ap_const_lv16_0);
}

void mnist_fp16::thread_tmp_156_fu_3962_p1() {
    tmp_156_fu_3962_p1 = tmp_90_fu_3956_p2.read().range(16-1, 0);
}

void mnist_fp16::thread_tmp_158_fu_3540_p2() {
    tmp_158_fu_3540_p2 = (!tmp_62_reg_15506.read().is_01() || !tmp_25_cast_fu_3536_p1.read().is_01())? sc_lv<13>(): (sc_biguint<13>(tmp_62_reg_15506.read()) + sc_biguint<13>(tmp_25_cast_fu_3536_p1.read()));
}

void mnist_fp16::thread_tmp_159_cast_fu_3642_p1() {
    tmp_159_cast_fu_3642_p1 = esl_sext<10,9>(tmp_58_fu_3636_p2.read());
}

void mnist_fp16::thread_tmp_159_fu_3485_p3() {
    tmp_159_fu_3485_p3 = esl_concat<5,5>(r_V_3_fu_3479_p2.read(), ap_const_lv5_0);
}

void mnist_fp16::thread_tmp_160_fu_7815_p3() {
    tmp_160_fu_7815_p3 = esl_concat<1,52>(ap_const_lv1_1, tmp_625_reg_16725.read());
}

void mnist_fp16::thread_tmp_161_fu_3497_p3() {
    tmp_161_fu_3497_p3 = esl_concat<5,1>(r_V_3_fu_3479_p2.read(), ap_const_lv1_0);
}

void mnist_fp16::thread_tmp_162_fu_5347_p1() {
    tmp_162_fu_5347_p1 = esl_zext<54,32>(sh_amt_2_cast_fu_5339_p1.read());
}

void mnist_fp16::thread_tmp_163_fu_5351_p2() {
    tmp_163_fu_5351_p2 = (!man_V_4_reg_16018.read().is_01() || !tmp_162_fu_5347_p1.read().is_01())? sc_lv<54>(): sc_bigint<54>(man_V_4_reg_16018.read()) >> (unsigned short)tmp_162_fu_5347_p1.read().to_uint();
}

void mnist_fp16::thread_tmp_164_fu_3509_p2() {
    tmp_164_fu_3509_p2 = (!p_shl20_cast_fu_3493_p1.read().is_01() || !p_shl21_cast_fu_3505_p1.read().is_01())? sc_lv<11>(): (sc_biguint<11>(p_shl20_cast_fu_3493_p1.read()) - sc_biguint<11>(p_shl21_cast_fu_3505_p1.read()));
}

void mnist_fp16::thread_tmp_165_fu_5367_p1() {
    tmp_165_fu_5367_p1 = esl_sext<32,16>(tmp_545_reg_16041.read());
}

void mnist_fp16::thread_tmp_166_fu_5370_p2() {
    tmp_166_fu_5370_p2 = (!sh_amt_2_cast_fu_5339_p1.read().is_01())? sc_lv<32>(): tmp_165_fu_5367_p1.read() << (unsigned short)sh_amt_2_cast_fu_5339_p1.read().to_uint();
}

void mnist_fp16::thread_tmp_167_fu_3519_p2() {
    tmp_167_fu_3519_p2 = (!tmp_27_cast_fu_3515_p1.read().is_01() || !tmp_49_cast_reg_15488.read().is_01())? sc_lv<7>(): (sc_biguint<7>(tmp_27_cast_fu_3515_p1.read()) + sc_bigint<7>(tmp_49_cast_reg_15488.read()));
}

void mnist_fp16::thread_tmp_168_fu_8480_p2() {
    tmp_168_fu_8480_p2 = (!relu3_0_V_q0.read().is_01() || !ap_const_lv16_0.is_01())? sc_lv<1>(): sc_lv<1>(relu3_0_V_q0.read() == ap_const_lv16_0);
}

void mnist_fp16::thread_tmp_169_cast_fu_5083_p1() {
    tmp_169_cast_fu_5083_p1 = esl_zext<9,2>(p_34_reg_1625.read());
}

void mnist_fp16::thread_tmp_169_fu_3524_p2() {
    tmp_169_fu_3524_p2 = (!ap_const_lv7_2.is_01())? sc_lv<7>(): tmp_167_fu_3519_p2.read() << (unsigned short)ap_const_lv7_2.to_uint();
}

void mnist_fp16::thread_tmp_16_cast_fu_3322_p1() {
    tmp_16_cast_fu_3322_p1 = esl_zext<11,5>(p_3_reg_1326.read());
}

void mnist_fp16::thread_tmp_16_fu_3368_p2() {
    tmp_16_fu_3368_p2 = (!p_shl16_cast_fu_3364_p1.read().is_01() || !tmp_7_cast_fu_3348_p1.read().is_01())? sc_lv<6>(): (sc_biguint<6>(p_shl16_cast_fu_3364_p1.read()) - sc_biguint<6>(tmp_7_cast_fu_3348_p1.read()));
}

void mnist_fp16::thread_tmp_170_fu_5254_p1() {
    tmp_170_fu_5254_p1 = esl_zext<12,11>(exp_tmp_V_1_reg_16002.read());
}

void mnist_fp16::thread_tmp_171_fu_5257_p3() {
    tmp_171_fu_5257_p3 = esl_concat<1,52>(ap_const_lv1_1, tmp_559_reg_16007.read());
}

void mnist_fp16::thread_tmp_172_fu_5163_p2() {
    tmp_172_fu_5163_p2 = (!tmp_554_fu_5137_p1.read().is_01() || !ap_const_lv63_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_554_fu_5137_p1.read() == ap_const_lv63_0);
}

void mnist_fp16::thread_tmp_173_fu_5287_p2() {
    tmp_173_fu_5287_p2 = (!F2_3_fu_5281_p2.read().is_01() || !ap_const_lv12_E.is_01())? sc_lv<1>(): (sc_bigint<12>(F2_3_fu_5281_p2.read()) > sc_bigint<12>(ap_const_lv12_E));
}

void mnist_fp16::thread_tmp_174_fu_5293_p2() {
    tmp_174_fu_5293_p2 = (!ap_const_lv12_FF2.is_01() || !F2_3_fu_5281_p2.read().is_01())? sc_lv<12>(): (sc_bigint<12>(ap_const_lv12_FF2) + sc_biguint<12>(F2_3_fu_5281_p2.read()));
}

void mnist_fp16::thread_tmp_175_fu_5299_p2() {
    tmp_175_fu_5299_p2 = (!ap_const_lv12_E.is_01() || !F2_3_fu_5281_p2.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_E) - sc_biguint<12>(F2_3_fu_5281_p2.read()));
}

void mnist_fp16::thread_tmp_176_fu_5313_p2() {
    tmp_176_fu_5313_p2 = (!F2_3_fu_5281_p2.read().is_01() || !ap_const_lv12_E.is_01())? sc_lv<1>(): sc_lv<1>(F2_3_fu_5281_p2.read() == ap_const_lv12_E);
}

void mnist_fp16::thread_tmp_177_fu_3530_p2() {
    tmp_177_fu_3530_p2 = (!tmp_169_fu_3524_p2.read().is_01() || !tmp_167_fu_3519_p2.read().is_01())? sc_lv<7>(): (sc_biguint<7>(tmp_169_fu_3524_p2.read()) - sc_biguint<7>(tmp_167_fu_3519_p2.read()));
}

void mnist_fp16::thread_tmp_178_fu_5491_p2() {
    tmp_178_fu_5491_p2 = (!sh_amt_3_reg_16063.read().is_01() || !ap_const_lv12_36.is_01())? sc_lv<1>(): (sc_biguint<12>(sh_amt_3_reg_16063.read()) < sc_biguint<12>(ap_const_lv12_36));
}

void mnist_fp16::thread_tmp_179_fu_5888_p3() {
    tmp_179_fu_5888_p3 = esl_concat<3,5>(p_19_reg_1636.read(), ap_const_lv5_0);
}

void mnist_fp16::thread_tmp_17_fu_3378_p3() {
    tmp_17_fu_3378_p3 = esl_concat<3,5>(p_2_reg_1353.read(), ap_const_lv5_0);
}

void mnist_fp16::thread_tmp_180_fu_5496_p1() {
    tmp_180_fu_5496_p1 = esl_zext<54,32>(sh_amt_3_cast_fu_5488_p1.read());
}

void mnist_fp16::thread_tmp_181_cast_fu_10823_p1() {
    tmp_181_cast_fu_10823_p1 = esl_zext<8,7>(tmp_181_fu_10815_p3.read());
}

void mnist_fp16::thread_tmp_181_fu_10815_p3() {
    tmp_181_fu_10815_p3 = esl_concat<4,3>(p_45_reg_2245.read(), ap_const_lv3_0);
}

void mnist_fp16::thread_tmp_182_fu_8486_p2() {
    tmp_182_fu_8486_p2 = (!ap_const_lv16_0.is_01() || !relu3_0_V_q0.read().is_01())? sc_lv<16>(): (sc_biguint<16>(ap_const_lv16_0) - sc_biguint<16>(relu3_0_V_q0.read()));
}

void mnist_fp16::thread_tmp_183_fu_5900_p3() {
    tmp_183_fu_5900_p3 = esl_concat<3,2>(p_19_reg_1636.read(), ap_const_lv2_0);
}

void mnist_fp16::thread_tmp_184_cast_fu_7716_p1() {
    tmp_184_cast_fu_7716_p1 = esl_zext<12,4>(p_47_reg_1934.read());
}

void mnist_fp16::thread_tmp_184_fu_5912_p2() {
    tmp_184_fu_5912_p2 = (!p_shl36_cast_fu_5896_p1.read().is_01() || !p_shl37_cast_fu_5908_p1.read().is_01())? sc_lv<9>(): (sc_biguint<9>(p_shl36_cast_fu_5896_p1.read()) - sc_biguint<9>(p_shl37_cast_fu_5908_p1.read()));
}

void mnist_fp16::thread_tmp_185_fu_5500_p2() {
    tmp_185_fu_5500_p2 = (!man_V_9_reg_16052.read().is_01() || !tmp_180_fu_5496_p1.read().is_01())? sc_lv<54>(): sc_bigint<54>(man_V_9_reg_16052.read()) >> (unsigned short)tmp_180_fu_5496_p1.read().to_uint();
}

void mnist_fp16::thread_tmp_186_fu_4825_p2() {
    tmp_186_fu_4825_p2 = (!tmp_583_cast_reg_15879.read().is_01() || !tmp_29_cast_fu_4821_p1.read().is_01())? sc_lv<10>(): (sc_bigint<10>(tmp_583_cast_reg_15879.read()) + sc_biguint<10>(tmp_29_cast_fu_4821_p1.read()));
}

void mnist_fp16::thread_tmp_187_fu_7526_p1() {
    tmp_187_fu_7526_p1 = esl_zext<64,2>(p_46_reg_1866.read());
}

void mnist_fp16::thread_tmp_188_fu_6592_p2() {
    tmp_188_fu_6592_p2 = (!ap_const_lv16_0.is_01() || !relu2_0_V_q0.read().is_01())? sc_lv<16>(): (sc_biguint<16>(ap_const_lv16_0) - sc_biguint<16>(relu2_0_V_q0.read()));
}

void mnist_fp16::thread_tmp_189_fu_4830_p1() {
    tmp_189_fu_4830_p1 = tmp_186_fu_4825_p2.read().range(8-1, 0);
}

void mnist_fp16::thread_tmp_18_cast_fu_3658_p1() {
    tmp_18_cast_fu_3658_p1 = esl_zext<10,5>(p_11_reg_1446.read());
}

void mnist_fp16::thread_tmp_18_fu_3390_p2() {
    tmp_18_fu_3390_p2 = (!p_shl14_cast_fu_3386_p1.read().is_01() || !p_shl16_cast1_fu_3360_p1.read().is_01())? sc_lv<9>(): (sc_biguint<9>(p_shl14_cast_fu_3386_p1.read()) - sc_biguint<9>(p_shl16_cast1_fu_3360_p1.read()));
}

void mnist_fp16::thread_tmp_190_fu_4842_p3() {
    tmp_190_fu_4842_p3 = esl_concat<10,2>(tmp_186_fu_4825_p2.read(), ap_const_lv2_0);
}

void mnist_fp16::thread_tmp_191_fu_4854_p2() {
    tmp_191_fu_4854_p2 = (!p_shl38_cast_fu_4834_p3.read().is_01() || !p_shl39_cast_fu_4850_p1.read().is_01())? sc_lv<13>(): (sc_biguint<13>(p_shl38_cast_fu_4834_p3.read()) - sc_bigint<13>(p_shl39_cast_fu_4850_p1.read()));
}

void mnist_fp16::thread_tmp_192_cast_fu_11193_p1() {
    tmp_192_cast_fu_11193_p1 = esl_zext<10,3>(p_49_reg_2291.read());
}

void mnist_fp16::thread_tmp_192_fu_4192_p2() {
    tmp_192_fu_4192_p2 = (!p_4_reg_1490.read().is_01() || !ap_const_lv5_1F.is_01())? sc_lv<1>(): sc_lv<1>(p_4_reg_1490.read() == ap_const_lv5_1F);
}

void mnist_fp16::thread_tmp_193_fu_9529_p2() {
    tmp_193_fu_9529_p2 = (!p_Val2_1_reg_2040.read().is_01() || !ap_const_lv16_0.is_01())? sc_lv<1>(): sc_lv<1>(p_Val2_1_reg_2040.read() == ap_const_lv16_0);
}

void mnist_fp16::thread_tmp_194_cast_fu_8826_p1() {
    tmp_194_cast_fu_8826_p1 = esl_zext<8,4>(p_51_reg_2052.read());
}

void mnist_fp16::thread_tmp_195_fu_8578_p2() {
    tmp_195_fu_8578_p2 = (!ap_const_lv32_1F.is_01() || !msb_idx_7_cast_fu_8555_p1.read().is_01())? sc_lv<32>(): (sc_biguint<32>(ap_const_lv32_1F) - sc_biguint<32>(msb_idx_7_cast_fu_8555_p1.read()));
}

void mnist_fp16::thread_tmp_196_fu_4198_p2() {
    tmp_196_fu_4198_p2 = (grp_fu_2879_p2.read() | tmp_192_fu_4192_p2.read());
}

void mnist_fp16::thread_tmp_197_fu_4204_p2() {
    tmp_197_fu_4204_p2 = (!p_4_reg_1490.read().is_01() || !ap_const_lv5_1D.is_01())? sc_lv<1>(): sc_lv<1>(p_4_reg_1490.read() == ap_const_lv5_1D);
}

void mnist_fp16::thread_tmp_198_fu_5516_p1() {
    tmp_198_fu_5516_p1 = esl_sext<32,16>(tmp_561_reg_16075.read());
}

void mnist_fp16::thread_tmp_199_fu_5519_p2() {
    tmp_199_fu_5519_p2 = (!sh_amt_3_cast_fu_5488_p1.read().is_01())? sc_lv<32>(): tmp_198_fu_5516_p1.read() << (unsigned short)sh_amt_3_cast_fu_5488_p1.read().to_uint();
}

void mnist_fp16::thread_tmp_19_fu_3093_p2() {
    tmp_19_fu_3093_p2 = (!p_s_reg_1314.read().is_01() || !ap_const_lv5_1F.is_01())? sc_lv<1>(): sc_lv<1>(p_s_reg_1314.read() == ap_const_lv5_1F);
}

void mnist_fp16::thread_tmp_1_fu_2946_p3() {
    tmp_1_fu_2946_p3 = esl_concat<5,5>(p_0_reg_1197.read(), ap_const_lv5_0);
}

void mnist_fp16::thread_tmp_200_fu_6760_p3() {
    tmp_200_fu_6760_p3 = esl_concat<1,8>(tmp_603_reg_16360.read(), p_Repl2_7_trunc_fu_6754_p2.read());
}

void mnist_fp16::thread_tmp_201_fu_4210_p2() {
    tmp_201_fu_4210_p2 = (tmp_197_fu_4204_p2.read() | tmp_196_fu_4198_p2.read());
}

void mnist_fp16::thread_tmp_202_fu_4216_p2() {
    tmp_202_fu_4216_p2 = (!p_4_reg_1490.read().is_01() || !ap_const_lv5_0.is_01())? sc_lv<1>(): sc_lv<1>(p_4_reg_1490.read() == ap_const_lv5_0);
}

void mnist_fp16::thread_tmp_203_fu_4222_p2() {
    tmp_203_fu_4222_p2 = (tmp_202_fu_4216_p2.read() | tmp_201_fu_4210_p2.read());
}

void mnist_fp16::thread_tmp_204_fu_3576_p2() {
    tmp_204_fu_3576_p2 = (!tmp_164_reg_15527.read().is_01() || !tmp_34_cast_fu_3572_p1.read().is_01())? sc_lv<11>(): (sc_biguint<11>(tmp_164_reg_15527.read()) + sc_biguint<11>(tmp_34_cast_fu_3572_p1.read()));
}

void mnist_fp16::thread_tmp_205_fu_6681_p2() {
    tmp_205_fu_6681_p2 = (!ap_const_lv32_1F.is_01() || !msb_idx_5_cast_fu_6658_p1.read().is_01())? sc_lv<32>(): (sc_biguint<32>(ap_const_lv32_1F) - sc_biguint<32>(msb_idx_5_cast_fu_6658_p1.read()));
}

void mnist_fp16::thread_tmp_206_cast_fu_12493_p1() {
    tmp_206_cast_fu_12493_p1 = esl_zext<9,5>(p_53_reg_2477.read());
}

void mnist_fp16::thread_tmp_206_fu_3590_p2() {
    tmp_206_fu_3590_p2 = (!tmp_177_reg_15532.read().is_01() || !tmp_35_cast_fu_3586_p1.read().is_01())? sc_lv<7>(): (sc_biguint<7>(tmp_177_reg_15532.read()) + sc_biguint<7>(tmp_35_cast_fu_3586_p1.read()));
}

void mnist_fp16::thread_tmp_207_fu_4228_p2() {
    tmp_207_fu_4228_p2 = (!p_14_reg_1502.read().is_01() || !ap_const_lv5_1F.is_01())? sc_lv<1>(): sc_lv<1>(p_14_reg_1502.read() == ap_const_lv5_1F);
}

void mnist_fp16::thread_tmp_208_fu_4234_p2() {
    tmp_208_fu_4234_p2 = (!p_14_reg_1502.read().is_01() || !ap_const_lv5_1E.is_01())? sc_lv<1>(): sc_lv<1>(p_14_reg_1502.read() == ap_const_lv5_1E);
}

void mnist_fp16::thread_tmp_209_fu_6738_p2() {
    tmp_209_fu_6738_p2 = (!p_Result_16_fu_6728_p4.read().is_01() || !ap_const_lv8_9E.is_01())? sc_lv<1>(): sc_lv<1>(p_Result_16_fu_6728_p4.read() != ap_const_lv8_9E);
}

void mnist_fp16::thread_tmp_20_cast_fu_3709_p1() {
    tmp_20_cast_fu_3709_p1 = esl_zext<13,5>(p_16_reg_1457.read());
}

void mnist_fp16::thread_tmp_210_fu_7812_p1() {
    tmp_210_fu_7812_p1 = esl_zext<12,11>(p_Result_18_reg_16720.read());
}

void mnist_fp16::thread_tmp_211_fu_6792_p4() {
    tmp_211_fu_6792_p4 = p_03_i1_to_int_fu_6789_p1.read().range(30, 23);
}

void mnist_fp16::thread_tmp_212_fu_7806_p2() {
    tmp_212_fu_7806_p2 = (!tmp_623_fu_7780_p1.read().is_01() || !ap_const_lv63_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_623_fu_7780_p1.read() == ap_const_lv63_0);
}

void mnist_fp16::thread_tmp_213_fu_4240_p2() {
    tmp_213_fu_4240_p2 = (tmp_208_fu_4234_p2.read() | tmp_207_fu_4228_p2.read());
}

void mnist_fp16::thread_tmp_214_cast_fu_7593_p1() {
    tmp_214_cast_fu_7593_p1 = esl_zext<10,2>(p_52_reg_1889.read());
}

void mnist_fp16::thread_tmp_214_fu_4246_p2() {
    tmp_214_fu_4246_p2 = (!p_14_reg_1502.read().is_01() || !ap_const_lv5_1D.is_01())? sc_lv<1>(): sc_lv<1>(p_14_reg_1502.read() == ap_const_lv5_1D);
}

void mnist_fp16::thread_tmp_216_fu_4252_p2() {
    tmp_216_fu_4252_p2 = (tmp_214_fu_4246_p2.read() | tmp_213_fu_4240_p2.read());
}

void mnist_fp16::thread_tmp_217_fu_6810_p4() {
    tmp_217_fu_6810_p4 = tmp_140_to_int_fu_6806_p1.read().range(30, 23);
}

void mnist_fp16::thread_tmp_218_fu_4258_p2() {
    tmp_218_fu_4258_p2 = (!p_14_reg_1502.read().is_01() || !ap_const_lv5_0.is_01())? sc_lv<1>(): sc_lv<1>(p_14_reg_1502.read() == ap_const_lv5_0);
}

void mnist_fp16::thread_tmp_219_fu_6836_p2() {
    tmp_219_fu_6836_p2 = (notrhs2_fu_6830_p2.read() | notlhs2_fu_6824_p2.read());
}

void mnist_fp16::thread_tmp_21_fu_3726_p4() {
    tmp_21_fu_3726_p4 = conv1_0_load_to_int_fu_3723_p1.read().range(30, 23);
}

void mnist_fp16::thread_tmp_220_fu_6854_p2() {
    tmp_220_fu_6854_p2 = (notrhs3_fu_6848_p2.read() | notlhs3_fu_6842_p2.read());
}

void mnist_fp16::thread_tmp_221_fu_6860_p2() {
    tmp_221_fu_6860_p2 = (tmp_219_fu_6836_p2.read() & tmp_220_fu_6854_p2.read());
}

void mnist_fp16::thread_tmp_223_fu_6866_p2() {
    tmp_223_fu_6866_p2 = (tmp_221_fu_6860_p2.read() & grp_fu_2839_p2.read());
}

void mnist_fp16::thread_tmp_224_fu_9953_p3() {
    tmp_224_fu_9953_p3 = esl_concat<1,52>(ap_const_lv1_1, tmp_700_reg_17309.read());
}

void mnist_fp16::thread_tmp_225_fu_7733_p4() {
    tmp_225_fu_7733_p4 = conv3_0_load_to_int_fu_7730_p1.read().range(30, 23);
}

void mnist_fp16::thread_tmp_226_fu_8857_p2() {
    tmp_226_fu_8857_p2 = (!ap_const_lv16_0.is_01() || !p_Val2_1_reg_2040.read().is_01())? sc_lv<16>(): (sc_biguint<16>(ap_const_lv16_0) - sc_biguint<16>(p_Val2_1_reg_2040.read()));
}

void mnist_fp16::thread_tmp_227_fu_4264_p2() {
    tmp_227_fu_4264_p2 = (tmp_218_fu_4258_p2.read() | tmp_216_fu_4252_p2.read());
}

void mnist_fp16::thread_tmp_228_fu_6328_p3() {
    tmp_228_fu_6328_p3 = esl_concat<3,5>(p_22_reg_1669.read(), ap_const_lv5_0);
}

void mnist_fp16::thread_tmp_229_fu_6340_p3() {
    tmp_229_fu_6340_p3 = esl_concat<3,2>(p_22_reg_1669.read(), ap_const_lv2_0);
}

void mnist_fp16::thread_tmp_22_fu_3099_p2() {
    tmp_22_fu_3099_p2 = (grp_fu_2872_p2.read() | tmp_19_fu_3093_p2.read());
}

void mnist_fp16::thread_tmp_230_fu_8635_p2() {
    tmp_230_fu_8635_p2 = (!p_Result_21_fu_8625_p4.read().is_01() || !ap_const_lv8_9E.is_01())? sc_lv<1>(): sc_lv<1>(p_Result_21_fu_8625_p4.read() != ap_const_lv8_9E);
}

void mnist_fp16::thread_tmp_231_fu_7759_p2() {
    tmp_231_fu_7759_p2 = (notrhs4_reg_16704.read() | notlhs4_reg_16699.read());
}

void mnist_fp16::thread_tmp_232_fu_7845_p2() {
    tmp_232_fu_7845_p2 = (!F2_4_fu_7839_p2.read().is_01() || !ap_const_lv12_E.is_01())? sc_lv<1>(): (sc_bigint<12>(F2_4_fu_7839_p2.read()) > sc_bigint<12>(ap_const_lv12_E));
}

void mnist_fp16::thread_tmp_233_fu_7851_p2() {
    tmp_233_fu_7851_p2 = (!ap_const_lv12_FF2.is_01() || !F2_4_fu_7839_p2.read().is_01())? sc_lv<12>(): (sc_bigint<12>(ap_const_lv12_FF2) + sc_biguint<12>(F2_4_fu_7839_p2.read()));
}

void mnist_fp16::thread_tmp_234_fu_7857_p2() {
    tmp_234_fu_7857_p2 = (!ap_const_lv12_E.is_01() || !F2_4_fu_7839_p2.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_E) - sc_biguint<12>(F2_4_fu_7839_p2.read()));
}

void mnist_fp16::thread_tmp_235_fu_7871_p2() {
    tmp_235_fu_7871_p2 = (!F2_4_fu_7839_p2.read().is_01() || !ap_const_lv12_E.is_01())? sc_lv<1>(): sc_lv<1>(F2_4_fu_7839_p2.read() == ap_const_lv12_E);
}

void mnist_fp16::thread_tmp_237_fu_6352_p2() {
    tmp_237_fu_6352_p2 = (!p_shl40_cast_fu_6336_p1.read().is_01() || !p_shl41_cast_fu_6348_p1.read().is_01())? sc_lv<9>(): (sc_biguint<9>(p_shl40_cast_fu_6336_p1.read()) - sc_biguint<9>(p_shl41_cast_fu_6348_p1.read()));
}

void mnist_fp16::thread_tmp_238_cast_fu_10197_p1() {
    tmp_238_cast_fu_10197_p1 = esl_zext<8,4>(p_54_reg_2143.read());
}

void mnist_fp16::thread_tmp_238_fu_6362_p3() {
    tmp_238_fu_6362_p3 = esl_concat<3,4>(p_22_reg_1669.read(), ap_const_lv4_0);
}

void mnist_fp16::thread_tmp_239_cast_fu_9803_p1() {
    tmp_239_cast_fu_9803_p1 = esl_zext<10,4>(p_55_reg_2121.read());
}

void mnist_fp16::thread_tmp_239_fu_6374_p3() {
    tmp_239_fu_6374_p3 = esl_concat<3,1>(p_22_reg_1669.read(), ap_const_lv1_0);
}

void mnist_fp16::thread_tmp_23_fu_3752_p2() {
    tmp_23_fu_3752_p2 = (notrhs_reg_15621.read() | notlhs_reg_15616.read());
}

void mnist_fp16::thread_tmp_240_fu_7763_p2() {
    tmp_240_fu_7763_p2 = (tmp_231_fu_7759_p2.read() & tmp_236_reg_16709.read());
}

void mnist_fp16::thread_tmp_242_fu_8657_p3() {
    tmp_242_fu_8657_p3 = esl_concat<1,8>(is_neg_2_reg_16905.read(), p_Repl2_10_trunc_fu_8651_p2.read());
}

void mnist_fp16::thread_tmp_243_fu_7768_p1() {
    tmp_243_fu_7768_p1 = grp_fu_2830_p1.read();
}

void mnist_fp16::thread_tmp_244_fu_6386_p2() {
    tmp_244_fu_6386_p2 = (!p_shl42_cast_fu_6370_p1.read().is_01() || !p_shl43_cast_fu_6382_p1.read().is_01())? sc_lv<8>(): (sc_biguint<8>(p_shl42_cast_fu_6370_p1.read()) - sc_biguint<8>(p_shl43_cast_fu_6382_p1.read()));
}

void mnist_fp16::thread_tmp_245_fu_7935_p2() {
    tmp_245_fu_7935_p2 = (!sh_amt_4_reg_16741.read().is_01() || !ap_const_lv12_36.is_01())? sc_lv<1>(): (sc_biguint<12>(sh_amt_4_reg_16741.read()) < sc_biguint<12>(ap_const_lv12_36));
}

void mnist_fp16::thread_tmp_246_fu_5938_p2() {
    tmp_246_fu_5938_p2 = (!tmp_598_cast_reg_16160.read().is_01() || !tmp_62_cast_fu_5934_p1.read().is_01())? sc_lv<10>(): (sc_bigint<10>(tmp_598_cast_reg_16160.read()) + sc_biguint<10>(tmp_62_cast_fu_5934_p1.read()));
}

void mnist_fp16::thread_tmp_247_fu_7940_p1() {
    tmp_247_fu_7940_p1 = esl_zext<54,32>(sh_amt_4_cast_fu_7932_p1.read());
}

void mnist_fp16::thread_tmp_248_cast1_fu_11954_p1() {
    tmp_248_cast1_fu_11954_p1 = esl_zext<8,4>(p_57_reg_2439.read());
}

void mnist_fp16::thread_tmp_248_fu_7944_p2() {
    tmp_248_fu_7944_p2 = (!man_V_7_reg_16736.read().is_01() || !tmp_247_fu_7940_p1.read().is_01())? sc_lv<54>(): sc_bigint<54>(man_V_7_reg_16736.read()) >> (unsigned short)tmp_247_fu_7940_p1.read().to_uint();
}

void mnist_fp16::thread_tmp_249_fu_5943_p1() {
    tmp_249_fu_5943_p1 = tmp_246_fu_5938_p2.read().range(8-1, 0);
}

void mnist_fp16::thread_tmp_24_cast_fu_3197_p1() {
    tmp_24_cast_fu_3197_p1 = esl_zext<6,5>(p_3_reg_1326.read());
}

void mnist_fp16::thread_tmp_250_fu_7960_p1() {
    tmp_250_fu_7960_p1 = esl_sext<32,16>(tmp_626_reg_16752.read());
}

void mnist_fp16::thread_tmp_251_fu_7963_p2() {
    tmp_251_fu_7963_p2 = (!sh_amt_4_cast_fu_7932_p1.read().is_01())? sc_lv<32>(): tmp_250_fu_7960_p1.read() << (unsigned short)sh_amt_4_cast_fu_7932_p1.read().to_uint();
}

void mnist_fp16::thread_tmp_252_cast_fu_11966_p1() {
    tmp_252_cast_fu_11966_p1 = esl_zext<8,7>(tmp_252_fu_11958_p3.read());
}

void mnist_fp16::thread_tmp_252_fu_11958_p3() {
    tmp_252_fu_11958_p3 = esl_concat<4,3>(p_57_reg_2439.read(), ap_const_lv3_0);
}

void mnist_fp16::thread_tmp_253_fu_10887_p3() {
    tmp_253_fu_10887_p3 = (!tmp_744_fu_10881_p2.read()[0].is_01())? sc_lv<3>(): ((tmp_744_fu_10881_p2.read()[0].to_bool())? r_V_22_fu_10875_p2.read(): ap_const_lv3_0);
}

void mnist_fp16::thread_tmp_254_cast_fu_11095_p1() {
    tmp_254_cast_fu_11095_p1 = esl_zext<10,3>(tmp_253_reg_17561.read());
}

void mnist_fp16::thread_tmp_254_fu_5955_p3() {
    tmp_254_fu_5955_p3 = esl_concat<10,2>(tmp_246_fu_5938_p2.read(), ap_const_lv2_0);
}

void mnist_fp16::thread_tmp_255_cast_fu_9854_p1() {
    tmp_255_cast_fu_9854_p1 = esl_zext<12,4>(p_60_reg_2132.read());
}

void mnist_fp16::thread_tmp_255_fu_5967_p2() {
    tmp_255_fu_5967_p2 = (!p_shl44_cast_fu_5947_p3.read().is_01() || !p_shl45_cast_fu_5963_p1.read().is_01())? sc_lv<13>(): (sc_biguint<13>(p_shl44_cast_fu_5947_p3.read()) - sc_bigint<13>(p_shl45_cast_fu_5963_p1.read()));
}

void mnist_fp16::thread_tmp_256_fu_4276_p2() {
    tmp_256_fu_4276_p2 = (!r_V_4_fu_4270_p2.read().is_01() || !ap_const_lv5_1C.is_01())? sc_lv<1>(): (sc_biguint<5>(r_V_4_fu_4270_p2.read()) < sc_biguint<5>(ap_const_lv5_1C));
}

void mnist_fp16::thread_tmp_258_fu_4282_p2() {
    tmp_258_fu_4282_p2 = (!ap_const_lv5_3.is_01() || !p_14_reg_1502.read().is_01())? sc_lv<5>(): (sc_biguint<5>(ap_const_lv5_3) + sc_biguint<5>(p_14_reg_1502.read()));
}

void mnist_fp16::thread_tmp_259_fu_8885_p1() {
    tmp_259_fu_8885_p1 = esl_zext<64,2>(p_61_reg_2076.read());
}

void mnist_fp16::thread_tmp_25_cast_fu_3536_p1() {
    tmp_25_cast_fu_3536_p1 = esl_zext<13,5>(p_8_reg_1376.read());
}

void mnist_fp16::thread_tmp_25_fu_3105_p2() {
    tmp_25_fu_3105_p2 = (!p_s_reg_1314.read().is_01() || !ap_const_lv5_1D.is_01())? sc_lv<1>(): sc_lv<1>(p_s_reg_1314.read() == ap_const_lv5_1D);
}

void mnist_fp16::thread_tmp_260_cast1_fu_8679_p1() {
    tmp_260_cast1_fu_8679_p1 = esl_zext<13,5>(p_39_reg_1978.read());
}

void mnist_fp16::thread_tmp_260_cast_fu_10895_p1() {
    tmp_260_cast_fu_10895_p1 = esl_zext<4,3>(tmp_253_fu_10887_p3.read());
}

void mnist_fp16::thread_tmp_260_fu_4332_p1() {
    tmp_260_fu_4332_p1 = mul1_fu_15237_p2.read().range(26-1, 0);
}

void mnist_fp16::thread_tmp_262_fu_9871_p4() {
    tmp_262_fu_9871_p4 = conv4_0_load_to_int_fu_9868_p1.read().range(30, 23);
}

void mnist_fp16::thread_tmp_265_fu_4369_p4() {
    tmp_265_fu_4369_p4 = neg_mul1_fu_4364_p2.read().range(25, 18);
}

void mnist_fp16::thread_tmp_266_fu_4379_p1() {
    tmp_266_fu_4379_p1 = esl_sext<13,8>(tmp_265_fu_4369_p4.read());
}

void mnist_fp16::thread_tmp_267_cast_fu_11061_p1() {
    tmp_267_cast_fu_11061_p1 = esl_zext<7,3>(r_V_23_reg_17604.read());
}

void mnist_fp16::thread_tmp_269_cast_fu_11125_p1() {
    tmp_269_cast_fu_11125_p1 = esl_zext<11,4>(p_56_reg_2256.read());
}

void mnist_fp16::thread_tmp_269_fu_4383_p1() {
    tmp_269_fu_4383_p1 = esl_sext<13,10>(tmp_267_reg_15765.read());
}

void mnist_fp16::thread_tmp_26_fu_3111_p2() {
    tmp_26_fu_3111_p2 = (tmp_25_fu_3105_p2.read() | tmp_22_fu_3099_p2.read());
}

void mnist_fp16::thread_tmp_270_fu_9897_p2() {
    tmp_270_fu_9897_p2 = (notrhs5_reg_17288.read() | notlhs5_reg_17283.read());
}

void mnist_fp16::thread_tmp_272_fu_9623_p2() {
    tmp_272_fu_9623_p2 = (!ap_const_lv32_1F.is_01() || !msb_idx_9_cast_fu_9600_p1.read().is_01())? sc_lv<32>(): (sc_biguint<32>(ap_const_lv32_1F) - sc_biguint<32>(msb_idx_9_cast_fu_9600_p1.read()));
}

void mnist_fp16::thread_tmp_273_fu_4386_p3() {
    tmp_273_fu_4386_p3 = (!tmp_264_reg_15757.read()[0].is_01())? sc_lv<13>(): ((tmp_264_reg_15757.read()[0].to_bool())? tmp_266_fu_4379_p1.read(): tmp_269_fu_4383_p1.read());
}

void mnist_fp16::thread_tmp_274_cast_fu_12547_p1() {
    tmp_274_cast_fu_12547_p1 = esl_zext<10,3>(p_62_reg_2488.read());
}

void mnist_fp16::thread_tmp_274_fu_4462_p1() {
    tmp_274_fu_4462_p1 = grp_fu_4406_p2.read().range(9-1, 0);
}

void mnist_fp16::thread_tmp_275_fu_9689_p2() {
    tmp_275_fu_9689_p2 = (!p_Result_28_fu_9679_p4.read().is_01() || !ap_const_lv8_9E.is_01())? sc_lv<1>(): sc_lv<1>(p_Result_28_fu_9679_p4.read() != ap_const_lv8_9E);
}

void mnist_fp16::thread_tmp_276_fu_4352_p1() {
    tmp_276_fu_4352_p1 = mul2_fu_15245_p2.read().range(27-1, 0);
}

void mnist_fp16::thread_tmp_277_fu_9950_p1() {
    tmp_277_fu_9950_p1 = esl_zext<12,11>(p_Result_24_reg_17304.read());
}

void mnist_fp16::thread_tmp_278_fu_9901_p2() {
    tmp_278_fu_9901_p2 = (tmp_270_fu_9897_p2.read() & tmp_271_reg_17293.read());
}

void mnist_fp16::thread_tmp_279_fu_9944_p2() {
    tmp_279_fu_9944_p2 = (!tmp_698_fu_9918_p1.read().is_01() || !ap_const_lv63_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_698_fu_9918_p1.read() == ap_const_lv63_0);
}

void mnist_fp16::thread_tmp_27_cast_fu_3515_p1() {
    tmp_27_cast_fu_3515_p1 = esl_zext<7,2>(p_12_reg_1388.read());
}

void mnist_fp16::thread_tmp_27_fu_3117_p2() {
    tmp_27_fu_3117_p2 = (!p_s_reg_1314.read().is_01() || !ap_const_lv5_0.is_01())? sc_lv<1>(): sc_lv<1>(p_s_reg_1314.read() == ap_const_lv5_0);
}

void mnist_fp16::thread_tmp_281_fu_9906_p1() {
    tmp_281_fu_9906_p1 = grp_fu_2830_p1.read();
}

void mnist_fp16::thread_tmp_282_fu_4417_p4() {
    tmp_282_fu_4417_p4 = neg_mul2_fu_4412_p2.read().range(26, 23);
}

void mnist_fp16::thread_tmp_283_fu_9711_p3() {
    tmp_283_fu_9711_p3 = esl_concat<1,8>(is_neg_3_reg_17186.read(), p_Repl2_13_trunc_fu_9705_p2.read());
}

void mnist_fp16::thread_tmp_284_fu_9038_p1() {
    tmp_284_fu_9038_p1 = esl_zext<12,11>(exp_tmp_V_2_reg_17060.read());
}

void mnist_fp16::thread_tmp_285_fu_9983_p2() {
    tmp_285_fu_9983_p2 = (!F2_5_fu_9977_p2.read().is_01() || !ap_const_lv12_E.is_01())? sc_lv<1>(): (sc_bigint<12>(F2_5_fu_9977_p2.read()) > sc_bigint<12>(ap_const_lv12_E));
}

void mnist_fp16::thread_tmp_286_fu_8996_p2() {
    tmp_286_fu_8996_p2 = (!tmp_736_fu_8970_p1.read().is_01() || !ap_const_lv63_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_736_fu_8970_p1.read() == ap_const_lv63_0);
}

void mnist_fp16::thread_tmp_287_fu_14452_p3() {
    tmp_287_fu_14452_p3 = esl_concat<1,52>(ap_const_lv1_1, tmp_861_reg_18612.read());
}

void mnist_fp16::thread_tmp_288_fu_9989_p2() {
    tmp_288_fu_9989_p2 = (!ap_const_lv12_FF2.is_01() || !F2_5_fu_9977_p2.read().is_01())? sc_lv<12>(): (sc_bigint<12>(ap_const_lv12_FF2) + sc_biguint<12>(F2_5_fu_9977_p2.read()));
}

void mnist_fp16::thread_tmp_289_fu_9995_p2() {
    tmp_289_fu_9995_p2 = (!ap_const_lv12_E.is_01() || !F2_5_fu_9977_p2.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_E) - sc_biguint<12>(F2_5_fu_9977_p2.read()));
}

void mnist_fp16::thread_tmp_28_cast_fu_3201_p1() {
    tmp_28_cast_fu_3201_p1 = esl_zext<6,5>(tmp_8_fu_3189_p3.read());
}

void mnist_fp16::thread_tmp_28_fu_3756_p2() {
    tmp_28_fu_3756_p2 = (tmp_23_fu_3752_p2.read() & tmp_24_reg_15626.read());
}

void mnist_fp16::thread_tmp_290_cast_fu_11305_p1() {
    tmp_290_cast_fu_11305_p1 = esl_zext<11,3>(p_58_reg_2302.read());
}

void mnist_fp16::thread_tmp_290_fu_4427_p1() {
    tmp_290_fu_4427_p1 = esl_sext<14,4>(tmp_282_fu_4417_p4.read());
}

void mnist_fp16::thread_tmp_291_cast1_fu_11252_p1() {
    tmp_291_cast1_fu_11252_p1 = esl_zext<8,4>(p_64_reg_2314.read());
}

void mnist_fp16::thread_tmp_291_cast_fu_11256_p1() {
    tmp_291_cast_fu_11256_p1 = esl_zext<9,4>(p_64_reg_2314.read());
}

void mnist_fp16::thread_tmp_292_cast_fu_9666_p1() {
    tmp_292_cast_fu_9666_p1 = esl_zext<12,4>(p_44_reg_2028.read());
}

void mnist_fp16::thread_tmp_292_fu_4431_p1() {
    tmp_292_fu_4431_p1 = esl_sext<14,5>(tmp_291_reg_15775.read());
}

void mnist_fp16::thread_tmp_293_fu_9041_p3() {
    tmp_293_fu_9041_p3 = esl_concat<1,52>(ap_const_lv1_1, tmp_738_reg_17065.read());
}

void mnist_fp16::thread_tmp_294_fu_12030_p3() {
    tmp_294_fu_12030_p3 = (!tmp_822_fu_12024_p2.read()[0].is_01())? sc_lv<3>(): ((tmp_822_fu_12024_p2.read()[0].to_bool())? r_V_25_fu_12018_p2.read(): ap_const_lv3_0);
}

void mnist_fp16::thread_tmp_295_cast_fu_12238_p1() {
    tmp_295_cast_fu_12238_p1 = esl_zext<11,3>(tmp_294_reg_17922.read());
}

void mnist_fp16::thread_tmp_295_fu_4441_p1() {
    tmp_295_fu_4441_p1 = p_v_fu_4434_p3.read().range(2-1, 0);
}

void mnist_fp16::thread_tmp_296_fu_10009_p2() {
    tmp_296_fu_10009_p2 = (!F2_5_fu_9977_p2.read().is_01() || !ap_const_lv12_E.is_01())? sc_lv<1>(): sc_lv<1>(F2_5_fu_9977_p2.read() == ap_const_lv12_E);
}

void mnist_fp16::thread_tmp_297_cast_fu_12038_p1() {
    tmp_297_cast_fu_12038_p1 = esl_zext<4,3>(tmp_294_fu_12030_p3.read());
}

void mnist_fp16::thread_tmp_297_fu_4451_p1() {
    tmp_297_fu_4451_p1 = p_v_fu_4434_p3.read().range(2-1, 0);
}

void mnist_fp16::thread_tmp_29_cast_fu_4821_p1() {
    tmp_29_cast_fu_4821_p1 = esl_zext<10,5>(p_10_reg_1543.read());
}

void mnist_fp16::thread_tmp_29_fu_3123_p2() {
    tmp_29_fu_3123_p2 = (tmp_27_fu_3117_p2.read() | tmp_26_fu_3111_p2.read());
}

void mnist_fp16::thread_tmp_2_fu_2976_p2() {
    tmp_2_fu_2976_p2 = (!ap_phi_mux_p_1_phi_fu_1212_p4.read().is_01() || !ap_const_lv5_1C.is_01())? sc_lv<1>(): sc_lv<1>(ap_phi_mux_p_1_phi_fu_1212_p4.read() == ap_const_lv5_1C);
}

void mnist_fp16::thread_tmp_300_fu_10073_p2() {
    tmp_300_fu_10073_p2 = (!sh_amt_5_reg_17325.read().is_01() || !ap_const_lv12_36.is_01())? sc_lv<1>(): (sc_biguint<12>(sh_amt_5_reg_17325.read()) < sc_biguint<12>(ap_const_lv12_36));
}

void mnist_fp16::thread_tmp_301_fu_4466_p3() {
    tmp_301_fu_4466_p3 = esl_concat<2,5>(r_V_6_reg_15785.read(), ap_const_lv5_0);
}

void mnist_fp16::thread_tmp_302_fu_10078_p1() {
    tmp_302_fu_10078_p1 = esl_zext<54,32>(sh_amt_5_cast_fu_10070_p1.read());
}

void mnist_fp16::thread_tmp_303_fu_10082_p2() {
    tmp_303_fu_10082_p2 = (!man_V_16_reg_17320.read().is_01() || !tmp_302_fu_10078_p1.read().is_01())? sc_lv<54>(): sc_bigint<54>(man_V_16_reg_17320.read()) >> (unsigned short)tmp_302_fu_10078_p1.read().to_uint();
}

void mnist_fp16::thread_tmp_304_fu_4477_p3() {
    tmp_304_fu_4477_p3 = esl_concat<2,2>(r_V_6_reg_15785.read(), ap_const_lv2_0);
}

void mnist_fp16::thread_tmp_305_fu_10098_p1() {
    tmp_305_fu_10098_p1 = esl_sext<32,16>(tmp_701_reg_17336.read());
}

void mnist_fp16::thread_tmp_307_fu_10101_p2() {
    tmp_307_fu_10101_p2 = (!sh_amt_5_cast_fu_10070_p1.read().is_01())? sc_lv<32>(): tmp_305_fu_10098_p1.read() << (unsigned short)sh_amt_5_cast_fu_10070_p1.read().to_uint();
}

void mnist_fp16::thread_tmp_308_fu_9071_p2() {
    tmp_308_fu_9071_p2 = (!F2_6_fu_9065_p2.read().is_01() || !ap_const_lv12_E.is_01())? sc_lv<1>(): (sc_bigint<12>(F2_6_fu_9065_p2.read()) > sc_bigint<12>(ap_const_lv12_E));
}

void mnist_fp16::thread_tmp_309_fu_9077_p2() {
    tmp_309_fu_9077_p2 = (!ap_const_lv12_FF2.is_01() || !F2_6_fu_9065_p2.read().is_01())? sc_lv<12>(): (sc_bigint<12>(ap_const_lv12_FF2) + sc_biguint<12>(F2_6_fu_9065_p2.read()));
}

void mnist_fp16::thread_tmp_310_fu_9083_p2() {
    tmp_310_fu_9083_p2 = (!ap_const_lv12_E.is_01() || !F2_6_fu_9065_p2.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_E) - sc_biguint<12>(F2_6_fu_9065_p2.read()));
}

void mnist_fp16::thread_tmp_311_fu_9097_p2() {
    tmp_311_fu_9097_p2 = (!F2_6_fu_9065_p2.read().is_01() || !ap_const_lv12_E.is_01())? sc_lv<1>(): sc_lv<1>(F2_6_fu_9065_p2.read() == ap_const_lv12_E);
}

void mnist_fp16::thread_tmp_312_cast_fu_11459_p1() {
    tmp_312_cast_fu_11459_p1 = esl_zext<9,5>(p_68_reg_2384.read());
}

void mnist_fp16::thread_tmp_312_fu_4488_p2() {
    tmp_312_fu_4488_p2 = (!p_shl32_cast_fu_4473_p1.read().is_01() || !p_shl33_cast_fu_4484_p1.read().is_01())? sc_lv<8>(): (sc_biguint<8>(p_shl32_cast_fu_4473_p1.read()) - sc_biguint<8>(p_shl33_cast_fu_4484_p1.read()));
}

void mnist_fp16::thread_tmp_313_cast_fu_10354_p1() {
    tmp_313_cast_fu_10354_p1 = esl_zext<4,2>(p_70_reg_2189.read());
}

void mnist_fp16::thread_tmp_313_fu_4498_p2() {
    tmp_313_fu_4498_p2 = (!tmp_274_fu_4462_p1.read().is_01() || !tmp_628_cast_fu_4494_p1.read().is_01())? sc_lv<9>(): (sc_biguint<9>(tmp_274_fu_4462_p1.read()) + sc_bigint<9>(tmp_628_cast_fu_4494_p1.read()));
}

void mnist_fp16::thread_tmp_314_cast_cast_fu_10363_p1() {
    tmp_314_cast_cast_fu_10363_p1 = esl_zext<10,4>(tmp_314_fu_10358_p2.read());
}

void mnist_fp16::thread_tmp_314_fu_10358_p2() {
    tmp_314_fu_10358_p2 = (!tmp_313_cast_fu_10354_p1.read().is_01() || !r_V_18_reg_17385.read().is_01())? sc_lv<4>(): (sc_biguint<4>(tmp_313_cast_fu_10354_p1.read()) + sc_biguint<4>(r_V_18_reg_17385.read()));
}

void mnist_fp16::thread_tmp_315_fu_10837_p2() {
    tmp_315_fu_10837_p2 = (!p_45_reg_2245.read().is_01() || !ap_const_lv4_0.is_01())? sc_lv<1>(): (sc_bigint<4>(p_45_reg_2245.read()) > sc_bigint<4>(ap_const_lv4_0));
}

void mnist_fp16::thread_tmp_316_fu_4504_p1() {
    tmp_316_fu_4504_p1 = tmp_313_fu_4498_p2.read().range(8-1, 0);
}

void mnist_fp16::thread_tmp_317_fu_4518_p3() {
    tmp_317_fu_4518_p3 = esl_concat<9,2>(tmp_313_reg_15791.read(), ap_const_lv2_0);
}

void mnist_fp16::thread_tmp_318_to_int_fu_10659_p1() {
    tmp_318_to_int_fu_10659_p1 = tmp_318_reg_2200.read();
}

void mnist_fp16::thread_tmp_319_fu_10860_p2() {
    tmp_319_fu_10860_p2 = (!p_56_reg_2256.read().is_01() || !ap_const_lv4_0.is_01())? sc_lv<1>(): (sc_bigint<4>(p_56_reg_2256.read()) > sc_bigint<4>(ap_const_lv4_0));
}

void mnist_fp16::thread_tmp_31_fu_3761_p1() {
    tmp_31_fu_3761_p1 = grp_fu_2830_p1.read();
}

void mnist_fp16::thread_tmp_320_fu_9211_p2() {
    tmp_320_fu_9211_p2 = (!sh_amt_6_reg_17109.read().is_01() || !ap_const_lv12_36.is_01())? sc_lv<1>(): (sc_biguint<12>(sh_amt_6_reg_17109.read()) < sc_biguint<12>(ap_const_lv12_36));
}

void mnist_fp16::thread_tmp_321_fu_4529_p2() {
    tmp_321_fu_4529_p2 = (!p_shl30_cast_fu_4511_p3.read().is_01() || !p_shl31_cast_fu_4525_p1.read().is_01())? sc_lv<13>(): (sc_biguint<13>(p_shl30_cast_fu_4511_p3.read()) - sc_bigint<13>(p_shl31_cast_fu_4525_p1.read()));
}

void mnist_fp16::thread_tmp_322_fu_9216_p1() {
    tmp_322_fu_9216_p1 = esl_zext<54,32>(sh_amt_6_cast_fu_9208_p1.read());
}

void mnist_fp16::thread_tmp_323_fu_9220_p2() {
    tmp_323_fu_9220_p2 = (!man_V_12_reg_17098.read().is_01() || !tmp_322_fu_9216_p1.read().is_01())? sc_lv<54>(): sc_bigint<54>(man_V_12_reg_17098.read()) >> (unsigned short)tmp_322_fu_9216_p1.read().to_uint();
}

void mnist_fp16::thread_tmp_324_fu_4535_p2() {
    tmp_324_fu_4535_p2 = (!tmp_38_cast_fu_4508_p1.read().is_01() || !tmp_321_fu_4529_p2.read().is_01())? sc_lv<13>(): (sc_biguint<13>(tmp_38_cast_fu_4508_p1.read()) + sc_biguint<13>(tmp_321_fu_4529_p2.read()));
}

void mnist_fp16::thread_tmp_325_fu_6901_p3() {
    tmp_325_fu_6901_p3 = esl_concat<3,4>(p_13_reg_1749.read(), ap_const_lv4_0);
}

void mnist_fp16::thread_tmp_326_fu_11639_p3() {
    tmp_326_fu_11639_p3 = esl_concat<1,52>(ap_const_lv1_1, tmp_799_reg_17820.read());
}

void mnist_fp16::thread_tmp_327_cast_fu_12204_p1() {
    tmp_327_cast_fu_12204_p1 = esl_zext<8,4>(r_V_26_reg_17965.read());
}

void mnist_fp16::thread_tmp_327_fu_4603_p1() {
    tmp_327_fu_4603_p1 = msb_idx_fu_4597_p2.read().range(31-1, 0);
}

void mnist_fp16::thread_tmp_328_cast_fu_11497_p1() {
    tmp_328_cast_fu_11497_p1 = esl_zext<10,3>(p_73_reg_2395.read());
}

void mnist_fp16::thread_tmp_329_fu_12268_p2() {
    tmp_329_fu_12268_p2 = (!relu5_0_V_q0.read().is_01() || !ap_const_lv16_0.is_01())? sc_lv<1>(): sc_lv<1>(relu5_0_V_q0.read() == ap_const_lv16_0);
}

void mnist_fp16::thread_tmp_32_fu_3805_p1() {
    tmp_32_fu_3805_p1 = esl_zext<12,11>(p_Result_2_reg_15637.read());
}

void mnist_fp16::thread_tmp_330_fu_9236_p1() {
    tmp_330_fu_9236_p1 = esl_sext<32,16>(tmp_739_reg_17121.read());
}

void mnist_fp16::thread_tmp_331_fu_9239_p2() {
    tmp_331_fu_9239_p2 = (!sh_amt_6_cast_fu_9208_p1.read().is_01())? sc_lv<32>(): tmp_330_fu_9236_p1.read() << (unsigned short)sh_amt_6_cast_fu_9208_p1.read().to_uint();
}

void mnist_fp16::thread_tmp_332_cast_cast_fu_10423_p1() {
    tmp_332_cast_cast_fu_10423_p1 = esl_zext<12,4>(tmp_332_fu_10418_p2.read());
}

void mnist_fp16::thread_tmp_332_fu_10418_p2() {
    tmp_332_fu_10418_p2 = (!tmp_591_cast_fu_10414_p1.read().is_01() || !r_V_19_reg_17403.read().is_01())? sc_lv<4>(): (sc_biguint<4>(tmp_591_cast_fu_10414_p1.read()) + sc_biguint<4>(r_V_19_reg_17403.read()));
}

void mnist_fp16::thread_tmp_333_fu_10451_p2() {
    tmp_333_fu_10451_p2 = (!relu4_0_V_load_reg_17434.read().is_01() || !ap_const_lv16_0.is_01())? sc_lv<1>(): sc_lv<1>(relu4_0_V_load_reg_17434.read() == ap_const_lv16_0);
}

void mnist_fp16::thread_tmp_334_fu_4625_p4() {
    tmp_334_fu_4625_p4 = msb_idx_1_fu_4615_p3.read().range(30, 5);
}

void mnist_fp16::thread_tmp_335_cast_fu_8952_p1() {
    tmp_335_cast_fu_8952_p1 = esl_zext<11,2>(p_66_reg_2099.read());
}

void mnist_fp16::thread_tmp_335_fu_4656_p1() {
    tmp_335_fu_4656_p1 = msb_idx_1_fu_4615_p3.read().range(4-1, 0);
}

void mnist_fp16::thread_tmp_336_fu_9123_p1() {
    tmp_336_fu_9123_p1 = esl_zext<12,11>(exp_tmp_V_3_reg_17082.read());
}

void mnist_fp16::thread_tmp_337_fu_9126_p3() {
    tmp_337_fu_9126_p3 = esl_concat<1,52>(ap_const_lv1_1, tmp_745_reg_17087.read());
}

void mnist_fp16::thread_tmp_338_fu_9032_p2() {
    tmp_338_fu_9032_p2 = (!tmp_743_fu_9006_p1.read().is_01() || !ap_const_lv63_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_743_fu_9006_p1.read() == ap_const_lv63_0);
}

void mnist_fp16::thread_tmp_339_fu_9156_p2() {
    tmp_339_fu_9156_p2 = (!F2_7_fu_9150_p2.read().is_01() || !ap_const_lv12_E.is_01())? sc_lv<1>(): (sc_bigint<12>(F2_7_fu_9150_p2.read()) > sc_bigint<12>(ap_const_lv12_E));
}

void mnist_fp16::thread_tmp_33_fu_3808_p3() {
    tmp_33_fu_3808_p3 = esl_concat<1,52>(ap_const_lv1_1, tmp_146_reg_15642.read());
}

void mnist_fp16::thread_tmp_340_fu_9162_p2() {
    tmp_340_fu_9162_p2 = (!ap_const_lv12_FF2.is_01() || !F2_7_fu_9150_p2.read().is_01())? sc_lv<12>(): (sc_bigint<12>(ap_const_lv12_FF2) + sc_biguint<12>(F2_7_fu_9150_p2.read()));
}

void mnist_fp16::thread_tmp_341_fu_9168_p2() {
    tmp_341_fu_9168_p2 = (!ap_const_lv12_E.is_01() || !F2_7_fu_9150_p2.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_E) - sc_biguint<12>(F2_7_fu_9150_p2.read()));
}

void mnist_fp16::thread_tmp_342_fu_9182_p2() {
    tmp_342_fu_9182_p2 = (!F2_7_fu_9150_p2.read().is_01() || !ap_const_lv12_E.is_01())? sc_lv<1>(): sc_lv<1>(F2_7_fu_9150_p2.read() == ap_const_lv12_E);
}

void mnist_fp16::thread_tmp_343_fu_4660_p2() {
    tmp_343_fu_4660_p2 = (!ap_const_lv4_1.is_01() || !tmp_335_fu_4656_p1.read().is_01())? sc_lv<4>(): (sc_biguint<4>(ap_const_lv4_1) + sc_biguint<4>(tmp_335_fu_4656_p1.read()));
}

void mnist_fp16::thread_tmp_344_fu_9360_p2() {
    tmp_344_fu_9360_p2 = (!sh_amt_7_reg_17143.read().is_01() || !ap_const_lv12_36.is_01())? sc_lv<1>(): (sc_biguint<12>(sh_amt_7_reg_17143.read()) < sc_biguint<12>(ap_const_lv12_36));
}

void mnist_fp16::thread_tmp_345_fu_4666_p1() {
    tmp_345_fu_4666_p1 = esl_zext<16,4>(tmp_343_fu_4660_p2.read());
}

void mnist_fp16::thread_tmp_346_fu_9365_p1() {
    tmp_346_fu_9365_p1 = esl_zext<54,32>(sh_amt_7_cast_fu_9357_p1.read());
}

void mnist_fp16::thread_tmp_347_fu_9369_p2() {
    tmp_347_fu_9369_p2 = (!man_V_15_reg_17132.read().is_01() || !tmp_346_fu_9365_p1.read().is_01())? sc_lv<54>(): sc_bigint<54>(man_V_15_reg_17132.read()) >> (unsigned short)tmp_346_fu_9365_p1.read().to_uint();
}

void mnist_fp16::thread_tmp_348_fu_14984_p1() {
    tmp_348_fu_14984_p1 = esl_zext<64,4>(p_69_reg_2729.read());
}

void mnist_fp16::thread_tmp_349_fu_6412_p2() {
    tmp_349_fu_6412_p2 = (!lhs_V_5_cast_fu_6408_p1.read().is_01() || !tmp_610_cast_reg_16287.read().is_01())? sc_lv<9>(): (sc_biguint<9>(lhs_V_5_cast_fu_6408_p1.read()) + sc_bigint<9>(tmp_610_cast_reg_16287.read()));
}

void mnist_fp16::thread_tmp_34_cast_fu_3572_p1() {
    tmp_34_cast_fu_3572_p1 = esl_zext<11,5>(r_V_2_fu_3566_p2.read());
}

void mnist_fp16::thread_tmp_34_fu_3011_p2() {
    tmp_34_fu_3011_p2 = (!tmp_6_reg_15373.read().is_01() || !tmp_6_cast_fu_3007_p1.read().is_01())? sc_lv<11>(): (sc_biguint<11>(tmp_6_reg_15373.read()) + sc_biguint<11>(tmp_6_cast_fu_3007_p1.read()));
}

void mnist_fp16::thread_tmp_350_fu_9385_p1() {
    tmp_350_fu_9385_p1 = esl_sext<32,16>(tmp_746_reg_17155.read());
}

void mnist_fp16::thread_tmp_351_fu_9388_p2() {
    tmp_351_fu_9388_p2 = (!sh_amt_7_cast_fu_9357_p1.read().is_01())? sc_lv<32>(): tmp_350_fu_9385_p1.read() << (unsigned short)sh_amt_7_cast_fu_9357_p1.read().to_uint();
}

void mnist_fp16::thread_tmp_352_fu_4707_p1() {
    tmp_352_fu_4707_p1 = msb_idx_reg_15831.read().range(8-1, 0);
}

void mnist_fp16::thread_tmp_353_fu_6417_p1() {
    tmp_353_fu_6417_p1 = tmp_349_fu_6412_p2.read().range(7-1, 0);
}

void mnist_fp16::thread_tmp_354_fu_10445_p2() {
    tmp_354_fu_10445_p2 = (!ap_const_lv16_0.is_01() || !relu4_0_V_q0.read().is_01())? sc_lv<16>(): (sc_biguint<16>(ap_const_lv16_0) - sc_biguint<16>(relu4_0_V_q0.read()));
}

void mnist_fp16::thread_tmp_355_fu_6429_p3() {
    tmp_355_fu_6429_p3 = esl_concat<9,1>(tmp_349_fu_6412_p2.read(), ap_const_lv1_0);
}

void mnist_fp16::thread_tmp_356_fu_6441_p2() {
    tmp_356_fu_6441_p2 = (!p_shl46_cast_fu_6421_p3.read().is_01() || !p_shl47_cast_fu_6437_p1.read().is_01())? sc_lv<11>(): (sc_biguint<11>(p_shl46_cast_fu_6421_p3.read()) - sc_bigint<11>(p_shl47_cast_fu_6437_p1.read()));
}

void mnist_fp16::thread_tmp_357_fu_10534_p2() {
    tmp_357_fu_10534_p2 = (!ap_const_lv32_1F.is_01() || !msb_idx_10_cast_fu_10511_p1.read().is_01())? sc_lv<32>(): (sc_biguint<32>(ap_const_lv32_1F) - sc_biguint<32>(msb_idx_10_cast_fu_10511_p1.read()));
}

void mnist_fp16::thread_tmp_358_fu_12274_p2() {
    tmp_358_fu_12274_p2 = (!ap_const_lv16_0.is_01() || !relu5_0_V_q0.read().is_01())? sc_lv<16>(): (sc_biguint<16>(ap_const_lv16_0) - sc_biguint<16>(relu5_0_V_q0.read()));
}

void mnist_fp16::thread_tmp_359_fu_5989_p2() {
    tmp_359_fu_5989_p2 = (!tmp_77_cast_fu_5985_p1.read().is_01() || !tmp_255_reg_16173.read().is_01())? sc_lv<13>(): (sc_biguint<13>(tmp_77_cast_fu_5985_p1.read()) + sc_biguint<13>(tmp_255_reg_16173.read()));
}

void mnist_fp16::thread_tmp_35_cast_fu_3586_p1() {
    tmp_35_cast_fu_3586_p1 = esl_zext<7,2>(p_17_reg_1412.read());
}

void mnist_fp16::thread_tmp_35_fu_3129_p2() {
    tmp_35_fu_3129_p2 = (!p_3_reg_1326.read().is_01() || !ap_const_lv5_1F.is_01())? sc_lv<1>(): sc_lv<1>(p_3_reg_1326.read() == ap_const_lv5_1F);
}

void mnist_fp16::thread_tmp_360_fu_6012_p1() {
    tmp_360_fu_6012_p1 = conv2_0_load_to_int_fu_5999_p1.read().range(23-1, 0);
}

void mnist_fp16::thread_tmp_361_cast_fu_11340_p1() {
    tmp_361_cast_fu_11340_p1 = esl_zext<8,4>(r_V_24_fu_11335_p2.read());
}

void mnist_fp16::thread_tmp_361_fu_6049_p1() {
    tmp_361_fu_6049_p1 = ireg_V_3_fu_6041_p3.read().range(63-1, 0);
}

void mnist_fp16::thread_tmp_362_cast_fu_11540_p1() {
    tmp_362_cast_fu_11540_p1 = esl_zext<11,3>(p_79_reg_2406.read());
}

void mnist_fp16::thread_tmp_363_fu_6071_p1() {
    tmp_363_fu_6071_p1 = ireg_V_3_fu_6041_p3.read().range(52-1, 0);
}

void mnist_fp16::thread_tmp_364_fu_11367_p1() {
    tmp_364_fu_11367_p1 = esl_zext<64,2>(p_71_reg_2338.read());
}

void mnist_fp16::thread_tmp_366_fu_10591_p2() {
    tmp_366_fu_10591_p2 = (!p_Result_35_fu_10581_p4.read().is_01() || !ap_const_lv8_9E.is_01())? sc_lv<1>(): sc_lv<1>(p_Result_35_fu_10581_p4.read() != ap_const_lv8_9E);
}

void mnist_fp16::thread_tmp_367_fu_6146_p1() {
    tmp_367_fu_6146_p1 = man_V_6_fu_6101_p3.read().range(16-1, 0);
}

void mnist_fp16::thread_tmp_368_fu_15093_p1() {
    tmp_368_fu_15093_p1 = esl_zext<64,4>(p_72_reg_2740.read());
}

void mnist_fp16::thread_tmp_374_fu_13365_p2() {
    tmp_374_fu_13365_p2 = (!p_Val2_44_reg_2511.read().is_01() || !ap_const_lv16_0.is_01())? sc_lv<1>(): sc_lv<1>(p_Val2_44_reg_2511.read() == ap_const_lv16_0);
}

void mnist_fp16::thread_tmp_375_cast1_fu_12606_p1() {
    tmp_375_cast1_fu_12606_p1 = esl_zext<9,5>(p_74_reg_2523.read());
}

void mnist_fp16::thread_tmp_375_cast_fu_12610_p1() {
    tmp_375_cast_fu_12610_p1 = esl_zext<10,5>(p_74_reg_2523.read());
}

void mnist_fp16::thread_tmp_375_fu_6150_p4() {
    tmp_375_fu_6150_p4 = sh_amt_1_fu_6132_p3.read().range(11, 4);
}

void mnist_fp16::thread_tmp_376_fu_12366_p2() {
    tmp_376_fu_12366_p2 = (!ap_const_lv32_1F.is_01() || !msb_idx_12_cast_fu_12343_p1.read().is_01())? sc_lv<32>(): (sc_biguint<32>(ap_const_lv32_1F) - sc_biguint<32>(msb_idx_12_cast_fu_12343_p1.read()));
}

void mnist_fp16::thread_tmp_377_fu_6218_p1() {
    tmp_377_fu_6218_p1 = tmp_145_fu_6213_p2.read().range(16-1, 0);
}

void mnist_fp16::thread_tmp_378_fu_10613_p3() {
    tmp_378_fu_10613_p3 = esl_concat<1,8>(tmp_779_reg_17440.read(), p_Repl2_16_trunc_fu_10607_p2.read());
}

void mnist_fp16::thread_tmp_379_fu_10645_p4() {
    tmp_379_fu_10645_p4 = p_03_i3_to_int_fu_10642_p1.read().range(30, 23);
}

void mnist_fp16::thread_tmp_37_fu_4288_p3() {
    tmp_37_fu_4288_p3 = (!tmp_256_fu_4276_p2.read()[0].is_01())? sc_lv<5>(): ((tmp_256_fu_4276_p2.read()[0].to_bool())? r_V_4_fu_4270_p2.read(): tmp_258_fu_4282_p2.read());
}

void mnist_fp16::thread_tmp_380_fu_6238_p1() {
    tmp_380_fu_6238_p1 = tmp_153_fu_6232_p2.read().range(16-1, 0);
}

void mnist_fp16::thread_tmp_381_fu_10663_p4() {
    tmp_381_fu_10663_p4 = tmp_318_to_int_fu_10659_p1.read().range(30, 23);
}

void mnist_fp16::thread_tmp_382_fu_5801_p2() {
    tmp_382_fu_5801_p2 = (!tmp_191_reg_15892.read().is_01() || !tmp_127_cast_fu_5797_p1.read().is_01())? sc_lv<13>(): (sc_biguint<13>(tmp_191_reg_15892.read()) + sc_biguint<13>(tmp_127_cast_fu_5797_p1.read()));
}

void mnist_fp16::thread_tmp_383_fu_10689_p2() {
    tmp_383_fu_10689_p2 = (notrhs6_fu_10683_p2.read() | notlhs6_fu_10677_p2.read());
}

void mnist_fp16::thread_tmp_384_fu_10707_p2() {
    tmp_384_fu_10707_p2 = (notrhs7_fu_10701_p2.read() | notlhs7_fu_10695_p2.read());
}

void mnist_fp16::thread_tmp_385_cast_fu_13584_p1() {
    tmp_385_cast_fu_13584_p1 = esl_zext<9,5>(p_82_reg_2580.read());
}

void mnist_fp16::thread_tmp_385_fu_5713_p1() {
    tmp_385_fu_5713_p1 = msb_idx_2_fu_5707_p2.read().range(31-1, 0);
}

void mnist_fp16::thread_tmp_386_fu_10713_p2() {
    tmp_386_fu_10713_p2 = (tmp_383_fu_10689_p2.read() & tmp_384_fu_10707_p2.read());
}

void mnist_fp16::thread_tmp_388_fu_11636_p1() {
    tmp_388_fu_11636_p1 = esl_zext<12,11>(p_Result_37_reg_17815.read());
}

void mnist_fp16::thread_tmp_389_fu_10719_p2() {
    tmp_389_fu_10719_p2 = (tmp_386_fu_10713_p2.read() & grp_fu_2839_p2.read());
}

void mnist_fp16::thread_tmp_38_cast_fu_4508_p1() {
    tmp_38_cast_fu_4508_p1 = esl_zext<13,5>(tmp_37_reg_15742.read());
}

void mnist_fp16::thread_tmp_38_fu_3135_p2() {
    tmp_38_fu_3135_p2 = (!p_3_reg_1326.read().is_01() || !ap_const_lv5_1E.is_01())? sc_lv<1>(): sc_lv<1>(p_3_reg_1326.read() == ap_const_lv5_1E);
}

void mnist_fp16::thread_tmp_390_fu_11630_p2() {
    tmp_390_fu_11630_p2 = (!tmp_797_fu_11604_p1.read().is_01() || !ap_const_lv63_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_797_fu_11604_p1.read() == ap_const_lv63_0);
}

void mnist_fp16::thread_tmp_391_cast_fu_11419_p1() {
    tmp_391_cast_fu_11419_p1 = esl_zext<11,4>(r_V_27_fu_11414_p2.read());
}

void mnist_fp16::thread_tmp_392_cast_fu_11433_p1() {
    tmp_392_cast_fu_11433_p1 = esl_zext<12,2>(p_76_reg_2361.read());
}

void mnist_fp16::thread_tmp_392_fu_5735_p4() {
    tmp_392_fu_5735_p4 = msb_idx_3_fu_5725_p3.read().range(30, 5);
}

void mnist_fp16::thread_tmp_394_fu_11557_p4() {
    tmp_394_fu_11557_p4 = conv5_0_load_to_int_fu_11554_p1.read().range(30, 23);
}

void mnist_fp16::thread_tmp_395_fu_5766_p1() {
    tmp_395_fu_5766_p1 = msb_idx_3_fu_5725_p3.read().range(4-1, 0);
}

void mnist_fp16::thread_tmp_396_fu_11583_p2() {
    tmp_396_fu_11583_p2 = (notrhs8_reg_17799.read() | notlhs8_reg_17794.read());
}

void mnist_fp16::thread_tmp_398_fu_11587_p2() {
    tmp_398_fu_11587_p2 = (tmp_396_fu_11583_p2.read() & tmp_397_reg_17804.read());
}

void mnist_fp16::thread_tmp_39_fu_3799_p2() {
    tmp_39_fu_3799_p2 = (!tmp_139_fu_3773_p1.read().is_01() || !ap_const_lv63_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_139_fu_3773_p1.read() == ap_const_lv63_0);
}

void mnist_fp16::thread_tmp_400_fu_11592_p1() {
    tmp_400_fu_11592_p1 = grp_fu_2830_p1.read();
}

void mnist_fp16::thread_tmp_401_fu_11669_p2() {
    tmp_401_fu_11669_p2 = (!F2_8_fu_11663_p2.read().is_01() || !ap_const_lv12_E.is_01())? sc_lv<1>(): (sc_bigint<12>(F2_8_fu_11663_p2.read()) > sc_bigint<12>(ap_const_lv12_E));
}

void mnist_fp16::thread_tmp_402_fu_13764_p3() {
    tmp_402_fu_13764_p3 = esl_concat<1,52>(ap_const_lv1_1, tmp_875_reg_18419.read());
}

void mnist_fp16::thread_tmp_403_fu_11675_p2() {
    tmp_403_fu_11675_p2 = (!ap_const_lv12_FF2.is_01() || !F2_8_fu_11663_p2.read().is_01())? sc_lv<12>(): (sc_bigint<12>(ap_const_lv12_FF2) + sc_biguint<12>(F2_8_fu_11663_p2.read()));
}

void mnist_fp16::thread_tmp_404_fu_12659_p2() {
    tmp_404_fu_12659_p2 = (!ap_const_lv16_0.is_01() || !p_Val2_44_reg_2511.read().is_01())? sc_lv<16>(): (sc_biguint<16>(ap_const_lv16_0) - sc_biguint<16>(p_Val2_44_reg_2511.read()));
}

void mnist_fp16::thread_tmp_405_fu_5770_p2() {
    tmp_405_fu_5770_p2 = (!ap_const_lv4_1.is_01() || !tmp_395_fu_5766_p1.read().is_01())? sc_lv<4>(): (sc_biguint<4>(ap_const_lv4_1) + sc_biguint<4>(tmp_395_fu_5766_p1.read()));
}

void mnist_fp16::thread_tmp_406_fu_11681_p2() {
    tmp_406_fu_11681_p2 = (!ap_const_lv12_E.is_01() || !F2_8_fu_11663_p2.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_E) - sc_biguint<12>(F2_8_fu_11663_p2.read()));
}

void mnist_fp16::thread_tmp_407_fu_5776_p1() {
    tmp_407_fu_5776_p1 = esl_zext<16,4>(tmp_405_fu_5770_p2.read());
}

void mnist_fp16::thread_tmp_408_fu_12423_p2() {
    tmp_408_fu_12423_p2 = (!p_Result_42_fu_12413_p4.read().is_01() || !ap_const_lv8_9E.is_01())? sc_lv<1>(): sc_lv<1>(p_Result_42_fu_12413_p4.read() != ap_const_lv8_9E);
}

void mnist_fp16::thread_tmp_409_fu_11695_p2() {
    tmp_409_fu_11695_p2 = (!F2_8_fu_11663_p2.read().is_01() || !ap_const_lv12_E.is_01())? sc_lv<1>(): sc_lv<1>(F2_8_fu_11663_p2.read() == ap_const_lv12_E);
}

void mnist_fp16::thread_tmp_40_fu_3141_p2() {
    tmp_40_fu_3141_p2 = (tmp_38_fu_3135_p2.read() | tmp_35_fu_3129_p2.read());
}

void mnist_fp16::thread_tmp_410_fu_4888_p2() {
    tmp_410_fu_4888_p2 = (!tmp_580_cast1_reg_15874.read().is_01() || !tmp_64_cast_fu_4884_p1.read().is_01())? sc_lv<6>(): (sc_biguint<6>(tmp_580_cast1_reg_15874.read()) + sc_biguint<6>(tmp_64_cast_fu_4884_p1.read()));
}

void mnist_fp16::thread_tmp_411_fu_11759_p2() {
    tmp_411_fu_11759_p2 = (!sh_amt_8_reg_17836.read().is_01() || !ap_const_lv12_36.is_01())? sc_lv<1>(): (sc_biguint<12>(sh_amt_8_reg_17836.read()) < sc_biguint<12>(ap_const_lv12_36));
}

void mnist_fp16::thread_tmp_412_fu_5826_p1() {
    tmp_412_fu_5826_p1 = msb_idx_2_reg_16117.read().range(8-1, 0);
}

void mnist_fp16::thread_tmp_413_fu_11764_p1() {
    tmp_413_fu_11764_p1 = esl_zext<54,32>(sh_amt_8_cast_fu_11756_p1.read());
}

void mnist_fp16::thread_tmp_414_fu_11768_p2() {
    tmp_414_fu_11768_p2 = (!man_V_21_reg_17831.read().is_01() || !tmp_413_fu_11764_p1.read().is_01())? sc_lv<54>(): sc_bigint<54>(man_V_21_reg_17831.read()) >> (unsigned short)tmp_413_fu_11764_p1.read().to_uint();
}

void mnist_fp16::thread_tmp_415_fu_4897_p3() {
    tmp_415_fu_4897_p3 = esl_concat<6,2>(tmp_410_fu_4888_p2.read(), ap_const_lv2_0);
}

void mnist_fp16::thread_tmp_416_fu_4909_p2() {
    tmp_416_fu_4909_p2 = (!p_shl_fu_4905_p1.read().is_01() || !tmp_642_cast_fu_4893_p1.read().is_01())? sc_lv<64>(): (sc_biguint<64>(p_shl_fu_4905_p1.read()) - sc_biguint<64>(tmp_642_cast_fu_4893_p1.read()));
}

void mnist_fp16::thread_tmp_417_cast_fu_14012_p1() {
    tmp_417_cast_fu_14012_p1 = esl_zext<9,5>(p_83_reg_2613.read());
}

void mnist_fp16::thread_tmp_417_fu_14008_p1() {
    tmp_417_fu_14008_p1 = esl_zext<64,5>(p_83_reg_2613.read());
}

void mnist_fp16::thread_tmp_418_cast_fu_13622_p1() {
    tmp_418_cast_fu_13622_p1 = esl_zext<10,3>(p_84_reg_2591.read());
}

void mnist_fp16::thread_tmp_418_fu_4915_p3() {
    tmp_418_fu_4915_p3 = esl_concat<3,5>(p_24_reg_1579.read(), ap_const_lv5_0);
}

void mnist_fp16::thread_tmp_419_fu_11784_p1() {
    tmp_419_fu_11784_p1 = esl_sext<32,16>(tmp_800_reg_17847.read());
}

void mnist_fp16::thread_tmp_41_fu_3147_p2() {
    tmp_41_fu_3147_p2 = (!p_3_reg_1326.read().is_01() || !ap_const_lv5_1D.is_01())? sc_lv<1>(): sc_lv<1>(p_3_reg_1326.read() == ap_const_lv5_1D);
}

void mnist_fp16::thread_tmp_420_fu_12445_p3() {
    tmp_420_fu_12445_p3 = esl_concat<1,8>(is_neg_4_reg_18000.read(), p_Repl2_19_trunc_fu_12439_p2.read());
}

void mnist_fp16::thread_tmp_421_fu_11787_p2() {
    tmp_421_fu_11787_p2 = (!sh_amt_8_cast_fu_11756_p1.read().is_01())? sc_lv<32>(): tmp_419_fu_11784_p1.read() << (unsigned short)sh_amt_8_cast_fu_11756_p1.read().to_uint();
}

void mnist_fp16::thread_tmp_422_fu_15115_p2() {
    tmp_422_fu_15115_p2 = (!index_V_reg_2787.read().is_01() || !ap_const_lv4_A.is_01())? sc_lv<1>(): sc_lv<1>(index_V_reg_2787.read() == ap_const_lv4_A);
}

void mnist_fp16::thread_tmp_423_fu_4927_p3() {
    tmp_423_fu_4927_p3 = esl_concat<3,1>(p_24_reg_1579.read(), ap_const_lv1_0);
}

void mnist_fp16::thread_tmp_424_fu_11980_p2() {
    tmp_424_fu_11980_p2 = (!p_57_reg_2439.read().is_01() || !ap_const_lv4_0.is_01())? sc_lv<1>(): (sc_bigint<4>(p_57_reg_2439.read()) > sc_bigint<4>(ap_const_lv4_0));
}

void mnist_fp16::thread_tmp_426_fu_15110_p1() {
    tmp_426_fu_15110_p1 = esl_zext<64,4>(p_77_reg_2763.read());
}

void mnist_fp16::thread_tmp_429_fu_12003_p2() {
    tmp_429_fu_12003_p2 = (!p_63_reg_2450.read().is_01() || !ap_const_lv4_0.is_01())? sc_lv<1>(): (sc_bigint<4>(p_63_reg_2450.read()) > sc_bigint<4>(ap_const_lv4_0));
}

void mnist_fp16::thread_tmp_42_fu_3153_p2() {
    tmp_42_fu_3153_p2 = (tmp_41_fu_3147_p2.read() | tmp_40_fu_3141_p2.read());
}

void mnist_fp16::thread_tmp_431_cast_fu_12467_p1() {
    tmp_431_cast_fu_12467_p1 = esl_zext<12,4>(p_63_reg_2450.read());
}

void mnist_fp16::thread_tmp_431_fu_4939_p2() {
    tmp_431_fu_4939_p2 = (!p_shl49_cast_fu_4923_p1.read().is_01() || !p_shl50_cast_fu_4935_p1.read().is_01())? sc_lv<9>(): (sc_biguint<9>(p_shl49_cast_fu_4923_p1.read()) - sc_biguint<9>(p_shl50_cast_fu_4935_p1.read()));
}

void mnist_fp16::thread_tmp_433_fu_14414_p1() {
    tmp_433_fu_14414_p1 = esl_zext<64,5>(p_85_reg_2671.read());
}

void mnist_fp16::thread_tmp_435_fu_14449_p1() {
    tmp_435_fu_14449_p1 = esl_zext<12,11>(p_Result_44_reg_18607.read());
}

void mnist_fp16::thread_tmp_436_fu_14476_p2() {
    tmp_436_fu_14476_p2 = (!tmp_859_reg_18596.read().is_01() || !ap_const_lv63_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_859_reg_18596.read() == ap_const_lv63_0);
}

void mnist_fp16::thread_tmp_437_fu_14487_p2() {
    tmp_437_fu_14487_p2 = (!F2_9_fu_14481_p2.read().is_01() || !ap_const_lv12_E.is_01())? sc_lv<1>(): (sc_bigint<12>(F2_9_fu_14481_p2.read()) > sc_bigint<12>(ap_const_lv12_E));
}

void mnist_fp16::thread_tmp_438_cast_fu_14050_p1() {
    tmp_438_cast_fu_14050_p1 = esl_zext<10,3>(p_86_reg_2637.read());
}

void mnist_fp16::thread_tmp_438_fu_4749_p2() {
    tmp_438_fu_4749_p2 = (!tmp_134_reg_15718.read().is_01() || !tmp_93_cast_fu_4745_p1.read().is_01())? sc_lv<13>(): (sc_biguint<13>(tmp_134_reg_15718.read()) + sc_biguint<13>(tmp_93_cast_fu_4745_p1.read()));
}

void mnist_fp16::thread_tmp_439_cast_fu_13665_p1() {
    tmp_439_cast_fu_13665_p1 = esl_zext<11,3>(p_87_reg_2602.read());
}

void mnist_fp16::thread_tmp_439_fu_7338_p3() {
    tmp_439_fu_7338_p3 = esl_concat<4,2>(p_15_reg_1806.read(), ap_const_lv2_0);
}

void mnist_fp16::thread_tmp_43_fu_3838_p2() {
    tmp_43_fu_3838_p2 = (!F2_fu_3832_p2.read().is_01() || !ap_const_lv12_E.is_01())? sc_lv<1>(): (sc_bigint<12>(F2_fu_3832_p2.read()) > sc_bigint<12>(ap_const_lv12_E));
}

void mnist_fp16::thread_tmp_440_fu_14493_p2() {
    tmp_440_fu_14493_p2 = (!ap_const_lv12_FF2.is_01() || !F2_9_fu_14481_p2.read().is_01())? sc_lv<12>(): (sc_bigint<12>(ap_const_lv12_FF2) + sc_biguint<12>(F2_9_fu_14481_p2.read()));
}

void mnist_fp16::thread_tmp_442_cast_fu_12695_p1() {
    tmp_442_cast_fu_12695_p1 = esl_zext<9,4>(r_V_28_fu_12690_p2.read());
}

void mnist_fp16::thread_tmp_442_fu_7350_p3() {
    tmp_442_fu_7350_p3 = esl_concat<4,4>(p_15_reg_1806.read(), ap_const_lv4_0);
}

void mnist_fp16::thread_tmp_443_fu_12722_p1() {
    tmp_443_fu_12722_p1 = esl_zext<64,2>(p_78_reg_2546.read());
}

void mnist_fp16::thread_tmp_444_fu_13459_p2() {
    tmp_444_fu_13459_p2 = (!ap_const_lv32_1F.is_01() || !msb_idx_14_cast_fu_13436_p1.read().is_01())? sc_lv<32>(): (sc_biguint<32>(ap_const_lv32_1F) - sc_biguint<32>(msb_idx_14_cast_fu_13436_p1.read()));
}

void mnist_fp16::thread_tmp_445_fu_7362_p3() {
    tmp_445_fu_7362_p3 = esl_concat<4,1>(p_15_reg_1806.read(), ap_const_lv1_0);
}

void mnist_fp16::thread_tmp_446_fu_14499_p2() {
    tmp_446_fu_14499_p2 = (!ap_const_lv12_E.is_01() || !F2_9_fu_14481_p2.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_E) - sc_biguint<12>(F2_9_fu_14481_p2.read()));
}

void mnist_fp16::thread_tmp_447_fu_14513_p2() {
    tmp_447_fu_14513_p2 = (!F2_9_fu_14481_p2.read().is_01() || !ap_const_lv12_E.is_01())? sc_lv<1>(): sc_lv<1>(F2_9_fu_14481_p2.read() == ap_const_lv12_E);
}

void mnist_fp16::thread_tmp_448_fu_7374_p2() {
    tmp_448_fu_7374_p2 = (!p_shl51_cast_fu_7358_p1.read().is_01() || !p_shl52_cast_fu_7370_p1.read().is_01())? sc_lv<9>(): (sc_biguint<9>(p_shl51_cast_fu_7358_p1.read()) - sc_biguint<9>(p_shl52_cast_fu_7370_p1.read()));
}

void mnist_fp16::thread_tmp_449_fu_14578_p2() {
    tmp_449_fu_14578_p2 = (!sh_amt_9_reg_18627.read().is_01() || !ap_const_lv12_36.is_01())? sc_lv<1>(): (sc_biguint<12>(sh_amt_9_reg_18627.read()) < sc_biguint<12>(ap_const_lv12_36));
}

void mnist_fp16::thread_tmp_44_fu_3159_p2() {
    tmp_44_fu_3159_p2 = (!p_3_reg_1326.read().is_01() || !ap_const_lv5_0.is_01())? sc_lv<1>(): sc_lv<1>(p_3_reg_1326.read() == ap_const_lv5_0);
}

void mnist_fp16::thread_tmp_450_fu_6941_p2() {
    tmp_450_fu_6941_p2 = (!lhs_V_1_cast_fu_6937_p1.read().is_01() || !tmp_635_cast_reg_16442.read().is_01())? sc_lv<8>(): (sc_biguint<8>(lhs_V_1_cast_fu_6937_p1.read()) + sc_biguint<8>(tmp_635_cast_reg_16442.read()));
}

void mnist_fp16::thread_tmp_451_fu_14583_p1() {
    tmp_451_fu_14583_p1 = esl_zext<54,32>(sh_amt_9_cast_fu_14575_p1.read());
}

void mnist_fp16::thread_tmp_452_fu_14587_p2() {
    tmp_452_fu_14587_p2 = (!man_V_26_reg_18617.read().is_01() || !tmp_451_fu_14583_p1.read().is_01())? sc_lv<54>(): sc_bigint<54>(man_V_26_reg_18617.read()) >> (unsigned short)tmp_451_fu_14583_p1.read().to_uint();
}

void mnist_fp16::thread_tmp_453_fu_6954_p1() {
    tmp_453_fu_6954_p1 = p_20_reg_1771.read().range(4-1, 0);
}

void mnist_fp16::thread_tmp_454_fu_14603_p1() {
    tmp_454_fu_14603_p1 = esl_sext<32,16>(tmp_862_reg_18638.read());
}

void mnist_fp16::thread_tmp_455_fu_14606_p2() {
    tmp_455_fu_14606_p2 = (!sh_amt_9_cast_fu_14575_p1.read().is_01())? sc_lv<32>(): tmp_454_fu_14603_p1.read() << (unsigned short)sh_amt_9_cast_fu_14575_p1.read().to_uint();
}

void mnist_fp16::thread_tmp_456_fu_6970_p2() {
    tmp_456_fu_6970_p2 = (!ap_const_lv5_1.is_01())? sc_lv<5>(): p_20_reg_1771.read() << (unsigned short)ap_const_lv5_1.to_uint();
}

void mnist_fp16::thread_tmp_457_to_int_fu_14329_p1() {
    tmp_457_to_int_fu_14329_p1 = tmp_457_reg_2648.read();
}

void mnist_fp16::thread_tmp_458_fu_15127_p1() {
    tmp_458_fu_15127_p1 = esl_zext<64,4>(index_V_reg_2787.read());
}

void mnist_fp16::thread_tmp_459_fu_13682_p4() {
    tmp_459_fu_13682_p4 = conv6_0_load_to_int_fu_13679_p1.read().range(30, 23);
}

void mnist_fp16::thread_tmp_45_cast_fu_4296_p1() {
    tmp_45_cast_fu_4296_p1 = esl_zext<6,5>(p_14_reg_1502.read());
}

void mnist_fp16::thread_tmp_45_fu_3844_p2() {
    tmp_45_fu_3844_p2 = (!ap_const_lv12_FF2.is_01() || !F2_fu_3832_p2.read().is_01())? sc_lv<12>(): (sc_bigint<12>(ap_const_lv12_FF2) + sc_biguint<12>(F2_fu_3832_p2.read()));
}

void mnist_fp16::thread_tmp_460_fu_6471_p2() {
    tmp_460_fu_6471_p2 = (!lhs_V_7_cast_fu_6467_p1.read().is_01() || !tmp_356_reg_16300.read().is_01())? sc_lv<11>(): (sc_biguint<11>(lhs_V_7_cast_fu_6467_p1.read()) + sc_biguint<11>(tmp_356_reg_16300.read()));
}

void mnist_fp16::thread_tmp_461_fu_4981_p2() {
    tmp_461_fu_4981_p2 = (!tmp_647_cast_reg_15918.read().is_01() || !tmp_80_cast_fu_4977_p1.read().is_01())? sc_lv<10>(): (sc_bigint<10>(tmp_647_cast_reg_15918.read()) + sc_biguint<10>(tmp_80_cast_fu_4977_p1.read()));
}

void mnist_fp16::thread_tmp_462_fu_13708_p2() {
    tmp_462_fu_13708_p2 = (notrhs9_reg_18398.read() | notlhs9_reg_18393.read());
}

void mnist_fp16::thread_tmp_463_fu_14942_p3() {
    tmp_463_fu_14942_p3 = esl_concat<1,8>(tmp_919_reg_18698.read(), p_Repl2_28_trunc_fu_14936_p2.read());
}

void mnist_fp16::thread_tmp_464_cast_fu_14706_p1() {
    tmp_464_cast_fu_14706_p1 = esl_zext<9,4>(p_88_reg_2682.read());
}

void mnist_fp16::thread_tmp_464_fu_14702_p1() {
    tmp_464_fu_14702_p1 = esl_zext<64,4>(p_88_reg_2682.read());
}

void mnist_fp16::thread_tmp_466_cast_fu_14093_p1() {
    tmp_466_cast_fu_14093_p1 = esl_zext<11,3>(p_89_reg_2660.read());
}

void mnist_fp16::thread_tmp_466_fu_4986_p1() {
    tmp_466_fu_4986_p1 = tmp_461_fu_4981_p2.read().range(8-1, 0);
}

void mnist_fp16::thread_tmp_467_fu_14121_p2() {
    tmp_467_fu_14121_p2 = (!relu6_0_V_load_reg_18508.read().is_01() || !ap_const_lv16_0.is_01())? sc_lv<1>(): sc_lv<1>(relu6_0_V_load_reg_18508.read() == ap_const_lv16_0);
}

void mnist_fp16::thread_tmp_468_fu_4998_p3() {
    tmp_468_fu_4998_p3 = esl_concat<10,1>(tmp_461_fu_4981_p2.read(), ap_const_lv1_0);
}

void mnist_fp16::thread_tmp_469_fu_13761_p1() {
    tmp_469_fu_13761_p1 = esl_zext<12,11>(p_Result_46_reg_18414.read());
}

void mnist_fp16::thread_tmp_46_cast_fu_4300_p1() {
    tmp_46_cast_fu_4300_p1 = esl_zext<6,5>(tmp_37_fu_4288_p3.read());
}

void mnist_fp16::thread_tmp_46_fu_3850_p2() {
    tmp_46_fu_3850_p2 = (!ap_const_lv12_E.is_01() || !F2_fu_3832_p2.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_E) - sc_biguint<12>(F2_fu_3832_p2.read()));
}

void mnist_fp16::thread_tmp_470_fu_13712_p2() {
    tmp_470_fu_13712_p2 = (tmp_462_fu_13708_p2.read() & tmp_465_reg_18403.read());
}

void mnist_fp16::thread_tmp_471_fu_13755_p2() {
    tmp_471_fu_13755_p2 = (!tmp_871_fu_13729_p1.read().is_01() || !ap_const_lv63_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_871_fu_13729_p1.read() == ap_const_lv63_0);
}

void mnist_fp16::thread_tmp_473_fu_13717_p1() {
    tmp_473_fu_13717_p1 = grp_fu_2830_p1.read();
}

void mnist_fp16::thread_tmp_474_cast_fu_12774_p1() {
    tmp_474_cast_fu_12774_p1 = esl_zext<12,4>(r_V_29_fu_12769_p2.read());
}

void mnist_fp16::thread_tmp_474_fu_5010_p2() {
    tmp_474_fu_5010_p2 = (!p_shl53_cast_fu_4990_p3.read().is_01() || !p_shl55_cast_fu_5006_p1.read().is_01())? sc_lv<13>(): (sc_biguint<13>(p_shl53_cast_fu_4990_p3.read()) - sc_bigint<13>(p_shl55_cast_fu_5006_p1.read()));
}

void mnist_fp16::thread_tmp_475_fu_14283_p3() {
    tmp_475_fu_14283_p3 = esl_concat<1,8>(tmp_904_reg_18514.read(), p_Repl2_26_trunc_fu_14277_p2.read());
}

void mnist_fp16::thread_tmp_476_fu_12874_p1() {
    tmp_476_fu_12874_p1 = esl_zext<12,11>(exp_tmp_V_4_reg_18175.read());
}

void mnist_fp16::thread_tmp_477_fu_13794_p2() {
    tmp_477_fu_13794_p2 = (!F2_s_fu_13788_p2.read().is_01() || !ap_const_lv12_E.is_01())? sc_lv<1>(): (sc_bigint<12>(F2_s_fu_13788_p2.read()) > sc_bigint<12>(ap_const_lv12_E));
}

void mnist_fp16::thread_tmp_478_fu_12832_p2() {
    tmp_478_fu_12832_p2 = (!tmp_928_fu_12806_p1.read().is_01() || !ap_const_lv63_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_928_fu_12806_p1.read() == ap_const_lv63_0);
}

void mnist_fp16::thread_tmp_479_fu_13800_p2() {
    tmp_479_fu_13800_p2 = (!ap_const_lv12_FF2.is_01() || !F2_s_fu_13788_p2.read().is_01())? sc_lv<12>(): (sc_bigint<12>(ap_const_lv12_FF2) + sc_biguint<12>(F2_s_fu_13788_p2.read()));
}

void mnist_fp16::thread_tmp_47_fu_4546_p2() {
    tmp_47_fu_4546_p2 = (!relu1_0_V_q0.read().is_01() || !ap_const_lv16_0.is_01())? sc_lv<1>(): sc_lv<1>(relu1_0_V_q0.read() == ap_const_lv16_0);
}

void mnist_fp16::thread_tmp_480_fu_13806_p2() {
    tmp_480_fu_13806_p2 = (!ap_const_lv12_E.is_01() || !F2_s_fu_13788_p2.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_E) - sc_biguint<12>(F2_s_fu_13788_p2.read()));
}

void mnist_fp16::thread_tmp_481_fu_13820_p2() {
    tmp_481_fu_13820_p2 = (!F2_s_fu_13788_p2.read().is_01() || !ap_const_lv12_E.is_01())? sc_lv<1>(): sc_lv<1>(F2_s_fu_13788_p2.read() == ap_const_lv12_E);
}

void mnist_fp16::thread_tmp_482_fu_13516_p2() {
    tmp_482_fu_13516_p2 = (!p_Result_51_fu_13506_p4.read().is_01() || !ap_const_lv8_9E.is_01())? sc_lv<1>(): sc_lv<1>(p_Result_51_fu_13506_p4.read() != ap_const_lv8_9E);
}

void mnist_fp16::thread_tmp_483_fu_5020_p2() {
    tmp_483_fu_5020_p2 = (!tmp_416_reg_15913.read().is_01() || !tmp_81_fu_5016_p1.read().is_01())? sc_lv<64>(): (sc_biguint<64>(tmp_416_reg_15913.read()) + sc_biguint<64>(tmp_81_fu_5016_p1.read()));
}

void mnist_fp16::thread_tmp_484_fu_13884_p2() {
    tmp_484_fu_13884_p2 = (!sh_amt_s_reg_18435.read().is_01() || !ap_const_lv12_36.is_01())? sc_lv<1>(): (sc_biguint<12>(sh_amt_s_reg_18435.read()) < sc_biguint<12>(ap_const_lv12_36));
}

void mnist_fp16::thread_tmp_485_fu_5025_p1() {
    tmp_485_fu_5025_p1 = tmp_483_fu_5020_p2.read().range(9-1, 0);
}

void mnist_fp16::thread_tmp_486_fu_13889_p1() {
    tmp_486_fu_13889_p1 = esl_zext<54,32>(sh_amt_cast_47_fu_13881_p1.read());
}

void mnist_fp16::thread_tmp_487_fu_13893_p2() {
    tmp_487_fu_13893_p2 = (!man_V_28_reg_18430.read().is_01() || !tmp_486_fu_13889_p1.read().is_01())? sc_lv<54>(): sc_bigint<54>(man_V_28_reg_18430.read()) >> (unsigned short)tmp_486_fu_13889_p1.read().to_uint();
}

void mnist_fp16::thread_tmp_488_fu_5029_p1() {
    tmp_488_fu_5029_p1 = tmp_483_fu_5020_p2.read().range(7-1, 0);
}

void mnist_fp16::thread_tmp_489_fu_5041_p2() {
    tmp_489_fu_5041_p2 = (!p_shl54_cast_fu_5033_p3.read().is_01() || !tmp_485_fu_5025_p1.read().is_01())? sc_lv<9>(): (sc_biguint<9>(p_shl54_cast_fu_5033_p3.read()) - sc_biguint<9>(tmp_485_fu_5025_p1.read()));
}

void mnist_fp16::thread_tmp_490_fu_13909_p1() {
    tmp_490_fu_13909_p1 = esl_sext<32,16>(tmp_881_reg_18446.read());
}

void mnist_fp16::thread_tmp_491_fu_12907_p2() {
    tmp_491_fu_12907_p2 = (!F2_10_fu_12901_p2.read().is_01() || !ap_const_lv12_E.is_01())? sc_lv<1>(): (sc_bigint<12>(F2_10_fu_12901_p2.read()) > sc_bigint<12>(ap_const_lv12_E));
}

void mnist_fp16::thread_tmp_492_fu_12913_p2() {
    tmp_492_fu_12913_p2 = (!ap_const_lv12_FF2.is_01() || !F2_10_fu_12901_p2.read().is_01())? sc_lv<12>(): (sc_bigint<12>(ap_const_lv12_FF2) + sc_biguint<12>(F2_10_fu_12901_p2.read()));
}

void mnist_fp16::thread_tmp_493_fu_12919_p2() {
    tmp_493_fu_12919_p2 = (!ap_const_lv12_E.is_01() || !F2_10_fu_12901_p2.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_E) - sc_biguint<12>(F2_10_fu_12901_p2.read()));
}

void mnist_fp16::thread_tmp_494_fu_12933_p2() {
    tmp_494_fu_12933_p2 = (!F2_10_fu_12901_p2.read().is_01() || !ap_const_lv12_E.is_01())? sc_lv<1>(): sc_lv<1>(F2_10_fu_12901_p2.read() == ap_const_lv12_E);
}

void mnist_fp16::thread_tmp_495_fu_13912_p2() {
    tmp_495_fu_13912_p2 = (!sh_amt_cast_47_fu_13881_p1.read().is_01())? sc_lv<32>(): tmp_490_fu_13909_p1.read() << (unsigned short)sh_amt_cast_47_fu_13881_p1.read().to_uint();
}

void mnist_fp16::thread_tmp_496_fu_13538_p3() {
    tmp_496_fu_13538_p3 = esl_concat<1,8>(is_neg_5_reg_18301.read(), p_Repl2_22_trunc_fu_13532_p2.read());
}

void mnist_fp16::thread_tmp_497_cast_fu_12665_p1() {
    tmp_497_cast_fu_12665_p1 = esl_zext<11,3>(p_67_reg_2499.read());
}

void mnist_fp16::thread_tmp_497_fu_7619_p3() {
    tmp_497_fu_7619_p3 = esl_concat<4,4>(p_36_reg_1912.read(), ap_const_lv4_0);
}

void mnist_fp16::thread_tmp_498_fu_12877_p3() {
    tmp_498_fu_12877_p3 = esl_concat<1,52>(ap_const_lv1_1, tmp_930_reg_18180.read());
}

void mnist_fp16::thread_tmp_499_fu_7631_p3() {
    tmp_499_fu_7631_p3 = esl_concat<4,1>(p_36_reg_1912.read(), ap_const_lv1_0);
}

void mnist_fp16::thread_tmp_49_cast_fu_3374_p1() {
    tmp_49_cast_fu_3374_p1 = esl_sext<7,6>(tmp_16_fu_3368_p2.read());
}

void mnist_fp16::thread_tmp_49_fu_3165_p2() {
    tmp_49_fu_3165_p2 = (tmp_44_fu_3159_p2.read() | tmp_42_fu_3153_p2.read());
}

void mnist_fp16::thread_tmp_500_fu_14115_p2() {
    tmp_500_fu_14115_p2 = (!ap_const_lv16_0.is_01() || !relu6_0_V_q0.read().is_01())? sc_lv<16>(): (sc_biguint<16>(ap_const_lv16_0) - sc_biguint<16>(relu6_0_V_q0.read()));
}

void mnist_fp16::thread_tmp_501_fu_7643_p2() {
    tmp_501_fu_7643_p2 = (!p_shl56_cast_fu_7627_p1.read().is_01() || !p_shl57_cast_fu_7639_p1.read().is_01())? sc_lv<9>(): (sc_biguint<9>(p_shl56_cast_fu_7627_p1.read()) - sc_biguint<9>(p_shl57_cast_fu_7639_p1.read()));
}

void mnist_fp16::thread_tmp_502_fu_14722_p1() {
    tmp_502_fu_14722_p1 = esl_zext<64,5>(p_90_reg_2693.read());
}

void mnist_fp16::thread_tmp_503_fu_14776_p2() {
    tmp_503_fu_14776_p2 = (!pool3_0_V_load_reg_18692.read().is_01() || !ap_const_lv16_0.is_01())? sc_lv<1>(): sc_lv<1>(pool3_0_V_load_reg_18692.read() == ap_const_lv16_0);
}

void mnist_fp16::thread_tmp_504_fu_7400_p2() {
    tmp_504_fu_7400_p2 = (!tmp_653_cast_reg_16563.read().is_01() || !tmp_66_cast_fu_7396_p1.read().is_01())? sc_lv<10>(): (sc_bigint<10>(tmp_653_cast_reg_16563.read()) + sc_biguint<10>(tmp_66_cast_fu_7396_p1.read()));
}

void mnist_fp16::thread_tmp_505_fu_7405_p1() {
    tmp_505_fu_7405_p1 = tmp_504_fu_7400_p2.read().range(8-1, 0);
}

void mnist_fp16::thread_tmp_506_fu_14204_p2() {
    tmp_506_fu_14204_p2 = (!ap_const_lv32_1F.is_01() || !msb_idx_16_cast_fu_14181_p1.read().is_01())? sc_lv<32>(): (sc_biguint<32>(ap_const_lv32_1F) - sc_biguint<32>(msb_idx_16_cast_fu_14181_p1.read()));
}

void mnist_fp16::thread_tmp_507_fu_7417_p3() {
    tmp_507_fu_7417_p3 = esl_concat<10,1>(tmp_504_fu_7400_p2.read(), ap_const_lv1_0);
}

void mnist_fp16::thread_tmp_508_fu_14261_p2() {
    tmp_508_fu_14261_p2 = (!p_Result_55_fu_14251_p4.read().is_01() || !ap_const_lv8_9E.is_01())? sc_lv<1>(): sc_lv<1>(p_Result_55_fu_14251_p4.read() != ap_const_lv8_9E);
}

void mnist_fp16::thread_tmp_509_fu_14315_p4() {
    tmp_509_fu_14315_p4 = p_03_i5_to_int_fu_14312_p1.read().range(30, 23);
}

void mnist_fp16::thread_tmp_510_fu_7429_p2() {
    tmp_510_fu_7429_p2 = (!p_shl58_cast_fu_7409_p3.read().is_01() || !p_shl59_cast_fu_7425_p1.read().is_01())? sc_lv<12>(): (sc_biguint<12>(p_shl58_cast_fu_7409_p3.read()) - sc_bigint<12>(p_shl59_cast_fu_7425_p1.read()));
}

void mnist_fp16::thread_tmp_511_fu_14333_p4() {
    tmp_511_fu_14333_p4 = tmp_457_to_int_fu_14329_p1.read().range(30, 23);
}

void mnist_fp16::thread_tmp_512_fu_13047_p2() {
    tmp_512_fu_13047_p2 = (!sh_amt_10_reg_18224.read().is_01() || !ap_const_lv12_36.is_01())? sc_lv<1>(): (sc_biguint<12>(sh_amt_10_reg_18224.read()) < sc_biguint<12>(ap_const_lv12_36));
}

void mnist_fp16::thread_tmp_513_fu_6514_p2() {
    tmp_513_fu_6514_p2 = (!tmp_607_cast_reg_16282.read().is_01() || !tmp_136_cast_cast_fu_6510_p1.read().is_01())? sc_lv<10>(): (sc_bigint<10>(tmp_607_cast_reg_16282.read()) + sc_biguint<10>(tmp_136_cast_cast_fu_6510_p1.read()));
}

void mnist_fp16::thread_tmp_514_fu_6519_p1() {
    tmp_514_fu_6519_p1 = tmp_513_fu_6514_p2.read().range(8-1, 0);
}

void mnist_fp16::thread_tmp_515_fu_14359_p2() {
    tmp_515_fu_14359_p2 = (notrhs10_fu_14353_p2.read() | notlhs10_fu_14347_p2.read());
}

void mnist_fp16::thread_tmp_516_fu_14377_p2() {
    tmp_516_fu_14377_p2 = (notrhs11_fu_14371_p2.read() | notlhs11_fu_14365_p2.read());
}

void mnist_fp16::thread_tmp_517_fu_14383_p2() {
    tmp_517_fu_14383_p2 = (tmp_515_fu_14359_p2.read() & tmp_516_fu_14377_p2.read());
}

void mnist_fp16::thread_tmp_519_fu_14389_p2() {
    tmp_519_fu_14389_p2 = (tmp_517_fu_14383_p2.read() & grp_fu_2839_p2.read());
}

void mnist_fp16::thread_tmp_51_fu_3612_p3() {
    tmp_51_fu_3612_p3 = esl_concat<3,5>(p_7_reg_1435.read(), ap_const_lv5_0);
}

void mnist_fp16::thread_tmp_520_fu_13052_p1() {
    tmp_520_fu_13052_p1 = esl_zext<54,32>(sh_amt_10_cast_fu_13044_p1.read());
}

}

