############################################################
## This file is generated automatically by Vivado HLS.
## Please DO NOT edit it.
## Copyright (C) 1986-2018 Xilinx, Inc. All Rights Reserved.
############################################################
open_project stream_duplicate
set_top stream_duplicate
add_files stream_duplicate/src/stream_duplicate.cpp
open_solution "default_solution"
set_part {xc7z020clg400-1}
create_clock -period 10 -name default
#source "./stream_duplicate/default_solution/directives.tcl"
#csim_design
csynth_design
#cosim_design -rtl vhdl
export_design -rtl vhdl -format ip_catalog
