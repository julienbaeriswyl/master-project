#include "mnist_fp8.h"
#include "AESL_pkg.h"

using namespace std;

namespace ap_rtl {

void mnist_fp8::thread_F2_1_fu_5288_p2() {
    F2_1_fu_5288_p2 = (!ap_const_lv12_433.is_01() || !tmp_99_fu_5261_p1.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_433) - sc_biguint<12>(tmp_99_fu_5261_p1.read()));
}

void mnist_fp8::thread_F2_2_fu_4376_p2() {
    F2_2_fu_4376_p2 = (!ap_const_lv12_433.is_01() || !tmp_106_fu_4349_p1.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_433) - sc_biguint<12>(tmp_106_fu_4349_p1.read()));
}

void mnist_fp8::thread_F2_3_fu_4461_p2() {
    F2_3_fu_4461_p2 = (!ap_const_lv12_433.is_01() || !tmp_170_fu_4434_p1.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_433) - sc_biguint<12>(tmp_170_fu_4434_p1.read()));
}

void mnist_fp8::thread_F2_4_fu_7017_p2() {
    F2_4_fu_7017_p2 = (!ap_const_lv12_433.is_01() || !tmp_210_fu_6990_p1.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_433) - sc_biguint<12>(tmp_210_fu_6990_p1.read()));
}

void mnist_fp8::thread_F2_5_fu_9155_p2() {
    F2_5_fu_9155_p2 = (!ap_const_lv12_433.is_01() || !tmp_277_fu_9128_p1.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_433) - sc_biguint<12>(tmp_277_fu_9128_p1.read()));
}

void mnist_fp8::thread_F2_6_fu_8243_p2() {
    F2_6_fu_8243_p2 = (!ap_const_lv12_433.is_01() || !tmp_284_fu_8216_p1.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_433) - sc_biguint<12>(tmp_284_fu_8216_p1.read()));
}

void mnist_fp8::thread_F2_7_fu_8328_p2() {
    F2_7_fu_8328_p2 = (!ap_const_lv12_433.is_01() || !tmp_334_fu_8301_p1.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_433) - sc_biguint<12>(tmp_334_fu_8301_p1.read()));
}

void mnist_fp8::thread_F2_fu_3014_p2() {
    F2_fu_3014_p2 = (!ap_const_lv12_433.is_01() || !tmp_32_fu_2987_p1.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_433) - sc_biguint<12>(tmp_32_fu_2987_p1.read()));
}

void mnist_fp8::thread_ap_CS_fsm_pp1_stage0() {
    ap_CS_fsm_pp1_stage0 = ap_CS_fsm.read()[4];
}

void mnist_fp8::thread_ap_CS_fsm_pp2_stage0() {
    ap_CS_fsm_pp2_stage0 = ap_CS_fsm.read()[6];
}

void mnist_fp8::thread_ap_CS_fsm_state1() {
    ap_CS_fsm_state1 = ap_CS_fsm.read()[0];
}

void mnist_fp8::thread_ap_CS_fsm_state10() {
    ap_CS_fsm_state10 = ap_CS_fsm.read()[7];
}

void mnist_fp8::thread_ap_CS_fsm_state100() {
    ap_CS_fsm_state100 = ap_CS_fsm.read()[97];
}

void mnist_fp8::thread_ap_CS_fsm_state101() {
    ap_CS_fsm_state101 = ap_CS_fsm.read()[98];
}

void mnist_fp8::thread_ap_CS_fsm_state102() {
    ap_CS_fsm_state102 = ap_CS_fsm.read()[99];
}

void mnist_fp8::thread_ap_CS_fsm_state103() {
    ap_CS_fsm_state103 = ap_CS_fsm.read()[100];
}

void mnist_fp8::thread_ap_CS_fsm_state104() {
    ap_CS_fsm_state104 = ap_CS_fsm.read()[101];
}

void mnist_fp8::thread_ap_CS_fsm_state109() {
    ap_CS_fsm_state109 = ap_CS_fsm.read()[106];
}

void mnist_fp8::thread_ap_CS_fsm_state11() {
    ap_CS_fsm_state11 = ap_CS_fsm.read()[8];
}

void mnist_fp8::thread_ap_CS_fsm_state110() {
    ap_CS_fsm_state110 = ap_CS_fsm.read()[107];
}

void mnist_fp8::thread_ap_CS_fsm_state111() {
    ap_CS_fsm_state111 = ap_CS_fsm.read()[108];
}

void mnist_fp8::thread_ap_CS_fsm_state112() {
    ap_CS_fsm_state112 = ap_CS_fsm.read()[109];
}

void mnist_fp8::thread_ap_CS_fsm_state113() {
    ap_CS_fsm_state113 = ap_CS_fsm.read()[110];
}

void mnist_fp8::thread_ap_CS_fsm_state114() {
    ap_CS_fsm_state114 = ap_CS_fsm.read()[111];
}

void mnist_fp8::thread_ap_CS_fsm_state115() {
    ap_CS_fsm_state115 = ap_CS_fsm.read()[112];
}

void mnist_fp8::thread_ap_CS_fsm_state116() {
    ap_CS_fsm_state116 = ap_CS_fsm.read()[113];
}

void mnist_fp8::thread_ap_CS_fsm_state117() {
    ap_CS_fsm_state117 = ap_CS_fsm.read()[114];
}

void mnist_fp8::thread_ap_CS_fsm_state118() {
    ap_CS_fsm_state118 = ap_CS_fsm.read()[115];
}

void mnist_fp8::thread_ap_CS_fsm_state119() {
    ap_CS_fsm_state119 = ap_CS_fsm.read()[116];
}

void mnist_fp8::thread_ap_CS_fsm_state12() {
    ap_CS_fsm_state12 = ap_CS_fsm.read()[9];
}

void mnist_fp8::thread_ap_CS_fsm_state120() {
    ap_CS_fsm_state120 = ap_CS_fsm.read()[117];
}

void mnist_fp8::thread_ap_CS_fsm_state121() {
    ap_CS_fsm_state121 = ap_CS_fsm.read()[118];
}

void mnist_fp8::thread_ap_CS_fsm_state122() {
    ap_CS_fsm_state122 = ap_CS_fsm.read()[119];
}

void mnist_fp8::thread_ap_CS_fsm_state123() {
    ap_CS_fsm_state123 = ap_CS_fsm.read()[120];
}

void mnist_fp8::thread_ap_CS_fsm_state124() {
    ap_CS_fsm_state124 = ap_CS_fsm.read()[121];
}

void mnist_fp8::thread_ap_CS_fsm_state125() {
    ap_CS_fsm_state125 = ap_CS_fsm.read()[122];
}

void mnist_fp8::thread_ap_CS_fsm_state126() {
    ap_CS_fsm_state126 = ap_CS_fsm.read()[123];
}

void mnist_fp8::thread_ap_CS_fsm_state127() {
    ap_CS_fsm_state127 = ap_CS_fsm.read()[124];
}

void mnist_fp8::thread_ap_CS_fsm_state128() {
    ap_CS_fsm_state128 = ap_CS_fsm.read()[125];
}

void mnist_fp8::thread_ap_CS_fsm_state13() {
    ap_CS_fsm_state13 = ap_CS_fsm.read()[10];
}

void mnist_fp8::thread_ap_CS_fsm_state133() {
    ap_CS_fsm_state133 = ap_CS_fsm.read()[130];
}

void mnist_fp8::thread_ap_CS_fsm_state134() {
    ap_CS_fsm_state134 = ap_CS_fsm.read()[131];
}

void mnist_fp8::thread_ap_CS_fsm_state135() {
    ap_CS_fsm_state135 = ap_CS_fsm.read()[132];
}

void mnist_fp8::thread_ap_CS_fsm_state136() {
    ap_CS_fsm_state136 = ap_CS_fsm.read()[133];
}

void mnist_fp8::thread_ap_CS_fsm_state137() {
    ap_CS_fsm_state137 = ap_CS_fsm.read()[134];
}

void mnist_fp8::thread_ap_CS_fsm_state138() {
    ap_CS_fsm_state138 = ap_CS_fsm.read()[135];
}

void mnist_fp8::thread_ap_CS_fsm_state139() {
    ap_CS_fsm_state139 = ap_CS_fsm.read()[136];
}

void mnist_fp8::thread_ap_CS_fsm_state14() {
    ap_CS_fsm_state14 = ap_CS_fsm.read()[11];
}

void mnist_fp8::thread_ap_CS_fsm_state140() {
    ap_CS_fsm_state140 = ap_CS_fsm.read()[137];
}

void mnist_fp8::thread_ap_CS_fsm_state154() {
    ap_CS_fsm_state154 = ap_CS_fsm.read()[151];
}

void mnist_fp8::thread_ap_CS_fsm_state155() {
    ap_CS_fsm_state155 = ap_CS_fsm.read()[152];
}

void mnist_fp8::thread_ap_CS_fsm_state156() {
    ap_CS_fsm_state156 = ap_CS_fsm.read()[153];
}

void mnist_fp8::thread_ap_CS_fsm_state157() {
    ap_CS_fsm_state157 = ap_CS_fsm.read()[154];
}

void mnist_fp8::thread_ap_CS_fsm_state158() {
    ap_CS_fsm_state158 = ap_CS_fsm.read()[155];
}

void mnist_fp8::thread_ap_CS_fsm_state159() {
    ap_CS_fsm_state159 = ap_CS_fsm.read()[156];
}

void mnist_fp8::thread_ap_CS_fsm_state160() {
    ap_CS_fsm_state160 = ap_CS_fsm.read()[157];
}

void mnist_fp8::thread_ap_CS_fsm_state161() {
    ap_CS_fsm_state161 = ap_CS_fsm.read()[158];
}

void mnist_fp8::thread_ap_CS_fsm_state162() {
    ap_CS_fsm_state162 = ap_CS_fsm.read()[159];
}

void mnist_fp8::thread_ap_CS_fsm_state163() {
    ap_CS_fsm_state163 = ap_CS_fsm.read()[160];
}

void mnist_fp8::thread_ap_CS_fsm_state164() {
    ap_CS_fsm_state164 = ap_CS_fsm.read()[161];
}

void mnist_fp8::thread_ap_CS_fsm_state165() {
    ap_CS_fsm_state165 = ap_CS_fsm.read()[162];
}

void mnist_fp8::thread_ap_CS_fsm_state168() {
    ap_CS_fsm_state168 = ap_CS_fsm.read()[165];
}

void mnist_fp8::thread_ap_CS_fsm_state169() {
    ap_CS_fsm_state169 = ap_CS_fsm.read()[166];
}

void mnist_fp8::thread_ap_CS_fsm_state173() {
    ap_CS_fsm_state173 = ap_CS_fsm.read()[170];
}

void mnist_fp8::thread_ap_CS_fsm_state174() {
    ap_CS_fsm_state174 = ap_CS_fsm.read()[171];
}

void mnist_fp8::thread_ap_CS_fsm_state175() {
    ap_CS_fsm_state175 = ap_CS_fsm.read()[172];
}

void mnist_fp8::thread_ap_CS_fsm_state176() {
    ap_CS_fsm_state176 = ap_CS_fsm.read()[173];
}

void mnist_fp8::thread_ap_CS_fsm_state177() {
    ap_CS_fsm_state177 = ap_CS_fsm.read()[174];
}

void mnist_fp8::thread_ap_CS_fsm_state178() {
    ap_CS_fsm_state178 = ap_CS_fsm.read()[175];
}

void mnist_fp8::thread_ap_CS_fsm_state179() {
    ap_CS_fsm_state179 = ap_CS_fsm.read()[176];
}

void mnist_fp8::thread_ap_CS_fsm_state180() {
    ap_CS_fsm_state180 = ap_CS_fsm.read()[177];
}

void mnist_fp8::thread_ap_CS_fsm_state181() {
    ap_CS_fsm_state181 = ap_CS_fsm.read()[178];
}

void mnist_fp8::thread_ap_CS_fsm_state182() {
    ap_CS_fsm_state182 = ap_CS_fsm.read()[179];
}

void mnist_fp8::thread_ap_CS_fsm_state183() {
    ap_CS_fsm_state183 = ap_CS_fsm.read()[180];
}

void mnist_fp8::thread_ap_CS_fsm_state184() {
    ap_CS_fsm_state184 = ap_CS_fsm.read()[181];
}

void mnist_fp8::thread_ap_CS_fsm_state185() {
    ap_CS_fsm_state185 = ap_CS_fsm.read()[182];
}

void mnist_fp8::thread_ap_CS_fsm_state186() {
    ap_CS_fsm_state186 = ap_CS_fsm.read()[183];
}

void mnist_fp8::thread_ap_CS_fsm_state187() {
    ap_CS_fsm_state187 = ap_CS_fsm.read()[184];
}

void mnist_fp8::thread_ap_CS_fsm_state2() {
    ap_CS_fsm_state2 = ap_CS_fsm.read()[1];
}

void mnist_fp8::thread_ap_CS_fsm_state202() {
    ap_CS_fsm_state202 = ap_CS_fsm.read()[199];
}

void mnist_fp8::thread_ap_CS_fsm_state203() {
    ap_CS_fsm_state203 = ap_CS_fsm.read()[200];
}

void mnist_fp8::thread_ap_CS_fsm_state204() {
    ap_CS_fsm_state204 = ap_CS_fsm.read()[201];
}

void mnist_fp8::thread_ap_CS_fsm_state205() {
    ap_CS_fsm_state205 = ap_CS_fsm.read()[202];
}

void mnist_fp8::thread_ap_CS_fsm_state206() {
    ap_CS_fsm_state206 = ap_CS_fsm.read()[203];
}

void mnist_fp8::thread_ap_CS_fsm_state207() {
    ap_CS_fsm_state207 = ap_CS_fsm.read()[204];
}

void mnist_fp8::thread_ap_CS_fsm_state212() {
    ap_CS_fsm_state212 = ap_CS_fsm.read()[209];
}

void mnist_fp8::thread_ap_CS_fsm_state213() {
    ap_CS_fsm_state213 = ap_CS_fsm.read()[210];
}

void mnist_fp8::thread_ap_CS_fsm_state214() {
    ap_CS_fsm_state214 = ap_CS_fsm.read()[211];
}

void mnist_fp8::thread_ap_CS_fsm_state215() {
    ap_CS_fsm_state215 = ap_CS_fsm.read()[212];
}

void mnist_fp8::thread_ap_CS_fsm_state216() {
    ap_CS_fsm_state216 = ap_CS_fsm.read()[213];
}

void mnist_fp8::thread_ap_CS_fsm_state217() {
    ap_CS_fsm_state217 = ap_CS_fsm.read()[214];
}

void mnist_fp8::thread_ap_CS_fsm_state218() {
    ap_CS_fsm_state218 = ap_CS_fsm.read()[215];
}

void mnist_fp8::thread_ap_CS_fsm_state219() {
    ap_CS_fsm_state219 = ap_CS_fsm.read()[216];
}

void mnist_fp8::thread_ap_CS_fsm_state220() {
    ap_CS_fsm_state220 = ap_CS_fsm.read()[217];
}

void mnist_fp8::thread_ap_CS_fsm_state221() {
    ap_CS_fsm_state221 = ap_CS_fsm.read()[218];
}

void mnist_fp8::thread_ap_CS_fsm_state222() {
    ap_CS_fsm_state222 = ap_CS_fsm.read()[219];
}

void mnist_fp8::thread_ap_CS_fsm_state223() {
    ap_CS_fsm_state223 = ap_CS_fsm.read()[220];
}

void mnist_fp8::thread_ap_CS_fsm_state224() {
    ap_CS_fsm_state224 = ap_CS_fsm.read()[221];
}

void mnist_fp8::thread_ap_CS_fsm_state225() {
    ap_CS_fsm_state225 = ap_CS_fsm.read()[222];
}

void mnist_fp8::thread_ap_CS_fsm_state226() {
    ap_CS_fsm_state226 = ap_CS_fsm.read()[223];
}

void mnist_fp8::thread_ap_CS_fsm_state227() {
    ap_CS_fsm_state227 = ap_CS_fsm.read()[224];
}

void mnist_fp8::thread_ap_CS_fsm_state232() {
    ap_CS_fsm_state232 = ap_CS_fsm.read()[229];
}

void mnist_fp8::thread_ap_CS_fsm_state233() {
    ap_CS_fsm_state233 = ap_CS_fsm.read()[230];
}

void mnist_fp8::thread_ap_CS_fsm_state234() {
    ap_CS_fsm_state234 = ap_CS_fsm.read()[231];
}

void mnist_fp8::thread_ap_CS_fsm_state235() {
    ap_CS_fsm_state235 = ap_CS_fsm.read()[232];
}

void mnist_fp8::thread_ap_CS_fsm_state236() {
    ap_CS_fsm_state236 = ap_CS_fsm.read()[233];
}

void mnist_fp8::thread_ap_CS_fsm_state237() {
    ap_CS_fsm_state237 = ap_CS_fsm.read()[234];
}

void mnist_fp8::thread_ap_CS_fsm_state238() {
    ap_CS_fsm_state238 = ap_CS_fsm.read()[235];
}

void mnist_fp8::thread_ap_CS_fsm_state239() {
    ap_CS_fsm_state239 = ap_CS_fsm.read()[236];
}

void mnist_fp8::thread_ap_CS_fsm_state240() {
    ap_CS_fsm_state240 = ap_CS_fsm.read()[237];
}

void mnist_fp8::thread_ap_CS_fsm_state241() {
    ap_CS_fsm_state241 = ap_CS_fsm.read()[238];
}

void mnist_fp8::thread_ap_CS_fsm_state242() {
    ap_CS_fsm_state242 = ap_CS_fsm.read()[239];
}

void mnist_fp8::thread_ap_CS_fsm_state243() {
    ap_CS_fsm_state243 = ap_CS_fsm.read()[240];
}

void mnist_fp8::thread_ap_CS_fsm_state244() {
    ap_CS_fsm_state244 = ap_CS_fsm.read()[241];
}

void mnist_fp8::thread_ap_CS_fsm_state245() {
    ap_CS_fsm_state245 = ap_CS_fsm.read()[242];
}

void mnist_fp8::thread_ap_CS_fsm_state246() {
    ap_CS_fsm_state246 = ap_CS_fsm.read()[243];
}

void mnist_fp8::thread_ap_CS_fsm_state247() {
    ap_CS_fsm_state247 = ap_CS_fsm.read()[244];
}

void mnist_fp8::thread_ap_CS_fsm_state248() {
    ap_CS_fsm_state248 = ap_CS_fsm.read()[245];
}

void mnist_fp8::thread_ap_CS_fsm_state249() {
    ap_CS_fsm_state249 = ap_CS_fsm.read()[246];
}

void mnist_fp8::thread_ap_CS_fsm_state250() {
    ap_CS_fsm_state250 = ap_CS_fsm.read()[247];
}

void mnist_fp8::thread_ap_CS_fsm_state251() {
    ap_CS_fsm_state251 = ap_CS_fsm.read()[248];
}

void mnist_fp8::thread_ap_CS_fsm_state252() {
    ap_CS_fsm_state252 = ap_CS_fsm.read()[249];
}

void mnist_fp8::thread_ap_CS_fsm_state253() {
    ap_CS_fsm_state253 = ap_CS_fsm.read()[250];
}

void mnist_fp8::thread_ap_CS_fsm_state254() {
    ap_CS_fsm_state254 = ap_CS_fsm.read()[251];
}

void mnist_fp8::thread_ap_CS_fsm_state255() {
    ap_CS_fsm_state255 = ap_CS_fsm.read()[252];
}

void mnist_fp8::thread_ap_CS_fsm_state256() {
    ap_CS_fsm_state256 = ap_CS_fsm.read()[253];
}

void mnist_fp8::thread_ap_CS_fsm_state257() {
    ap_CS_fsm_state257 = ap_CS_fsm.read()[254];
}

void mnist_fp8::thread_ap_CS_fsm_state258() {
    ap_CS_fsm_state258 = ap_CS_fsm.read()[255];
}

void mnist_fp8::thread_ap_CS_fsm_state259() {
    ap_CS_fsm_state259 = ap_CS_fsm.read()[256];
}

void mnist_fp8::thread_ap_CS_fsm_state260() {
    ap_CS_fsm_state260 = ap_CS_fsm.read()[257];
}

void mnist_fp8::thread_ap_CS_fsm_state261() {
    ap_CS_fsm_state261 = ap_CS_fsm.read()[258];
}

void mnist_fp8::thread_ap_CS_fsm_state262() {
    ap_CS_fsm_state262 = ap_CS_fsm.read()[259];
}

void mnist_fp8::thread_ap_CS_fsm_state263() {
    ap_CS_fsm_state263 = ap_CS_fsm.read()[260];
}

void mnist_fp8::thread_ap_CS_fsm_state264() {
    ap_CS_fsm_state264 = ap_CS_fsm.read()[261];
}

void mnist_fp8::thread_ap_CS_fsm_state265() {
    ap_CS_fsm_state265 = ap_CS_fsm.read()[262];
}

void mnist_fp8::thread_ap_CS_fsm_state266() {
    ap_CS_fsm_state266 = ap_CS_fsm.read()[263];
}

void mnist_fp8::thread_ap_CS_fsm_state267() {
    ap_CS_fsm_state267 = ap_CS_fsm.read()[264];
}

void mnist_fp8::thread_ap_CS_fsm_state268() {
    ap_CS_fsm_state268 = ap_CS_fsm.read()[265];
}

void mnist_fp8::thread_ap_CS_fsm_state29() {
    ap_CS_fsm_state29 = ap_CS_fsm.read()[26];
}

void mnist_fp8::thread_ap_CS_fsm_state3() {
    ap_CS_fsm_state3 = ap_CS_fsm.read()[2];
}

void mnist_fp8::thread_ap_CS_fsm_state30() {
    ap_CS_fsm_state30 = ap_CS_fsm.read()[27];
}

void mnist_fp8::thread_ap_CS_fsm_state31() {
    ap_CS_fsm_state31 = ap_CS_fsm.read()[28];
}

void mnist_fp8::thread_ap_CS_fsm_state32() {
    ap_CS_fsm_state32 = ap_CS_fsm.read()[29];
}

void mnist_fp8::thread_ap_CS_fsm_state33() {
    ap_CS_fsm_state33 = ap_CS_fsm.read()[30];
}

void mnist_fp8::thread_ap_CS_fsm_state34() {
    ap_CS_fsm_state34 = ap_CS_fsm.read()[31];
}

void mnist_fp8::thread_ap_CS_fsm_state35() {
    ap_CS_fsm_state35 = ap_CS_fsm.read()[32];
}

void mnist_fp8::thread_ap_CS_fsm_state36() {
    ap_CS_fsm_state36 = ap_CS_fsm.read()[33];
}

void mnist_fp8::thread_ap_CS_fsm_state37() {
    ap_CS_fsm_state37 = ap_CS_fsm.read()[34];
}

void mnist_fp8::thread_ap_CS_fsm_state38() {
    ap_CS_fsm_state38 = ap_CS_fsm.read()[35];
}

void mnist_fp8::thread_ap_CS_fsm_state39() {
    ap_CS_fsm_state39 = ap_CS_fsm.read()[36];
}

void mnist_fp8::thread_ap_CS_fsm_state4() {
    ap_CS_fsm_state4 = ap_CS_fsm.read()[3];
}

void mnist_fp8::thread_ap_CS_fsm_state42() {
    ap_CS_fsm_state42 = ap_CS_fsm.read()[39];
}

void mnist_fp8::thread_ap_CS_fsm_state43() {
    ap_CS_fsm_state43 = ap_CS_fsm.read()[40];
}

void mnist_fp8::thread_ap_CS_fsm_state47() {
    ap_CS_fsm_state47 = ap_CS_fsm.read()[44];
}

void mnist_fp8::thread_ap_CS_fsm_state48() {
    ap_CS_fsm_state48 = ap_CS_fsm.read()[45];
}

void mnist_fp8::thread_ap_CS_fsm_state49() {
    ap_CS_fsm_state49 = ap_CS_fsm.read()[46];
}

void mnist_fp8::thread_ap_CS_fsm_state50() {
    ap_CS_fsm_state50 = ap_CS_fsm.read()[47];
}

void mnist_fp8::thread_ap_CS_fsm_state51() {
    ap_CS_fsm_state51 = ap_CS_fsm.read()[48];
}

void mnist_fp8::thread_ap_CS_fsm_state52() {
    ap_CS_fsm_state52 = ap_CS_fsm.read()[49];
}

void mnist_fp8::thread_ap_CS_fsm_state53() {
    ap_CS_fsm_state53 = ap_CS_fsm.read()[50];
}

void mnist_fp8::thread_ap_CS_fsm_state54() {
    ap_CS_fsm_state54 = ap_CS_fsm.read()[51];
}

void mnist_fp8::thread_ap_CS_fsm_state55() {
    ap_CS_fsm_state55 = ap_CS_fsm.read()[52];
}

void mnist_fp8::thread_ap_CS_fsm_state56() {
    ap_CS_fsm_state56 = ap_CS_fsm.read()[53];
}

void mnist_fp8::thread_ap_CS_fsm_state57() {
    ap_CS_fsm_state57 = ap_CS_fsm.read()[54];
}

void mnist_fp8::thread_ap_CS_fsm_state58() {
    ap_CS_fsm_state58 = ap_CS_fsm.read()[55];
}

void mnist_fp8::thread_ap_CS_fsm_state59() {
    ap_CS_fsm_state59 = ap_CS_fsm.read()[56];
}

void mnist_fp8::thread_ap_CS_fsm_state60() {
    ap_CS_fsm_state60 = ap_CS_fsm.read()[57];
}

void mnist_fp8::thread_ap_CS_fsm_state61() {
    ap_CS_fsm_state61 = ap_CS_fsm.read()[58];
}

void mnist_fp8::thread_ap_CS_fsm_state7() {
    ap_CS_fsm_state7 = ap_CS_fsm.read()[5];
}

void mnist_fp8::thread_ap_CS_fsm_state78() {
    ap_CS_fsm_state78 = ap_CS_fsm.read()[75];
}

void mnist_fp8::thread_ap_CS_fsm_state79() {
    ap_CS_fsm_state79 = ap_CS_fsm.read()[76];
}

void mnist_fp8::thread_ap_CS_fsm_state80() {
    ap_CS_fsm_state80 = ap_CS_fsm.read()[77];
}

void mnist_fp8::thread_ap_CS_fsm_state81() {
    ap_CS_fsm_state81 = ap_CS_fsm.read()[78];
}

void mnist_fp8::thread_ap_CS_fsm_state82() {
    ap_CS_fsm_state82 = ap_CS_fsm.read()[79];
}

void mnist_fp8::thread_ap_CS_fsm_state83() {
    ap_CS_fsm_state83 = ap_CS_fsm.read()[80];
}

void mnist_fp8::thread_ap_CS_fsm_state88() {
    ap_CS_fsm_state88 = ap_CS_fsm.read()[85];
}

void mnist_fp8::thread_ap_CS_fsm_state89() {
    ap_CS_fsm_state89 = ap_CS_fsm.read()[86];
}

void mnist_fp8::thread_ap_CS_fsm_state90() {
    ap_CS_fsm_state90 = ap_CS_fsm.read()[87];
}

void mnist_fp8::thread_ap_CS_fsm_state91() {
    ap_CS_fsm_state91 = ap_CS_fsm.read()[88];
}

void mnist_fp8::thread_ap_CS_fsm_state92() {
    ap_CS_fsm_state92 = ap_CS_fsm.read()[89];
}

void mnist_fp8::thread_ap_CS_fsm_state93() {
    ap_CS_fsm_state93 = ap_CS_fsm.read()[90];
}

void mnist_fp8::thread_ap_CS_fsm_state94() {
    ap_CS_fsm_state94 = ap_CS_fsm.read()[91];
}

void mnist_fp8::thread_ap_CS_fsm_state95() {
    ap_CS_fsm_state95 = ap_CS_fsm.read()[92];
}

void mnist_fp8::thread_ap_CS_fsm_state96() {
    ap_CS_fsm_state96 = ap_CS_fsm.read()[93];
}

void mnist_fp8::thread_ap_CS_fsm_state97() {
    ap_CS_fsm_state97 = ap_CS_fsm.read()[94];
}

void mnist_fp8::thread_ap_CS_fsm_state98() {
    ap_CS_fsm_state98 = ap_CS_fsm.read()[95];
}

void mnist_fp8::thread_ap_CS_fsm_state99() {
    ap_CS_fsm_state99 = ap_CS_fsm.read()[96];
}

void mnist_fp8::thread_ap_block_pp1_stage0() {
    ap_block_pp1_stage0 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void mnist_fp8::thread_ap_block_pp1_stage0_11001() {
    ap_block_pp1_stage0_11001 = (esl_seteq<1,1,1>(ap_enable_reg_pp1_iter1.read(), ap_const_logic_1) && esl_seteq<1,1,1>(ap_const_logic_0, stream_in_V_data_V_0_vld_out.read()) && esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op348_read_state6.read()));
}

void mnist_fp8::thread_ap_block_pp1_stage0_subdone() {
    ap_block_pp1_stage0_subdone = (esl_seteq<1,1,1>(ap_enable_reg_pp1_iter1.read(), ap_const_logic_1) && esl_seteq<1,1,1>(ap_const_logic_0, stream_in_V_data_V_0_vld_out.read()) && esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op348_read_state6.read()));
}

void mnist_fp8::thread_ap_block_pp2_stage0() {
    ap_block_pp2_stage0 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void mnist_fp8::thread_ap_block_pp2_stage0_11001() {
    ap_block_pp2_stage0_11001 = (esl_seteq<1,1,1>(ap_enable_reg_pp2_iter1.read(), ap_const_logic_1) && esl_seteq<1,1,1>(ap_const_lv1_0, eol_2_reg_898.read()) && esl_seteq<1,1,1>(ap_const_logic_0, stream_in_V_data_V_0_vld_out.read()));
}

void mnist_fp8::thread_ap_block_pp2_stage0_subdone() {
    ap_block_pp2_stage0_subdone = (esl_seteq<1,1,1>(ap_enable_reg_pp2_iter1.read(), ap_const_logic_1) && esl_seteq<1,1,1>(ap_const_lv1_0, eol_2_reg_898.read()) && esl_seteq<1,1,1>(ap_const_logic_0, stream_in_V_data_V_0_vld_out.read()));
}

void mnist_fp8::thread_ap_block_state5_pp1_stage0_iter0() {
    ap_block_state5_pp1_stage0_iter0 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void mnist_fp8::thread_ap_block_state6_pp1_stage0_iter1() {
    ap_block_state6_pp1_stage0_iter1 = (esl_seteq<1,1,1>(ap_const_logic_0, stream_in_V_data_V_0_vld_out.read()) && esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op348_read_state6.read()));
}

void mnist_fp8::thread_ap_block_state8_pp2_stage0_iter0() {
    ap_block_state8_pp2_stage0_iter0 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void mnist_fp8::thread_ap_block_state9_pp2_stage0_iter1() {
    ap_block_state9_pp2_stage0_iter1 = (esl_seteq<1,1,1>(ap_const_lv1_0, eol_2_reg_898.read()) && esl_seteq<1,1,1>(ap_const_logic_0, stream_in_V_data_V_0_vld_out.read()));
}

void mnist_fp8::thread_ap_condition_2746() {
    ap_condition_2746 = (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage0.read()) && esl_seteq<1,1,1>(ap_enable_reg_pp1_iter1.read(), ap_const_logic_1) && esl_seteq<1,1,1>(ap_block_pp1_stage0.read(), ap_const_boolean_0) && esl_seteq<1,1,1>(ap_const_lv1_0, tmp_2_reg_9782.read()));
}

void mnist_fp8::thread_ap_done() {
    if ((esl_seteq<1,1,1>(exitcond54_fu_9602_p2.read(), ap_const_lv1_1) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state263.read()))) {
        ap_done = ap_const_logic_1;
    } else {
        ap_done = ap_const_logic_0;
    }
}

void mnist_fp8::thread_ap_enable_pp1() {
    ap_enable_pp1 = (ap_idle_pp1.read() ^ ap_const_logic_1);
}

void mnist_fp8::thread_ap_enable_pp2() {
    ap_enable_pp2 = (ap_idle_pp2.read() ^ ap_const_logic_1);
}

void mnist_fp8::thread_ap_idle() {
    if ((esl_seteq<1,1,1>(ap_const_logic_0, ap_start.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state1.read()))) {
        ap_idle = ap_const_logic_1;
    } else {
        ap_idle = ap_const_logic_0;
    }
}

void mnist_fp8::thread_ap_idle_pp1() {
    if ((esl_seteq<1,1,1>(ap_const_logic_0, ap_enable_reg_pp1_iter0.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_0, ap_enable_reg_pp1_iter1.read()))) {
        ap_idle_pp1 = ap_const_logic_1;
    } else {
        ap_idle_pp1 = ap_const_logic_0;
    }
}

void mnist_fp8::thread_ap_idle_pp2() {
    if ((esl_seteq<1,1,1>(ap_const_logic_0, ap_enable_reg_pp2_iter0.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_0, ap_enable_reg_pp2_iter1.read()))) {
        ap_idle_pp2 = ap_const_logic_1;
    } else {
        ap_idle_pp2 = ap_const_logic_0;
    }
}

void mnist_fp8::thread_ap_phi_mux_eol_2_phi_fu_901_p4() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp2_stage0.read()) && 
         esl_seteq<1,1,1>(ap_enable_reg_pp2_iter1.read(), ap_const_logic_1) && 
         esl_seteq<1,1,1>(ap_block_pp2_stage0.read(), ap_const_boolean_0) && 
         esl_seteq<1,1,1>(ap_const_lv1_0, eol_2_reg_898.read()))) {
        ap_phi_mux_eol_2_phi_fu_901_p4 = stream_in_V_last_V_0_data_out.read();
    } else {
        ap_phi_mux_eol_2_phi_fu_901_p4 = eol_2_reg_898.read();
    }
}

void mnist_fp8::thread_ap_phi_mux_eol_phi_fu_843_p4() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage0.read()) && 
         esl_seteq<1,1,1>(ap_enable_reg_pp1_iter1.read(), ap_const_logic_1) && 
         esl_seteq<1,1,1>(ap_block_pp1_stage0.read(), ap_const_boolean_0) && 
         esl_seteq<1,1,1>(ap_const_lv1_0, tmp_2_reg_9782.read()))) {
        ap_phi_mux_eol_phi_fu_843_p4 = ap_phi_mux_pixel_last_V_2_phi_fu_878_p4.read();
    } else {
        ap_phi_mux_eol_phi_fu_843_p4 = eol_reg_839.read();
    }
}

void mnist_fp8::thread_ap_phi_mux_p_1_phi_fu_831_p4() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage0.read()) && 
         esl_seteq<1,1,1>(ap_enable_reg_pp1_iter1.read(), ap_const_logic_1) && 
         esl_seteq<1,1,1>(ap_block_pp1_stage0.read(), ap_const_boolean_0) && 
         esl_seteq<1,1,1>(ap_const_lv1_0, tmp_2_reg_9782.read()))) {
        ap_phi_mux_p_1_phi_fu_831_p4 = j_V_reg_9786.read();
    } else {
        ap_phi_mux_p_1_phi_fu_831_p4 = p_1_reg_827.read();
    }
}

void mnist_fp8::thread_ap_phi_mux_p_4_phi_fu_1113_p4() {
    ap_phi_mux_p_4_phi_fu_1113_p4 = p_4_reg_1109.read();
}

void mnist_fp8::thread_ap_phi_mux_p_s_phi_fu_937_p4() {
    ap_phi_mux_p_s_phi_fu_937_p4 = p_s_reg_933.read();
}

void mnist_fp8::thread_ap_phi_mux_pixel_data_V_2_phi_fu_890_p4() {
    if (esl_seteq<1,1,1>(ap_condition_2746.read(), ap_const_boolean_1)) {
        if (esl_seteq<1,1,1>(ap_const_lv1_1, brmerge_reg_9791.read())) {
            ap_phi_mux_pixel_data_V_2_phi_fu_890_p4 = pixel_data_V_1_reg_862.read();
        } else if (esl_seteq<1,1,1>(ap_const_lv1_0, brmerge_reg_9791.read())) {
            ap_phi_mux_pixel_data_V_2_phi_fu_890_p4 = stream_in_V_data_V_0_data_out.read();
        } else {
            ap_phi_mux_pixel_data_V_2_phi_fu_890_p4 = ap_phi_reg_pp1_iter1_pixel_data_V_2_reg_886.read();
        }
    } else {
        ap_phi_mux_pixel_data_V_2_phi_fu_890_p4 = ap_phi_reg_pp1_iter1_pixel_data_V_2_reg_886.read();
    }
}

void mnist_fp8::thread_ap_phi_mux_pixel_last_V_2_phi_fu_878_p4() {
    if (esl_seteq<1,1,1>(ap_condition_2746.read(), ap_const_boolean_1)) {
        if (esl_seteq<1,1,1>(ap_const_lv1_1, brmerge_reg_9791.read())) {
            ap_phi_mux_pixel_last_V_2_phi_fu_878_p4 = eol_1_reg_851.read();
        } else if (esl_seteq<1,1,1>(ap_const_lv1_0, brmerge_reg_9791.read())) {
            ap_phi_mux_pixel_last_V_2_phi_fu_878_p4 = stream_in_V_last_V_0_data_out.read();
        } else {
            ap_phi_mux_pixel_last_V_2_phi_fu_878_p4 = ap_phi_reg_pp1_iter1_pixel_last_V_2_reg_873.read();
        }
    } else {
        ap_phi_mux_pixel_last_V_2_phi_fu_878_p4 = ap_phi_reg_pp1_iter1_pixel_last_V_2_reg_873.read();
    }
}

void mnist_fp8::thread_ap_phi_mux_tmp_257_phi_fu_1613_p6() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state213.read()) && 
         esl_seteq<1,1,1>(ap_const_lv1_1, or_cond_42_reg_11264.read()) && 
         esl_seteq<1,1,1>(ap_const_lv1_0, tmp_168_reg_11338.read()))) {
        ap_phi_mux_tmp_257_phi_fu_1613_p6 = f_5_fu_7852_p1.read();
    } else {
        ap_phi_mux_tmp_257_phi_fu_1613_p6 = tmp_257_reg_1609.read();
    }
}

void mnist_fp8::thread_ap_phi_reg_pp1_iter1_pixel_data_V_2_reg_886() {
    ap_phi_reg_pp1_iter1_pixel_data_V_2_reg_886 =  (sc_lv<32>) ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
}

void mnist_fp8::thread_ap_phi_reg_pp1_iter1_pixel_last_V_2_reg_873() {
    ap_phi_reg_pp1_iter1_pixel_last_V_2_reg_873 =  (sc_lv<1>) ("X");
}

void mnist_fp8::thread_ap_predicate_op348_read_state6() {
    ap_predicate_op348_read_state6 = (esl_seteq<1,1,1>(ap_const_lv1_0, tmp_2_reg_9782.read()) && esl_seteq<1,1,1>(ap_const_lv1_0, brmerge_reg_9791.read()));
}

void mnist_fp8::thread_ap_ready() {
    if ((esl_seteq<1,1,1>(exitcond54_fu_9602_p2.read(), ap_const_lv1_1) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state263.read()))) {
        ap_ready = ap_const_logic_1;
    } else {
        ap_ready = ap_const_logic_0;
    }
}

void mnist_fp8::thread_ap_rst_n_inv() {
    ap_rst_n_inv =  (sc_logic) (~ap_rst_n.read());
}

void mnist_fp8::thread_args01_V_fu_5062_p2() {
    args01_V_fu_5062_p2 = (!p_19_reg_1255.read().is_01() || !ap_const_lv3_1.is_01())? sc_lv<3>(): (sc_biguint<3>(p_19_reg_1255.read()) + sc_biguint<3>(ap_const_lv3_1));
}

void mnist_fp8::thread_args02_V_fu_6791_p2() {
    args02_V_fu_6791_p2 = (!p_36_reg_1531.read().is_01() || !ap_const_lv4_1.is_01())? sc_lv<4>(): (sc_biguint<4>(p_36_reg_1531.read()) + sc_biguint<4>(ap_const_lv4_1));
}

void mnist_fp8::thread_args03_V_fu_8929_p2() {
    args03_V_fu_8929_p2 = (!p_50_reg_1729.read().is_01() || !ap_const_lv4_1.is_01())? sc_lv<4>(): (sc_biguint<4>(p_50_reg_1729.read()) + sc_biguint<4>(ap_const_lv4_1));
}

void mnist_fp8::thread_args04_V_fu_9536_p2() {
    args04_V_fu_9536_p2 = (!p_68_reg_1916.read().is_01() || !ap_const_lv5_1.is_01())? sc_lv<5>(): (sc_biguint<5>(p_68_reg_1916.read()) + sc_biguint<5>(ap_const_lv5_1));
}

void mnist_fp8::thread_args0_V_fu_2788_p2() {
    args0_V_fu_2788_p2 = (!p_7_reg_1054.read().is_01() || !ap_const_lv3_1.is_01())? sc_lv<3>(): (sc_biguint<3>(p_7_reg_1054.read()) + sc_biguint<3>(ap_const_lv3_1));
}

void mnist_fp8::thread_args11_V_fu_5108_p2() {
    args11_V_fu_5108_p2 = (!p_23_reg_1266.read().is_01() || !ap_const_lv5_1.is_01())? sc_lv<5>(): (sc_biguint<5>(p_23_reg_1266.read()) + sc_biguint<5>(ap_const_lv5_1));
}

void mnist_fp8::thread_args12_V_fu_6837_p2() {
    args12_V_fu_6837_p2 = (!p_41_reg_1542.read().is_01() || !ap_const_lv4_1.is_01())? sc_lv<4>(): (sc_biguint<4>(p_41_reg_1542.read()) + sc_biguint<4>(ap_const_lv4_1));
}

void mnist_fp8::thread_args13_V_fu_8975_p2() {
    args13_V_fu_8975_p2 = (!p_55_reg_1740.read().is_01() || !ap_const_lv4_1.is_01())? sc_lv<4>(): (sc_biguint<4>(p_55_reg_1740.read()) + sc_biguint<4>(ap_const_lv4_1));
}

void mnist_fp8::thread_args14_V_fu_9548_p2() {
    args14_V_fu_9548_p2 = (!p_70_reg_1927.read().is_01() || !ap_const_lv3_1.is_01())? sc_lv<3>(): (sc_biguint<3>(p_70_reg_1927.read()) + sc_biguint<3>(ap_const_lv3_1));
}

void mnist_fp8::thread_args1_V_fu_2834_p2() {
    args1_V_fu_2834_p2 = (!p_11_reg_1065.read().is_01() || !ap_const_lv5_1.is_01())? sc_lv<5>(): (sc_biguint<5>(p_11_reg_1065.read()) + sc_biguint<5>(ap_const_lv5_1));
}

void mnist_fp8::thread_args21_V_fu_5159_p2() {
    args21_V_fu_5159_p2 = (!p_28_reg_1277.read().is_01() || !ap_const_lv5_1.is_01())? sc_lv<5>(): (sc_biguint<5>(p_28_reg_1277.read()) + sc_biguint<5>(ap_const_lv5_1));
}

void mnist_fp8::thread_args22_V_fu_6888_p2() {
    args22_V_fu_6888_p2 = (!p_47_reg_1553.read().is_01() || !ap_const_lv4_1.is_01())? sc_lv<4>(): (sc_biguint<4>(p_47_reg_1553.read()) + sc_biguint<4>(ap_const_lv4_1));
}

void mnist_fp8::thread_args23_V_fu_9026_p2() {
    args23_V_fu_9026_p2 = (!p_60_reg_1751.read().is_01() || !ap_const_lv4_1.is_01())? sc_lv<4>(): (sc_biguint<4>(p_60_reg_1751.read()) + sc_biguint<4>(ap_const_lv4_1));
}

void mnist_fp8::thread_args24_V_fu_9560_p2() {
    args24_V_fu_9560_p2 = (!p_73_reg_1938.read().is_01() || !ap_const_lv3_1.is_01())? sc_lv<3>(): (sc_biguint<3>(p_73_reg_1938.read()) + sc_biguint<3>(ap_const_lv3_1));
}

void mnist_fp8::thread_args2_V_fu_2885_p2() {
    args2_V_fu_2885_p2 = (!p_16_reg_1076.read().is_01() || !ap_const_lv5_1.is_01())? sc_lv<5>(): (sc_biguint<5>(p_16_reg_1076.read()) + sc_biguint<5>(ap_const_lv5_1));
}

void mnist_fp8::thread_brmerge_fu_2174_p2() {
    brmerge_fu_2174_p2 = (sof_1_fu_464.read() | ap_phi_mux_eol_phi_fu_843_p4.read());
}

void mnist_fp8::thread_c1_V_fu_9368_p2() {
    c1_V_fu_9368_p2 = (!p_54_reg_1762.read().is_01() || !ap_const_lv4_1.is_01())? sc_lv<4>(): (sc_biguint<4>(p_54_reg_1762.read()) + sc_biguint<4>(ap_const_lv4_1));
}

void mnist_fp8::thread_c_V_fu_5501_p2() {
    c_V_fu_5501_p2 = (!p_22_reg_1288.read().is_01() || !ap_const_lv3_1.is_01())? sc_lv<3>(): (sc_biguint<3>(p_22_reg_1288.read()) + sc_biguint<3>(ap_const_lv3_1));
}

void mnist_fp8::thread_conv1_0_address0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state50.read())) {
        conv1_0_address0 =  (sc_lv<12>) (tmp_368_cast_fu_2900_p1.read());
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state36.read())) {
        conv1_0_address0 =  (sc_lv<12>) (tmp_369_cast_fu_2727_p1.read());
    } else {
        conv1_0_address0 = "XXXXXXXXXXXX";
    }
}

void mnist_fp8::thread_conv1_0_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state36.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state50.read()))) {
        conv1_0_ce0 = ap_const_logic_1;
    } else {
        conv1_0_ce0 = ap_const_logic_0;
    }
}

void mnist_fp8::thread_conv1_0_load_to_int_fu_2905_p1() {
    conv1_0_load_to_int_fu_2905_p1 = conv1_0_load_reg_10020.read();
}

void mnist_fp8::thread_conv1_0_we0() {
    if ((esl_seteq<1,1,1>(exitcond13_fu_2645_p2.read(), ap_const_lv1_1) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state36.read()))) {
        conv1_0_we0 = ap_const_logic_1;
    } else {
        conv1_0_we0 = ap_const_logic_0;
    }
}

void mnist_fp8::thread_conv2_0_address0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state113.read())) {
        conv2_0_address0 =  (sc_lv<12>) (tmp_429_cast_fu_5174_p1.read());
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state110.read())) {
        conv2_0_address0 =  (sc_lv<12>) (tmp_430_cast_fu_5052_p1.read());
    } else {
        conv2_0_address0 = "XXXXXXXXXXXX";
    }
}

void mnist_fp8::thread_conv2_0_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state113.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state110.read()))) {
        conv2_0_ce0 = ap_const_logic_1;
    } else {
        conv2_0_ce0 = ap_const_logic_0;
    }
}

void mnist_fp8::thread_conv2_0_d0() {
    conv2_0_d0 = (!tmp_63_reg_10525.read()[0].is_01())? sc_lv<32>(): ((tmp_63_reg_10525.read()[0].to_bool())? ap_const_lv32_0: f_2_fu_5040_p1.read());
}

void mnist_fp8::thread_conv2_0_load_to_int_fu_5179_p1() {
    conv2_0_load_to_int_fu_5179_p1 = conv2_0_load_reg_10620.read();
}

void mnist_fp8::thread_conv2_0_we0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state110.read())) {
        conv2_0_we0 = ap_const_logic_1;
    } else {
        conv2_0_we0 = ap_const_logic_0;
    }
}

void mnist_fp8::thread_conv3_0_address0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state176.read())) {
        conv3_0_address0 =  (sc_lv<11>) (tmp_506_cast_fu_6903_p1.read());
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state161.read())) {
        conv3_0_address0 =  (sc_lv<11>) (tmp_507_cast_fu_6677_p1.read());
    } else {
        conv3_0_address0 =  (sc_lv<11>) ("XXXXXXXXXXX");
    }
}

void mnist_fp8::thread_conv3_0_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state161.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state176.read()))) {
        conv3_0_ce0 = ap_const_logic_1;
    } else {
        conv3_0_ce0 = ap_const_logic_0;
    }
}

void mnist_fp8::thread_conv3_0_load_to_int_fu_6908_p1() {
    conv3_0_load_to_int_fu_6908_p1 = conv3_0_load_reg_11127.read();
}

void mnist_fp8::thread_conv3_0_we0() {
    if ((esl_seteq<1,1,1>(exitcond41_fu_6625_p2.read(), ap_const_lv1_1) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state161.read()))) {
        conv3_0_we0 = ap_const_logic_1;
    } else {
        conv3_0_we0 = ap_const_logic_0;
    }
}

void mnist_fp8::thread_conv4_0_address0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state236.read())) {
        conv4_0_address0 =  (sc_lv<11>) (tmp_558_cast_fu_9041_p1.read());
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state233.read())) {
        conv4_0_address0 =  (sc_lv<11>) (tmp_559_cast_fu_8919_p1.read());
    } else {
        conv4_0_address0 =  (sc_lv<11>) ("XXXXXXXXXXX");
    }
}

void mnist_fp8::thread_conv4_0_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state236.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state233.read()))) {
        conv4_0_ce0 = ap_const_logic_1;
    } else {
        conv4_0_ce0 = ap_const_logic_0;
    }
}

void mnist_fp8::thread_conv4_0_d0() {
    conv4_0_d0 = (!tmp_193_reg_11618.read()[0].is_01())? sc_lv<32>(): ((tmp_193_reg_11618.read()[0].to_bool())? ap_const_lv32_0: f_6_fu_8907_p1.read());
}

void mnist_fp8::thread_conv4_0_load_to_int_fu_9046_p1() {
    conv4_0_load_to_int_fu_9046_p1 = conv4_0_load_reg_11713.read();
}

void mnist_fp8::thread_conv4_0_we0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state233.read())) {
        conv4_0_we0 = ap_const_logic_1;
    } else {
        conv4_0_we0 = ap_const_logic_0;
    }
}

void mnist_fp8::thread_exitcond11_fu_3989_p2() {
    exitcond11_fu_3989_p2 = (!p_10_reg_1162.read().is_01() || !ap_const_lv5_1C.is_01())? sc_lv<1>(): sc_lv<1>(p_10_reg_1162.read() == ap_const_lv5_1C);
}

void mnist_fp8::thread_exitcond12_fu_2879_p2() {
    exitcond12_fu_2879_p2 = (!p_16_reg_1076.read().is_01() || !ap_const_lv5_1C.is_01())? sc_lv<1>(): sc_lv<1>(p_16_reg_1076.read() == ap_const_lv5_1C);
}

void mnist_fp8::thread_exitcond13_fu_2645_p2() {
    exitcond13_fu_2645_p2 = (!p_12_reg_1007.read().is_01() || !ap_const_lv2_3.is_01())? sc_lv<1>(): sc_lv<1>(p_12_reg_1007.read() == ap_const_lv2_3);
}

void mnist_fp8::thread_exitcond14_fu_6068_p2() {
    exitcond14_fu_6068_p2 = (!p_13_reg_1368.read().is_01() || !ap_const_lv3_4.is_01())? sc_lv<1>(): sc_lv<1>(p_13_reg_1368.read() == ap_const_lv3_4);
}

void mnist_fp8::thread_exitcond15_fu_3361_p2() {
    exitcond15_fu_3361_p2 = (!p_14_reg_1121.read().is_01() || !ap_const_lv5_1E.is_01())? sc_lv<1>(): sc_lv<1>(p_14_reg_1121.read() == ap_const_lv5_1E);
}

void mnist_fp8::thread_exitcond16_fu_6504_p2() {
    exitcond16_fu_6504_p2 = (!p_15_reg_1425.read().is_01() || !ap_const_lv4_8.is_01())? sc_lv<1>(): sc_lv<1>(p_15_reg_1425.read() == ap_const_lv4_8);
}

void mnist_fp8::thread_exitcond17_fu_5056_p2() {
    exitcond17_fu_5056_p2 = (!p_19_reg_1255.read().is_01() || !ap_const_lv3_4.is_01())? sc_lv<1>(): sc_lv<1>(p_19_reg_1255.read() == ap_const_lv3_4);
}

void mnist_fp8::thread_exitcond18_fu_2732_p2() {
    exitcond18_fu_2732_p2 = (!p_17_reg_1031.read().is_01() || !ap_const_lv2_3.is_01())? sc_lv<1>(): sc_lv<1>(p_17_reg_1031.read() == ap_const_lv2_3);
}

void mnist_fp8::thread_exitcond19_fu_4040_p2() {
    exitcond19_fu_4040_p2 = (!p_18_reg_1174.read().is_01() || !ap_const_lv5_1C.is_01())? sc_lv<1>(): sc_lv<1>(p_18_reg_1174.read() == ap_const_lv5_1C);
}

void mnist_fp8::thread_exitcond1_fu_2117_p2() {
    exitcond1_fu_2117_p2 = (!p_0_reg_816.read().is_01() || !ap_const_lv5_1C.is_01())? sc_lv<1>(): sc_lv<1>(p_0_reg_816.read() == ap_const_lv5_1C);
}

void mnist_fp8::thread_exitcond20_fu_5495_p2() {
    exitcond20_fu_5495_p2 = (!p_22_reg_1288.read().is_01() || !ap_const_lv3_4.is_01())? sc_lv<1>(): sc_lv<1>(p_22_reg_1288.read() == ap_const_lv3_4);
}

void mnist_fp8::thread_exitcond21_fu_6092_p2() {
    exitcond21_fu_6092_p2 = (!p_20_reg_1390.read().is_01() || !ap_const_lv5_10.is_01())? sc_lv<1>(): sc_lv<1>(p_20_reg_1390.read() == ap_const_lv5_10);
}

void mnist_fp8::thread_exitcond22_fu_7234_p2() {
    exitcond22_fu_7234_p2 = (!p_21_reg_1564.read().is_01() || !ap_const_lv4_8.is_01())? sc_lv<1>(): sc_lv<1>(p_21_reg_1564.read() == ap_const_lv4_8);
}

void mnist_fp8::thread_exitcond23_fu_5102_p2() {
    exitcond23_fu_5102_p2 = (!p_23_reg_1266.read().is_01() || !ap_const_lv5_1C.is_01())? sc_lv<1>(): sc_lv<1>(p_23_reg_1266.read() == ap_const_lv5_1C);
}

void mnist_fp8::thread_exitcond24_fu_5575_p2() {
    exitcond24_fu_5575_p2 = (!p_27_reg_1299.read().is_01() || !ap_const_lv4_E.is_01())? sc_lv<1>(): sc_lv<1>(p_27_reg_1299.read() == ap_const_lv4_E);
}

void mnist_fp8::thread_exitcond25_fu_4052_p2() {
    exitcond25_fu_4052_p2 = (!p_24_reg_1198.read().is_01() || !ap_const_lv3_4.is_01())? sc_lv<1>(): sc_lv<1>(p_24_reg_1198.read() == ap_const_lv3_4);
}

void mnist_fp8::thread_exitcond26_fu_6562_p2() {
    exitcond26_fu_6562_p2 = (!p_25_reg_1436.read().is_01() || !ap_const_lv4_E.is_01())? sc_lv<1>(): sc_lv<1>(p_25_reg_1436.read() == ap_const_lv4_E);
}

void mnist_fp8::thread_exitcond27_fu_7871_p2() {
    exitcond27_fu_7871_p2 = (!p_26_reg_1624.read().is_01() || !ap_const_lv4_8.is_01())? sc_lv<1>(): sc_lv<1>(p_26_reg_1624.read() == ap_const_lv4_8);
}

void mnist_fp8::thread_exitcond28_fu_5153_p2() {
    exitcond28_fu_5153_p2 = (!p_28_reg_1277.read().is_01() || !ap_const_lv5_1C.is_01())? sc_lv<1>(): sc_lv<1>(p_28_reg_1277.read() == ap_const_lv5_1C);
}

void mnist_fp8::thread_exitcond29_fu_5634_p2() {
    exitcond29_fu_5634_p2 = (!p_33_reg_1310.read().is_01() || !ap_const_lv4_E.is_01())? sc_lv<1>(): sc_lv<1>(p_33_reg_1310.read() == ap_const_lv4_E);
}

void mnist_fp8::thread_exitcond30_fu_4135_p2() {
    exitcond30_fu_4135_p2 = (!p_29_reg_1221.read().is_01() || !ap_const_lv2_3.is_01())? sc_lv<1>(): sc_lv<1>(p_29_reg_1221.read() == ap_const_lv2_3);
}

void mnist_fp8::thread_exitcond31_fu_6180_p2() {
    exitcond31_fu_6180_p2 = (!p_30_reg_1401.read().is_01() || !ap_const_lv5_10.is_01())? sc_lv<1>(): sc_lv<1>(p_30_reg_1401.read() == ap_const_lv5_10);
}

void mnist_fp8::thread_exitcond32_fu_7258_p2() {
    exitcond32_fu_7258_p2 = (!p_31_reg_1586.read().is_01() || !ap_const_lv5_10.is_01())? sc_lv<1>(): sc_lv<1>(p_31_reg_1586.read() == ap_const_lv5_10);
}

void mnist_fp8::thread_exitcond33_fu_6613_p2() {
    exitcond33_fu_6613_p2 = (!p_32_reg_1448.read().is_01() || !ap_const_lv4_E.is_01())? sc_lv<1>(): sc_lv<1>(p_32_reg_1448.read() == ap_const_lv4_E);
}

void mnist_fp8::thread_exitcond34_fu_6785_p2() {
    exitcond34_fu_6785_p2 = (!p_36_reg_1531.read().is_01() || !ap_const_lv4_8.is_01())? sc_lv<1>(): sc_lv<1>(p_36_reg_1531.read() == ap_const_lv4_8);
}

void mnist_fp8::thread_exitcond35_fu_4227_p2() {
    exitcond35_fu_4227_p2 = (!p_34_reg_1244.read().is_01() || !ap_const_lv2_3.is_01())? sc_lv<1>(): sc_lv<1>(p_34_reg_1244.read() == ap_const_lv2_3);
}

void mnist_fp8::thread_exitcond36_fu_7929_p2() {
    exitcond36_fu_7929_p2 = (!p_35_reg_1635.read().is_01() || !ap_const_lv4_E.is_01())? sc_lv<1>(): sc_lv<1>(p_35_reg_1635.read() == ap_const_lv4_E);
}

void mnist_fp8::thread_exitcond37_fu_5668_p2() {
    exitcond37_fu_5668_p2 = (!p_38_reg_1334.read().is_01() || !ap_const_lv2_2.is_01())? sc_lv<1>(): sc_lv<1>(p_38_reg_1334.read() == ap_const_lv2_2);
}

void mnist_fp8::thread_exitcond38_fu_9422_p2() {
    exitcond38_fu_9422_p2 = (!p_37_reg_1817.read().is_01() || !ap_const_lv4_8.is_01())? sc_lv<1>(): sc_lv<1>(p_37_reg_1817.read() == ap_const_lv4_8);
}

void mnist_fp8::thread_exitcond39_fu_6831_p2() {
    exitcond39_fu_6831_p2 = (!p_41_reg_1542.read().is_01() || !ap_const_lv4_E.is_01())? sc_lv<1>(): sc_lv<1>(p_41_reg_1542.read() == ap_const_lv4_E);
}

void mnist_fp8::thread_exitcond3_fu_2518_p2() {
    exitcond3_fu_2518_p2 = (!p_2_reg_972.read().is_01() || !ap_const_lv3_4.is_01())? sc_lv<1>(): sc_lv<1>(p_2_reg_972.read() == ap_const_lv3_4);
}

void mnist_fp8::thread_exitcond40_fu_7346_p2() {
    exitcond40_fu_7346_p2 = (!p_39_reg_1597.read().is_01() || !ap_const_lv5_10.is_01())? sc_lv<1>(): sc_lv<1>(p_39_reg_1597.read() == ap_const_lv5_10);
}

void mnist_fp8::thread_exitcond41_fu_6625_p2() {
    exitcond41_fu_6625_p2 = (!p_40_reg_1460.read().is_01() || !ap_const_lv3_4.is_01())? sc_lv<1>(): sc_lv<1>(p_40_reg_1460.read() == ap_const_lv3_4);
}

void mnist_fp8::thread_exitcond42_fu_5728_p2() {
    exitcond42_fu_5728_p2 = (!p_43_reg_1357.read().is_01() || !ap_const_lv2_2.is_01())? sc_lv<1>(): sc_lv<1>(p_43_reg_1357.read() == ap_const_lv2_2);
}

void mnist_fp8::thread_exitcond43_fu_9458_p2() {
    exitcond43_fu_9458_p2 = (!p_42_reg_1850.read().is_01() || !ap_const_lv5_10.is_01())? sc_lv<1>(): sc_lv<1>(p_42_reg_1850.read() == ap_const_lv5_10);
}

void mnist_fp8::thread_exitcond44_fu_6882_p2() {
    exitcond44_fu_6882_p2 = (!p_47_reg_1553.read().is_01() || !ap_const_lv4_E.is_01())? sc_lv<1>(): sc_lv<1>(p_47_reg_1553.read() == ap_const_lv4_E);
}

void mnist_fp8::thread_exitcond45_fu_7980_p2() {
    exitcond45_fu_7980_p2 = (!p_44_reg_1647.read().is_01() || !ap_const_lv4_E.is_01())? sc_lv<1>(): sc_lv<1>(p_44_reg_1647.read() == ap_const_lv4_E);
}

void mnist_fp8::thread_exitcond46_fu_9434_p2() {
    exitcond46_fu_9434_p2 = (!p_45_reg_1828.read().is_01() || !ap_const_lv4_9.is_01())? sc_lv<1>(): sc_lv<1>(p_45_reg_1828.read() == ap_const_lv4_9);
}

void mnist_fp8::thread_exitcond47_fu_6682_p2() {
    exitcond47_fu_6682_p2 = (!p_46_reg_1485.read().is_01() || !ap_const_lv2_3.is_01())? sc_lv<1>(): sc_lv<1>(p_46_reg_1485.read() == ap_const_lv2_3);
}

void mnist_fp8::thread_exitcond48_fu_8923_p2() {
    exitcond48_fu_8923_p2 = (!p_50_reg_1729.read().is_01() || !ap_const_lv4_8.is_01())? sc_lv<1>(): sc_lv<1>(p_50_reg_1729.read() == ap_const_lv4_8);
}

void mnist_fp8::thread_exitcond49_fu_9566_p2() {
    exitcond49_fu_9566_p2 = (!p_48_reg_1949.read().is_01() || !ap_const_lv5_10.is_01())? sc_lv<1>(): sc_lv<1>(p_48_reg_1949.read() == ap_const_lv5_10);
}

void mnist_fp8::thread_exitcond4_fu_2264_p2() {
    exitcond4_fu_2264_p2 = (!p_3_reg_945.read().is_01() || !ap_const_lv5_1E.is_01())? sc_lv<1>(): sc_lv<1>(p_3_reg_945.read() == ap_const_lv5_1E);
}

void mnist_fp8::thread_exitcond50_fu_9470_p2() {
    exitcond50_fu_9470_p2 = (!p_49_reg_1861.read().is_01() || !ap_const_lv3_7.is_01())? sc_lv<1>(): sc_lv<1>(p_49_reg_1861.read() == ap_const_lv3_7);
}

void mnist_fp8::thread_exitcond51_fu_9362_p2() {
    exitcond51_fu_9362_p2 = (!p_54_reg_1762.read().is_01() || !ap_const_lv4_8.is_01())? sc_lv<1>(): sc_lv<1>(p_54_reg_1762.read() == ap_const_lv4_8);
}

void mnist_fp8::thread_exitcond52_fu_7992_p2() {
    exitcond52_fu_7992_p2 = (!p_51_reg_1671.read().is_01() || !ap_const_lv4_8.is_01())? sc_lv<1>(): sc_lv<1>(p_51_reg_1671.read() == ap_const_lv4_8);
}

void mnist_fp8::thread_exitcond53_fu_6735_p2() {
    exitcond53_fu_6735_p2 = (!p_52_reg_1508.read().is_01() || !ap_const_lv2_3.is_01())? sc_lv<1>(): sc_lv<1>(p_52_reg_1508.read() == ap_const_lv2_3);
}

void mnist_fp8::thread_exitcond54_fu_9602_p2() {
    exitcond54_fu_9602_p2 = (!p_53_reg_1982.read().is_01() || !ap_const_lv5_10.is_01())? sc_lv<1>(): sc_lv<1>(p_53_reg_1982.read() == ap_const_lv5_10);
}

void mnist_fp8::thread_exitcond55_fu_8969_p2() {
    exitcond55_fu_8969_p2 = (!p_55_reg_1740.read().is_01() || !ap_const_lv4_E.is_01())? sc_lv<1>(): sc_lv<1>(p_55_reg_1740.read() == ap_const_lv4_E);
}

void mnist_fp8::thread_exitcond56_fu_9374_p2() {
    exitcond56_fu_9374_p2 = (!p_59_reg_1773.read().is_01() || !ap_const_lv3_7.is_01())? sc_lv<1>(): sc_lv<1>(p_59_reg_1773.read() == ap_const_lv3_7);
}

void mnist_fp8::thread_exitcond57_fu_9446_p2() {
    exitcond57_fu_9446_p2 = (!p_56_reg_1839.read().is_01() || !ap_const_lv4_9.is_01())? sc_lv<1>(): sc_lv<1>(p_56_reg_1839.read() == ap_const_lv4_9);
}

void mnist_fp8::thread_exitcond58_fu_9578_p2() {
    exitcond58_fu_9578_p2 = (!p_57_reg_1960.read().is_01() || !ap_const_lv4_9.is_01())? sc_lv<1>(): sc_lv<1>(p_57_reg_1960.read() == ap_const_lv4_9);
}

void mnist_fp8::thread_exitcond59_fu_9482_p2() {
    exitcond59_fu_9482_p2 = (!p_58_reg_1872.read().is_01() || !ap_const_lv3_7.is_01())? sc_lv<1>(): sc_lv<1>(p_58_reg_1872.read() == ap_const_lv3_7);
}

void mnist_fp8::thread_exitcond5_fu_3939_p2() {
    exitcond5_fu_3939_p2 = (!p_9_reg_1151.read().is_01() || !ap_const_lv3_4.is_01())? sc_lv<1>(): sc_lv<1>(p_9_reg_1151.read() == ap_const_lv3_4);
}

void mnist_fp8::thread_exitcond60_fu_9020_p2() {
    exitcond60_fu_9020_p2 = (!p_60_reg_1751.read().is_01() || !ap_const_lv4_E.is_01())? sc_lv<1>(): sc_lv<1>(p_60_reg_1751.read() == ap_const_lv4_E);
}

void mnist_fp8::thread_exitcond61_fu_9386_p2() {
    exitcond61_fu_9386_p2 = (!p_65_reg_1784.read().is_01() || !ap_const_lv3_7.is_01())? sc_lv<1>(): sc_lv<1>(p_65_reg_1784.read() == ap_const_lv3_7);
}

void mnist_fp8::thread_exitcond62_fu_8041_p2() {
    exitcond62_fu_8041_p2 = (!p_61_reg_1695.read().is_01() || !ap_const_lv2_3.is_01())? sc_lv<1>(): sc_lv<1>(p_61_reg_1695.read() == ap_const_lv2_3);
}

void mnist_fp8::thread_exitcond63_fu_9614_p2() {
    exitcond63_fu_9614_p2 = (!p_62_reg_1993.read().is_01() || !ap_const_lv3_7.is_01())? sc_lv<1>(): sc_lv<1>(p_62_reg_1993.read() == ap_const_lv3_7);
}

void mnist_fp8::thread_exitcond64_fu_9590_p2() {
    exitcond64_fu_9590_p2 = (!p_63_reg_1971.read().is_01() || !ap_const_lv4_9.is_01())? sc_lv<1>(): sc_lv<1>(p_63_reg_1971.read() == ap_const_lv4_9);
}

void mnist_fp8::thread_exitcond65_fu_9494_p2() {
    exitcond65_fu_9494_p2 = (!p_64_reg_1883.read().is_01() || !ap_const_lv4_8.is_01())? sc_lv<1>(): sc_lv<1>(p_64_reg_1883.read() == ap_const_lv4_8);
}

void mnist_fp8::thread_exitcond66_fu_9530_p2() {
    exitcond66_fu_9530_p2 = (!p_68_reg_1916.read().is_01() || !ap_const_lv5_10.is_01())? sc_lv<1>(): sc_lv<1>(p_68_reg_1916.read() == ap_const_lv5_10);
}

void mnist_fp8::thread_exitcond67_fu_8094_p2() {
    exitcond67_fu_8094_p2 = (!p_66_reg_1718.read().is_01() || !ap_const_lv2_3.is_01())? sc_lv<1>(): sc_lv<1>(p_66_reg_1718.read() == ap_const_lv2_3);
}

void mnist_fp8::thread_exitcond68_fu_9626_p2() {
    exitcond68_fu_9626_p2 = (!p_67_reg_2004.read().is_01() || !ap_const_lv3_7.is_01())? sc_lv<1>(): sc_lv<1>(p_67_reg_2004.read() == ap_const_lv3_7);
}

void mnist_fp8::thread_exitcond69_fu_9398_p2() {
    exitcond69_fu_9398_p2 = (!p_69_reg_1795.read().is_01() || !ap_const_lv2_2.is_01())? sc_lv<1>(): sc_lv<1>(p_69_reg_1795.read() == ap_const_lv2_2);
}

void mnist_fp8::thread_exitcond6_fu_2582_p2() {
    exitcond6_fu_2582_p2 = (!p_5_reg_983.read().is_01() || !ap_const_lv5_1C.is_01())? sc_lv<1>(): sc_lv<1>(p_5_reg_983.read() == ap_const_lv5_1C);
}

void mnist_fp8::thread_exitcond70_fu_9542_p2() {
    exitcond70_fu_9542_p2 = (!p_70_reg_1927.read().is_01() || !ap_const_lv3_7.is_01())? sc_lv<1>(): sc_lv<1>(p_70_reg_1927.read() == ap_const_lv3_7);
}

void mnist_fp8::thread_exitcond71_fu_9410_p2() {
    exitcond71_fu_9410_p2 = (!p_72_reg_1806.read().is_01() || !ap_const_lv2_2.is_01())? sc_lv<1>(): sc_lv<1>(p_72_reg_1806.read() == ap_const_lv2_2);
}

void mnist_fp8::thread_exitcond72_fu_9506_p2() {
    exitcond72_fu_9506_p2 = (!p_71_reg_1894.read().is_01() || !ap_const_lv2_3.is_01())? sc_lv<1>(): sc_lv<1>(p_71_reg_1894.read() == ap_const_lv2_3);
}

void mnist_fp8::thread_exitcond73_fu_9638_p2() {
    exitcond73_fu_9638_p2 = (!p_74_reg_2015.read().is_01() || !ap_const_lv5_10.is_01())? sc_lv<1>(): sc_lv<1>(p_74_reg_2015.read() == ap_const_lv5_10);
}

void mnist_fp8::thread_exitcond74_fu_9554_p2() {
    exitcond74_fu_9554_p2 = (!p_73_reg_1938.read().is_01() || !ap_const_lv3_7.is_01())? sc_lv<1>(): sc_lv<1>(p_73_reg_1938.read() == ap_const_lv3_7);
}

void mnist_fp8::thread_exitcond75_fu_9518_p2() {
    exitcond75_fu_9518_p2 = (!p_75_reg_1905.read().is_01() || !ap_const_lv2_3.is_01())? sc_lv<1>(): sc_lv<1>(p_75_reg_1905.read() == ap_const_lv2_3);
}

void mnist_fp8::thread_exitcond76_fu_9650_p2() {
    exitcond76_fu_9650_p2 = (!p_76_reg_2026.read().is_01() || !ap_const_lv2_3.is_01())? sc_lv<1>(): sc_lv<1>(p_76_reg_2026.read() == ap_const_lv2_3);
}

void mnist_fp8::thread_exitcond77_fu_9662_p2() {
    exitcond77_fu_9662_p2 = (!p_77_reg_2037.read().is_01() || !ap_const_lv2_3.is_01())? sc_lv<1>(): sc_lv<1>(p_77_reg_2037.read() == ap_const_lv2_3);
}

void mnist_fp8::thread_exitcond7_fu_3231_p2() {
    exitcond7_fu_3231_p2 = (!p_6_reg_1087.read().is_01() || !ap_const_lv3_4.is_01())? sc_lv<1>(): sc_lv<1>(p_6_reg_1087.read() == ap_const_lv3_4);
}

void mnist_fp8::thread_exitcond8_fu_2782_p2() {
    exitcond8_fu_2782_p2 = (!p_7_reg_1054.read().is_01() || !ap_const_lv3_4.is_01())? sc_lv<1>(): sc_lv<1>(p_7_reg_1054.read() == ap_const_lv3_4);
}

void mnist_fp8::thread_exitcond9_fu_2633_p2() {
    exitcond9_fu_2633_p2 = (!p_8_reg_995.read().is_01() || !ap_const_lv5_1C.is_01())? sc_lv<1>(): sc_lv<1>(p_8_reg_995.read() == ap_const_lv5_1C);
}

void mnist_fp8::thread_exitcond_fu_2828_p2() {
    exitcond_fu_2828_p2 = (!p_11_reg_1065.read().is_01() || !ap_const_lv5_1C.is_01())? sc_lv<1>(): sc_lv<1>(p_11_reg_1065.read() == ap_const_lv5_1C);
}

void mnist_fp8::thread_f_2_fu_5040_p1() {
    f_2_fu_5040_p1 = p_Result_40_fu_5029_p5.read();
}

void mnist_fp8::thread_f_4_fu_5957_p1() {
    f_4_fu_5957_p1 = p_Result_17_fu_5946_p5.read();
}

void mnist_fp8::thread_f_5_fu_7852_p1() {
    f_5_fu_7852_p1 = p_Result_43_fu_7841_p5.read();
}

void mnist_fp8::thread_f_6_fu_8907_p1() {
    f_6_fu_8907_p1 = p_Result_48_fu_8896_p5.read();
}

void mnist_fp8::thread_f_fu_3922_p1() {
    f_fu_3922_p1 = p_Result_35_fu_3911_p5.read();
}

void mnist_fp8::thread_ff1_V_fu_3945_p2() {
    ff1_V_fu_3945_p2 = (!p_9_reg_1151.read().is_01() || !ap_const_lv3_1.is_01())? sc_lv<3>(): (sc_biguint<3>(p_9_reg_1151.read()) + sc_biguint<3>(ap_const_lv3_1));
}

void mnist_fp8::thread_ff2_V_fu_6510_p2() {
    ff2_V_fu_6510_p2 = (!p_15_reg_1425.read().is_01() || !ap_const_lv4_1.is_01())? sc_lv<4>(): (sc_biguint<4>(p_15_reg_1425.read()) + sc_biguint<4>(ap_const_lv4_1));
}

void mnist_fp8::thread_ff3_V_fu_7877_p2() {
    ff3_V_fu_7877_p2 = (!p_26_reg_1624.read().is_01() || !ap_const_lv4_1.is_01())? sc_lv<4>(): (sc_biguint<4>(p_26_reg_1624.read()) + sc_biguint<4>(ap_const_lv4_1));
}

void mnist_fp8::thread_ff4_V_fu_9464_p2() {
    ff4_V_fu_9464_p2 = (!p_42_reg_1850.read().is_01() || !ap_const_lv5_1.is_01())? sc_lv<5>(): (sc_biguint<5>(p_42_reg_1850.read()) + sc_biguint<5>(ap_const_lv5_1));
}

void mnist_fp8::thread_ff5_V_fu_9608_p2() {
    ff5_V_fu_9608_p2 = (!p_53_reg_1982.read().is_01() || !ap_const_lv5_1.is_01())? sc_lv<5>(): (sc_biguint<5>(p_53_reg_1982.read()) + sc_biguint<5>(ap_const_lv5_1));
}

void mnist_fp8::thread_ff_V_fu_2524_p2() {
    ff_V_fu_2524_p2 = (!p_2_reg_972.read().is_01() || !ap_const_lv3_1.is_01())? sc_lv<3>(): (sc_biguint<3>(p_2_reg_972.read()) + sc_biguint<3>(ap_const_lv3_1));
}

void mnist_fp8::thread_grp_fu_2048_p1() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state169.read())) {
        grp_fu_2048_p1 = reducer135_2_reg_1519.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state43.read())) {
        grp_fu_2048_p1 = reducer132_1_reg_1042.read();
    } else {
        grp_fu_2048_p1 =  (sc_lv<32>) ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
    }
}

void mnist_fp8::thread_grp_fu_2054_p0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state165.read())) {
        grp_fu_2054_p0 = pad_temp2_0_load_reg_11073.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state39.read())) {
        grp_fu_2054_p0 = pad_temp_0_0_load_reg_9966.read();
    } else {
        grp_fu_2054_p0 =  (sc_lv<32>) ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
    }
}

void mnist_fp8::thread_grp_fu_2054_p1() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state165.read())) {
        grp_fu_2054_p1 = w_conv3_load_reg_11078.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state39.read())) {
        grp_fu_2054_p1 = w_conv1_load_reg_9971.read();
    } else {
        grp_fu_2054_p1 =  (sc_lv<32>) ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
    }
}

void mnist_fp8::thread_grp_fu_2058_p0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state227.read())) {
        grp_fu_2058_p0 = tmp32_V_24_reg_11649.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state207.read())) {
        grp_fu_2058_p0 = tmp32_V_21_reg_11373.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state128.read())) {
        grp_fu_2058_p0 = p_012_0_i2_reg_10826.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state104.read())) {
        grp_fu_2058_p0 = tmp32_V_18_reg_10556.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state83.read())) {
        grp_fu_2058_p0 = tmp32_V_12_reg_10265.read();
    } else {
        grp_fu_2058_p0 =  (sc_lv<32>) ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
    }
}

void mnist_fp8::thread_grp_fu_2061_p0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state239.read())) {
        grp_fu_2061_p0 = conv4_0_load_reg_11713.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state221.read())) {
        grp_fu_2061_p0 = pad_temp3_0_load_reg_11481.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state179.read())) {
        grp_fu_2061_p0 = conv3_0_load_reg_11127.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state116.read())) {
        grp_fu_2061_p0 = conv2_0_load_reg_10620.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state98.read())) {
        grp_fu_2061_p0 = pad_temp1_0_load_reg_10388.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state53.read())) {
        grp_fu_2061_p0 = conv1_0_load_reg_10020.read();
    } else {
        grp_fu_2061_p0 =  (sc_lv<32>) ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
    }
}

void mnist_fp8::thread_grp_fu_2064_p0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state221.read())) {
        grp_fu_2064_p0 = w_conv4_load_reg_11486.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state98.read())) {
        grp_fu_2064_p0 = w_conv2_load_reg_10393.read();
    } else {
        grp_fu_2064_p0 =  (sc_lv<32>) ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
    }
}

void mnist_fp8::thread_grp_fu_2067_p0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state238.read())) {
        grp_fu_2067_p0 = conv4_0_load_reg_11713.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state178.read())) {
        grp_fu_2067_p0 = conv3_0_load_reg_11127.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state135.read())) {
        grp_fu_2067_p0 = p_03_i1_reg_10841.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state115.read())) {
        grp_fu_2067_p0 = conv2_0_load_reg_10620.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state52.read())) {
        grp_fu_2067_p0 = conv1_0_load_reg_10020.read();
    } else {
        grp_fu_2067_p0 =  (sc_lv<32>) ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
    }
}

void mnist_fp8::thread_grp_fu_2067_p1() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state135.read())) {
        grp_fu_2067_p1 = tmp_140_reg_1345.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state52.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state115.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state178.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state238.read()))) {
        grp_fu_2067_p1 = ap_const_lv32_0;
    } else {
        grp_fu_2067_p1 =  (sc_lv<32>) ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
    }
}

void mnist_fp8::thread_grp_fu_2083_p0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state12.read())) {
        grp_fu_2083_p0 = p_s_reg_933.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state11.read())) {
        grp_fu_2083_p0 = ap_phi_mux_p_s_phi_fu_937_p4.read();
    } else {
        grp_fu_2083_p0 =  (sc_lv<5>) ("XXXXX");
    }
}

void mnist_fp8::thread_grp_fu_2083_p2() {
    grp_fu_2083_p2 = (!grp_fu_2083_p0.read().is_01() || !ap_const_lv5_1E.is_01())? sc_lv<1>(): sc_lv<1>(grp_fu_2083_p0.read() == ap_const_lv5_1E);
}

void mnist_fp8::thread_grp_fu_2090_p0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state59.read())) {
        grp_fu_2090_p0 = p_4_reg_1109.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state58.read())) {
        grp_fu_2090_p0 = ap_phi_mux_p_4_phi_fu_1113_p4.read();
    } else {
        grp_fu_2090_p0 =  (sc_lv<5>) ("XXXXX");
    }
}

void mnist_fp8::thread_grp_fu_2090_p2() {
    grp_fu_2090_p2 = (!grp_fu_2090_p0.read().is_01() || !ap_const_lv5_1E.is_01())? sc_lv<1>(): sc_lv<1>(grp_fu_2090_p0.read() == ap_const_lv5_1E);
}

void mnist_fp8::thread_grp_fu_2468_ap_start() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state14.read())) {
        grp_fu_2468_ap_start = ap_const_logic_1;
    } else {
        grp_fu_2468_ap_start = ap_const_logic_0;
    }
}

void mnist_fp8::thread_grp_fu_2468_p0() {
    grp_fu_2468_p0 =  (sc_lv<12>) (grp_fu_2468_p00.read());
}

void mnist_fp8::thread_grp_fu_2468_p00() {
    grp_fu_2468_p00 = (!tmp_68_reg_9850.read()[0].is_01())? sc_lv<11>(): ((tmp_68_reg_9850.read()[0].to_bool())? neg_ti_fu_2455_p2.read(): tmp_77_fu_2445_p1.read());
}

void mnist_fp8::thread_grp_fu_2468_p1() {
    grp_fu_2468_p1 =  (sc_lv<6>) (ap_const_lv11_1C);
}

void mnist_fp8::thread_grp_fu_3587_ap_start() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state61.read())) {
        grp_fu_3587_ap_start = ap_const_logic_1;
    } else {
        grp_fu_3587_ap_start = ap_const_logic_0;
    }
}

void mnist_fp8::thread_grp_fu_3587_p0() {
    grp_fu_3587_p0 =  (sc_lv<14>) (grp_fu_3587_p00.read());
}

void mnist_fp8::thread_grp_fu_3587_p00() {
    grp_fu_3587_p00 = (!tmp_256_reg_10175.read()[0].is_01())? sc_lv<13>(): ((tmp_256_reg_10175.read()[0].to_bool())? neg_ti1_fu_3574_p2.read(): tmp_263_fu_3564_p1.read());
}

void mnist_fp8::thread_grp_fu_3587_p1() {
    grp_fu_3587_p1 =  (sc_lv<6>) (ap_const_lv13_1C);
}

void mnist_fp8::thread_grp_fu_6351_ap_start() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state140.read())) {
        grp_fu_6351_ap_start = ap_const_logic_1;
    } else {
        grp_fu_6351_ap_start = ap_const_logic_0;
    }
}

void mnist_fp8::thread_grp_fu_6351_p0() {
    grp_fu_6351_p0 = (!tmp_405_reg_10926.read()[0].is_01())? sc_lv<11>(): ((tmp_405_reg_10926.read()[0].to_bool())? neg_ti3_fu_6338_p2.read(): tmp_409_fu_6328_p1.read());
}

void mnist_fp8::thread_grp_fu_6351_p1() {
    grp_fu_6351_p1 =  (sc_lv<5>) (ap_const_lv11_E);
}

void mnist_fp8::thread_grp_fu_7517_ap_start() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state187.read())) {
        grp_fu_7517_ap_start = ap_const_logic_1;
    } else {
        grp_fu_7517_ap_start = ap_const_logic_0;
    }
}

void mnist_fp8::thread_grp_fu_7517_p0() {
    grp_fu_7517_p0 = (!tmp_480_reg_11283.read()[0].is_01())? sc_lv<12>(): ((tmp_480_reg_11283.read()[0].to_bool())? neg_ti5_fu_7504_p2.read(): tmp_484_fu_7494_p1.read());
}

void mnist_fp8::thread_grp_fu_7517_p1() {
    grp_fu_7517_p1 =  (sc_lv<5>) (ap_const_lv12_E);
}

void mnist_fp8::thread_grp_fu_9698_p2() {
    grp_fu_9698_p2 = esl_concat<8,6>(p_Val2_21_reg_1232.read(), ap_const_lv6_0);
}

void mnist_fp8::thread_grp_fu_9739_p2() {
    grp_fu_9739_p2 = esl_concat<8,6>(p_Val2_37_reg_1706.read(), ap_const_lv6_0);
}

void mnist_fp8::thread_h1_V_fu_9380_p2() {
    h1_V_fu_9380_p2 = (!p_59_reg_1773.read().is_01() || !ap_const_lv3_1.is_01())? sc_lv<3>(): (sc_biguint<3>(p_59_reg_1773.read()) + sc_biguint<3>(ap_const_lv3_1));
}

void mnist_fp8::thread_h_V_fu_5581_p2() {
    h_V_fu_5581_p2 = (!p_27_reg_1299.read().is_01() || !ap_const_lv4_1.is_01())? sc_lv<4>(): (sc_biguint<4>(p_27_reg_1299.read()) + sc_biguint<4>(ap_const_lv4_1));
}

void mnist_fp8::thread_i1_V_fu_3367_p2() {
    i1_V_fu_3367_p2 = (!p_14_reg_1121.read().is_01() || !ap_const_lv5_1.is_01())? sc_lv<5>(): (sc_biguint<5>(p_14_reg_1121.read()) + sc_biguint<5>(ap_const_lv5_1));
}

void mnist_fp8::thread_i3_V_fu_6186_p2() {
    i3_V_fu_6186_p2 = (!p_30_reg_1401.read().is_01() || !ap_const_lv5_1.is_01())? sc_lv<5>(): (sc_biguint<5>(p_30_reg_1401.read()) + sc_biguint<5>(ap_const_lv5_1));
}

void mnist_fp8::thread_i4_V_fu_7352_p2() {
    i4_V_fu_7352_p2 = (!p_39_reg_1597.read().is_01() || !ap_const_lv5_1.is_01())? sc_lv<5>(): (sc_biguint<5>(p_39_reg_1597.read()) + sc_biguint<5>(ap_const_lv5_1));
}

void mnist_fp8::thread_i6_V_fu_9452_p2() {
    i6_V_fu_9452_p2 = (!p_56_reg_1839.read().is_01() || !ap_const_lv4_1.is_01())? sc_lv<4>(): (sc_biguint<4>(p_56_reg_1839.read()) + sc_biguint<4>(ap_const_lv4_1));
}

void mnist_fp8::thread_i7_V_fu_9596_p2() {
    i7_V_fu_9596_p2 = (!p_63_reg_1971.read().is_01() || !ap_const_lv4_1.is_01())? sc_lv<4>(): (sc_biguint<4>(p_63_reg_1971.read()) + sc_biguint<4>(ap_const_lv4_1));
}

void mnist_fp8::thread_i_V_1_fu_2270_p2() {
    i_V_1_fu_2270_p2 = (!p_3_reg_945.read().is_01() || !ap_const_lv5_1.is_01())? sc_lv<5>(): (sc_biguint<5>(p_3_reg_945.read()) + sc_biguint<5>(ap_const_lv5_1));
}

void mnist_fp8::thread_i_V_fu_2123_p2() {
    i_V_fu_2123_p2 = (!p_0_reg_816.read().is_01() || !ap_const_lv5_1.is_01())? sc_lv<5>(): (sc_biguint<5>(p_0_reg_816.read()) + sc_biguint<5>(ap_const_lv5_1));
}

void mnist_fp8::thread_icmp10_fu_8792_p2() {
    icmp10_fu_8792_p2 = (!tmp_529_fu_8782_p4.read().is_01() || !ap_const_lv26_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_529_fu_8782_p4.read() == ap_const_lv26_0);
}

void mnist_fp8::thread_icmp11_fu_8295_p2() {
    icmp11_fu_8295_p2 = (!tmp_547_fu_8285_p4.read().is_01() || !ap_const_lv9_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_547_fu_8285_p4.read() == ap_const_lv9_0);
}

void mnist_fp8::thread_icmp12_fu_8380_p2() {
    icmp12_fu_8380_p2 = (!tmp_554_fu_8370_p4.read().is_01() || !ap_const_lv9_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_554_fu_8370_p4.read() == ap_const_lv9_0);
}

void mnist_fp8::thread_icmp1_fu_3816_p2() {
    icmp1_fu_3816_p2 = (!tmp_306_fu_3806_p4.read().is_01() || !ap_const_lv26_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_306_fu_3806_p4.read() == ap_const_lv26_0);
}

void mnist_fp8::thread_icmp2_fu_5340_p2() {
    icmp2_fu_5340_p2 = (!tmp_330_fu_5330_p4.read().is_01() || !ap_const_lv9_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_330_fu_5330_p4.read() == ap_const_lv9_0);
}

void mnist_fp8::thread_icmp3_fu_4925_p2() {
    icmp3_fu_4925_p2 = (!tmp_346_fu_4915_p4.read().is_01() || !ap_const_lv26_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_346_fu_4915_p4.read() == ap_const_lv26_0);
}

void mnist_fp8::thread_icmp4_fu_4428_p2() {
    icmp4_fu_4428_p2 = (!tmp_392_fu_4418_p4.read().is_01() || !ap_const_lv9_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_392_fu_4418_p4.read() == ap_const_lv9_0);
}

void mnist_fp8::thread_icmp5_fu_4513_p2() {
    icmp5_fu_4513_p2 = (!tmp_399_fu_4503_p4.read().is_01() || !ap_const_lv9_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_399_fu_4503_p4.read() == ap_const_lv9_0);
}

void mnist_fp8::thread_icmp6_fu_5851_p2() {
    icmp6_fu_5851_p2 = (!tmp_437_fu_5841_p4.read().is_01() || !ap_const_lv26_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_437_fu_5841_p4.read() == ap_const_lv26_0);
}

void mnist_fp8::thread_icmp7_fu_7069_p2() {
    icmp7_fu_7069_p2 = (!tmp_458_fu_7059_p4.read().is_01() || !ap_const_lv9_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_458_fu_7059_p4.read() == ap_const_lv9_0);
}

void mnist_fp8::thread_icmp8_fu_7746_p2() {
    icmp8_fu_7746_p2 = (!tmp_509_fu_7736_p4.read().is_01() || !ap_const_lv26_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_509_fu_7736_p4.read() == ap_const_lv26_0);
}

void mnist_fp8::thread_icmp9_fu_9207_p2() {
    icmp9_fu_9207_p2 = (!tmp_523_fu_9197_p4.read().is_01() || !ap_const_lv9_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_523_fu_9197_p4.read() == ap_const_lv9_0);
}

void mnist_fp8::thread_icmp_fu_3066_p2() {
    icmp_fu_3066_p2 = (!tmp_149_fu_3056_p4.read().is_01() || !ap_const_lv9_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_149_fu_3056_p4.read() == ap_const_lv9_0);
}

void mnist_fp8::thread_image_0_0_address0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state30.read())) {
        image_0_0_address0 =  (sc_lv<10>) (tmp_282_cast_fu_2501_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage0.read()) && 
                esl_seteq<1,1,1>(ap_enable_reg_pp1_iter1.read(), ap_const_logic_1) && 
                esl_seteq<1,1,1>(ap_block_pp1_stage0.read(), ap_const_boolean_0))) {
        image_0_0_address0 =  (sc_lv<10>) (tmp_97_cast_fu_2199_p1.read());
    } else {
        image_0_0_address0 =  (sc_lv<10>) ("XXXXXXXXXX");
    }
}

void mnist_fp8::thread_image_0_0_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state30.read()) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage0.read()) && 
          esl_seteq<1,1,1>(ap_enable_reg_pp1_iter1.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_block_pp1_stage0_11001.read(), ap_const_boolean_0)))) {
        image_0_0_ce0 = ap_const_logic_1;
    } else {
        image_0_0_ce0 = ap_const_logic_0;
    }
}

void mnist_fp8::thread_image_0_0_d0() {
    image_0_0_d0 = ap_phi_mux_pixel_data_V_2_phi_fu_890_p4.read();
}

void mnist_fp8::thread_image_0_0_we0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage0.read()) && 
         esl_seteq<1,1,1>(ap_enable_reg_pp1_iter1.read(), ap_const_logic_1) && 
         esl_seteq<1,1,1>(ap_const_lv1_0, tmp_2_reg_9782.read()) && 
         esl_seteq<1,1,1>(ap_block_pp1_stage0_11001.read(), ap_const_boolean_0))) {
        image_0_0_we0 = ap_const_logic_1;
    } else {
        image_0_0_we0 = ap_const_logic_0;
    }
}

void mnist_fp8::thread_index_tuple1_V_fu_3277_p2() {
    index_tuple1_V_fu_3277_p2 = (!p_4_reg_1109.read().is_01() || !ap_const_lv5_1.is_01())? sc_lv<5>(): (sc_biguint<5>(p_4_reg_1109.read()) + sc_biguint<5>(ap_const_lv5_1));
}

void mnist_fp8::thread_index_tuple2_V_fu_6098_p2() {
    index_tuple2_V_fu_6098_p2 = (!p_20_reg_1390.read().is_01() || !ap_const_lv5_1.is_01())? sc_lv<5>(): (sc_biguint<5>(p_20_reg_1390.read()) + sc_biguint<5>(ap_const_lv5_1));
}

void mnist_fp8::thread_index_tuple3_V_fu_7264_p2() {
    index_tuple3_V_fu_7264_p2 = (!p_31_reg_1586.read().is_01() || !ap_const_lv5_1.is_01())? sc_lv<5>(): (sc_biguint<5>(p_31_reg_1586.read()) + sc_biguint<5>(ap_const_lv5_1));
}

void mnist_fp8::thread_index_tuple4_V_fu_9440_p2() {
    index_tuple4_V_fu_9440_p2 = (!p_45_reg_1828.read().is_01() || !ap_const_lv4_1.is_01())? sc_lv<4>(): (sc_biguint<4>(p_45_reg_1828.read()) + sc_biguint<4>(ap_const_lv4_1));
}

void mnist_fp8::thread_index_tuple5_V_fu_9584_p2() {
    index_tuple5_V_fu_9584_p2 = (!p_57_reg_1960.read().is_01() || !ap_const_lv4_1.is_01())? sc_lv<4>(): (sc_biguint<4>(p_57_reg_1960.read()) + sc_biguint<4>(ap_const_lv4_1));
}

void mnist_fp8::thread_index_tuple_V_fu_2204_p2() {
    index_tuple_V_fu_2204_p2 = (!p_s_reg_933.read().is_01() || !ap_const_lv5_1.is_01())? sc_lv<5>(): (sc_biguint<5>(p_s_reg_933.read()) + sc_biguint<5>(ap_const_lv5_1));
}

void mnist_fp8::thread_ireg_V_10_fu_8144_p1() {
    ireg_V_10_fu_8144_p1 = grp_fu_2061_p1.read();
}

void mnist_fp8::thread_ireg_V_11_fu_8180_p1() {
    ireg_V_11_fu_8180_p1 = grp_fu_2064_p1.read();
}

void mnist_fp8::thread_ireg_V_3_fu_5221_p3() {
    ireg_V_3_fu_5221_p3 = (!tmp_122_fu_5212_p2.read()[0].is_01())? sc_lv<64>(): ((tmp_122_fu_5212_p2.read()[0].to_bool())? ap_const_lv64_0: tmp_135_fu_5217_p1.read());
}

void mnist_fp8::thread_ireg_V_4_fu_6950_p3() {
    ireg_V_4_fu_6950_p3 = (!tmp_240_fu_6941_p2.read()[0].is_01())? sc_lv<64>(): ((tmp_240_fu_6941_p2.read()[0].to_bool())? ap_const_lv64_0: tmp_243_fu_6946_p1.read());
}

void mnist_fp8::thread_ireg_V_7_fu_9088_p3() {
    ireg_V_7_fu_9088_p3 = (!tmp_274_fu_9079_p2.read()[0].is_01())? sc_lv<64>(): ((tmp_274_fu_9079_p2.read()[0].to_bool())? ap_const_lv64_0: tmp_280_fu_9084_p1.read());
}

void mnist_fp8::thread_ireg_V_8_fu_4277_p1() {
    ireg_V_8_fu_4277_p1 = grp_fu_2061_p1.read();
}

void mnist_fp8::thread_ireg_V_9_fu_4313_p1() {
    ireg_V_9_fu_4313_p1 = grp_fu_2064_p1.read();
}

void mnist_fp8::thread_ireg_V_fu_2947_p3() {
    ireg_V_fu_2947_p3 = (!tmp_28_fu_2938_p2.read()[0].is_01())? sc_lv<64>(): ((tmp_28_fu_2938_p2.read()[0].to_bool())? ap_const_lv64_0: tmp_31_fu_2943_p1.read());
}

void mnist_fp8::thread_is_neg_1_fu_4846_p3() {
    is_neg_1_fu_4846_p3 = p_Val2_3_reg_1186.read().range(7, 7);
}

void mnist_fp8::thread_is_neg_2_fu_7669_p3() {
    is_neg_2_fu_7669_p3 = p_Val2_25_reg_11332.read().range(7, 7);
}

void mnist_fp8::thread_is_neg_3_fu_8713_p3() {
    is_neg_3_fu_8713_p3 = p_Val2_1_reg_1659.read().range(7, 7);
}

void mnist_fp8::thread_is_neg_fu_3739_p3() {
    is_neg_fu_3739_p3 = p_Val2_s_reg_10224.read().range(7, 7);
}

void mnist_fp8::thread_j_V_fu_2165_p2() {
    j_V_fu_2165_p2 = (!ap_phi_mux_p_1_phi_fu_831_p4.read().is_01() || !ap_const_lv5_1.is_01())? sc_lv<5>(): (sc_biguint<5>(ap_phi_mux_p_1_phi_fu_831_p4.read()) + sc_biguint<5>(ap_const_lv5_1));
}

void mnist_fp8::thread_lhs_V_10_cast_fu_3491_p1() {
    lhs_V_10_cast_fu_3491_p1 = esl_sext<7,6>(lhs_V_2_fu_3485_p2.read());
}

void mnist_fp8::thread_lhs_V_18_cast_fu_6255_p1() {
    lhs_V_18_cast_fu_6255_p1 = esl_sext<6,5>(lhs_V_8_fu_6249_p2.read());
}

void mnist_fp8::thread_lhs_V_1_cast_fu_6116_p1() {
    lhs_V_1_cast_fu_6116_p1 = esl_zext<8,5>(p_20_reg_1390.read());
}

void mnist_fp8::thread_lhs_V_24_cast_fu_7421_p1() {
    lhs_V_24_cast_fu_7421_p1 = esl_sext<6,5>(lhs_V_4_fu_7415_p2.read());
}

void mnist_fp8::thread_lhs_V_2_fu_3485_p2() {
    lhs_V_2_fu_3485_p2 = (!tmp_45_cast_fu_3477_p1.read().is_01() || !tmp_46_cast_fu_3481_p1.read().is_01())? sc_lv<6>(): (sc_biguint<6>(tmp_45_cast_fu_3477_p1.read()) - sc_biguint<6>(tmp_46_cast_fu_3481_p1.read()));
}

void mnist_fp8::thread_lhs_V_3_cast_fu_7282_p1() {
    lhs_V_3_cast_fu_7282_p1 = esl_zext<9,5>(p_31_reg_1586.read());
}

void mnist_fp8::thread_lhs_V_4_fu_7415_p2() {
    lhs_V_4_fu_7415_p2 = (!p_39_reg_1597.read().is_01() || !tmp_151_cast_fu_7411_p1.read().is_01())? sc_lv<5>(): (sc_biguint<5>(p_39_reg_1597.read()) - sc_biguint<5>(tmp_151_cast_fu_7411_p1.read()));
}

void mnist_fp8::thread_lhs_V_5_cast_fu_5587_p1() {
    lhs_V_5_cast_fu_5587_p1 = esl_zext<9,4>(p_27_reg_1299.read());
}

void mnist_fp8::thread_lhs_V_7_cast_fu_5646_p1() {
    lhs_V_7_cast_fu_5646_p1 = esl_zext<11,4>(p_33_reg_1310.read());
}

void mnist_fp8::thread_lhs_V_8_fu_6249_p2() {
    lhs_V_8_fu_6249_p2 = (!p_30_reg_1401.read().is_01() || !tmp_90_cast_fu_6245_p1.read().is_01())? sc_lv<5>(): (sc_biguint<5>(p_30_reg_1401.read()) - sc_biguint<5>(tmp_90_cast_fu_6245_p1.read()));
}

void mnist_fp8::thread_lhs_V_cast_39_fu_3283_p1() {
    lhs_V_cast_39_fu_3283_p1 = esl_zext<10,5>(p_4_reg_1109.read());
}

void mnist_fp8::thread_lhs_V_cast_fu_2394_p1() {
    lhs_V_cast_fu_2394_p1 = esl_sext<11,6>(lhs_V_s_fu_2388_p2.read());
}

void mnist_fp8::thread_lhs_V_s_fu_2388_p2() {
    lhs_V_s_fu_2388_p2 = (!tmp_24_cast_fu_2380_p1.read().is_01() || !tmp_28_cast_fu_2384_p1.read().is_01())? sc_lv<6>(): (sc_biguint<6>(tmp_24_cast_fu_2380_p1.read()) - sc_biguint<6>(tmp_28_cast_fu_2384_p1.read()));
}

void mnist_fp8::thread_man_V_11_fu_8230_p2() {
    man_V_11_fu_8230_p2 = (!ap_const_lv54_0.is_01() || !p_Result_44_fu_8226_p1.read().is_01())? sc_lv<54>(): (sc_biguint<54>(ap_const_lv54_0) - sc_biguint<54>(p_Result_44_fu_8226_p1.read()));
}

void mnist_fp8::thread_man_V_12_fu_8236_p3() {
    man_V_12_fu_8236_p3 = (!isneg_2_reg_11491.read()[0].is_01())? sc_lv<54>(): ((isneg_2_reg_11491.read()[0].to_bool())? man_V_11_fu_8230_p2.read(): p_Result_44_fu_8226_p1.read());
}

void mnist_fp8::thread_man_V_13_fu_9142_p2() {
    man_V_13_fu_9142_p2 = (!ap_const_lv54_0.is_01() || !p_Result_25_fu_9138_p1.read().is_01())? sc_lv<54>(): (sc_biguint<54>(ap_const_lv54_0) - sc_biguint<54>(p_Result_25_fu_9138_p1.read()));
}

void mnist_fp8::thread_man_V_14_fu_8315_p2() {
    man_V_14_fu_8315_p2 = (!ap_const_lv54_0.is_01() || !p_Result_45_fu_8311_p1.read().is_01())? sc_lv<54>(): (sc_biguint<54>(ap_const_lv54_0) - sc_biguint<54>(p_Result_45_fu_8311_p1.read()));
}

void mnist_fp8::thread_man_V_15_fu_8321_p3() {
    man_V_15_fu_8321_p3 = (!isneg_3_reg_11513.read()[0].is_01())? sc_lv<54>(): ((isneg_3_reg_11513.read()[0].to_bool())? man_V_14_fu_8315_p2.read(): p_Result_45_fu_8311_p1.read());
}

void mnist_fp8::thread_man_V_16_fu_9148_p3() {
    man_V_16_fu_9148_p3 = (!tmp_520_reg_11735.read()[0].is_01())? sc_lv<54>(): ((tmp_520_reg_11735.read()[0].to_bool())? man_V_13_fu_9142_p2.read(): p_Result_25_fu_9138_p1.read());
}

void mnist_fp8::thread_man_V_1_fu_3001_p2() {
    man_V_1_fu_3001_p2 = (!ap_const_lv54_0.is_01() || !p_Result_4_fu_2997_p1.read().is_01())? sc_lv<54>(): (sc_biguint<54>(ap_const_lv54_0) - sc_biguint<54>(p_Result_4_fu_2997_p1.read()));
}

void mnist_fp8::thread_man_V_2_fu_3007_p3() {
    man_V_2_fu_3007_p3 = (!tmp_142_reg_10042.read()[0].is_01())? sc_lv<54>(): ((tmp_142_reg_10042.read()[0].to_bool())? man_V_1_fu_3001_p2.read(): p_Result_4_fu_2997_p1.read());
}

void mnist_fp8::thread_man_V_3_fu_4363_p2() {
    man_V_3_fu_4363_p2 = (!ap_const_lv54_0.is_01() || !p_Result_36_fu_4359_p1.read().is_01())? sc_lv<54>(): (sc_biguint<54>(ap_const_lv54_0) - sc_biguint<54>(p_Result_36_fu_4359_p1.read()));
}

void mnist_fp8::thread_man_V_4_fu_4369_p3() {
    man_V_4_fu_4369_p3 = (!isneg_reg_10398.read()[0].is_01())? sc_lv<54>(): ((isneg_reg_10398.read()[0].to_bool())? man_V_3_fu_4363_p2.read(): p_Result_36_fu_4359_p1.read());
}

void mnist_fp8::thread_man_V_5_fu_5275_p2() {
    man_V_5_fu_5275_p2 = (!ap_const_lv54_0.is_01() || !p_Result_7_fu_5271_p1.read().is_01())? sc_lv<54>(): (sc_biguint<54>(ap_const_lv54_0) - sc_biguint<54>(p_Result_7_fu_5271_p1.read()));
}

void mnist_fp8::thread_man_V_6_fu_5281_p3() {
    man_V_6_fu_5281_p3 = (!tmp_327_reg_10642.read()[0].is_01())? sc_lv<54>(): ((tmp_327_reg_10642.read()[0].to_bool())? man_V_5_fu_5275_p2.read(): p_Result_7_fu_5271_p1.read());
}

void mnist_fp8::thread_man_V_7_fu_7010_p3() {
    man_V_7_fu_7010_p3 = (!tmp_455_reg_11149.read()[0].is_01())? sc_lv<54>(): ((tmp_455_reg_11149.read()[0].to_bool())? man_V_s_fu_7004_p2.read(): p_Result_19_fu_7000_p1.read());
}

void mnist_fp8::thread_man_V_8_fu_4448_p2() {
    man_V_8_fu_4448_p2 = (!ap_const_lv54_0.is_01() || !p_Result_37_fu_4444_p1.read().is_01())? sc_lv<54>(): (sc_biguint<54>(ap_const_lv54_0) - sc_biguint<54>(p_Result_37_fu_4444_p1.read()));
}

void mnist_fp8::thread_man_V_9_fu_4454_p3() {
    man_V_9_fu_4454_p3 = (!isneg_1_reg_10420.read()[0].is_01())? sc_lv<54>(): ((isneg_1_reg_10420.read()[0].to_bool())? man_V_8_fu_4448_p2.read(): p_Result_37_fu_4444_p1.read());
}

void mnist_fp8::thread_man_V_s_fu_7004_p2() {
    man_V_s_fu_7004_p2 = (!ap_const_lv54_0.is_01() || !p_Result_19_fu_7000_p1.read().is_01())? sc_lv<54>(): (sc_biguint<54>(ap_const_lv54_0) - sc_biguint<54>(p_Result_19_fu_7000_p1.read()));
}

void mnist_fp8::thread_msb_idx_1_cast_fu_3802_p1() {
    msb_idx_1_cast_fu_3802_p1 = esl_zext<32,31>(msb_idx_1_fu_3796_p3.read());
}

void mnist_fp8::thread_msb_idx_1_fu_3796_p3() {
    msb_idx_1_fu_3796_p3 = (!tmp_305_reg_10260.read()[0].is_01())? sc_lv<31>(): ((tmp_305_reg_10260.read()[0].to_bool())? ap_const_lv31_0: tmp_302_reg_10255.read());
}

void mnist_fp8::thread_msb_idx_2_fu_4887_p2() {
    msb_idx_2_fu_4887_p2 = (!ap_const_lv32_7.is_01() || !num_zeros_1_fu_4879_p3.read().is_01())? sc_lv<32>(): (sc_biguint<32>(ap_const_lv32_7) - sc_biguint<32>(num_zeros_1_fu_4879_p3.read()));
}

void mnist_fp8::thread_msb_idx_3_cast_fu_4911_p1() {
    msb_idx_3_cast_fu_4911_p1 = esl_zext<32,31>(msb_idx_3_fu_4905_p3.read());
}

void mnist_fp8::thread_msb_idx_3_fu_4905_p3() {
    msb_idx_3_fu_4905_p3 = (!tmp_343_reg_10551.read()[0].is_01())? sc_lv<31>(): ((tmp_343_reg_10551.read()[0].to_bool())? ap_const_lv31_0: tmp_341_reg_10546.read());
}

void mnist_fp8::thread_msb_idx_4_fu_5813_p2() {
    msb_idx_4_fu_5813_p2 = (!ap_const_lv32_7.is_01() || !num_zeros_2_fu_5805_p3.read().is_01())? sc_lv<32>(): (sc_biguint<32>(ap_const_lv32_7) - sc_biguint<32>(num_zeros_2_fu_5805_p3.read()));
}

void mnist_fp8::thread_msb_idx_5_cast_fu_5837_p1() {
    msb_idx_5_cast_fu_5837_p1 = esl_zext<32,31>(msb_idx_5_fu_5831_p3.read());
}

void mnist_fp8::thread_msb_idx_5_fu_5831_p3() {
    msb_idx_5_fu_5831_p3 = (!tmp_436_reg_10821.read()[0].is_01())? sc_lv<31>(): ((tmp_436_reg_10821.read()[0].to_bool())? ap_const_lv31_0: tmp_435_reg_10816.read());
}

void mnist_fp8::thread_msb_idx_6_fu_7708_p2() {
    msb_idx_6_fu_7708_p2 = (!ap_const_lv32_7.is_01() || !num_zeros_3_fu_7700_p3.read().is_01())? sc_lv<32>(): (sc_biguint<32>(ap_const_lv32_7) - sc_biguint<32>(num_zeros_3_fu_7700_p3.read()));
}

void mnist_fp8::thread_msb_idx_7_cast_fu_7732_p1() {
    msb_idx_7_cast_fu_7732_p1 = esl_zext<32,31>(msb_idx_7_fu_7726_p3.read());
}

void mnist_fp8::thread_msb_idx_7_fu_7726_p3() {
    msb_idx_7_fu_7726_p3 = (!tmp_508_reg_11368.read()[0].is_01())? sc_lv<31>(): ((tmp_508_reg_11368.read()[0].to_bool())? ap_const_lv31_0: tmp_507_reg_11363.read());
}

void mnist_fp8::thread_msb_idx_8_fu_8754_p2() {
    msb_idx_8_fu_8754_p2 = (!ap_const_lv32_7.is_01() || !num_zeros_4_fu_8746_p3.read().is_01())? sc_lv<32>(): (sc_biguint<32>(ap_const_lv32_7) - sc_biguint<32>(num_zeros_4_fu_8746_p3.read()));
}

void mnist_fp8::thread_msb_idx_9_cast_fu_8778_p1() {
    msb_idx_9_cast_fu_8778_p1 = esl_zext<32,31>(msb_idx_9_fu_8772_p3.read());
}

void mnist_fp8::thread_msb_idx_9_fu_8772_p3() {
    msb_idx_9_fu_8772_p3 = (!tmp_528_reg_11644.read()[0].is_01())? sc_lv<31>(): ((tmp_528_reg_11644.read()[0].to_bool())? ap_const_lv31_0: tmp_527_reg_11639.read());
}

void mnist_fp8::thread_msb_idx_fu_3778_p2() {
    msb_idx_fu_3778_p2 = (!ap_const_lv32_7.is_01() || !num_zeros_fu_3770_p3.read().is_01())? sc_lv<32>(): (sc_biguint<32>(ap_const_lv32_7) - sc_biguint<32>(num_zeros_fu_3770_p3.read()));
}

void mnist_fp8::thread_mul1_fu_9682_p0() {
    mul1_fu_9682_p0 =  (sc_lv<15>) (ap_const_lv28_2493);
}

void mnist_fp8::thread_mul1_fu_9682_p1() {
    mul1_fu_9682_p1 =  (sc_lv<13>) (sext1_cast_fu_3509_p1.read());
}

void mnist_fp8::thread_mul2_fu_9690_p0() {
    mul2_fu_9690_p0 =  (sc_lv<15>) (ap_const_lv28_29CC);
}

void mnist_fp8::thread_mul2_fu_9690_p1() {
    mul2_fu_9690_p1 =  (sc_lv<13>) (sext1_cast_fu_3509_p1.read());
}

void mnist_fp8::thread_mul3_fu_9707_p0() {
    mul3_fu_9707_p0 =  (sc_lv<13>) (ap_const_lv24_925);
}

void mnist_fp8::thread_mul3_fu_9707_p1() {
    mul3_fu_9707_p1 =  (sc_lv<11>) (sext3_cast_fu_6273_p1.read());
}

void mnist_fp8::thread_mul4_fu_9715_p0() {
    mul4_fu_9715_p0 =  (sc_lv<13>) (ap_const_lv24_A73);
}

void mnist_fp8::thread_mul4_fu_9715_p1() {
    mul4_fu_9715_p1 =  (sc_lv<11>) (sext3_cast_fu_6273_p1.read());
}

void mnist_fp8::thread_mul5_fu_9723_p0() {
    mul5_fu_9723_p0 =  (sc_lv<14>) (ap_const_lv26_124A);
}

void mnist_fp8::thread_mul5_fu_9723_p1() {
    mul5_fu_9723_p1 =  (sc_lv<12>) (sext5_cast_fu_7439_p1.read());
}

void mnist_fp8::thread_mul6_fu_9731_p0() {
    mul6_fu_9731_p0 =  (sc_lv<14>) (ap_const_lv26_14E6);
}

void mnist_fp8::thread_mul6_fu_9731_p1() {
    mul6_fu_9731_p1 =  (sc_lv<12>) (sext5_cast_fu_7439_p1.read());
}

void mnist_fp8::thread_mul_fu_9674_p0() {
    mul_fu_9674_p0 =  (sc_lv<13>) (ap_const_lv24_925);
}

void mnist_fp8::thread_neg_mul1_fu_3545_p2() {
    neg_mul1_fu_3545_p2 = (!ap_const_lv26_0.is_01() || !tmp_255_reg_10170.read().is_01())? sc_lv<26>(): (sc_biguint<26>(ap_const_lv26_0) - sc_biguint<26>(tmp_255_reg_10170.read()));
}

void mnist_fp8::thread_neg_mul2_fu_3593_p2() {
    neg_mul2_fu_3593_p2 = (!ap_const_lv27_0.is_01() || !tmp_266_reg_10188.read().is_01())? sc_lv<27>(): (sc_biguint<27>(ap_const_lv27_0) - sc_biguint<27>(tmp_266_reg_10188.read()));
}

void mnist_fp8::thread_neg_mul3_fu_6309_p2() {
    neg_mul3_fu_6309_p2 = (!ap_const_lv22_0.is_01() || !tmp_404_reg_10921.read().is_01())? sc_lv<22>(): (sc_biguint<22>(ap_const_lv22_0) - sc_biguint<22>(tmp_404_reg_10921.read()));
}

void mnist_fp8::thread_neg_mul4_fu_6357_p2() {
    neg_mul4_fu_6357_p2 = (!ap_const_lv23_0.is_01() || !tmp_412_reg_10939.read().is_01())? sc_lv<23>(): (sc_biguint<23>(ap_const_lv23_0) - sc_biguint<23>(tmp_412_reg_10939.read()));
}

void mnist_fp8::thread_neg_mul5_fu_7475_p2() {
    neg_mul5_fu_7475_p2 = (!ap_const_lv24_0.is_01() || !tmp_479_reg_11278.read().is_01())? sc_lv<24>(): (sc_biguint<24>(ap_const_lv24_0) - sc_biguint<24>(tmp_479_reg_11278.read()));
}

void mnist_fp8::thread_neg_mul6_fu_7523_p2() {
    neg_mul6_fu_7523_p2 = (!ap_const_lv25_0.is_01() || !tmp_487_reg_11296.read().is_01())? sc_lv<25>(): (sc_biguint<25>(ap_const_lv25_0) - sc_biguint<25>(tmp_487_reg_11296.read()));
}

void mnist_fp8::thread_neg_mul_fu_2426_p2() {
    neg_mul_fu_2426_p2 = (!ap_const_lv23_0.is_01() || !tmp_66_reg_9856.read().is_01())? sc_lv<23>(): (sc_biguint<23>(ap_const_lv23_0) - sc_biguint<23>(tmp_66_reg_9856.read()));
}

void mnist_fp8::thread_neg_ti1_fu_3574_p2() {
    neg_ti1_fu_3574_p2 = (!ap_const_lv13_0.is_01() || !tmp_264_fu_3567_p3.read().is_01())? sc_lv<13>(): (sc_biguint<13>(ap_const_lv13_0) - sc_biguint<13>(tmp_264_fu_3567_p3.read()));
}

void mnist_fp8::thread_neg_ti2_fu_3626_p2() {
    neg_ti2_fu_3626_p2 = (!ap_const_lv2_0.is_01() || !tmp_276_fu_3622_p1.read().is_01())? sc_lv<2>(): (sc_biguint<2>(ap_const_lv2_0) - sc_biguint<2>(tmp_276_fu_3622_p1.read()));
}

void mnist_fp8::thread_neg_ti3_fu_6338_p2() {
    neg_ti3_fu_6338_p2 = (!ap_const_lv11_0.is_01() || !tmp_410_fu_6331_p3.read().is_01())? sc_lv<11>(): (sc_biguint<11>(ap_const_lv11_0) - sc_biguint<11>(tmp_410_fu_6331_p3.read()));
}

void mnist_fp8::thread_neg_ti4_fu_6390_p2() {
    neg_ti4_fu_6390_p2 = (!ap_const_lv2_0.is_01() || !tmp_417_fu_6386_p1.read().is_01())? sc_lv<2>(): (sc_biguint<2>(ap_const_lv2_0) - sc_biguint<2>(tmp_417_fu_6386_p1.read()));
}

void mnist_fp8::thread_neg_ti5_fu_7504_p2() {
    neg_ti5_fu_7504_p2 = (!ap_const_lv12_0.is_01() || !tmp_485_fu_7497_p3.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_0) - sc_biguint<12>(tmp_485_fu_7497_p3.read()));
}

void mnist_fp8::thread_neg_ti6_fu_7556_p2() {
    neg_ti6_fu_7556_p2 = (!ap_const_lv3_0.is_01() || !tmp_492_fu_7552_p1.read().is_01())? sc_lv<3>(): (sc_biguint<3>(ap_const_lv3_0) - sc_biguint<3>(tmp_492_fu_7552_p1.read()));
}

void mnist_fp8::thread_neg_ti_fu_2455_p2() {
    neg_ti_fu_2455_p2 = (!ap_const_lv11_0.is_01() || !tmp_79_fu_2448_p3.read().is_01())? sc_lv<11>(): (sc_biguint<11>(ap_const_lv11_0) - sc_biguint<11>(tmp_79_fu_2448_p3.read()));
}

void mnist_fp8::thread_newSel10_fu_4646_p3() {
    newSel10_fu_4646_p3 = (!or_cond6_fu_4627_p2.read()[0].is_01())? sc_lv<8>(): ((or_cond6_fu_4627_p2.read()[0].to_bool())? newSel8_fu_4619_p3.read(): newSel9_fu_4633_p3.read());
}

void mnist_fp8::thread_newSel11_fu_4768_p3() {
    newSel11_fu_4768_p3 = (!sel_tmp35_fu_4763_p2.read()[0].is_01())? sc_lv<8>(): ((sel_tmp35_fu_4763_p2.read()[0].to_bool())? tmp_401_fu_4705_p1.read(): tmp_400_fu_4685_p1.read());
}

void mnist_fp8::thread_newSel12_fu_4782_p3() {
    newSel12_fu_4782_p3 = (!sel_tmp32_fu_4740_p2.read()[0].is_01())? sc_lv<8>(): ((sel_tmp32_fu_4740_p2.read()[0].to_bool())? storemerge6_fu_4689_p3.read(): tmp_398_reg_10499.read());
}

void mnist_fp8::thread_newSel13_fu_4795_p3() {
    newSel13_fu_4795_p3 = (!or_cond9_fu_4776_p2.read()[0].is_01())? sc_lv<8>(): ((or_cond9_fu_4776_p2.read()[0].to_bool())? newSel11_fu_4768_p3.read(): newSel12_fu_4782_p3.read());
}

void mnist_fp8::thread_newSel14_fu_7175_p3() {
    newSel14_fu_7175_p3 = (!sel_tmp44_fu_7170_p2.read()[0].is_01())? sc_lv<8>(): ((sel_tmp44_fu_7170_p2.read()[0].to_bool())? tmp_460_fu_7112_p1.read(): tmp_459_fu_7092_p1.read());
}

void mnist_fp8::thread_newSel15_fu_7189_p3() {
    newSel15_fu_7189_p3 = (!sel_tmp41_fu_7147_p2.read()[0].is_01())? sc_lv<8>(): ((sel_tmp41_fu_7147_p2.read()[0].to_bool())? storemerge8_fu_7096_p3.read(): tmp_457_reg_11194.read());
}

void mnist_fp8::thread_newSel16_fu_7202_p3() {
    newSel16_fu_7202_p3 = (!or_cond12_fu_7183_p2.read()[0].is_01())? sc_lv<8>(): ((or_cond12_fu_7183_p2.read()[0].to_bool())? newSel14_fu_7175_p3.read(): newSel15_fu_7189_p3.read());
}

void mnist_fp8::thread_newSel17_fu_7216_p3() {
    newSel17_fu_7216_p3 = (!or_cond14_fu_7210_p2.read()[0].is_01())? sc_lv<8>(): ((or_cond14_fu_7210_p2.read()[0].to_bool())? newSel16_fu_7202_p3.read(): ap_const_lv8_0);
}

void mnist_fp8::thread_newSel18_fu_9313_p3() {
    newSel18_fu_9313_p3 = (!sel_tmp53_fu_9308_p2.read()[0].is_01())? sc_lv<8>(): ((sel_tmp53_fu_9308_p2.read()[0].to_bool())? tmp_525_fu_9250_p1.read(): tmp_524_fu_9230_p1.read());
}

void mnist_fp8::thread_newSel19_fu_9327_p3() {
    newSel19_fu_9327_p3 = (!sel_tmp50_fu_9285_p2.read()[0].is_01())? sc_lv<8>(): ((sel_tmp50_fu_9285_p2.read()[0].to_bool())? storemerge5_fu_9234_p3.read(): tmp_522_reg_11780.read());
}

void mnist_fp8::thread_newSel1_fu_3186_p3() {
    newSel1_fu_3186_p3 = (!sel_tmp9_fu_3144_p2.read()[0].is_01())? sc_lv<8>(): ((sel_tmp9_fu_3144_p2.read()[0].to_bool())? storemerge_fu_3093_p3.read(): tmp_148_reg_10087.read());
}

void mnist_fp8::thread_newSel20_fu_9340_p3() {
    newSel20_fu_9340_p3 = (!or_cond15_fu_9321_p2.read()[0].is_01())? sc_lv<8>(): ((or_cond15_fu_9321_p2.read()[0].to_bool())? newSel18_fu_9313_p3.read(): newSel19_fu_9327_p3.read());
}

void mnist_fp8::thread_newSel21_fu_9354_p3() {
    newSel21_fu_9354_p3 = (!or_cond17_fu_9348_p2.read()[0].is_01())? sc_lv<8>(): ((or_cond17_fu_9348_p2.read()[0].to_bool())? newSel20_fu_9340_p3.read(): ap_const_lv8_0);
}

void mnist_fp8::thread_newSel22_fu_8486_p3() {
    newSel22_fu_8486_p3 = (!sel_tmp62_fu_8481_p2.read()[0].is_01())? sc_lv<8>(): ((sel_tmp62_fu_8481_p2.read()[0].to_bool())? tmp_549_fu_8423_p1.read(): tmp_548_fu_8403_p1.read());
}

void mnist_fp8::thread_newSel23_fu_8500_p3() {
    newSel23_fu_8500_p3 = (!sel_tmp59_fu_8458_p2.read()[0].is_01())? sc_lv<8>(): ((sel_tmp59_fu_8458_p2.read()[0].to_bool())? storemerge1_fu_8407_p3.read(): tmp_546_reg_11558.read());
}

void mnist_fp8::thread_newSel24_fu_8513_p3() {
    newSel24_fu_8513_p3 = (!or_cond18_fu_8494_p2.read()[0].is_01())? sc_lv<8>(): ((or_cond18_fu_8494_p2.read()[0].to_bool())? newSel22_fu_8486_p3.read(): newSel23_fu_8500_p3.read());
}

void mnist_fp8::thread_newSel25_fu_8635_p3() {
    newSel25_fu_8635_p3 = (!sel_tmp71_fu_8630_p2.read()[0].is_01())? sc_lv<8>(): ((sel_tmp71_fu_8630_p2.read()[0].to_bool())? tmp_556_fu_8572_p1.read(): tmp_555_fu_8552_p1.read());
}

void mnist_fp8::thread_newSel26_fu_8649_p3() {
    newSel26_fu_8649_p3 = (!sel_tmp68_fu_8607_p2.read()[0].is_01())? sc_lv<8>(): ((sel_tmp68_fu_8607_p2.read()[0].to_bool())? storemerge3_fu_8556_p3.read(): tmp_553_reg_11592.read());
}

void mnist_fp8::thread_newSel27_fu_8662_p3() {
    newSel27_fu_8662_p3 = (!or_cond21_fu_8643_p2.read()[0].is_01())? sc_lv<8>(): ((or_cond21_fu_8643_p2.read()[0].to_bool())? newSel25_fu_8635_p3.read(): newSel26_fu_8649_p3.read());
}

void mnist_fp8::thread_newSel2_fu_3199_p3() {
    newSel2_fu_3199_p3 = (!or_cond_fu_3180_p2.read()[0].is_01())? sc_lv<8>(): ((or_cond_fu_3180_p2.read()[0].to_bool())? newSel_fu_3172_p3.read(): newSel1_fu_3186_p3.read());
}

void mnist_fp8::thread_newSel3_fu_3213_p3() {
    newSel3_fu_3213_p3 = (!or_cond2_fu_3207_p2.read()[0].is_01())? sc_lv<8>(): ((or_cond2_fu_3207_p2.read()[0].to_bool())? newSel2_fu_3199_p3.read(): ap_const_lv8_0);
}

void mnist_fp8::thread_newSel4_fu_5446_p3() {
    newSel4_fu_5446_p3 = (!sel_tmp17_fu_5441_p2.read()[0].is_01())? sc_lv<8>(): ((sel_tmp17_fu_5441_p2.read()[0].to_bool())? tmp_332_fu_5383_p1.read(): tmp_331_fu_5363_p1.read());
}

void mnist_fp8::thread_newSel5_fu_5460_p3() {
    newSel5_fu_5460_p3 = (!sel_tmp14_fu_5418_p2.read()[0].is_01())? sc_lv<8>(): ((sel_tmp14_fu_5418_p2.read()[0].to_bool())? storemerge2_fu_5367_p3.read(): tmp_329_reg_10687.read());
}

void mnist_fp8::thread_newSel6_fu_5473_p3() {
    newSel6_fu_5473_p3 = (!or_cond3_fu_5454_p2.read()[0].is_01())? sc_lv<8>(): ((or_cond3_fu_5454_p2.read()[0].to_bool())? newSel4_fu_5446_p3.read(): newSel5_fu_5460_p3.read());
}

void mnist_fp8::thread_newSel7_fu_5487_p3() {
    newSel7_fu_5487_p3 = (!or_cond5_fu_5481_p2.read()[0].is_01())? sc_lv<8>(): ((or_cond5_fu_5481_p2.read()[0].to_bool())? newSel6_fu_5473_p3.read(): ap_const_lv8_0);
}

void mnist_fp8::thread_newSel8_fu_4619_p3() {
    newSel8_fu_4619_p3 = (!sel_tmp26_fu_4614_p2.read()[0].is_01())? sc_lv<8>(): ((sel_tmp26_fu_4614_p2.read()[0].to_bool())? tmp_394_fu_4556_p1.read(): tmp_393_fu_4536_p1.read());
}

void mnist_fp8::thread_newSel9_fu_4633_p3() {
    newSel9_fu_4633_p3 = (!sel_tmp23_fu_4591_p2.read()[0].is_01())? sc_lv<8>(): ((sel_tmp23_fu_4591_p2.read()[0].to_bool())? storemerge4_fu_4540_p3.read(): tmp_391_reg_10465.read());
}

void mnist_fp8::thread_newSel_fu_3172_p3() {
    newSel_fu_3172_p3 = (!sel_tmp4_fu_3167_p2.read()[0].is_01())? sc_lv<8>(): ((sel_tmp4_fu_3167_p2.read()[0].to_bool())? tmp_156_fu_3109_p1.read(): tmp_150_fu_3089_p1.read());
}

void mnist_fp8::thread_next_mul1_fu_6062_p2() {
    next_mul1_fu_6062_p2 = (!phi_mul1_reg_1379.read().is_01() || !ap_const_lv10_C4.is_01())? sc_lv<10>(): (sc_biguint<10>(phi_mul1_reg_1379.read()) + sc_biguint<10>(ap_const_lv10_C4));
}

void mnist_fp8::thread_next_mul2_fu_7228_p2() {
    next_mul2_fu_7228_p2 = (!phi_mul2_reg_1575.read().is_01() || !ap_const_lv11_C4.is_01())? sc_lv<11>(): (sc_biguint<11>(phi_mul2_reg_1575.read()) + sc_biguint<11>(ap_const_lv11_C4));
}

void mnist_fp8::thread_next_mul_fu_3225_p2() {
    next_mul_fu_3225_p2 = (!phi_mul_reg_1098.read().is_01() || !ap_const_lv12_310.is_01())? sc_lv<12>(): (sc_biguint<12>(phi_mul_reg_1098.read()) + sc_biguint<12>(ap_const_lv12_310));
}

void mnist_fp8::thread_not_zero1_V_fu_6074_p2() {
    not_zero1_V_fu_6074_p2 = (!p_13_reg_1368.read().is_01() || !ap_const_lv3_1.is_01())? sc_lv<3>(): (sc_biguint<3>(p_13_reg_1368.read()) + sc_biguint<3>(ap_const_lv3_1));
}

void mnist_fp8::thread_not_zero2_V_fu_7240_p2() {
    not_zero2_V_fu_7240_p2 = (!p_21_reg_1564.read().is_01() || !ap_const_lv4_1.is_01())? sc_lv<4>(): (sc_biguint<4>(p_21_reg_1564.read()) + sc_biguint<4>(ap_const_lv4_1));
}

void mnist_fp8::thread_not_zero3_V_fu_9428_p2() {
    not_zero3_V_fu_9428_p2 = (!p_37_reg_1817.read().is_01() || !ap_const_lv4_1.is_01())? sc_lv<4>(): (sc_biguint<4>(p_37_reg_1817.read()) + sc_biguint<4>(ap_const_lv4_1));
}

void mnist_fp8::thread_not_zero4_V_fu_9572_p2() {
    not_zero4_V_fu_9572_p2 = (!p_48_reg_1949.read().is_01() || !ap_const_lv5_1.is_01())? sc_lv<5>(): (sc_biguint<5>(p_48_reg_1949.read()) + sc_biguint<5>(ap_const_lv5_1));
}

void mnist_fp8::thread_not_zero_V_fu_3237_p2() {
    not_zero_V_fu_3237_p2 = (!p_6_reg_1087.read().is_01() || !ap_const_lv3_1.is_01())? sc_lv<3>(): (sc_biguint<3>(p_6_reg_1087.read()) + sc_biguint<3>(ap_const_lv3_1));
}

void mnist_fp8::thread_notlhs1_fu_5196_p2() {
    notlhs1_fu_5196_p2 = (!tmp_100_fu_5182_p4.read().is_01() || !ap_const_lv8_FF.is_01())? sc_lv<1>(): sc_lv<1>(tmp_100_fu_5182_p4.read() != ap_const_lv8_FF);
}

void mnist_fp8::thread_notlhs2_fu_6003_p2() {
    notlhs2_fu_6003_p2 = (!tmp_211_fu_5971_p4.read().is_01() || !ap_const_lv8_FF.is_01())? sc_lv<1>(): sc_lv<1>(tmp_211_fu_5971_p4.read() != ap_const_lv8_FF);
}

void mnist_fp8::thread_notlhs3_fu_6021_p2() {
    notlhs3_fu_6021_p2 = (!tmp_217_fu_5989_p4.read().is_01() || !ap_const_lv8_FF.is_01())? sc_lv<1>(): sc_lv<1>(tmp_217_fu_5989_p4.read() != ap_const_lv8_FF);
}

void mnist_fp8::thread_notlhs4_fu_6925_p2() {
    notlhs4_fu_6925_p2 = (!tmp_225_fu_6911_p4.read().is_01() || !ap_const_lv8_FF.is_01())? sc_lv<1>(): sc_lv<1>(tmp_225_fu_6911_p4.read() != ap_const_lv8_FF);
}

void mnist_fp8::thread_notlhs5_fu_9063_p2() {
    notlhs5_fu_9063_p2 = (!tmp_262_fu_9049_p4.read().is_01() || !ap_const_lv8_FF.is_01())? sc_lv<1>(): sc_lv<1>(tmp_262_fu_9049_p4.read() != ap_const_lv8_FF);
}

void mnist_fp8::thread_notlhs_fu_2922_p2() {
    notlhs_fu_2922_p2 = (!tmp_21_fu_2908_p4.read().is_01() || !ap_const_lv8_FF.is_01())? sc_lv<1>(): sc_lv<1>(tmp_21_fu_2908_p4.read() != ap_const_lv8_FF);
}

void mnist_fp8::thread_notrhs1_fu_5202_p2() {
    notrhs1_fu_5202_p2 = (!tmp_325_fu_5192_p1.read().is_01() || !ap_const_lv23_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_325_fu_5192_p1.read() == ap_const_lv23_0);
}

void mnist_fp8::thread_notrhs2_fu_6009_p2() {
    notrhs2_fu_6009_p2 = (!tmp_443_fu_5981_p1.read().is_01() || !ap_const_lv23_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_443_fu_5981_p1.read() == ap_const_lv23_0);
}

void mnist_fp8::thread_notrhs3_fu_6027_p2() {
    notrhs3_fu_6027_p2 = (!tmp_444_fu_5999_p1.read().is_01() || !ap_const_lv23_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_444_fu_5999_p1.read() == ap_const_lv23_0);
}

void mnist_fp8::thread_notrhs4_fu_6931_p2() {
    notrhs4_fu_6931_p2 = (!tmp_453_fu_6921_p1.read().is_01() || !ap_const_lv23_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_453_fu_6921_p1.read() == ap_const_lv23_0);
}

void mnist_fp8::thread_notrhs5_fu_9069_p2() {
    notrhs5_fu_9069_p2 = (!tmp_518_fu_9059_p1.read().is_01() || !ap_const_lv23_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_518_fu_9059_p1.read() == ap_const_lv23_0);
}

void mnist_fp8::thread_notrhs_fu_2928_p2() {
    notrhs_fu_2928_p2 = (!tmp_138_fu_2918_p1.read().is_01() || !ap_const_lv23_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_138_fu_2918_p1.read() == ap_const_lv23_0);
}

void mnist_fp8::thread_num_zeros_1_fu_4879_p3() {
    num_zeros_1_fu_4879_p3 = esl_cttz<32,32>(p_Result_39_fu_4871_p3.read());
}

void mnist_fp8::thread_num_zeros_2_fu_5805_p3() {
    num_zeros_2_fu_5805_p3 = esl_cttz<32,32>(p_Result_15_fu_5797_p3.read());
}

void mnist_fp8::thread_num_zeros_3_fu_7700_p3() {
    num_zeros_3_fu_7700_p3 = esl_cttz<32,32>(p_Result_42_fu_7692_p3.read());
}

void mnist_fp8::thread_num_zeros_4_fu_8746_p3() {
    num_zeros_4_fu_8746_p3 = esl_cttz<32,32>(p_Result_47_fu_8738_p3.read());
}

void mnist_fp8::thread_num_zeros_fu_3770_p3() {
    num_zeros_fu_3770_p3 = esl_cttz<32,32>(p_Result_34_fu_3762_p3.read());
}

void mnist_fp8::thread_or_cond10_fu_4789_p2() {
    or_cond10_fu_4789_p2 = (sel_tmp32_fu_4740_p2.read() | sel_tmp28_fu_4714_p2.read());
}

void mnist_fp8::thread_or_cond11_fu_4803_p2() {
    or_cond11_fu_4803_p2 = (or_cond9_fu_4776_p2.read() | or_cond10_fu_4789_p2.read());
}

void mnist_fp8::thread_or_cond12_fu_7183_p2() {
    or_cond12_fu_7183_p2 = (sel_tmp44_fu_7170_p2.read() | sel_tmp42_fu_7153_p2.read());
}

void mnist_fp8::thread_or_cond13_fu_7196_p2() {
    or_cond13_fu_7196_p2 = (sel_tmp41_fu_7147_p2.read() | sel_tmp37_fu_7121_p2.read());
}

void mnist_fp8::thread_or_cond14_fu_7210_p2() {
    or_cond14_fu_7210_p2 = (or_cond12_fu_7183_p2.read() | or_cond13_fu_7196_p2.read());
}

void mnist_fp8::thread_or_cond15_fu_9321_p2() {
    or_cond15_fu_9321_p2 = (sel_tmp53_fu_9308_p2.read() | sel_tmp51_fu_9291_p2.read());
}

void mnist_fp8::thread_or_cond16_fu_9334_p2() {
    or_cond16_fu_9334_p2 = (sel_tmp50_fu_9285_p2.read() | sel_tmp46_fu_9259_p2.read());
}

void mnist_fp8::thread_or_cond17_fu_9348_p2() {
    or_cond17_fu_9348_p2 = (or_cond15_fu_9321_p2.read() | or_cond16_fu_9334_p2.read());
}

void mnist_fp8::thread_or_cond18_fu_8494_p2() {
    or_cond18_fu_8494_p2 = (sel_tmp62_fu_8481_p2.read() | sel_tmp60_fu_8464_p2.read());
}

void mnist_fp8::thread_or_cond19_fu_8507_p2() {
    or_cond19_fu_8507_p2 = (sel_tmp59_fu_8458_p2.read() | sel_tmp55_fu_8432_p2.read());
}

void mnist_fp8::thread_or_cond1_fu_3193_p2() {
    or_cond1_fu_3193_p2 = (sel_tmp9_fu_3144_p2.read() | sel_tmp2_fu_3118_p2.read());
}

void mnist_fp8::thread_or_cond20_fu_8521_p2() {
    or_cond20_fu_8521_p2 = (or_cond18_fu_8494_p2.read() | or_cond19_fu_8507_p2.read());
}

void mnist_fp8::thread_or_cond21_fu_8643_p2() {
    or_cond21_fu_8643_p2 = (sel_tmp71_fu_8630_p2.read() | sel_tmp69_fu_8613_p2.read());
}

void mnist_fp8::thread_or_cond22_fu_8656_p2() {
    or_cond22_fu_8656_p2 = (sel_tmp68_fu_8607_p2.read() | sel_tmp64_fu_8581_p2.read());
}

void mnist_fp8::thread_or_cond23_fu_8670_p2() {
    or_cond23_fu_8670_p2 = (or_cond21_fu_8643_p2.read() | or_cond22_fu_8656_p2.read());
}

void mnist_fp8::thread_or_cond2_fu_3207_p2() {
    or_cond2_fu_3207_p2 = (or_cond_fu_3180_p2.read() | or_cond1_fu_3193_p2.read());
}

void mnist_fp8::thread_or_cond3_fu_5454_p2() {
    or_cond3_fu_5454_p2 = (sel_tmp17_fu_5441_p2.read() | sel_tmp15_fu_5424_p2.read());
}

void mnist_fp8::thread_or_cond4_fu_5467_p2() {
    or_cond4_fu_5467_p2 = (sel_tmp14_fu_5418_p2.read() | sel_tmp10_fu_5392_p2.read());
}

void mnist_fp8::thread_or_cond5_fu_5481_p2() {
    or_cond5_fu_5481_p2 = (or_cond3_fu_5454_p2.read() | or_cond4_fu_5467_p2.read());
}

void mnist_fp8::thread_or_cond6_fu_4627_p2() {
    or_cond6_fu_4627_p2 = (sel_tmp26_fu_4614_p2.read() | sel_tmp24_fu_4597_p2.read());
}

void mnist_fp8::thread_or_cond7_fu_4640_p2() {
    or_cond7_fu_4640_p2 = (sel_tmp23_fu_4591_p2.read() | sel_tmp19_fu_4565_p2.read());
}

void mnist_fp8::thread_or_cond8_41_fu_6210_p2() {
    or_cond8_41_fu_6210_p2 = (tmp137_fu_6204_p2.read() & tmp136_reg_10889.read());
}

void mnist_fp8::thread_or_cond8_fu_4654_p2() {
    or_cond8_fu_4654_p2 = (or_cond6_fu_4627_p2.read() | or_cond7_fu_4640_p2.read());
}

void mnist_fp8::thread_or_cond9_fu_4776_p2() {
    or_cond9_fu_4776_p2 = (sel_tmp35_fu_4763_p2.read() | sel_tmp33_fu_4746_p2.read());
}

void mnist_fp8::thread_or_cond_42_fu_7376_p2() {
    or_cond_42_fu_7376_p2 = (tmp245_fu_7370_p2.read() & tmp244_reg_11246.read());
}

void mnist_fp8::thread_or_cond_fu_3180_p2() {
    or_cond_fu_3180_p2 = (sel_tmp4_fu_3167_p2.read() | sel_tmp_fu_3150_p2.read());
}

void mnist_fp8::thread_p_012_0_i2_fu_5895_p3() {
    p_012_0_i2_fu_5895_p3 = (!icmp6_fu_5851_p2.read()[0].is_01())? sc_lv<32>(): ((icmp6_fu_5851_p2.read()[0].to_bool())? tmp32_V_3_fu_5866_p2.read(): tmp32_V_4_fu_5891_p1.read());
}

void mnist_fp8::thread_p_03_i1_fu_5961_p3() {
    p_03_i1_fu_5961_p3 = (!tmp_155_reg_10800.read()[0].is_01())? sc_lv<32>(): ((tmp_155_reg_10800.read()[0].to_bool())? ap_const_lv32_0: f_4_fu_5957_p1.read());
}

void mnist_fp8::thread_p_03_i1_to_int_fu_5968_p1() {
    p_03_i1_to_int_fu_5968_p1 = p_03_i1_reg_10841.read();
}

void mnist_fp8::thread_p_Repl2_10_trunc_fu_7828_p2() {
    p_Repl2_10_trunc_fu_7828_p2 = (!tmp336_cast_cast_fu_7821_p3.read().is_01() || !tmp_514_fu_7818_p1.read().is_01())? sc_lv<8>(): (sc_biguint<8>(tmp336_cast_cast_fu_7821_p3.read()) + sc_biguint<8>(tmp_514_fu_7818_p1.read()));
}

void mnist_fp8::thread_p_Repl2_13_trunc_fu_8883_p2() {
    p_Repl2_13_trunc_fu_8883_p2 = (!tmp_534_fu_8873_p1.read().is_01() || !tmp337_cast_cast_fu_8876_p3.read().is_01())? sc_lv<8>(): (sc_biguint<8>(tmp_534_fu_8873_p1.read()) + sc_biguint<8>(tmp337_cast_cast_fu_8876_p3.read()));
}

void mnist_fp8::thread_p_Repl2_1_trunc_fu_3898_p2() {
    p_Repl2_1_trunc_fu_3898_p2 = (!tmp323_cast_cast_fu_3891_p3.read().is_01() || !tmp_315_fu_3888_p1.read().is_01())? sc_lv<8>(): (sc_biguint<8>(tmp323_cast_cast_fu_3891_p3.read()) + sc_biguint<8>(tmp_315_fu_3888_p1.read()));
}

void mnist_fp8::thread_p_Repl2_4_trunc_fu_5016_p2() {
    p_Repl2_4_trunc_fu_5016_p2 = (!tmp_353_fu_5006_p1.read().is_01() || !tmp324_cast_cast_fu_5009_p3.read().is_01())? sc_lv<8>(): (sc_biguint<8>(tmp_353_fu_5006_p1.read()) + sc_biguint<8>(tmp324_cast_cast_fu_5009_p3.read()));
}

void mnist_fp8::thread_p_Repl2_7_trunc_fu_5933_p2() {
    p_Repl2_7_trunc_fu_5933_p2 = (!tmp_442_fu_5923_p1.read().is_01() || !tmp325_cast_cast_fu_5926_p3.read().is_01())? sc_lv<8>(): (sc_biguint<8>(tmp_442_fu_5923_p1.read()) + sc_biguint<8>(tmp325_cast_cast_fu_5926_p3.read()));
}

void mnist_fp8::thread_p_Result_14_fu_5787_p4() {
    p_Result_14_fu_5787_p4 = tmp_V_2_fu_5782_p3.read().range(0, 7);
}

void mnist_fp8::thread_p_Result_15_fu_5797_p3() {
    p_Result_15_fu_5797_p3 = esl_concat<24,8>(ap_const_lv24_FFFFFF, p_Result_14_fu_5787_p4.read());
}

void mnist_fp8::thread_p_Result_16_fu_5907_p4() {
    p_Result_16_fu_5907_p4 = tmp32_V_8_fu_5903_p1.read().range(30, 23);
}

void mnist_fp8::thread_p_Result_17_fu_5946_p5() {
    p_Result_17_fu_5946_p5 = esl_partset<32,32,9,32,32>(tmp32_V_8_reg_10831.read(), tmp_200_fu_5939_p3.read(), ap_const_lv32_17, ap_const_lv32_1F);
}

void mnist_fp8::thread_p_Result_19_fu_7000_p1() {
    p_Result_19_fu_7000_p1 = esl_zext<54,53>(tmp_160_fu_6993_p3.read());
}

void mnist_fp8::thread_p_Result_21_fu_7802_p4() {
    p_Result_21_fu_7802_p4 = tmp32_V_29_fu_7798_p1.read().range(30, 23);
}

void mnist_fp8::thread_p_Result_22_fu_7781_p2() {
    p_Result_22_fu_7781_p2 = (!tmp_512_fu_7777_p1.read().is_01())? sc_lv<8>(): p_Val2_30_reg_11352.read() >> (unsigned short)tmp_512_fu_7777_p1.read().to_uint();
}

void mnist_fp8::thread_p_Result_25_fu_9138_p1() {
    p_Result_25_fu_9138_p1 = esl_zext<54,53>(tmp_224_fu_9131_p3.read());
}

void mnist_fp8::thread_p_Result_28_fu_8857_p4() {
    p_Result_28_fu_8857_p4 = tmp32_V_30_fu_8853_p1.read().range(30, 23);
}

void mnist_fp8::thread_p_Result_29_fu_8827_p2() {
    p_Result_29_fu_8827_p2 = (!tmp_532_fu_8823_p1.read().is_01())? sc_lv<8>(): p_Val2_33_reg_11628.read() >> (unsigned short)tmp_532_fu_8823_p1.read().to_uint();
}

void mnist_fp8::thread_p_Result_33_fu_3752_p4() {
    p_Result_33_fu_3752_p4 = p_Val2_6_fu_3746_p3.read().range(0, 7);
}

void mnist_fp8::thread_p_Result_34_fu_3762_p3() {
    p_Result_34_fu_3762_p3 = esl_concat<24,8>(ap_const_lv24_FFFFFF, p_Result_33_fu_3752_p4.read());
}

void mnist_fp8::thread_p_Result_35_fu_3911_p5() {
    p_Result_35_fu_3911_p5 = esl_partset<32,32,9,32,32>(tmp32_V_27_reg_10270.read(), tmp_84_fu_3904_p3.read(), ap_const_lv32_17, ap_const_lv32_1F);
}

void mnist_fp8::thread_p_Result_36_fu_4359_p1() {
    p_Result_36_fu_4359_p1 = esl_zext<54,53>(tmp_128_fu_4352_p3.read());
}

void mnist_fp8::thread_p_Result_37_fu_4444_p1() {
    p_Result_37_fu_4444_p1 = esl_zext<54,53>(tmp_171_fu_4437_p3.read());
}

void mnist_fp8::thread_p_Result_38_fu_4861_p4() {
    p_Result_38_fu_4861_p4 = p_Val2_12_fu_4854_p3.read().range(0, 7);
}

void mnist_fp8::thread_p_Result_39_fu_4871_p3() {
    p_Result_39_fu_4871_p3 = esl_concat<24,8>(ap_const_lv24_FFFFFF, p_Result_38_fu_4861_p4.read());
}

void mnist_fp8::thread_p_Result_40_fu_5029_p5() {
    p_Result_40_fu_5029_p5 = esl_partset<32,32,9,32,32>(tmp32_V_28_reg_10566.read(), tmp_115_fu_5022_p3.read(), ap_const_lv32_17, ap_const_lv32_1F);
}

void mnist_fp8::thread_p_Result_41_fu_7682_p4() {
    p_Result_41_fu_7682_p4 = p_Val2_30_fu_7676_p3.read().range(0, 7);
}

void mnist_fp8::thread_p_Result_42_fu_7692_p3() {
    p_Result_42_fu_7692_p3 = esl_concat<24,8>(ap_const_lv24_FFFFFF, p_Result_41_fu_7682_p4.read());
}

void mnist_fp8::thread_p_Result_43_fu_7841_p5() {
    p_Result_43_fu_7841_p5 = esl_partset<32,32,9,32,32>(tmp32_V_29_reg_11378.read(), tmp_242_fu_7834_p3.read(), ap_const_lv32_17, ap_const_lv32_1F);
}

void mnist_fp8::thread_p_Result_44_fu_8226_p1() {
    p_Result_44_fu_8226_p1 = esl_zext<54,53>(tmp_293_fu_8219_p3.read());
}

void mnist_fp8::thread_p_Result_45_fu_8311_p1() {
    p_Result_45_fu_8311_p1 = esl_zext<54,53>(tmp_335_fu_8304_p3.read());
}

void mnist_fp8::thread_p_Result_46_fu_8728_p4() {
    p_Result_46_fu_8728_p4 = p_Val2_33_fu_8721_p3.read().range(0, 7);
}

void mnist_fp8::thread_p_Result_47_fu_8738_p3() {
    p_Result_47_fu_8738_p3 = esl_concat<24,8>(ap_const_lv24_FFFFFF, p_Result_46_fu_8728_p4.read());
}

void mnist_fp8::thread_p_Result_48_fu_8896_p5() {
    p_Result_48_fu_8896_p5 = esl_partset<32,32,9,32,32>(tmp32_V_30_reg_11659.read(), tmp_283_fu_8889_p3.read(), ap_const_lv32_17, ap_const_lv32_1F);
}

void mnist_fp8::thread_p_Result_4_fu_2997_p1() {
    p_Result_4_fu_2997_p1 = esl_zext<54,53>(tmp_33_fu_2990_p3.read());
}

void mnist_fp8::thread_p_Result_7_fu_5271_p1() {
    p_Result_7_fu_5271_p1 = esl_zext<54,53>(tmp_72_fu_5264_p3.read());
}

void mnist_fp8::thread_p_Result_8_fu_4960_p2() {
    p_Result_8_fu_4960_p2 = (!tmp_351_fu_4956_p1.read().is_01())? sc_lv<8>(): p_Val2_12_reg_10535.read() >> (unsigned short)tmp_351_fu_4956_p1.read().to_uint();
}

void mnist_fp8::thread_p_Result_9_fu_3872_p4() {
    p_Result_9_fu_3872_p4 = tmp32_V_27_fu_3868_p1.read().range(30, 23);
}

void mnist_fp8::thread_p_Result_s_40_fu_4990_p4() {
    p_Result_s_40_fu_4990_p4 = tmp32_V_28_fu_4986_p1.read().range(30, 23);
}

void mnist_fp8::thread_p_Result_s_fu_3851_p2() {
    p_Result_s_fu_3851_p2 = (!tmp_313_fu_3847_p1.read().is_01())? sc_lv<8>(): p_Val2_6_reg_10244.read() >> (unsigned short)tmp_313_fu_3847_p1.read().to_uint();
}

void mnist_fp8::thread_p_Val2_12_fu_4854_p3() {
    p_Val2_12_fu_4854_p3 = (!is_neg_1_fu_4846_p3.read()[0].is_01())? sc_lv<8>(): ((is_neg_1_fu_4846_p3.read()[0].to_bool())? tmp_73_reg_10347.read(): p_Val2_3_reg_1186.read());
}

void mnist_fp8::thread_p_Val2_30_fu_7676_p3() {
    p_Val2_30_fu_7676_p3 = (!is_neg_2_fu_7669_p3.read()[0].is_01())? sc_lv<8>(): ((is_neg_2_fu_7669_p3.read()[0].to_bool())? tmp_182_reg_11342.read(): p_Val2_25_reg_11332.read());
}

void mnist_fp8::thread_p_Val2_33_fu_8721_p3() {
    p_Val2_33_fu_8721_p3 = (!is_neg_3_fu_8713_p3.read()[0].is_01())? sc_lv<8>(): ((is_neg_3_fu_8713_p3.read()[0].to_bool())? tmp_226_reg_11440.read(): p_Val2_1_reg_1659.read());
}

void mnist_fp8::thread_p_Val2_4_fu_4660_p3() {
    p_Val2_4_fu_4660_p3 = (!or_cond8_fu_4654_p2.read()[0].is_01())? sc_lv<8>(): ((or_cond8_fu_4654_p2.read()[0].to_bool())? newSel10_fu_4646_p3.read(): ap_const_lv8_0);
}

void mnist_fp8::thread_p_Val2_5_fu_8527_p3() {
    p_Val2_5_fu_8527_p3 = (!or_cond20_fu_8521_p2.read()[0].is_01())? sc_lv<8>(): ((or_cond20_fu_8521_p2.read()[0].to_bool())? newSel24_fu_8513_p3.read(): ap_const_lv8_0);
}

void mnist_fp8::thread_p_Val2_6_fu_3746_p3() {
    p_Val2_6_fu_3746_p3 = (!is_neg_fu_3739_p3.read()[0].is_01())? sc_lv<8>(): ((is_neg_fu_3739_p3.read()[0].to_bool())? tmp_55_reg_10234.read(): p_Val2_s_reg_10224.read());
}

void mnist_fp8::thread_p_Val2_7_fu_4809_p3() {
    p_Val2_7_fu_4809_p3 = (!or_cond11_fu_4803_p2.read()[0].is_01())? sc_lv<8>(): ((or_cond11_fu_4803_p2.read()[0].to_bool())? newSel13_fu_4795_p3.read(): ap_const_lv8_0);
}

void mnist_fp8::thread_p_Val2_8_fu_8676_p3() {
    p_Val2_8_fu_8676_p3 = (!or_cond23_fu_8670_p2.read()[0].is_01())? sc_lv<8>(): ((or_cond23_fu_8670_p2.read()[0].to_bool())? newSel27_fu_8662_p3.read(): ap_const_lv8_0);
}

void mnist_fp8::thread_p_shl10_cast_fu_2218_p1() {
    p_shl10_cast_fu_2218_p1 = esl_zext<11,10>(tmp_7_fu_2210_p3.read());
}

void mnist_fp8::thread_p_shl11_cast_fu_2230_p1() {
    p_shl11_cast_fu_2230_p1 = esl_zext<11,6>(tmp_10_fu_2222_p3.read());
}

void mnist_fp8::thread_p_shl14_cast_fu_2568_p1() {
    p_shl14_cast_fu_2568_p1 = esl_zext<9,8>(tmp_17_fu_2560_p3.read());
}

void mnist_fp8::thread_p_shl16_cast1_fu_2542_p1() {
    p_shl16_cast1_fu_2542_p1 = esl_zext<9,5>(tmp_13_fu_2534_p3.read());
}

void mnist_fp8::thread_p_shl16_cast_fu_2546_p1() {
    p_shl16_cast_fu_2546_p1 = esl_zext<6,5>(tmp_13_fu_2534_p3.read());
}

void mnist_fp8::thread_p_shl17_cast_fu_2607_p3() {
    p_shl17_cast_fu_2607_p3 = esl_concat<8,5>(tmp_60_fu_2603_p1.read(), ap_const_lv5_0);
}

void mnist_fp8::thread_p_shl18_cast_fu_2623_p1() {
    p_shl18_cast_fu_2623_p1 = esl_sext<13,12>(tmp_61_fu_2615_p3.read());
}

void mnist_fp8::thread_p_shl1_cast_fu_2248_p1() {
    p_shl1_cast_fu_2248_p1 = esl_zext<11,7>(p_shl1_fu_2240_p3.read());
}

void mnist_fp8::thread_p_shl1_fu_2240_p3() {
    p_shl1_fu_2240_p3 = esl_concat<5,2>(p_s_reg_933.read(), ap_const_lv2_0);
}

void mnist_fp8::thread_p_shl20_cast_fu_2675_p1() {
    p_shl20_cast_fu_2675_p1 = esl_zext<11,10>(tmp_159_fu_2667_p3.read());
}

void mnist_fp8::thread_p_shl21_cast_fu_2687_p1() {
    p_shl21_cast_fu_2687_p1 = esl_zext<11,6>(tmp_161_fu_2679_p3.read());
}

void mnist_fp8::thread_p_shl22_cast_fu_2802_p1() {
    p_shl22_cast_fu_2802_p1 = esl_zext<9,8>(tmp_51_fu_2794_p3.read());
}

void mnist_fp8::thread_p_shl23_cast_fu_2814_p1() {
    p_shl23_cast_fu_2814_p1 = esl_zext<9,5>(tmp_53_fu_2806_p3.read());
}

void mnist_fp8::thread_p_shl24_cast_fu_2853_p3() {
    p_shl24_cast_fu_2853_p3 = esl_concat<8,5>(tmp_104_fu_2849_p1.read(), ap_const_lv5_0);
}

void mnist_fp8::thread_p_shl25_cast_fu_2869_p1() {
    p_shl25_cast_fu_2869_p1 = esl_sext<13,12>(tmp_105_fu_2861_p3.read());
}

void mnist_fp8::thread_p_shl26_cast_fu_3251_p1() {
    p_shl26_cast_fu_3251_p1 = esl_zext<9,8>(tmp_96_fu_3243_p3.read());
}

void mnist_fp8::thread_p_shl27_cast_fu_3263_p1() {
    p_shl27_cast_fu_3263_p1 = esl_zext<9,4>(tmp_97_fu_3255_p3.read());
}

void mnist_fp8::thread_p_shl28_cast_fu_3296_p3() {
    p_shl28_cast_fu_3296_p3 = esl_concat<8,5>(tmp_120_fu_3292_p1.read(), ap_const_lv5_0);
}

void mnist_fp8::thread_p_shl29_cast_fu_3312_p1() {
    p_shl29_cast_fu_3312_p1 = esl_sext<13,11>(tmp_127_fu_3304_p3.read());
}

void mnist_fp8::thread_p_shl2_cast_fu_3330_p1() {
    p_shl2_cast_fu_3330_p1 = esl_zext<11,10>(p_shl2_fu_3322_p3.read());
}

void mnist_fp8::thread_p_shl2_fu_3322_p3() {
    p_shl2_fu_3322_p3 = esl_concat<5,5>(p_4_reg_1109.read(), ap_const_lv5_0);
}

void mnist_fp8::thread_p_shl30_cast_fu_3692_p3() {
    p_shl30_cast_fu_3692_p3 = esl_concat<8,5>(tmp_294_reg_10214.read(), ap_const_lv5_0);
}

void mnist_fp8::thread_p_shl31_cast_fu_3706_p1() {
    p_shl31_cast_fu_3706_p1 = esl_sext<13,11>(tmp_295_fu_3699_p3.read());
}

void mnist_fp8::thread_p_shl32_cast_fu_3654_p1() {
    p_shl32_cast_fu_3654_p1 = esl_zext<8,7>(tmp_289_fu_3647_p3.read());
}

void mnist_fp8::thread_p_shl33_cast_fu_3665_p1() {
    p_shl33_cast_fu_3665_p1 = esl_zext<8,4>(tmp_290_fu_3658_p3.read());
}

void mnist_fp8::thread_p_shl34_cast_fu_3975_p1() {
    p_shl34_cast_fu_3975_p1 = esl_zext<9,8>(tmp_111_fu_3967_p3.read());
}

void mnist_fp8::thread_p_shl36_cast_fu_5076_p1() {
    p_shl36_cast_fu_5076_p1 = esl_zext<9,8>(tmp_179_fu_5068_p3.read());
}

void mnist_fp8::thread_p_shl37_cast_fu_5088_p1() {
    p_shl37_cast_fu_5088_p1 = esl_zext<9,5>(tmp_181_fu_5080_p3.read());
}

void mnist_fp8::thread_p_shl38_cast_fu_4014_p3() {
    p_shl38_cast_fu_4014_p3 = esl_concat<8,5>(tmp_186_fu_4010_p1.read(), ap_const_lv5_0);
}

void mnist_fp8::thread_p_shl39_cast_fu_4030_p1() {
    p_shl39_cast_fu_4030_p1 = esl_sext<13,12>(tmp_189_fu_4022_p3.read());
}

void mnist_fp8::thread_p_shl3_cast_fu_3342_p1() {
    p_shl3_cast_fu_3342_p1 = esl_zext<11,7>(p_shl3_fu_3334_p3.read());
}

void mnist_fp8::thread_p_shl3_fu_3334_p3() {
    p_shl3_fu_3334_p3 = esl_concat<5,2>(p_4_reg_1109.read(), ap_const_lv2_0);
}

void mnist_fp8::thread_p_shl40_cast_fu_5515_p1() {
    p_shl40_cast_fu_5515_p1 = esl_zext<9,8>(tmp_227_fu_5507_p3.read());
}

void mnist_fp8::thread_p_shl41_cast_fu_5527_p1() {
    p_shl41_cast_fu_5527_p1 = esl_zext<9,5>(tmp_228_fu_5519_p3.read());
}

void mnist_fp8::thread_p_shl42_cast_fu_5549_p1() {
    p_shl42_cast_fu_5549_p1 = esl_zext<8,7>(tmp_237_fu_5541_p3.read());
}

void mnist_fp8::thread_p_shl43_cast_fu_5561_p1() {
    p_shl43_cast_fu_5561_p1 = esl_zext<8,4>(tmp_238_fu_5553_p3.read());
}

void mnist_fp8::thread_p_shl44_cast_fu_5127_p3() {
    p_shl44_cast_fu_5127_p3 = esl_concat<8,5>(tmp_246_fu_5123_p1.read(), ap_const_lv5_0);
}

void mnist_fp8::thread_p_shl45_cast_fu_5143_p1() {
    p_shl45_cast_fu_5143_p1 = esl_sext<13,12>(tmp_249_fu_5135_p3.read());
}

void mnist_fp8::thread_p_shl46_cast_fu_5600_p3() {
    p_shl46_cast_fu_5600_p3 = esl_concat<7,4>(tmp_316_fu_5596_p1.read(), ap_const_lv4_0);
}

void mnist_fp8::thread_p_shl47_cast_fu_5616_p1() {
    p_shl47_cast_fu_5616_p1 = esl_sext<11,10>(tmp_318_fu_5608_p3.read());
}

void mnist_fp8::thread_p_shl49_cast_fu_4103_p1() {
    p_shl49_cast_fu_4103_p1 = esl_zext<9,8>(tmp_356_fu_4095_p3.read());
}

void mnist_fp8::thread_p_shl4_cast_fu_6145_p1() {
    p_shl4_cast_fu_6145_p1 = esl_zext<9,8>(p_shl4_fu_6137_p3.read());
}

void mnist_fp8::thread_p_shl4_fu_6137_p3() {
    p_shl4_fu_6137_p3 = esl_concat<4,4>(tmp_365_fu_6133_p1.read(), ap_const_lv4_0);
}

void mnist_fp8::thread_p_shl50_cast_fu_4115_p1() {
    p_shl50_cast_fu_4115_p1 = esl_zext<9,4>(tmp_357_fu_4107_p3.read());
}

void mnist_fp8::thread_p_shl51_cast_fu_6536_p1() {
    p_shl51_cast_fu_6536_p1 = esl_zext<9,8>(tmp_361_fu_6528_p3.read());
}

void mnist_fp8::thread_p_shl52_cast_fu_6548_p1() {
    p_shl52_cast_fu_6548_p1 = esl_zext<9,5>(tmp_362_fu_6540_p3.read());
}

void mnist_fp8::thread_p_shl53_cast_fu_4170_p3() {
    p_shl53_cast_fu_4170_p3 = esl_concat<8,5>(tmp_369_fu_4166_p1.read(), ap_const_lv5_0);
}

void mnist_fp8::thread_p_shl54_cast_fu_4213_p3() {
    p_shl54_cast_fu_4213_p3 = esl_concat<7,2>(tmp_374_fu_4209_p1.read(), ap_const_lv2_0);
}

void mnist_fp8::thread_p_shl55_cast_fu_4186_p1() {
    p_shl55_cast_fu_4186_p1 = esl_sext<13,11>(tmp_370_fu_4178_p3.read());
}

void mnist_fp8::thread_p_shl56_cast_fu_6805_p1() {
    p_shl56_cast_fu_6805_p1 = esl_zext<9,8>(tmp_376_fu_6797_p3.read());
}

void mnist_fp8::thread_p_shl57_cast_fu_6817_p1() {
    p_shl57_cast_fu_6817_p1 = esl_zext<9,5>(tmp_377_fu_6809_p3.read());
}

void mnist_fp8::thread_p_shl58_cast_fu_6587_p3() {
    p_shl58_cast_fu_6587_p3 = esl_concat<8,4>(tmp_380_fu_6583_p1.read(), ap_const_lv4_0);
}

void mnist_fp8::thread_p_shl59_cast_fu_6603_p1() {
    p_shl59_cast_fu_6603_p1 = esl_sext<12,11>(tmp_381_fu_6595_p3.read());
}

void mnist_fp8::thread_p_shl5_cast_fu_6155_p1() {
    p_shl5_cast_fu_6155_p1 = esl_zext<9,5>(tmp_366_fu_6149_p2.read());
}

void mnist_fp8::thread_p_shl5_fu_6658_p1() {
    p_shl5_fu_6658_p1 = esl_zext<64,9>(tmp_463_fu_6650_p3.read());
}

void mnist_fp8::thread_p_shl60_cast_fu_5702_p3() {
    p_shl60_cast_fu_5702_p3 = esl_concat<8,5>(tmp_384_fu_5698_p1.read(), ap_const_lv5_0);
}

void mnist_fp8::thread_p_shl61_cast_fu_5718_p1() {
    p_shl61_cast_fu_5718_p1 = esl_sext<13,12>(tmp_385_fu_5710_p3.read());
}

void mnist_fp8::thread_p_shl62_cast_fu_6418_p1() {
    p_shl62_cast_fu_6418_p1 = esl_zext<7,6>(tmp_419_fu_6411_p3.read());
}

void mnist_fp8::thread_p_shl63_cast_fu_6429_p1() {
    p_shl63_cast_fu_6429_p1 = esl_zext<7,3>(tmp_420_fu_6422_p3.read());
}

void mnist_fp8::thread_p_shl64_cast_fu_6456_p3() {
    p_shl64_cast_fu_6456_p3 = esl_concat<7,4>(tmp_423_reg_10965.read(), ap_const_lv4_0);
}

void mnist_fp8::thread_p_shl65_cast_fu_6470_p1() {
    p_shl65_cast_fu_6470_p1 = esl_sext<11,9>(tmp_424_fu_6463_p3.read());
}

void mnist_fp8::thread_p_shl66_cast_fu_6856_p3() {
    p_shl66_cast_fu_6856_p3 = esl_concat<8,4>(tmp_430_fu_6852_p1.read(), ap_const_lv4_0);
}

void mnist_fp8::thread_p_shl67_cast_fu_6872_p1() {
    p_shl67_cast_fu_6872_p1 = esl_sext<12,11>(tmp_431_fu_6864_p3.read());
}

void mnist_fp8::thread_p_shl68_cast_fu_7903_p1() {
    p_shl68_cast_fu_7903_p1 = esl_zext<9,8>(tmp_446_fu_7895_p3.read());
}

void mnist_fp8::thread_p_shl69_cast_fu_7915_p1() {
    p_shl69_cast_fu_7915_p1 = esl_zext<9,5>(tmp_447_fu_7907_p3.read());
}

void mnist_fp8::thread_p_shl6_cast_fu_7311_p1() {
    p_shl6_cast_fu_7311_p1 = esl_zext<9,8>(p_shl6_fu_7303_p3.read());
}

void mnist_fp8::thread_p_shl6_fu_7303_p3() {
    p_shl6_fu_7303_p3 = esl_concat<4,4>(tmp_450_fu_7299_p1.read(), ap_const_lv4_0);
}

void mnist_fp8::thread_p_shl71_cast_fu_8943_p1() {
    p_shl71_cast_fu_8943_p1 = esl_zext<9,8>(tmp_465_fu_8935_p3.read());
}

void mnist_fp8::thread_p_shl72_cast_fu_8955_p1() {
    p_shl72_cast_fu_8955_p1 = esl_zext<9,5>(tmp_466_fu_8947_p3.read());
}

void mnist_fp8::thread_p_shl73_cast_fu_7954_p3() {
    p_shl73_cast_fu_7954_p3 = esl_concat<8,4>(tmp_469_fu_7950_p1.read(), ap_const_lv4_0);
}

void mnist_fp8::thread_p_shl74_cast_fu_7970_p1() {
    p_shl74_cast_fu_7970_p1 = esl_sext<12,11>(tmp_470_fu_7962_p3.read());
}

void mnist_fp8::thread_p_shl75_cast_fu_6721_p3() {
    p_shl75_cast_fu_6721_p3 = esl_concat<8,2>(tmp_474_fu_6717_p1.read(), ap_const_lv2_0);
}

void mnist_fp8::thread_p_shl76_cast_fu_7584_p1() {
    p_shl76_cast_fu_7584_p1 = esl_zext<8,7>(tmp_494_fu_7577_p3.read());
}

void mnist_fp8::thread_p_shl77_cast_fu_7595_p1() {
    p_shl77_cast_fu_7595_p1 = esl_zext<8,4>(tmp_495_fu_7588_p3.read());
}

void mnist_fp8::thread_p_shl78_cast_fu_7622_p3() {
    p_shl78_cast_fu_7622_p3 = esl_concat<8,4>(tmp_498_reg_11322.read(), ap_const_lv4_0);
}

void mnist_fp8::thread_p_shl79_cast_fu_7636_p1() {
    p_shl79_cast_fu_7636_p1 = esl_sext<12,10>(tmp_499_fu_7629_p3.read());
}

void mnist_fp8::thread_p_shl7_cast_fu_7321_p1() {
    p_shl7_cast_fu_7321_p1 = esl_zext<9,5>(tmp_451_fu_7315_p2.read());
}

void mnist_fp8::thread_p_shl7_fu_8025_p1() {
    p_shl7_fu_8025_p1 = esl_zext<64,10>(tmp_535_fu_8017_p3.read());
}

void mnist_fp8::thread_p_shl83_cast_fu_8994_p3() {
    p_shl83_cast_fu_8994_p3 = esl_concat<8,4>(tmp_503_fu_8990_p1.read(), ap_const_lv4_0);
}

void mnist_fp8::thread_p_shl84_cast_fu_9010_p1() {
    p_shl84_cast_fu_9010_p1 = esl_sext<12,11>(tmp_504_fu_9002_p3.read());
}

void mnist_fp8::thread_p_shl89_cast_fu_8080_p3() {
    p_shl89_cast_fu_8080_p3 = esl_concat<9,2>(tmp_539_fu_8076_p1.read(), ap_const_lv2_0);
}

void mnist_fp8::thread_p_shl8_cast_fu_2137_p1() {
    p_shl8_cast_fu_2137_p1 = esl_zext<11,10>(tmp_1_fu_2129_p3.read());
}

void mnist_fp8::thread_p_shl9_cast_fu_2149_p1() {
    p_shl9_cast_fu_2149_p1 = esl_zext<11,7>(tmp_9_fu_2141_p3.read());
}

void mnist_fp8::thread_p_shl_fu_4085_p1() {
    p_shl_fu_4085_p1 = esl_zext<64,8>(tmp_354_fu_4077_p3.read());
}

void mnist_fp8::thread_p_v1_fu_6379_p3() {
    p_v1_fu_6379_p3 = (!tmp_405_reg_10926.read()[0].is_01())? sc_lv<12>(): ((tmp_405_reg_10926.read()[0].to_bool())? tmp_414_fu_6372_p1.read(): tmp_416_fu_6376_p1.read());
}

void mnist_fp8::thread_p_v2_fu_7545_p3() {
    p_v2_fu_7545_p3 = (!tmp_480_reg_11283.read()[0].is_01())? sc_lv<13>(): ((tmp_480_reg_11283.read()[0].to_bool())? tmp_489_fu_7538_p1.read(): tmp_491_fu_7542_p1.read());
}

void mnist_fp8::thread_p_v_fu_3615_p3() {
    p_v_fu_3615_p3 = (!tmp_256_reg_10175.read()[0].is_01())? sc_lv<14>(): ((tmp_256_reg_10175.read()[0].to_bool())? tmp_268_fu_3608_p1.read(): tmp_273_fu_3612_p1.read());
}

void mnist_fp8::thread_pad_temp1_0_address0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state96.read())) {
        pad_temp1_0_address0 =  (sc_lv<12>) (tmp_467_cast_fu_4258_p1.read());
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state90.read())) {
        pad_temp1_0_address0 =  (sc_lv<12>) (tmp_437_cast_fu_3935_p1.read());
    } else {
        pad_temp1_0_address0 = "XXXXXXXXXXXX";
    }
}

void mnist_fp8::thread_pad_temp1_0_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state96.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state90.read()))) {
        pad_temp1_0_ce0 = ap_const_logic_1;
    } else {
        pad_temp1_0_ce0 = ap_const_logic_0;
    }
}

void mnist_fp8::thread_pad_temp1_0_we0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state90.read())) {
        pad_temp1_0_we0 = ap_const_logic_1;
    } else {
        pad_temp1_0_we0 = ap_const_logic_0;
    }
}

void mnist_fp8::thread_pad_temp2_0_address0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state163.read())) {
        pad_temp2_0_address0 =  (sc_lv<10>) (tmp_513_fu_6766_p1.read());
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state157.read())) {
        pad_temp2_0_address0 =  (sc_lv<10>) (tmp_488_cast_fu_6500_p1.read());
    } else {
        pad_temp2_0_address0 =  (sc_lv<10>) ("XXXXXXXXXX");
    }
}

void mnist_fp8::thread_pad_temp2_0_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state163.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state157.read()))) {
        pad_temp2_0_ce0 = ap_const_logic_1;
    } else {
        pad_temp2_0_ce0 = ap_const_logic_0;
    }
}

void mnist_fp8::thread_pad_temp2_0_we0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state157.read())) {
        pad_temp2_0_we0 = ap_const_logic_1;
    } else {
        pad_temp2_0_we0 = ap_const_logic_0;
    }
}

void mnist_fp8::thread_pad_temp3_0_address0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state219.read())) {
        pad_temp3_0_address0 =  (sc_lv<11>) (tmp_542_fu_8125_p1.read());
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state213.read())) {
        pad_temp3_0_address0 =  (sc_lv<11>) (tmp_552_cast_fu_7866_p1.read());
    } else {
        pad_temp3_0_address0 =  (sc_lv<11>) ("XXXXXXXXXXX");
    }
}

void mnist_fp8::thread_pad_temp3_0_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state219.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state213.read()))) {
        pad_temp3_0_ce0 = ap_const_logic_1;
    } else {
        pad_temp3_0_ce0 = ap_const_logic_0;
    }
}

void mnist_fp8::thread_pad_temp3_0_we0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state213.read())) {
        pad_temp3_0_we0 = ap_const_logic_1;
    } else {
        pad_temp3_0_we0 = ap_const_logic_0;
    }
}

void mnist_fp8::thread_pad_temp_0_0_address0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state37.read())) {
        pad_temp_0_0_address0 =  (sc_lv<10>) (tmp_389_cast_fu_2763_p1.read());
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state32.read())) {
        pad_temp_0_0_address0 =  (sc_lv<10>) (tmp_290_cast_fu_2514_p1.read());
    } else {
        pad_temp_0_0_address0 =  (sc_lv<10>) ("XXXXXXXXXX");
    }
}

void mnist_fp8::thread_pad_temp_0_0_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state37.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state32.read()))) {
        pad_temp_0_0_ce0 = ap_const_logic_1;
    } else {
        pad_temp_0_0_ce0 = ap_const_logic_0;
    }
}

void mnist_fp8::thread_pad_temp_0_0_we0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state32.read())) {
        pad_temp_0_0_we0 = ap_const_logic_1;
    } else {
        pad_temp_0_0_we0 = ap_const_logic_0;
    }
}

void mnist_fp8::thread_phi_mul160_cast_fu_6058_p1() {
    phi_mul160_cast_fu_6058_p1 = esl_zext<11,10>(phi_mul1_reg_1379.read());
}

void mnist_fp8::thread_phi_mul162_cast_fu_7224_p1() {
    phi_mul162_cast_fu_7224_p1 = esl_zext<12,11>(phi_mul2_reg_1575.read());
}

void mnist_fp8::thread_phi_mul_cast_fu_3221_p1() {
    phi_mul_cast_fu_3221_p1 = esl_zext<13,12>(phi_mul_reg_1098.read());
}

void mnist_fp8::thread_pool1_0_address0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state155.read())) {
        pool1_0_address0 =  (sc_lv<10>) (tmp_487_cast_fu_6486_p1.read());
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state123.read())) {
        pool1_0_address0 = pool1_0_addr_1_reg_10747.read();
    } else {
        pool1_0_address0 =  (sc_lv<10>) ("XXXXXXXXXX");
    }
}

void mnist_fp8::thread_pool1_0_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state123.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state155.read()))) {
        pool1_0_ce0 = ap_const_logic_1;
    } else {
        pool1_0_ce0 = ap_const_logic_0;
    }
}

void mnist_fp8::thread_pool1_0_we0() {
    if ((esl_seteq<1,1,1>(exitcond37_fu_5668_p2.read(), ap_const_lv1_1) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state123.read()))) {
        pool1_0_we0 = ap_const_logic_1;
    } else {
        pool1_0_we0 = ap_const_logic_0;
    }
}

void mnist_fp8::thread_r_V_10_fu_4243_p2() {
    r_V_10_fu_4243_p2 = (!rhs_V_3_cast_fu_4239_p1.read().is_01() || !p_18_reg_1174.read().is_01())? sc_lv<5>(): (sc_biguint<5>(rhs_V_3_cast_fu_4239_p1.read()) + sc_biguint<5>(p_18_reg_1174.read()));
}

void mnist_fp8::thread_r_V_11_fu_6219_p2() {
    r_V_11_fu_6219_p2 = (!ap_const_lv4_F.is_01() || !tmp_402_fu_6215_p1.read().is_01())? sc_lv<4>(): (sc_bigint<4>(ap_const_lv4_F) + sc_biguint<4>(tmp_402_fu_6215_p1.read()));
}

void mnist_fp8::thread_r_V_12_fu_6400_p3() {
    r_V_12_fu_6400_p3 = (!tmp_405_reg_10926.read()[0].is_01())? sc_lv<2>(): ((tmp_405_reg_10926.read()[0].to_bool())? neg_ti4_fu_6390_p2.read(): tmp_418_fu_6396_p1.read());
}

void mnist_fp8::thread_r_V_13_fu_7325_p2() {
    r_V_13_fu_7325_p2 = (!p_shl6_cast_fu_7311_p1.read().is_01() || !p_shl7_cast_fu_7321_p1.read().is_01())? sc_lv<9>(): (sc_biguint<9>(p_shl6_cast_fu_7311_p1.read()) - sc_biguint<9>(p_shl7_cast_fu_7321_p1.read()));
}

void mnist_fp8::thread_r_V_14_fu_6698_p2() {
    r_V_14_fu_6698_p2 = (!rhs_V_13_cast_fu_6694_p1.read().is_01() || !p_25_reg_1436.read().is_01())? sc_lv<4>(): (sc_biguint<4>(rhs_V_13_cast_fu_6694_p1.read()) + sc_biguint<4>(p_25_reg_1436.read()));
}

void mnist_fp8::thread_r_V_15_fu_7385_p2() {
    r_V_15_fu_7385_p2 = (!ap_const_lv4_F.is_01() || !tmp_476_fu_7381_p1.read().is_01())? sc_lv<4>(): (sc_bigint<4>(ap_const_lv4_F) + sc_biguint<4>(tmp_476_fu_7381_p1.read()));
}

void mnist_fp8::thread_r_V_16_fu_7566_p3() {
    r_V_16_fu_7566_p3 = (!tmp_480_reg_11283.read()[0].is_01())? sc_lv<3>(): ((tmp_480_reg_11283.read()[0].to_bool())? neg_ti6_fu_7556_p2.read(): tmp_493_fu_7562_p1.read());
}

void mnist_fp8::thread_r_V_17_fu_6751_p2() {
    r_V_17_fu_6751_p2 = (!rhs_V_8_cast_fu_6747_p1.read().is_01() || !p_32_reg_1448.read().is_01())? sc_lv<4>(): (sc_biguint<4>(rhs_V_8_cast_fu_6747_p1.read()) + sc_biguint<4>(p_32_reg_1448.read()));
}

void mnist_fp8::thread_r_V_17_tr_fu_3504_p2() {
    r_V_17_tr_fu_3504_p2 = (!tmp322_cast_fu_3501_p1.read().is_01() || !tmp32_reg_10139.read().is_01())? sc_lv<13>(): (sc_bigint<13>(tmp322_cast_fu_3501_p1.read()) + sc_biguint<13>(tmp32_reg_10139.read()));
}

void mnist_fp8::thread_r_V_18_fu_8057_p2() {
    r_V_18_fu_8057_p2 = (!rhs_V_17_cast_fu_8053_p1.read().is_01() || !p_35_reg_1635.read().is_01())? sc_lv<4>(): (sc_biguint<4>(rhs_V_17_cast_fu_8053_p1.read()) + sc_biguint<4>(p_35_reg_1635.read()));
}

void mnist_fp8::thread_r_V_19_fu_8110_p2() {
    r_V_19_fu_8110_p2 = (!rhs_V_15_cast_fu_8106_p1.read().is_01() || !p_44_reg_1647.read().is_01())? sc_lv<4>(): (sc_biguint<4>(rhs_V_15_cast_fu_8106_p1.read()) + sc_biguint<4>(p_44_reg_1647.read()));
}

void mnist_fp8::thread_r_V_1_fu_2252_p2() {
    r_V_1_fu_2252_p2 = (!p_shl10_cast_fu_2218_p1.read().is_01() || !p_shl1_cast_fu_2248_p1.read().is_01())? sc_lv<11>(): (sc_biguint<11>(p_shl10_cast_fu_2218_p1.read()) - sc_biguint<11>(p_shl1_cast_fu_2248_p1.read()));
}

void mnist_fp8::thread_r_V_2_fu_2748_p2() {
    r_V_2_fu_2748_p2 = (!rhs_V_cast_fu_2744_p1.read().is_01() || !p_8_reg_995.read().is_01())? sc_lv<5>(): (sc_biguint<5>(rhs_V_cast_fu_2744_p1.read()) + sc_biguint<5>(p_8_reg_995.read()));
}

void mnist_fp8::thread_r_V_31_tr_fu_6268_p2() {
    r_V_31_tr_fu_6268_p2 = (!tmp330_cast_fu_6265_p1.read().is_01() || !tmp138_reg_10894.read().is_01())? sc_lv<11>(): (sc_bigint<11>(tmp330_cast_fu_6265_p1.read()) + sc_biguint<11>(tmp138_reg_10894.read()));
}

void mnist_fp8::thread_r_V_3_fu_2661_p2() {
    r_V_3_fu_2661_p2 = (!p_5_reg_983.read().is_01() || !rhs_V_2_cast_fu_2657_p1.read().is_01())? sc_lv<5>(): (sc_biguint<5>(p_5_reg_983.read()) + sc_biguint<5>(rhs_V_2_cast_fu_2657_p1.read()));
}

void mnist_fp8::thread_r_V_4_fu_3451_p2() {
    r_V_4_fu_3451_p2 = (!ap_const_lv5_1F.is_01() || !p_14_reg_1121.read().is_01())? sc_lv<5>(): (sc_bigint<5>(ap_const_lv5_1F) + sc_biguint<5>(p_14_reg_1121.read()));
}

void mnist_fp8::thread_r_V_5_fu_3346_p2() {
    r_V_5_fu_3346_p2 = (!p_shl2_cast_fu_3330_p1.read().is_01() || !p_shl3_cast_fu_3342_p1.read().is_01())? sc_lv<11>(): (sc_biguint<11>(p_shl2_cast_fu_3330_p1.read()) - sc_biguint<11>(p_shl3_cast_fu_3342_p1.read()));
}

void mnist_fp8::thread_r_V_5_tr_fu_2398_p2() {
    r_V_5_tr_fu_2398_p2 = (!tmp1_reg_9819.read().is_01() || !lhs_V_cast_fu_2394_p1.read().is_01())? sc_lv<11>(): (sc_biguint<11>(tmp1_reg_9819.read()) + sc_bigint<11>(lhs_V_cast_fu_2394_p1.read()));
}

void mnist_fp8::thread_r_V_6_fu_3636_p3() {
    r_V_6_fu_3636_p3 = (!tmp_256_reg_10175.read()[0].is_01())? sc_lv<2>(): ((tmp_256_reg_10175.read()[0].to_bool())? neg_ti2_fu_3626_p2.read(): tmp_282_fu_3632_p1.read());
}

void mnist_fp8::thread_r_V_7_fu_6159_p2() {
    r_V_7_fu_6159_p2 = (!p_shl4_cast_fu_6145_p1.read().is_01() || !p_shl5_cast_fu_6155_p1.read().is_01())? sc_lv<9>(): (sc_biguint<9>(p_shl4_cast_fu_6145_p1.read()) - sc_biguint<9>(p_shl5_cast_fu_6155_p1.read()));
}

void mnist_fp8::thread_r_V_85_tr_fu_7434_p2() {
    r_V_85_tr_fu_7434_p2 = (!tmp335_cast_fu_7431_p1.read().is_01() || !tmp246_reg_11251.read().is_01())? sc_lv<12>(): (sc_bigint<12>(tmp335_cast_fu_7431_p1.read()) + sc_biguint<12>(tmp246_reg_11251.read()));
}

void mnist_fp8::thread_r_V_8_fu_5660_p3() {
    r_V_8_fu_5660_p3 = esl_concat<4,1>(p_33_reg_1310.read(), ap_const_lv1_0);
}

void mnist_fp8::thread_r_V_9_fu_4151_p2() {
    r_V_9_fu_4151_p2 = (!rhs_V_7_cast_fu_4147_p1.read().is_01() || !p_10_reg_1162.read().is_01())? sc_lv<5>(): (sc_biguint<5>(rhs_V_7_cast_fu_4147_p1.read()) + sc_biguint<5>(p_10_reg_1162.read()));
}

void mnist_fp8::thread_r_V_fu_2354_p2() {
    r_V_fu_2354_p2 = (!ap_const_lv5_1F.is_01() || !p_3_reg_945.read().is_01())? sc_lv<5>(): (sc_bigint<5>(ap_const_lv5_1F) + sc_biguint<5>(p_3_reg_945.read()));
}

void mnist_fp8::thread_r_V_s_fu_5626_p3() {
    r_V_s_fu_5626_p3 = esl_concat<4,1>(p_27_reg_1299.read(), ap_const_lv1_0);
}

void mnist_fp8::thread_ra100_V_fu_5734_p2() {
    ra100_V_fu_5734_p2 = (!p_43_reg_1357.read().is_01() || !ap_const_lv2_1.is_01())? sc_lv<2>(): (sc_biguint<2>(p_43_reg_1357.read()) + sc_biguint<2>(ap_const_lv2_1));
}

void mnist_fp8::thread_ra101_V_fu_9404_p2() {
    ra101_V_fu_9404_p2 = (!p_69_reg_1795.read().is_01() || !ap_const_lv2_1.is_01())? sc_lv<2>(): (sc_biguint<2>(p_69_reg_1795.read()) + sc_biguint<2>(ap_const_lv2_1));
}

void mnist_fp8::thread_ra102_V_fu_9416_p2() {
    ra102_V_fu_9416_p2 = (!p_72_reg_1806.read().is_01() || !ap_const_lv2_1.is_01())? sc_lv<2>(): (sc_biguint<2>(p_72_reg_1806.read()) + sc_biguint<2>(ap_const_lv2_1));
}

void mnist_fp8::thread_ra99_V_fu_5674_p2() {
    ra99_V_fu_5674_p2 = (!p_38_reg_1334.read().is_01() || !ap_const_lv2_1.is_01())? sc_lv<2>(): (sc_biguint<2>(p_38_reg_1334.read()) + sc_biguint<2>(ap_const_lv2_1));
}

void mnist_fp8::thread_rc1_V_fu_6631_p2() {
    rc1_V_fu_6631_p2 = (!p_40_reg_1460.read().is_01() || !ap_const_lv3_1.is_01())? sc_lv<3>(): (sc_biguint<3>(p_40_reg_1460.read()) + sc_biguint<3>(ap_const_lv3_1));
}

void mnist_fp8::thread_rc2_V_fu_7998_p2() {
    rc2_V_fu_7998_p2 = (!p_51_reg_1671.read().is_01() || !ap_const_lv4_1.is_01())? sc_lv<4>(): (sc_biguint<4>(p_51_reg_1671.read()) + sc_biguint<4>(ap_const_lv4_1));
}

void mnist_fp8::thread_rc3_V_fu_9500_p2() {
    rc3_V_fu_9500_p2 = (!p_64_reg_1883.read().is_01() || !ap_const_lv4_1.is_01())? sc_lv<4>(): (sc_biguint<4>(p_64_reg_1883.read()) + sc_biguint<4>(ap_const_lv4_1));
}

void mnist_fp8::thread_rc4_V_fu_9644_p2() {
    rc4_V_fu_9644_p2 = (!p_74_reg_2015.read().is_01() || !ap_const_lv5_1.is_01())? sc_lv<5>(): (sc_biguint<5>(p_74_reg_2015.read()) + sc_biguint<5>(ap_const_lv5_1));
}

void mnist_fp8::thread_rc_V_fu_4058_p2() {
    rc_V_fu_4058_p2 = (!p_24_reg_1198.read().is_01() || !ap_const_lv3_1.is_01())? sc_lv<3>(): (sc_biguint<3>(p_24_reg_1198.read()) + sc_biguint<3>(ap_const_lv3_1));
}

void mnist_fp8::thread_reducer4_fu_6051_p3() {
    reducer4_fu_6051_p3 = (!tmp_223_fu_6045_p2.read()[0].is_01())? sc_lv<32>(): ((tmp_223_fu_6045_p2.read()[0].to_bool())? tmp_140_reg_1345.read(): p_03_i1_reg_10841.read());
}

void mnist_fp8::thread_relu1_0_V_address0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state79.read())) {
        relu1_0_V_address0 =  (sc_lv<12>) (tmp_422_cast_fu_3722_p1.read());
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state56.read())) {
        relu1_0_V_address0 =  (sc_lv<12>) (tmp_368_cast_reg_10010.read());
    } else {
        relu1_0_V_address0 = "XXXXXXXXXXXX";
    }
}

void mnist_fp8::thread_relu1_0_V_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state79.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state56.read()))) {
        relu1_0_V_ce0 = ap_const_logic_1;
    } else {
        relu1_0_V_ce0 = ap_const_logic_0;
    }
}

void mnist_fp8::thread_relu1_0_V_we0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state56.read())) {
        relu1_0_V_we0 = ap_const_logic_1;
    } else {
        relu1_0_V_we0 = ap_const_logic_0;
    }
}

void mnist_fp8::thread_relu2_0_V_address0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state124.read())) {
        relu2_0_V_address0 =  (sc_lv<12>) (tmp_495_cast_fu_5758_p1.read());
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state119.read())) {
        relu2_0_V_address0 =  (sc_lv<12>) (tmp_429_cast_reg_10610.read());
    } else {
        relu2_0_V_address0 = "XXXXXXXXXXXX";
    }
}

void mnist_fp8::thread_relu2_0_V_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state124.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state119.read()))) {
        relu2_0_V_ce0 = ap_const_logic_1;
    } else {
        relu2_0_V_ce0 = ap_const_logic_0;
    }
}

void mnist_fp8::thread_relu2_0_V_we0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state119.read())) {
        relu2_0_V_we0 = ap_const_logic_1;
    } else {
        relu2_0_V_we0 = ap_const_logic_0;
    }
}

void mnist_fp8::thread_relu3_0_V_address0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state203.read())) {
        relu3_0_V_address0 =  (sc_lv<11>) (tmp_539_cast_fu_7652_p1.read());
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state182.read())) {
        relu3_0_V_address0 =  (sc_lv<11>) (tmp_506_cast_reg_11117.read());
    } else {
        relu3_0_V_address0 =  (sc_lv<11>) ("XXXXXXXXXXX");
    }
}

void mnist_fp8::thread_relu3_0_V_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state203.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state182.read()))) {
        relu3_0_V_ce0 = ap_const_logic_1;
    } else {
        relu3_0_V_ce0 = ap_const_logic_0;
    }
}

void mnist_fp8::thread_relu3_0_V_we0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state182.read())) {
        relu3_0_V_we0 = ap_const_logic_1;
    } else {
        relu3_0_V_we0 = ap_const_logic_0;
    }
}

void mnist_fp8::thread_relu4_0_V_address0() {
    relu4_0_V_address0 =  (sc_lv<11>) (tmp_558_cast_reg_11703.read());
}

void mnist_fp8::thread_relu4_0_V_ce0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state242.read())) {
        relu4_0_V_ce0 = ap_const_logic_1;
    } else {
        relu4_0_V_ce0 = ap_const_logic_0;
    }
}

void mnist_fp8::thread_relu4_0_V_we0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state242.read())) {
        relu4_0_V_we0 = ap_const_logic_1;
    } else {
        relu4_0_V_we0 = ap_const_logic_0;
    }
}

void mnist_fp8::thread_rhs_V_10_cast_fu_7331_p1() {
    rhs_V_10_cast_fu_7331_p1 = esl_sext<12,9>(r_V_13_fu_7325_p2.read());
}

void mnist_fp8::thread_rhs_V_13_cast_fu_6694_p1() {
    rhs_V_13_cast_fu_6694_p1 = esl_zext<4,2>(p_46_reg_1485.read());
}

void mnist_fp8::thread_rhs_V_15_cast_fu_8106_p1() {
    rhs_V_15_cast_fu_8106_p1 = esl_zext<4,2>(p_66_reg_1718.read());
}

void mnist_fp8::thread_rhs_V_17_cast_fu_8053_p1() {
    rhs_V_17_cast_fu_8053_p1 = esl_zext<4,2>(p_61_reg_1695.read());
}

void mnist_fp8::thread_rhs_V_2_cast_fu_2657_p1() {
    rhs_V_2_cast_fu_2657_p1 = esl_zext<5,2>(p_12_reg_1007.read());
}

void mnist_fp8::thread_rhs_V_3_cast_fu_4239_p1() {
    rhs_V_3_cast_fu_4239_p1 = esl_zext<5,2>(p_34_reg_1244.read());
}

void mnist_fp8::thread_rhs_V_4_cast_fu_6165_p1() {
    rhs_V_4_cast_fu_6165_p1 = esl_sext<11,9>(r_V_7_fu_6159_p2.read());
}

void mnist_fp8::thread_rhs_V_5_cast_fu_3352_p1() {
    rhs_V_5_cast_fu_3352_p1 = esl_sext<13,11>(r_V_5_fu_3346_p2.read());
}

void mnist_fp8::thread_rhs_V_7_cast_fu_4147_p1() {
    rhs_V_7_cast_fu_4147_p1 = esl_zext<5,2>(p_29_reg_1221.read());
}

void mnist_fp8::thread_rhs_V_8_cast_fu_6747_p1() {
    rhs_V_8_cast_fu_6747_p1 = esl_zext<4,2>(p_52_reg_1508.read());
}

void mnist_fp8::thread_rhs_V_cast_fu_2744_p1() {
    rhs_V_cast_fu_2744_p1 = esl_zext<5,2>(p_17_reg_1031.read());
}

void mnist_fp8::thread_rx1_V_fu_4233_p2() {
    rx1_V_fu_4233_p2 = (!p_34_reg_1244.read().is_01() || !ap_const_lv2_1.is_01())? sc_lv<2>(): (sc_biguint<2>(p_34_reg_1244.read()) + sc_biguint<2>(ap_const_lv2_1));
}

void mnist_fp8::thread_rx2_V_fu_6741_p2() {
    rx2_V_fu_6741_p2 = (!p_52_reg_1508.read().is_01() || !ap_const_lv2_1.is_01())? sc_lv<2>(): (sc_biguint<2>(p_52_reg_1508.read()) + sc_biguint<2>(ap_const_lv2_1));
}

void mnist_fp8::thread_rx3_V_fu_8100_p2() {
    rx3_V_fu_8100_p2 = (!p_66_reg_1718.read().is_01() || !ap_const_lv2_1.is_01())? sc_lv<2>(): (sc_biguint<2>(p_66_reg_1718.read()) + sc_biguint<2>(ap_const_lv2_1));
}

void mnist_fp8::thread_rx4_V_fu_9524_p2() {
    rx4_V_fu_9524_p2 = (!p_75_reg_1905.read().is_01() || !ap_const_lv2_1.is_01())? sc_lv<2>(): (sc_biguint<2>(p_75_reg_1905.read()) + sc_biguint<2>(ap_const_lv2_1));
}

void mnist_fp8::thread_rx5_V_fu_9668_p2() {
    rx5_V_fu_9668_p2 = (!p_77_reg_2037.read().is_01() || !ap_const_lv2_1.is_01())? sc_lv<2>(): (sc_biguint<2>(p_77_reg_2037.read()) + sc_biguint<2>(ap_const_lv2_1));
}

void mnist_fp8::thread_rx_V_fu_2738_p2() {
    rx_V_fu_2738_p2 = (!p_17_reg_1031.read().is_01() || !ap_const_lv2_1.is_01())? sc_lv<2>(): (sc_biguint<2>(p_17_reg_1031.read()) + sc_biguint<2>(ap_const_lv2_1));
}

void mnist_fp8::thread_ry1_V_fu_4141_p2() {
    ry1_V_fu_4141_p2 = (!p_29_reg_1221.read().is_01() || !ap_const_lv2_1.is_01())? sc_lv<2>(): (sc_biguint<2>(p_29_reg_1221.read()) + sc_biguint<2>(ap_const_lv2_1));
}

void mnist_fp8::thread_ry2_V_fu_6688_p2() {
    ry2_V_fu_6688_p2 = (!p_46_reg_1485.read().is_01() || !ap_const_lv2_1.is_01())? sc_lv<2>(): (sc_biguint<2>(p_46_reg_1485.read()) + sc_biguint<2>(ap_const_lv2_1));
}

void mnist_fp8::thread_ry3_V_fu_8047_p2() {
    ry3_V_fu_8047_p2 = (!p_61_reg_1695.read().is_01() || !ap_const_lv2_1.is_01())? sc_lv<2>(): (sc_biguint<2>(p_61_reg_1695.read()) + sc_biguint<2>(ap_const_lv2_1));
}

void mnist_fp8::thread_ry4_V_fu_9512_p2() {
    ry4_V_fu_9512_p2 = (!p_71_reg_1894.read().is_01() || !ap_const_lv2_1.is_01())? sc_lv<2>(): (sc_biguint<2>(p_71_reg_1894.read()) + sc_biguint<2>(ap_const_lv2_1));
}

void mnist_fp8::thread_ry5_V_fu_9656_p2() {
    ry5_V_fu_9656_p2 = (!p_76_reg_2026.read().is_01() || !ap_const_lv2_1.is_01())? sc_lv<2>(): (sc_biguint<2>(p_76_reg_2026.read()) + sc_biguint<2>(ap_const_lv2_1));
}

void mnist_fp8::thread_ry_V_fu_2651_p2() {
    ry_V_fu_2651_p2 = (!p_12_reg_1007.read().is_01() || !ap_const_lv2_1.is_01())? sc_lv<2>(): (sc_biguint<2>(p_12_reg_1007.read()) + sc_biguint<2>(ap_const_lv2_1));
}

void mnist_fp8::thread_sel_tmp10_fu_5392_p2() {
    sel_tmp10_fu_5392_p2 = (tmp_126_reg_10681.read() & sel_tmp5_fu_5387_p2.read());
}

void mnist_fp8::thread_sel_tmp110_demorgan_fu_7126_p2() {
    sel_tmp110_demorgan_fu_7126_p2 = (tmp_212_reg_11165.read() | tmp_235_reg_11188.read());
}

void mnist_fp8::thread_sel_tmp11_fu_5401_p2() {
    sel_tmp11_fu_5401_p2 = (sel_tmp83_demorgan_fu_5397_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp8::thread_sel_tmp125_demorgan_fu_7159_p2() {
    sel_tmp125_demorgan_fu_7159_p2 = (sel_tmp110_demorgan_fu_7126_p2.read() | tmp_232_reg_11176.read());
}

void mnist_fp8::thread_sel_tmp12_fu_5407_p2() {
    sel_tmp12_fu_5407_p2 = (tmp_123_reg_10669.read() & sel_tmp11_fu_5401_p2.read());
}

void mnist_fp8::thread_sel_tmp137_demorgan_fu_8437_p2() {
    sel_tmp137_demorgan_fu_8437_p2 = (tmp_286_reg_11507.read() | tmp_311_reg_11552.read());
}

void mnist_fp8::thread_sel_tmp13_fu_5412_p2() {
    sel_tmp13_fu_5412_p2 = (tmp_141_fu_5349_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp8::thread_sel_tmp14_fu_5418_p2() {
    sel_tmp14_fu_5418_p2 = (sel_tmp12_fu_5407_p2.read() & sel_tmp13_fu_5412_p2.read());
}

void mnist_fp8::thread_sel_tmp152_demorgan_fu_8470_p2() {
    sel_tmp152_demorgan_fu_8470_p2 = (sel_tmp137_demorgan_fu_8437_p2.read() | tmp_308_reg_11540.read());
}

void mnist_fp8::thread_sel_tmp15_fu_5424_p2() {
    sel_tmp15_fu_5424_p2 = (sel_tmp12_fu_5407_p2.read() & tmp_141_fu_5349_p2.read());
}

void mnist_fp8::thread_sel_tmp161_demorgan_fu_8586_p2() {
    sel_tmp161_demorgan_fu_8586_p2 = (tmp_336_reg_11529.read() | tmp_340_reg_11586.read());
}

void mnist_fp8::thread_sel_tmp16_fu_5435_p2() {
    sel_tmp16_fu_5435_p2 = (sel_tmp98_demorgan_fu_5430_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp8::thread_sel_tmp176_demorgan_fu_8619_p2() {
    sel_tmp176_demorgan_fu_8619_p2 = (sel_tmp161_demorgan_fu_8586_p2.read() | tmp_337_reg_11574.read());
}

void mnist_fp8::thread_sel_tmp17_fu_5441_p2() {
    sel_tmp17_fu_5441_p2 = (icmp2_reg_10693.read() & sel_tmp16_fu_5435_p2.read());
}

void mnist_fp8::thread_sel_tmp188_demorgan_fu_9264_p2() {
    sel_tmp188_demorgan_fu_9264_p2 = (tmp_279_reg_11751.read() | tmp_288_reg_11774.read());
}

void mnist_fp8::thread_sel_tmp18_fu_4560_p2() {
    sel_tmp18_fu_4560_p2 = (tmp_108_reg_10414.read() ^ ap_const_lv1_1);
}

void mnist_fp8::thread_sel_tmp19_fu_4565_p2() {
    sel_tmp19_fu_4565_p2 = (tmp_133_reg_10459.read() & sel_tmp18_fu_4560_p2.read());
}

void mnist_fp8::thread_sel_tmp1_fu_3113_p2() {
    sel_tmp1_fu_3113_p2 = (tmp_39_reg_10058.read() ^ ap_const_lv1_1);
}

void mnist_fp8::thread_sel_tmp203_demorgan_fu_9297_p2() {
    sel_tmp203_demorgan_fu_9297_p2 = (sel_tmp188_demorgan_fu_9264_p2.read() | tmp_281_reg_11762.read());
}

void mnist_fp8::thread_sel_tmp20_fu_4574_p2() {
    sel_tmp20_fu_4574_p2 = (sel_tmp32_demorgan_fu_4570_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp8::thread_sel_tmp21_demorgan_fu_3156_p2() {
    sel_tmp21_demorgan_fu_3156_p2 = (sel_tmp6_demorgan_fu_3123_p2.read() | tmp_43_reg_10069.read());
}

void mnist_fp8::thread_sel_tmp21_fu_4580_p2() {
    sel_tmp21_fu_4580_p2 = (tmp_130_reg_10447.read() & sel_tmp20_fu_4574_p2.read());
}

void mnist_fp8::thread_sel_tmp22_fu_4585_p2() {
    sel_tmp22_fu_4585_p2 = (tmp_144_fu_4522_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp8::thread_sel_tmp23_fu_4591_p2() {
    sel_tmp23_fu_4591_p2 = (sel_tmp21_fu_4580_p2.read() & sel_tmp22_fu_4585_p2.read());
}

void mnist_fp8::thread_sel_tmp24_fu_4597_p2() {
    sel_tmp24_fu_4597_p2 = (sel_tmp21_fu_4580_p2.read() & tmp_144_fu_4522_p2.read());
}

void mnist_fp8::thread_sel_tmp25_fu_4608_p2() {
    sel_tmp25_fu_4608_p2 = (sel_tmp47_demorgan_fu_4603_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp8::thread_sel_tmp26_fu_4614_p2() {
    sel_tmp26_fu_4614_p2 = (icmp4_reg_10471.read() & sel_tmp25_fu_4608_p2.read());
}

void mnist_fp8::thread_sel_tmp27_fu_4709_p2() {
    sel_tmp27_fu_4709_p2 = (tmp_172_reg_10436.read() ^ ap_const_lv1_1);
}

void mnist_fp8::thread_sel_tmp28_fu_4714_p2() {
    sel_tmp28_fu_4714_p2 = (tmp_176_reg_10493.read() & sel_tmp27_fu_4709_p2.read());
}

void mnist_fp8::thread_sel_tmp29_fu_4723_p2() {
    sel_tmp29_fu_4723_p2 = (sel_tmp56_demorgan_fu_4719_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp8::thread_sel_tmp2_fu_3118_p2() {
    sel_tmp2_fu_3118_p2 = (tmp_52_reg_10081.read() & sel_tmp1_fu_3113_p2.read());
}

void mnist_fp8::thread_sel_tmp30_fu_4729_p2() {
    sel_tmp30_fu_4729_p2 = (tmp_173_reg_10481.read() & sel_tmp29_fu_4723_p2.read());
}

void mnist_fp8::thread_sel_tmp31_fu_4734_p2() {
    sel_tmp31_fu_4734_p2 = (tmp_178_fu_4671_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp8::thread_sel_tmp32_demorgan_fu_4570_p2() {
    sel_tmp32_demorgan_fu_4570_p2 = (tmp_108_reg_10414.read() | tmp_133_reg_10459.read());
}

void mnist_fp8::thread_sel_tmp32_fu_4740_p2() {
    sel_tmp32_fu_4740_p2 = (sel_tmp30_fu_4729_p2.read() & sel_tmp31_fu_4734_p2.read());
}

void mnist_fp8::thread_sel_tmp33_fu_4746_p2() {
    sel_tmp33_fu_4746_p2 = (sel_tmp30_fu_4729_p2.read() & tmp_178_fu_4671_p2.read());
}

void mnist_fp8::thread_sel_tmp34_fu_4757_p2() {
    sel_tmp34_fu_4757_p2 = (sel_tmp71_demorgan_fu_4752_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp8::thread_sel_tmp35_fu_4763_p2() {
    sel_tmp35_fu_4763_p2 = (icmp5_reg_10505.read() & sel_tmp34_fu_4757_p2.read());
}

void mnist_fp8::thread_sel_tmp36_fu_7116_p2() {
    sel_tmp36_fu_7116_p2 = (tmp_212_reg_11165.read() ^ ap_const_lv1_1);
}

void mnist_fp8::thread_sel_tmp37_fu_7121_p2() {
    sel_tmp37_fu_7121_p2 = (tmp_235_reg_11188.read() & sel_tmp36_fu_7116_p2.read());
}

void mnist_fp8::thread_sel_tmp38_fu_7130_p2() {
    sel_tmp38_fu_7130_p2 = (sel_tmp110_demorgan_fu_7126_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp8::thread_sel_tmp39_fu_7136_p2() {
    sel_tmp39_fu_7136_p2 = (tmp_232_reg_11176.read() & sel_tmp38_fu_7130_p2.read());
}

void mnist_fp8::thread_sel_tmp3_fu_3161_p2() {
    sel_tmp3_fu_3161_p2 = (sel_tmp21_demorgan_fu_3156_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp8::thread_sel_tmp40_fu_7141_p2() {
    sel_tmp40_fu_7141_p2 = (tmp_245_fu_7078_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp8::thread_sel_tmp41_fu_7147_p2() {
    sel_tmp41_fu_7147_p2 = (sel_tmp39_fu_7136_p2.read() & sel_tmp40_fu_7141_p2.read());
}

void mnist_fp8::thread_sel_tmp42_fu_7153_p2() {
    sel_tmp42_fu_7153_p2 = (sel_tmp39_fu_7136_p2.read() & tmp_245_fu_7078_p2.read());
}

void mnist_fp8::thread_sel_tmp43_fu_7164_p2() {
    sel_tmp43_fu_7164_p2 = (sel_tmp125_demorgan_fu_7159_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp8::thread_sel_tmp44_fu_7170_p2() {
    sel_tmp44_fu_7170_p2 = (icmp7_reg_11200.read() & sel_tmp43_fu_7164_p2.read());
}

void mnist_fp8::thread_sel_tmp45_fu_9254_p2() {
    sel_tmp45_fu_9254_p2 = (tmp_279_reg_11751.read() ^ ap_const_lv1_1);
}

void mnist_fp8::thread_sel_tmp46_fu_9259_p2() {
    sel_tmp46_fu_9259_p2 = (tmp_288_reg_11774.read() & sel_tmp45_fu_9254_p2.read());
}

void mnist_fp8::thread_sel_tmp47_demorgan_fu_4603_p2() {
    sel_tmp47_demorgan_fu_4603_p2 = (sel_tmp32_demorgan_fu_4570_p2.read() | tmp_130_reg_10447.read());
}

void mnist_fp8::thread_sel_tmp47_fu_9268_p2() {
    sel_tmp47_fu_9268_p2 = (sel_tmp188_demorgan_fu_9264_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp8::thread_sel_tmp48_fu_9274_p2() {
    sel_tmp48_fu_9274_p2 = (tmp_281_reg_11762.read() & sel_tmp47_fu_9268_p2.read());
}

void mnist_fp8::thread_sel_tmp49_fu_9279_p2() {
    sel_tmp49_fu_9279_p2 = (tmp_296_fu_9216_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp8::thread_sel_tmp4_fu_3167_p2() {
    sel_tmp4_fu_3167_p2 = (icmp_reg_10093.read() & sel_tmp3_fu_3161_p2.read());
}

void mnist_fp8::thread_sel_tmp50_fu_9285_p2() {
    sel_tmp50_fu_9285_p2 = (sel_tmp48_fu_9274_p2.read() & sel_tmp49_fu_9279_p2.read());
}

void mnist_fp8::thread_sel_tmp51_fu_9291_p2() {
    sel_tmp51_fu_9291_p2 = (sel_tmp48_fu_9274_p2.read() & tmp_296_fu_9216_p2.read());
}

void mnist_fp8::thread_sel_tmp52_fu_9302_p2() {
    sel_tmp52_fu_9302_p2 = (sel_tmp203_demorgan_fu_9297_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp8::thread_sel_tmp53_fu_9308_p2() {
    sel_tmp53_fu_9308_p2 = (icmp9_reg_11786.read() & sel_tmp52_fu_9302_p2.read());
}

void mnist_fp8::thread_sel_tmp54_fu_8427_p2() {
    sel_tmp54_fu_8427_p2 = (tmp_286_reg_11507.read() ^ ap_const_lv1_1);
}

void mnist_fp8::thread_sel_tmp55_fu_8432_p2() {
    sel_tmp55_fu_8432_p2 = (tmp_311_reg_11552.read() & sel_tmp54_fu_8427_p2.read());
}

void mnist_fp8::thread_sel_tmp56_demorgan_fu_4719_p2() {
    sel_tmp56_demorgan_fu_4719_p2 = (tmp_172_reg_10436.read() | tmp_176_reg_10493.read());
}

void mnist_fp8::thread_sel_tmp56_fu_8441_p2() {
    sel_tmp56_fu_8441_p2 = (sel_tmp137_demorgan_fu_8437_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp8::thread_sel_tmp57_fu_8447_p2() {
    sel_tmp57_fu_8447_p2 = (tmp_308_reg_11540.read() & sel_tmp56_fu_8441_p2.read());
}

void mnist_fp8::thread_sel_tmp58_fu_8452_p2() {
    sel_tmp58_fu_8452_p2 = (tmp_317_fu_8389_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp8::thread_sel_tmp59_fu_8458_p2() {
    sel_tmp59_fu_8458_p2 = (sel_tmp57_fu_8447_p2.read() & sel_tmp58_fu_8452_p2.read());
}

void mnist_fp8::thread_sel_tmp5_fu_5387_p2() {
    sel_tmp5_fu_5387_p2 = (tmp_101_reg_10658.read() ^ ap_const_lv1_1);
}

void mnist_fp8::thread_sel_tmp60_fu_8464_p2() {
    sel_tmp60_fu_8464_p2 = (sel_tmp57_fu_8447_p2.read() & tmp_317_fu_8389_p2.read());
}

void mnist_fp8::thread_sel_tmp61_fu_8475_p2() {
    sel_tmp61_fu_8475_p2 = (sel_tmp152_demorgan_fu_8470_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp8::thread_sel_tmp62_fu_8481_p2() {
    sel_tmp62_fu_8481_p2 = (icmp11_reg_11564.read() & sel_tmp61_fu_8475_p2.read());
}

void mnist_fp8::thread_sel_tmp63_fu_8576_p2() {
    sel_tmp63_fu_8576_p2 = (tmp_336_reg_11529.read() ^ ap_const_lv1_1);
}

void mnist_fp8::thread_sel_tmp64_fu_8581_p2() {
    sel_tmp64_fu_8581_p2 = (tmp_340_reg_11586.read() & sel_tmp63_fu_8576_p2.read());
}

void mnist_fp8::thread_sel_tmp65_fu_8590_p2() {
    sel_tmp65_fu_8590_p2 = (sel_tmp161_demorgan_fu_8586_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp8::thread_sel_tmp66_fu_8596_p2() {
    sel_tmp66_fu_8596_p2 = (tmp_337_reg_11574.read() & sel_tmp65_fu_8590_p2.read());
}

void mnist_fp8::thread_sel_tmp67_fu_8601_p2() {
    sel_tmp67_fu_8601_p2 = (tmp_342_fu_8538_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp8::thread_sel_tmp68_fu_8607_p2() {
    sel_tmp68_fu_8607_p2 = (sel_tmp66_fu_8596_p2.read() & sel_tmp67_fu_8601_p2.read());
}

void mnist_fp8::thread_sel_tmp69_fu_8613_p2() {
    sel_tmp69_fu_8613_p2 = (sel_tmp66_fu_8596_p2.read() & tmp_342_fu_8538_p2.read());
}

void mnist_fp8::thread_sel_tmp6_demorgan_fu_3123_p2() {
    sel_tmp6_demorgan_fu_3123_p2 = (tmp_39_reg_10058.read() | tmp_52_reg_10081.read());
}

void mnist_fp8::thread_sel_tmp6_fu_3127_p2() {
    sel_tmp6_fu_3127_p2 = (sel_tmp6_demorgan_fu_3123_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp8::thread_sel_tmp70_fu_8624_p2() {
    sel_tmp70_fu_8624_p2 = (sel_tmp176_demorgan_fu_8619_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp8::thread_sel_tmp71_demorgan_fu_4752_p2() {
    sel_tmp71_demorgan_fu_4752_p2 = (sel_tmp56_demorgan_fu_4719_p2.read() | tmp_173_reg_10481.read());
}

void mnist_fp8::thread_sel_tmp71_fu_8630_p2() {
    sel_tmp71_fu_8630_p2 = (icmp12_reg_11598.read() & sel_tmp70_fu_8624_p2.read());
}

void mnist_fp8::thread_sel_tmp7_fu_3133_p2() {
    sel_tmp7_fu_3133_p2 = (tmp_43_reg_10069.read() & sel_tmp6_fu_3127_p2.read());
}

void mnist_fp8::thread_sel_tmp83_demorgan_fu_5397_p2() {
    sel_tmp83_demorgan_fu_5397_p2 = (tmp_101_reg_10658.read() | tmp_126_reg_10681.read());
}

void mnist_fp8::thread_sel_tmp8_fu_3138_p2() {
    sel_tmp8_fu_3138_p2 = (tmp_54_fu_3075_p2.read() ^ ap_const_lv1_1);
}

void mnist_fp8::thread_sel_tmp98_demorgan_fu_5430_p2() {
    sel_tmp98_demorgan_fu_5430_p2 = (sel_tmp83_demorgan_fu_5397_p2.read() | tmp_123_reg_10669.read());
}

void mnist_fp8::thread_sel_tmp9_fu_3144_p2() {
    sel_tmp9_fu_3144_p2 = (sel_tmp7_fu_3133_p2.read() & sel_tmp8_fu_3138_p2.read());
}

void mnist_fp8::thread_sel_tmp_fu_3150_p2() {
    sel_tmp_fu_3150_p2 = (sel_tmp7_fu_3133_p2.read() & tmp_54_fu_3075_p2.read());
}

void mnist_fp8::thread_sext1_cast_fu_3509_p1() {
    sext1_cast_fu_3509_p1 = esl_sext<28,13>(r_V_17_tr_fu_3504_p2.read());
}

void mnist_fp8::thread_sext3_cast_fu_6273_p1() {
    sext3_cast_fu_6273_p1 = esl_sext<24,11>(r_V_31_tr_fu_6268_p2.read());
}

void mnist_fp8::thread_sext5_cast_fu_7439_p1() {
    sext5_cast_fu_7439_p1 = esl_sext<26,12>(r_V_85_tr_fu_7434_p2.read());
}

void mnist_fp8::thread_sh_amt_1_cast_fu_5346_p1() {
    sh_amt_1_cast_fu_5346_p1 = esl_sext<32,12>(sh_amt_1_reg_10675.read());
}

void mnist_fp8::thread_sh_amt_1_fu_5312_p3() {
    sh_amt_1_fu_5312_p3 = (!tmp_123_fu_5294_p2.read()[0].is_01())? sc_lv<12>(): ((tmp_123_fu_5294_p2.read()[0].to_bool())? tmp_124_fu_5300_p2.read(): tmp_125_fu_5306_p2.read());
}

void mnist_fp8::thread_sh_amt_2_cast_fu_4519_p1() {
    sh_amt_2_cast_fu_4519_p1 = esl_sext<32,12>(sh_amt_2_reg_10453.read());
}

void mnist_fp8::thread_sh_amt_2_fu_4400_p3() {
    sh_amt_2_fu_4400_p3 = (!tmp_130_fu_4382_p2.read()[0].is_01())? sc_lv<12>(): ((tmp_130_fu_4382_p2.read()[0].to_bool())? tmp_131_fu_4388_p2.read(): tmp_132_fu_4394_p2.read());
}

void mnist_fp8::thread_sh_amt_3_cast_fu_4668_p1() {
    sh_amt_3_cast_fu_4668_p1 = esl_sext<32,12>(sh_amt_3_reg_10487.read());
}

void mnist_fp8::thread_sh_amt_3_fu_4485_p3() {
    sh_amt_3_fu_4485_p3 = (!tmp_173_fu_4467_p2.read()[0].is_01())? sc_lv<12>(): ((tmp_173_fu_4467_p2.read()[0].to_bool())? tmp_174_fu_4473_p2.read(): tmp_175_fu_4479_p2.read());
}

void mnist_fp8::thread_sh_amt_4_cast_fu_7075_p1() {
    sh_amt_4_cast_fu_7075_p1 = esl_sext<32,12>(sh_amt_4_reg_11182.read());
}

void mnist_fp8::thread_sh_amt_4_fu_7041_p3() {
    sh_amt_4_fu_7041_p3 = (!tmp_232_fu_7023_p2.read()[0].is_01())? sc_lv<12>(): ((tmp_232_fu_7023_p2.read()[0].to_bool())? tmp_233_fu_7029_p2.read(): tmp_234_fu_7035_p2.read());
}

void mnist_fp8::thread_sh_amt_5_cast_fu_9213_p1() {
    sh_amt_5_cast_fu_9213_p1 = esl_sext<32,12>(sh_amt_5_reg_11768.read());
}

void mnist_fp8::thread_sh_amt_5_fu_9179_p3() {
    sh_amt_5_fu_9179_p3 = (!tmp_281_fu_9161_p2.read()[0].is_01())? sc_lv<12>(): ((tmp_281_fu_9161_p2.read()[0].to_bool())? tmp_285_fu_9167_p2.read(): tmp_287_fu_9173_p2.read());
}

void mnist_fp8::thread_sh_amt_6_cast_fu_8386_p1() {
    sh_amt_6_cast_fu_8386_p1 = esl_sext<32,12>(sh_amt_6_reg_11546.read());
}

void mnist_fp8::thread_sh_amt_6_fu_8267_p3() {
    sh_amt_6_fu_8267_p3 = (!tmp_308_fu_8249_p2.read()[0].is_01())? sc_lv<12>(): ((tmp_308_fu_8249_p2.read()[0].to_bool())? tmp_309_fu_8255_p2.read(): tmp_310_fu_8261_p2.read());
}

void mnist_fp8::thread_sh_amt_7_cast_fu_8535_p1() {
    sh_amt_7_cast_fu_8535_p1 = esl_sext<32,12>(sh_amt_7_reg_11580.read());
}

void mnist_fp8::thread_sh_amt_7_fu_8352_p3() {
    sh_amt_7_fu_8352_p3 = (!tmp_337_fu_8334_p2.read()[0].is_01())? sc_lv<12>(): ((tmp_337_fu_8334_p2.read()[0].to_bool())? tmp_338_fu_8340_p2.read(): tmp_339_fu_8346_p2.read());
}

void mnist_fp8::thread_sh_amt_cast_fu_3072_p1() {
    sh_amt_cast_fu_3072_p1 = esl_sext<32,12>(sh_amt_reg_10075.read());
}

void mnist_fp8::thread_sh_amt_fu_3038_p3() {
    sh_amt_fu_3038_p3 = (!tmp_43_fu_3020_p2.read()[0].is_01())? sc_lv<12>(): ((tmp_43_fu_3020_p2.read()[0].to_bool())? tmp_45_fu_3026_p2.read(): tmp_46_fu_3032_p2.read());
}

void mnist_fp8::thread_storemerge1_fu_8407_p3() {
    storemerge1_fu_8407_p3 = (!isneg_2_reg_11491.read()[0].is_01())? sc_lv<8>(): ((isneg_2_reg_11491.read()[0].to_bool())? ap_const_lv8_FF: ap_const_lv8_0);
}

void mnist_fp8::thread_storemerge2_fu_5367_p3() {
    storemerge2_fu_5367_p3 = (!tmp_327_reg_10642.read()[0].is_01())? sc_lv<8>(): ((tmp_327_reg_10642.read()[0].to_bool())? ap_const_lv8_FF: ap_const_lv8_0);
}

void mnist_fp8::thread_storemerge3_fu_8556_p3() {
    storemerge3_fu_8556_p3 = (!isneg_3_reg_11513.read()[0].is_01())? sc_lv<8>(): ((isneg_3_reg_11513.read()[0].to_bool())? ap_const_lv8_FF: ap_const_lv8_0);
}

void mnist_fp8::thread_storemerge4_fu_4540_p3() {
    storemerge4_fu_4540_p3 = (!isneg_reg_10398.read()[0].is_01())? sc_lv<8>(): ((isneg_reg_10398.read()[0].to_bool())? ap_const_lv8_FF: ap_const_lv8_0);
}

void mnist_fp8::thread_storemerge5_fu_9234_p3() {
    storemerge5_fu_9234_p3 = (!tmp_520_reg_11735.read()[0].is_01())? sc_lv<8>(): ((tmp_520_reg_11735.read()[0].to_bool())? ap_const_lv8_FF: ap_const_lv8_0);
}

void mnist_fp8::thread_storemerge6_fu_4689_p3() {
    storemerge6_fu_4689_p3 = (!isneg_1_reg_10420.read()[0].is_01())? sc_lv<8>(): ((isneg_1_reg_10420.read()[0].to_bool())? ap_const_lv8_FF: ap_const_lv8_0);
}

void mnist_fp8::thread_storemerge8_fu_7096_p3() {
    storemerge8_fu_7096_p3 = (!tmp_455_reg_11149.read()[0].is_01())? sc_lv<8>(): ((tmp_455_reg_11149.read()[0].to_bool())? ap_const_lv8_FF: ap_const_lv8_0);
}

void mnist_fp8::thread_storemerge_fu_3093_p3() {
    storemerge_fu_3093_p3 = (!tmp_142_reg_10042.read()[0].is_01())? sc_lv<8>(): ((tmp_142_reg_10042.read()[0].to_bool())? ap_const_lv8_FF: ap_const_lv8_0);
}

void mnist_fp8::thread_stream_in_TDATA_blk_n() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state2.read()) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage0.read()) && 
          esl_seteq<1,1,1>(ap_enable_reg_pp1_iter1.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_block_pp1_stage0.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, tmp_2_reg_9782.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, brmerge_reg_9791.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp2_stage0.read()) && 
          esl_seteq<1,1,1>(ap_enable_reg_pp2_iter1.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_block_pp2_stage0.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, eol_2_reg_898.read())))) {
        stream_in_TDATA_blk_n = stream_in_V_data_V_0_state.read()[0];
    } else {
        stream_in_TDATA_blk_n = ap_const_logic_1;
    }
}

void mnist_fp8::thread_stream_in_TREADY() {
    stream_in_TREADY = stream_in_V_dest_V_0_state.read()[1];
}

void mnist_fp8::thread_stream_in_V_data_V_0_ack_in() {
    stream_in_V_data_V_0_ack_in = stream_in_V_data_V_0_state.read()[1];
}

void mnist_fp8::thread_stream_in_V_data_V_0_ack_out() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state2.read()) && 
          esl_seteq<1,1,1>(stream_in_V_data_V_0_vld_out.read(), ap_const_logic_1)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp2_stage0.read()) && 
          esl_seteq<1,1,1>(ap_enable_reg_pp2_iter1.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, eol_2_reg_898.read()) && 
          esl_seteq<1,1,1>(ap_block_pp2_stage0_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage0.read()) && 
          esl_seteq<1,1,1>(ap_enable_reg_pp1_iter1.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op348_read_state6.read()) && 
          esl_seteq<1,1,1>(ap_block_pp1_stage0_11001.read(), ap_const_boolean_0)))) {
        stream_in_V_data_V_0_ack_out = ap_const_logic_1;
    } else {
        stream_in_V_data_V_0_ack_out = ap_const_logic_0;
    }
}

void mnist_fp8::thread_stream_in_V_data_V_0_data_out() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, stream_in_V_data_V_0_sel.read())) {
        stream_in_V_data_V_0_data_out = stream_in_V_data_V_0_payload_B.read();
    } else {
        stream_in_V_data_V_0_data_out = stream_in_V_data_V_0_payload_A.read();
    }
}

void mnist_fp8::thread_stream_in_V_data_V_0_load_A() {
    stream_in_V_data_V_0_load_A = (stream_in_V_data_V_0_state_cmp_full.read() & ~stream_in_V_data_V_0_sel_wr.read());
}

void mnist_fp8::thread_stream_in_V_data_V_0_load_B() {
    stream_in_V_data_V_0_load_B = (stream_in_V_data_V_0_sel_wr.read() & stream_in_V_data_V_0_state_cmp_full.read());
}

void mnist_fp8::thread_stream_in_V_data_V_0_sel() {
    stream_in_V_data_V_0_sel = stream_in_V_data_V_0_sel_rd.read();
}

void mnist_fp8::thread_stream_in_V_data_V_0_state_cmp_full() {
    stream_in_V_data_V_0_state_cmp_full =  (sc_logic) ((!stream_in_V_data_V_0_state.read().is_01() || !ap_const_lv2_1.is_01())? sc_lv<1>(): sc_lv<1>(stream_in_V_data_V_0_state.read() != ap_const_lv2_1))[0];
}

void mnist_fp8::thread_stream_in_V_data_V_0_vld_in() {
    stream_in_V_data_V_0_vld_in = stream_in_TVALID.read();
}

void mnist_fp8::thread_stream_in_V_data_V_0_vld_out() {
    stream_in_V_data_V_0_vld_out = stream_in_V_data_V_0_state.read()[0];
}

void mnist_fp8::thread_stream_in_V_dest_V_0_ack_out() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state2.read()) && 
          esl_seteq<1,1,1>(stream_in_V_data_V_0_vld_out.read(), ap_const_logic_1)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp2_stage0.read()) && 
          esl_seteq<1,1,1>(ap_enable_reg_pp2_iter1.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, eol_2_reg_898.read()) && 
          esl_seteq<1,1,1>(ap_block_pp2_stage0_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage0.read()) && 
          esl_seteq<1,1,1>(ap_enable_reg_pp1_iter1.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op348_read_state6.read()) && 
          esl_seteq<1,1,1>(ap_block_pp1_stage0_11001.read(), ap_const_boolean_0)))) {
        stream_in_V_dest_V_0_ack_out = ap_const_logic_1;
    } else {
        stream_in_V_dest_V_0_ack_out = ap_const_logic_0;
    }
}

void mnist_fp8::thread_stream_in_V_dest_V_0_vld_in() {
    stream_in_V_dest_V_0_vld_in = stream_in_TVALID.read();
}

void mnist_fp8::thread_stream_in_V_last_V_0_ack_in() {
    stream_in_V_last_V_0_ack_in = stream_in_V_last_V_0_state.read()[1];
}

void mnist_fp8::thread_stream_in_V_last_V_0_ack_out() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state2.read()) && 
          esl_seteq<1,1,1>(stream_in_V_data_V_0_vld_out.read(), ap_const_logic_1)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp2_stage0.read()) && 
          esl_seteq<1,1,1>(ap_enable_reg_pp2_iter1.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, eol_2_reg_898.read()) && 
          esl_seteq<1,1,1>(ap_block_pp2_stage0_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage0.read()) && 
          esl_seteq<1,1,1>(ap_enable_reg_pp1_iter1.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op348_read_state6.read()) && 
          esl_seteq<1,1,1>(ap_block_pp1_stage0_11001.read(), ap_const_boolean_0)))) {
        stream_in_V_last_V_0_ack_out = ap_const_logic_1;
    } else {
        stream_in_V_last_V_0_ack_out = ap_const_logic_0;
    }
}

void mnist_fp8::thread_stream_in_V_last_V_0_data_out() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, stream_in_V_last_V_0_sel.read())) {
        stream_in_V_last_V_0_data_out = stream_in_V_last_V_0_payload_B.read();
    } else {
        stream_in_V_last_V_0_data_out = stream_in_V_last_V_0_payload_A.read();
    }
}

void mnist_fp8::thread_stream_in_V_last_V_0_load_A() {
    stream_in_V_last_V_0_load_A = (stream_in_V_last_V_0_state_cmp_full.read() & ~stream_in_V_last_V_0_sel_wr.read());
}

void mnist_fp8::thread_stream_in_V_last_V_0_load_B() {
    stream_in_V_last_V_0_load_B = (stream_in_V_last_V_0_sel_wr.read() & stream_in_V_last_V_0_state_cmp_full.read());
}

void mnist_fp8::thread_stream_in_V_last_V_0_sel() {
    stream_in_V_last_V_0_sel = stream_in_V_last_V_0_sel_rd.read();
}

void mnist_fp8::thread_stream_in_V_last_V_0_state_cmp_full() {
    stream_in_V_last_V_0_state_cmp_full =  (sc_logic) ((!stream_in_V_last_V_0_state.read().is_01() || !ap_const_lv2_1.is_01())? sc_lv<1>(): sc_lv<1>(stream_in_V_last_V_0_state.read() != ap_const_lv2_1))[0];
}

void mnist_fp8::thread_stream_in_V_last_V_0_vld_in() {
    stream_in_V_last_V_0_vld_in = stream_in_TVALID.read();
}

void mnist_fp8::thread_stream_in_V_last_V_0_vld_out() {
    stream_in_V_last_V_0_vld_out = stream_in_V_last_V_0_state.read()[0];
}

void mnist_fp8::thread_stream_in_V_user_V_0_ack_in() {
    stream_in_V_user_V_0_ack_in = stream_in_V_user_V_0_state.read()[1];
}

void mnist_fp8::thread_stream_in_V_user_V_0_ack_out() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state2.read()) && 
          esl_seteq<1,1,1>(stream_in_V_data_V_0_vld_out.read(), ap_const_logic_1)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp2_stage0.read()) && 
          esl_seteq<1,1,1>(ap_enable_reg_pp2_iter1.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, eol_2_reg_898.read()) && 
          esl_seteq<1,1,1>(ap_block_pp2_stage0_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage0.read()) && 
          esl_seteq<1,1,1>(ap_enable_reg_pp1_iter1.read(), ap_const_logic_1) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op348_read_state6.read()) && 
          esl_seteq<1,1,1>(ap_block_pp1_stage0_11001.read(), ap_const_boolean_0)))) {
        stream_in_V_user_V_0_ack_out = ap_const_logic_1;
    } else {
        stream_in_V_user_V_0_ack_out = ap_const_logic_0;
    }
}

void mnist_fp8::thread_stream_in_V_user_V_0_data_out() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, stream_in_V_user_V_0_sel.read())) {
        stream_in_V_user_V_0_data_out = stream_in_V_user_V_0_payload_B.read();
    } else {
        stream_in_V_user_V_0_data_out = stream_in_V_user_V_0_payload_A.read();
    }
}

void mnist_fp8::thread_stream_in_V_user_V_0_load_A() {
    stream_in_V_user_V_0_load_A = (stream_in_V_user_V_0_state_cmp_full.read() & ~stream_in_V_user_V_0_sel_wr.read());
}

void mnist_fp8::thread_stream_in_V_user_V_0_load_B() {
    stream_in_V_user_V_0_load_B = (stream_in_V_user_V_0_sel_wr.read() & stream_in_V_user_V_0_state_cmp_full.read());
}

void mnist_fp8::thread_stream_in_V_user_V_0_sel() {
    stream_in_V_user_V_0_sel = stream_in_V_user_V_0_sel_rd.read();
}

void mnist_fp8::thread_stream_in_V_user_V_0_state_cmp_full() {
    stream_in_V_user_V_0_state_cmp_full =  (sc_logic) ((!stream_in_V_user_V_0_state.read().is_01() || !ap_const_lv2_1.is_01())? sc_lv<1>(): sc_lv<1>(stream_in_V_user_V_0_state.read() != ap_const_lv2_1))[0];
}

void mnist_fp8::thread_stream_in_V_user_V_0_vld_in() {
    stream_in_V_user_V_0_vld_in = stream_in_TVALID.read();
}

void mnist_fp8::thread_stream_in_V_user_V_0_vld_out() {
    stream_in_V_user_V_0_vld_out = stream_in_V_user_V_0_state.read()[0];
}

void mnist_fp8::thread_tmp136_fu_6169_p2() {
    tmp136_fu_6169_p2 = (tmp_56_fu_6104_p2.read() & tmp_57_fu_6110_p2.read());
}

void mnist_fp8::thread_tmp137_fu_6204_p2() {
    tmp137_fu_6204_p2 = (tmp_70_fu_6192_p2.read() & tmp_71_fu_6198_p2.read());
}

void mnist_fp8::thread_tmp138_fu_6175_p2() {
    tmp138_fu_6175_p2 = (!rhs_V_4_cast_fu_6165_p1.read().is_01() || !phi_mul160_cast_reg_10853.read().is_01())? sc_lv<11>(): (sc_bigint<11>(rhs_V_4_cast_fu_6165_p1.read()) + sc_biguint<11>(phi_mul160_cast_reg_10853.read()));
}

void mnist_fp8::thread_tmp139_fu_6259_p2() {
    tmp139_fu_6259_p2 = (!ap_const_lv6_31.is_01() || !lhs_V_18_cast_fu_6255_p1.read().is_01())? sc_lv<6>(): (sc_bigint<6>(ap_const_lv6_31) + sc_bigint<6>(lhs_V_18_cast_fu_6255_p1.read()));
}

void mnist_fp8::thread_tmp1_fu_2258_p2() {
    tmp1_fu_2258_p2 = (!r_V_1_fu_2252_p2.read().is_01() || !ap_const_lv11_7E3.is_01())? sc_lv<11>(): (sc_biguint<11>(r_V_1_fu_2252_p2.read()) + sc_bigint<11>(ap_const_lv11_7E3));
}

void mnist_fp8::thread_tmp244_fu_7335_p2() {
    tmp244_fu_7335_p2 = (tmp_85_fu_7270_p2.read() & tmp_86_fu_7276_p2.read());
}

void mnist_fp8::thread_tmp245_fu_7370_p2() {
    tmp245_fu_7370_p2 = (tmp_117_fu_7358_p2.read() & tmp_118_fu_7364_p2.read());
}

void mnist_fp8::thread_tmp246_fu_7341_p2() {
    tmp246_fu_7341_p2 = (!rhs_V_10_cast_fu_7331_p1.read().is_01() || !phi_mul162_cast_reg_11210.read().is_01())? sc_lv<12>(): (sc_bigint<12>(rhs_V_10_cast_fu_7331_p1.read()) + sc_biguint<12>(phi_mul162_cast_reg_11210.read()));
}

void mnist_fp8::thread_tmp247_fu_7425_p2() {
    tmp247_fu_7425_p2 = (!ap_const_lv6_31.is_01() || !lhs_V_24_cast_fu_7421_p1.read().is_01())? sc_lv<6>(): (sc_bigint<6>(ap_const_lv6_31) + sc_bigint<6>(lhs_V_24_cast_fu_7421_p1.read()));
}

void mnist_fp8::thread_tmp322_cast_fu_3501_p1() {
    tmp322_cast_fu_3501_p1 = esl_sext<13,7>(tmp33_reg_10165.read());
}

void mnist_fp8::thread_tmp323_cast_cast_fu_3891_p3() {
    tmp323_cast_cast_fu_3891_p3 = (!tmp_76_reg_10275.read()[0].is_01())? sc_lv<8>(): ((tmp_76_reg_10275.read()[0].to_bool())? ap_const_lv8_7A: ap_const_lv8_79);
}

void mnist_fp8::thread_tmp324_cast_cast_fu_5009_p3() {
    tmp324_cast_cast_fu_5009_p3 = (!tmp_112_reg_10571.read()[0].is_01())? sc_lv<8>(): ((tmp_112_reg_10571.read()[0].to_bool())? ap_const_lv8_7A: ap_const_lv8_79);
}

void mnist_fp8::thread_tmp325_cast_cast_fu_5926_p3() {
    tmp325_cast_cast_fu_5926_p3 = (!tmp_209_reg_10836.read()[0].is_01())? sc_lv<8>(): ((tmp_209_reg_10836.read()[0].to_bool())? ap_const_lv8_7A: ap_const_lv8_79);
}

void mnist_fp8::thread_tmp32_V_10_fu_7761_p2() {
    tmp32_V_10_fu_7761_p2 = (!tmp_195_fu_7755_p2.read().is_01())? sc_lv<32>(): tmp32_V_9_fu_7752_p1.read() << (unsigned short)tmp_195_fu_7755_p2.read().to_uint();
}

void mnist_fp8::thread_tmp32_V_11_fu_7786_p1() {
    tmp32_V_11_fu_7786_p1 = esl_zext<32,8>(p_Result_22_fu_7781_p2.read());
}

void mnist_fp8::thread_tmp32_V_12_fu_3860_p3() {
    tmp32_V_12_fu_3860_p3 = (!icmp1_fu_3816_p2.read()[0].is_01())? sc_lv<32>(): ((icmp1_fu_3816_p2.read()[0].to_bool())? tmp32_V_1_fu_3831_p2.read(): tmp32_V_2_fu_3856_p1.read());
}

void mnist_fp8::thread_tmp32_V_13_fu_8798_p1() {
    tmp32_V_13_fu_8798_p1 = esl_zext<32,8>(p_Val2_33_reg_11628.read());
}

void mnist_fp8::thread_tmp32_V_14_fu_8807_p2() {
    tmp32_V_14_fu_8807_p2 = (!tmp_272_fu_8801_p2.read().is_01())? sc_lv<32>(): tmp32_V_13_fu_8798_p1.read() << (unsigned short)tmp_272_fu_8801_p2.read().to_uint();
}

void mnist_fp8::thread_tmp32_V_15_fu_8832_p1() {
    tmp32_V_15_fu_8832_p1 = esl_zext<32,8>(p_Result_29_fu_8827_p2.read());
}

void mnist_fp8::thread_tmp32_V_18_fu_4969_p3() {
    tmp32_V_18_fu_4969_p3 = (!icmp3_fu_4925_p2.read()[0].is_01())? sc_lv<32>(): ((icmp3_fu_4925_p2.read()[0].to_bool())? tmp32_V_6_fu_4940_p2.read(): tmp32_V_7_fu_4965_p1.read());
}

void mnist_fp8::thread_tmp32_V_1_fu_3831_p2() {
    tmp32_V_1_fu_3831_p2 = (!tmp_67_fu_3825_p2.read().is_01())? sc_lv<32>(): tmp32_V_fu_3822_p1.read() << (unsigned short)tmp_67_fu_3825_p2.read().to_uint();
}

void mnist_fp8::thread_tmp32_V_21_fu_7790_p3() {
    tmp32_V_21_fu_7790_p3 = (!icmp8_fu_7746_p2.read()[0].is_01())? sc_lv<32>(): ((icmp8_fu_7746_p2.read()[0].to_bool())? tmp32_V_10_fu_7761_p2.read(): tmp32_V_11_fu_7786_p1.read());
}

void mnist_fp8::thread_tmp32_V_24_fu_8836_p3() {
    tmp32_V_24_fu_8836_p3 = (!icmp10_fu_8792_p2.read()[0].is_01())? sc_lv<32>(): ((icmp10_fu_8792_p2.read()[0].to_bool())? tmp32_V_14_fu_8807_p2.read(): tmp32_V_15_fu_8832_p1.read());
}

void mnist_fp8::thread_tmp32_V_27_fu_3868_p1() {
    tmp32_V_27_fu_3868_p1 = grp_fu_2058_p1.read();
}

void mnist_fp8::thread_tmp32_V_28_fu_4986_p1() {
    tmp32_V_28_fu_4986_p1 = grp_fu_2058_p1.read();
}

void mnist_fp8::thread_tmp32_V_29_fu_7798_p1() {
    tmp32_V_29_fu_7798_p1 = grp_fu_2058_p1.read();
}

void mnist_fp8::thread_tmp32_V_2_fu_3856_p1() {
    tmp32_V_2_fu_3856_p1 = esl_zext<32,8>(p_Result_s_fu_3851_p2.read());
}

void mnist_fp8::thread_tmp32_V_30_fu_8853_p1() {
    tmp32_V_30_fu_8853_p1 = grp_fu_2058_p1.read();
}

void mnist_fp8::thread_tmp32_V_3_fu_5866_p2() {
    tmp32_V_3_fu_5866_p2 = (!tmp_205_fu_5860_p2.read().is_01())? sc_lv<32>(): tmp32_V_s_fu_5857_p1.read() << (unsigned short)tmp_205_fu_5860_p2.read().to_uint();
}

void mnist_fp8::thread_tmp32_V_4_fu_5891_p1() {
    tmp32_V_4_fu_5891_p1 = esl_zext<32,8>(tmp_441_fu_5886_p2.read());
}

void mnist_fp8::thread_tmp32_V_5_fu_4931_p1() {
    tmp32_V_5_fu_4931_p1 = esl_zext<32,8>(p_Val2_12_reg_10535.read());
}

void mnist_fp8::thread_tmp32_V_6_fu_4940_p2() {
    tmp32_V_6_fu_4940_p2 = (!tmp_102_fu_4934_p2.read().is_01())? sc_lv<32>(): tmp32_V_5_fu_4931_p1.read() << (unsigned short)tmp_102_fu_4934_p2.read().to_uint();
}

void mnist_fp8::thread_tmp32_V_7_fu_4965_p1() {
    tmp32_V_7_fu_4965_p1 = esl_zext<32,8>(p_Result_8_fu_4960_p2.read());
}

void mnist_fp8::thread_tmp32_V_8_fu_5903_p1() {
    tmp32_V_8_fu_5903_p1 = grp_fu_2058_p1.read();
}

void mnist_fp8::thread_tmp32_V_9_fu_7752_p1() {
    tmp32_V_9_fu_7752_p1 = esl_zext<32,8>(p_Val2_30_reg_11352.read());
}

void mnist_fp8::thread_tmp32_V_fu_3822_p1() {
    tmp32_V_fu_3822_p1 = esl_zext<32,8>(p_Val2_6_reg_10244.read());
}

void mnist_fp8::thread_tmp32_V_s_fu_5857_p1() {
    tmp32_V_s_fu_5857_p1 = esl_zext<32,8>(tmp_V_2_reg_10805.read());
}

void mnist_fp8::thread_tmp32_fu_3356_p2() {
    tmp32_fu_3356_p2 = (!rhs_V_5_cast_fu_3352_p1.read().is_01() || !phi_mul_cast_reg_10103.read().is_01())? sc_lv<13>(): (sc_bigint<13>(rhs_V_5_cast_fu_3352_p1.read()) + sc_biguint<13>(phi_mul_cast_reg_10103.read()));
}

void mnist_fp8::thread_tmp330_cast_fu_6265_p1() {
    tmp330_cast_fu_6265_p1 = esl_sext<11,6>(tmp139_reg_10916.read());
}

void mnist_fp8::thread_tmp335_cast_fu_7431_p1() {
    tmp335_cast_fu_7431_p1 = esl_sext<12,6>(tmp247_reg_11273.read());
}

void mnist_fp8::thread_tmp336_cast_cast_fu_7821_p3() {
    tmp336_cast_cast_fu_7821_p3 = (!tmp_230_reg_11383.read()[0].is_01())? sc_lv<8>(): ((tmp_230_reg_11383.read()[0].to_bool())? ap_const_lv8_7A: ap_const_lv8_79);
}

void mnist_fp8::thread_tmp337_cast_cast_fu_8876_p3() {
    tmp337_cast_cast_fu_8876_p3 = (!tmp_275_reg_11664.read()[0].is_01())? sc_lv<8>(): ((tmp_275_reg_11664.read()[0].to_bool())? ap_const_lv8_7A: ap_const_lv8_79);
}

void mnist_fp8::thread_tmp33_fu_3495_p2() {
    tmp33_fu_3495_p2 = (!ap_const_lv7_63.is_01() || !lhs_V_10_cast_fu_3491_p1.read().is_01())? sc_lv<7>(): (sc_bigint<7>(ap_const_lv7_63) + sc_bigint<7>(lhs_V_10_cast_fu_3491_p1.read()));
}

void mnist_fp8::thread_tmp_100_fu_5182_p4() {
    tmp_100_fu_5182_p4 = conv2_0_load_to_int_fu_5179_p1.read().range(30, 23);
}

void mnist_fp8::thread_tmp_101_fu_5255_p2() {
    tmp_101_fu_5255_p2 = (!tmp_326_fu_5229_p1.read().is_01() || !ap_const_lv63_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_326_fu_5229_p1.read() == ap_const_lv63_0);
}

void mnist_fp8::thread_tmp_102_fu_4934_p2() {
    tmp_102_fu_4934_p2 = (!ap_const_lv32_1F.is_01() || !msb_idx_3_cast_fu_4911_p1.read().is_01())? sc_lv<32>(): (sc_biguint<32>(ap_const_lv32_1F) - sc_biguint<32>(msb_idx_3_cast_fu_4911_p1.read()));
}

void mnist_fp8::thread_tmp_103_fu_2844_p2() {
    tmp_103_fu_2844_p2 = (!tmp_138_cast_reg_9984.read().is_01() || !tmp_18_cast_fu_2840_p1.read().is_01())? sc_lv<10>(): (sc_bigint<10>(tmp_138_cast_reg_9984.read()) + sc_biguint<10>(tmp_18_cast_fu_2840_p1.read()));
}

void mnist_fp8::thread_tmp_104_cast_fu_4249_p1() {
    tmp_104_cast_fu_4249_p1 = esl_zext<13,5>(r_V_10_fu_4243_p2.read());
}

void mnist_fp8::thread_tmp_104_fu_2849_p1() {
    tmp_104_fu_2849_p1 = tmp_103_fu_2844_p2.read().range(8-1, 0);
}

void mnist_fp8::thread_tmp_105_fu_2861_p3() {
    tmp_105_fu_2861_p3 = esl_concat<10,2>(tmp_103_fu_2844_p2.read(), ap_const_lv2_0);
}

void mnist_fp8::thread_tmp_106_fu_4349_p1() {
    tmp_106_fu_4349_p1 = esl_zext<12,11>(exp_tmp_V_reg_10404.read());
}

void mnist_fp8::thread_tmp_107_fu_2873_p2() {
    tmp_107_fu_2873_p2 = (!p_shl24_cast_fu_2853_p3.read().is_01() || !p_shl25_cast_fu_2869_p1.read().is_01())? sc_lv<13>(): (sc_biguint<13>(p_shl24_cast_fu_2853_p3.read()) - sc_bigint<13>(p_shl25_cast_fu_2869_p1.read()));
}

void mnist_fp8::thread_tmp_108_fu_4307_p2() {
    tmp_108_fu_4307_p2 = (!tmp_388_fu_4281_p1.read().is_01() || !ap_const_lv63_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_388_fu_4281_p1.read() == ap_const_lv63_0);
}

void mnist_fp8::thread_tmp_10_cast_fu_2474_p1() {
    tmp_10_cast_fu_2474_p1 = esl_zext<11,5>(tmp_8_reg_9840.read());
}

void mnist_fp8::thread_tmp_10_fu_2222_p3() {
    tmp_10_fu_2222_p3 = esl_concat<5,1>(p_s_reg_933.read(), ap_const_lv1_0);
}

void mnist_fp8::thread_tmp_110_cast_fu_6491_p1() {
    tmp_110_cast_fu_6491_p1 = esl_zext<12,5>(p_30_reg_1401.read());
}

void mnist_fp8::thread_tmp_110_fu_3951_p3() {
    tmp_110_fu_3951_p3 = esl_concat<3,2>(p_9_reg_1151.read(), ap_const_lv2_0);
}

void mnist_fp8::thread_tmp_111_cast_fu_7941_p1() {
    tmp_111_cast_fu_7941_p1 = esl_zext<10,4>(p_35_reg_1635.read());
}

void mnist_fp8::thread_tmp_111_fu_3967_p3() {
    tmp_111_fu_3967_p3 = esl_concat<3,5>(p_9_reg_1151.read(), ap_const_lv5_0);
}

void mnist_fp8::thread_tmp_112_fu_5000_p2() {
    tmp_112_fu_5000_p2 = (!p_Result_s_40_fu_4990_p4.read().is_01() || !ap_const_lv8_9E.is_01())? sc_lv<1>(): sc_lv<1>(p_Result_s_40_fu_4990_p4.read() != ap_const_lv8_9E);
}

void mnist_fp8::thread_tmp_113_fu_5208_p2() {
    tmp_113_fu_5208_p2 = (notrhs1_reg_10632.read() | notlhs1_reg_10627.read());
}

void mnist_fp8::thread_tmp_115_fu_5022_p3() {
    tmp_115_fu_5022_p3 = esl_concat<1,8>(is_neg_1_reg_10530.read(), p_Repl2_4_trunc_fu_5016_p2.read());
}

void mnist_fp8::thread_tmp_116_fu_3979_p2() {
    tmp_116_fu_3979_p2 = (!p_shl34_cast_fu_3975_p1.read().is_01() || !tmp_327_cast_fu_3963_p1.read().is_01())? sc_lv<9>(): (sc_biguint<9>(p_shl34_cast_fu_3975_p1.read()) - sc_biguint<9>(tmp_327_cast_fu_3963_p1.read()));
}

void mnist_fp8::thread_tmp_117_fu_7358_p2() {
    tmp_117_fu_7358_p2 = (!p_39_reg_1597.read().is_01() || !ap_const_lv5_0.is_01())? sc_lv<1>(): sc_lv<1>(p_39_reg_1597.read() != ap_const_lv5_0);
}

void mnist_fp8::thread_tmp_118_fu_7364_p2() {
    tmp_118_fu_7364_p2 = (!p_39_reg_1597.read().is_01() || !ap_const_lv5_F.is_01())? sc_lv<1>(): (sc_biguint<5>(p_39_reg_1597.read()) < sc_biguint<5>(ap_const_lv5_F));
}

void mnist_fp8::thread_tmp_119_cast_fu_6668_p1() {
    tmp_119_cast_fu_6668_p1 = esl_zext<12,4>(p_32_reg_1448.read());
}

void mnist_fp8::thread_tmp_119_fu_3287_p2() {
    tmp_119_fu_3287_p2 = (!lhs_V_cast_39_fu_3283_p1.read().is_01() || !tmp_298_cast_reg_10121.read().is_01())? sc_lv<10>(): (sc_biguint<10>(lhs_V_cast_39_fu_3283_p1.read()) + sc_bigint<10>(tmp_298_cast_reg_10121.read()));
}

void mnist_fp8::thread_tmp_11_cast_fu_2594_p1() {
    tmp_11_cast_fu_2594_p1 = esl_zext<10,5>(p_5_reg_983.read());
}

}

