############################################################
## This file is generated automatically by Vivado HLS.
## Please DO NOT edit it.
## Copyright (C) 1986-2018 Xilinx, Inc. All Rights Reserved.
############################################################
open_project center_image
set_top center_image
add_files center_image/src/center_image.cpp
add_files -tb center_image/tb/7_1920x1080.jpg -cflags "-Wno-unknown-pragmas -Wno-unknown-pragmas"
add_files -tb center_image/tb/center_image_tb.cpp -cflags "-Wno-unknown-pragmas -Wno-unknown-pragmas -Wno-unknown-pragmas -Wno-unknown-pragmas -Wno-unknown-pragmas -Wno-unknown-pragmas"
open_solution "axis"
set_part {xc7z020clg400-1}
create_clock -period 10 -name default
#source "./center_image/axis/directives.tcl"
csim_design
csynth_design
cosim_design -rtl vhdl
export_design -rtl vhdl -format ip_catalog
