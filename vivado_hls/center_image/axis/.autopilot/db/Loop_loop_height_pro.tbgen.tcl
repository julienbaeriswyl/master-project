set moduleName Loop_loop_height_pro
set isTaskLevelControl 1
set isCombinational 0
set isDatapathOnly 0
set isPipelined 0
set pipeline_type none
set FunctionProtocol ap_ctrl_hs
set isOneStateSeq 0
set ProfileFlag 0
set StallSigGenFlag 0
set isEnableWaveformDebug 1
set C_modelName {Loop_loop_height_pro}
set C_modelType { void 0 }
set C_modelArgList {
	{ input_data_data_stream_0_V int 8 regular {fifo 0 volatile }  }
	{ input_data_data_stream_1_V int 8 regular {fifo 0 volatile }  }
	{ input_data_data_stream_2_V int 8 regular {fifo 0 volatile }  }
	{ passthrough_data_stream_0_V int 8 regular {fifo 1 volatile }  }
	{ passthrough_data_stream_1_V int 8 regular {fifo 1 volatile }  }
	{ passthrough_data_stream_2_V int 8 regular {fifo 1 volatile }  }
	{ crop_data_data_stream_0_V int 8 regular {fifo 1 volatile }  }
	{ crop_data_data_stream_1_V int 8 regular {fifo 1 volatile }  }
	{ crop_data_data_stream_2_V int 8 regular {fifo 1 volatile }  }
}
set C_modelArgMapList {[ 
	{ "Name" : "input_data_data_stream_0_V", "interface" : "fifo", "bitwidth" : 8, "direction" : "READONLY"} , 
 	{ "Name" : "input_data_data_stream_1_V", "interface" : "fifo", "bitwidth" : 8, "direction" : "READONLY"} , 
 	{ "Name" : "input_data_data_stream_2_V", "interface" : "fifo", "bitwidth" : 8, "direction" : "READONLY"} , 
 	{ "Name" : "passthrough_data_stream_0_V", "interface" : "fifo", "bitwidth" : 8, "direction" : "WRITEONLY"} , 
 	{ "Name" : "passthrough_data_stream_1_V", "interface" : "fifo", "bitwidth" : 8, "direction" : "WRITEONLY"} , 
 	{ "Name" : "passthrough_data_stream_2_V", "interface" : "fifo", "bitwidth" : 8, "direction" : "WRITEONLY"} , 
 	{ "Name" : "crop_data_data_stream_0_V", "interface" : "fifo", "bitwidth" : 8, "direction" : "WRITEONLY"} , 
 	{ "Name" : "crop_data_data_stream_1_V", "interface" : "fifo", "bitwidth" : 8, "direction" : "WRITEONLY"} , 
 	{ "Name" : "crop_data_data_stream_2_V", "interface" : "fifo", "bitwidth" : 8, "direction" : "WRITEONLY"} ]}
# RTL Port declarations: 
set portNum 37
set portList { 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst sc_in sc_logic 1 reset -1 active_high_sync } 
	{ ap_start sc_in sc_logic 1 start -1 } 
	{ start_full_n sc_in sc_logic 1 signal -1 } 
	{ ap_done sc_out sc_logic 1 predone -1 } 
	{ ap_continue sc_in sc_logic 1 continue -1 } 
	{ ap_idle sc_out sc_logic 1 done -1 } 
	{ ap_ready sc_out sc_logic 1 ready -1 } 
	{ start_out sc_out sc_logic 1 signal -1 } 
	{ start_write sc_out sc_logic 1 signal -1 } 
	{ input_data_data_stream_0_V_dout sc_in sc_lv 8 signal 0 } 
	{ input_data_data_stream_0_V_empty_n sc_in sc_logic 1 signal 0 } 
	{ input_data_data_stream_0_V_read sc_out sc_logic 1 signal 0 } 
	{ input_data_data_stream_1_V_dout sc_in sc_lv 8 signal 1 } 
	{ input_data_data_stream_1_V_empty_n sc_in sc_logic 1 signal 1 } 
	{ input_data_data_stream_1_V_read sc_out sc_logic 1 signal 1 } 
	{ input_data_data_stream_2_V_dout sc_in sc_lv 8 signal 2 } 
	{ input_data_data_stream_2_V_empty_n sc_in sc_logic 1 signal 2 } 
	{ input_data_data_stream_2_V_read sc_out sc_logic 1 signal 2 } 
	{ passthrough_data_stream_0_V_din sc_out sc_lv 8 signal 3 } 
	{ passthrough_data_stream_0_V_full_n sc_in sc_logic 1 signal 3 } 
	{ passthrough_data_stream_0_V_write sc_out sc_logic 1 signal 3 } 
	{ passthrough_data_stream_1_V_din sc_out sc_lv 8 signal 4 } 
	{ passthrough_data_stream_1_V_full_n sc_in sc_logic 1 signal 4 } 
	{ passthrough_data_stream_1_V_write sc_out sc_logic 1 signal 4 } 
	{ passthrough_data_stream_2_V_din sc_out sc_lv 8 signal 5 } 
	{ passthrough_data_stream_2_V_full_n sc_in sc_logic 1 signal 5 } 
	{ passthrough_data_stream_2_V_write sc_out sc_logic 1 signal 5 } 
	{ crop_data_data_stream_0_V_din sc_out sc_lv 8 signal 6 } 
	{ crop_data_data_stream_0_V_full_n sc_in sc_logic 1 signal 6 } 
	{ crop_data_data_stream_0_V_write sc_out sc_logic 1 signal 6 } 
	{ crop_data_data_stream_1_V_din sc_out sc_lv 8 signal 7 } 
	{ crop_data_data_stream_1_V_full_n sc_in sc_logic 1 signal 7 } 
	{ crop_data_data_stream_1_V_write sc_out sc_logic 1 signal 7 } 
	{ crop_data_data_stream_2_V_din sc_out sc_lv 8 signal 8 } 
	{ crop_data_data_stream_2_V_full_n sc_in sc_logic 1 signal 8 } 
	{ crop_data_data_stream_2_V_write sc_out sc_logic 1 signal 8 } 
}
set NewPortList {[ 
	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst", "role": "default" }} , 
 	{ "name": "ap_start", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "start", "bundle":{"name": "ap_start", "role": "default" }} , 
 	{ "name": "start_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "start_full_n", "role": "default" }} , 
 	{ "name": "ap_done", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "predone", "bundle":{"name": "ap_done", "role": "default" }} , 
 	{ "name": "ap_continue", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "continue", "bundle":{"name": "ap_continue", "role": "default" }} , 
 	{ "name": "ap_idle", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "done", "bundle":{"name": "ap_idle", "role": "default" }} , 
 	{ "name": "ap_ready", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "ready", "bundle":{"name": "ap_ready", "role": "default" }} , 
 	{ "name": "start_out", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "start_out", "role": "default" }} , 
 	{ "name": "start_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "start_write", "role": "default" }} , 
 	{ "name": "input_data_data_stream_0_V_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "input_data_data_stream_0_V", "role": "dout" }} , 
 	{ "name": "input_data_data_stream_0_V_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "input_data_data_stream_0_V", "role": "empty_n" }} , 
 	{ "name": "input_data_data_stream_0_V_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "input_data_data_stream_0_V", "role": "read" }} , 
 	{ "name": "input_data_data_stream_1_V_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "input_data_data_stream_1_V", "role": "dout" }} , 
 	{ "name": "input_data_data_stream_1_V_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "input_data_data_stream_1_V", "role": "empty_n" }} , 
 	{ "name": "input_data_data_stream_1_V_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "input_data_data_stream_1_V", "role": "read" }} , 
 	{ "name": "input_data_data_stream_2_V_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "input_data_data_stream_2_V", "role": "dout" }} , 
 	{ "name": "input_data_data_stream_2_V_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "input_data_data_stream_2_V", "role": "empty_n" }} , 
 	{ "name": "input_data_data_stream_2_V_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "input_data_data_stream_2_V", "role": "read" }} , 
 	{ "name": "passthrough_data_stream_0_V_din", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "passthrough_data_stream_0_V", "role": "din" }} , 
 	{ "name": "passthrough_data_stream_0_V_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "passthrough_data_stream_0_V", "role": "full_n" }} , 
 	{ "name": "passthrough_data_stream_0_V_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "passthrough_data_stream_0_V", "role": "write" }} , 
 	{ "name": "passthrough_data_stream_1_V_din", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "passthrough_data_stream_1_V", "role": "din" }} , 
 	{ "name": "passthrough_data_stream_1_V_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "passthrough_data_stream_1_V", "role": "full_n" }} , 
 	{ "name": "passthrough_data_stream_1_V_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "passthrough_data_stream_1_V", "role": "write" }} , 
 	{ "name": "passthrough_data_stream_2_V_din", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "passthrough_data_stream_2_V", "role": "din" }} , 
 	{ "name": "passthrough_data_stream_2_V_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "passthrough_data_stream_2_V", "role": "full_n" }} , 
 	{ "name": "passthrough_data_stream_2_V_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "passthrough_data_stream_2_V", "role": "write" }} , 
 	{ "name": "crop_data_data_stream_0_V_din", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "crop_data_data_stream_0_V", "role": "din" }} , 
 	{ "name": "crop_data_data_stream_0_V_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "crop_data_data_stream_0_V", "role": "full_n" }} , 
 	{ "name": "crop_data_data_stream_0_V_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "crop_data_data_stream_0_V", "role": "write" }} , 
 	{ "name": "crop_data_data_stream_1_V_din", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "crop_data_data_stream_1_V", "role": "din" }} , 
 	{ "name": "crop_data_data_stream_1_V_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "crop_data_data_stream_1_V", "role": "full_n" }} , 
 	{ "name": "crop_data_data_stream_1_V_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "crop_data_data_stream_1_V", "role": "write" }} , 
 	{ "name": "crop_data_data_stream_2_V_din", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "crop_data_data_stream_2_V", "role": "din" }} , 
 	{ "name": "crop_data_data_stream_2_V_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "crop_data_data_stream_2_V", "role": "full_n" }} , 
 	{ "name": "crop_data_data_stream_2_V_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "crop_data_data_stream_2_V", "role": "write" }}  ]}

set RtlHierarchyInfo {[
	{"ID" : "0", "Level" : "0", "Path" : "`AUTOTB_DUT_INST", "Parent" : "",
		"CDFG" : "Loop_loop_height_pro",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "None", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "0",
		"VariableLatency" : "1", "ExactLatency" : "-1", "EstimateLatencyMin" : "2077921", "EstimateLatencyMax" : "2077921",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "0",
		"Port" : [
			{"Name" : "input_data_data_stream_0_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "input_data_data_stream_0_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "input_data_data_stream_1_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "input_data_data_stream_1_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "input_data_data_stream_2_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "input_data_data_stream_2_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "passthrough_data_stream_0_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "passthrough_data_stream_0_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "passthrough_data_stream_1_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "passthrough_data_stream_1_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "passthrough_data_stream_2_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "passthrough_data_stream_2_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "crop_data_data_stream_0_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "crop_data_data_stream_0_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "crop_data_data_stream_1_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "crop_data_data_stream_1_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "crop_data_data_stream_2_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "crop_data_data_stream_2_V_blk_n", "Type" : "RtlSignal"}]}]}]}


set ArgLastReadFirstWriteLatency {
	Loop_loop_height_pro {
		input_data_data_stream_0_V {Type I LastRead 4 FirstWrite -1}
		input_data_data_stream_1_V {Type I LastRead 4 FirstWrite -1}
		input_data_data_stream_2_V {Type I LastRead 4 FirstWrite -1}
		passthrough_data_stream_0_V {Type O LastRead -1 FirstWrite 2}
		passthrough_data_stream_1_V {Type O LastRead -1 FirstWrite 2}
		passthrough_data_stream_2_V {Type O LastRead -1 FirstWrite 2}
		crop_data_data_stream_0_V {Type O LastRead -1 FirstWrite 3}
		crop_data_data_stream_1_V {Type O LastRead -1 FirstWrite 3}
		crop_data_data_stream_2_V {Type O LastRead -1 FirstWrite 3}}}

set hasDtUnsupportedChannel 0

set PerformanceInfo {[
	{"Name" : "Latency", "Min" : "2077921", "Max" : "2077921"}
	, {"Name" : "Interval", "Min" : "2077921", "Max" : "2077921"}
]}

set PipelineEnableSignalInfo {[
]}

set Spec2ImplPortList { 
	input_data_data_stream_0_V { ap_fifo {  { input_data_data_stream_0_V_dout fifo_data 0 8 }  { input_data_data_stream_0_V_empty_n fifo_status 0 1 }  { input_data_data_stream_0_V_read fifo_update 1 1 } } }
	input_data_data_stream_1_V { ap_fifo {  { input_data_data_stream_1_V_dout fifo_data 0 8 }  { input_data_data_stream_1_V_empty_n fifo_status 0 1 }  { input_data_data_stream_1_V_read fifo_update 1 1 } } }
	input_data_data_stream_2_V { ap_fifo {  { input_data_data_stream_2_V_dout fifo_data 0 8 }  { input_data_data_stream_2_V_empty_n fifo_status 0 1 }  { input_data_data_stream_2_V_read fifo_update 1 1 } } }
	passthrough_data_stream_0_V { ap_fifo {  { passthrough_data_stream_0_V_din fifo_data 1 8 }  { passthrough_data_stream_0_V_full_n fifo_status 0 1 }  { passthrough_data_stream_0_V_write fifo_update 1 1 } } }
	passthrough_data_stream_1_V { ap_fifo {  { passthrough_data_stream_1_V_din fifo_data 1 8 }  { passthrough_data_stream_1_V_full_n fifo_status 0 1 }  { passthrough_data_stream_1_V_write fifo_update 1 1 } } }
	passthrough_data_stream_2_V { ap_fifo {  { passthrough_data_stream_2_V_din fifo_data 1 8 }  { passthrough_data_stream_2_V_full_n fifo_status 0 1 }  { passthrough_data_stream_2_V_write fifo_update 1 1 } } }
	crop_data_data_stream_0_V { ap_fifo {  { crop_data_data_stream_0_V_din fifo_data 1 8 }  { crop_data_data_stream_0_V_full_n fifo_status 0 1 }  { crop_data_data_stream_0_V_write fifo_update 1 1 } } }
	crop_data_data_stream_1_V { ap_fifo {  { crop_data_data_stream_1_V_din fifo_data 1 8 }  { crop_data_data_stream_1_V_full_n fifo_status 0 1 }  { crop_data_data_stream_1_V_write fifo_update 1 1 } } }
	crop_data_data_stream_2_V { ap_fifo {  { crop_data_data_stream_2_V_din fifo_data 1 8 }  { crop_data_data_stream_2_V_full_n fifo_status 0 1 }  { crop_data_data_stream_2_V_write fifo_update 1 1 } } }
}
