#include <hls_opencv.h>
#include <iostream>

typedef ap_axiu<24,1,1,1> pixel_t;
typedef hls::stream<pixel_t> stream_t;

void center_image(stream_t &image_in, stream_t &image_out, stream_t &crop_out);

int main(int argc, char **argv) {
    // Load reference image
    IplImage *image_in = cvLoadImage("7_1920x1080.jpg");
    CvSize size_in = cvGetSize(image_in);

    // Create output image (with sides blacken out)
    IplImage *image_out = cvCreateImage(size_in, image_in->depth, image_in->nChannels);

    // Create output cropped image
    CvSize size_crop = { 1080, 1080 };
    IplImage *image_crop = cvCreateImage(size_crop, image_in->depth, image_in->nChannels);

    // Create streams.
    stream_t stream_in, stream_out, stream_crop;

    // Convert OpenCV data to AXI-Stream.
    IplImage2AXIvideo(image_in, stream_in);

    // Call the center_image() function.
    center_image(stream_in, stream_out, stream_crop);

    // Convert AXI-Stream data to OpenCV.
    AXIvideo2IplImage(stream_out, image_out);
    AXIvideo2IplImage(stream_crop, image_crop);

    // Save output image.
    cvSaveImage("/home/faku/master-project/vivado_hls/center_image/tb/image_out.jpg", image_out);
    cvSaveImage("/home/faku/master-project/vivado_hls/center_image/tb/image_crop.jpg", image_crop);

    cvReleaseImage(&image_in);
    cvReleaseImage(&image_out);
    cvReleaseImage(&image_crop);

    return 0;
}
