#include <ap_axi_sdata.h>
#include <hls_video.h>

#define HEIGHT_IN   1080
#define WIDTH_IN    1080

#define HEIGHT_OUT  28
#define WIDTH_OUT   28

typedef ap_axiu<24,1,1,1> pixel24_t;
typedef ap_axiu< 8,1,1,1> pixel8_t;
typedef hls::stream<pixel24_t> stream24_t;
typedef hls::stream<pixel8_t> stream8_t;

void gray_rescale(stream24_t &stream_in, ap_uint<8> threshold, stream8_t &stream_out) {
#pragma HLS INTERFACE axis port=stream_in
#pragma HLS INTERFACE s_axilite port=threshold
#pragma HLS INTERFACE axis port=stream_out

#pragma HLS dataflow

    hls::Mat< HEIGHT_IN,  WIDTH_IN, HLS_8UC3>   input_data(HEIGHT_IN,  WIDTH_IN);
    hls::Mat<HEIGHT_OUT, WIDTH_OUT, HLS_8UC3>     resized(HEIGHT_OUT, WIDTH_OUT);
    hls::Mat<HEIGHT_OUT, WIDTH_OUT, HLS_8UC1>        gray(HEIGHT_OUT, WIDTH_OUT);
    hls::Mat<HEIGHT_OUT, WIDTH_OUT, HLS_8UC1>          bw(HEIGHT_OUT, WIDTH_OUT);
    hls::Mat<HEIGHT_OUT, WIDTH_OUT, HLS_8UC1> output_data(HEIGHT_OUT, WIDTH_OUT);

    hls::AXIvideo2Mat(stream_in, input_data);
    hls::Resize(input_data, resized, HLS_INTER_LINEAR);
    hls::CvtColor<HLS_RGB2GRAY>(resized, gray);
    hls::Threshold(gray, bw, threshold, 255, HLS_THRESH_BINARY);
    // hls::Not(bw, output_data);
    hls::Mat2AXIvideo(bw /*output_data*/, stream_out);
}
