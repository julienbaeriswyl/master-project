// ==============================================================
// RTL generated by Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC
// Version: 2018.2
// Copyright (C) 1986-2018 Xilinx, Inc. All Rights Reserved.
// 
// ===========================================================

`timescale 1 ns / 1 ps 

module Block_proc157 (
        ap_clk,
        ap_rst,
        ap_start,
        start_full_n,
        ap_done,
        ap_continue,
        ap_idle,
        ap_ready,
        start_out,
        start_write,
        threshold_V,
        input_data_rows_V_out_din,
        input_data_rows_V_out_full_n,
        input_data_rows_V_out_write,
        input_data_cols_V_out_din,
        input_data_cols_V_out_full_n,
        input_data_cols_V_out_write,
        resized_rows_V_out_din,
        resized_rows_V_out_full_n,
        resized_rows_V_out_write,
        resized_cols_V_out_din,
        resized_cols_V_out_full_n,
        resized_cols_V_out_write,
        threshold_V_out_din,
        threshold_V_out_full_n,
        threshold_V_out_write
);

parameter    ap_ST_fsm_state1 = 1'd1;

input   ap_clk;
input   ap_rst;
input   ap_start;
input   start_full_n;
output   ap_done;
input   ap_continue;
output   ap_idle;
output   ap_ready;
output   start_out;
output   start_write;
input  [7:0] threshold_V;
output  [11:0] input_data_rows_V_out_din;
input   input_data_rows_V_out_full_n;
output   input_data_rows_V_out_write;
output  [11:0] input_data_cols_V_out_din;
input   input_data_cols_V_out_full_n;
output   input_data_cols_V_out_write;
output  [5:0] resized_rows_V_out_din;
input   resized_rows_V_out_full_n;
output   resized_rows_V_out_write;
output  [5:0] resized_cols_V_out_din;
input   resized_cols_V_out_full_n;
output   resized_cols_V_out_write;
output  [7:0] threshold_V_out_din;
input   threshold_V_out_full_n;
output   threshold_V_out_write;

reg ap_done;
reg ap_idle;
reg start_write;
reg input_data_rows_V_out_write;
reg input_data_cols_V_out_write;
reg resized_rows_V_out_write;
reg resized_cols_V_out_write;
reg threshold_V_out_write;

reg    real_start;
reg    start_once_reg;
reg    ap_done_reg;
(* fsm_encoding = "none" *) reg   [0:0] ap_CS_fsm;
wire    ap_CS_fsm_state1;
reg    internal_ap_ready;
reg    input_data_rows_V_out_blk_n;
reg    input_data_cols_V_out_blk_n;
reg    resized_rows_V_out_blk_n;
reg    resized_cols_V_out_blk_n;
reg    threshold_V_out_blk_n;
reg    ap_block_state1;
reg   [0:0] ap_NS_fsm;

// power-on initialization
initial begin
#0 start_once_reg = 1'b0;
#0 ap_done_reg = 1'b0;
#0 ap_CS_fsm = 1'd1;
end

always @ (posedge ap_clk) begin
    if (ap_rst == 1'b1) begin
        ap_CS_fsm <= ap_ST_fsm_state1;
    end else begin
        ap_CS_fsm <= ap_NS_fsm;
    end
end

always @ (posedge ap_clk) begin
    if (ap_rst == 1'b1) begin
        ap_done_reg <= 1'b0;
    end else begin
        if ((ap_continue == 1'b1)) begin
            ap_done_reg <= 1'b0;
        end else if ((~((real_start == 1'b0) | (threshold_V_out_full_n == 1'b0) | (resized_cols_V_out_full_n == 1'b0) | (resized_rows_V_out_full_n == 1'b0) | (input_data_cols_V_out_full_n == 1'b0) | (input_data_rows_V_out_full_n == 1'b0) | (ap_done_reg == 1'b1)) & (1'b1 == ap_CS_fsm_state1))) begin
            ap_done_reg <= 1'b1;
        end
    end
end

always @ (posedge ap_clk) begin
    if (ap_rst == 1'b1) begin
        start_once_reg <= 1'b0;
    end else begin
        if (((internal_ap_ready == 1'b0) & (real_start == 1'b1))) begin
            start_once_reg <= 1'b1;
        end else if ((internal_ap_ready == 1'b1)) begin
            start_once_reg <= 1'b0;
        end
    end
end

always @ (*) begin
    if ((~((real_start == 1'b0) | (threshold_V_out_full_n == 1'b0) | (resized_cols_V_out_full_n == 1'b0) | (resized_rows_V_out_full_n == 1'b0) | (input_data_cols_V_out_full_n == 1'b0) | (input_data_rows_V_out_full_n == 1'b0) | (ap_done_reg == 1'b1)) & (1'b1 == ap_CS_fsm_state1))) begin
        ap_done = 1'b1;
    end else begin
        ap_done = ap_done_reg;
    end
end

always @ (*) begin
    if (((real_start == 1'b0) & (1'b1 == ap_CS_fsm_state1))) begin
        ap_idle = 1'b1;
    end else begin
        ap_idle = 1'b0;
    end
end

always @ (*) begin
    if ((1'b1 == ap_CS_fsm_state1)) begin
        input_data_cols_V_out_blk_n = input_data_cols_V_out_full_n;
    end else begin
        input_data_cols_V_out_blk_n = 1'b1;
    end
end

always @ (*) begin
    if ((~((real_start == 1'b0) | (threshold_V_out_full_n == 1'b0) | (resized_cols_V_out_full_n == 1'b0) | (resized_rows_V_out_full_n == 1'b0) | (input_data_cols_V_out_full_n == 1'b0) | (input_data_rows_V_out_full_n == 1'b0) | (ap_done_reg == 1'b1)) & (1'b1 == ap_CS_fsm_state1))) begin
        input_data_cols_V_out_write = 1'b1;
    end else begin
        input_data_cols_V_out_write = 1'b0;
    end
end

always @ (*) begin
    if ((1'b1 == ap_CS_fsm_state1)) begin
        input_data_rows_V_out_blk_n = input_data_rows_V_out_full_n;
    end else begin
        input_data_rows_V_out_blk_n = 1'b1;
    end
end

always @ (*) begin
    if ((~((real_start == 1'b0) | (threshold_V_out_full_n == 1'b0) | (resized_cols_V_out_full_n == 1'b0) | (resized_rows_V_out_full_n == 1'b0) | (input_data_cols_V_out_full_n == 1'b0) | (input_data_rows_V_out_full_n == 1'b0) | (ap_done_reg == 1'b1)) & (1'b1 == ap_CS_fsm_state1))) begin
        input_data_rows_V_out_write = 1'b1;
    end else begin
        input_data_rows_V_out_write = 1'b0;
    end
end

always @ (*) begin
    if ((~((real_start == 1'b0) | (threshold_V_out_full_n == 1'b0) | (resized_cols_V_out_full_n == 1'b0) | (resized_rows_V_out_full_n == 1'b0) | (input_data_cols_V_out_full_n == 1'b0) | (input_data_rows_V_out_full_n == 1'b0) | (ap_done_reg == 1'b1)) & (1'b1 == ap_CS_fsm_state1))) begin
        internal_ap_ready = 1'b1;
    end else begin
        internal_ap_ready = 1'b0;
    end
end

always @ (*) begin
    if (((start_full_n == 1'b0) & (start_once_reg == 1'b0))) begin
        real_start = 1'b0;
    end else begin
        real_start = ap_start;
    end
end

always @ (*) begin
    if ((1'b1 == ap_CS_fsm_state1)) begin
        resized_cols_V_out_blk_n = resized_cols_V_out_full_n;
    end else begin
        resized_cols_V_out_blk_n = 1'b1;
    end
end

always @ (*) begin
    if ((~((real_start == 1'b0) | (threshold_V_out_full_n == 1'b0) | (resized_cols_V_out_full_n == 1'b0) | (resized_rows_V_out_full_n == 1'b0) | (input_data_cols_V_out_full_n == 1'b0) | (input_data_rows_V_out_full_n == 1'b0) | (ap_done_reg == 1'b1)) & (1'b1 == ap_CS_fsm_state1))) begin
        resized_cols_V_out_write = 1'b1;
    end else begin
        resized_cols_V_out_write = 1'b0;
    end
end

always @ (*) begin
    if ((1'b1 == ap_CS_fsm_state1)) begin
        resized_rows_V_out_blk_n = resized_rows_V_out_full_n;
    end else begin
        resized_rows_V_out_blk_n = 1'b1;
    end
end

always @ (*) begin
    if ((~((real_start == 1'b0) | (threshold_V_out_full_n == 1'b0) | (resized_cols_V_out_full_n == 1'b0) | (resized_rows_V_out_full_n == 1'b0) | (input_data_cols_V_out_full_n == 1'b0) | (input_data_rows_V_out_full_n == 1'b0) | (ap_done_reg == 1'b1)) & (1'b1 == ap_CS_fsm_state1))) begin
        resized_rows_V_out_write = 1'b1;
    end else begin
        resized_rows_V_out_write = 1'b0;
    end
end

always @ (*) begin
    if (((start_once_reg == 1'b0) & (real_start == 1'b1))) begin
        start_write = 1'b1;
    end else begin
        start_write = 1'b0;
    end
end

always @ (*) begin
    if ((1'b1 == ap_CS_fsm_state1)) begin
        threshold_V_out_blk_n = threshold_V_out_full_n;
    end else begin
        threshold_V_out_blk_n = 1'b1;
    end
end

always @ (*) begin
    if ((~((real_start == 1'b0) | (threshold_V_out_full_n == 1'b0) | (resized_cols_V_out_full_n == 1'b0) | (resized_rows_V_out_full_n == 1'b0) | (input_data_cols_V_out_full_n == 1'b0) | (input_data_rows_V_out_full_n == 1'b0) | (ap_done_reg == 1'b1)) & (1'b1 == ap_CS_fsm_state1))) begin
        threshold_V_out_write = 1'b1;
    end else begin
        threshold_V_out_write = 1'b0;
    end
end

always @ (*) begin
    case (ap_CS_fsm)
        ap_ST_fsm_state1 : begin
            ap_NS_fsm = ap_ST_fsm_state1;
        end
        default : begin
            ap_NS_fsm = 'bx;
        end
    endcase
end

assign ap_CS_fsm_state1 = ap_CS_fsm[32'd0];

always @ (*) begin
    ap_block_state1 = ((real_start == 1'b0) | (threshold_V_out_full_n == 1'b0) | (resized_cols_V_out_full_n == 1'b0) | (resized_rows_V_out_full_n == 1'b0) | (input_data_cols_V_out_full_n == 1'b0) | (input_data_rows_V_out_full_n == 1'b0) | (ap_done_reg == 1'b1));
end

assign ap_ready = internal_ap_ready;

assign input_data_cols_V_out_din = 12'd1080;

assign input_data_rows_V_out_din = 12'd1080;

assign resized_cols_V_out_din = 6'd28;

assign resized_rows_V_out_din = 6'd28;

assign start_out = real_start;

assign threshold_V_out_din = threshold_V;

endmodule //Block_proc157
