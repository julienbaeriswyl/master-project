#include "hls_design_meta.h"
const Port_Property HLS_Design_Meta::port_props[]={
	Port_Property("ap_clk", 1, hls_in, -1, "", "", 1),
	Port_Property("ap_rst_n", 1, hls_in, -1, "", "", 1),
	Port_Property("ap_start", 1, hls_in, -1, "", "", 1),
	Port_Property("ap_done", 1, hls_out, -1, "", "", 1),
	Port_Property("ap_idle", 1, hls_out, -1, "", "", 1),
	Port_Property("ap_ready", 1, hls_out, -1, "", "", 1),
	Port_Property("stream_in_TDATA", 32, hls_in, 0, "axis", "in_data", 1),
	Port_Property("stream_in_TVALID", 1, hls_in, 6, "axis", "in_vld", 1),
	Port_Property("stream_in_TREADY", 1, hls_out, 6, "axis", "in_acc", 1),
	Port_Property("stream_in_TKEEP", 4, hls_in, 1, "axis", "in_data", 1),
	Port_Property("stream_in_TSTRB", 4, hls_in, 2, "axis", "in_data", 1),
	Port_Property("stream_in_TUSER", 1, hls_in, 3, "axis", "in_data", 1),
	Port_Property("stream_in_TLAST", 1, hls_in, 4, "axis", "in_data", 1),
	Port_Property("stream_in_TID", 1, hls_in, 5, "axis", "in_data", 1),
	Port_Property("stream_in_TDEST", 1, hls_in, 6, "axis", "in_data", 1),
	Port_Property("ap_return", 4, hls_out, -1, "", "", 1),
};
const char* HLS_Design_Meta::dut_name = "mnist_inference";
